﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;


public partial class Reports_Rpt_MonthlyProd : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionFinYearCode;
    string SessionFinYearVal;

    string RptName = ""; string RptType = "";
    string ButtonName = ""; string Count = ""; string CountName = "";
    string MachineName = ""; string ProdNo = ""; string Shift = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "ERP Module :: Production Report";
            purchase_Request_Department_Add();
        }
    }

    private void purchase_Request_Department_Add()
    {
        txtDepartment.Items.Clear();
        txtDepartment.Items.Add("-Select-");
        txtDepartment.Items.Add("Carding Production");
        txtDepartment.Items.Add("Drawing Production");
        txtDepartment.Items.Add("OE Production");
    }

    protected void btnReport_Click(object sender, EventArgs e)
    {
        RptName = "Monthly Production Report";

        ResponseHelper.Redirect("ReportDisplay.aspx?DeptName=" + txtDepartment.SelectedItem.Text + "&FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text +"&ReportName=" + RptName, "_blank", "");

    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        txtDepartment.SelectedValue = "-Select-";
        txtFromDate.Text = ""; txtToDate.Text = "";
    }
}
