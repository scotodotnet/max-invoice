﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;


public partial class Reports_Rpt_RawMaterial : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionEnquiryNo;
    string SessionFinYearCode;
    string SessionFinYearVal;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();
        if (!IsPostBack)
        {
            Page.Title = "ERP Module :: Raw Material Report";
            HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_RptStock"));
            li.Attributes.Add("class", "droplink active open");

            GoDown_Name_Load();
            Supplier_Name_Load();
            Variety_Name_Load();
            Transaction_No_Load();
        }
    }

    private void GoDown_Name_Load()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        txtGoDownName.Items.Clear();
        query = "Select * from MstGoDown where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        txtGoDownName.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["GoDownID"] = "-Select-";
        dr["GoDownName"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        txtGoDownName.DataTextField = "GoDownName";
        txtGoDownName.DataValueField = "GoDownID";
        txtGoDownName.DataBind();
    }

    private void Supplier_Name_Load()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        txtSupplierName.Items.Clear();
        query = "Select * from MstSupplier where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        txtSupplierName.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["SuppCode"] = "-Select-";
        dr["SuppName"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        txtSupplierName.DataTextField = "SuppName";
        txtSupplierName.DataValueField = "SuppCode";
        txtSupplierName.DataBind();
    }

    private void Variety_Name_Load()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        txtVarietyName.Items.Clear();
        query = "Select * from MstVariety where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        txtVarietyName.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["VarietyID"] = "-Select-";
        dr["VarietyName"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        txtVarietyName.DataTextField = "VarietyName";
        txtVarietyName.DataValueField = "VarietyID";
        txtVarietyName.DataBind();
    }

    private void Transaction_No_Load()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        txtTransNo.Items.Clear();
        query = "Select * from RawMaterial where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        txtTransNo.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["Trans_No"] = "-Select-";
        dr["Trans_No"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        txtTransNo.DataTextField = "Trans_No";
        txtTransNo.DataValueField = "Trans_No";
        txtTransNo.DataBind();
    }
    protected void btnReport_Click(object sender, EventArgs e)
    {
        string RptName = "Raw Material Receipt Details Report";
        string TransNo;
        string SuppCode;
        string GoDownID;

        string VarietyCode;

        if (txtTransNo.SelectedItem.Text != "-Select-")
        {
            TransNo = txtTransNo.SelectedItem.Text;
        }
        else
        {
            TransNo = "";
        }
        if (txtSupplierName.SelectedItem.Text != "-Select-")
        {
            SuppCode = txtSupplierName.SelectedValue;
            
        }
        else
        {
            SuppCode = "";
           
        }

        if (txtGoDownName.SelectedItem.Text != "-Select-")
        {
            GoDownID = txtGoDownName.SelectedValue;
        }
        else
        {
            GoDownID = "";
        }

        if (txtVarietyName.SelectedItem.Text != "-Select-")
        {
            VarietyCode = txtVarietyName.SelectedValue;
        }
        else
        {
            VarietyCode = "";
        }


        ResponseHelper.Redirect("ReportDisplay.aspx?SuppCode=" + SuppCode + "&TransNo=" + TransNo + "&GoDownID=" + GoDownID + "&VarietyCode=" + VarietyCode + "&FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text + "&ReportName=" + RptName, "_blank", "");
   
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        txtTransNo.SelectedValue = "-Select-";
        txtSupplierName.SelectedValue = "-Select-";
        txtGoDownName.SelectedValue = "-Select-";
        txtVarietyName.SelectedValue = "-Select-";
        txtFromDate.Text = ""; txtToDate.Text = "";
    }
}
