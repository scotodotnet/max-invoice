﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;


public partial class Rpt_DetailOE : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionEnquiryNo;
    string SessionFinYearCode;
    string SessionFinYearVal;
    string SessionUnplanReceiptNo;
    protected void Page_Load(object sender, EventArgs e)
    {

        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();
        if (!IsPostBack)
        {
            Page.Title = "ERP Module ::OE Details Report";
            HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_RptStock"));
            li.Attributes.Add("class", "droplink active open");

            Load_LotNo();
            Transaction_No_Load();
            Color();
            Load_MachineName();

        }
    }
    private void Load_LotNo()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlLotNo.Items.Clear();
        query = "Select Distinct LotNo from Carding_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' and FinYearCode='" + SessionFinYearCode + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlLotNo.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["LotNo"] = "-Select-";
        dr["LotNo"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlLotNo.DataTextField = "LotNo";
        ddlLotNo.DataValueField = "LotNo";
        ddlLotNo.DataBind();
    }
    private void Transaction_No_Load()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        txtTransNo.Items.Clear();
        query = "Select * from OE_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        txtTransNo.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["Trans_No"] = "-Select-";
        dr["Trans_No"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        txtTransNo.DataTextField = "Trans_No";
        txtTransNo.DataValueField = "Trans_No";
        txtTransNo.DataBind();
    }
    private void Load_MachineName()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        txtMachineName.Items.Clear();
        query = "Select *from MstMachine where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And DeptName='OE'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        txtMachineName.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["MachineName"] = "-Select-";
        dr["MachineCode"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        txtMachineName.DataTextField = "MachineName";
        txtMachineName.DataValueField = "MachineCode";
        txtMachineName.DataBind();
    }

    private void Color()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        txtColorName.Items.Clear();
        query = "Select *from MstColor where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        txtColorName.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["ColorName"] = "-Select-";
        dr["ColorName"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        txtColorName.DataTextField = "ColorName";
        txtColorName.DataValueField = "ColorName";
        txtColorName.DataBind();
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        txtTransNo.SelectedValue = "-Select-";
        txtColorName.SelectedValue = "-Select-";
        txtMachineName.SelectedValue = "-Select-";

        txtFromDate.Text = ""; txtToDate.Text = "";
    }
    protected void btnAbstract_Click(object sender, EventArgs e)
    {
        string RptName = "OE Abstract Details Report";

        string TransNo;
        string MachineName;
        string Color;
        string LotNo;
        string VarietyCode;

        if (txtTransNo.SelectedItem.Text != "-Select-")
        {
            TransNo = txtTransNo.SelectedItem.Text;
        }
        else
        {
            TransNo = "";
        }
        if (txtMachineName.SelectedItem.Text != "-Select-")
        {
            MachineName = txtMachineName.SelectedValue;

        }
        else
        {
            MachineName = "";

        }

        if (txtColorName.SelectedItem.Text != "-Select-")
        {
            Color = txtColorName.SelectedValue;
        }
        else
        {
            Color = "";



        }
        if (ddlLotNo.SelectedItem.Text != "-Select-")
        {
            LotNo = ddlLotNo.SelectedValue;
        }
        else
        {
            LotNo = "";

        }


        if (txtFromDate.Text != "")
        {
            string[] DateSplit = txtFromDate.Text.Split('/');
            string formDate = "01/" + DateSplit[1] + "/" + DateSplit[2];
            string toDate = txtFromDate.Text;

            ResponseHelper.Redirect("ReportDisplay.aspx?MachineName=" + MachineName + "&TransNo=" + TransNo + "&Color=" + Color + "&FromDate=" + formDate + "&ToDate=" + toDate + "&ReportName=" + RptName + "&LotNo=" + LotNo, "_blank", "");
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the From Date.');", true);
        }

    }
      protected void btnDailyReport_Click(object sender, EventArgs e)
    {
        string RptName = "OE Daily Report";
        string TransNo;
        string MachineName;
        string Color;

        string VarietyCode;

        if (txtTransNo.SelectedItem.Text != "-Select-")
        {
            TransNo = txtTransNo.SelectedItem.Text;
        }
        else
        {
            TransNo = "";
        }
        if (txtMachineName.SelectedItem.Text != "-Select-")
        {
            MachineName = txtMachineName.SelectedValue;

        }
        else
        {
            MachineName = "";

        }

        if (txtColorName.SelectedItem.Text != "-Select-")
        {
            Color = txtColorName.SelectedValue;
        }
        else
        {
            Color = "";



        }

        ResponseHelper.Redirect("ReportDisplay.aspx?MachineName=" + MachineName + "&TransNo=" + TransNo + "&Color=" + Color + "&FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text + "&ReportName=" + RptName, "_blank", "");


      }
    protected void btnReport_Click(object sender, EventArgs e)
    {
        string RptName = "OE Details Report";
        string TransNo;
        string MachineName;
        string Color;

        string VarietyCode;

        if (txtTransNo.SelectedItem.Text != "-Select-")
        {
            TransNo = txtTransNo.SelectedItem.Text;
        }
        else
        {
            TransNo = "";
        }
        if (txtMachineName.SelectedItem.Text != "-Select-")
        {
            MachineName = txtMachineName.SelectedValue;

        }
        else
        {
            MachineName = "";

        }

        if (txtColorName.SelectedItem.Text != "-Select-")
        {
            Color = txtColorName.SelectedValue;
        }
        else
        {
            Color = "";



        }

        ResponseHelper.Redirect("ReportDisplay.aspx?MachineName=" + MachineName + "&TransNo=" + TransNo + "&Color=" + Color + "&FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text + "&ReportName=" + RptName, "_blank", "");


    }
    protected void btnOEDetails_Click(object sender, EventArgs e)
    {
        string RptName = "OE Over All Details Report";
        string TransNo;
        string MachineName;
        string Color;
        string LotNo;
        string VarietyCode;

        if (txtTransNo.SelectedItem.Text != "-Select-")
        {
            TransNo = txtTransNo.SelectedItem.Text;
        }
        else
        {
            TransNo = "";
        }
        if (txtMachineName.SelectedItem.Text != "-Select-")
        {
            MachineName = txtMachineName.SelectedValue;

        }
        else
        {
            MachineName = "";

        }

        if (txtColorName.SelectedItem.Text != "-Select-")
        {
            Color = txtColorName.SelectedValue;
        }
        else
        {
            Color = "";



        }
        if (ddlLotNo.SelectedItem.Text != "-Select-")
        {
            LotNo = ddlLotNo.SelectedValue;
        }
        else
        {
            LotNo = "";

        }

        ResponseHelper.Redirect("ReportDisplay.aspx?MachineName=" + MachineName + "&TransNo=" + TransNo + "&Color=" + Color + "&FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text + "&ReportName=" + RptName + "&LotNo=" + LotNo, "_blank", "");
    }

    protected void btnBtwDateReport_Click(object sender, EventArgs e)
    {
        string RptName = "OE Abstract Between Dates Details Report";

        string TransNo;
        string MachineName;
        string Color;
        string LotNo;
        string VarietyCode;

        if (txtTransNo.SelectedItem.Text != "-Select-")
        {
            TransNo = txtTransNo.SelectedItem.Text;
        }
        else
        {
            TransNo = "";
        }
        if (txtMachineName.SelectedItem.Text != "-Select-")
        {
            MachineName = txtMachineName.SelectedValue;

        }
        else
        {
            MachineName = "";

        }

        if (txtColorName.SelectedItem.Text != "-Select-")
        {
            Color = txtColorName.SelectedValue;
        }
        else
        {
            Color = "";



        }
        if (ddlLotNo.SelectedItem.Text != "-Select-")
        {
            LotNo = ddlLotNo.SelectedValue;
        }
        else
        {
            LotNo = "";

        }


        //if (txtFromDate.Text != "")
        //{

        string formDate = txtFromDate.Text;
        string toDate = txtToDate.Text;

            ResponseHelper.Redirect("ReportDisplay.aspx?MachineName=" + MachineName + "&TransNo=" + TransNo + "&Color=" + Color + "&FromDate=" + formDate + "&ToDate=" + toDate + "&ReportName=" + RptName + "&LotNo=" + LotNo, "_blank", "");
        //}
        //else
        //{
        //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the From Date.');", true);
        //}

    }
}
