﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
public partial class Reports_Rpt_Del_Rawmaterial : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionEnquiryNo;
    string SessionFinYearCode;
    string SessionFinYearVal;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();
        if (!IsPostBack)
        {
            Page.Title = "ERP Module :: Rawmaterial Report";
            HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_RptStock"));
            li.Attributes.Add("class", "droplink active open");

            Load_Trans();
            Load_LotNo();
            Load_Variety();

        }
    }
    private void Load_Trans()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        txtTrans.Items.Clear();
        query = "Select Trans_No from RawMaterialDelivery_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        txtTrans.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["Trans_No"] = "-Select-";
        dr["Trans_No"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        txtTrans.DataTextField = "Trans_No";
        txtTrans.DataValueField = "Trans_No";
        txtTrans.DataBind();
    }
    private void Load_LotNo()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        txtLotNo.Items.Clear();
        query = "Select distinct LotNo from RawMaterialDelivery_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        txtLotNo.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["LotNo"] = "-Select-";
        dr["LotNo"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        txtLotNo.DataTextField = "LotNo";
        txtLotNo.DataValueField = "LotNo";
        txtLotNo.DataBind();
    }
    private void Load_Variety()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlVariety.Items.Clear();
        query = "Select VarietyID,VarietyName from MstVariety where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlVariety.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["VarietyName"] = "-Select-";
        dr["VarietyID"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlVariety.DataTextField = "VarietyName";
        ddlVariety.DataValueField = "VarietyID";
        ddlVariety.DataBind();
    }
    protected void btnReport_Click(object sender, EventArgs e)
    {
        string[] finyear = SessionFinYearVal.Split('_');
        string RptName = "";
        string FromDate_Chk = "01/04/" + finyear[0];
        string ToDate_Chk = "31/03/" + finyear[1];

        string fromDate = txtFromDate.Text;
        string toDate = txtToDate.Text;
        string TransNo = "";
        string LotNo = "";
        string VarietyCode = "";

        if (txtTrans.SelectedItem.Text != "-Select-")
        {
            TransNo = txtTrans.SelectedValue;
        }
        else
        {
            TransNo = "";
        }

        if (txtLotNo.SelectedItem.Text != "-Select-")
        {
            LotNo = txtLotNo.SelectedValue;
        }
        else
        {
            LotNo = "";
        }

        if (ddlVariety.SelectedItem.Text != "-Select-")
        {
            VarietyCode = ddlVariety.SelectedValue;
        }
        else
        {
            VarietyCode = "";
        }

        if (txtFromDate.Text != "" && txtToDate.Text != "")
        {
            if ((Convert.ToDateTime(fromDate) >= Convert.ToDateTime(FromDate_Chk)) && (Convert.ToDateTime(toDate) <= Convert.ToDateTime(ToDate_Chk)))
            {
                RptName = "Delivery Rawmaterial Report";
                ResponseHelper.Redirect("ReportDisplay.aspx?FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text + "&TransCode=" + TransNo + "&LotNo=" + LotNo + "&VarietyCode=" + VarietyCode + "&ReportName=" + RptName, "_blank", "");
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check with given Date...');", true);
            }
        }
        else
        {
            RptName = "Delivery Rawmaterial Report";
            ResponseHelper.Redirect("ReportDisplay.aspx?FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text + "&TransCode=" + TransNo + "&LotNo=" + LotNo + "&VarietyCode=" + VarietyCode + "&ReportName=" + RptName, "_blank", "");
        }
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        txtFromDate.Text = ""; txtToDate.Text = "";
        txtTrans.SelectedValue = "-Select-";
        txtLotNo.SelectedValue = "-Select-";
        ddlVariety.SelectedValue = "-Select-";
    }
}
