﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;


public partial class Reports_Rpt_CottonInward : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionEnquiryNo;
    string SessionFinYearCode;
    string SessionFinYearVal;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();
        if (!IsPostBack)
        {
            Page.Title = "ERP Module :: CottonInward Report";
            HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_RptStock"));
            li.Attributes.Add("class", "droplink active open");


            Lot_No_Load();
            Transaction_No_Load();
        }
    }

    private void Transaction_No_Load()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        txtTransNo.Items.Clear();
        query = "Select * from CottonInwards where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        txtTransNo.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["Ctn_Inwards_No"] = "-Select-";
        dr["Ctn_Inwards_No"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        txtTransNo.DataTextField = "Ctn_Inwards_No";
        txtTransNo.DataValueField = "Ctn_Inwards_No";
        txtTransNo.DataBind();
    }

    private void Lot_No_Load()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        txtLotNo.Items.Clear();
        query = "Select * from CottonInwards where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        txtLotNo.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["LotNo"] = "-Select-";
        dr["LotNo"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        txtLotNo.DataTextField = "LotNo";
        txtLotNo.DataValueField = "LotNo";
        txtLotNo.DataBind();
    }

    protected void btnReport_Click(object sender, EventArgs e)
    {
        string RptName = "Cotton Inward Details Report";
        string TransNo;
        string LotNo;
       
        if (txtTransNo.SelectedItem.Text != "-Select-")
        {
            TransNo = txtTransNo.SelectedItem.Text;
        }
        else
        {
            TransNo = "";
        }
        

        if (txtLotNo.SelectedItem.Text != "-Select-")
        {
            LotNo = txtLotNo.SelectedValue;
        }
        else
        {
            LotNo = "";
        }

        ResponseHelper.Redirect("ReportDisplay.aspx?TransNo=" + TransNo + "&LotNo=" + LotNo + "&FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text + "&ReportName=" + RptName, "_blank", "");

    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        txtFromDate.Text = "";
        txtToDate.Text = "";
        txtLotNo.SelectedValue = "-Select-";
        txtTransNo.SelectedValue = "-Select-";
    }
}
