﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;


public partial class Reports_Rpt_YarnStock : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionEnquiryNo;
    string SessionFinYearCode;
    string SessionFinYearVal;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();
        if (!IsPostBack)
        {
            Page.Title = "ERP Module :: Raw Material Stock";
            HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_RptStock"));
            li.Attributes.Add("class", "droplink active open");

            Load_GoDown();
            Load_ItemName();

        }
    }

    private void Load_ItemName()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlItemName.Items.Clear();
        query = "Select *from MstCount where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlItemName.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["CountName"] = "-Select-";
        dr["CountID"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlItemName.DataTextField = "CountName";
        ddlItemName.DataValueField = "CountID";
        ddlItemName.DataBind();
    }

    private void Load_GoDown()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        txtGoDownName.Items.Clear();
        query = "Select *from MstGoDown where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        txtGoDownName.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["GoDownName"] = "-Select-";
        dr["GoDownID"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        txtGoDownName.DataTextField = "GoDownName";
        txtGoDownName.DataValueField = "GoDownID";
        txtGoDownName.DataBind();
    }

    protected void btnReport_Click(object sender, EventArgs e)
    {
        string[] finyear = SessionFinYearVal.Split('_');
        string RptName = "";
        string FromDate_Chk = "01/04/" + finyear[0];
        string ToDate_Chk = "31/03/" + finyear[1];

        string fromDate = txtFromDate.Text;
        string toDate = txtToDate.Text;
        string WareHouseCode = "";
        string ZoneCode = "";
        string VarietyCode = "";

        if (txtGoDownName.SelectedItem.Text != "-Select-")
        {
            WareHouseCode = txtGoDownName.SelectedValue;
        }
        else
        {
            WareHouseCode = "";
        }


        if (ddlItemName.SelectedItem.Text != "-Select-")
        {
            VarietyCode = ddlItemName.SelectedValue;
        }
        else
        {
            VarietyCode = "";
        }

        if (txtFromDate.Text != "" && txtToDate.Text != "")
        {
            if ((Convert.ToDateTime(fromDate) >= Convert.ToDateTime(FromDate_Chk)) && (Convert.ToDateTime(toDate) <= Convert.ToDateTime(ToDate_Chk)))
            {
                RptName = "Yarn Stock Details Report";
                ResponseHelper.Redirect("ReportDisplay.aspx?FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text + "&WareHouseCode=" + WareHouseCode + "&VarietyCode=" + VarietyCode + "&ReportName=" + RptName, "_blank", "");
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check with given Date...');", true);
            }
        }
        else
        {
            RptName = "Yarn Stock Details Report";
            ResponseHelper.Redirect("ReportDisplay.aspx?FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text + "&WareHouseCode=" + WareHouseCode + "&VarietyCode=" + VarietyCode + "&ReportName=" + RptName, "_blank", "");
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        txtFromDate.Text = ""; txtToDate.Text = "";
        txtGoDownName.SelectedValue = "-Select-";
        ddlItemName.SelectedValue = "-Select-";
    }
    protected void btnAbstract_Click(object sender, EventArgs e)
    {
        string[] finyear = SessionFinYearVal.Split('_');
        string RptName = "";
        string FromDate_Chk = "01/04/" + finyear[0];
        string ToDate_Chk = "31/03/" + finyear[1];

        string fromDate = txtFromDate.Text;
        string toDate = txtToDate.Text;
        string WareHouseCode = "";
        string ZoneCode = "";
        string VarietyCode = "";

        if (txtGoDownName.SelectedItem.Text != "-Select-")
        {
            WareHouseCode = txtGoDownName.SelectedValue;
        }
        else
        {
            WareHouseCode = "";
        }


        if (ddlItemName.SelectedItem.Text != "-Select-")
        {
            VarietyCode = ddlItemName.SelectedValue;
        }
        else
        {
            VarietyCode = "";
        }

        if (txtFromDate.Text != "" && txtToDate.Text != "")
        {
            if ((Convert.ToDateTime(fromDate) >= Convert.ToDateTime(FromDate_Chk)) && (Convert.ToDateTime(toDate) <= Convert.ToDateTime(ToDate_Chk)))
            {
                RptName = "Yarn Stock Abstract Report";
                ResponseHelper.Redirect("ReportDisplay.aspx?FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text + "&WareHouseCode=" + WareHouseCode + "&VarietyCode=" + VarietyCode + "&ReportName=" + RptName, "_blank", "");
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check with given Date...');", true);
            }
        }
        else
        {
            RptName = "Yarn Stock Abstract Report";
            ResponseHelper.Redirect("ReportDisplay.aspx?FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text + "&WareHouseCode=" + WareHouseCode + "&VarietyCode=" + VarietyCode + "&ReportName=" + RptName, "_blank", "");
        }
    }
    protected void btnCurrent_Click(object sender, EventArgs e)
    {
        string[] finyear = SessionFinYearVal.Split('_');
        string RptName = "";
        string FromDate_Chk = "01/04/" + finyear[0];
        string ToDate_Chk = "31/03/" + finyear[1];

        string fromDate = txtFromDate.Text;
        string toDate = txtToDate.Text;
        string WareHouseCode = "";
        string ZoneCode = "";
        string VarietyCode = "";



        if (ddlItemName.SelectedItem.Text != "-Select-")
        {
            VarietyCode = ddlItemName.SelectedValue;
        }
        else
        {
            VarietyCode = "";
        }

        RptName = "Yarn Current Stock Report";
        ResponseHelper.Redirect("ReportDisplay.aspx?VarietyCode=" + VarietyCode + "&ReportName=" + RptName, "_blank", "");
    }
}
