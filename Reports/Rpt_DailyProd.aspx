﻿<%@ Page Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="Rpt_DailyProd.aspx.cs" Inherits="Reports_Rpt_DailyProd" Title="" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">



<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('.date-picker').datepicker({ format: 'dd/mm/yyyy' });
                $('.js-states').select2();
            }
        });
    };
</script>

<div class="page-breadcrumb">
    <ol class="breadcrumb container">
       <h4><li class="active">Daily Production Report</li></h4> 
    </ol>
</div>
<div id="main-wrapper" class="container">
 <div class="row">
    <div class="col-md-12">        
            <div class="col-md-9">
			<div class="panel panel-white">
			<div class="panel panel-primary">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">Daily Production Report</h4>
				</div>
			</div>
				<form class="form-horizontal">
				<div class="panel-body">
			
					<div class="col-md-12">
					    <div class="row">
					      
				  <div class="form-group col-md-4">
					      <label for="exampleInputName">Department<span class="mandatory">*</span></label>
					      <asp:DropDownList ID="txtDepartment" runat="server" class="js-states form-control">
					     </asp:DropDownList>
					     <asp:RequiredFieldValidator ControlToValidate="txtDepartment" InitialValue="-Select-" ValidationGroup="Validate_Field" class="form_error" ID="validate1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                         </asp:RequiredFieldValidator>
                          <asp:RequiredFieldValidator ControlToValidate="txtDepartment" InitialValue="-Select-" ValidationGroup="Validate_Field1" class="form_error" ID="RequiredFieldValidator2" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                         </asp:RequiredFieldValidator>
				   </div>
					      
		            <div class="form-group col-md-2">
			            <label for="exampleInputName">Date</label>
			            <asp:TextBox ID="txtFromDate" MaxLength="20" class="form-control date-picker" 
                         runat="server"></asp:TextBox>
                         <asp:RequiredFieldValidator ControlToValidate="txtFromDate" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
                         <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtenderFromDate" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                            TargetControlID="txtFromDate" ValidChars="0123456789./">
                         </cc1:FilteredTextBoxExtender>
		            </div>
		
                    </div>
                    </div>
	
					<div class="clearfix"></div>
					<div class="clearfix"></div>
					<div class="form-group row"></div>	
                    <!-- Button start -->
                    <div class="txtcenter">
                        <asp:Button ID="btnReport" class="btn-success"  runat="server" 
                            Text="Report" ValidationGroup="Validate_Field" height="32" Width="140" 
                            onclick="btnReport_Click"/>
                           
                        <asp:Button ID="btnClear" class="btn-danger" runat="server" 
                            Text="Clear" height="32" Width="120" onclick="btnClear_Click" />
                            
                            
                    </div>
                    <!-- Button end -->
				</div><!-- panel body end -->
				</form>
			</div><!-- panel white end -->
		    </div><!-- col-9 end -->
		    <!-- Dashboard start -->
		     <div class="col-lg-3 col-md-6">
                            <div class="panel panel-white" style="height: 100%;">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Dashboard Details</h4>
                                    <div class="panel-control">
                                    </div>
                                </div>
                                <div class="panel-body">
                                </div>
                            </div>
             </div>  
                        
             <div class="col-lg-3 col-md-6">
                 <div class="panel panel-white">
                    <div class="panel-body">
                       <div class="live-tile flip ha" data-mode="flip" data-speed="750" data-delay="3000">
                                      
                        </div>
                    </div>
                 </div>
              </div> 
		    <!-- Dashboard End -->
	<div class="col-md-2"></div>
	
  </div><!-- col 12 end -->
  </div><!-- row end -->
 </div><!-- main-wrapper end -->




</asp:Content>

