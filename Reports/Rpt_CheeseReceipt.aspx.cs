﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;


public partial class Reports_Rpt_CheeseReceipt : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionEnquiryNo;
    string SessionFinYearCode;
    string SessionFinYearVal;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();
        if (!IsPostBack)
        {
            Page.Title = "ERP Module :: Spares Report";
            HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_RptStock"));
            li.Attributes.Add("class", "droplink active open");

            WareHouse_Name_Load();
            Supplier_Name_Load();
            Item_Name_Load();
            Transaction_No_Load();
        }
    }

    private void WareHouse_Name_Load()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        txtWareHouseName.Items.Clear();
        query = "Select * from MstWarehouse where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        txtWareHouseName.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["WarehouseName"] = "-Select-";
        dr["WarehouseCode"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        txtWareHouseName.DataTextField = "WarehouseName";
        txtWareHouseName.DataValueField = "WarehouseCode";
        txtWareHouseName.DataBind();
    }

    private void Supplier_Name_Load()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        txtSupplierName.Items.Clear();
        query = "Select * from MstSupplier where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        txtSupplierName.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["SuppCode"] = "-Select-";
        dr["SuppName"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        txtSupplierName.DataTextField = "SuppName";
        txtSupplierName.DataValueField = "SuppCode";
        txtSupplierName.DataBind();
    }

    private void Item_Name_Load()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        txtItemName.Items.Clear();
        query = "Select * from MstCheese where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        txtItemName.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["CheeseCode"] = "-Select-";
        dr["CheeseName"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        txtItemName.DataTextField = "CheeseName";
        txtItemName.DataValueField = "CheeseCode";
        txtItemName.DataBind();
    }

    private void Transaction_No_Load()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        txtTransNo.Items.Clear();
        query = "Select * from Cheese_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        txtTransNo.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["Trans_No"] = "-Select-";
        dr["Trans_No"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        txtTransNo.DataTextField = "Trans_No";
        txtTransNo.DataValueField = "Trans_No";
        txtTransNo.DataBind();
    }

    protected void btnReport_Click(object sender, EventArgs e)
    {
        string RptName = "Cheese Receipt Details Report";
        string TransNo;
        string SuppCode;
        string WareHouseCode;

        string VarietyCode;

        if (txtTransNo.SelectedItem.Text != "-Select-")
        {
            TransNo = txtTransNo.SelectedItem.Text;
        }
        else
        {
            TransNo = "";
        }
        if (txtSupplierName.SelectedItem.Text != "-Select-")
        {
            SuppCode = txtSupplierName.SelectedValue;

        }
        else
        {
            SuppCode = "";

        }

        if (txtWareHouseName.SelectedItem.Text != "-Select-")
        {
            WareHouseCode = txtWareHouseName.SelectedValue;
        }
        else
        {
            WareHouseCode = "";
        }

        if (txtItemName.SelectedItem.Text != "-Select-")
        {
            VarietyCode = txtItemName.SelectedValue;
        }
        else
        {
            VarietyCode = "";
        }

        ResponseHelper.Redirect("ReportDisplay.aspx?SuppCode=" + SuppCode + "&TransNo=" + TransNo + "&WareHouseCode=" + WareHouseCode + "&VarietyCode=" + VarietyCode + "&FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text + "&ReportName=" + RptName, "_blank", "");

    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        txtTransNo.SelectedValue = "-Select-";
        txtSupplierName.SelectedValue = "-Select-";
        txtWareHouseName.SelectedValue = "-Select-";
        txtItemName.SelectedValue = "-Select-";
        txtFromDate.Text = ""; txtToDate.Text = "";
    }
    protected void btnSlip_Click(object sender, EventArgs e)
    {
        string RptName = "Cheese Receipt Slip Report";
        string TransNo;
        string SuppCode;
        string WareHouseCode;

        string VarietyCode;

        if (txtTransNo.SelectedItem.Text != "-Select-")
        {
            TransNo = txtTransNo.SelectedItem.Text;
        }
        else
        {
            TransNo = "";
        }
        if (txtSupplierName.SelectedItem.Text != "-Select-")
        {
            SuppCode = txtSupplierName.SelectedValue;

        }
        else
        {
            SuppCode = "";

        }

        if (txtWareHouseName.SelectedItem.Text != "-Select-")
        {
            WareHouseCode = txtWareHouseName.SelectedValue;
        }
        else
        {
            WareHouseCode = "";
        }

        if (txtItemName.SelectedItem.Text != "-Select-")
        {
            VarietyCode = txtItemName.SelectedValue;
        }
        else
        {
            VarietyCode = "";
        }

        ResponseHelper.Redirect("ReportDisplay.aspx?SuppCode=" + SuppCode + "&TransNo=" + TransNo + "&WareHouseCode=" + WareHouseCode + "&VarietyCode=" + VarietyCode + "&FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text + "&ReportName=" + RptName, "_blank", "");

    }
}
