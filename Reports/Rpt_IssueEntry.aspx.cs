﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;


public partial class Reports_Rpt_IssueEntry : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionEnquiryNo;
    string SessionFinYearCode;
    string SessionFinYearVal;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();
        if (!IsPostBack)
        {
            Page.Title = "ERP Module :: Issue Report";
            HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_RptStock"));
            li.Attributes.Add("class", "droplink active open");

            Variety_Load();
            Lot_No_Load();
            Yar_Lot_No_Load();
            Transaction_No_Load();
        }
    }

    private void Transaction_No_Load()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        txtTransNo.Items.Clear();
        query = "Select * from Issue_Entry_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        txtTransNo.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["Issue_Entry_No"] = "-Select-";
        dr["Issue_Entry_No"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        txtTransNo.DataTextField = "Issue_Entry_No";
        txtTransNo.DataValueField = "Issue_Entry_No";
        txtTransNo.DataBind();
    }

    private void Lot_No_Load()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        txtLotNo.Items.Clear();
        query = "Select * from CottonInwards where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        txtLotNo.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["LotNo"] = "-Select-";
        dr["LotNo"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        txtLotNo.DataTextField = "LotNo";
        txtLotNo.DataValueField = "LotNo";
        txtLotNo.DataBind();
    }
    private void Yar_Lot_No_Load()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlYarnLot.Items.Clear();
        query = "select Distinct YarnLotNo from Issue_Entry_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlYarnLot.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["YarnLotNo"] = "-Select-";
        dr["YarnLotNo"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlYarnLot.DataTextField = "YarnLotNo";
        ddlYarnLot.DataValueField = "YarnLotNo";
        ddlYarnLot.DataBind();
    }

    private void Variety_Load()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        txtVariety.Items.Clear();
        query = "Select * from MstVariety where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        txtVariety.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["VarietyID"] = "-Select-";
        dr["VarietyName"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        txtVariety.DataTextField = "VarietyName";
        txtVariety.DataValueField = "VarietyID";
        txtVariety.DataBind();
    }

    protected void btnReport_Click(object sender, EventArgs e)
    {
        string RptName = "Issue Details Report";
        string TransNo;
        string LotNo;
        string VarietyCode;

        if (txtTransNo.SelectedItem.Text != "-Select-")
        {
            TransNo = txtTransNo.SelectedItem.Text;
        }
        else
        {
            TransNo = "";
        }


        if (txtLotNo.SelectedItem.Text != "-Select-")
        {
            LotNo = txtLotNo.SelectedValue;
        }
        else
        {
            LotNo = "";
        }

        if (txtVariety.SelectedItem.Text != "-Select-")
        {
            VarietyCode = txtVariety.SelectedValue;
        }
        else
        {
            VarietyCode = "";
        }

        ResponseHelper.Redirect("ReportDisplay.aspx?TransNo=" + TransNo + "&LotNo=" + LotNo + "&VarietyCode=" + VarietyCode + "&FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text + "&ReportName=" + RptName, "_blank", "");

    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        txtTransNo.SelectedValue = "-Select-";
        txtLotNo.SelectedValue = "-Select-";
        txtVariety.SelectedValue = "-Select-";
        txtFromDate.Text = ""; txtToDate.Text = "";
    }
    protected void btnDetailAll_Click(object sender, EventArgs e)
    {
        string RptName = "Issue Details ALL Report";
        string TransNo;
        string LotNo;
        string YarnLot;
        string VarietyCode;

        if (txtTransNo.SelectedItem.Text != "-Select-")
        {
            TransNo = txtTransNo.SelectedItem.Text;
        }
        else
        {
            TransNo = "";
        }


        if (txtLotNo.SelectedItem.Text != "-Select-")
        {
            LotNo = txtLotNo.SelectedValue;
        }
        else
        {
            LotNo = "";
        }
        if (ddlYarnLot.SelectedItem.Text != "-Select-")
        {
            YarnLot = ddlYarnLot.SelectedValue;
        }
        else
        {
            YarnLot = "";
        }

        if (txtVariety.SelectedItem.Text != "-Select-")
        {
            VarietyCode = txtVariety.SelectedValue;
        }
        else
        {
            VarietyCode = "";
        }

        ResponseHelper.Redirect("ReportDisplay.aspx?TransNo=" + TransNo + "&LotNo=" + LotNo + "&YarnLotNo=" + YarnLot + "&VarietyCode=" + VarietyCode + "&FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text + "&ReportName=" + RptName, "_blank", "");
    }
}
