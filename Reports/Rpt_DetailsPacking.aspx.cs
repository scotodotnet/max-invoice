﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
public partial class Reports_Rpt_DetailsPacking : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionEnquiryNo;
    string SessionFinYearCode;
    string SessionFinYearVal;
    string SessionUnplanReceiptNo;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();
        if (!IsPostBack)
        {
            Page.Title = "ERP Module ::Winding Details Report";
            HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_RptStock"));
            li.Attributes.Add("class", "droplink active open");

            Load_LotNo();
            Count_No_Load();
            Color();
            Load_MachineName();
            SupervisorName();

        }
    }
    private void Load_LotNo()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlLotNo.Items.Clear();
        query = "Select Distinct LotNo from Packing_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' and FinYearCode='" + SessionFinYearCode + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlLotNo.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["LotNo"] = "-Select-";
        dr["LotNo"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlLotNo.DataTextField = "LotNo";
        ddlLotNo.DataValueField = "LotNo";
        ddlLotNo.DataBind();
    }


    private void SupervisorName()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlSupName.Items.Clear();
        query = "Select *from MstEmployee where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' ";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlSupName.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["EmpName"] = "-Select-";
        dr["EmpCode"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlSupName.DataTextField = "EmpName";
        ddlSupName.DataValueField = "EmpCode";
        ddlSupName.DataBind();
    }
    private void Count_No_Load()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        txtCountID.Items.Clear();
        query = "select CountID,CountName from MstCount where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        txtCountID.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["CountID"] = "-Select-";
        dr["CountName"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        txtCountID.DataTextField = "CountName";
        txtCountID.DataValueField = "CountID";
        txtCountID.DataBind();
    }
    private void Load_MachineName()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        txtMachineName.Items.Clear();
        query = "Select * from MstMachine where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And DeptName='WINDING'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        txtMachineName.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["MachineName"] = "-Select-";
        dr["MachineCode"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        txtMachineName.DataTextField = "MachineName";
        txtMachineName.DataValueField = "MachineCode";
        txtMachineName.DataBind();
    }

    private void Color()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        txtColorName.Items.Clear();
        query = "Select *from MstColor where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        txtColorName.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["ColorName"] = "-Select-";
        dr["ColorName"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        txtColorName.DataTextField = "ColorName";
        txtColorName.DataValueField = "ColorName";
        txtColorName.DataBind();
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        txtCountID.SelectedValue = "-Select-";
        txtColorName.SelectedValue = "-Select-";
        txtMachineName.SelectedValue = "-Select-";

        txtFromDate.Text = ""; txtToDate.Text = "";
    }
    protected void btnReport_Click(object sender, EventArgs e)
    {
        string RptName = "Packing Details Report";
        string TransNo;
        string CountName;
        string MachineName;
        string LotNo;
        string Color;
        string Supervisor;
        string VarietyCode;

        if (txtCountID.SelectedItem.Text != "-Select-")
        {
            CountName = txtCountID.SelectedItem.Text;
        }
        else
        {
            CountName = "";
        }
        if (ddlLotNo.SelectedItem.Text != "-Select-")
        {
            LotNo = ddlLotNo.SelectedValue;

        }
        else
        {
            LotNo = "";

        }

        if (txtColorName.SelectedItem.Text != "-Select-")
        {
            Color = txtColorName.SelectedValue;
        }
        else
        {
            Color = "";



        }
        if (ddlSupName.SelectedItem.Text != "-Select-")
        {
            Supervisor = ddlSupName.SelectedValue;
        }
        else
        {
            Supervisor = "";



        }

        ResponseHelper.Redirect("ReportDisplay.aspx?LotNo=" + LotNo + "&TransNo=" + CountName + "&Supervisor=" + Supervisor + "&Color=" + Color + "&FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text + "&ReportName=" + RptName, "_blank", "");


    }
    protected void btnDetails_Click(object sender, EventArgs e)
    {
        string RptName = "OverAll production Report";
        string TransNo;
        string MachineName;
        string Color;

        string VarietyCode;

        if (txtFromDate.Text != "" && txtToDate.Text != "")
        {
            ResponseHelper.Redirect("ReportDisplay.aspx?FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text + "&ReportName=" + RptName, "_blank", "");

        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the From Date and To Date.');", true);

        }
        //string RptName = "Winding Details Over All Report";
        //string TransNo;
        //string MachineName;
        //string Color;
        //string LotNo;
        //string VarietyCode;
        //string Sup_Name;
        //bool ErrFlag = false;
        //if (txtCountID.SelectedItem.Text != "-Select-")
        //{
        //    TransNo = txtCountID.SelectedItem.Text;
        //}
        //else
        //{
        //    TransNo = "";
        //}
        //if (txtMachineName.SelectedItem.Text != "-Select-")
        //{
        //    MachineName = txtMachineName.SelectedItem.Text;

        //}
        //else
        //{
        //    MachineName = "";

        //}

        //if (txtColorName.SelectedItem.Text != "-Select-")
        //{
        //    Color = txtColorName.SelectedValue;
        //}
        //else
        //{
        //    Color = "";



        //}
        //if (ddlLotNo.SelectedItem.Text != "-Select-")
        //{
        //    LotNo = ddlLotNo.SelectedValue;
        //}
        //else
        //{
        //    LotNo = "";

        //}
        //if (ddlSupName.SelectedItem.Text != "-Select-")
        //{
        //    Sup_Name = ddlSupName.SelectedValue;
        //}
        //else
        //{
        //    Sup_Name = "";
        //}
        //if (txtFromDate.Text == "")
        //{
        //    ErrFlag = true;
        //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the From Date..');", true);
        //}
        //else if (txtToDate.Text == "")
        //{
        //    ErrFlag = true;
        //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the To Date..');", true);
        //}
        //if (!ErrFlag)
        //{
        //    ResponseHelper.Redirect("ReportDisplay.aspx?MachineName=" + MachineName + "&TransNo=" + TransNo + "&Color=" + Color + "&SupervisorName=" + Sup_Name + "&FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text + "&ReportName=" + RptName + "&LotNo=" + LotNo, "_blank", "");
        //}
    }

}
