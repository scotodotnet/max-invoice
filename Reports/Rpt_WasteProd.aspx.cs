﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;


public partial class Reports_Rpt_WasteProd : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionEnquiryNo;
    string SessionFinYearCode;
    string SessionFinYearVal;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();
        if (!IsPostBack)
        {
            Page.Title = "ERP Module :: Waste Report";
            HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_RptStock"));
            li.Attributes.Add("class", "droplink active open");

          
            Transaction_No_Load();
        }
    }

    private void Transaction_No_Load()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        txtTransNo.Items.Clear();
        query = "Select * from Waste_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        txtTransNo.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["Trans_No"] = "-Select-";
        dr["Trans_No"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        txtTransNo.DataTextField = "Trans_No";
        txtTransNo.DataValueField = "Trans_No";
        txtTransNo.DataBind();
    }

    protected void btnReport_Click(object sender, EventArgs e)
    {
        string RptName = "Waste Details Report";
        string TransNo;
        string VarietyCode;

        if (txtTransNo.SelectedItem.Text != "-Select-")
        {
            TransNo = txtTransNo.SelectedItem.Text;
        }
        else
        {
            TransNo = "";
        }

        if (ddlWasteDesc.SelectedItem.Text != "-Select-")
        {
            VarietyCode = ddlWasteDesc.SelectedValue;
        }
        else
        {
            VarietyCode = "";
        }

        ResponseHelper.Redirect("ReportDisplay.aspx?TransNo=" + TransNo + "&VarietyCode=" + VarietyCode + "&FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text + "&ReportName=" + RptName, "_blank", "");

    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        txtTransNo.SelectedValue = "-Select-";
        ddlWasteDesc.SelectedValue = "-Select-";
        txtFromDate.Text = ""; txtToDate.Text = "";
    }
}
