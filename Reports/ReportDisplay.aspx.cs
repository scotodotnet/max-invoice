﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class Reports_ReportDisplay : System.Web.UI.Page
{
    string SSQL = "";
    BALDataAccess objdata = new BALDataAccess();
    ReportDocument RD_Report = new ReportDocument();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionEnquiryNo;
    string SessionFinYearCode;
    string SessionFinYearVal;
    bool Errflag;
    string RptName;
    string FromDate;
    string Sup_Name;
    string ToDate;
    string Supervisor;
    DateTime FDate;
    DateTime TDate;
    Decimal OsBale;
    Decimal OsKg;
    Decimal WeightListBale;
    Decimal WeightListKg;
    Decimal IssueEntryBale;
    Decimal IssueEntryKg;
    Decimal ClosingStockBale;
    Decimal ClosingStockKg;
    DataSet ds = new DataSet();
    DataTable DtdFirst = new DataTable();
    DataTable AutoDataTable = new DataTable();
    DataTable DtdVarietyName = new DataTable();
    string CtnInwardsNo;
    string LotNo; string Rtype; string YarnLot;
    string BaleNo;
    string InvoiceNo;
    string Vcode;
    string SupplierName;
    string TransName;
    string WeightListNo;
    string IssueEntryNo;
    string RejectedEntryNo;
    string TransferNo;
    string FromUnit;
    string ToUnit;
    string Issueweight = "";
    string IssueDate = "";
    string Month;
    string Fyear;
    Decimal Total;
    bool ErrFlag = false;
    DateTime ReceiptDate;
    int Difference;
    DateTime DateDiff = new DateTime();
    string Inwards;
    string Bweight;
    string NoOfDays;
    string DeptName;
    string WareHouseCode;
    string GodownName;
    string ZoneCode;
    string DeptCode;
    string ReportName;
    string Supplier_Code = "";
    string Transaction_No;
    string ButtonName;
    string MachineCode;
    string AgentName;
    string VarietyCode;
    string VarietyName;
    DateTime frmDate;
    DateTime toDate; string TransNo = "";
    System.Web.UI.WebControls.DataGrid grid =
                            new System.Web.UI.WebControls.DataGrid();

    string MachineName;
    string ColorName;


    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }

        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();
        RptName = Request.QueryString["ReportName"].ToString();

        if (RptName.ToString() == "Raw Material Receipt Details Report")
        {
            Transaction_No = Request.QueryString["TransNo"].ToString();
            SupplierName = Request.QueryString["SuppCode"].ToString();
            GodownName = Request.QueryString["GoDownID"].ToString();
            VarietyCode = Request.QueryString["VarietyCode"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();

            RawMaterial_Details_Report();
        }

        else if (RptName.ToString() == "Spares Receipt Details Report" || RptName.ToString() == "Spares Receipt Slip Report")
        {
            Transaction_No = Request.QueryString["TransNo"].ToString();
            SupplierName = Request.QueryString["SuppCode"].ToString();
            DeptCode = Request.QueryString["DeptCode"].ToString();
            VarietyCode = Request.QueryString["VarietyCode"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();

            Spares_Details_Report();
        }
        else if (RptName.ToString() == "Services Receipt Details Report" || RptName=="Services Receipt Slip Report")
        {
            Transaction_No = Request.QueryString["TransNo"].ToString();
            SupplierName = Request.QueryString["SuppCode"].ToString();
            VarietyCode = Request.QueryString["VarietyCode"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();

            Services_Details_Report();
        }
        if (RptName.ToString() == "OverAll production Report")
        {

            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();

            OverAll_Prouction_Report();
        }

        else if (RptName.ToString() == "Cheese Stock Details Report")
        {
            WareHouseCode = Request.QueryString["WareHouseCode"].ToString();
            ZoneCode = Request.QueryString["ZoneCode"].ToString();
            VarietyCode = Request.QueryString["VarietyCode"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();

            CheeseStock_Details_Report();
        }
        else if (RptName.ToString() == "Raw Material Stock Details Report")
        {
            WareHouseCode = Request.QueryString["WareHouseCode"].ToString();
            VarietyCode = Request.QueryString["VarietyCode"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();

            RawMaterialStock_Details_Report();
        }
        else if (RptName.ToString() == "Spares Stock Details Report")
        {
            WareHouseCode = Request.QueryString["WareHouseCode"].ToString();
            ZoneCode = Request.QueryString["ZoneCode"].ToString();
            VarietyCode = Request.QueryString["VarietyCode"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();

            SparesStock_Details_Report();
        }
        else if (RptName.ToString() == "Yarn Stock Details Report")
        {
            WareHouseCode = Request.QueryString["WareHouseCode"].ToString();
            VarietyCode = Request.QueryString["VarietyCode"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();

            YarnStock_Details_Report();
        }
        else if (RptName.ToString() == "Yarn Stock Abstract Report")
        {
            WareHouseCode = Request.QueryString["WareHouseCode"].ToString();
            VarietyCode = Request.QueryString["VarietyCode"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();

            YarnStock_Abstract_Report();
        }
        else if (RptName == "Carding Shiftwise Details Report")
        {
            MachineName = Request.QueryString["MachineName"].ToString();
            ColorName = Request.QueryString["Color"].ToString();
            LotNo = Request.QueryString["LotNo"].ToString();
            Transaction_No = Request.QueryString["TransNo"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();

            Carding_Daily_Production();
        }
        else if (RptName == "Carding Abstract Details Report")
        {
            MachineName = Request.QueryString["MachineName"].ToString();
            ColorName = Request.QueryString["Color"].ToString();
            LotNo = Request.QueryString["LotNo"].ToString();
            Transaction_No = Request.QueryString["TransNo"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();

            Carding_Daily_Production();
        }
        else if (RptName == "Daily Production Report")
        {
            DeptName = Request.QueryString["DeptName"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            if (DeptName == "Carding Production")
            {
                Carding_Daily_Production();
            }
            else if (DeptName == "Drawing Production")
            {
                Drawing_Daily_Production();
            }
            else if (DeptName == "OE Production")
            {
                OE_Daily_Production();
            }
        }
        else if (RptName == "Monthly Production Report")
        {
            DeptName = Request.QueryString["DeptName"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();
            if (DeptName == "Carding Production")
            {
                Monthly_Production_Report();
            }
            else if (DeptName == "Drawing Production")
            {
                Monthly_Production_Report();
            }
            else if (DeptName == "OE Production")
            {
                Monthly_Production_Report();
            }
        }
        else if (RptName.ToString() == "Yarn Production GoDownwise Report")
        {
            TransNo = Request.QueryString["TransNo"].ToString();
            WareHouseCode = Request.QueryString["WareHouseCode"].ToString();
            VarietyCode = Request.QueryString["VarietyCode"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();

            YarnProduction_GoDownwise_Report();
        }
        else if (RptName.ToString() == "Yarn Production Detail Report")
        {
            TransNo = Request.QueryString["TransNo"].ToString();
            WareHouseCode = Request.QueryString["WareHouseCode"].ToString();
            VarietyCode = Request.QueryString["VarietyCode"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();

            YarnProduction_Detail_Report();
        }
        else if (RptName.ToString() == "EB Consumption Details Report" || RptName.ToString() == "EB Consumption Machinewise Report")
        {
            TransNo = Request.QueryString["TransNo"].ToString();
            MachineCode = Request.QueryString["MachineCode"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();
            if (RptName.ToString() == "EB Consumption Details Report")
            {
                EBConsumption_Detail_Report();
            }
            else if (RptName.ToString() == "EB Consumption Machinewise Report")
            {
                EBConsumption_Machinewise_Report();
            }
        }
        else if (RptName.ToString() == "Cheese Receipt Details Report" || RptName == "Cheese Receipt Slip Report")
        {
            Transaction_No = Request.QueryString["TransNo"].ToString();
            SupplierName = Request.QueryString["SuppCode"].ToString();
            WareHouseCode = Request.QueryString["WareHouseCode"].ToString();
            VarietyCode = Request.QueryString["VarietyCode"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();

            Cheese_Receipt_Details_Report();
        }
        else if (RptName.ToString() == "Waste Details Report")
        {
            Transaction_No = Request.QueryString["TransNo"].ToString();
            VarietyCode = Request.QueryString["VarietyCode"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();

            Waste_Details_Report();
        }
        else if (RptName.ToString() == "Yarn Delivery Details Report")
        {
            TransNo = Request.QueryString["TransNo"].ToString();
            WareHouseCode = Request.QueryString["WareHouseCode"].ToString();
            VarietyCode = Request.QueryString["VarietyCode"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();

            YarnDelivery_Detail_Report();
        }
        else if (RptName.ToString() == "Services Delivery Details Report")
        {
            Transaction_No = Request.QueryString["TransNo"].ToString();
            SupplierName = Request.QueryString["SuppCode"].ToString();
            VarietyCode = Request.QueryString["VarietyCode"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();

            ServicesDelivery_Details_Report();
        }
        else if (RptName.ToString() == "Raw Material Current Stock Report")
        {
            WareHouseCode = Request.QueryString["WareHouseCode"].ToString();

            RawMaterialCurrentStock();
        }
        else if (RptName.ToString() == "Spares Current Stock Report")
        {

            VarietyCode = Request.QueryString["VarietyCode"].ToString();
            Spare_Current_Report();
        }
        else if (RptName.ToString() == "Yarn Current Stock Report")
        {

            VarietyCode = Request.QueryString["VarietyCode"].ToString();
            Yarn_Stock_Current();
        }
        else if (RptName.ToString() == "Delivery Rawmaterial Report")
        {
            Transaction_No = Request.QueryString["TransCode"].ToString();
            LotNo = Request.QueryString["LotNo"].ToString();
            VarietyCode = Request.QueryString["VarietyCode"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();

            Delivery_Rawmaterial_Report();
        }
        else if (RptName.ToString() == "Delivery Sapre Report")
        {
            Transaction_No = Request.QueryString["TransCode"].ToString();
            //LotNo = Request.QueryString["LotNo"].ToString();
            VarietyCode = Request.QueryString["VarietyCode"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();

            Delivery_Sapre_Report();
        }
        else if (RptName.ToString() == "Delivery Cheese Report")
        {
            Transaction_No = Request.QueryString["TransCode"].ToString();
            //LotNo = Request.QueryString["LotNo"].ToString();
            VarietyCode = Request.QueryString["VarietyCode"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();

            Delivery_Cheese_Report();
        }
        else if (RptName.ToString() == "Cotton Inward Details Report")
        {
            Transaction_No = Request.QueryString["TransNo"].ToString();
            LotNo = Request.QueryString["LotNo"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();

            Cotton_Inward_Details_Report();
        }
        else if (RptName.ToString() == "Invoice Details Report")
        {
            Transaction_No = Request.QueryString["TransNo"].ToString();
            //Supplier_Code = Request.QueryString["SuppCode"].ToString();
            //FromDate = Request.QueryString["FromDate"].ToString();
            //ToDate = Request.QueryString["ToDate"].ToString();

            Invoice_Report();
        }
        else if (RptName.ToString() == "Carding Details Report")
        {
            MachineName = Request.QueryString["MachineName"].ToString();
            ColorName = Request.QueryString["Color"].ToString();
            Transaction_No = Request.QueryString["TransNo"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();

            Carding_Details_Report();
        }
        else if (RptName.ToString() == "Carding Details Over All Report")
        {
            MachineName = Request.QueryString["MachineName"].ToString();
            ColorName = Request.QueryString["Color"].ToString();
            LotNo = Request.QueryString["LotNo"].ToString();
            Transaction_No = Request.QueryString["TransNo"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();

            Carding_Details_OverAll_Report();
        }
        else if (RptName.ToString() == "Winding Details Over All Report")
        {
            MachineName = Request.QueryString["MachineName"].ToString();
            ColorName = Request.QueryString["Color"].ToString();
            LotNo = Request.QueryString["LotNo"].ToString();
            Transaction_No = Request.QueryString["TransNo"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();
            Sup_Name = Request.QueryString["SupervisorName"].ToString();
            Winding_Details_OverAll_Report();
        }
        else if (RptName.ToString() == "Drawing Details Report")
        {
            MachineName = Request.QueryString["MachineName"].ToString();
            ColorName = Request.QueryString["Color"].ToString();
            Transaction_No = Request.QueryString["TransNo"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();

            Drawing_Details_Report();
        }
        else if (RptName.ToString() == "Drawing Details Over All Report")
        {
            MachineName = Request.QueryString["MachineName"].ToString();
            LotNo = Request.QueryString["LotNo"].ToString();
            ColorName = Request.QueryString["Color"].ToString();
            Transaction_No = Request.QueryString["TransNo"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();

            Drawing_Details_OverAll_Report();
        }
        else if (RptName.ToString() == "OE Over All Details Report")
        {
            MachineName = Request.QueryString["MachineName"].ToString();
            LotNo = Request.QueryString["LotNo"].ToString();
            ColorName = Request.QueryString["Color"].ToString();
            Transaction_No = Request.QueryString["TransNo"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();

            OE_Details_OverAll_Report();
        }

        else if (RptName.ToString() == "OE Details Report")
        {
            MachineName = Request.QueryString["MachineName"].ToString();
            ColorName = Request.QueryString["Color"].ToString();
            Transaction_No = Request.QueryString["TransNo"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();

            OE_Details_Report();
        }
        else if (RptName.ToString() == "Winding Details Report")
        {
            MachineName = Request.QueryString["MachineName"].ToString();
            LotNo = Request.QueryString["LotNo"].ToString();
            Transaction_No = Request.QueryString["TransNo"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();

            Winding_Details_Report();
        }
        else if (RptName.ToString() == "Packing Details Report")
        {
            Supervisor = Request.QueryString["Supervisor"].ToString();
            LotNo = Request.QueryString["LotNo"].ToString();
            ColorName = Request.QueryString["Color"].ToString();
            Transaction_No = Request.QueryString["TransNo"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();

            Packing_Details_Report();
        }
        else if (RptName.ToString() == "Issue Details Report")
        {
            TransNo = Request.QueryString["TransNo"].ToString();
            LotNo = Request.QueryString["LotNo"].ToString();
            VarietyCode = Request.QueryString["VarietyCode"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();

            Issue_Details_Report();
        }
        else if (RptName.ToString() == "Issue Details ALL Report")
        {
            TransNo = Request.QueryString["TransNo"].ToString();
            LotNo = Request.QueryString["LotNo"].ToString();
            YarnLot = Request.QueryString["YarnLotNo"].ToString();
            VarietyCode = Request.QueryString["VarietyCode"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();

            Issue_Details_ALL_Report();
        }
        else if (RptName.ToString() == "Cotton Stock Details Report")
        {
            LotNo = Request.QueryString["LotNo"].ToString();

            Cotton_Stock_Details_Report();
        }
        else if (RptName.ToString() == "ShiftWise Drawing Details Report")
        {
            MachineName = Request.QueryString["MachineName"].ToString();
            ColorName = Request.QueryString["Color"].ToString();
            LotNo = Request.QueryString["LotNo"].ToString();
            Transaction_No = Request.QueryString["TransNo"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();

            ShiftWise_Drawing_Details_Report();
        }
        else if (RptName.ToString() == "Drawing Abstract Details Report")
        {
            MachineName = Request.QueryString["MachineName"].ToString();
            ColorName = Request.QueryString["Color"].ToString();
            LotNo = Request.QueryString["LotNo"].ToString();
            Transaction_No = Request.QueryString["TransNo"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();

            ShiftWise_Drawing_Details_Report();
        }
        else if (RptName.ToString() == "Drawing Abstract Between Dates Details Report")
        {
            MachineName = Request.QueryString["MachineName"].ToString();
            ColorName = Request.QueryString["Color"].ToString();
            LotNo = Request.QueryString["LotNo"].ToString();
            Transaction_No = Request.QueryString["TransNo"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();

            ShiftWise_Drawing_Details_Report();
        }
        else if (RptName.ToString() == "OE Abstract Details Report")
        {
            MachineName = Request.QueryString["MachineName"].ToString();
            ColorName = Request.QueryString["Color"].ToString();
            LotNo = Request.QueryString["LotNo"].ToString();
            Transaction_No = Request.QueryString["TransNo"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();

            ShiftWise_Drawing_Details_Report();
        }
        else if (RptName.ToString() == "OE Abstract Between Dates Details Report")
        {
            MachineName = Request.QueryString["MachineName"].ToString();
            ColorName = Request.QueryString["Color"].ToString();
            LotNo = Request.QueryString["LotNo"].ToString();
            Transaction_No = Request.QueryString["TransNo"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();

            ShiftWise_Drawing_Details_Report();
        }

        else if (RptName.ToString() == "OE Daily Report")
        {
            MachineName = Request.QueryString["MachineName"].ToString();
            ColorName = Request.QueryString["Color"].ToString();
            Transaction_No = Request.QueryString["TransNo"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();

            OE_Daily_Report();
        }

        else if (RptName == "Carding Monthly Details Report")
        {
            MachineName = Request.QueryString["MachineName"].ToString();
            ColorName = Request.QueryString["Color"].ToString();
            LotNo = Request.QueryString["LotNo"].ToString();
            Transaction_No = Request.QueryString["TransNo"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();

            Carding_Daily_Production();
        }

    }
    //OverAll_Prouction_Report
    public void OverAll_Prouction_Report()
    {
        DataTable Da_Check = new DataTable();
        DataTable Da_Draw = new DataTable();
        DataTable Da_OE = new DataTable();
        DataTable Da_Wind = new DataTable();
        DataTable Da_Packing = new DataTable();
        DataTable AutoDTable = new DataTable();
        DataTable dt = new DataTable();

        AutoDTable.Columns.Add("CompanyName");
        AutoDTable.Columns.Add("Address1");
        AutoDTable.Columns.Add("Address2");
        AutoDTable.Columns.Add("Name");
        AutoDTable.Columns.Add("A");
        AutoDTable.Columns.Add("B");
        AutoDTable.Columns.Add("C");
        AutoDTable.Columns.Add("D AND E");
        AutoDTable.Columns.Add("Total");
        if (FromDate != "")
        {
            frmDate = Convert.ToDateTime(FromDate);
        }
        if (ToDate != "")
        {
            toDate = Convert.ToDateTime(ToDate);
        }

        //Carding
        SSQL = "Select '' as Name,isnull([A],0) as [A],";
        SSQL = SSQL + " isnull([B],0) as [B],isnull([C],0) as[C],isnull([D AND E],0) as[D AND E],(isnull([A],0)+isnull([B],0)+isnull([C],0)+isnull([D AND E],0)) as Total ";
        SSQL = SSQL + " from (Select CM.Unit,isnull(sum(CONVERT(Decimal(18,2),CS.ActProduction)),0) as ActProduction ";
        SSQL = SSQL + " from Carding_Main CM inner join Carding_Main_Sub CS on CM.Trans_No = CS.Trans_No  ";
        SSQL = SSQL + " Where CM.Ccode='" + SessionCcode + "' And CM.Lcode='" + SessionLcode + "'";
        SSQL = SSQL + " And CM.FinYearCode='" + SessionFinYearCode + "'";
        SSQL = SSQL + " And CS.Ccode='" + SessionCcode + "' And CS.Lcode='" + SessionLcode + "'";
        SSQL = SSQL + " And CS.FinYearCode='" + SessionFinYearCode + "'";
        if (FromDate != "")
        {
            SSQL = SSQL + " And CONVERT(DATETIME,CM.Trans_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";
            SSQL = SSQL + "And CONVERT(DATETIME,CM.Trans_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
            SSQL = SSQL + " And CONVERT(DATETIME,CS.Trans_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";
            SSQL = SSQL + "And CONVERT(DATETIME,CS.Trans_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
        }
        SSQL = SSQL + " Group by CM.Unit )src pivot( ";
        SSQL = SSQL + " max(ActProduction) for Unit in ([A], [B], [C],[D AND E])) as MaxBookingDays ";
        Da_Check = objdata.RptEmployeeMultipleDetails(SSQL);
        if (Da_Check.Rows.Count != 0)
        {
            AutoDTable.NewRow();
            AutoDTable.Rows.Add();

            AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Name"] = "Carding";
            AutoDTable.Rows[AutoDTable.Rows.Count - 1]["A"] = Da_Check.Rows[0]["A"].ToString();
            AutoDTable.Rows[AutoDTable.Rows.Count - 1]["B"] = Da_Check.Rows[0]["B"].ToString();
            AutoDTable.Rows[AutoDTable.Rows.Count - 1]["C"] = Da_Check.Rows[0]["C"].ToString();
            AutoDTable.Rows[AutoDTable.Rows.Count - 1]["D AND E"] = Da_Check.Rows[0]["D AND E"].ToString();
            AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Total"] = Da_Check.Rows[0]["Total"].ToString();
        }


        //Drawing
        SSQL = "Select '' as Name,isnull([A],0) as [A],";
        SSQL = SSQL + " isnull([B],0) as [B],isnull([C],0) as[C],isnull([D AND E],0) as[D AND E],(isnull([A],0)+isnull([B],0)+isnull([C],0)+isnull([D AND E],0)) as Total ";
        SSQL = SSQL + " from (Select CM.Unit,isnull(sum(CONVERT(Decimal(18,2),CS.ActProduction)),0) as ActProduction ";
        SSQL = SSQL + " from Drawing_Main CM inner join Drawing_Main_Sub CS on CM.Trans_No = CS.Trans_No  ";
        SSQL = SSQL + " Where CM.Ccode='" + SessionCcode + "' And CM.Lcode='" + SessionLcode + "'";
        SSQL = SSQL + " And CM.FinYearCode='" + SessionFinYearCode + "'";
        SSQL = SSQL + " And CS.Ccode='" + SessionCcode + "' And CS.Lcode='" + SessionLcode + "'";
        SSQL = SSQL + " And CS.FinYearCode='" + SessionFinYearCode + "'";
        if (FromDate != "")
        {
            SSQL = SSQL + " And CONVERT(DATETIME,CM.Trans_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";
            SSQL = SSQL + "And CONVERT(DATETIME,CM.Trans_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
            SSQL = SSQL + " And CONVERT(DATETIME,CS.Trans_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";
            SSQL = SSQL + "And CONVERT(DATETIME,CS.Trans_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
        }
        SSQL = SSQL + " Group by CM.Unit )src pivot( ";
        SSQL = SSQL + " max(ActProduction) for Unit in ([A], [B], [C],[D AND E])) as MaxBookingDays ";
        Da_Draw = objdata.RptEmployeeMultipleDetails(SSQL);
        if (Da_Draw.Rows.Count != 0)
        {
            AutoDTable.NewRow();
            AutoDTable.Rows.Add();

            AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Name"] = "Drawing";
            AutoDTable.Rows[AutoDTable.Rows.Count - 1]["A"] = Da_Draw.Rows[0]["A"].ToString();
            AutoDTable.Rows[AutoDTable.Rows.Count - 1]["B"] = Da_Draw.Rows[0]["B"].ToString();
            AutoDTable.Rows[AutoDTable.Rows.Count - 1]["C"] = Da_Draw.Rows[0]["C"].ToString();
            AutoDTable.Rows[AutoDTable.Rows.Count - 1]["D AND E"] = Da_Draw.Rows[0]["D AND E"].ToString();
            AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Total"] = Da_Draw.Rows[0]["Total"].ToString();
        }


        //OE
        SSQL = "Select '' as Name,isnull([A],0) as [A],";
        SSQL = SSQL + " isnull([B],0) as [B],isnull([C],0) as[C],isnull([D AND E],0) as[D AND E],(isnull([A],0)+isnull([B],0)+isnull([C],0)+isnull([D AND E],0)) as Total ";
        SSQL = SSQL + " from (Select CM.Unit,isnull(sum(CONVERT(Decimal(18,2),CS.ActProd)),0) as ActProduction ";
        SSQL = SSQL + " from OE_Main CM inner join OE_Sub CS on CM.Trans_No = CS.Trans_No  ";
        SSQL = SSQL + " Where CM.Ccode='" + SessionCcode + "' And CM.Lcode='" + SessionLcode + "'";
        SSQL = SSQL + " And CM.FinYearCode='" + SessionFinYearCode + "'";
        SSQL = SSQL + " And CS.Ccode='" + SessionCcode + "' And CS.Lcode='" + SessionLcode + "'";
        SSQL = SSQL + " And CS.FinYearCode='" + SessionFinYearCode + "'";
        if (FromDate != "")
        {
            SSQL = SSQL + " And CONVERT(DATETIME,CM.Trans_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";
            SSQL = SSQL + "And CONVERT(DATETIME,CM.Trans_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
            SSQL = SSQL + " And CONVERT(DATETIME,CS.Trans_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";
            SSQL = SSQL + "And CONVERT(DATETIME,CS.Trans_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
        }
        SSQL = SSQL + " Group by CM.Unit )src pivot( ";
        SSQL = SSQL + " max(ActProduction) for Unit in ([A], [B], [C],[D AND E])) as MaxBookingDays ";
        Da_OE = objdata.RptEmployeeMultipleDetails(SSQL);
        if (Da_OE.Rows.Count != 0)
        {
            AutoDTable.NewRow();
            AutoDTable.Rows.Add();

            AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Name"] = "OE";
            AutoDTable.Rows[AutoDTable.Rows.Count - 1]["A"] = Da_OE.Rows[0]["A"].ToString();
            AutoDTable.Rows[AutoDTable.Rows.Count - 1]["B"] = Da_OE.Rows[0]["B"].ToString();
            AutoDTable.Rows[AutoDTable.Rows.Count - 1]["C"] = Da_OE.Rows[0]["C"].ToString();
            AutoDTable.Rows[AutoDTable.Rows.Count - 1]["D AND E"] = Da_OE.Rows[0]["D AND E"].ToString();
            AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Total"] = Da_OE.Rows[0]["Total"].ToString();
        }


        //Winding
        SSQL = "Select '' as Name,isnull([A],0) as [A],";
        SSQL = SSQL + " isnull([B],0) as [B],isnull([C],0) as[C],isnull([D AND E],0) as[D AND E],(isnull([A],0)+isnull([B],0)+isnull([C],0)+isnull([D AND E],0)) as Total ";
        SSQL = SSQL + " from (Select CS.Unit,isnull(sum(CONVERT(Decimal(18,2),CS.Production)),0) as ActProduction ";
        SSQL = SSQL + " from Winding_Main CM inner join Winding_Sub CS on CM.Trans_No = CS.Trans_No  ";
        SSQL = SSQL + " Where CM.Ccode='" + SessionCcode + "' And CM.Lcode='" + SessionLcode + "'";
        SSQL = SSQL + " And CM.FinYearCode='" + SessionFinYearCode + "'";
        SSQL = SSQL + " And CS.Ccode='" + SessionCcode + "' And CS.Lcode='" + SessionLcode + "'";
        SSQL = SSQL + " And CS.FinYearCode='" + SessionFinYearCode + "'";
        if (FromDate != "")
        {
            SSQL = SSQL + " And CONVERT(DATETIME,CM.Trans_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";
            SSQL = SSQL + "And CONVERT(DATETIME,CM.Trans_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
            SSQL = SSQL + " And CONVERT(DATETIME,CS.Trans_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";
            SSQL = SSQL + "And CONVERT(DATETIME,CS.Trans_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
        }
        SSQL = SSQL + " Group by CS.Unit )src pivot( ";
        SSQL = SSQL + " max(ActProduction) for Unit in ([A], [B], [C],[D AND E])) as MaxBookingDays ";
        Da_Wind = objdata.RptEmployeeMultipleDetails(SSQL);
        if (Da_Wind.Rows.Count != 0)
        {
            AutoDTable.NewRow();
            AutoDTable.Rows.Add();

            AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Name"] = "Winding";
            AutoDTable.Rows[AutoDTable.Rows.Count - 1]["A"] = Da_Wind.Rows[0]["A"].ToString();
            AutoDTable.Rows[AutoDTable.Rows.Count - 1]["B"] = Da_Wind.Rows[0]["B"].ToString();
            AutoDTable.Rows[AutoDTable.Rows.Count - 1]["C"] = Da_Wind.Rows[0]["C"].ToString();
            AutoDTable.Rows[AutoDTable.Rows.Count - 1]["D AND E"] = Da_Wind.Rows[0]["D AND E"].ToString();
            AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Total"] = Da_Wind.Rows[0]["Total"].ToString();
        }

        //Packing
        SSQL = "Select '' as Name,isnull([A],0) as [A],";
        SSQL = SSQL + " isnull([B],0) as [B],isnull([C],0) as[C],isnull([D AND E],0) as[D AND E],(isnull([A],0)+isnull([B],0)+isnull([C],0)+isnull([D AND E],0)) as Total ";
        SSQL = SSQL + " from (Select CM.Unit,isnull(sum(CONVERT(Decimal(18,2),CS.NoofBag)),0) as ActProduction ";
        SSQL = SSQL + " from Packing_Main CM inner join Packing_Sub CS on CM.Trans_No = CS.Trans_No  ";
        SSQL = SSQL + " Where CM.Ccode='" + SessionCcode + "' And CM.Lcode='" + SessionLcode + "'";
        SSQL = SSQL + " And CM.FinYearCode='" + SessionFinYearCode + "'";
        SSQL = SSQL + " And CS.Ccode='" + SessionCcode + "' And CS.Lcode='" + SessionLcode + "'";
        SSQL = SSQL + " And CS.FinYearCode='" + SessionFinYearCode + "'";
        if (FromDate != "")
        {
            SSQL = SSQL + " And CONVERT(DATETIME,CM.Trans_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";
            SSQL = SSQL + "And CONVERT(DATETIME,CM.Trans_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
            SSQL = SSQL + " And CONVERT(DATETIME,CS.Trans_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";
            SSQL = SSQL + "And CONVERT(DATETIME,CS.Trans_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
        }
        SSQL = SSQL + " Group by CM.Unit )src pivot( ";
        SSQL = SSQL + " max(ActProduction) for Unit in ([A], [B], [C],[D AND E])) as MaxBookingDays ";
        Da_Packing = objdata.RptEmployeeMultipleDetails(SSQL);
        if (Da_Packing.Rows.Count != 0)
        {
            AutoDTable.NewRow();
            AutoDTable.Rows.Add();

            AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Name"] = "Packing";
            AutoDTable.Rows[AutoDTable.Rows.Count - 1]["A"] = Da_Packing.Rows[0]["A"].ToString();
            AutoDTable.Rows[AutoDTable.Rows.Count - 1]["B"] = Da_Packing.Rows[0]["B"].ToString();
            AutoDTable.Rows[AutoDTable.Rows.Count - 1]["C"] = Da_Packing.Rows[0]["C"].ToString();
            AutoDTable.Rows[AutoDTable.Rows.Count - 1]["D AND E"] = Da_Packing.Rows[0]["D AND E"].ToString();
            AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Total"] = Da_Packing.Rows[0]["Total"].ToString();
        }

        if (AutoDTable.Rows.Count != 0)
        {
            for (int i = 0; i < AutoDTable.Rows.Count; i++)
            {
                DataTable dtdCompanyAddress = new DataTable();
                SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dtdCompanyAddress.Rows.Count != 0)
                {
                    AutoDTable.Rows[i]["CompanyName"] = "" + dtdCompanyAddress.Rows[0]["Cname"].ToString() + " - " + SessionLcode + "";
                    AutoDTable.Rows[i]["Address1"] = "" + dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString() + "";
                    AutoDTable.Rows[i]["Address2"] = "" + dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString() + "";
                }
            }


            ds.Tables.Add(AutoDTable);
            ReportDocument report = new ReportDocument();
            report.Load(Server.MapPath("~/crystal/OverAllProductionDetails.rpt"));

            report.SetDatabaseLogon("username", "password", "sa", "Altius2005");
            report.Database.Tables[0].SetDataSource(ds.Tables[0]);

            if (FromDate != "" && ToDate != "")
            {
                report.DataDefinition.FormulaFields["FromDate"].Text = "'" + FromDate.ToString() + "'";
                report.DataDefinition.FormulaFields["ToDate"].Text = "'" + ToDate.ToString() + "'";
            }

            report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = report;
        }
        else
        {
            Errflag = false;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
        }

    }

    public void Issue_Details_ALL_Report()
    {
        DataTable dt = new DataTable();
        string Query = "select IM.Issue_Entry_No,IM.Issue_Entry_Date,IM.YarnLotNo,SM.LotNo,(SM.BaleWeight) as NoOfBale,(SM.Percents) as BaleNo,(CM.BaleWeight) as TotalWeight,(CM.CandyWeight) as TotalBale,";
        Query = Query + " SM.CountCode,SM.CountName,SM.VarietyCode,SM.VarietyName,SM.Supp_Code,SM.Supp_Name,'' as TotalWeight ";
        Query = Query + " from Issue_Entry_Main IM inner join Issue_Entry_Main_Sub SM on IM.Issue_Entry_No=SM.Issue_Entry_No and IM.Lcode=SM.Lcode ";
        Query = Query + " inner join CottonInwards CM on CM.LotNo=SM.LotNo and CM.Lcode=SM.Lcode ";
        Query = Query + "where IM.Ccode='" + SessionCcode + "' and IM.Lcode='" + SessionLcode + "' and SM.Ccode='" + SessionCcode + "' and SM.Lcode='" + SessionLcode + "'";
        if (LotNo.ToString() != "")
        {
            Query = Query + "and SM.LotNo='" + LotNo + "' ";
        }
        if (YarnLot.ToString() != "")
        {
            Query = Query + "and IM.YarnLotNo='" + YarnLot + "' ";
        }
        Query = Query + "order by SM.LotNo asc";
        dt = objdata.RptEmployeeMultipleDetails(Query);
        if (dt.Rows.Count > 0)
        {
            DataSet ds1 = new DataSet();
            ds1.Tables.Add(dt);
            RD_Report.Load(Server.MapPath("~/crystal/IssueDetailReportALL.rpt"));
            RD_Report.SetDataSource(ds1.Tables[0]);

            DataTable dtdCompanyAddress = new DataTable();
            DataTable Close_Det = new DataTable();

            SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
            dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dtdCompanyAddress.Rows.Count != 0)
            {
                RD_Report.DataDefinition.FormulaFields["CompanyName"].Text = "'" + dtdCompanyAddress.Rows[0]["Cname"].ToString() + "'";
                RD_Report.DataDefinition.FormulaFields["Address1"].Text = "'" + dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString() + "'";
                RD_Report.DataDefinition.FormulaFields["Address2"].Text = "'" + dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString() + "'";
            }


            RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = RD_Report;
            CrystalReportViewer1.RefreshReport();
            CrystalReportViewer1.DataBind();


        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
            lblUploadSuccessfully.Text = "No Records Matched..";
        }
    }

    public void Delivery_Rawmaterial_Report()
    {
        DataTable dt = new DataTable();
        string Query = "select RM.Trans_No,RM.Trans_Date,RM.ToUnit,RM.FromGoDownID,RM.FromGoDownName,RM.ToGoDownID,RM.ToGoDownName, ";
        Query = Query + "RM.Remarks,RM.TotNoOfBale,RM.TotBaleWeight, ";
        Query = Query + "RS.LotNo,RS.VarietyID,RS.VarietyName,RS.NoOfBale,RS.AvlBale,RS.BaleWeight,RS.AvlBaleWt,RS.LineTotal ";
        Query = Query + "from RawMaterialDelivery_Main RM inner join RawMaterialDelivery_Main_Sub RS on RM.Trans_No=RS.Trans_No ";
        Query = Query + "where RM.Ccode='" + SessionCcode + "' and RM.Lcode='" + SessionLcode + "'";
        if (Transaction_No.ToString() != "")
        {
            Query = Query + "and RM.Trans_No='" + Transaction_No + "' ";
        }
        if (LotNo.ToString() != "")
        {
            Query = Query + "and RS.LotNo='" + LotNo + "' ";
        }
        if (VarietyCode.ToString() != "")
        {
            Query = Query + "and RS.VarietyID='" + VarietyCode + "' ";
        }
        if (FromDate != "" && ToDate != "")
        {
            Query = Query + "and CONVERT(datetime,RM.Trans_Date,103)>=CONVERT(datetime,'" + FromDate + "',103) and CONVERT(datetime,RM.Trans_Date,103)<=CONVERT(datetime,'" + ToDate + "',103) ";
        }


        dt = objdata.RptEmployeeMultipleDetails(Query);
        if (dt.Rows.Count > 0)
        {
            DataSet ds1 = new DataSet();
            ds1.Tables.Add(dt);
            RD_Report.Load(Server.MapPath("~/crystal/DeliveryRawmaterial.rpt"));
            RD_Report.SetDataSource(ds1.Tables[0]);

            DataTable dtdCompanyAddress = new DataTable();
            DataTable Close_Det = new DataTable();

            SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
            dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dtdCompanyAddress.Rows.Count != 0)
            {
                RD_Report.DataDefinition.FormulaFields["CompanyName"].Text = "'" + dtdCompanyAddress.Rows[0]["Cname"].ToString() + "'";
                RD_Report.DataDefinition.FormulaFields["Address1"].Text = "'" + dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString() + "'";
                RD_Report.DataDefinition.FormulaFields["Address2"].Text = "'" + dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString() + "'";
            }


            RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = RD_Report;
            CrystalReportViewer1.RefreshReport();
            CrystalReportViewer1.DataBind();
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
            lblUploadSuccessfully.Text = "No Records Matched..";
        }
    }

    public void Delivery_Sapre_Report()
    {
        DataTable dt = new DataTable();
        string Query = "select SM.Trans_No,SS.Trans_Date,SM.ToUnit,SM.Remarks,SM.TotalQty, ";
        Query = Query + "SS.ItemCode,SS.ItemName,SS.Qty,SS.AvlQty,SS.FromWarehouseCode,SS.FromWarehouseName, ";
        Query = Query + "SS.FromZoneCode,SS.FromZoneName,SS.ToWarehouseCode,SS.ToWarehouseName,SS.ToZoneCode,SS.ToZoneName,SS.LineTotal ";
        Query = Query + "from SparesDelivery_Main SM inner join SparesDelivery_Main_Sub SS on SM.Trans_No=SS.Trans_No ";
        Query = Query + "where SM.Ccode='" + SessionCcode + "' and SM.Lcode='" + SessionLcode + "'";
        if (Transaction_No.ToString() != "")
        {
            Query = Query + "and SM.Trans_No='" + Transaction_No + "' ";
        }

        if (VarietyCode.ToString() != "")
        {
            Query = Query + "and SM.ItemCode='" + VarietyCode + "' ";
        }
        if (FromDate != "" && ToDate != "")
        {
            Query = Query + "and CONVERT(datetime,SM.Trans_Date,103)>=CONVERT(datetime,'" + FromDate + "',103) and CONVERT(datetime,SM.Trans_Date,103)<=CONVERT(datetime,'" + ToDate + "',103) ";
        }


        dt = objdata.RptEmployeeMultipleDetails(Query);
        if (dt.Rows.Count > 0)
        {
            DataSet ds1 = new DataSet();
            ds1.Tables.Add(dt);
            RD_Report.Load(Server.MapPath("~/crystal/DeliverySapres.rpt"));
            RD_Report.SetDataSource(ds1.Tables[0]);

            DataTable dtdCompanyAddress = new DataTable();
            DataTable Close_Det = new DataTable();

            SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
            dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dtdCompanyAddress.Rows.Count != 0)
            {
                RD_Report.DataDefinition.FormulaFields["CompanyName"].Text = "'" + dtdCompanyAddress.Rows[0]["Cname"].ToString() + "'";
                RD_Report.DataDefinition.FormulaFields["Address1"].Text = "'" + dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString() + "'";
                RD_Report.DataDefinition.FormulaFields["Address2"].Text = "'" + dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString() + "'";
            }


            RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = RD_Report;
            CrystalReportViewer1.RefreshReport();
            CrystalReportViewer1.DataBind();
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
            lblUploadSuccessfully.Text = "No Records Matched..";
        }
    }

    public void Delivery_Cheese_Report()
    {
        DataTable dt = new DataTable();
        string Query = "select CM.Trans_No,CM.Trans_Date,CM.ToUnit,CM.Remarks,CM.TotalQty, ";
        Query = Query + "CB.CheeseCode,CB.CheeseName,CB.Qty,CB.AvlQty,CB.FromWarehouseCode,CB.FromWarehouseName,";
        Query = Query + "CB.FromZoneCode,CB.FromZoneName,CB.ToWarehouseCode,CB.ToWarehouseName,CB.ToZoneCode,CB.ToZoneName ";
        Query = Query + "from Cheese_Delivery_Main CM inner join Cheese_Delivery_Main_Sub CB on CM.Trans_No=CB.Trans_No ";
        Query = Query + "where CM.Ccode='" + SessionCcode + "' and CM.Lcode='" + SessionLcode + "'";
        if (Transaction_No.ToString() != "")
        {
            Query = Query + "and CM.Trans_No='" + Transaction_No + "' ";
        }

        if (VarietyCode.ToString() != "")
        {
            Query = Query + "and CB.CheeseCode='" + VarietyCode + "' ";
        }
        if (FromDate != "" && ToDate != "")
        {
            Query = Query + "and CONVERT(datetime,CM.Trans_Date,103)>=CONVERT(datetime,'" + FromDate + "',103) and CONVERT(datetime,CM.Trans_Date,103)<=CONVERT(datetime,'" + ToDate + "',103) ";
        }


        dt = objdata.RptEmployeeMultipleDetails(Query);
        if (dt.Rows.Count > 0)
        {
            DataSet ds1 = new DataSet();
            ds1.Tables.Add(dt);
            RD_Report.Load(Server.MapPath("~/crystal/DeliveryCheese.rpt"));
            RD_Report.SetDataSource(ds1.Tables[0]);

            DataTable dtdCompanyAddress = new DataTable();
            DataTable Close_Det = new DataTable();

            SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
            dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dtdCompanyAddress.Rows.Count != 0)
            {
                RD_Report.DataDefinition.FormulaFields["CompanyName"].Text = "'" + dtdCompanyAddress.Rows[0]["Cname"].ToString() + "'";
                RD_Report.DataDefinition.FormulaFields["Address1"].Text = "'" + dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString() + "'";
                RD_Report.DataDefinition.FormulaFields["Address2"].Text = "'" + dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString() + "'";
            }


            RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = RD_Report;
            CrystalReportViewer1.RefreshReport();
            CrystalReportViewer1.DataBind();
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
            lblUploadSuccessfully.Text = "No Records Matched..";
        }
    }



    public void RawMaterialCurrentStock()
    {
        DataTable dt = new DataTable();
        string Query = "select GoDownID,GoDownName,ItemCode,ItemName,LotNo,SUM(Add_Bale) as AddBale, SUM(Minus_Bale) as MinBale,(SUM(Add_Bale) -  SUM(Minus_Bale)) as BalanceBal from Cotton_Stock_Ledger_All where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' ";
        if (WareHouseCode.ToString() != "")
        {
            Query = Query + "and GoDownID='" + WareHouseCode + "' ";
        }

        Query = Query + "group by GoDownID,GoDownName,ItemCode,ItemName,LotNo";
        dt = objdata.RptEmployeeMultipleDetails(Query);
        if (dt.Rows.Count > 0)
        {
            DataSet ds1 = new DataSet();
            ds1.Tables.Add(dt);
            RD_Report.Load(Server.MapPath("~/crystal/RawMaterialCurrentStock.rpt"));
            RD_Report.SetDataSource(ds1.Tables[0]);

            DataTable dtdCompanyAddress = new DataTable();
            DataTable Close_Det = new DataTable();

            SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
            dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dtdCompanyAddress.Rows.Count != 0)
            {
                RD_Report.DataDefinition.FormulaFields["CompanyName"].Text = "'" + dtdCompanyAddress.Rows[0]["Cname"].ToString() + "'";
                RD_Report.DataDefinition.FormulaFields["Address1"].Text = "'" + dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString() + "'";
                RD_Report.DataDefinition.FormulaFields["Address2"].Text = "'" + dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString() + "'";
            }


            RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = RD_Report;
            CrystalReportViewer1.RefreshReport();
            CrystalReportViewer1.DataBind();
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
            lblUploadSuccessfully.Text = "No Records Matched..";
        }

    }

    public void Spare_Current_Report()
    {
        DataTable dt = new DataTable();
        string Query = "select SA.ItemCode,SA.ItemName,MI.PurchaseUOM,(SUM(SA.Add_Qty) - SUM(Minus_Qty)) as Qty, (SUM(SA.Add_Value) - SUM(SA.Minus_Value)) as Value ";
        Query = Query + "from Stores_Stock_Ledger_All SA inner join MstItemMaster MI on SA.ItemCode=MI.ItemCode where SA.Ccode='" + SessionCcode + "' and SA.Lcode='" + SessionLcode + "' ";
        if (VarietyCode.ToString() != "")
        {
            Query = Query + "and SA.ItemCode='" + VarietyCode + "' ";
        }

        Query = Query + "group by SA.ItemCode,SA.ItemName,MI.PurchaseUOM";
        dt = objdata.RptEmployeeMultipleDetails(Query);
        if (dt.Rows.Count > 0)
        {
            DataSet ds1 = new DataSet();
            ds1.Tables.Add(dt);
            RD_Report.Load(Server.MapPath("~/crystal/SparesCurrent.rpt"));
            RD_Report.SetDataSource(ds1.Tables[0]);

            DataTable dtdCompanyAddress = new DataTable();
            DataTable Close_Det = new DataTable();

            SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
            dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dtdCompanyAddress.Rows.Count != 0)
            {
                RD_Report.DataDefinition.FormulaFields["CompanyName"].Text = "'" + dtdCompanyAddress.Rows[0]["Cname"].ToString() + "'";
                RD_Report.DataDefinition.FormulaFields["Address1"].Text = "'" + dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString() + "'";
                RD_Report.DataDefinition.FormulaFields["Address2"].Text = "'" + dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString() + "'";
            }


            RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = RD_Report;
            CrystalReportViewer1.RefreshReport();
            CrystalReportViewer1.DataBind();
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
            lblUploadSuccessfully.Text = "No Records Matched..";
        }
    }

    public void Yarn_Stock_Current()
    {
        DataTable dt = new DataTable();
        string Query = "select GoDownID,GoDownName,CountID,CountName,LotNo,(SUM(Add_Qty) - SUM(Minus_Qty)) as Qty, ";
        Query = Query + "(SUM(Add_Bag) - SUM(Minus_Bag)) as Bags,(SUM(Add_LossKg) - SUM(Minus_LossKg)) as LoseKG from Yarn_Stock_Ledger_All where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
        if (VarietyCode.ToString() != "")
        {
            Query = Query + " and CountID='" + VarietyCode + "' ";
        }

        Query = Query + "group by GoDownID,GoDownName,CountID,CountName,LotNo";
        dt = objdata.RptEmployeeMultipleDetails(Query);
        if (dt.Rows.Count > 0)
        {
            DataSet ds1 = new DataSet();
            ds1.Tables.Add(dt);
            RD_Report.Load(Server.MapPath("~/crystal/YarnCurrent.rpt"));
            RD_Report.SetDataSource(ds1.Tables[0]);

            DataTable dtdCompanyAddress = new DataTable();
            DataTable Close_Det = new DataTable();

            SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
            dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dtdCompanyAddress.Rows.Count != 0)
            {
                RD_Report.DataDefinition.FormulaFields["CompanyName"].Text = "'" + dtdCompanyAddress.Rows[0]["Cname"].ToString() + "'";
                RD_Report.DataDefinition.FormulaFields["Address1"].Text = "'" + dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString() + "'";
                RD_Report.DataDefinition.FormulaFields["Address2"].Text = "'" + dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString() + "'";
            }


            RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = RD_Report;
            CrystalReportViewer1.RefreshReport();
            CrystalReportViewer1.DataBind();
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
            lblUploadSuccessfully.Text = "No Records Matched..";
        }
    }


    public void RawMaterial_Details_Report()
    {
        DataTable Da_Check = new DataTable();
        if (FromDate != "")
        {
            frmDate = Convert.ToDateTime(FromDate);
        }
        if (ToDate != "")
        {
            toDate = Convert.ToDateTime(ToDate);
        }

        SSQL = "select '' as CompanyName,'' as LocationName,'' as CompanyCode,'' as LocationCode,'' as Address1,'' as Address2,";
        SSQL = SSQL + "Trans_No,Trans_Date,SuppCode,SuppName,StationID,StationName,GoDownID,GoDownName,LotNo,";
        SSQL = SSQL + "VarietyID,VarietyName,Trash,Moisture,NoOfBale,CandyWeight,BaleWeight,NetAmount";
        SSQL = SSQL + " from RawMaterial where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        SSQL = SSQL + " And FinYearCode='" + SessionFinYearCode + "'";
        if (Transaction_No != "")
        {
            SSQL = SSQL + " And Trans_No='" + Transaction_No + "'";
        }
        if (SupplierName != "")
        {
            SSQL = SSQL + " And SuppCode='" + SupplierName + "'";
        }
        if (GodownName != "")
        {
            SSQL = SSQL + " And GoDownID='" + GodownName + "'";
        }
        if (VarietyCode != "")
        {
            SSQL = SSQL + " And VarietyID='" + VarietyCode + "'";
        }
        if (FromDate != "" && ToDate != "")
        {
            SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(DATETIME,Trans_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
        }
        Da_Check = objdata.RptEmployeeMultipleDetails(SSQL);

        if (Da_Check.Rows.Count > 0)
        {
            DataTable dtdCompanyAddress = new DataTable();
            SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
            dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);

            

            for (int i = 0; i < Da_Check.Rows.Count; i++)
            {
                Da_Check.Rows[i]["CompanyName"] = dtdCompanyAddress.Rows[0]["Cname"].ToString();
                Da_Check.Rows[i]["LocationName"] = dtdCompanyAddress.Rows[0]["location"].ToString();
                Da_Check.Rows[i]["CompanyCode"] = SessionCcode;
                Da_Check.Rows[i]["LocationCode"] = SessionLcode;
                Da_Check.Rows[i]["Address1"] = dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString();
                Da_Check.Rows[i]["Address2"] = dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString();
            }



            ds.Tables.Add(Da_Check);
            ReportDocument report = new ReportDocument();
            report.Load(Server.MapPath("~/crystal/RawMaterialDetails.rpt"));
            report.SetDatabaseLogon("username", "password", "sa", "Altius2005");
            report.Database.Tables[0].SetDataSource(ds.Tables[0]);

            if (FromDate != "" && ToDate != "")
            {
                report.DataDefinition.FormulaFields["FromDate"].Text = "'" + FromDate.ToString() + "'";
                report.DataDefinition.FormulaFields["ToDate"].Text = "'" + ToDate.ToString() + "'";
            }       

            report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = report;


        }
        else
        {
            Errflag = false;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
        }
        if (!Errflag)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
            lblUploadSuccessfully.Text = "No Records Matched..";
        }
    }

    public void Spares_Details_Report()
    {
        DataTable Da_Check = new DataTable();
        if (FromDate != "")
        {
            frmDate = Convert.ToDateTime(FromDate);
        }
        if (ToDate != "")
        {
            toDate = Convert.ToDateTime(ToDate);
        }

        SSQL = "select '' as CompanyName,'' as LocationName,'' as CompanyCode,'' as LocationCode,'' as Address1,'' as Address2,";
        SSQL = SSQL + "SM.Recp_No,SM.Recp_Date,SM.SuppCode,SuppName,SM.RefNo,SM.RefDate,SM.Description,SM.TotalAmt,SM.AddOrLess,";
        SSQL = SSQL + "SM.NetAmount,SS.ItemCode,SS.ItemName,SS.DeptCode,SS.DeptName,SS.WarehouseCode,SS.WarehouseName,";
        SSQL = SSQL + "SS.ZoneCode,SS.ZoneName,SS.ReceivedQty,SS.PrevRate,SS.ItemRate,SS.ItemTotal,SS.Discount_Per,SS.Discount,";
        SSQL = SSQL + "SS.TaxPer,SS.TaxAmount,SS.BDUTaxPer,SS.BDUTaxAmount,SS.OtherCharge,SS.LineTotal,";
        SSQL = SSQL + "SS.CGSTPer,SS.CGSTAmount,SS.SGSTPer,SS.SGSTAmount,SS.IGSTPer,SS.IGSTAmount,SS.TaxableValue";
        SSQL = SSQL + " from Spares_Receipt_Main SM inner join Spares_Receipt_Main_Sub SS on SM.Recp_No=SS.Recp_No";
        SSQL = SSQL + " Where SM.Ccode='" + SessionCcode + "' And SM.Lcode='" + SessionLcode + "'";
        SSQL = SSQL + " And SM.FinYearCode='" + SessionFinYearCode + "'";
        SSQL = SSQL + " And SS.Ccode='" + SessionCcode + "' And SS.Lcode='" + SessionLcode + "'";
        SSQL = SSQL + " And SS.FinYearCode='" + SessionFinYearCode + "'";
        if (Transaction_No != "")
        {
            SSQL = SSQL + " And SM.Recp_No='" + Transaction_No + "'";
            SSQL = SSQL + " And SS.Recp_No='" + Transaction_No + "'";
        }
        if (SupplierName != "")
        {
            SSQL = SSQL + " And SM.SuppCode='" + SupplierName + "'";
        }
        if (DeptCode != "")
        {
            SSQL = SSQL + " And SS.DeptCode='" + DeptCode + "'";
        }
        if (VarietyCode != "")
        {
            SSQL = SSQL + " And SS.ItemCode='" + VarietyCode + "'";
        }
        if (FromDate != "" && ToDate != "")
        {
            SSQL = SSQL + " And CONVERT(DATETIME,SM.Recp_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(DATETIME,SM.Recp_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
            SSQL = SSQL + " And CONVERT(DATETIME,SS.Recp_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(DATETIME,SS.Recp_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
        }
        Da_Check = objdata.RptEmployeeMultipleDetails(SSQL);

        if (Da_Check.Rows.Count > 0)
        {
            DataTable dtdCompanyAddress = new DataTable();
            SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
            dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);



            for (int i = 0; i < Da_Check.Rows.Count; i++)
            {
                Da_Check.Rows[i]["CompanyName"] = dtdCompanyAddress.Rows[0]["Cname"].ToString();
                Da_Check.Rows[i]["LocationName"] = dtdCompanyAddress.Rows[0]["location"].ToString();
                Da_Check.Rows[i]["CompanyCode"] = SessionCcode;
                Da_Check.Rows[i]["LocationCode"] = SessionLcode;
                Da_Check.Rows[i]["Address1"] = dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString();
                Da_Check.Rows[i]["Address2"] = dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString();
            }

            if (RptName == "Spares Receipt Slip Report")
            {
                if (Da_Check.Rows.Count < 10)
                {
                    int Row_Count = Da_Check.Rows.Count;
                    for (int i = Row_Count; i < 8; i++)
                    {
                        Da_Check.NewRow();
                        Da_Check.Rows.Add();
                        Da_Check.Rows[i]["CompanyName"] = Da_Check.Rows[0]["CompanyName"].ToString();
                        Da_Check.Rows[i]["LocationName"] = Da_Check.Rows[0]["LocationName"].ToString();
                        Da_Check.Rows[i]["CompanyCode"] = Da_Check.Rows[0]["CompanyCode"];
                        Da_Check.Rows[i]["LocationCode"] = Da_Check.Rows[0]["LocationCode"];
                        Da_Check.Rows[i]["Address1"] = Da_Check.Rows[0]["Address1"].ToString();
                        Da_Check.Rows[i]["Address2"] = Da_Check.Rows[0]["Address2"].ToString();
                        Da_Check.Rows[i]["Recp_No"] = Da_Check.Rows[0]["Recp_No"];
                    }
                }
            }

            ds.Tables.Add(Da_Check);
            ReportDocument report = new ReportDocument();
            if (RptName.ToString() == "Spares Receipt Details Report")
            {
                report.Load(Server.MapPath("~/crystal/SparesReceiptDetails.rpt"));
            }
            else if (RptName.ToString() == "Spares Receipt Slip Report")
            {
                report.Load(Server.MapPath("~/crystal/SparesReceiptSlipDetails.rpt"));
            }
            report.SetDatabaseLogon("username", "password", "sa", "Altius2005");
            report.Database.Tables[0].SetDataSource(ds.Tables[0]);

            if (FromDate != "" && ToDate != "")
            {
                report.DataDefinition.FormulaFields["FromDate"].Text = "'" + FromDate.ToString() + "'";
                report.DataDefinition.FormulaFields["ToDate"].Text = "'" + ToDate.ToString() + "'";
            }

            report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = report;


        }
        else
        {
            Errflag = false;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
        }
        if (!Errflag)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
            lblUploadSuccessfully.Text = "No Records Matched..";
        }
    }

    public void Services_Details_Report()
    {
        DataTable Da_Check = new DataTable();
        if (FromDate != "")
        {
            frmDate = Convert.ToDateTime(FromDate);
        }
        if (ToDate != "")
        {
            toDate = Convert.ToDateTime(ToDate);
        }

        SSQL = "select '' as CompanyName,'' as LocationName,'' as CompanyCode,'' as LocationCode,'' as Address1,'' as Address2,";
        SSQL = SSQL + "SM.Trans_No as Recp_No,SM.Trans_Date as Recp_Date,SM.SuppCode,SuppName,SM.RefNo,SM.RefDate,SM.Remarks as Description,SM.TotalAmt,SM.AddOrLess,";
        SSQL = SSQL + "SM.NetAmount,SS.ItemCode,SS.ItemName,SS.WarehouseCode,SS.WarehouseName,";
        SSQL = SSQL + "SS.ZoneCode,SS.ZoneName,SS.Qty as ReceivedQty,SS.ItemRate,SS.ItemTotal";
        SSQL = SSQL + " from Services_Main SM inner join Services_Main_Sub SS on SM.Trans_No=SS.Trans_No";
        SSQL = SSQL + " Where SM.Ccode='" + SessionCcode + "' And SM.Lcode='" + SessionLcode + "'";
        SSQL = SSQL + " And SM.FinYearCode='" + SessionFinYearCode + "'";
        SSQL = SSQL + " And SS.Ccode='" + SessionCcode + "' And SS.Lcode='" + SessionLcode + "'";
        SSQL = SSQL + " And SS.FinYearCode='" + SessionFinYearCode + "'";
        if (Transaction_No != "")
        {
            SSQL = SSQL + " And SM.Trans_No='" + Transaction_No + "'";
            SSQL = SSQL + " And SS.Trans_No='" + Transaction_No + "'";
        }
        if (SupplierName != "")
        {
            SSQL = SSQL + " And SM.SuppCode='" + SupplierName + "'";
        }
      
        if (VarietyCode != "")
        {
            SSQL = SSQL + " And SS.ItemCode='" + VarietyCode + "'";
        }
        if (FromDate != "" && ToDate != "")
        {
            SSQL = SSQL + " And CONVERT(DATETIME,SM.Trans_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(DATETIME,SM.Trans_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
            SSQL = SSQL + " And CONVERT(DATETIME,SS.Trans_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(DATETIME,SS.Trans_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
        }
        Da_Check = objdata.RptEmployeeMultipleDetails(SSQL);

        if (Da_Check.Rows.Count > 0)
        {
            DataTable dtdCompanyAddress = new DataTable();
            SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
            dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);



            for (int i = 0; i < Da_Check.Rows.Count; i++)
            {
                Da_Check.Rows[i]["CompanyName"] = dtdCompanyAddress.Rows[0]["Cname"].ToString();
                Da_Check.Rows[i]["LocationName"] = dtdCompanyAddress.Rows[0]["location"].ToString();
                Da_Check.Rows[i]["CompanyCode"] = SessionCcode;
                Da_Check.Rows[i]["LocationCode"] = SessionLcode;
                Da_Check.Rows[i]["Address1"] = dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString();
                Da_Check.Rows[i]["Address2"] = dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString();
            }

            if (RptName == "Services Receipt Slip Report")
            {
                if (Da_Check.Rows.Count < 10)
                {
                    int Row_Count = Da_Check.Rows.Count;
                    for (int i = Row_Count; i < 8; i++)
                    {
                        Da_Check.NewRow();
                        Da_Check.Rows.Add();
                        Da_Check.Rows[i]["CompanyName"] = Da_Check.Rows[0]["CompanyName"].ToString();
                        Da_Check.Rows[i]["LocationName"] = Da_Check.Rows[0]["LocationName"].ToString();
                        Da_Check.Rows[i]["CompanyCode"] = Da_Check.Rows[0]["CompanyCode"];
                        Da_Check.Rows[i]["LocationCode"] = Da_Check.Rows[0]["LocationCode"];
                        Da_Check.Rows[i]["Address1"] = Da_Check.Rows[0]["Address1"].ToString();
                        Da_Check.Rows[i]["Address2"] = Da_Check.Rows[0]["Address2"].ToString();
                        Da_Check.Rows[i]["Recp_No"] = Da_Check.Rows[0]["Recp_No"];
                    }
                }
            }

            ds.Tables.Add(Da_Check);
            ReportDocument report = new ReportDocument();
            if (RptName == "Services Receipt Details Report")
            {
                report.Load(Server.MapPath("~/crystal/ServicesReceiptDetails.rpt"));
            }
            else if(RptName=="Services Receipt Slip Report")
            {
            report.Load(Server.MapPath("~/crystal/ServicesReceiptSlipDetails.rpt"));
            }
            report.SetDatabaseLogon("username", "password", "sa", "Altius2005");
            report.Database.Tables[0].SetDataSource(ds.Tables[0]);

            if (FromDate != "" && ToDate != "")
            {
                report.DataDefinition.FormulaFields["FromDate"].Text = "'" + FromDate.ToString() + "'";
                report.DataDefinition.FormulaFields["ToDate"].Text = "'" + ToDate.ToString() + "'";
            }

            report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = report;


        }
        else
        {
            Errflag = false;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
        }
        if (!Errflag)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
            lblUploadSuccessfully.Text = "No Records Matched..";
        }
    }

    public void CheeseStock_Details_Report()
    {
        DataTable StockDT = new DataTable();
        DataTable Department_DT = new DataTable();
        DataTable Open_Stck = new DataTable();
        DataTable Purchase_DT = new DataTable();
        DataTable Issue_DT = new DataTable();
        DataTable GenReceiptDT = new DataTable();
        DataTable ItemDT = new DataTable();

        AutoDataTable.Columns.Add("WareHouseName");
        AutoDataTable.Columns.Add("ItemName");
        AutoDataTable.Columns.Add("Opening_Qty");
        AutoDataTable.Columns.Add("Opening_Val");
        AutoDataTable.Columns.Add("Rec_Qty");
        AutoDataTable.Columns.Add("Rec_Val");
        AutoDataTable.Columns.Add("Consum_Qty");
        AutoDataTable.Columns.Add("Consum_Val");


        if (FromDate != "")
        {
            frmDate = Convert.ToDateTime(FromDate);
        }
        if (ToDate != "")
        {
            toDate = Convert.ToDateTime(ToDate);
        }

        string[] finyear = SessionFinYearVal.Split('_');
        string FromDate_Chk = "01/04/" + finyear[0];
        DateTime fromDate_Chk = Convert.ToDateTime(FromDate_Chk);


        string[] FromDate_ChkStr = FromDate.Split('/');


        if (FromDate == FromDate_Chk)
        {
            SSQL = "Delete  Cheese_Cheese_Closing_Stock_Print";
            objdata.RptEmployeeMultipleDetails(SSQL);

            SSQL = "Select Distinct WarehouseName from Cheese_Stock_Ledger_All where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "'";
            Department_DT = objdata.RptEmployeeMultipleDetails(SSQL);
            for (int i = 0; i < Department_DT.Rows.Count; i++)
            {
                SSQL = "Select Distinct CheeseName from Cheese_Stock_Ledger_All where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "'";
                SSQL = SSQL + "And WarehouseName='" + Department_DT.Rows[i]["WarehouseName"].ToString() + "'";

                ItemDT = objdata.RptEmployeeMultipleDetails(SSQL);
                for (int j = 0; j < ItemDT.Rows.Count; j++)
                {
                    SSQL = "Select sum(Add_Qty) as Opening_Qty,sum(Add_Value) as Opening_Val from Cheese_Stock_Ledger_All where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "'";
                    SSQL = SSQL + "And WarehouseName='" + Department_DT.Rows[i]["WarehouseName"].ToString() + "'";
                    SSQL = SSQL + "And CheeseName='" + ItemDT.Rows[j]["CheeseName"].ToString() + "'";
                    SSQL = SSQL + "And Trans_Type='OPENING RECEIPT'";
                    //SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date_Str, 103)=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";

                    Open_Stck = objdata.RptEmployeeMultipleDetails(SSQL);

                    SSQL = "Select sum(Add_Qty) as Rec_Qty,sum(Add_Value) as Rec_Val from Cheese_Stock_Ledger_All where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "'";
                    SSQL = SSQL + "And WarehouseName='" + Department_DT.Rows[i]["WarehouseName"].ToString() + "'";
                    SSQL = SSQL + "And CheeseName='" + ItemDT.Rows[j]["CheeseName"].ToString() + "'";
                    SSQL = SSQL + "And (Trans_Type='CHEESE RECEIPT' or Trans_Type='CHEESE DELIVERY')";
                    SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date_Str, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";
                    SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date_Str, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";

                    Purchase_DT = objdata.RptEmployeeMultipleDetails(SSQL);

                    SSQL = "Select sum(Minus_Qty) as Consum_Qty,sum(Minus_Value) as Consum_Val from Cheese_Stock_Ledger_All where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "'";
                    SSQL = SSQL + "And WarehouseName='" + Department_DT.Rows[i]["WarehouseName"].ToString() + "'";
                    SSQL = SSQL + "And CheeseName='" + ItemDT.Rows[j]["CheeseName"].ToString() + "'";
                    SSQL = SSQL + "And Trans_Type='CHEESE DELIVERY'";
                    SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date_Str, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";
                    SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date_Str, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";

                    Issue_DT = objdata.RptEmployeeMultipleDetails(SSQL);



                    SSQL = "Insert Into Cheese_Closing_Stock_Print(Ccode,Lcode,FinYearCode,FinYearVal,WareHouseName,ItemName,Opening_Qty,Opening_Val,Rec_Qty,Rec_Val,";
                    SSQL = SSQL + "Consum_Qty,Consum_Val,From_Date,To_Date,UserID,UserName) Values('" + SessionCcode + "','" + SessionLcode + "',";
                    SSQL = SSQL + " '" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + Department_DT.Rows[i]["WareHouseName"].ToString() + "','" + ItemDT.Rows[j]["CheeseName"].ToString() + "',";
                    if (Open_Stck.Rows.Count != 0)
                    {
                        if (Open_Stck.Rows[0]["Opening_Qty"].ToString() != "")
                        {
                            SSQL = SSQL + " '" + Open_Stck.Rows[0]["Opening_Qty"] + "',";
                        }
                        else
                        {
                            SSQL = SSQL + " '0.00',";
                        }
                        if (Open_Stck.Rows[0]["Opening_Val"].ToString() != "")
                        {
                            SSQL = SSQL + " '" + Open_Stck.Rows[0]["Opening_Val"] + "',";
                        }
                        else
                        {
                            SSQL = SSQL + " '0.00',";
                        }
                    }
                    else
                    {
                        SSQL = SSQL + " '0.00','0.00',";
                    }

                    if (Purchase_DT.Rows.Count != 0)
                    {
                        if (Purchase_DT.Rows[0]["Rec_Qty"].ToString() != "")
                        {
                            SSQL = SSQL + "'" + Purchase_DT.Rows[0]["Rec_Qty"] + "',";
                        }
                        else
                        {
                            SSQL = SSQL + "'0.00',";
                        }
                        if (Purchase_DT.Rows[0]["Rec_Val"].ToString() != "")
                        {
                            SSQL = SSQL + "'" + Purchase_DT.Rows[0]["Rec_Val"] + "',";
                        }
                        else
                        {
                            SSQL = SSQL + "'0.00',";
                        }
                    }
                    else
                    {
                        SSQL = SSQL + "'0.00','0.00',";
                    }
                    if (Issue_DT.Rows.Count != 0)
                    {
                        if (Issue_DT.Rows[0]["Consum_Qty"].ToString() != "")
                        {
                            SSQL = SSQL + "'" + Issue_DT.Rows[0]["Consum_Qty"] + "',";
                        }
                        else
                        {
                            SSQL = SSQL + "'0.00',";
                        }
                        if (Issue_DT.Rows[0]["Consum_Val"].ToString() != "")
                        {
                            SSQL = SSQL + "'" + Issue_DT.Rows[0]["Consum_Val"] + "',";
                        }
                        else
                        {
                            SSQL = SSQL + "'0.00',";
                        }
                    }
                    else
                    {
                        SSQL = SSQL + "'0.00','0.00',";
                    }


                    SSQL = SSQL + " '" + FromDate + "','" + ToDate + "','" + SessionUserID + "','" + SessionUserName + "')";
                    objdata.RptEmployeeMultipleDetails(SSQL);
                }
            }

            //Get Closing Stock
            SSQL = "Select * from Cheese_Closing_Stock_Print where";
            SSQL = SSQL + " Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
            if (FromDate != "" && ToDate != "")
            {
                SSQL = SSQL + " And CONVERT(DATETIME,From_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";
                SSQL = SSQL + " And CONVERT(DATETIME,To_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
            }
            SSQL = SSQL + " order by WareHouseName";

            GenReceiptDT = objdata.RptEmployeeMultipleDetails(SSQL);
            if (GenReceiptDT.Rows.Count != 0)
            {

                if (RptName == "Cheese Stock Details Report")
                {

                    DataSet ds1 = new DataSet();
                    ds1.Tables.Add(GenReceiptDT);
                    RD_Report.Load(Server.MapPath("~/crystal/CheeseClosingStock.rpt"));
                    RD_Report.SetDataSource(ds1.Tables[0]);
                    //Company Details Add
                    DataTable dtdCompanyAddress = new DataTable();
                    DataTable Close_Det = new DataTable();

                    SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                    dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);
                    if (dtdCompanyAddress.Rows.Count != 0)
                    {
                        RD_Report.DataDefinition.FormulaFields["CompanyName"].Text = "'" + dtdCompanyAddress.Rows[0]["Cname"].ToString() + "'";
                        RD_Report.DataDefinition.FormulaFields["Address1"].Text = "'" + dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString() + "'";
                        RD_Report.DataDefinition.FormulaFields["Address2"].Text = "'" + dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString() + "'";
                    }
                    if (FromDate != "" && ToDate != "")
                    {
                        RD_Report.DataDefinition.FormulaFields["FromDate"].Text = "'" + FromDate.ToString() + "'";
                        RD_Report.DataDefinition.FormulaFields["ToDate"].Text = "'" + ToDate.ToString() + "'";
                    }


                    RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
                    CrystalReportViewer1.ReportSource = RD_Report;
                    CrystalReportViewer1.RefreshReport();
                    CrystalReportViewer1.DataBind();
                }
               
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
                lblUploadSuccessfully.Text = "No Records Matched..";
            }

        }
        else
        {

            SSQL = "Delete  Cheese_Closing_Stock_Print";
            objdata.RptEmployeeMultipleDetails(SSQL);

            SSQL = "Select Distinct WarehouseName from Cheese_Stock_Ledger_All where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "'";
            Department_DT = objdata.RptEmployeeMultipleDetails(SSQL);
            for (int i = 0; i < Department_DT.Rows.Count; i++)
            {
                SSQL = "Select Distinct CheeseName from Cheese_Stock_Ledger_All where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "'";
                SSQL = SSQL + "And WarehouseName='" + Department_DT.Rows[i]["WarehouseName"].ToString() + "'";

                ItemDT = objdata.RptEmployeeMultipleDetails(SSQL);
                for (int j = 0; j < ItemDT.Rows.Count; j++)
                {
                    SSQL = "select (sum(Add_Qty)-sum(Minus_Qty)) as Opening_Qty,(sum(Add_Value)-sum(Minus_Value)) as Opening_Val from Cheese_Stock_Ledger_All where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "'";
                    SSQL = SSQL + "And WarehouseName='" + Department_DT.Rows[i]["WarehouseName"].ToString() + "'";
                    SSQL = SSQL + "And CheeseName='" + ItemDT.Rows[j]["CheeseName"].ToString() + "'";
                    if (FromDate != "" && ToDate != "")
                    {
                        SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date_Str, 103)<=CONVERT(DATETIME,'" + frmDate.AddDays(-1).ToString("dd/MM/yyyy") + "',103)";
                    }
                    else
                    {
                        SSQL = SSQL + "And Trans_Type='OPENING RECEIPT'";
                    }
                    Open_Stck = objdata.RptEmployeeMultipleDetails(SSQL);


                    SSQL = "Select sum(Add_Qty) as Rec_Qty,sum(Add_Value) as Rec_Val from Cheese_Stock_Ledger_All where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "'";
                    SSQL = SSQL + "And WarehouseName='" + Department_DT.Rows[i]["WarehouseName"].ToString() + "'";
                    SSQL = SSQL + "And CheeseName='" + ItemDT.Rows[j]["CheeseName"].ToString() + "'";
                    SSQL = SSQL + "And (Trans_Type='CHEESE RECEIPT' or Trans_Type='CHEESE DELIVERY')";
                    if (FromDate != "" && ToDate != "")
                    {
                        SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date_Str, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";
                        SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date_Str, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
                    }
                    Purchase_DT = objdata.RptEmployeeMultipleDetails(SSQL);

                    SSQL = "Select sum(Minus_Qty) as Consum_Qty,sum(Minus_Value) as Consum_Val from Cheese_Stock_Ledger_All where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "'";
                    SSQL = SSQL + "And WarehouseName='" + Department_DT.Rows[i]["WarehouseName"].ToString() + "'";
                    SSQL = SSQL + "And CheeseName='" + ItemDT.Rows[j]["CheeseName"].ToString() + "'";
                    SSQL = SSQL + "And Trans_Type='CHEESE DELIVERY'";
                    if (FromDate != "" && ToDate != "")
                    {
                        SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date_Str, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";
                        SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date_Str, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
                    }
                    Issue_DT = objdata.RptEmployeeMultipleDetails(SSQL);



                    SSQL = "Insert Into Cheese_Closing_Stock_Print(Ccode,Lcode,FinYearCode,FinYearVal,WareHouseName,ItemName,Opening_Qty,Opening_Val,Rec_Qty,Rec_Val,";
                    SSQL = SSQL + "Consum_Qty,Consum_Val,From_Date,To_Date,UserID,UserName) Values('" + SessionCcode + "','" + SessionLcode + "',";
                    SSQL = SSQL + " '" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + Department_DT.Rows[i]["WareHouseName"].ToString() + "','" + ItemDT.Rows[j]["CheeseName"].ToString() + "',";
                    if (Open_Stck.Rows.Count != 0)
                    {
                        if (Open_Stck.Rows[0]["Opening_Qty"].ToString() != "")
                        {
                            SSQL = SSQL + " '" + Open_Stck.Rows[0]["Opening_Qty"] + "',";
                        }
                        else
                        {
                            SSQL = SSQL + " '0.00',";
                        }
                        if (Open_Stck.Rows[0]["Opening_Val"].ToString() != "")
                        {
                            SSQL = SSQL + " '" + Open_Stck.Rows[0]["Opening_Val"] + "',";
                        }
                        else
                        {
                            SSQL = SSQL + " '0.00',";
                        }
                    }
                    else
                    {
                        SSQL = SSQL + " '0.00','0.00',";
                    }

                    if (Purchase_DT.Rows.Count != 0)
                    {
                        if (Purchase_DT.Rows[0]["Rec_Qty"].ToString() != "")
                        {
                            SSQL = SSQL + "'" + Purchase_DT.Rows[0]["Rec_Qty"] + "',";
                        }
                        else
                        {
                            SSQL = SSQL + "'0.00',";
                        }
                        if (Purchase_DT.Rows[0]["Rec_Val"].ToString() != "")
                        {
                            SSQL = SSQL + "'" + Purchase_DT.Rows[0]["Rec_Val"] + "',";
                        }
                        else
                        {
                            SSQL = SSQL + "'0.00',";
                        }
                    }
                    else
                    {
                        SSQL = SSQL + "'0.00','0.00',";
                    }
                    if (Issue_DT.Rows.Count != 0)
                    {
                        if (Issue_DT.Rows[0]["Consum_Qty"].ToString() != "")
                        {
                            SSQL = SSQL + "'" + Issue_DT.Rows[0]["Consum_Qty"] + "',";
                        }
                        else
                        {
                            SSQL = SSQL + "'0.00',";
                        }
                        if (Issue_DT.Rows[0]["Consum_Val"].ToString() != "")
                        {
                            SSQL = SSQL + "'" + Issue_DT.Rows[0]["Consum_Val"] + "',";
                        }
                        else
                        {
                            SSQL = SSQL + "'0.00',";
                        }
                    }
                    else
                    {
                        SSQL = SSQL + "'0.00','0.00',";
                    }


                    SSQL = SSQL + " '" + FromDate + "','" + ToDate + "','" + SessionUserID + "','" + SessionUserName + "')";
                    objdata.RptEmployeeMultipleDetails(SSQL);

                }
            }



            //Get Closing Stock
            SSQL = "Select * from Cheese_Closing_Stock_Print where";
            SSQL = SSQL + " Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
            if (FromDate != "" && ToDate != "")
            {
                SSQL = SSQL + " And CONVERT(DATETIME,From_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";
                SSQL = SSQL + " And CONVERT(DATETIME,To_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
            }
            SSQL = SSQL + " order by WareHouseName";

            GenReceiptDT = objdata.RptEmployeeMultipleDetails(SSQL);
            if (GenReceiptDT.Rows.Count != 0)
            {

                if (RptName == "Cheese Stock Details Report")
                {

                    DataSet ds1 = new DataSet();
                    ds1.Tables.Add(GenReceiptDT);
                    RD_Report.Load(Server.MapPath("~/crystal/CheeseClosingStock.rpt"));
                    RD_Report.SetDataSource(ds1.Tables[0]);
                    //Company Details Add
                    DataTable dtdCompanyAddress = new DataTable();
                    DataTable Close_Det = new DataTable();

                    SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                    dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);
                    if (dtdCompanyAddress.Rows.Count != 0)
                    {
                        RD_Report.DataDefinition.FormulaFields["CompanyName"].Text = "'" + dtdCompanyAddress.Rows[0]["Cname"].ToString() + "'";
                        RD_Report.DataDefinition.FormulaFields["Address1"].Text = "'" + dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString() + "'";
                        RD_Report.DataDefinition.FormulaFields["Address2"].Text = "'" + dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString() + "'";
                    }
                    if (FromDate != "" && ToDate != "")
                    {
                        RD_Report.DataDefinition.FormulaFields["FromDate"].Text = "'" + FromDate.ToString() + "'";
                        RD_Report.DataDefinition.FormulaFields["ToDate"].Text = "'" + ToDate.ToString() + "'";
                    }

                    RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
                    CrystalReportViewer1.ReportSource = RD_Report;
                    CrystalReportViewer1.RefreshReport();
                    CrystalReportViewer1.DataBind();
                }

            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
                lblUploadSuccessfully.Text = "No Records Matched..";
            }


        }

    }

    public void RawMaterialStock_Details_Report()
    {
        DataTable StockDT = new DataTable();
        DataTable Department_DT = new DataTable();
        DataTable Open_Stck = new DataTable();
        DataTable Purchase_DT = new DataTable();
        DataTable Issue_DT = new DataTable();
        DataTable GenReceiptDT = new DataTable();
        DataTable ItemDT = new DataTable();

        AutoDataTable.Columns.Add("GoDownName");
        AutoDataTable.Columns.Add("ItemName");
        AutoDataTable.Columns.Add("Opening_Qty");
        AutoDataTable.Columns.Add("Opening_Val");
        AutoDataTable.Columns.Add("Opening_Bale");
        AutoDataTable.Columns.Add("Rec_Qty");
        AutoDataTable.Columns.Add("Rec_Val");
        AutoDataTable.Columns.Add("Rec_Bale");
        AutoDataTable.Columns.Add("Consum_Qty");
        AutoDataTable.Columns.Add("Consum_Val");
        AutoDataTable.Columns.Add("Consum_Bale");
        

        if (FromDate != "")
        {
            frmDate = Convert.ToDateTime(FromDate);
        }
        if (ToDate != "")
        {
            toDate = Convert.ToDateTime(ToDate);
        }

        string[] finyear = SessionFinYearVal.Split('_');
        string FromDate_Chk = "01/04/" + finyear[0];
        DateTime fromDate_Chk = Convert.ToDateTime(FromDate_Chk);


        string[] FromDate_ChkStr = FromDate.Split('/');


        if (FromDate == FromDate_Chk)
        {
            SSQL = "Delete  RawMaterial_Closing_Stock_Print";
            objdata.RptEmployeeMultipleDetails(SSQL);

            SSQL = "Select Distinct GoDownName from Cotton_Stock_Ledger_All where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "'";
            Department_DT = objdata.RptEmployeeMultipleDetails(SSQL);
            for (int i = 0; i < Department_DT.Rows.Count; i++)
            {
                SSQL = "Select Distinct ItemName from Cotton_Stock_Ledger_All where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "'";
                SSQL = SSQL + "And GoDownName='" + Department_DT.Rows[i]["GoDownName"].ToString() + "'";

                ItemDT = objdata.RptEmployeeMultipleDetails(SSQL);
                for (int j = 0; j < ItemDT.Rows.Count; j++)
                {
                    SSQL = "Select sum(Add_Qty) as Opening_Qty,sum(Add_Value) as Opening_Val,sum(Add_Bale) as Opening_Bale from Cotton_Stock_Ledger_All where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "'";
                    SSQL = SSQL + "And GoDownName='" + Department_DT.Rows[i]["GoDownName"].ToString() + "'";
                    SSQL = SSQL + "And ItemName='" + ItemDT.Rows[j]["ItemName"].ToString() + "'";
                    SSQL = SSQL + "And Trans_Type='OPENING RECEIPT'";
                    //SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date_Str, 103)=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";

                    Open_Stck = objdata.RptEmployeeMultipleDetails(SSQL);

                    SSQL = "Select sum(Add_Qty) as Rec_Qty,sum(Add_Value) as Rec_Val,sum(Add_Bale) as Rec_Bale from Cotton_Stock_Ledger_All where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "'";
                    SSQL = SSQL + "And GoDownName='" + Department_DT.Rows[i]["GoDownName"].ToString() + "'";
                    SSQL = SSQL + "And ItemName='" + ItemDT.Rows[j]["ItemName"].ToString() + "'";
                    SSQL = SSQL + "And (Trans_Type='RAW MATERIAL RECEIPT' or Trans_Type='RAW MATERIAL DELIVERY')";
                    SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date_Str, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";
                    SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date_Str, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";

                    Purchase_DT = objdata.RptEmployeeMultipleDetails(SSQL);

                    SSQL = "Select sum(Minus_Qty) as Consum_Qty,sum(Minus_Value) as Consum_Val,sum(Minus_Bale) as Consum_Bale from Cotton_Stock_Ledger_All where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "'";
                    SSQL = SSQL + "And GoDownName='" + Department_DT.Rows[i]["GoDownName"].ToString() + "'";
                    SSQL = SSQL + "And ItemName='" + ItemDT.Rows[j]["ItemName"].ToString() + "'";
                    SSQL = SSQL + "And Trans_Type='RAW MATERIAL DELIVERY'";
                    SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date_Str, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";
                    SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date_Str, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";

                    Issue_DT = objdata.RptEmployeeMultipleDetails(SSQL);

                    SSQL = "Insert Into RawMaterial_Closing_Stock_Print(Ccode,Lcode,FinYearCode,FinYearVal,GoDownName,ItemName,Opening_Qty,Opening_Val,Opening_Bale,Rec_Qty,Rec_Val,Rec_Bale,";
                    SSQL = SSQL + "Consum_Qty,Consum_Val,Consum_Bale,From_Date,To_Date,UserID,UserName) Values('" + SessionCcode + "','" + SessionLcode + "',";
                    SSQL = SSQL + " '" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + Department_DT.Rows[i]["GoDownName"].ToString() + "','" + ItemDT.Rows[j]["ItemName"].ToString() + "',";
                    if (Open_Stck.Rows.Count != 0)
                    {
                        if (Open_Stck.Rows[0]["Opening_Qty"].ToString() != "")
                        {
                            SSQL = SSQL + " '" + Open_Stck.Rows[0]["Opening_Qty"] + "',";
                        }
                        else
                        {
                            SSQL = SSQL + " '0.00',";
                        }
                        if (Open_Stck.Rows[0]["Opening_Val"].ToString() != "")
                        {
                            SSQL = SSQL + " '" + Open_Stck.Rows[0]["Opening_Val"] + "',";
                        }
                        else
                        {
                            SSQL = SSQL + " '0.00',";
                        }
                        if (Open_Stck.Rows[0]["Opening_Bale"].ToString() != "")
                        {
                            SSQL = SSQL + " '" + Open_Stck.Rows[0]["Opening_Bale"] + "',";
                        }
                        else
                        {
                            SSQL = SSQL + " '0.00',";
                        }
                    }
                    else
                    {
                        SSQL = SSQL + " '0.00','0.00','0.00',";
                    }

                    if (Purchase_DT.Rows.Count != 0)
                    {
                        if (Purchase_DT.Rows[0]["Rec_Qty"].ToString() != "")
                        {
                            SSQL = SSQL + "'" + Purchase_DT.Rows[0]["Rec_Qty"] + "',";
                        }
                        else
                        {
                            SSQL = SSQL + "'0.00',";
                        }
                        if (Purchase_DT.Rows[0]["Rec_Val"].ToString() != "")
                        {
                            SSQL = SSQL + "'" + Purchase_DT.Rows[0]["Rec_Val"] + "',";
                        }
                        else
                        {
                            SSQL = SSQL + "'0.00',";
                        }
                        if (Purchase_DT.Rows[0]["Rec_Bale"].ToString() != "")
                        {
                            SSQL = SSQL + "'" + Purchase_DT.Rows[0]["Rec_Bale"] + "',";
                        }
                        else
                        {
                            SSQL = SSQL + "'0.00',";
                        }
                    }
                    else
                    {
                        SSQL = SSQL + "'0.00','0.00','0.00',";
                    }
                    if (Issue_DT.Rows.Count != 0)
                    {
                        if (Issue_DT.Rows[0]["Consum_Qty"].ToString() != "")
                        {
                            SSQL = SSQL + "'" + Issue_DT.Rows[0]["Consum_Qty"] + "',";
                        }
                        else
                        {
                            SSQL = SSQL + "'0.00',";
                        }
                        if (Issue_DT.Rows[0]["Consum_Val"].ToString() != "")
                        {
                            SSQL = SSQL + "'" + Issue_DT.Rows[0]["Consum_Val"] + "',";
                        }
                        else
                        {
                            SSQL = SSQL + "'0.00',";
                        }
                        if (Issue_DT.Rows[0]["Consum_Bale"].ToString() != "")
                        {
                            SSQL = SSQL + "'" + Issue_DT.Rows[0]["Consum_Bale"] + "',";
                        }
                        else
                        {
                            SSQL = SSQL + "'0.00',";
                        }
                    }
                    else
                    {
                        SSQL = SSQL + "'0.00','0.00','0.00',";
                    }


                    SSQL = SSQL + " '" + FromDate + "','" + ToDate + "','" + SessionUserID + "','" + SessionUserName + "')";
                    objdata.RptEmployeeMultipleDetails(SSQL);
                }
            }

            //Get Closing Stock
            SSQL = "Select * from RawMaterial_Closing_Stock_Print where";
            SSQL = SSQL + " Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
            if (FromDate != "" && ToDate != "")
            {
                SSQL = SSQL + " And CONVERT(DATETIME,From_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";
                SSQL = SSQL + " And CONVERT(DATETIME,To_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
            }
            SSQL = SSQL + " order by GoDownName";

            GenReceiptDT = objdata.RptEmployeeMultipleDetails(SSQL);
            if (GenReceiptDT.Rows.Count != 0)
            {

                if (RptName == "Raw Material Stock Details Report")
                {

                    DataSet ds1 = new DataSet();
                    ds1.Tables.Add(GenReceiptDT);
                    RD_Report.Load(Server.MapPath("~/crystal/RawMaterialClosingStock.rpt"));
                    RD_Report.SetDataSource(ds1.Tables[0]);
                    //Company Details Add
                    DataTable dtdCompanyAddress = new DataTable();
                    DataTable Close_Det = new DataTable();

                    SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                    dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);
                    if (dtdCompanyAddress.Rows.Count != 0)
                    {
                        RD_Report.DataDefinition.FormulaFields["CompanyName"].Text = "'" + dtdCompanyAddress.Rows[0]["Cname"].ToString() + "'";
                        RD_Report.DataDefinition.FormulaFields["Address1"].Text = "'" + dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString() + "'";
                        RD_Report.DataDefinition.FormulaFields["Address2"].Text = "'" + dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString() + "'";
                    }
                    if (FromDate != "" && ToDate != "")
                    {
                        RD_Report.DataDefinition.FormulaFields["FromDate"].Text = "'" + FromDate.ToString() + "'";
                        RD_Report.DataDefinition.FormulaFields["ToDate"].Text = "'" + ToDate.ToString() + "'";
                    }


                    RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
                    CrystalReportViewer1.ReportSource = RD_Report;
                    CrystalReportViewer1.RefreshReport();
                    CrystalReportViewer1.DataBind();
                }

            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
                lblUploadSuccessfully.Text = "No Records Matched..";
            }

        }
        else
        {

            SSQL = "Delete  RawMaterial_Closing_Stock_Print";
            objdata.RptEmployeeMultipleDetails(SSQL);

            SSQL = "Select Distinct GoDownName from Cotton_Stock_Ledger_All where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "'";
            Department_DT = objdata.RptEmployeeMultipleDetails(SSQL);
            for (int i = 0; i < Department_DT.Rows.Count; i++)
            {
                SSQL = "Select Distinct ItemName from Cotton_Stock_Ledger_All where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "'";
                SSQL = SSQL + "And GoDownName='" + Department_DT.Rows[i]["GoDownName"].ToString() + "'";

                ItemDT = objdata.RptEmployeeMultipleDetails(SSQL);
                for (int j = 0; j < ItemDT.Rows.Count; j++)
                {
                    SSQL = "select (sum(Add_Qty)-sum(Minus_Qty)) as Opening_Qty,(sum(Add_Value)-sum(Minus_Value)) as Opening_Val,(sum(Add_Bale)-sum(Minus_Bale)) as Opening_Bale from Cotton_Stock_Ledger_All where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "'";
                    SSQL = SSQL + " And GoDownName='" + Department_DT.Rows[i]["GoDownName"].ToString() + "'";
                    SSQL = SSQL + " And ItemName='" + ItemDT.Rows[j]["ItemName"].ToString() + "'";
                    if (FromDate != "" && ToDate != "")
                    {
                        SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date_Str, 103)<=CONVERT(DATETIME,'" + frmDate.AddDays(-1).ToString("dd/MM/yyyy") + "',103)";
                    }
                    else
                    {
                        SSQL = SSQL + " And Trans_Type='OPENING RECEIPT'";
                    }
                    Open_Stck = objdata.RptEmployeeMultipleDetails(SSQL);


                    SSQL = "Select sum(Add_Qty) as Rec_Qty,sum(Add_Value) as Rec_Val,sum(Add_Bale) as Rec_Bale from Cotton_Stock_Ledger_All where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "'";
                    SSQL = SSQL + "And GoDownName='" + Department_DT.Rows[i]["GoDownName"].ToString() + "'";
                    SSQL = SSQL + "And ItemName='" + ItemDT.Rows[j]["ItemName"].ToString() + "'";
                    SSQL = SSQL + "And (Trans_Type='RAW MATERIAL RECEIPT' or Trans_Type='RAW MATERIAL DELIVERY')";
                    if (FromDate != "" && ToDate != "")
                    {
                        SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date_Str, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";
                        SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date_Str, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
                    }
                    Purchase_DT = objdata.RptEmployeeMultipleDetails(SSQL);

                    SSQL = "Select sum(Minus_Qty) as Consum_Qty,sum(Minus_Value) as Consum_Val,sum(Minus_Bale) as Consum_Bale from Cotton_Stock_Ledger_All where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "'";
                    SSQL = SSQL + "And GoDownName='" + Department_DT.Rows[i]["GoDownName"].ToString() + "'";
                    SSQL = SSQL + "And ItemName='" + ItemDT.Rows[j]["ItemName"].ToString() + "'";
                    SSQL = SSQL + "And Trans_Type='RAW MATERIAL DELIVERY'";
                    if (FromDate != "" && ToDate != "")
                    {
                        SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date_Str, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";
                        SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date_Str, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
                    }
                    Issue_DT = objdata.RptEmployeeMultipleDetails(SSQL);



                    SSQL = "Insert Into RawMaterial_Closing_Stock_Print(Ccode,Lcode,FinYearCode,FinYearVal,GoDownName,ItemName,Opening_Qty,Opening_Val,Opening_Bale,Rec_Qty,Rec_Val,Rec_Bale,";
                    SSQL = SSQL + "Consum_Qty,Consum_Val,Consum_Bale,From_Date,To_Date,UserID,UserName) Values('" + SessionCcode + "','" + SessionLcode + "',";
                    SSQL = SSQL + " '" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + Department_DT.Rows[i]["GoDownName"].ToString() + "','" + ItemDT.Rows[j]["ItemName"].ToString() + "',";
                    if (Open_Stck.Rows.Count != 0)
                    {
                        if (Open_Stck.Rows[0]["Opening_Qty"].ToString() != "")
                        {
                            SSQL = SSQL + " '" + Open_Stck.Rows[0]["Opening_Qty"] + "',";
                        }
                        else
                        {
                            SSQL = SSQL + " '0.00',";
                        }
                        if (Open_Stck.Rows[0]["Opening_Val"].ToString() != "")
                        {
                            SSQL = SSQL + " '" + Open_Stck.Rows[0]["Opening_Val"] + "',";
                        }
                        else
                        {
                            SSQL = SSQL + " '0.00',";
                        }
                        if (Open_Stck.Rows[0]["Opening_Bale"].ToString() != "")
                        {
                            SSQL = SSQL + " '" + Open_Stck.Rows[0]["Opening_Bale"] + "',";
                        }
                        else
                        {
                            SSQL = SSQL + " '0.00',";
                        }
                    }
                    else
                    {
                        SSQL = SSQL + " '0.00','0.00','0.00',";
                    }

                    if (Purchase_DT.Rows.Count != 0)
                    {
                        if (Purchase_DT.Rows[0]["Rec_Qty"].ToString() != "")
                        {
                            SSQL = SSQL + "'" + Purchase_DT.Rows[0]["Rec_Qty"] + "',";
                        }
                        else
                        {
                            SSQL = SSQL + "'0.00',";
                        }
                        if (Purchase_DT.Rows[0]["Rec_Val"].ToString() != "")
                        {
                            SSQL = SSQL + "'" + Purchase_DT.Rows[0]["Rec_Val"] + "',";
                        }
                        else
                        {
                            SSQL = SSQL + "'0.00',";
                        }
                        if (Purchase_DT.Rows[0]["Rec_Bale"].ToString() != "")
                        {
                            SSQL = SSQL + "'" + Purchase_DT.Rows[0]["Rec_Bale"] + "',";
                        }
                        else
                        {
                            SSQL = SSQL + "'0.00',";
                        }
                    }
                    else
                    {
                        SSQL = SSQL + "'0.00','0.00','0.00',";
                    }
                    if (Issue_DT.Rows.Count != 0)
                    {
                        if (Issue_DT.Rows[0]["Consum_Qty"].ToString() != "")
                        {
                            SSQL = SSQL + "'" + Issue_DT.Rows[0]["Consum_Qty"] + "',";
                        }
                        else
                        {
                            SSQL = SSQL + "'0.00',";
                        }
                        if (Issue_DT.Rows[0]["Consum_Val"].ToString() != "")
                        {
                            SSQL = SSQL + "'" + Issue_DT.Rows[0]["Consum_Val"] + "',";
                        }
                        else
                        {
                            SSQL = SSQL + "'0.00',";
                        }
                        if (Issue_DT.Rows[0]["Consum_Bale"].ToString() != "")
                        {
                            SSQL = SSQL + "'" + Issue_DT.Rows[0]["Consum_Bale"] + "',";
                        }
                        else
                        {
                            SSQL = SSQL + "'0.00',";
                        }
                    }
                    else
                    {
                        SSQL = SSQL + "'0.00','0.00','0.00',";
                    }


                    SSQL = SSQL + " '" + FromDate + "','" + ToDate + "','" + SessionUserID + "','" + SessionUserName + "')";
                    objdata.RptEmployeeMultipleDetails(SSQL);

                }
            }



            //Get Closing Stock
            SSQL = "Select * from RawMaterial_Closing_Stock_Print where";
            SSQL = SSQL + " Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
            if (FromDate != "" && ToDate != "")
            {
                SSQL = SSQL + " And CONVERT(DATETIME,From_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";
                SSQL = SSQL + " And CONVERT(DATETIME,To_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
            }
            SSQL = SSQL + " order by GoDownName";

            GenReceiptDT = objdata.RptEmployeeMultipleDetails(SSQL);
            if (GenReceiptDT.Rows.Count != 0)
            {

                if (RptName == "Raw Material Stock Details Report")
                {

                    DataSet ds1 = new DataSet();
                    ds1.Tables.Add(GenReceiptDT);
                    RD_Report.Load(Server.MapPath("~/crystal/RawMaterialClosingStock.rpt"));
                    RD_Report.SetDataSource(ds1.Tables[0]);
                    //Company Details Add
                    DataTable dtdCompanyAddress = new DataTable();
                    DataTable Close_Det = new DataTable();

                    SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                    dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);
                    if (dtdCompanyAddress.Rows.Count != 0)
                    {
                        RD_Report.DataDefinition.FormulaFields["CompanyName"].Text = "'" + dtdCompanyAddress.Rows[0]["Cname"].ToString() + "'";
                        RD_Report.DataDefinition.FormulaFields["Address1"].Text = "'" + dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString() + "'";
                        RD_Report.DataDefinition.FormulaFields["Address2"].Text = "'" + dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString() + "'";
                    }
                    if (FromDate != "" && ToDate != "")
                    {
                        RD_Report.DataDefinition.FormulaFields["FromDate"].Text = "'" + FromDate.ToString() + "'";
                        RD_Report.DataDefinition.FormulaFields["ToDate"].Text = "'" + ToDate.ToString() + "'";
                    }

                    RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
                    CrystalReportViewer1.ReportSource = RD_Report;
                    CrystalReportViewer1.RefreshReport();
                    CrystalReportViewer1.DataBind();
                }

            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
                lblUploadSuccessfully.Text = "No Records Matched..";
            }


        }

    }

    public void SparesStock_Details_Report()
    {
        DataTable StockDT = new DataTable();
        DataTable Department_DT = new DataTable();
        DataTable Open_Stck = new DataTable();
        DataTable Purchase_DT = new DataTable();
        DataTable Issue_DT = new DataTable();
        DataTable GenReceiptDT = new DataTable();
        DataTable ItemDT = new DataTable();

        AutoDataTable.Columns.Add("WareHouseName");
        AutoDataTable.Columns.Add("ItemName");
        AutoDataTable.Columns.Add("Opening_Qty");
        AutoDataTable.Columns.Add("Opening_Val");
        AutoDataTable.Columns.Add("Rec_Qty");
        AutoDataTable.Columns.Add("Rec_Val");
        AutoDataTable.Columns.Add("Consum_Qty");
        AutoDataTable.Columns.Add("Consum_Val");


        if (FromDate != "")
        {
            frmDate = Convert.ToDateTime(FromDate);
        }
        if (ToDate != "")
        {
            toDate = Convert.ToDateTime(ToDate);
        }

        string[] finyear = SessionFinYearVal.Split('_');
        string FromDate_Chk = "01/04/" + finyear[0];
        DateTime fromDate_Chk = Convert.ToDateTime(FromDate_Chk);


        string[] FromDate_ChkStr = FromDate.Split('/');


        if (FromDate == FromDate_Chk)
        {
            SSQL = "Delete  Stores_Closing_Stock_Print";
            objdata.RptEmployeeMultipleDetails(SSQL);

            SSQL = "Select Distinct WarehouseName from Stores_Stock_Ledger_All where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "'";
            Department_DT = objdata.RptEmployeeMultipleDetails(SSQL);
            for (int i = 0; i < Department_DT.Rows.Count; i++)
            {
                SSQL = "Select Distinct ItemName from Stores_Stock_Ledger_All where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "'";
                SSQL = SSQL + "And WarehouseName='" + Department_DT.Rows[i]["WarehouseName"].ToString() + "'";

                ItemDT = objdata.RptEmployeeMultipleDetails(SSQL);
                for (int j = 0; j < ItemDT.Rows.Count; j++)
                {
                    SSQL = "Select sum(Add_Qty) as Opening_Qty,sum(Add_Value) as Opening_Val from Cheese_Stock_Ledger_All where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "'";
                    SSQL = SSQL + "And WarehouseName='" + Department_DT.Rows[i]["WarehouseName"].ToString() + "'";
                    SSQL = SSQL + "And ItemName='" + ItemDT.Rows[j]["ItemName"].ToString() + "'";
                    SSQL = SSQL + "And Trans_Type='OPENING RECEIPT'";
                    //SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date_Str, 103)=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";

                    Open_Stck = objdata.RptEmployeeMultipleDetails(SSQL);

                    SSQL = "Select sum(Add_Qty) as Rec_Qty,sum(Add_Value) as Rec_Val from Cheese_Stock_Ledger_All where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "'";
                    SSQL = SSQL + "And WarehouseName='" + Department_DT.Rows[i]["WarehouseName"].ToString() + "'";
                    SSQL = SSQL + "And ItemName='" + ItemDT.Rows[j]["ItemName"].ToString() + "'";
                    SSQL = SSQL + "And (Trans_Type='SPARES RECEIPT' or Trans_Type='SPARES DELIVERY' or Trans_Type='SERVICE RECEIPT' or Trans_Type='SERVICE DELIVERY')";
                    SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date_Str, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";
                    SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date_Str, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";

                    Purchase_DT = objdata.RptEmployeeMultipleDetails(SSQL);

                    SSQL = "Select sum(Minus_Qty) as Consum_Qty,sum(Minus_Value) as Consum_Val from Cheese_Stock_Ledger_All where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "'";
                    SSQL = SSQL + "And WarehouseName='" + Department_DT.Rows[i]["WarehouseName"].ToString() + "'";
                    SSQL = SSQL + "And ItemName='" + ItemDT.Rows[j]["ItemName"].ToString() + "'";
                    SSQL = SSQL + "And (Trans_Type='SPARES DELIVERY' or Trans_Type='SERVICE DELIVERY')";
                    SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date_Str, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";
                    SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date_Str, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";

                    Issue_DT = objdata.RptEmployeeMultipleDetails(SSQL);



                    SSQL = "Insert Into Stores_Closing_Stock_Print(Ccode,Lcode,FinYearCode,FinYearVal,WareHouseName,ItemName,Opening_Qty,Opening_Val,Rec_Qty,Rec_Val,";
                    SSQL = SSQL + "Consum_Qty,Consum_Val,From_Date,To_Date,UserID,UserName) Values('" + SessionCcode + "','" + SessionLcode + "',";
                    SSQL = SSQL + " '" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + Department_DT.Rows[i]["WareHouseName"].ToString() + "','" + ItemDT.Rows[j]["ItemName"].ToString() + "',";
                    if (Open_Stck.Rows.Count != 0)
                    {
                        if (Open_Stck.Rows[0]["Opening_Qty"].ToString() != "")
                        {
                            SSQL = SSQL + " '" + Open_Stck.Rows[0]["Opening_Qty"] + "',";
                        }
                        else
                        {
                            SSQL = SSQL + " '0.00',";
                        }
                        if (Open_Stck.Rows[0]["Opening_Val"].ToString() != "")
                        {
                            SSQL = SSQL + " '" + Open_Stck.Rows[0]["Opening_Val"] + "',";
                        }
                        else
                        {
                            SSQL = SSQL + " '0.00',";
                        }
                    }
                    else
                    {
                        SSQL = SSQL + " '0.00','0.00',";
                    }

                    if (Purchase_DT.Rows.Count != 0)
                    {
                        if (Purchase_DT.Rows[0]["Rec_Qty"].ToString() != "")
                        {
                            SSQL = SSQL + "'" + Purchase_DT.Rows[0]["Rec_Qty"] + "',";
                        }
                        else
                        {
                            SSQL = SSQL + "'0.00',";
                        }
                        if (Purchase_DT.Rows[0]["Rec_Val"].ToString() != "")
                        {
                            SSQL = SSQL + "'" + Purchase_DT.Rows[0]["Rec_Val"] + "',";
                        }
                        else
                        {
                            SSQL = SSQL + "'0.00',";
                        }
                    }
                    else
                    {
                        SSQL = SSQL + "'0.00','0.00',";
                    }
                    if (Issue_DT.Rows.Count != 0)
                    {
                        if (Issue_DT.Rows[0]["Consum_Qty"].ToString() != "")
                        {
                            SSQL = SSQL + "'" + Issue_DT.Rows[0]["Consum_Qty"] + "',";
                        }
                        else
                        {
                            SSQL = SSQL + "'0.00',";
                        }
                        if (Issue_DT.Rows[0]["Consum_Val"].ToString() != "")
                        {
                            SSQL = SSQL + "'" + Issue_DT.Rows[0]["Consum_Val"] + "',";
                        }
                        else
                        {
                            SSQL = SSQL + "'0.00',";
                        }
                    }
                    else
                    {
                        SSQL = SSQL + "'0.00','0.00',";
                    }


                    SSQL = SSQL + " '" + FromDate + "','" + ToDate + "','" + SessionUserID + "','" + SessionUserName + "')";
                    objdata.RptEmployeeMultipleDetails(SSQL);
                }
            }

            //Get Closing Stock
            SSQL = "Select * from Stores_Closing_Stock_Print where";
            SSQL = SSQL + " Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
            if (FromDate != "" && ToDate != "")
            {
                SSQL = SSQL + " And CONVERT(DATETIME,From_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";
                SSQL = SSQL + " And CONVERT(DATETIME,To_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
            }
            SSQL = SSQL + " order by WareHouseName";

            GenReceiptDT = objdata.RptEmployeeMultipleDetails(SSQL);
            if (GenReceiptDT.Rows.Count != 0)
            {

                if (RptName == "Spares Stock Details Report")
                {

                    DataSet ds1 = new DataSet();
                    ds1.Tables.Add(GenReceiptDT);
                    RD_Report.Load(Server.MapPath("~/crystal/SparesClosingStock.rpt"));
                    RD_Report.SetDataSource(ds1.Tables[0]);
                    //Company Details Add
                    DataTable dtdCompanyAddress = new DataTable();
                    DataTable Close_Det = new DataTable();

                    SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                    dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);
                    if (dtdCompanyAddress.Rows.Count != 0)
                    {
                        RD_Report.DataDefinition.FormulaFields["CompanyName"].Text = "'" + dtdCompanyAddress.Rows[0]["Cname"].ToString() + "'";
                        RD_Report.DataDefinition.FormulaFields["Address1"].Text = "'" + dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString() + "'";
                        RD_Report.DataDefinition.FormulaFields["Address2"].Text = "'" + dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString() + "'";
                    }
                    if (FromDate != "" && ToDate != "")
                    {
                        RD_Report.DataDefinition.FormulaFields["FromDate"].Text = "'" + FromDate.ToString() + "'";
                        RD_Report.DataDefinition.FormulaFields["ToDate"].Text = "'" + ToDate.ToString() + "'";
                    }


                    RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
                    CrystalReportViewer1.ReportSource = RD_Report;
                    CrystalReportViewer1.RefreshReport();
                    CrystalReportViewer1.DataBind();
                }

            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
                lblUploadSuccessfully.Text = "No Records Matched..";
            }

        }
        else
        {

            SSQL = "Delete  Stores_Closing_Stock_Print";
            objdata.RptEmployeeMultipleDetails(SSQL);

            SSQL = "Select Distinct WarehouseName from Stores_Stock_Ledger_All where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "'";
            Department_DT = objdata.RptEmployeeMultipleDetails(SSQL);
            for (int i = 0; i < Department_DT.Rows.Count; i++)
            {
                SSQL = "Select Distinct ItemName from Stores_Stock_Ledger_All where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "'";
                SSQL = SSQL + "And WarehouseName='" + Department_DT.Rows[i]["WarehouseName"].ToString() + "'";

                ItemDT = objdata.RptEmployeeMultipleDetails(SSQL);
                for (int j = 0; j < ItemDT.Rows.Count; j++)
                {
                    SSQL = "select (sum(Add_Qty)-sum(Minus_Qty)) as Opening_Qty,(sum(Add_Value)-sum(Minus_Value)) as Opening_Val from Stores_Stock_Ledger_All where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "'";
                    SSQL = SSQL + "And WarehouseName='" + Department_DT.Rows[i]["WarehouseName"].ToString() + "'";
                    SSQL = SSQL + "And ItemName='" + ItemDT.Rows[j]["ItemName"].ToString() + "'";
                    if (FromDate != "" && ToDate != "")
                    {
                        SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date_Str, 103)<=CONVERT(DATETIME,'" + frmDate.AddDays(-1).ToString("dd/MM/yyyy") + "',103)";
                    }
                    else
                    {
                        SSQL = SSQL + "And Trans_Type='OPENING RECEIPT'";
                    }
                    Open_Stck = objdata.RptEmployeeMultipleDetails(SSQL);


                    SSQL = "Select sum(Add_Qty) as Rec_Qty,sum(Add_Value) as Rec_Val from Stores_Stock_Ledger_All where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "'";
                    SSQL = SSQL + "And WarehouseName='" + Department_DT.Rows[i]["WarehouseName"].ToString() + "'";
                    SSQL = SSQL + "And ItemName='" + ItemDT.Rows[j]["ItemName"].ToString() + "'";
                    SSQL = SSQL + "And (Trans_Type='SPARES RECEIPT' or Trans_Type='SPARES DELIVERY' or Trans_Type='SERVICE RECEIPT' or Trans_Type='SERVICE DELIVERY')";
                    if (FromDate != "" && ToDate != "")
                    {
                        SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date_Str, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";
                        SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date_Str, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
                    }
                    Purchase_DT = objdata.RptEmployeeMultipleDetails(SSQL);

                    SSQL = "Select sum(Minus_Qty) as Consum_Qty,sum(Minus_Value) as Consum_Val from Stores_Stock_Ledger_All where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "'";
                    SSQL = SSQL + "And WarehouseName='" + Department_DT.Rows[i]["WarehouseName"].ToString() + "'";
                    SSQL = SSQL + "And ItemName='" + ItemDT.Rows[j]["ItemName"].ToString() + "'";
                    SSQL = SSQL + "And (Trans_Type='SPARES DELIVERY' or Trans_Type='SERVICE DELIVERY')";
                    if (FromDate != "" && ToDate != "")
                    {
                        SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date_Str, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";
                        SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date_Str, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
                    }
                    Issue_DT = objdata.RptEmployeeMultipleDetails(SSQL);



                    SSQL = "Insert Into Stores_Closing_Stock_Print(Ccode,Lcode,FinYearCode,FinYearVal,WareHouseName,ItemName,Opening_Qty,Opening_Val,Rec_Qty,Rec_Val,";
                    SSQL = SSQL + "Consum_Qty,Consum_Val,From_Date,To_Date,UserID,UserName) Values('" + SessionCcode + "','" + SessionLcode + "',";
                    SSQL = SSQL + " '" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + Department_DT.Rows[i]["WareHouseName"].ToString() + "','" + ItemDT.Rows[j]["ItemName"].ToString() + "',";
                    if (Open_Stck.Rows.Count != 0)
                    {
                        if (Open_Stck.Rows[0]["Opening_Qty"].ToString() != "")
                        {
                            SSQL = SSQL + " '" + Open_Stck.Rows[0]["Opening_Qty"] + "',";
                        }
                        else
                        {
                            SSQL = SSQL + " '0.00',";
                        }
                        if (Open_Stck.Rows[0]["Opening_Val"].ToString() != "")
                        {
                            SSQL = SSQL + " '" + Open_Stck.Rows[0]["Opening_Val"] + "',";
                        }
                        else
                        {
                            SSQL = SSQL + " '0.00',";
                        }
                    }
                    else
                    {
                        SSQL = SSQL + " '0.00','0.00',";
                    }

                    if (Purchase_DT.Rows.Count != 0)
                    {
                        if (Purchase_DT.Rows[0]["Rec_Qty"].ToString() != "")
                        {
                            SSQL = SSQL + "'" + Purchase_DT.Rows[0]["Rec_Qty"] + "',";
                        }
                        else
                        {
                            SSQL = SSQL + "'0.00',";
                        }
                        if (Purchase_DT.Rows[0]["Rec_Val"].ToString() != "")
                        {
                            SSQL = SSQL + "'" + Purchase_DT.Rows[0]["Rec_Val"] + "',";
                        }
                        else
                        {
                            SSQL = SSQL + "'0.00',";
                        }
                    }
                    else
                    {
                        SSQL = SSQL + "'0.00','0.00',";
                    }
                    if (Issue_DT.Rows.Count != 0)
                    {
                        if (Issue_DT.Rows[0]["Consum_Qty"].ToString() != "")
                        {
                            SSQL = SSQL + "'" + Issue_DT.Rows[0]["Consum_Qty"] + "',";
                        }
                        else
                        {
                            SSQL = SSQL + "'0.00',";
                        }
                        if (Issue_DT.Rows[0]["Consum_Val"].ToString() != "")
                        {
                            SSQL = SSQL + "'" + Issue_DT.Rows[0]["Consum_Val"] + "',";
                        }
                        else
                        {
                            SSQL = SSQL + "'0.00',";
                        }
                    }
                    else
                    {
                        SSQL = SSQL + "'0.00','0.00',";
                    }


                    SSQL = SSQL + " '" + FromDate + "','" + ToDate + "','" + SessionUserID + "','" + SessionUserName + "')";
                    objdata.RptEmployeeMultipleDetails(SSQL);

                }
            }



            //Get Closing Stock
            SSQL = "Select * from Stores_Closing_Stock_Print where";
            SSQL = SSQL + " Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
            if (FromDate != "" && ToDate != "")
            {
                SSQL = SSQL + " And CONVERT(DATETIME,From_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";
                SSQL = SSQL + " And CONVERT(DATETIME,To_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
            }
            SSQL = SSQL + " order by WareHouseName";

            GenReceiptDT = objdata.RptEmployeeMultipleDetails(SSQL);
            if (GenReceiptDT.Rows.Count != 0)
            {

                if (RptName == "Spares Stock Details Report")
                {

                    DataSet ds1 = new DataSet();
                    ds1.Tables.Add(GenReceiptDT);
                    RD_Report.Load(Server.MapPath("~/crystal/SparesClosingStock.rpt"));
                    RD_Report.SetDataSource(ds1.Tables[0]);
                    //Company Details Add
                    DataTable dtdCompanyAddress = new DataTable();
                    DataTable Close_Det = new DataTable();

                    SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                    dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);
                    if (dtdCompanyAddress.Rows.Count != 0)
                    {
                        RD_Report.DataDefinition.FormulaFields["CompanyName"].Text = "'" + dtdCompanyAddress.Rows[0]["Cname"].ToString() + "'";
                        RD_Report.DataDefinition.FormulaFields["Address1"].Text = "'" + dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString() + "'";
                        RD_Report.DataDefinition.FormulaFields["Address2"].Text = "'" + dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString() + "'";
                    }
                    if (FromDate != "" && ToDate != "")
                    {
                        RD_Report.DataDefinition.FormulaFields["FromDate"].Text = "'" + FromDate.ToString() + "'";
                        RD_Report.DataDefinition.FormulaFields["ToDate"].Text = "'" + ToDate.ToString() + "'";
                    }

                    RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
                    CrystalReportViewer1.ReportSource = RD_Report;
                    CrystalReportViewer1.RefreshReport();
                    CrystalReportViewer1.DataBind();
                }

            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
                lblUploadSuccessfully.Text = "No Records Matched..";
            }


        }

    }

    public void YarnStock_Details_Report()
    {
        DataTable StockDT = new DataTable();
        DataTable Department_DT = new DataTable();
        DataTable Open_Stck = new DataTable();
        DataTable Purchase_DT = new DataTable();
        DataTable Issue_DT = new DataTable();
        DataTable GenReceiptDT = new DataTable();
        DataTable ItemDT = new DataTable();

        AutoDataTable.Columns.Add("GoDownName");
        AutoDataTable.Columns.Add("ItemName");
        AutoDataTable.Columns.Add("Opening_Qty");
        AutoDataTable.Columns.Add("Opening_Val");
        AutoDataTable.Columns.Add("Opening_Bag");
        AutoDataTable.Columns.Add("Rec_Qty");
        AutoDataTable.Columns.Add("Rec_Val");
        AutoDataTable.Columns.Add("Rec_Bag");
        AutoDataTable.Columns.Add("Consum_Qty");
        AutoDataTable.Columns.Add("Consum_Val");
        AutoDataTable.Columns.Add("Consum_Bag");


        if (FromDate != "")
        {
            frmDate = Convert.ToDateTime(FromDate);
        }
        if (ToDate != "")
        {
            toDate = Convert.ToDateTime(ToDate);
        }

        string[] finyear = SessionFinYearVal.Split('_');
        string FromDate_Chk = "01/04/" + finyear[0];
        DateTime fromDate_Chk = Convert.ToDateTime(FromDate_Chk);


        string[] FromDate_ChkStr = FromDate.Split('/');


        if (FromDate == FromDate_Chk)
        {
            SSQL = "Delete  Yarn_Closing_Stock_Print";
            objdata.RptEmployeeMultipleDetails(SSQL);

            SSQL = "Select Distinct GoDownName from Yarn_Stock_Ledger_All where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "'";
            Department_DT = objdata.RptEmployeeMultipleDetails(SSQL);
            for (int i = 0; i < Department_DT.Rows.Count; i++)
            {
                SSQL = "Select Distinct CountName from Yarn_Stock_Ledger_All where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "'";
                SSQL = SSQL + "And GoDownName='" + Department_DT.Rows[i]["GoDownName"].ToString() + "'";

                ItemDT = objdata.RptEmployeeMultipleDetails(SSQL);
                for (int j = 0; j < ItemDT.Rows.Count; j++)
                {
                    SSQL = "Select sum(Add_Qty) as Opening_Qty,sum(Add_LossKg) as Opening_Val,sum(Add_Bag) as Opening_Bag from Yarn_Stock_Ledger_All where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "'";
                    SSQL = SSQL + "And GoDownName='" + Department_DT.Rows[i]["GoDownName"].ToString() + "'";
                    SSQL = SSQL + "And CountName='" + ItemDT.Rows[j]["CountName"].ToString() + "'";
                    SSQL = SSQL + "And Trans_Type='OPENING RECEIPT'";
                    //SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date_Str, 103)=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";

                    Open_Stck = objdata.RptEmployeeMultipleDetails(SSQL);

                    SSQL = "Select sum(Add_Qty) as Rec_Qty,sum(Add_LossKg) as Rec_Val,sum(Add_Bag) as Rec_Bag from Yarn_Stock_Ledger_All where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "'";
                    SSQL = SSQL + "And GoDownName='" + Department_DT.Rows[i]["GoDownName"].ToString() + "'";
                    SSQL = SSQL + "And CountName='" + ItemDT.Rows[j]["CountName"].ToString() + "'";
                    SSQL = SSQL + "And (Trans_Type='Yarn Production' or Trans_Type='YARN DELIVERY')";
                    SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date_Str, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";
                    SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date_Str, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";

                    Purchase_DT = objdata.RptEmployeeMultipleDetails(SSQL);

                    SSQL = "Select sum(Minus_Qty) as Consum_Qty,sum(Minus_LossKg) as Consum_Val,sum(Minus_Bag) as Consum_Bag from Yarn_Stock_Ledger_All where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "'";
                    SSQL = SSQL + "And GoDownName='" + Department_DT.Rows[i]["GoDownName"].ToString() + "'";
                    SSQL = SSQL + "And CountName='" + ItemDT.Rows[j]["CountName"].ToString() + "'";
                    SSQL = SSQL + "And Trans_Type='RAW MATERIAL DELIVERY'";
                    SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date_Str, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";
                    SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date_Str, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";

                    Issue_DT = objdata.RptEmployeeMultipleDetails(SSQL);

                    SSQL = "Insert Into Yarn_Closing_Stock_Print(Ccode,Lcode,FinYearCode,FinYearVal,GoDownName,CountName,Opening_Qty,Opening_Val,Opening_Bag,Rec_Qty,Rec_Val,Rec_Bag,";
                    SSQL = SSQL + "Consum_Qty,Consum_Val,Consum_Bag,From_Date,To_Date,UserID,UserName) Values('" + SessionCcode + "','" + SessionLcode + "',";
                    SSQL = SSQL + " '" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + Department_DT.Rows[i]["GoDownName"].ToString() + "','" + ItemDT.Rows[j]["CountName"].ToString() + "',";
                    if (Open_Stck.Rows.Count != 0)
                    {
                        if (Open_Stck.Rows[0]["Opening_Qty"].ToString() != "")
                        {
                            SSQL = SSQL + " '" + Open_Stck.Rows[0]["Opening_Qty"] + "',";
                        }
                        else
                        {
                            SSQL = SSQL + " '0.00',";
                        }
                        if (Open_Stck.Rows[0]["Opening_Val"].ToString() != "")
                        {
                            SSQL = SSQL + " '" + Open_Stck.Rows[0]["Opening_Val"] + "',";
                        }
                        else
                        {
                            SSQL = SSQL + " '0.00',";
                        }
                        if (Open_Stck.Rows[0]["Opening_Bag"].ToString() != "")
                        {
                            SSQL = SSQL + " '" + Open_Stck.Rows[0]["Opening_Bag"] + "',";
                        }
                        else
                        {
                            SSQL = SSQL + " '0.00',";
                        }
                    }
                    else
                    {
                        SSQL = SSQL + " '0.00','0.00','0.00',";
                    }

                    if (Purchase_DT.Rows.Count != 0)
                    {
                        if (Purchase_DT.Rows[0]["Rec_Qty"].ToString() != "")
                        {
                            SSQL = SSQL + "'" + Purchase_DT.Rows[0]["Rec_Qty"] + "',";
                        }
                        else
                        {
                            SSQL = SSQL + "'0.00',";
                        }
                        if (Purchase_DT.Rows[0]["Rec_Val"].ToString() != "")
                        {
                            SSQL = SSQL + "'" + Purchase_DT.Rows[0]["Rec_Val"] + "',";
                        }
                        else
                        {
                            SSQL = SSQL + "'0.00',";
                        }
                        if (Purchase_DT.Rows[0]["Rec_Bag"].ToString() != "")
                        {
                            SSQL = SSQL + "'" + Purchase_DT.Rows[0]["Rec_Bag"] + "',";
                        }
                        else
                        {
                            SSQL = SSQL + "'0.00',";
                        }
                    }
                    else
                    {
                        SSQL = SSQL + "'0.00','0.00','0.00',";
                    }
                    if (Issue_DT.Rows.Count != 0)
                    {
                        if (Issue_DT.Rows[0]["Consum_Qty"].ToString() != "")
                        {
                            SSQL = SSQL + "'" + Issue_DT.Rows[0]["Consum_Qty"] + "',";
                        }
                        else
                        {
                            SSQL = SSQL + "'0.00',";
                        }
                        if (Issue_DT.Rows[0]["Consum_Val"].ToString() != "")
                        {
                            SSQL = SSQL + "'" + Issue_DT.Rows[0]["Consum_Val"] + "',";
                        }
                        else
                        {
                            SSQL = SSQL + "'0.00',";
                        }
                        if (Issue_DT.Rows[0]["Consum_Bag"].ToString() != "")
                        {
                            SSQL = SSQL + "'" + Issue_DT.Rows[0]["Consum_Bag"] + "',";
                        }
                        else
                        {
                            SSQL = SSQL + "'0.00',";
                        }
                    }
                    else
                    {
                        SSQL = SSQL + "'0.00','0.00','0.00',";
                    }


                    SSQL = SSQL + " '" + FromDate + "','" + ToDate + "','" + SessionUserID + "','" + SessionUserName + "')";
                    objdata.RptEmployeeMultipleDetails(SSQL);
                }
            }

            //Get Closing Stock
            SSQL = "Select * from Yarn_Closing_Stock_Print where";
            SSQL = SSQL + " Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
            if (FromDate != "" && ToDate != "")
            {
                SSQL = SSQL + " And CONVERT(DATETIME,From_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";
                SSQL = SSQL + " And CONVERT(DATETIME,To_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
            }
            SSQL = SSQL + " order by GoDownName";

            GenReceiptDT = objdata.RptEmployeeMultipleDetails(SSQL);
            if (GenReceiptDT.Rows.Count != 0)
            {

                if (RptName == "Yarn Stock Details Report")
                {

                    DataSet ds1 = new DataSet();
                    ds1.Tables.Add(GenReceiptDT);
                    RD_Report.Load(Server.MapPath("~/crystal/YarnClosingStock.rpt"));
                    RD_Report.SetDataSource(ds1.Tables[0]);
                    //Company Details Add
                    DataTable dtdCompanyAddress = new DataTable();
                    DataTable Close_Det = new DataTable();

                    SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                    dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);
                    if (dtdCompanyAddress.Rows.Count != 0)
                    {
                        RD_Report.DataDefinition.FormulaFields["CompanyName"].Text = "'" + dtdCompanyAddress.Rows[0]["Cname"].ToString() + "'";
                        RD_Report.DataDefinition.FormulaFields["Address1"].Text = "'" + dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString() + "'";
                        RD_Report.DataDefinition.FormulaFields["Address2"].Text = "'" + dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString() + "'";
                    }
                    if (FromDate != "" && ToDate != "")
                    {
                        RD_Report.DataDefinition.FormulaFields["FromDate"].Text = "'" + FromDate.ToString() + "'";
                        RD_Report.DataDefinition.FormulaFields["ToDate"].Text = "'" + ToDate.ToString() + "'";
                    }


                    RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
                    CrystalReportViewer1.ReportSource = RD_Report;
                    CrystalReportViewer1.RefreshReport();
                    CrystalReportViewer1.DataBind();
                }

            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
                lblUploadSuccessfully.Text = "No Records Matched..";
            }

        }
        else
        {

            SSQL = "Delete  Yarn_Closing_Stock_Print";
            objdata.RptEmployeeMultipleDetails(SSQL);

            SSQL = "Select Distinct GoDownName from Yarn_Stock_Ledger_All where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "'";

            Department_DT = objdata.RptEmployeeMultipleDetails(SSQL);
            for (int i = 0; i < Department_DT.Rows.Count; i++)
            {
                SSQL = "Select Distinct CountName from Yarn_Stock_Ledger_All where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "'";
                SSQL = SSQL + "And GoDownName='" + Department_DT.Rows[i]["GoDownName"].ToString() + "'";

                ItemDT = objdata.RptEmployeeMultipleDetails(SSQL);
                for (int j = 0; j < ItemDT.Rows.Count; j++)
                {
                    SSQL = "select (sum(Add_Qty)-sum(Minus_Qty)) as Opening_Qty,(sum(Add_LossKg)-sum(Minus_LossKg)) as Opening_Val,(sum(Add_Bag)-sum(Minus_Bag)) as Opening_Bag from Yarn_Stock_Ledger_All where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "'";
                    SSQL = SSQL + " And GoDownName='" + Department_DT.Rows[i]["GoDownName"].ToString() + "'";
                    SSQL = SSQL + " And CountName='" + ItemDT.Rows[j]["CountName"].ToString() + "'";
                    if (FromDate != "" && ToDate != "")
                    {
                        SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date_Str, 103)<=CONVERT(DATETIME,'" + frmDate.AddDays(-1).ToString("dd/MM/yyyy") + "',103)";
                    }
                    else
                    {
                        SSQL = SSQL + " And Trans_Type='OPENING RECEIPT'";
                    }
                    Open_Stck = objdata.RptEmployeeMultipleDetails(SSQL);


                    SSQL = "Select sum(Add_Qty) as Rec_Qty,sum(Add_LossKg) as Rec_Val,sum(Add_Bag) as Rec_Bag from Yarn_Stock_Ledger_All where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "'";
                    SSQL = SSQL + "And GoDownName='" + Department_DT.Rows[i]["GoDownName"].ToString() + "'";
                    SSQL = SSQL + "And CountName='" + ItemDT.Rows[j]["CountName"].ToString() + "'";
                    SSQL = SSQL + "And (Trans_Type='Yarn Production' or Trans_Type='YARN DELIVERY')";
                    if (FromDate != "" && ToDate != "")
                    {
                        SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date_Str, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";
                        SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date_Str, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
                    }
                    Purchase_DT = objdata.RptEmployeeMultipleDetails(SSQL);

                    SSQL = "Select sum(Minus_Qty) as Consum_Qty,sum(Minus_LossKg) as Consum_Val,sum(Minus_Bag) as Consum_Bag from Yarn_Stock_Ledger_All where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "'";
                    SSQL = SSQL + "And GoDownName='" + Department_DT.Rows[i]["GoDownName"].ToString() + "'";
                    SSQL = SSQL + "And CountName='" + ItemDT.Rows[j]["CountName"].ToString() + "'";
                    SSQL = SSQL + "And Trans_Type='YARN DELIVERY'";
                    if (FromDate != "" && ToDate != "")
                    {
                        SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date_Str, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";
                        SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date_Str, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
                    }
                    Issue_DT = objdata.RptEmployeeMultipleDetails(SSQL);



                    SSQL = "Insert Into Yarn_Closing_Stock_Print(Ccode,Lcode,FinYearCode,FinYearVal,GoDownName,ItemName,Opening_Qty,Opening_Val,Opening_Bag,Rec_Qty,Rec_Val,Rec_Bag,";
                    SSQL = SSQL + "Consum_Qty,Consum_Val,Consum_Bag,From_Date,To_Date,UserID,UserName) Values('" + SessionCcode + "','" + SessionLcode + "',";
                    SSQL = SSQL + " '" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + Department_DT.Rows[i]["GoDownName"].ToString() + "','" + ItemDT.Rows[j]["CountName"].ToString() + "',";
                    if (Open_Stck.Rows.Count != 0)
                    {
                        if (Open_Stck.Rows[0]["Opening_Qty"].ToString() != "")
                        {
                            SSQL = SSQL + " '" + Open_Stck.Rows[0]["Opening_Qty"] + "',";
                        }
                        else
                        {
                            SSQL = SSQL + " '0.00',";
                        }
                        if (Open_Stck.Rows[0]["Opening_Val"].ToString() != "")
                        {
                            SSQL = SSQL + " '" + Open_Stck.Rows[0]["Opening_Val"] + "',";
                        }
                        else
                        {
                            SSQL = SSQL + " '0.00',";
                        }
                        if (Open_Stck.Rows[0]["Opening_Bag"].ToString() != "")
                        {
                            SSQL = SSQL + " '" + Open_Stck.Rows[0]["Opening_Bag"] + "',";
                        }
                        else
                        {
                            SSQL = SSQL + " '0.00',";
                        }
                    }
                    else
                    {
                        SSQL = SSQL + " '0.00','0.00','0.00',";
                    }

                    if (Purchase_DT.Rows.Count != 0)
                    {
                        if (Purchase_DT.Rows[0]["Rec_Qty"].ToString() != "")
                        {
                            SSQL = SSQL + "'" + Purchase_DT.Rows[0]["Rec_Qty"] + "',";
                        }
                        else
                        {
                            SSQL = SSQL + "'0.00',";
                        }
                        if (Purchase_DT.Rows[0]["Rec_Val"].ToString() != "")
                        {
                            SSQL = SSQL + "'" + Purchase_DT.Rows[0]["Rec_Val"] + "',";
                        }
                        else
                        {
                            SSQL = SSQL + "'0.00',";
                        }
                        if (Purchase_DT.Rows[0]["Rec_Bag"].ToString() != "")
                        {
                            SSQL = SSQL + "'" + Purchase_DT.Rows[0]["Rec_Bag"] + "',";
                        }
                        else
                        {
                            SSQL = SSQL + "'0.00',";
                        }
                    }
                    else
                    {
                        SSQL = SSQL + "'0.00','0.00','0.00',";
                    }
                    if (Issue_DT.Rows.Count != 0)
                    {
                        if (Issue_DT.Rows[0]["Consum_Qty"].ToString() != "")
                        {
                            SSQL = SSQL + "'" + Issue_DT.Rows[0]["Consum_Qty"] + "',";
                        }
                        else
                        {
                            SSQL = SSQL + "'0.00',";
                        }
                        if (Issue_DT.Rows[0]["Consum_Val"].ToString() != "")
                        {
                            SSQL = SSQL + "'" + Issue_DT.Rows[0]["Consum_Val"] + "',";
                        }
                        else
                        {
                            SSQL = SSQL + "'0.00',";
                        }
                        if (Issue_DT.Rows[0]["Consum_Bag"].ToString() != "")
                        {
                            SSQL = SSQL + "'" + Issue_DT.Rows[0]["Consum_Bag"] + "',";
                        }
                        else
                        {
                            SSQL = SSQL + "'0.00',";
                        }
                    }
                    else
                    {
                        SSQL = SSQL + "'0.00','0.00','0.00',";
                    }


                    SSQL = SSQL + " '" + FromDate + "','" + ToDate + "','" + SessionUserID + "','" + SessionUserName + "')";
                    objdata.RptEmployeeMultipleDetails(SSQL);

                }
            }



            //Get Closing Stock
            SSQL = "Select * from Yarn_Closing_Stock_Print where";
            SSQL = SSQL + " Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
            if (FromDate != "" && ToDate != "")
            {
                SSQL = SSQL + " And CONVERT(DATETIME,From_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";
                SSQL = SSQL + " And CONVERT(DATETIME,To_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
            }
            SSQL = SSQL + " order by GoDownName";

            GenReceiptDT = objdata.RptEmployeeMultipleDetails(SSQL);
            if (GenReceiptDT.Rows.Count != 0)
            {

                if (RptName == "Yarn Stock Details Report")
                {

                    DataSet ds1 = new DataSet();
                    ds1.Tables.Add(GenReceiptDT);
                    RD_Report.Load(Server.MapPath("~/crystal/YarnClosingStock.rpt"));
                    RD_Report.SetDataSource(ds1.Tables[0]);
                    //Company Details Add
                    DataTable dtdCompanyAddress = new DataTable();
                    DataTable Close_Det = new DataTable();

                    SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                    dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);
                    if (dtdCompanyAddress.Rows.Count != 0)
                    {
                        RD_Report.DataDefinition.FormulaFields["CompanyName"].Text = "'" + dtdCompanyAddress.Rows[0]["Cname"].ToString() + "'";
                        RD_Report.DataDefinition.FormulaFields["Address1"].Text = "'" + dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString() + "'";
                        RD_Report.DataDefinition.FormulaFields["Address2"].Text = "'" + dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString() + "'";
                    }
                    if (FromDate != "" && ToDate != "")
                    {
                        RD_Report.DataDefinition.FormulaFields["FromDate"].Text = "'" + FromDate.ToString() + "'";
                        RD_Report.DataDefinition.FormulaFields["ToDate"].Text = "'" + ToDate.ToString() + "'";
                    }

                    RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
                    CrystalReportViewer1.ReportSource = RD_Report;
                    CrystalReportViewer1.RefreshReport();
                    CrystalReportViewer1.DataBind();
                }

            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
                lblUploadSuccessfully.Text = "No Records Matched..";
            }


        }

    }

    public void YarnStock_Abstract_Report()
    {
        DataTable StockDT = new DataTable();
        DataTable Item_DT = new DataTable();
        DataTable Open_Stck = new DataTable();
        DataTable GoDown_DT = new DataTable();

        if (FromDate != "")
        {
            frmDate = Convert.ToDateTime(FromDate);
        }
        if (ToDate != "")
        {
            toDate = Convert.ToDateTime(ToDate);
        }

        //Get the Team for Particular Date
        SSQL = "Select Distinct GoDownName from Yarn_Stock_Ledger_All where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "'";
        if (WareHouseCode != "")
        {
            SSQL = SSQL + " And GoDownID='" + WareHouseCode + "'";
        }
        if (VarietyCode != "")
        {
            SSQL = SSQL + " And CountID='" + VarietyCode + "'";
        }
        if (FromDate != "" && ToDate != "")
        {
            SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date_Str, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";
            SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date_Str, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
        }
        GoDown_DT = objdata.RptEmployeeMultipleDetails(SSQL);

        DataTable dtdCompanyAddress = new DataTable();
        string CompanyName = "";
        string Address1 = ""; 
        SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
        dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);
        if (dtdCompanyAddress.Rows.Count != 0)
        {
            CompanyName = dtdCompanyAddress.Rows[0]["Cname"].ToString();
            Address1 = dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString()+","+dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString();
            
        }

        grid.DataSource = GoDown_DT;
        grid.DataBind();
        string attachment = "attachment;filename=YARN PRODUCTION ABSTRACT.xls";
        Response.ClearContent();
        Response.AddHeader("content-disposition", attachment);
        Response.ContentType = "application/ms-excel";

        grid.HeaderStyle.Font.Bold = true;
        System.IO.StringWriter stw = new System.IO.StringWriter();
        HtmlTextWriter htextw = new HtmlTextWriter(stw);
        grid.RenderControl(htextw);

        Response.Write("<table border='1'>");
        Response.Write("<tr Font-Bold='true' align='center'>");
        Response.Write("<td colspan='" + Convert.ToInt32(GoDown_DT.Rows.Count + 3) + "'>");
        Response.Write("<a style=\"font-weight:bold\">" + CompanyName + "</a>");
        //Response.Write(" --- ");
        //Response.Write("<a style=\"font-weight:bold\">" + SessionLcode + "</a>");
        Response.Write("</td>");
        Response.Write("</tr>");
        Response.Write("<tr Font-Bold='true' align='center'>");
        Response.Write("<td colspan='" + Convert.ToInt32(GoDown_DT.Rows.Count + 3) + "'>");
        Response.Write("<a style=\"font-weight:bold\">" + Address1 + "</a>");
        Response.Write("</td>");
        Response.Write("</tr>");
        Response.Write("<tr Font-Bold='true' align='center'>");
        Response.Write("<td colspan='" + Convert.ToInt32(GoDown_DT.Rows.Count + 3) + "'>");
        Response.Write("<a style=\"font-weight:bold\">YARN PRODUCTION ABSTRACT REPORT</a>");
        Response.Write("  ");
        Response.Write("</td>");
        Response.Write("</tr>");
        Response.Write("<tr Font-Bold='true' align='center'>");
        Response.Write("<td rowspan='2' align='Center' valign='top'>");
        Response.Write("<a style=\"font-weight:bold\"> SNo </a>");
        Response.Write("</td>");
        Response.Write("<td colspan='2' rowspan='2' align='Center' valign='top'>");
        Response.Write("<a style=\"font-weight:bold\"> CountName </a>");
        Response.Write("</td>");
        for (int i = 0; i < GoDown_DT.Rows.Count; i++)
        {
            Response.Write("<td  align='Center' valign='top'>");
            Response.Write("<a style=\"font-weight:bold\">" + GoDown_DT.Rows[i]["GoDownName"].ToString() + "</a>");
            Response.Write("</td>");
        }
        Response.Write("</tr>");
        Response.Write("<tr Font-Bold='true' align='center'>");
        for (int i = 0; i < GoDown_DT.Rows.Count; i++)
        {
            Response.Write("<td align='Center' valign='top'>");
            Response.Write("<a style=\"font-weight:bold\">No Of Bag</a>");
            Response.Write("</td>");
        }
        
        SSQL = "Select Distinct CountName from Yarn_Stock_Ledger_All where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "'";
        Item_DT = objdata.RptEmployeeMultipleDetails(SSQL);

        for (int i = 0; i < Item_DT.Rows.Count; i++)
        {
            Response.Write("</tr>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td>");
            Response.Write("<a>" + (i+1) + "</a>");
            Response.Write("</td>");
            Response.Write("<td colspan='2'>");
            Response.Write("<a>" + Item_DT.Rows[i]["CountName"].ToString() + "</a>");
            Response.Write("</td>");
            for (int j = 0; j < GoDown_DT.Rows.Count; j++)
            {
                SSQL = "Select (sum(Add_Bag)-sum(Minus_Bag)) as Opening_Bag from Yarn_Stock_Ledger_All where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "'";
                SSQL = SSQL + " And CountName='" + Item_DT.Rows[i]["CountName"].ToString() + "' And GoDownName='" + GoDown_DT.Rows[j]["GoDownName"].ToString() +"'";
                StockDT = objdata.RptEmployeeMultipleDetails(SSQL);

                if (StockDT.Rows.Count != 0)
                {
                    Response.Write("<td>");
                    Response.Write("<a>" + StockDT.Rows[0]["Opening_Bag"].ToString() + "</a>");
                    Response.Write("</td>");
                }
                else
                {
                    Response.Write("<td>");
                    Response.Write("<a>0</a>");
                    Response.Write("</td>");
                }

            }
            Response.Write("</tr>");
        }

        
        Response.Write("</table>");


        Response.End();
        Response.Clear();
    }


    public void Carding_Daily_Production()
    {
        DataTable AutoDT = new DataTable();
        DataTable dt = new DataTable();

        if (FromDate != "")
        {
            frmDate = Convert.ToDateTime(FromDate);
        }
        if (ToDate != "")
        {
            toDate = Convert.ToDateTime(ToDate);
        }
        if (RptName == "Carding Shiftwise Details Report")
        {
            SSQL = "Select '' as CompanyName,'' as Address1,'' as Address2,Trans_Date,Unit,MachineName,'' as Hank,'' as Speed,'' as StdProd,";
            SSQL = SSQL + "isnull([shift I],0) as [shift I],";
            SSQL = SSQL + "isnull([shift II],0) as [shift II],isnull([shift III],0) as[shift III],";
            SSQL = SSQL + "(isnull([shift I],0)+isnull([shift II],0)+isnull([shift III],0)) as Total,";
            SSQL = SSQL + "'' as Utilization,'' as Waste,'' as Breakage";
            SSQL = SSQL + " from (Select CM.Trans_Date,CM.Unit,CM.Shift,CS.MachineName,";
            SSQL = SSQL + "isnull(sum(CONVERT(Decimal(18,2),CS.ActProduction)),0) as ActProduction";
            SSQL = SSQL + " from Carding_Main CM inner join Carding_Main_Sub CS on CM.Trans_No = CS.Trans_No ";
            SSQL = SSQL + " Where CM.Ccode='" + SessionCcode + "' And CM.Lcode='" + SessionLcode + "'";
            SSQL = SSQL + " And CM.FinYearCode='" + SessionFinYearCode + "'";
            SSQL = SSQL + " And CS.Ccode='" + SessionCcode + "' And CS.Lcode='" + SessionLcode + "'";
            SSQL = SSQL + " And CS.FinYearCode='" + SessionFinYearCode + "'";
            if (FromDate != "")
            {
                SSQL = SSQL + "And CONVERT(DATETIME,CM.Trans_Date, 103)=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";
                SSQL = SSQL + "And CONVERT(DATETIME,CS.Trans_Date, 103)=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";
                //SSQL = SSQL + "And CONVERT(DATETIME,CS.Trans_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";
                //SSQL = SSQL + "And CONVERT(DATETIME,CS.Trans_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
            }
            if (MachineName != "")
            {
                SSQL = SSQL + " And CS.MachineCode='" + MachineName + "'";
            }
            if (ColorName != "")
            {
                SSQL = SSQL + " And CM.Color='" + ColorName + "'";
            }
            if (LotNo != "")
            {
                SSQL = SSQL + " And CM.LotNo='" + LotNo + "'";
            }
            SSQL = SSQL + " Group by CM.Trans_Date,CM.Unit,CM.Shift,CS.MachineName";
            SSQL = SSQL + ")src pivot(";
            SSQL = SSQL + " max(ActProduction) for Shift in ([shift I], [shift II], [shift III])) as MaxBookingDays order by Trans_Date asc";
            AutoDT = objdata.RptEmployeeMultipleDetails(SSQL);




            if (AutoDT.Rows.Count != 0)
            {
                for (int i = 0; i < AutoDT.Rows.Count; i++)
                {
                    SSQL = "Select CS.Speed,CS.Hank,CS.StdProduction,isnull(sum(CONVERT(Decimal(18,2),CS.Utilization)),0) as Utilization,";
                    SSQL = SSQL + "(isnull(sum(CONVERT(Decimal(18,2),CS.Silver_Wastage)),0)+isnull(sum(CONVERT(Decimal(18,2),CS.FS)),0)) as Waste,";
                    SSQL = SSQL + "isnull(sum(CONVERT(Decimal(18,2),CS.Stop_Page_Mintus)),0) as Stop_Page_Mintus";
                    SSQL = SSQL + " from Carding_Main CM inner join Carding_Main_Sub CS on CM.Trans_No = CS.Trans_No";
                    SSQL = SSQL + " Where CM.Ccode='" + SessionCcode + "' And CM.Lcode='" + SessionLcode + "'";
                    SSQL = SSQL + " And CM.FinYearCode='" + SessionFinYearCode + "'";
                    SSQL = SSQL + " And CS.Ccode='" + SessionCcode + "' And CS.Lcode='" + SessionLcode + "'";
                    SSQL = SSQL + " And CS.FinYearCode='" + SessionFinYearCode + "'";
                    SSQL = SSQL + " And CM.Trans_Date='" + AutoDT.Rows[i]["Trans_Date"].ToString() + "'";
                    SSQL = SSQL + " And CS.Trans_Date='" + AutoDT.Rows[i]["Trans_Date"].ToString() + "'";
                    SSQL = SSQL + " And CM.Unit='" + AutoDT.Rows[i]["Unit"].ToString() + "'";
                    SSQL = SSQL + " And CS.MachineName='" + AutoDT.Rows[i]["MachineName"].ToString() + "'";
                    SSQL = SSQL + " Group by CS.Speed,CS.Hank,CS.StdProduction";
                    dt = objdata.RptEmployeeMultipleDetails(SSQL);

                    if (dt.Rows.Count != 0)
                    {
                        AutoDT.Rows[i]["Speed"] = dt.Rows[0]["Speed"].ToString();
                        AutoDT.Rows[i]["Hank"] = dt.Rows[0]["Hank"].ToString();
                        AutoDT.Rows[i]["StdProd"] = dt.Rows[0]["StdProduction"].ToString();
                        AutoDT.Rows[i]["Waste"] = dt.Rows[0]["Waste"].ToString();
                        AutoDT.Rows[i]["Breakage"] = dt.Rows[0]["Stop_Page_Mintus"].ToString();
                        AutoDT.Rows[i]["Utilization"] = dt.Rows[0]["Utilization"].ToString();

                    }


                    DataTable dtdCompanyAddress = new DataTable();
                    SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                    dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);
                    if (dtdCompanyAddress.Rows.Count != 0)
                    {
                        AutoDT.Rows[i]["CompanyName"] = "" + dtdCompanyAddress.Rows[0]["Cname"].ToString() + " - " + SessionLcode + "";
                        AutoDT.Rows[i]["Address1"] = "" + dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString() + "";
                        AutoDT.Rows[i]["Address2"] = "" + dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString() + "";
                    }
                }

                DataSet ds1 = new DataSet();
                ds1.Tables.Add(AutoDT);

                RD_Report.Load(Server.MapPath("~/crystal/CardingDailyProductionShiftwise.rpt"));
                RD_Report.SetDataSource(ds1.Tables[0]);
                //Company Details Add

                if (FromDate != "")
                {
                    RD_Report.DataDefinition.FormulaFields["FromDate"].Text = "'" + FromDate.ToString() + "'";
                }

                RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
                CrystalReportViewer1.ReportSource = RD_Report;
                CrystalReportViewer1.RefreshReport();
                CrystalReportViewer1.DataBind();

            }
            else
            {
                Errflag = false;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
            }
        }
        if (RptName == "Carding Abstract Details Report")
        {
            SSQL = "Select '' as CompanyName,'' as Address1,'' as Address2,CM.Unit,";
            SSQL = SSQL + "isnull(sum(CONVERT(Decimal(18,2),CS.ActProduction)),0) as UpToDateProd,";
            SSQL = SSQL + "isnull(sum(CONVERT(Decimal(18,2),CS.Stop_Page_Mintus)),0) as UpToDateBreak,";
            SSQL = SSQL + "'' as OnDateProd,'' as OnDateBreak";
            SSQL = SSQL + " from Carding_Main CM inner join Carding_Main_Sub CS on CM.Trans_No = CS.Trans_No ";
            SSQL = SSQL + " Where CM.Ccode='" + SessionCcode + "' And CM.Lcode='" + SessionLcode + "'";
            SSQL = SSQL + " And CM.FinYearCode='" + SessionFinYearCode + "'";
            SSQL = SSQL + " And CS.Ccode='" + SessionCcode + "' And CS.Lcode='" + SessionLcode + "'";
            SSQL = SSQL + " And CS.FinYearCode='" + SessionFinYearCode + "'";
            if (FromDate != "")
            {
                SSQL = SSQL + "And CONVERT(DATETIME,CM.Trans_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";
                SSQL = SSQL + "And CONVERT(DATETIME,CM.Trans_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
                SSQL = SSQL + "And CONVERT(DATETIME,CS.Trans_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";
                SSQL = SSQL + "And CONVERT(DATETIME,CS.Trans_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
            }
            if (MachineName != "")
            {
                SSQL = SSQL + " And CS.MachineCode='" + MachineName + "'";
            }
            if (ColorName != "")
            {
                SSQL = SSQL + " And CM.Color='" + ColorName + "'";
            }
            if (LotNo != "")
            {
                SSQL = SSQL + " And CM.LotNo='" + LotNo + "'";
            }
            SSQL = SSQL + " Group by CM.Unit";
            AutoDT = objdata.RptEmployeeMultipleDetails(SSQL);




            if (AutoDT.Rows.Count != 0)
            {
                for (int i = 0; i < AutoDT.Rows.Count; i++)
                {
                    SSQL = "Select isnull(sum(CONVERT(Decimal(18,2),CS.ActProduction)),0) as OnDateProd,";
                    SSQL = SSQL + "isnull(sum(CONVERT(Decimal(18,2),CS.Stop_Page_Mintus)),0) as OnDateBreak";
                    SSQL = SSQL + " from Carding_Main CM inner join Carding_Main_Sub CS on CM.Trans_No = CS.Trans_No ";
                    SSQL = SSQL + " Where CM.Ccode='" + SessionCcode + "' And CM.Lcode='" + SessionLcode + "'";
                    SSQL = SSQL + " And CM.FinYearCode='" + SessionFinYearCode + "'";
                    SSQL = SSQL + " And CS.Ccode='" + SessionCcode + "' And CS.Lcode='" + SessionLcode + "'";
                    SSQL = SSQL + " And CS.FinYearCode='" + SessionFinYearCode + "'";
                    if (FromDate != "")
                    {
                        SSQL = SSQL + "And CONVERT(DATETIME,CM.Trans_Date, 103)=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
                        SSQL = SSQL + "And CONVERT(DATETIME,CS.Trans_Date, 103)=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
                        //SSQL = SSQL + "And CONVERT(DATETIME,CS.Trans_Date, 103)=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";
                        //SSQL = SSQL + "And CONVERT(DATETIME,CS.Trans_Date, 103)=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
                    }
                    SSQL = SSQL + " And CM.Unit='" + AutoDT.Rows[i]["Unit"].ToString() + "'";
                    //SSQL = SSQL + " Group by CS.Speed,CS.Hank,CS.StdProduction";
                    dt = objdata.RptEmployeeMultipleDetails(SSQL);

                    if (dt.Rows.Count != 0)
                    {
                        AutoDT.Rows[i]["OnDateProd"] = dt.Rows[0]["OnDateProd"].ToString();
                        AutoDT.Rows[i]["OnDateBreak"] = dt.Rows[0]["OnDateBreak"].ToString();
                    }


                    DataTable dtdCompanyAddress = new DataTable();
                    SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                    dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);
                    if (dtdCompanyAddress.Rows.Count != 0)
                    {
                        AutoDT.Rows[i]["CompanyName"] = "" + dtdCompanyAddress.Rows[0]["Cname"].ToString() + " - " + SessionLcode + "";
                        AutoDT.Rows[i]["Address1"] = "" + dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString() + "";
                        AutoDT.Rows[i]["Address2"] = "" + dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString() + "";
                    }
                }

                DataSet ds1 = new DataSet();
                ds1.Tables.Add(AutoDT);

                RD_Report.Load(Server.MapPath("~/crystal/CardingProductionAbstract.rpt"));
                RD_Report.SetDataSource(ds1.Tables[0]);
                //Company Details Add

                if (ToDate != "")
                {
                    RD_Report.DataDefinition.FormulaFields["FromDate"].Text = "'" + ToDate.ToString() + "'";
                }

                RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
                CrystalReportViewer1.ReportSource = RD_Report;
                CrystalReportViewer1.RefreshReport();
                CrystalReportViewer1.DataBind();

            }
            else
            {
                Errflag = false;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
            }
        }
        if (RptName == "Carding Monthly Details Report")
        {

            SSQL = "Select '' as CompanyName,'' as Address1,'' as Address2,CM.Unit,";
            SSQL = SSQL + "isnull(sum(CONVERT(Decimal(18,2),CS.ActProduction)),0) as UpToDateProd,";
            SSQL = SSQL + "isnull(sum(CONVERT(Decimal(18,2),CS.Stop_Page_Mintus)),0) as UpToDateBreak,";
            SSQL = SSQL + "'' as OnDateProd,'' as OnDateBreak";
            SSQL = SSQL + " from Carding_Main CM inner join Carding_Main_Sub CS on CM.Trans_No = CS.Trans_No ";
            SSQL = SSQL + " Where CM.Ccode='" + SessionCcode + "' And CM.Lcode='" + SessionLcode + "'";
            SSQL = SSQL + " And CM.FinYearCode='" + SessionFinYearCode + "'";
            SSQL = SSQL + " And CS.Ccode='" + SessionCcode + "' And CS.Lcode='" + SessionLcode + "'";
            SSQL = SSQL + " And CS.FinYearCode='" + SessionFinYearCode + "'";
            if (FromDate != "" && ToDate != "")
            {
                SSQL = SSQL + "And CONVERT(DATETIME,CM.Trans_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";
                SSQL = SSQL + "And CONVERT(DATETIME,CM.Trans_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
                SSQL = SSQL + "And CONVERT(DATETIME,CS.Trans_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";
                SSQL = SSQL + "And CONVERT(DATETIME,CS.Trans_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
            }
            if (MachineName != "")
            {
                SSQL = SSQL + " And CS.MachineCode='" + MachineName + "'";
            }
            if (ColorName != "")
            {
                SSQL = SSQL + " And CM.Color='" + ColorName + "'";
            }
            if (LotNo != "")
            {
                SSQL = SSQL + " And CM.LotNo='" + LotNo + "'";
            }
            SSQL = SSQL + " Group by CM.Unit";
            AutoDT = objdata.RptEmployeeMultipleDetails(SSQL);




            if (AutoDT.Rows.Count != 0)
            {
                for (int i = 0; i < AutoDT.Rows.Count; i++)
                {
                    //SSQL = "Select isnull(sum(CONVERT(Decimal(18,2),CS.ActProduction)),0) as OnDateProd,";
                    //SSQL = SSQL + "isnull(sum(CONVERT(Decimal(18,2),CS.Stop_Page_Mintus)),0) as OnDateBreak";
                    //SSQL = SSQL + " from Carding_Main CM inner join Carding_Main_Sub CS on CM.Trans_No = CS.Trans_No ";
                    //SSQL = SSQL + " Where CM.Ccode='" + SessionCcode + "' And CM.Lcode='" + SessionLcode + "'";
                    //SSQL = SSQL + " And CM.FinYearCode='" + SessionFinYearCode + "'";
                    //SSQL = SSQL + " And CS.Ccode='" + SessionCcode + "' And CS.Lcode='" + SessionLcode + "'";
                    //SSQL = SSQL + " And CS.FinYearCode='" + SessionFinYearCode + "'";
                    //if (FromDate != "" && ToDate != "")
                    //{
                    //    SSQL = SSQL + "And CONVERT(DATETIME,CM.Trans_Date, 103)=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
                    //    SSQL = SSQL + "And CONVERT(DATETIME,CS.Trans_Date, 103)=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
                    //    SSQL = SSQL + "And CONVERT(DATETIME,CS.Trans_Date, 103)=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";
                    //    SSQL = SSQL + "And CONVERT(DATETIME,CS.Trans_Date, 103)=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
                    //}
                    //SSQL = SSQL + " And CM.Unit='" + AutoDT.Rows[i]["Unit"].ToString() + "'";
                    ////SSQL = SSQL + " Group by CS.Speed,CS.Hank,CS.StdProduction";
                    //dt = objdata.RptEmployeeMultipleDetails(SSQL);

                    //if (dt.Rows.Count != 0)
                    //{
                    //    AutoDT.Rows[i]["OnDateProd"] = dt.Rows[0]["OnDateProd"].ToString();
                    //    AutoDT.Rows[i]["OnDateBreak"] = dt.Rows[0]["OnDateBreak"].ToString();
                    //}


                    DataTable dtdCompanyAddress = new DataTable();
                    SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                    dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);
                    if (dtdCompanyAddress.Rows.Count != 0)
                    {
                        AutoDT.Rows[i]["CompanyName"] = "" + dtdCompanyAddress.Rows[0]["Cname"].ToString() + " - " + SessionLcode + "";
                        AutoDT.Rows[i]["Address1"] = "" + dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString() + "";
                        AutoDT.Rows[i]["Address2"] = "" + dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString() + "";
                    }
                }

                DataSet ds1 = new DataSet();
                ds1.Tables.Add(AutoDT);

                RD_Report.Load(Server.MapPath("~/crystal/CardingProductionMonthly.rpt"));
                RD_Report.SetDataSource(ds1.Tables[0]);
                //Company Details Add

                if (ToDate != "")
                {
                    RD_Report.DataDefinition.FormulaFields["FromDate"].Text = "'" + FromDate.ToString() + "'";
                    RD_Report.DataDefinition.FormulaFields["ToDate"].Text = "'" + ToDate.ToString() + "'";
                }

                RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
                CrystalReportViewer1.ReportSource = RD_Report;
                CrystalReportViewer1.RefreshReport();
                CrystalReportViewer1.DataBind();

            }
            else
            {
                Errflag = false;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
            }

        }
        if (!Errflag)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
            lblUploadSuccessfully.Text = "No Records Matched..";
        }

    }

    //public void Carding_Daily_Production()
    //{
    //    DataTable AutoDT = new DataTable();
    //    DataTable StdHank_DT = new DataTable();
    //    DataTable ActHank_DT = new DataTable();
    //    DataTable StdProd_DT = new DataTable();
    //    DataTable ActProd_DT = new DataTable();
    //    DataTable Eff_DT = new DataTable();

    //    //DataTable AutoDT = new DataTable();
    //    AutoDT.Columns.Add("CompanyName");
    //    AutoDT.Columns.Add("Address1");
    //    AutoDT.Columns.Add("Address2");
    //    AutoDT.Columns.Add("ProductionDate");
    //    AutoDT.Columns.Add("MachineName");
    //    AutoDT.Columns.Add("SI_StdProd");
    //    AutoDT.Columns.Add("SI_ActProd");
    //    AutoDT.Columns.Add("SI_Eff");
    //    AutoDT.Columns.Add("SII_StdProd");
    //    AutoDT.Columns.Add("SII_ActProd");
    //    AutoDT.Columns.Add("SII_Eff");
    //    AutoDT.Columns.Add("SIII_StdProd");
    //    AutoDT.Columns.Add("SIII_ActProd");
    //    AutoDT.Columns.Add("SIII_Eff");
        



    //    DataTable RingSPGDT = new DataTable();
    //    if (FromDate != "")
    //    {
    //        frmDate = Convert.ToDateTime(FromDate);
    //    }

        
    //        SSQL = "Select *from (Select CM.Trans_Date,CM.Shift,CS.MachineName,CS.StdProduction";
    //        SSQL = SSQL + " from Carding_Main CM inner join Carding_Main_Sub CS";
    //        SSQL = SSQL + " on CM.Trans_No = CS.Trans_No Where CM.Ccode='" + SessionCcode + "' And CM.Lcode='" + SessionLcode + "'";
    //        SSQL = SSQL + " And CM.FinYearCode='" + SessionFinYearCode + "'";
    //        if (FromDate != "")
    //        {
    //            SSQL = SSQL + "And CONVERT(DATETIME,CM.Trans_Date, 103)=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";
    //        }
    //        SSQL = SSQL + ")src pivot(";
    //        SSQL = SSQL + " max(StdProduction) for Shift in ([shift I], [shift II], [shift III])) as MaxBookingDays ";
        
    //    StdProd_DT = objdata.RptEmployeeMultipleDetails(SSQL);


    //    if (StdProd_DT.Rows.Count != 0)
    //    {
    //        for (int i = 0; i < StdProd_DT.Rows.Count; i++)
    //        {
    //            AutoDT.NewRow();
    //            AutoDT.Rows.Add();


    //            AutoDT.Rows[AutoDT.Rows.Count - 1]["ProductionDate"] = StdProd_DT.Rows[i]["Trans_Date"].ToString();
    //            AutoDT.Rows[AutoDT.Rows.Count - 1]["MachineName"] = StdProd_DT.Rows[i]["MachineName"].ToString();
    //            AutoDT.Rows[AutoDT.Rows.Count - 1]["SI_StdProd"] = StdProd_DT.Rows[i]["shift I"].ToString();
    //            AutoDT.Rows[AutoDT.Rows.Count - 1]["SII_StdProd"] = StdProd_DT.Rows[i]["shift II"].ToString();
    //            AutoDT.Rows[AutoDT.Rows.Count - 1]["SIII_StdProd"] = StdProd_DT.Rows[i]["shift III"].ToString();

    //            //if (DeptName == "Carding Production")
    //            //{
    //                SSQL = "Select *from (Select CM.Trans_Date,CM.Shift,CS.MachineName,CS.ActProduction";
    //                SSQL = SSQL + " from Carding_Main CM inner join Carding_Main_Sub CS";
    //                SSQL = SSQL + " on CM.Trans_No = CS.Trans_No Where CM.Ccode='" + SessionCcode + "' And CM.Lcode='" + SessionLcode + "'";
    //                SSQL = SSQL + " And CM.FinYearCode='" + SessionFinYearCode + "'";
    //                if (FromDate != "")
    //                {
    //                    SSQL = SSQL + "And CONVERT(DATETIME,CM.Trans_Date, 103)=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";
    //                }
    //                SSQL = SSQL + ")src pivot(";
    //                SSQL = SSQL + " max(ActProduction) for Shift in ([shift I], [shift II], [shift III])) as MaxBookingDays ";
    //            //}

    //            ActProd_DT = objdata.RptEmployeeMultipleDetails(SSQL);
    //            if (ActProd_DT.Rows.Count != 0)
    //            {
    //                AutoDT.Rows[AutoDT.Rows.Count - 1]["SI_ActProd"] = ActProd_DT.Rows[i]["shift I"].ToString();
    //                AutoDT.Rows[AutoDT.Rows.Count - 1]["SII_ActProd"] = ActProd_DT.Rows[i]["shift II"].ToString();
    //                AutoDT.Rows[AutoDT.Rows.Count - 1]["SIII_ActProd"] = ActProd_DT.Rows[i]["shift III"].ToString();

    //            }

    //            SSQL = "Select *from (Select CM.Trans_Date,CM.Shift,CS.MachineName,CS.Efficiency";
    //            SSQL = SSQL + " from Carding_Main CM inner join Carding_Main_Sub CS";
    //            SSQL = SSQL + " on CM.Trans_No = CS.Trans_No Where CM.Ccode='" + SessionCcode + "' And CM.Lcode='" + SessionLcode + "'";
    //            SSQL = SSQL + " And CM.FinYearCode='" + SessionFinYearCode + "'";
    //            if (FromDate != "")
    //            {
    //                SSQL = SSQL + "And CONVERT(DATETIME,CM.Trans_Date, 103)=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";
    //            }
    //            SSQL = SSQL + ")src pivot(";
    //            SSQL = SSQL + " max(Efficiency) for Shift in ([shift I], [shift II], [shift III])) as MaxBookingDays ";
                
    //            Eff_DT = objdata.RptEmployeeMultipleDetails(SSQL);
    //            if (Eff_DT.Rows.Count != 0)
    //            {
    //                AutoDT.Rows[AutoDT.Rows.Count - 1]["SI_Eff"] = Eff_DT.Rows[i]["shift I"].ToString();
    //                AutoDT.Rows[AutoDT.Rows.Count - 1]["SII_Eff"] = Eff_DT.Rows[i]["shift II"].ToString();
    //                AutoDT.Rows[AutoDT.Rows.Count - 1]["SIII_Eff"] = Eff_DT.Rows[i]["shift III"].ToString();

    //            }

    //        }
    //    }

    //    if (AutoDT.Rows.Count != 0)
    //    {
    //        DataTable dtdCompanyAddress = new DataTable();
    //        DataTable Close_Det = new DataTable();
    //        SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
    //        dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);
    //        if (dtdCompanyAddress.Rows.Count != 0)
    //        {
    //            for (int i = 0; i < AutoDT.Rows.Count; i++)
    //            {
    //                AutoDT.Rows[i]["CompanyName"] = "" + dtdCompanyAddress.Rows[0]["Cname"].ToString() + " - " + SessionLcode + "";
    //                AutoDT.Rows[i]["Address1"] = "" + dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString() + "";
    //                AutoDT.Rows[i]["Address2"] = "" + dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString() + "";
    //            }
    //        }


    //        DataSet ds1 = new DataSet();
    //        ds1.Tables.Add(AutoDT);
           
    //            RD_Report.Load(Server.MapPath("~/crystal/CardingDailyProductionDetails.rpt"));
            
            

    //        RD_Report.SetDataSource(ds1.Tables[0]);
    //        //Company Details Add

    //        //if (FromDate != "")
    //        //{
    //        //    RD_Report.DataDefinition.FormulaFields["FromDate"].Text = "'" + FromDate.ToString() + "'";
    //        //}
    //        //if (ToDate != "")
    //        //{
    //        //    RD_Report.DataDefinition.FormulaFields["ToDate"].Text = "'" + ToDate.ToString() + "'";
    //        //}

    //        RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
    //        CrystalReportViewer1.ReportSource = RD_Report;
    //        CrystalReportViewer1.RefreshReport();
    //        CrystalReportViewer1.DataBind();
    //    }
    //    else
    //    {
    //        Errflag = false;
    //        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
    //    }
    //    if (!Errflag)
    //    {
    //        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
    //        lblUploadSuccessfully.Text = "No Records Matched..";
    //    }
    //}

    public void Drawing_Daily_Production()
    {
        DataTable AutoDT = new DataTable();
        DataTable StdHank_DT = new DataTable();
        DataTable ActHank_DT = new DataTable();
        DataTable StdProd_DT = new DataTable();
        DataTable ActProd_DT = new DataTable();
        DataTable Eff_DT = new DataTable();

        //DataTable AutoDT = new DataTable();
        AutoDT.Columns.Add("CompanyName");
        AutoDT.Columns.Add("Address1");
        AutoDT.Columns.Add("Address2");
        AutoDT.Columns.Add("ProductionDate");
        AutoDT.Columns.Add("MachineName");
        AutoDT.Columns.Add("SI_StdProd");
        AutoDT.Columns.Add("SI_ActProd");
        AutoDT.Columns.Add("SI_Eff");
        AutoDT.Columns.Add("SII_StdProd");
        AutoDT.Columns.Add("SII_ActProd");
        AutoDT.Columns.Add("SII_Eff");
        AutoDT.Columns.Add("SIII_StdProd");
        AutoDT.Columns.Add("SIII_ActProd");
        AutoDT.Columns.Add("SIII_Eff");


        DataTable RingSPGDT = new DataTable();
        if (FromDate != "")
        {
            frmDate = Convert.ToDateTime(FromDate);
        }

        if (DeptName == "Drawing Production")
        {
            SSQL = "Select *from (Select CM.Trans_Date,CM.Shift,CS.MachineName,CS.StdProduction";
            SSQL = SSQL + " from Drawing_Main CM inner join Drawing_Main_Sub CS";
            SSQL = SSQL + " on CM.Trans_No = CS.Trans_No Where CM.Ccode='" + SessionCcode + "' And CM.Lcode='" + SessionLcode + "'";
            SSQL = SSQL + " And CM.FinYearCode='" + SessionFinYearCode + "'";
            if (FromDate != "")
            {
                SSQL = SSQL + "And CONVERT(DATETIME,CM.Trans_Date, 103)=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";
            }
            SSQL = SSQL + ")src pivot(";
            SSQL = SSQL + " max(StdProduction) for Shift in ([shift I], [shift II], [shift III])) as MaxBookingDays ";
        }
        StdProd_DT = objdata.RptEmployeeMultipleDetails(SSQL);


        if (StdProd_DT.Rows.Count != 0)
        {
            for (int i = 0; i < StdProd_DT.Rows.Count; i++)
            {
                AutoDT.NewRow();
                AutoDT.Rows.Add();


                AutoDT.Rows[AutoDT.Rows.Count - 1]["ProductionDate"] = StdProd_DT.Rows[i]["Trans_Date"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["MachineName"] = StdProd_DT.Rows[i]["MachineName"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["SI_StdProd"] = StdProd_DT.Rows[i]["shift I"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["SII_StdProd"] = StdProd_DT.Rows[i]["shift II"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["SIII_StdProd"] = StdProd_DT.Rows[i]["shift III"].ToString();

                if (DeptName == "Drawing Production")
                {
                    SSQL = "Select *from (Select CM.Trans_Date,CM.Shift,CS.MachineName,CS.ActProduction";
                    SSQL = SSQL + " from Drawing_Main CM inner join Drawing_Main_Sub CS";
                    SSQL = SSQL + " on CM.Trans_No = CS.Trans_No Where CM.Ccode='" + SessionCcode + "' And CM.Lcode='" + SessionLcode + "'";
                    SSQL = SSQL + " And CM.FinYearCode='" + SessionFinYearCode + "'";
                    if (FromDate != "")
                    {
                        SSQL = SSQL + "And CONVERT(DATETIME,CM.Trans_Date, 103)=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";
                    }
                    SSQL = SSQL + ")src pivot(";
                    SSQL = SSQL + " max(ActProduction) for Shift in ([shift I], [shift II], [shift III])) as MaxBookingDays ";
                }

                ActProd_DT = objdata.RptEmployeeMultipleDetails(SSQL);
                if (ActProd_DT.Rows.Count != 0)
                {
                    AutoDT.Rows[AutoDT.Rows.Count - 1]["SI_ActProd"] = ActProd_DT.Rows[i]["shift I"].ToString();
                    AutoDT.Rows[AutoDT.Rows.Count - 1]["SII_ActProd"] = ActProd_DT.Rows[i]["shift II"].ToString();
                    AutoDT.Rows[AutoDT.Rows.Count - 1]["SIII_ActProd"] = ActProd_DT.Rows[i]["shift III"].ToString();

                }

                SSQL = "Select *from (Select CM.Trans_Date,CM.Shift,CS.MachineName,CS.Efficiency";
                SSQL = SSQL + " from Drawing_Main CM inner join Drawing_Main_Sub CS";
                SSQL = SSQL + " on CM.Trans_No = CS.Trans_No Where CM.Ccode='" + SessionCcode + "' And CM.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And CM.FinYearCode='" + SessionFinYearCode + "'";
                if (FromDate != "")
                {
                    SSQL = SSQL + "And CONVERT(DATETIME,CM.Trans_Date, 103)=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";
                }
                SSQL = SSQL + ")src pivot(";
                SSQL = SSQL + " max(Efficiency) for Shift in ([shift I], [shift II], [shift III])) as MaxBookingDays ";

                Eff_DT = objdata.RptEmployeeMultipleDetails(SSQL);
                if (Eff_DT.Rows.Count != 0)
                {
                    AutoDT.Rows[AutoDT.Rows.Count - 1]["SI_Eff"] = Eff_DT.Rows[i]["shift I"].ToString();
                    AutoDT.Rows[AutoDT.Rows.Count - 1]["SII_Eff"] = Eff_DT.Rows[i]["shift II"].ToString();
                    AutoDT.Rows[AutoDT.Rows.Count - 1]["SIII_Eff"] = Eff_DT.Rows[i]["shift III"].ToString();

                }

            }
        }

        if (AutoDT.Rows.Count != 0)
        {
            DataTable dtdCompanyAddress = new DataTable();
            DataTable Close_Det = new DataTable();
            SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
            dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dtdCompanyAddress.Rows.Count != 0)
            {
                for (int i = 0; i < AutoDT.Rows.Count; i++)
                {
                    AutoDT.Rows[i]["CompanyName"] = "" + dtdCompanyAddress.Rows[0]["Cname"].ToString() + " - " + SessionLcode + "";
                    AutoDT.Rows[i]["Address1"] = "" + dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString() + "";
                    AutoDT.Rows[i]["Address2"] = "" + dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString() + "";
                }
            }


            DataSet ds1 = new DataSet();
            ds1.Tables.Add(AutoDT);
            if (DeptName == "Drawing Production")
            {
                RD_Report.Load(Server.MapPath("~/crystal/DrawingDailyProductionDetails.rpt"));
            }


            RD_Report.SetDataSource(ds1.Tables[0]);
            //Company Details Add

            //if (FromDate != "")
            //{
            //    RD_Report.DataDefinition.FormulaFields["FromDate"].Text = "'" + FromDate.ToString() + "'";
            //}
            //if (ToDate != "")
            //{
            //    RD_Report.DataDefinition.FormulaFields["ToDate"].Text = "'" + ToDate.ToString() + "'";
            //}

            RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = RD_Report;
            CrystalReportViewer1.RefreshReport();
            CrystalReportViewer1.DataBind();
        }
        else
        {
            Errflag = false;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
        }
        if (!Errflag)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
            lblUploadSuccessfully.Text = "No Records Matched..";
        }
    }

    public void OE_Daily_Production()
    {
        DataTable AutoDT = new DataTable();
        DataTable StdHank_DT = new DataTable();
        DataTable ActHank_DT = new DataTable();
        DataTable StdProd_DT = new DataTable();
        DataTable ActProd_DT = new DataTable();
        DataTable Eff_DT = new DataTable();

        //DataTable AutoDT = new DataTable();
        AutoDT.Columns.Add("CompanyName");
        AutoDT.Columns.Add("Address1");
        AutoDT.Columns.Add("Address2");
        AutoDT.Columns.Add("ProductionDate");
        AutoDT.Columns.Add("MachineName");
        AutoDT.Columns.Add("SI_StdProd");
        AutoDT.Columns.Add("SI_ActProd");
        AutoDT.Columns.Add("SI_Eff");
        AutoDT.Columns.Add("SII_StdProd");
        AutoDT.Columns.Add("SII_ActProd");
        AutoDT.Columns.Add("SII_Eff");
        AutoDT.Columns.Add("SIII_StdProd");
        AutoDT.Columns.Add("SIII_ActProd");
        AutoDT.Columns.Add("SIII_Eff");


        DataTable RingSPGDT = new DataTable();
        if (FromDate != "")
        {
            frmDate = Convert.ToDateTime(FromDate);
        }

        if (DeptName == "OE Production")
        {
            SSQL = "Select *from (Select CM.Trans_Date,CM.ShiftName,CS.MachineName,CS.StdProd";
            SSQL = SSQL + " from OE_Main CM inner join OE_Sub CS";
            SSQL = SSQL + " on CM.Trans_No = CS.Trans_No Where CM.Ccode='" + SessionCcode + "' And CM.Lcode='" + SessionLcode + "'";
            SSQL = SSQL + " And CM.FinYearCode='" + SessionFinYearCode + "'";
            if (FromDate != "")
            {
                SSQL = SSQL + "And CONVERT(DATETIME,CM.Trans_Date, 103)=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";
            }
            SSQL = SSQL + ")src pivot(";
            SSQL = SSQL + " max(StdProd) for ShiftName in ([shift I], [shift II], [shift III])) as MaxBookingDays ";
        }
        StdProd_DT = objdata.RptEmployeeMultipleDetails(SSQL);


        if (StdProd_DT.Rows.Count != 0)
        {
            for (int i = 0; i < StdProd_DT.Rows.Count; i++)
            {
                AutoDT.NewRow();
                AutoDT.Rows.Add();


                AutoDT.Rows[AutoDT.Rows.Count - 1]["ProductionDate"] = StdProd_DT.Rows[i]["Trans_Date"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["MachineName"] = StdProd_DT.Rows[i]["MachineName"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["SI_StdProd"] = StdProd_DT.Rows[i]["shift I"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["SII_StdProd"] = StdProd_DT.Rows[i]["shift II"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["SIII_StdProd"] = StdProd_DT.Rows[i]["shift III"].ToString();

                if (DeptName == "OE Production")
                {
                    SSQL = "Select *from (Select CM.Trans_Date,CM.ShiftName,CS.MachineName,CS.ActProd";
                    SSQL = SSQL + " from OE_Main CM inner join OE_Sub CS";
                    SSQL = SSQL + " on CM.Trans_No = CS.Trans_No Where CM.Ccode='" + SessionCcode + "' And CM.Lcode='" + SessionLcode + "'";
                    SSQL = SSQL + " And CM.FinYearCode='" + SessionFinYearCode + "'";
                    if (FromDate != "")
                    {
                        SSQL = SSQL + "And CONVERT(DATETIME,CM.Trans_Date, 103)=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";
                    }
                    SSQL = SSQL + ")src pivot(";
                    SSQL = SSQL + " max(ActProd) for ShiftName in ([shift I], [shift II], [shift III])) as MaxBookingDays ";
                }

                ActProd_DT = objdata.RptEmployeeMultipleDetails(SSQL);
                if (ActProd_DT.Rows.Count != 0)
                {
                    AutoDT.Rows[AutoDT.Rows.Count - 1]["SI_ActProd"] = ActProd_DT.Rows[i]["shift I"].ToString();
                    AutoDT.Rows[AutoDT.Rows.Count - 1]["SII_ActProd"] = ActProd_DT.Rows[i]["shift II"].ToString();
                    AutoDT.Rows[AutoDT.Rows.Count - 1]["SIII_ActProd"] = ActProd_DT.Rows[i]["shift III"].ToString();

                }

                SSQL = "Select *from (Select CM.Trans_Date,CM.ShiftName,CS.MachineName,CS.Efficiency";
                SSQL = SSQL + " from OE_Main CM inner join OE_Sub CS";
                SSQL = SSQL + " on CM.Trans_No = CS.Trans_No Where CM.Ccode='" + SessionCcode + "' And CM.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And CM.FinYearCode='" + SessionFinYearCode + "'";
                if (FromDate != "")
                {
                    SSQL = SSQL + "And CONVERT(DATETIME,CM.Trans_Date, 103)=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";
                }
                SSQL = SSQL + ")src pivot(";
                SSQL = SSQL + " max(Efficiency) for ShiftName in ([shift I], [shift II], [shift III])) as MaxBookingDays ";

                Eff_DT = objdata.RptEmployeeMultipleDetails(SSQL);
                if (Eff_DT.Rows.Count != 0)
                {
                    AutoDT.Rows[AutoDT.Rows.Count - 1]["SI_Eff"] = Eff_DT.Rows[i]["shift I"].ToString();
                    AutoDT.Rows[AutoDT.Rows.Count - 1]["SII_Eff"] = Eff_DT.Rows[i]["shift II"].ToString();
                    AutoDT.Rows[AutoDT.Rows.Count - 1]["SIII_Eff"] = Eff_DT.Rows[i]["shift III"].ToString();

                }

            }
        }

        if (AutoDT.Rows.Count != 0)
        {
            DataTable dtdCompanyAddress = new DataTable();
            DataTable Close_Det = new DataTable();
            SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
            dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dtdCompanyAddress.Rows.Count != 0)
            {
                for (int i = 0; i < AutoDT.Rows.Count; i++)
                {
                    AutoDT.Rows[i]["CompanyName"] = "" + dtdCompanyAddress.Rows[0]["Cname"].ToString() + " - " + SessionLcode + "";
                    AutoDT.Rows[i]["Address1"] = "" + dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString() + "";
                    AutoDT.Rows[i]["Address2"] = "" + dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString() + "";
                }
            }


            DataSet ds1 = new DataSet();
            ds1.Tables.Add(AutoDT);
            if (DeptName == "OE Production")
            {
                RD_Report.Load(Server.MapPath("~/crystal/OEDailyProductionDetails.rpt"));
            }


            RD_Report.SetDataSource(ds1.Tables[0]);
            //Company Details Add

            //if (FromDate != "")
            //{
            //    RD_Report.DataDefinition.FormulaFields["FromDate"].Text = "'" + FromDate.ToString() + "'";
            //}
            //if (ToDate != "")
            //{
            //    RD_Report.DataDefinition.FormulaFields["ToDate"].Text = "'" + ToDate.ToString() + "'";
            //}

            RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = RD_Report;
            CrystalReportViewer1.RefreshReport();
            CrystalReportViewer1.DataBind();
        }
        else
        {
            Errflag = false;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
        }
        if (!Errflag)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
            lblUploadSuccessfully.Text = "No Records Matched..";
        }
    }

    //Monthly_Production_Report start
    public void Monthly_Production_Report()
    {
        DataTable AutoDT = new DataTable();
        if (FromDate != "")
        {
            frmDate = Convert.ToDateTime(FromDate);
        }

        if (ToDate != "")
        {
            toDate = Convert.ToDateTime(ToDate);
        }

        SSQL = "Select '' as CompanyName,'' as Address1,'' as Address2,";
        if (DeptName == "Carding Production")
        {
            SSQL = SSQL + "CS.MachineName,isnull(sum(CONVERT(Decimal(18,2),CS.ActProduction)),0) as ActProd,";
            SSQL = SSQL + "'' as ActHank,isnull(sum(CONVERT(Decimal(18,2),CS.StdProduction)),0) as StdProd";
            SSQL = SSQL + " from Carding_Main CM inner join Carding_Main_Sub CS on CM.Trans_No=CS.Trans_No";
            SSQL = SSQL + " Where CM.Ccode='" + SessionCcode + "' And CM.Lcode='" + SessionLcode + "' And CM.FinYearCode='" + SessionFinYearCode + "'";
            if (FromDate != "" && ToDate != "")
            {
                SSQL = SSQL + "And CONVERT(DATETIME,CM.Trans_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";
                SSQL = SSQL + "And CONVERT(DATETIME,CM.Trans_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
            }
            SSQL = SSQL + " Group by CS.MachineName";
        }
        if (DeptName == "Drawing Production")
        {
            SSQL = SSQL + "CS.MachineName,isnull(sum(CONVERT(Decimal(18,2),CS.ActProduction)),0) as ActProd,";
            SSQL = SSQL + "'' as ActHank,isnull(sum(CONVERT(Decimal(18,2),CS.StdProduction)),0) as StdProd";
            SSQL = SSQL + " from Drawing_Main CM inner join Drawing_Main_Sub CS on CM.Trans_No=CS.Trans_No";
            SSQL = SSQL + " Where CM.Ccode='" + SessionCcode + "' And CM.Lcode='" + SessionLcode + "' And CM.FinYearCode='" + SessionFinYearCode + "'";
            if (FromDate != "" && ToDate != "")
            {
                SSQL = SSQL + "And CONVERT(DATETIME,CM.Trans_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";
                SSQL = SSQL + "And CONVERT(DATETIME,CM.Trans_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
            }
            SSQL = SSQL + " Group by CS.MachineName";
        }
        if (DeptName == "OE Production")
        {
            SSQL = SSQL + "CS.MachineName,isnull(sum(CONVERT(Decimal(18,2),CS.ActProd)),0) as ActProd,";
            SSQL = SSQL + "'' as ActHank,isnull(sum(CONVERT(Decimal(18,2),CS.StdProd)),0) as StdProd";
            SSQL = SSQL + " from OE_Main CM inner join OE_Sub CS on CM.Trans_No=CS.Trans_No";
            SSQL = SSQL + " Where CM.Ccode='" + SessionCcode + "' And CM.Lcode='" + SessionLcode + "' And CM.FinYearCode='" + SessionFinYearCode + "'";
            if (FromDate != "" && ToDate != "")
            {
                SSQL = SSQL + "And CONVERT(DATETIME,CM.Trans_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";
                SSQL = SSQL + "And CONVERT(DATETIME,CM.Trans_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
            }
            SSQL = SSQL + " Group by CS.MachineName";
        }
        AutoDT = objdata.RptEmployeeMultipleDetails(SSQL);

        if (AutoDT.Rows.Count != 0)
        {
            DataTable dtdCompanyAddress = new DataTable();
            DataTable Close_Det = new DataTable();
            SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
            dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dtdCompanyAddress.Rows.Count != 0)
            {
                for (int i = 0; i < AutoDT.Rows.Count; i++)
                {
                    AutoDT.Rows[i]["CompanyName"] = "" + dtdCompanyAddress.Rows[0]["Cname"].ToString() + " - " + SessionLcode + "";
                    AutoDT.Rows[i]["Address1"] = "" + dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString() + "";
                    AutoDT.Rows[i]["Address2"] = "" + dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString() + "";
                }
            }


            DataSet ds1 = new DataSet();
            ds1.Tables.Add(AutoDT);
            if (DeptName == "Carding Production" || DeptName == "Drawing Production" || DeptName == "OE Production")
            {
                RD_Report.Load(Server.MapPath("~/crystal/MontlyAllProductionReportCarding.rpt"));
            }
            RD_Report.SetDataSource(ds1.Tables[0]);
            //Company Details Add

            if (DeptName != "")
            {
                RD_Report.DataDefinition.FormulaFields["DeptName"].Text = "'" + DeptName.ToString() + "'";
            }

            if (FromDate != "")
            {
                RD_Report.DataDefinition.FormulaFields["FromDate"].Text = "'" + FromDate.ToString() + "'";
            }
            if (ToDate != "")
            {
                RD_Report.DataDefinition.FormulaFields["ToDate"].Text = "'" + ToDate.ToString() + "'";
            }

            RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = RD_Report;
            CrystalReportViewer1.RefreshReport();
            CrystalReportViewer1.DataBind();
        }
        else
        {
            Errflag = false;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
        }
        if (!Errflag)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
            lblUploadSuccessfully.Text = "No Records Matched..";
        }
    }
    //Monthly_Production_Report end

    //YarnProduction_GoDownwise_Report start
    public void YarnProduction_GoDownwise_Report()
    {
        if (FromDate != "")
        {
            frmDate = Convert.ToDateTime(FromDate);
        }
        if (ToDate != "")
        {
            toDate = Convert.ToDateTime(ToDate);
        }

        DataTable AutoDT = new DataTable();

        SSQL = "Select '' as CompanyName,'' as Address1,'' as Address2,";
        SSQL = SSQL + "CM.GoDownName,CS.CountName,CS.LotNo,isnull(sum(CONVERT(Decimal(18,2),CS.Qty)),0) as Qty,";
        SSQL = SSQL + "isnull(sum(CONVERT(Decimal(18,2),CS.NoofBag)),0) as NoofBag,";
        SSQL = SSQL + "isnull(sum(CONVERT(Decimal(18,2),CS.LoseKG)),0) as LoseKG";
        SSQL = SSQL + " from YarnProd_Main CM inner join YarnProd_Sub CS on CM.Trans_No=CS.Trans_No";
        SSQL = SSQL + " Where CM.Ccode='" + SessionCcode + "' And CM.Lcode='" + SessionLcode + "' And CM.FinYearCode='" + SessionFinYearCode + "'";
        if (FromDate != "" && ToDate != "")
        {
            SSQL = SSQL + "And CONVERT(DATETIME,CM.Trans_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";
            SSQL = SSQL + "And CONVERT(DATETIME,CM.Trans_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
        }
        if (WareHouseCode != "")
        {
            SSQL = SSQL + " And CM.GoDown='" + WareHouseCode + "'";
        }
        if (VarietyCode != "")
        {
            SSQL = SSQL + " And CS.CountID='" + VarietyCode + "'";
        }
        if (TransNo != "")
        {
            SSQL = SSQL + " And CM.Trans_No='" + TransNo + "'";
        }
        SSQL = SSQL + " Group by CM.GoDownName,CS.CountName,CS.LotNo";

        AutoDT = objdata.RptEmployeeMultipleDetails(SSQL);

        if (AutoDT.Rows.Count != 0)
        {
            DataTable dtdCompanyAddress = new DataTable();
            DataTable Close_Det = new DataTable();
            SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
            dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dtdCompanyAddress.Rows.Count != 0)
            {
                for (int i = 0; i < AutoDT.Rows.Count; i++)
                {
                    AutoDT.Rows[i]["CompanyName"] = "" + dtdCompanyAddress.Rows[0]["Cname"].ToString() + " - " + SessionLcode + "";
                    AutoDT.Rows[i]["Address1"] = "" + dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString() + "";
                    AutoDT.Rows[i]["Address2"] = "" + dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString() + "";
                }
            }

            DataSet ds1 = new DataSet();
            ds1.Tables.Add(AutoDT);

            RD_Report.Load(Server.MapPath("~/crystal/YarnProductionGoDownwiseReport.rpt"));
            RD_Report.SetDataSource(ds1.Tables[0]);
            //Company Details Add

            
            if (FromDate != "")
            {
                RD_Report.DataDefinition.FormulaFields["FromDate"].Text = "'" + FromDate.ToString() + "'";
            }
            if (ToDate != "")
            {
                RD_Report.DataDefinition.FormulaFields["ToDate"].Text = "'" + ToDate.ToString() + "'";
            }

            RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = RD_Report;
            CrystalReportViewer1.RefreshReport();
            CrystalReportViewer1.DataBind();
        }
        else
        {
            Errflag = false;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
        }
        if (!Errflag)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
            lblUploadSuccessfully.Text = "No Records Matched..";
        }
    }
    //YarnProduction_GoDownwise_Report end

    //YarnProduction_Detail_Report start
    public void YarnProduction_Detail_Report()
    {
        if (FromDate != "")
        {
            frmDate = Convert.ToDateTime(FromDate);
        }
        if (ToDate != "")
        {
            toDate = Convert.ToDateTime(ToDate);
        }

        DataTable AutoDT = new DataTable();

        SSQL = "Select '' as CompanyName,'' as Address1,'' as Address2,";
        SSQL = SSQL + "CM.Trans_No,CM.Trans_Date,CM.ShiftName,CM.PerpaidBy,";
        SSQL = SSQL + "CM.GoDownName,CS.CountName,CS.LotNo,CS.Qty,";
        SSQL = SSQL + "CS.NoofBag,";
        SSQL = SSQL + "CS.LoseKG,CS.Rate,CS.Amout";
        SSQL = SSQL + " from YarnProd_Main CM inner join YarnProd_Sub CS on CM.Trans_No=CS.Trans_No";
        SSQL = SSQL + " Where CM.Ccode='" + SessionCcode + "' And CM.Lcode='" + SessionLcode + "' And CM.FinYearCode='" + SessionFinYearCode + "'";
        if (FromDate != "" && ToDate != "")
        {
            SSQL = SSQL + "And CONVERT(DATETIME,CM.Trans_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";
            SSQL = SSQL + "And CONVERT(DATETIME,CM.Trans_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
        }
        if (WareHouseCode != "")
        {
            SSQL = SSQL + " And CM.GoDown='" + WareHouseCode + "'";
        }
        if (VarietyCode != "")
        {
            SSQL = SSQL + " And CS.CountID='" + VarietyCode + "'";
        }
        if (TransNo != "")
        {
            SSQL = SSQL + " And CM.Trans_No='" + TransNo + "'";
        }
        
        AutoDT = objdata.RptEmployeeMultipleDetails(SSQL);

        if (AutoDT.Rows.Count != 0)
        {
            DataTable dtdCompanyAddress = new DataTable();
            DataTable Close_Det = new DataTable();
            SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
            dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dtdCompanyAddress.Rows.Count != 0)
            {
                for (int i = 0; i < AutoDT.Rows.Count; i++)
                {
                    AutoDT.Rows[i]["CompanyName"] = "" + dtdCompanyAddress.Rows[0]["Cname"].ToString() + " - " + SessionLcode + "";
                    AutoDT.Rows[i]["Address1"] = "" + dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString() + "";
                    AutoDT.Rows[i]["Address2"] = "" + dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString() + "";
                }
            }

            DataSet ds1 = new DataSet();
            ds1.Tables.Add(AutoDT);

            RD_Report.Load(Server.MapPath("~/crystal/YarnProductionDetailReport.rpt"));
            RD_Report.SetDataSource(ds1.Tables[0]);
            //Company Details Add


            if (FromDate != "")
            {
                RD_Report.DataDefinition.FormulaFields["FromDate"].Text = "'" + FromDate.ToString() + "'";
            }
            if (ToDate != "")
            {
                RD_Report.DataDefinition.FormulaFields["ToDate"].Text = "'" + ToDate.ToString() + "'";
            }

            RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = RD_Report;
            CrystalReportViewer1.RefreshReport();
            CrystalReportViewer1.DataBind();
        }
        else
        {
            Errflag = false;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
        }
        if (!Errflag)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
            lblUploadSuccessfully.Text = "No Records Matched..";
        }
    }
    //YarnProduction_Detail_Report end

    //EBConsumption_Detail_Report start
    public void EBConsumption_Detail_Report()
    {
        if (FromDate != "")
        {
            frmDate = Convert.ToDateTime(FromDate);
        }
        if (ToDate != "")
        {
            toDate = Convert.ToDateTime(ToDate);
        }

        DataTable AutoDT = new DataTable();

        SSQL = "Select '' as CompanyName,'' as Address1,'' as Address2,";
        SSQL = SSQL + "CM.Trans_No,CM.Trans_Date,CM.Shift,CM.PreparedBy,";
        SSQL = SSQL + "CM.OtherConsume,CM.Rate,CM.Value,CM.Remarks,";
        SSQL = SSQL + "CM.TotalAmt,";
        SSQL = SSQL + "CS.MachineCode,CS.MachineName,CS.TargetUnit,CS.ActUnit,CS.LineTotal";
        SSQL = SSQL + " from EB_Consumption_Main CM inner join EB_Consumption_Main_Sub CS on CM.Trans_No=CS.Trans_No";
        SSQL = SSQL + " Where CM.Ccode='" + SessionCcode + "' And CM.Lcode='" + SessionLcode + "' And CM.FinYearCode='" + SessionFinYearCode + "'";
        if (FromDate != "" && ToDate != "")
        {
            SSQL = SSQL + "And CONVERT(DATETIME,CM.Trans_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";
            SSQL = SSQL + "And CONVERT(DATETIME,CM.Trans_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
        }
        if (MachineCode != "")
        {
            SSQL = SSQL + " And CS.MachineCode='" + MachineCode + "'";
        }
        
        if (TransNo != "")
        {
            SSQL = SSQL + " And CM.Trans_No='" + TransNo + "'";
        }

        AutoDT = objdata.RptEmployeeMultipleDetails(SSQL);

        if (AutoDT.Rows.Count != 0)
        {
            DataTable dtdCompanyAddress = new DataTable();
            DataTable Close_Det = new DataTable();
            SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
            dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dtdCompanyAddress.Rows.Count != 0)
            {
                for (int i = 0; i < AutoDT.Rows.Count; i++)
                {
                    AutoDT.Rows[i]["CompanyName"] = "" + dtdCompanyAddress.Rows[0]["Cname"].ToString() + " - " + SessionLcode + "";
                    AutoDT.Rows[i]["Address1"] = "" + dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString() + "";
                    AutoDT.Rows[i]["Address2"] = "" + dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString() + "";
                }
            }

            DataSet ds1 = new DataSet();
            ds1.Tables.Add(AutoDT);

            RD_Report.Load(Server.MapPath("~/crystal/EBConsumptionDetailReport.rpt"));
            RD_Report.SetDataSource(ds1.Tables[0]);
            //Company Details Add


            if (FromDate != "")
            {
                RD_Report.DataDefinition.FormulaFields["FromDate"].Text = "'" + FromDate.ToString() + "'";
            }
            if (ToDate != "")
            {
                RD_Report.DataDefinition.FormulaFields["ToDate"].Text = "'" + ToDate.ToString() + "'";
            }

            RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = RD_Report;
            CrystalReportViewer1.RefreshReport();
            CrystalReportViewer1.DataBind();
        }
        else
        {
            Errflag = false;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
        }
        if (!Errflag)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
            lblUploadSuccessfully.Text = "No Records Matched..";
        }
    }
    //EBConsumption_Detail_Report end

    //EBConsumption_Machinewise_Report start
    public void EBConsumption_Machinewise_Report()
    {
        DataTable AutoDT = new DataTable();
        DataTable StdHank_DT = new DataTable();
        DataTable ActHank_DT = new DataTable();
        DataTable StdProd_DT = new DataTable();
        DataTable ActProd_DT = new DataTable();
        DataTable Eff_DT = new DataTable();

        //DataTable AutoDT = new DataTable();
        AutoDT.Columns.Add("CompanyName");
        AutoDT.Columns.Add("Address1");
        AutoDT.Columns.Add("Address2");
        AutoDT.Columns.Add("MachineName");
        AutoDT.Columns.Add("OtherConsume");
        AutoDT.Columns.Add("Rate");
        AutoDT.Columns.Add("Value");
        AutoDT.Columns.Add("SI_ActUnit");
        AutoDT.Columns.Add("SI_Value");
        AutoDT.Columns.Add("SII_ActUnit");
        AutoDT.Columns.Add("SII_Value");
        AutoDT.Columns.Add("SIII_ActUnit");
        AutoDT.Columns.Add("SIII_Value");
        




        DataTable RingSPGDT = new DataTable();
        if (FromDate != "")
        {
            frmDate = Convert.ToDateTime(FromDate);
        }

        if (ToDate != "")
        {
            toDate = Convert.ToDateTime(ToDate);
        }


            SSQL = "Select *from (Select CM.Shift,CS.MachineName,CM.Rate,isnull(sum(CONVERT(Decimal(18,2),CM.OtherConsume)),0) as OtherConsume,isnull(sum(CONVERT(Decimal(18,2),CM.Value)),0) as Value,CS.ActUnit";
            SSQL = SSQL + " from EB_Consumption_Main CM inner join EB_Consumption_Main_Sub CS";
            SSQL = SSQL + " on CM.Trans_No = CS.Trans_No Where CM.Ccode='" + SessionCcode + "' And CM.Lcode='" + SessionLcode + "'";
            SSQL = SSQL + " And CM.FinYearCode='" + SessionFinYearCode + "'";
            if (FromDate != "" && ToDate!="")
            {
                SSQL = SSQL + "And CONVERT(DATETIME,CM.Trans_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";
                SSQL = SSQL + "And CONVERT(DATETIME,CM.Trans_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
            }
            if (MachineCode != "")
            {
                SSQL = SSQL + " And CS.MachineCode='" + MachineCode + "'";
            }
            SSQL = SSQL + " Group by CM.Shift,CS.MachineName,CM.Rate,CS.ActUnit)src pivot(";
            SSQL = SSQL + " max(ActUnit) for Shift in ([shift I], [shift II], [shift III])) as MaxBookingDays ";
        
            StdProd_DT = objdata.RptEmployeeMultipleDetails(SSQL);


        if (StdProd_DT.Rows.Count != 0)
        {
            for (int i = 0; i < StdProd_DT.Rows.Count; i++)
            {
                AutoDT.NewRow();
                AutoDT.Rows.Add();


                AutoDT.Rows[AutoDT.Rows.Count - 1]["MachineName"] = StdProd_DT.Rows[i]["MachineName"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["Rate"] = StdProd_DT.Rows[i]["Rate"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["OtherConsume"] = StdProd_DT.Rows[i]["OtherConsume"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["Value"] = StdProd_DT.Rows[i]["Value"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["SI_ActUnit"] = StdProd_DT.Rows[i]["shift I"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["SII_ActUnit"] = StdProd_DT.Rows[i]["shift II"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["SIII_ActUnit"] = StdProd_DT.Rows[i]["shift III"].ToString();


                SSQL = "Select *from (Select CM.Shift,CS.MachineName,CM.Rate,isnull(sum(CONVERT(Decimal(18,2),CM.OtherConsume)),0) as OtherConsume,isnull(sum(CONVERT(Decimal(18,2),CM.Value)),0) as Value,CS.LineTotal";
                SSQL = SSQL + " from EB_Consumption_Main CM inner join EB_Consumption_Main_Sub CS";
                SSQL = SSQL + " on CM.Trans_No = CS.Trans_No Where CM.Ccode='" + SessionCcode + "' And CM.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And CM.FinYearCode='" + SessionFinYearCode + "'";
                if (FromDate != "" && ToDate != "")
                {
                    SSQL = SSQL + "And CONVERT(DATETIME,CM.Trans_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";
                    SSQL = SSQL + "And CONVERT(DATETIME,CM.Trans_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
                }
                if (MachineCode != "")
                {
                    SSQL = SSQL + " And CS.MachineCode='" + MachineCode + "'";
                }
                SSQL = SSQL + " Group by CM.Shift,CS.MachineName,CM.Rate,CS.LineTotal)src pivot(";
                SSQL = SSQL + " max(LineTotal) for Shift in ([shift I], [shift II], [shift III])) as MaxBookingDays ";
               
                ActProd_DT = objdata.RptEmployeeMultipleDetails(SSQL);

                if (ActProd_DT.Rows.Count != 0)
                {
                    AutoDT.Rows[AutoDT.Rows.Count - 1]["SI_Value"] = ActProd_DT.Rows[i]["shift I"].ToString();
                    AutoDT.Rows[AutoDT.Rows.Count - 1]["SII_Value"] = ActProd_DT.Rows[i]["shift II"].ToString();
                    AutoDT.Rows[AutoDT.Rows.Count - 1]["SIII_Value"] = ActProd_DT.Rows[i]["shift III"].ToString();

                }

            }
        }

        if (AutoDT.Rows.Count != 0)
        {
            DataTable dtdCompanyAddress = new DataTable();
            DataTable Close_Det = new DataTable();
            SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
            dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dtdCompanyAddress.Rows.Count != 0)
            {
                for (int i = 0; i < AutoDT.Rows.Count; i++)
                {
                    AutoDT.Rows[i]["CompanyName"] = "" + dtdCompanyAddress.Rows[0]["Cname"].ToString() + " - " + SessionLcode + "";
                    AutoDT.Rows[i]["Address1"] = "" + dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString() + "";
                    AutoDT.Rows[i]["Address2"] = "" + dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString() + "";
                }
            }


            DataSet ds1 = new DataSet();
            ds1.Tables.Add(AutoDT);

            RD_Report.Load(Server.MapPath("~/crystal/EBConsumptionMachinewise.rpt"));
            

            RD_Report.SetDataSource(ds1.Tables[0]);
            //Company Details Add

            if (FromDate != "")
            {
                RD_Report.DataDefinition.FormulaFields["FromDate"].Text = "'" + FromDate.ToString() + "'";
            }
            if (ToDate != "")
            {
                RD_Report.DataDefinition.FormulaFields["ToDate"].Text = "'" + ToDate.ToString() + "'";
            }

            RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = RD_Report;
            CrystalReportViewer1.RefreshReport();
            CrystalReportViewer1.DataBind();
        }
        else
        {
            Errflag = false;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
        }
        if (!Errflag)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
            lblUploadSuccessfully.Text = "No Records Matched..";
        }
    }
    //EBConsumption_Machinewise_Report end

    public void Cheese_Receipt_Details_Report()
    {
        DataTable Da_Check = new DataTable();
        if (FromDate != "")
        {
            frmDate = Convert.ToDateTime(FromDate);
        }
        if (ToDate != "")
        {
            toDate = Convert.ToDateTime(ToDate);
        }

        SSQL = "select '' as CompanyName,'' as LocationName,'' as CompanyCode,'' as LocationCode,'' as Address1,'' as Address2,";
        SSQL = SSQL + "SM.Trans_No,SM.Trans_Date,SM.SuppCode,SM.SuppName,SM.RefNo,SM.RefDate,SM.Remarks,SM.TotalAmt,SM.AddOrLess,";
        SSQL = SSQL + "SM.NetAmount,SS.CheeseCode,SS.CheeseName,SS.WarehouseCode,SS.WarehouseName,SS.ZoneCode,SS.ZoneName,";
        SSQL = SSQL + "SS.Qty,SS.Rate,SS.LineTotal";
        SSQL = SSQL + " from Cheese_Main SM inner join Cheese_Main_Sub SS on SM.Trans_No=SS.Trans_No";
        SSQL = SSQL + " Where SM.Ccode='" + SessionCcode + "' And SM.Lcode='" + SessionLcode + "'";
        SSQL = SSQL + " And SM.FinYearCode='" + SessionFinYearCode + "'";
        SSQL = SSQL + " And SS.Ccode='" + SessionCcode + "' And SS.Lcode='" + SessionLcode + "'";
        SSQL = SSQL + " And SS.FinYearCode='" + SessionFinYearCode + "'";
        if (Transaction_No != "")
        {
            SSQL = SSQL + " And SM.Trans_No='" + Transaction_No + "'";
            SSQL = SSQL + " And SS.Trans_No='" + Transaction_No + "'";
        }
        if (SupplierName != "")
        {
            SSQL = SSQL + " And SM.SuppCode='" + SupplierName + "'";
        }
        if (WareHouseCode != "")
        {
            SSQL = SSQL + " And SS.WarehouseCode='" + WareHouseCode + "'";
        }
        if (VarietyCode != "")
        {
            SSQL = SSQL + " And SS.CheeseCode='" + VarietyCode + "'";
        }
        if (FromDate != "" && ToDate != "")
        {
            SSQL = SSQL + " And CONVERT(DATETIME,SM.Trans_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(DATETIME,SM.Trans_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
            SSQL = SSQL + " And CONVERT(DATETIME,SS.Trans_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(DATETIME,SS.Trans_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
        }
        Da_Check = objdata.RptEmployeeMultipleDetails(SSQL);

        if (Da_Check.Rows.Count > 0)
        {
            DataTable dtdCompanyAddress = new DataTable();
            SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
            dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);



            for (int i = 0; i < Da_Check.Rows.Count; i++)
            {
                Da_Check.Rows[i]["CompanyName"] = dtdCompanyAddress.Rows[0]["Cname"].ToString();
                Da_Check.Rows[i]["LocationName"] = dtdCompanyAddress.Rows[0]["location"].ToString();
                Da_Check.Rows[i]["CompanyCode"] = SessionCcode;
                Da_Check.Rows[i]["LocationCode"] = SessionLcode;
                Da_Check.Rows[i]["Address1"] = dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString();
                Da_Check.Rows[i]["Address2"] = dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString();
            }
             if(RptName=="Cheese Receipt Slip Report")
             {
                    if (Da_Check.Rows.Count < 10)
                    {
                        int Row_Count = Da_Check.Rows.Count;
                        for (int i = Row_Count; i < 8; i++)
                        {
                            Da_Check.NewRow();
                            Da_Check.Rows.Add();
                            Da_Check.Rows[i]["CompanyName"] = Da_Check.Rows[0]["CompanyName"].ToString();
                            Da_Check.Rows[i]["LocationName"] = Da_Check.Rows[0]["LocationName"].ToString();
                            Da_Check.Rows[i]["CompanyCode"] = Da_Check.Rows[0]["CompanyCode"];
                            Da_Check.Rows[i]["LocationCode"] = Da_Check.Rows[0]["LocationCode"];
                            Da_Check.Rows[i]["Address1"] = Da_Check.Rows[0]["Address1"].ToString();
                            Da_Check.Rows[i]["Address2"] = Da_Check.Rows[0]["Address2"].ToString();
                            Da_Check.Rows[i]["Trans_No"] = Da_Check.Rows[0]["Trans_No"];
                        }
                    }
             }

            ds.Tables.Add(Da_Check);
            ReportDocument report = new ReportDocument();
            if(RptName.ToString() == "Cheese Receipt Details Report" )
            {
            report.Load(Server.MapPath("~/crystal/CheeseReceiptDetails.rpt"));
            }
            else if(RptName=="Cheese Receipt Slip Report")
            {
                 report.Load(Server.MapPath("~/crystal/CheeseReceiptSlipDetails.rpt"));
            }
            report.SetDatabaseLogon("username", "password", "sa", "Altius2005");
            report.Database.Tables[0].SetDataSource(ds.Tables[0]);

            if (FromDate != "" && ToDate != "")
            {
                report.DataDefinition.FormulaFields["FromDate"].Text = "'" + FromDate.ToString() + "'";
                report.DataDefinition.FormulaFields["ToDate"].Text = "'" + ToDate.ToString() + "'";
            }

            report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = report;


        }
        else
        {
            Errflag = false;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
        }
        if (!Errflag)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
            lblUploadSuccessfully.Text = "No Records Matched..";
        }
    }

    public void Waste_Details_Report()
    {
        DataTable Da_Check = new DataTable();
        if (FromDate != "")
        {
            frmDate = Convert.ToDateTime(FromDate);
        }
        if (ToDate != "")
        {
            toDate = Convert.ToDateTime(ToDate);
        }

        SSQL = "select '' as CompanyName,'' as LocationName,'' as CompanyCode,'' as LocationCode,'' as Address1,'' as Address2,";
        SSQL = SSQL + "SM.Trans_No,SM.Trans_Date,SM.Shift,SM.MixingKg,SM.Remarks,";
        SSQL = SSQL + "SS.WasteDesc,SS.OPT,SS.WasteKg,SS.Reason";
        SSQL = SSQL + " from Waste_Main SM inner join Waste_Main_Sub SS on SM.Trans_No=SS.Trans_No";
        SSQL = SSQL + " Where SM.Ccode='" + SessionCcode + "' And SM.Lcode='" + SessionLcode + "'";
        SSQL = SSQL + " And SM.FinYearCode='" + SessionFinYearCode + "'";
        SSQL = SSQL + " And SS.Ccode='" + SessionCcode + "' And SS.Lcode='" + SessionLcode + "'";
        SSQL = SSQL + " And SS.FinYearCode='" + SessionFinYearCode + "'";
        if (Transaction_No != "")
        {
            SSQL = SSQL + " And SM.Trans_No='" + Transaction_No + "'";
            SSQL = SSQL + " And SS.Trans_No='" + Transaction_No + "'";
        }
        
        if (VarietyCode != "")
        {
            SSQL = SSQL + " And SS.WasteDesc='" + VarietyCode + "'";
        }
        if (FromDate != "" && ToDate != "")
        {
            SSQL = SSQL + " And CONVERT(DATETIME,SM.Trans_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(DATETIME,SM.Trans_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
            SSQL = SSQL + " And CONVERT(DATETIME,SS.Trans_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(DATETIME,SS.Trans_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
        }
        Da_Check = objdata.RptEmployeeMultipleDetails(SSQL);

        if (Da_Check.Rows.Count > 0)
        {
            DataTable dtdCompanyAddress = new DataTable();
            SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
            dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);


            for (int i = 0; i < Da_Check.Rows.Count; i++)
            {
                Da_Check.Rows[i]["CompanyName"] = dtdCompanyAddress.Rows[0]["Cname"].ToString();
                Da_Check.Rows[i]["LocationName"] = dtdCompanyAddress.Rows[0]["location"].ToString();
                Da_Check.Rows[i]["CompanyCode"] = SessionCcode;
                Da_Check.Rows[i]["LocationCode"] = SessionLcode;
                Da_Check.Rows[i]["Address1"] = dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString();
                Da_Check.Rows[i]["Address2"] = dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString();
            }
            

            ds.Tables.Add(Da_Check);
            ReportDocument report = new ReportDocument();
            report.Load(Server.MapPath("~/crystal/WasteDetails.rpt"));
           
            report.SetDatabaseLogon("username", "password", "sa", "Altius2005");
            report.Database.Tables[0].SetDataSource(ds.Tables[0]);

            if (FromDate != "" && ToDate != "")
            {
                report.DataDefinition.FormulaFields["FromDate"].Text = "'" + FromDate.ToString() + "'";
                report.DataDefinition.FormulaFields["ToDate"].Text = "'" + ToDate.ToString() + "'";
            }

            report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = report;


        }
        else
        {
            Errflag = false;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
        }
        if (!Errflag)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
            lblUploadSuccessfully.Text = "No Records Matched..";
        }
    }

    public void YarnDelivery_Detail_Report()
    {
        DataTable Da_Check = new DataTable();
        if (FromDate != "")
        {
            frmDate = Convert.ToDateTime(FromDate);
        }
        if (ToDate != "")
        {
            toDate = Convert.ToDateTime(ToDate);
        }

        SSQL = "select '' as CompanyName,'' as LocationName,'' as CompanyCode,'' as LocationCode,'' as Address1,'' as Address2,";
        SSQL = SSQL + "YM.Trans_No,YM.Trans_Date,YM.ToUnit,YM.FromGoDownID,YM.FromGoDownName,";
        SSQL = SSQL + "YM.ToGoDownID,YM.ToGoDownName,YM.Remarks,YM.TotNoOfBag,YM.TotalAmount,";
        SSQL = SSQL + " YS.LotNo,YS.CountID,YS.CountName,YS.NoOfBag,YS.AvlBag,YS.Rate,YS.LineTotal";
        SSQL = SSQL + " from YarnDelivery_Main YM inner Join YarnDelivery_Main_Sub YS on YM.Trans_No=YS.Trans_No";
        SSQL = SSQL + " Where YM.Ccode='" + SessionCcode + "' And YM.Lcode='" + SessionLcode + "'";
        SSQL = SSQL + " And YM.FinYearCode='" + SessionFinYearCode + "'";
        SSQL = SSQL + " And YS.Ccode='" + SessionCcode + "' And YS.Lcode='" + SessionLcode + "'";
        SSQL = SSQL + " And YS.FinYearCode='" + SessionFinYearCode + "'";
        if (TransNo != "")
        {
            SSQL = SSQL + " And YM.Trans_No='" + TransNo + "'";
            SSQL = SSQL + " And YS.Trans_No='" + TransNo + "'";
        }
        if (WareHouseCode != "")
        {
            SSQL = SSQL + " And YM.FromGoDownID='" + WareHouseCode + "'";
        }
        if (VarietyCode != "")
        {
            SSQL = SSQL + " And YS.CountID='" + VarietyCode + "'";
        }
        if (FromDate != "" && ToDate != "")
        {
            SSQL = SSQL + " And CONVERT(DATETIME,YM.Trans_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(DATETIME,YM.Trans_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
            SSQL = SSQL + " And CONVERT(DATETIME,YS.Trans_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(DATETIME,YS.Trans_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
        }
        Da_Check = objdata.RptEmployeeMultipleDetails(SSQL);

        if (Da_Check.Rows.Count > 0)
        {
            DataTable dtdCompanyAddress = new DataTable();
            SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
            dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);


            for (int i = 0; i < Da_Check.Rows.Count; i++)
            {
                Da_Check.Rows[i]["CompanyName"] = dtdCompanyAddress.Rows[0]["Cname"].ToString();
                Da_Check.Rows[i]["LocationName"] = dtdCompanyAddress.Rows[0]["location"].ToString();
                Da_Check.Rows[i]["CompanyCode"] = SessionCcode;
                Da_Check.Rows[i]["LocationCode"] = SessionLcode;
                Da_Check.Rows[i]["Address1"] = dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString();
                Da_Check.Rows[i]["Address2"] = dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString();
            }


            ds.Tables.Add(Da_Check);
            ReportDocument report = new ReportDocument();
            report.Load(Server.MapPath("~/crystal/DeliveryYarn.rpt"));

            report.SetDatabaseLogon("username", "password", "sa", "Altius2005");
            report.Database.Tables[0].SetDataSource(ds.Tables[0]);

            if (FromDate != "" && ToDate != "")
            {
                report.DataDefinition.FormulaFields["FromDate"].Text = "'" + FromDate.ToString() + "'";
                report.DataDefinition.FormulaFields["ToDate"].Text = "'" + ToDate.ToString() + "'";
            }

            report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = report;


        }
        else
        {
            Errflag = false;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
        }
        if (!Errflag)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
            lblUploadSuccessfully.Text = "No Records Matched..";
        }
    }

    public void ServicesDelivery_Details_Report()
    {
        DataTable Da_Check = new DataTable();
        if (FromDate != "")
        {
            frmDate = Convert.ToDateTime(FromDate);
        }
        if (ToDate != "")
        {
            toDate = Convert.ToDateTime(ToDate);
        }

        SSQL = "select '' as CompanyName,'' as LocationName,'' as CompanyCode,'' as LocationCode,'' as Address1,'' as Address2,";
        SSQL = SSQL + "SM.Trans_No,SM.Trans_Date,SM.SuppCode,SM.SuppName,SM.DeliveryDate,";
        SSQL = SSQL + "SM.TakenBy,SM.IssuedBy,SM.PreferredBy,SM.Remarks,SM.TotalAmt,SM.AddOrLess,SM.NetAmt,";
        SSQL = SSQL + "SB.ItemCode,SB.ItemName,SB.WarehouseCode,SB.WarehouseName,SB.ZoneCode,SB.ZoneName,SB.Qty,SB.Rate,SB.LineTotal";
        SSQL = SSQL + " from Service_Delivery_Main SM inner join Service_Delivery_Main_Sub SB on SM.Trans_No=SB.Trans_No ";
        SSQL = SSQL + " Where SM.Ccode='" + SessionCcode + "' And SM.Lcode='" + SessionLcode + "'";
        SSQL = SSQL + " And SM.FinYearCode='" + SessionFinYearCode + "'";
        SSQL = SSQL + " And SB.Ccode='" + SessionCcode + "' And SB.Lcode='" + SessionLcode + "'";
        SSQL = SSQL + " And SB.FinYearCode='" + SessionFinYearCode + "'";
        if (Transaction_No != "")
        {
            SSQL = SSQL + " And SM.Trans_No='" + Transaction_No + "'";
            SSQL = SSQL + " And SB.Trans_No='" + Transaction_No + "'";
        }
        if (SupplierName != "")
        {
            SSQL = SSQL + " And SM.SuppCode='" + SupplierName + "'";
        }

        if (VarietyCode != "")
        {
            SSQL = SSQL + " And SB.ItemCode='" + VarietyCode + "'";
        }
        if (FromDate != "" && ToDate != "")
        {
            SSQL = SSQL + " And CONVERT(DATETIME,SM.Trans_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(DATETIME,SM.Trans_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
            SSQL = SSQL + " And CONVERT(DATETIME,SB.Trans_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(DATETIME,SB.Trans_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
        }
        Da_Check = objdata.RptEmployeeMultipleDetails(SSQL);

        if (Da_Check.Rows.Count > 0)
        {
            DataTable dtdCompanyAddress = new DataTable();
            SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
            dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);



            for (int i = 0; i < Da_Check.Rows.Count; i++)
            {
                Da_Check.Rows[i]["CompanyName"] = dtdCompanyAddress.Rows[0]["Cname"].ToString();
                Da_Check.Rows[i]["LocationName"] = dtdCompanyAddress.Rows[0]["location"].ToString();
                Da_Check.Rows[i]["CompanyCode"] = SessionCcode;
                Da_Check.Rows[i]["LocationCode"] = SessionLcode;
                Da_Check.Rows[i]["Address1"] = dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString();
                Da_Check.Rows[i]["Address2"] = dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString();
            }

           

            ds.Tables.Add(Da_Check);
            ReportDocument report = new ReportDocument();
            report.Load(Server.MapPath("~/crystal/ServicesDeliveryDetails.rpt"));
            
            report.SetDatabaseLogon("username", "password", "sa", "Altius2005");
            report.Database.Tables[0].SetDataSource(ds.Tables[0]);

            if (FromDate != "" && ToDate != "")
            {
                report.DataDefinition.FormulaFields["FromDate"].Text = "'" + FromDate.ToString() + "'";
                report.DataDefinition.FormulaFields["ToDate"].Text = "'" + ToDate.ToString() + "'";
            }

            report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = report;


        }
        else
        {
            Errflag = false;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
        }
        if (!Errflag)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
            lblUploadSuccessfully.Text = "No Records Matched..";
        }
    }

    //Cotton_Inward_Details_Report start
    public void Invoice_Report()
    {
        string DisPer = "";
        string Packing = "";
        string Courier = "";
        string Service = "";
        DataTable dt = new DataTable();
        SSQL = "";
        SSQL = SSQL + "select IM.Invoice_Entry_No,IM.Invoice_Entry_Date,(MS.SuppCode) as SupplierID,(MS.SuppName) as SupplierName,";
        SSQL = SSQL + "(MS.Address1) as SpAdd1,(MS.Address2) as SpAdd2,(MS.City) as SpCity,(MS.Pincode) as SpPincode,";
        SSQL = SSQL + "(MS.TinNo) as GSTIN,(MS.CstNo) as Stcode,(MS.MobileNo) as SpMob,";
        SSQL = SSQL + "IM.OrderNum,IM.OrderDate,IM.Despatch,IM.LRno,IM.Documents,IM.Payment,IM.Ewaybill,";
        SSQL = SSQL + "IM.SubTotal,IM.DiscountPer,IM.PackingAmt,IM.ServiceAmt,IM.CourrierAmt,IM.DiscountAmt,IM.NetAmount,IM.GSTDescription,IM.CGSTPercentage,IM.CGSTAmount, ";
        SSQL = SSQL + "IM.SGSTPercentage,IM.SGSTAmount,IM.IGSTPercentage,IM.IGSTAmount,IM.TotalGST, ";
        SSQL = SSQL + "SI.ItemCode,SI.ItemName,SI.HsnCode,SI.Price,(SI.Qty+' '+SI.QtyType) as Qty,'' as QtyType ,SI.Amount,IM.Ccode,IM.Lcode ";
        SSQL = SSQL + "from Invoice_Main IM inner join Invoice_Sub SI on IM.Invoice_Entry_No=SI.Invoice_Entry_No and IM.Lcode=SI.Lcode ";
        SSQL = SSQL + "inner join MstSupplier MS on MS.SuppCode=IM.SupplierID and MS.Lcode=IM.Lcode ";
        SSQL = SSQL + "where IM.Ccode='" + SessionCcode + "' and SI.Ccode='" + SessionCcode + "' and IM.Lcode='" + SessionLcode + "' and SI.Lcode='" + SessionLcode + "' and IM.Invoice_Entry_No='" + Transaction_No + "'";
        SSQL = SSQL + " and IM.FinYearCode='" + SessionFinYearCode + "' and SI.FinYearCode='" + SessionFinYearCode + "'";
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        if (dt.Rows.Count > 0)
        {
            if (dt.Rows.Count<12)
            {
                int count_str = Convert.ToInt32(11) - Convert.ToInt32(dt.Rows.Count);

                DisPer = dt.Rows[0]["DiscountPer"].ToString();
                Packing= dt.Rows[0]["PackingAmt"].ToString();
                Service = dt.Rows[0]["ServiceAmt"].ToString();
                Courier = dt.Rows[0]["CourrierAmt"].ToString();


                for (int i = 0; i < count_str; i++)
                {
                    dt.NewRow();
                    dt.Rows.Add();
                    dt.Rows[dt.Rows.Count - 1]["Invoice_Entry_No"] = dt.Rows[0]["Invoice_Entry_No"].ToString();
                    dt.Rows[dt.Rows.Count - 1]["Invoice_Entry_Date"] = dt.Rows[0]["Invoice_Entry_Date"].ToString();
                    dt.Rows[dt.Rows.Count - 1]["SupplierID"] = dt.Rows[0]["SupplierID"].ToString();
                    dt.Rows[dt.Rows.Count - 1]["SupplierName"] = dt.Rows[0]["SupplierName"].ToString();
                    dt.Rows[dt.Rows.Count - 1]["SpAdd1"] = dt.Rows[0]["SpAdd1"].ToString();
                    dt.Rows[dt.Rows.Count - 1]["SpAdd2"] = dt.Rows[0]["SpAdd2"].ToString();
                    dt.Rows[dt.Rows.Count - 1]["SpCity"] = dt.Rows[0]["SpCity"].ToString();
                    dt.Rows[dt.Rows.Count - 1]["SpPincode"] = dt.Rows[0]["SpPincode"].ToString();
                    dt.Rows[dt.Rows.Count - 1]["GSTIN"] = dt.Rows[0]["GSTIN"].ToString();
                    dt.Rows[dt.Rows.Count - 1]["Stcode"] = dt.Rows[0]["Stcode"].ToString();
                    dt.Rows[dt.Rows.Count - 1]["SpMob"] = dt.Rows[0]["SpMob"].ToString();
                    dt.Rows[dt.Rows.Count - 1]["OrderNum"] = dt.Rows[0]["OrderNum"].ToString();
                    dt.Rows[dt.Rows.Count - 1]["OrderDate"] = dt.Rows[0]["OrderDate"].ToString();
                    dt.Rows[dt.Rows.Count - 1]["Despatch"] = dt.Rows[0]["Despatch"].ToString();
                    dt.Rows[dt.Rows.Count - 1]["LRno"] = dt.Rows[0]["LRno"].ToString();
                    dt.Rows[dt.Rows.Count - 1]["Documents"] = dt.Rows[0]["Documents"].ToString();
                    dt.Rows[dt.Rows.Count - 1]["Payment"] = dt.Rows[0]["Payment"].ToString();
                    dt.Rows[dt.Rows.Count - 1]["Ewaybill"] = dt.Rows[0]["Ewaybill"].ToString();
                    dt.Rows[dt.Rows.Count - 1]["SubTotal"] = dt.Rows[0]["SubTotal"].ToString();
                    dt.Rows[dt.Rows.Count - 1]["NetAmount"] = dt.Rows[0]["NetAmount"].ToString();

                    dt.Rows[dt.Rows.Count - 1]["DiscountPer"] = dt.Rows[0]["DiscountPer"].ToString();
                    dt.Rows[dt.Rows.Count - 1]["DiscountAmt"] = dt.Rows[0]["DiscountAmt"].ToString();
                    
                    dt.Rows[dt.Rows.Count - 1]["GSTDescription"] = dt.Rows[0]["GSTDescription"].ToString();
                    dt.Rows[dt.Rows.Count - 1]["CGSTPercentage"] = dt.Rows[0]["CGSTPercentage"].ToString();
                    dt.Rows[dt.Rows.Count - 1]["CGSTAmount"] = dt.Rows[0]["CGSTAmount"].ToString();
                    dt.Rows[dt.Rows.Count - 1]["SGSTPercentage"] = dt.Rows[0]["SGSTPercentage"].ToString();
                    dt.Rows[dt.Rows.Count - 1]["SGSTAmount"] = dt.Rows[0]["SGSTAmount"].ToString();
                    dt.Rows[dt.Rows.Count - 1]["IGSTPercentage"] = dt.Rows[0]["IGSTPercentage"].ToString();
                    dt.Rows[dt.Rows.Count - 1]["IGSTAmount"] = dt.Rows[0]["IGSTAmount"].ToString();
                    dt.Rows[dt.Rows.Count - 1]["TotalGST"] = dt.Rows[0]["TotalGST"].ToString();
                    dt.Rows[dt.Rows.Count - 1]["ItemCode"] = "";
                    dt.Rows[dt.Rows.Count - 1]["ItemName"] = "";
                    dt.Rows[dt.Rows.Count - 1]["HsnCode"] = "";
                    dt.Rows[dt.Rows.Count - 1]["Price"] = "";
                    dt.Rows[dt.Rows.Count - 1]["Qty"] = "";
                    dt.Rows[dt.Rows.Count - 1]["QtyType"] = "";
                    dt.Rows[dt.Rows.Count - 1]["Amount"] = "";

                    if (dt.Rows.Count == Convert.ToInt32(11))
                    {
                        int rows = 1;
                        if (Convert.ToDecimal(DisPer) > Convert.ToDecimal(0))
                        {
                            dt.Rows[dt.Rows.Count - rows]["Price"] = "Discount" + "@" + dt.Rows[0]["DiscountPer"].ToString() + " %";
                            //dt.Rows[dt.Rows.Count - 1]["QtyType"] = ;
                            dt.Rows[dt.Rows.Count - rows]["Amount"] = dt.Rows[0]["DiscountAmt"].ToString();
                            rows = rows + 1;
                        }
                         if(Convert.ToDecimal(Courier)>Convert.ToDecimal(0))
                        {
                            dt.Rows[dt.Rows.Count - rows]["Price"] = "Courier ";
                            //dt.Rows[dt.Rows.Count - 1]["QtyType"] = ;
                            dt.Rows[dt.Rows.Count - rows]["Amount"] = dt.Rows[0]["CourrierAmt"].ToString();
                            rows = rows + 1;
                        }
                         if (Convert.ToDecimal(Packing) > Convert.ToDecimal(0))
                        {
                            dt.Rows[dt.Rows.Count - rows]["Price"] = "Packing ";
                            //dt.Rows[dt.Rows.Count - 1]["QtyType"] = ;
                            dt.Rows[dt.Rows.Count - rows]["Amount"] = dt.Rows[0]["PackingAmt"].ToString();
                            rows = rows + 1;
                        }
                         if (Convert.ToDecimal(Service) > Convert.ToDecimal(0))
                        {
                            dt.Rows[dt.Rows.Count - rows]["Price"] = "Service ";
                            //dt.Rows[dt.Rows.Count - 1]["QtyType"] = ;
                            dt.Rows[dt.Rows.Count - rows]["Amount"] = dt.Rows[0]["ServiceAmt"].ToString();
                            rows = rows + 1;
                        }
                    }
                }
            }


            DataSet ds1 = new DataSet();
            ds1.Tables.Add(dt);
            RD_Report.Load(Server.MapPath("~/crystal/InvoiceRpt_New.rpt"));
            RD_Report.SetDataSource(ds1.Tables[0]);

            DataTable dtdCompanyAddress = new DataTable();
            DataTable Close_Det = new DataTable();

            SSQL = "select Cname,location,Address1,Address2,Pincode,State,TinNo,CstNo From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
            dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dtdCompanyAddress.Rows.Count != 0)
            {
                RD_Report.DataDefinition.FormulaFields["CompanyName"].Text = "'" + dtdCompanyAddress.Rows[0]["Cname"].ToString() + "'";
                RD_Report.DataDefinition.FormulaFields["Address1"].Text = "'" + dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString() + "'";
                RD_Report.DataDefinition.FormulaFields["Address2"].Text = "'" + dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString() + "'";
                RD_Report.DataDefinition.FormulaFields["GSTIN"].Text = "'" + dtdCompanyAddress.Rows[0]["TinNo"].ToString() + "'";
                RD_Report.DataDefinition.FormulaFields["StateCode"].Text = "'" + dtdCompanyAddress.Rows[0]["CstNo"].ToString() + "'";
            }
            RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = RD_Report;
            CrystalReportViewer1.RefreshReport();
            CrystalReportViewer1.DataBind();
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('No Records Found')", true);
        }

    }
    public void Cotton_Inward_Details_Report()
    {
        DataTable dt = new DataTable();
        string query = "Select Ctn_Inwards_No,";
        query = query + "Ctn_Inwards_Date,GatePassIN,LotNo,LotDate,Type,Purchase_Order_No,AgentID,";
        query = query + "AgentName,Supp_Code,Supp_Name,InvoiceNo,InvoiceDate,TransportCode,TransportName,";
        query = query + "StationCode,StationName,LorryNo,LorryFright,PartyLotNo,";
        query = query + "PressMarkNo,VarietyCode,VarietyName,NoOfBale,RatePerCandy,CandyWeight,BaleWeight,";
        query = query + "NetAmount,Cotton_Stable,Cotton_Mic,Cotton_Lengh,Cotton_Master,Cottin_Others";
        query = query + " from CottonInwards where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
        if (Transaction_No.ToString() != "")
        {
            query = query + "and Ctn_Inwards_No='" + Transaction_No + "' ";
        }

        if (LotNo.ToString() != "")
        {
            query = query + "and LotNo='" + LotNo + "' ";
        }
        if (FromDate != "" && ToDate != "")
        {
            query = query + "and CONVERT(datetime,Ctn_Inwards_Date,103)>=CONVERT(datetime,'" + FromDate + "',103) and CONVERT(datetime,Ctn_Inwards_Date,103)<=CONVERT(datetime,'" + ToDate + "',103) ";
        }


        dt = objdata.RptEmployeeMultipleDetails(query);
        if (dt.Rows.Count > 0)
        {
            DataSet ds1 = new DataSet();
            ds1.Tables.Add(dt);
            RD_Report.Load(Server.MapPath("~/crystal/CottonInwards.rpt"));
            RD_Report.SetDataSource(ds1.Tables[0]);

            DataTable dtdCompanyAddress = new DataTable();
            DataTable Close_Det = new DataTable();

            SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
            dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dtdCompanyAddress.Rows.Count != 0)
            {
                RD_Report.DataDefinition.FormulaFields["CompanyName"].Text = "'" + dtdCompanyAddress.Rows[0]["Cname"].ToString() + "'";
                RD_Report.DataDefinition.FormulaFields["Address1"].Text = "'" + dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString() + "'";
                RD_Report.DataDefinition.FormulaFields["Address2"].Text = "'" + dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString() + "'";
            }
            RD_Report.DataDefinition.FormulaFields["FromDate"].Text = "'" + FromDate.ToString() + "'";
            RD_Report.DataDefinition.FormulaFields["ToDate"].Text = "'" + ToDate.ToString() + "'";


            RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = RD_Report;
            CrystalReportViewer1.RefreshReport();
            CrystalReportViewer1.DataBind();
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
            lblUploadSuccessfully.Text = "No Records Matched..";
        }
    }

    public void OE_Details_OverAll_Report()
    {
        DataTable Da_Check = new DataTable();

        SSQL = "select '' as CompanyName,'' as LocationName,'' as CompanyCode,'' as LocationCode,'' as Address1,'' as Address2,";
        SSQL = SSQL + " CM.Trans_No,CM.Trans_Date,CM.ShiftName,CS.Unit,CS.Color,CS.LotNo, ";
        SSQL = SSQL + " CS.MachineCode,CS.MachineName,CS.StdProd,CS.ActProd,CS.Utility, ";
        SSQL = SSQL + " CS.Efficiency,CS.SliverWaste,CS.StoppageMins,CS.CountID,CS.CountName,CS.Side from OE_Main CM inner join OE_Sub CS on CM.Trans_No=CS.Trans_No";
        SSQL = SSQL + " Where CM.Ccode='" + SessionCcode + "' And CM.Lcode='" + SessionLcode + "' and CS.Ccode='" + SessionCcode + "' And CS.Lcode='" + SessionLcode + "' ";
        SSQL = SSQL + " And CM.FinYearCode='" + SessionFinYearCode + "' and CS.FinYearCode='" + SessionFinYearCode + "'";
        if (Transaction_No != "")
        {
            SSQL = SSQL + " And CM.Trans_No='" + Transaction_No + "'";
        }
        if (MachineName != "")
        {
            SSQL = SSQL + " And CS.MachineCode='" + MachineName + "'";
        }
        if (LotNo != "")
        {
            SSQL = SSQL + " And CM.LotNo='" + LotNo + "'";
        }
        if (FromDate != "" && ToDate != "")
        {
            SSQL = SSQL + " And CONVERT(DATETIME,CM.Trans_Date, 103)>=CONVERT(DATETIME,'" + FromDate + "',103) And CONVERT(DATETIME,CM.Trans_Date, 103)<=CONVERT(DATETIME,'" + ToDate + "',103)";
        }
        SSQL = SSQL + "order by CM.ShiftName asc";
        Da_Check = objdata.RptEmployeeMultipleDetails(SSQL);
        if (Da_Check.Rows.Count > 0)
        {
            DataTable dtdCompanyAddress = new DataTable();
            SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
            dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);



            for (int i = 0; i < Da_Check.Rows.Count; i++)
            {
                Da_Check.Rows[i]["CompanyName"] = dtdCompanyAddress.Rows[0]["Cname"].ToString();
                Da_Check.Rows[i]["LocationName"] = dtdCompanyAddress.Rows[0]["location"].ToString();
                Da_Check.Rows[i]["CompanyCode"] = SessionCcode;
                Da_Check.Rows[i]["LocationCode"] = SessionLcode;
                Da_Check.Rows[i]["Address1"] = dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString();
                Da_Check.Rows[i]["Address2"] = dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString();
            }



            ds.Tables.Add(Da_Check);
            ReportDocument report = new ReportDocument();
            report.Load(Server.MapPath("~/crystal/OEOverAllDetails.rpt"));

            report.SetDatabaseLogon("username", "password", "sa", "Altius2005");
            report.Database.Tables[0].SetDataSource(ds.Tables[0]);

            if (FromDate != "" && ToDate != "")
            {
                report.DataDefinition.FormulaFields["FromDate"].Text = "'" + FromDate.ToString() + "'";
                report.DataDefinition.FormulaFields["ToDate"].Text = "'" + ToDate.ToString() + "'";
            }

            report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = report;


        }
        else
        {
            Errflag = false;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
        }
        if (!Errflag)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
            lblUploadSuccessfully.Text = "No Records Matched..";
        }
    }

    public void Drawing_Details_OverAll_Report()
    {
        DataTable Da_Check = new DataTable();

        SSQL = "select '' as CompanyName,'' as LocationName,'' as CompanyCode,'' as LocationCode,'' as Address1,'' as Address2,";
        SSQL = SSQL + " CM.Trans_No,CM.Trans_Date,CM.Shift,CM.Unit,CM.Color,CM.LotNo, ";
        SSQL = SSQL + " CS.MachineCode,CS.MachineName,CS.StdProduction,CS.ActProduction,CS.Utilization, ";
        SSQL = SSQL + " CS.Efficiency,CS.Silver_Wastage,CS.Stop_Page_Mintus from Drawing_Main CM inner join Drawing_Main_Sub CS on CM.Trans_No=CS.Trans_No";
        SSQL = SSQL + " Where CM.Ccode='" + SessionCcode + "' And CM.Lcode='" + SessionLcode + "' and CS.Ccode='" + SessionCcode + "' And CS.Lcode='" + SessionLcode + "' ";
        SSQL = SSQL + " And CM.FinYearCode='" + SessionFinYearCode + "' and CS.FinYearCode='" + SessionFinYearCode + "'";
        if (Transaction_No != "")
        {
            SSQL = SSQL + " And CM.Trans_No='" + Transaction_No + "'";
        }
        if (MachineName != "")
        {
            SSQL = SSQL + " And CS.MachineCode='" + MachineName + "'";
        }
        if (LotNo != "")
        {
            SSQL = SSQL + " And CM.LotNo='" + LotNo + "'";
        }
        if (FromDate != "" && ToDate != "")
        {
            SSQL = SSQL + " And CONVERT(DATETIME,CM.Trans_Date, 103)>=CONVERT(DATETIME,'" + FromDate + "',103) And CONVERT(DATETIME,CM.Trans_Date, 103)<=CONVERT(DATETIME,'" + ToDate + "',103)";
        }
        SSQL = SSQL + "order by CM.Shift asc";
        Da_Check = objdata.RptEmployeeMultipleDetails(SSQL);
        if (Da_Check.Rows.Count > 0)
        {
            DataTable dtdCompanyAddress = new DataTable();
            SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
            dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);



            for (int i = 0; i < Da_Check.Rows.Count; i++)
            {
                Da_Check.Rows[i]["CompanyName"] = dtdCompanyAddress.Rows[0]["Cname"].ToString();
                Da_Check.Rows[i]["LocationName"] = dtdCompanyAddress.Rows[0]["location"].ToString();
                Da_Check.Rows[i]["CompanyCode"] = SessionCcode;
                Da_Check.Rows[i]["LocationCode"] = SessionLcode;
                Da_Check.Rows[i]["Address1"] = dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString();
                Da_Check.Rows[i]["Address2"] = dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString();
            }



            ds.Tables.Add(Da_Check);
            ReportDocument report = new ReportDocument();
            report.Load(Server.MapPath("~/crystal/DrawingDetailsOverAll.rpt"));

            report.SetDatabaseLogon("username", "password", "sa", "Altius2005");
            report.Database.Tables[0].SetDataSource(ds.Tables[0]);

            if (FromDate != "" && ToDate != "")
            {
                report.DataDefinition.FormulaFields["FromDate"].Text = "'" + FromDate.ToString() + "'";
                report.DataDefinition.FormulaFields["ToDate"].Text = "'" + ToDate.ToString() + "'";
            }

            report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = report;


        }
        else
        {
            Errflag = false;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
        }
        if (!Errflag)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
            lblUploadSuccessfully.Text = "No Records Matched..";
        }

    
    }
    public void Winding_Details_OverAll_Report()
    {

        DataTable Da_Check = new DataTable();

        SSQL = "select '' as CompanyName,'' as LocationName,'' as CompanyCode,'' as LocationCode,'' as Address1,'' as Address2,";
        SSQL = SSQL + " CM.Trans_No,CM.Trans_Date,CM.Shift,CM.ToGoDownName as ToGoDownID,ME.EmpName as ToGoDownName,CS.Unit,CS.LotNo,CS.Side, ";
        SSQL = SSQL + " CS.MachineCode,CS.MachineName,CS.DrumAllot,CS.DoffCone,CS.ConeWeight, ";
        SSQL = SSQL + " CS.Production,CS.HardWaste,CS.Efficiency,CS.CountID,CS.CountName,CS.Reason as Color,CS.Side from Winding_Main CM inner join Winding_Sub CS on CM.Trans_No=CS.Trans_No";
        SSQL = SSQL + " inner join MstEmployee ME on  CM.PrepaidBy=ME.EmpCode ";
        SSQL = SSQL + " Where CM.Ccode='" + SessionCcode + "' And CM.Lcode='" + SessionLcode + "' and CS.Ccode='" + SessionCcode + "' And CS.Lcode='" + SessionLcode + "' ";
        SSQL = SSQL + " And CM.FinYearCode='" + SessionFinYearCode + "' and CS.FinYearCode='" + SessionFinYearCode + "'";
        SSQL = SSQL + " And ME.Ccode='" + SessionCcode + "' and ME.Lcode='" + SessionLcode + "'";
        if (Transaction_No != "")
        {
            SSQL = SSQL + " And CM.Trans_No='" + Transaction_No + "'";
        }
        if (MachineName != "")
        {
            SSQL = SSQL + " And CS.MachineCode='" + MachineName + "'";
        }
        if (LotNo != "")
        {
            SSQL = SSQL + " And CS.LotNo='" + LotNo + "'";
        }
        if (FromDate != "" && ToDate != "")
        {
            SSQL = SSQL + " And CONVERT(DATETIME,CM.Trans_Date, 103)>=CONVERT(DATETIME,'" + FromDate + "',103) ";
            SSQL = SSQL + " And CONVERT(DATETIME,CM.Trans_Date, 103)<=CONVERT(DATETIME,'" + ToDate + "',103)";
        }
        if (Sup_Name != "")
        {
            SSQL = SSQL + " And CM.PrepaidBy='" + Sup_Name + "'";
        }
        SSQL = SSQL + "order by CM.Shift asc";
        Da_Check = objdata.RptEmployeeMultipleDetails(SSQL);
        if (Da_Check.Rows.Count > 0)
        {
            DataTable dtdCompanyAddress = new DataTable();
            SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
            dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);



            for (int i = 0; i < Da_Check.Rows.Count; i++)
            {

                Da_Check.Rows[i]["CompanyName"] = dtdCompanyAddress.Rows[0]["Cname"].ToString();
                Da_Check.Rows[i]["LocationName"] = dtdCompanyAddress.Rows[0]["location"].ToString();
                Da_Check.Rows[i]["CompanyCode"] = SessionCcode;
                Da_Check.Rows[i]["LocationCode"] = SessionLcode;
                Da_Check.Rows[i]["Address1"] = dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString();
                Da_Check.Rows[i]["Address2"] = dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString();
            }



            ds.Tables.Add(Da_Check);
            ReportDocument report = new ReportDocument();
            report.Load(Server.MapPath("~/crystal/WindingDetailOverAllNew.rpt"));

            report.SetDatabaseLogon("username", "password", "sa", "Altius2005");
            report.Database.Tables[0].SetDataSource(ds.Tables[0]);

            if (FromDate != "")
            {
                report.DataDefinition.FormulaFields["FromDate"].Text = "'" + FromDate.ToString() + "'";

            }
            if (ToDate != "")
            {
                report.DataDefinition.FormulaFields["ToDate"].Text = "'" + ToDate.ToString() + "'";
            }

            report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = report;


        }
        else
        {
            Errflag = false;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
        }
        if (!Errflag)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
            lblUploadSuccessfully.Text = "No Records Matched..";
        }


        // DataTable Da_Check = new DataTable();

        //SSQL = "select '' as CompanyName,'' as LocationName,'' as CompanyCode,'' as LocationCode,'' as Address1,'' as Address2,";
        //SSQL = SSQL + " CM.Trans_No,CM.Trans_Date,CM.Shift,CM.ToGoDownID,CM.ToGoDownName,CS.Unit,CS.LotNo,CS.Side, ";
        //SSQL = SSQL + " CS.MachineCode,CS.MachineName,CS.DrumAllot,CS.DoffCone,CS.ConeWeight, ";
        //SSQL = SSQL + " CS.Production,CS.HardWaste,CS.Efficiency,CS.CountID,CS.CountName  from Winding_Main CM inner join Winding_Sub CS on CM.Trans_No=CS.Trans_No";
        //SSQL = SSQL + " Where CM.Ccode='" + SessionCcode + "' And CM.Lcode='" + SessionLcode + "' and CS.Ccode='" + SessionCcode + "' And CS.Lcode='" + SessionLcode + "' ";
        //SSQL = SSQL + " And CM.FinYearCode='" + SessionFinYearCode + "' and CS.FinYearCode='" + SessionFinYearCode + "'";
        //if (Transaction_No != "")
        //{
        //    SSQL = SSQL + " And CM.Trans_No='" + Transaction_No + "'";
        //}
        //if (MachineName != "")
        //{
        //    SSQL = SSQL + " And CS.MachineCode='" + MachineName + "'";
        //}
        //if (LotNo != "")
        //{
        //    SSQL = SSQL + " And CM.LotNo='" + LotNo + "'";
        //}
        //if (FromDate != "" && ToDate != "")
        //{
        //    SSQL = SSQL + " And CONVERT(DATETIME,CM.Trans_Date, 103)>=CONVERT(DATETIME,'" + FromDate + "',103) And CONVERT(DATETIME,CM.Trans_Date, 103)<=CONVERT(DATETIME,'" + ToDate + "',103)";
        //}
        //SSQL = SSQL + "order by CM.Shift asc";
        //Da_Check = objdata.RptEmployeeMultipleDetails(SSQL);
        //if (Da_Check.Rows.Count > 0)
        //{
        //    DataTable dtdCompanyAddress = new DataTable();
        //    SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
        //    dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);



        //    for (int i = 0; i < Da_Check.Rows.Count; i++)
        //    {
        //        Da_Check.Rows[i]["CompanyName"] = dtdCompanyAddress.Rows[0]["Cname"].ToString();
        //        Da_Check.Rows[i]["LocationName"] = dtdCompanyAddress.Rows[0]["location"].ToString();
        //        Da_Check.Rows[i]["CompanyCode"] = SessionCcode;
        //        Da_Check.Rows[i]["LocationCode"] = SessionLcode;
        //        Da_Check.Rows[i]["Address1"] = dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString();
        //        Da_Check.Rows[i]["Address2"] = dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString();
        //    }



        //    ds.Tables.Add(Da_Check);
        //    ReportDocument report = new ReportDocument();
        //    report.Load(Server.MapPath("~/crystal/WindingDetailOverAll.rpt"));

        //    report.SetDatabaseLogon("username", "password", "sa", "Altius2005");
        //    report.Database.Tables[0].SetDataSource(ds.Tables[0]);

        //    if (FromDate != "" && ToDate != "")
        //    {
        //        report.DataDefinition.FormulaFields["FromDate"].Text = "'" + FromDate.ToString() + "'";
        //        report.DataDefinition.FormulaFields["ToDate"].Text = "'" + ToDate.ToString() + "'";
        //    }

        //    report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
        //    CrystalReportViewer1.ReportSource = report;


        //}
        //else
        //{
        //    Errflag = false;
        //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
        //}
        //if (!Errflag)
        //{
        //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
        //    lblUploadSuccessfully.Text = "No Records Matched..";
        //}

    }

    //public void Winding_Details_OverAll_Report()
    //{
    //    DataTable Da_Check = new DataTable();

    //    SSQL = "select '' as CompanyName,'' as LocationName,'' as CompanyCode,'' as LocationCode,'' as Address1,'' as Address2,";
    //    SSQL = SSQL + " CM.Trans_No,CM.Trans_Date,CM.Shift,CM.ToGoDownID,CM.ToGoDownName,CS.Unit,CS.LotNo,CS.Side, ";
    //    SSQL = SSQL + " CS.MachineCode,CS.MachineName,CS.DrumAllot,CS.DoffCone,CS.ConeWeight, ";
    //    SSQL = SSQL + " CS.Production,CS.HardWaste,CS.Efficiency,CS.CountID,CS.CountName  from Winding_Main CM inner join Winding_Sub CS on CM.Trans_No=CS.Trans_No";
    //    SSQL = SSQL + " Where CM.Ccode='" + SessionCcode + "' And CM.Lcode='" + SessionLcode + "' and CS.Ccode='" + SessionCcode + "' And CS.Lcode='" + SessionLcode + "' ";
    //    SSQL = SSQL + " And CM.FinYearCode='" + SessionFinYearCode + "' and CS.FinYearCode='" + SessionFinYearCode + "'";
    //    if (Transaction_No != "")
    //    {
    //        SSQL = SSQL + " And CM.Trans_No='" + Transaction_No + "'";
    //    }
    //    if (MachineName != "")
    //    {
    //        SSQL = SSQL + " And CS.MachineCode='" + MachineName + "'";
    //    }
    //    if (LotNo != "")
    //    {
    //        SSQL = SSQL + " And CM.LotNo='" + LotNo + "'";
    //    }
    //    if (FromDate != "" && ToDate != "")
    //    {
    //        SSQL = SSQL + " And CONVERT(DATETIME,CM.Trans_Date, 103)>=CONVERT(DATETIME,'" + FromDate + "',103) And CONVERT(DATETIME,CM.Trans_Date, 103)<=CONVERT(DATETIME,'" + ToDate + "',103)";
    //    }
    //    SSQL = SSQL + "order by CM.Shift asc";
    //    Da_Check = objdata.RptEmployeeMultipleDetails(SSQL);
    //    if (Da_Check.Rows.Count > 0)
    //    {
    //        DataTable dtdCompanyAddress = new DataTable();
    //        SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
    //        dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);



    //        for (int i = 0; i < Da_Check.Rows.Count; i++)
    //        {
    //            Da_Check.Rows[i]["CompanyName"] = dtdCompanyAddress.Rows[0]["Cname"].ToString();
    //            Da_Check.Rows[i]["LocationName"] = dtdCompanyAddress.Rows[0]["location"].ToString();
    //            Da_Check.Rows[i]["CompanyCode"] = SessionCcode;
    //            Da_Check.Rows[i]["LocationCode"] = SessionLcode;
    //            Da_Check.Rows[i]["Address1"] = dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString();
    //            Da_Check.Rows[i]["Address2"] = dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString();
    //        }



    //        ds.Tables.Add(Da_Check);
    //        ReportDocument report = new ReportDocument();
    //        report.Load(Server.MapPath("~/crystal/WindingDetailOverAll.rpt"));

    //        report.SetDatabaseLogon("username", "password", "sa", "Altius2005");
    //        report.Database.Tables[0].SetDataSource(ds.Tables[0]);

    //        if (FromDate != "" && ToDate != "")
    //        {
    //            report.DataDefinition.FormulaFields["FromDate"].Text = "'" + FromDate.ToString() + "'";
    //            report.DataDefinition.FormulaFields["ToDate"].Text = "'" + ToDate.ToString() + "'";
    //        }

    //        report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
    //        CrystalReportViewer1.ReportSource = report;


    //    }
    //    else
    //    {
    //        Errflag = false;
    //        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
    //    }
    //    if (!Errflag)
    //    {
    //        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
    //        lblUploadSuccessfully.Text = "No Records Matched..";
    //    }

    //}

    public void Carding_Details_OverAll_Report()
    {
        DataTable Da_Check = new DataTable();

        SSQL = "select '' as CompanyName,'' as LocationName,'' as CompanyCode,'' as LocationCode,'' as Address1,'' as Address2,";
        SSQL = SSQL + " CM.Trans_No,CM.Trans_Date,CM.Shift,CM.Unit,CM.Color,CM.LotNo, ";
        SSQL = SSQL + " CS.MachineCode,CS.MachineName,CS.StdProduction,CS.ActProduction,CS.Utilization, ";
        SSQL = SSQL + " CS.Efficiency,CS.Silver_Wastage,CS.Stop_Page_Mintus from Carding_Main CM inner join Carding_Main_Sub CS on CM.Trans_No=CS.Trans_No";
        SSQL = SSQL + " Where CM.Ccode='" + SessionCcode + "' And CM.Lcode='" + SessionLcode + "' and CS.Ccode='" + SessionCcode + "' And CS.Lcode='" + SessionLcode + "' ";
        SSQL = SSQL + " And CM.FinYearCode='" + SessionFinYearCode + "' and CS.FinYearCode='" + SessionFinYearCode + "'";
        if (Transaction_No != "")
        {
            SSQL = SSQL + " And CM.Trans_No='" + Transaction_No + "'";
        }
        if (MachineName != "")
        {
            SSQL = SSQL + " And CS.MachineCode='" + MachineName + "'";
        }
        if (LotNo != "")
        {
            SSQL = SSQL + " And CM.LotNo='" + LotNo + "'";
        }
        if (FromDate != "" && ToDate != "")
        {
            SSQL = SSQL + " And CONVERT(DATETIME,CM.Trans_Date, 103)>=CONVERT(DATETIME,'" + FromDate + "',103) And CONVERT(DATETIME,CM.Trans_Date, 103)<=CONVERT(DATETIME,'" + ToDate + "',103)";
        }
        SSQL = SSQL + "order by CM.Shift asc";
        Da_Check = objdata.RptEmployeeMultipleDetails(SSQL);
        if (Da_Check.Rows.Count > 0)
        {
            DataTable dtdCompanyAddress = new DataTable();
            SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
            dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);



            for (int i = 0; i < Da_Check.Rows.Count; i++)
            {
                Da_Check.Rows[i]["CompanyName"] = dtdCompanyAddress.Rows[0]["Cname"].ToString();
                Da_Check.Rows[i]["LocationName"] = dtdCompanyAddress.Rows[0]["location"].ToString();
                Da_Check.Rows[i]["CompanyCode"] = SessionCcode;
                Da_Check.Rows[i]["LocationCode"] = SessionLcode;
                Da_Check.Rows[i]["Address1"] = dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString();
                Da_Check.Rows[i]["Address2"] = dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString();
            }



            ds.Tables.Add(Da_Check);
            ReportDocument report = new ReportDocument();
            report.Load(Server.MapPath("~/crystal/CardingDetailsOverAll.rpt"));

            report.SetDatabaseLogon("username", "password", "sa", "Altius2005");
            report.Database.Tables[0].SetDataSource(ds.Tables[0]);

            if (FromDate != "" && ToDate != "")
            {
                report.DataDefinition.FormulaFields["FromDate"].Text = "'" + FromDate.ToString() + "'";
                report.DataDefinition.FormulaFields["ToDate"].Text = "'" + ToDate.ToString() + "'";
            }

            report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = report;


        }
        else
        {
            Errflag = false;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
        }
        if (!Errflag)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
            lblUploadSuccessfully.Text = "No Records Matched..";
        }


    }
    //Cotton_Inward_Details_Report end

    public void Carding_Details_Report()
    {
        DataTable Da_Check = new DataTable();
        if (FromDate != "")
        {
            frmDate = Convert.ToDateTime(FromDate);
        }
        if (ToDate != "")
        {
            toDate = Convert.ToDateTime(ToDate);
        }

        SSQL = "select '' as CompanyName,'' as LocationName,'' as CompanyCode,'' as LocationCode,'' as Address1,'' as Address2,";
        SSQL = SSQL + "CM.Trans_No,CM.Trans_Date,CM.Unit,CM.Supervisor,CM.Color,CM.LotNo,CM.Mixing_issue,CM.Shift,CMS.MachineName,";
        SSQL = SSQL + "CMS.Speed,CMS.Hank,CMS.StdProduction,CMS.ActProduction,CMS.Utilization,CMS.Efficiency,CMS.Silver_Wastage,";
        SSQL = SSQL + "CMS.Stop_Page_Mintus from Carding_Main CM inner join Carding_Main_Sub CMS on CM.Trans_No=CMS.Trans_No ";
        SSQL = SSQL + " Where CM.Ccode='" + SessionCcode + "' And CM.Lcode='" + SessionLcode + "'";
        SSQL = SSQL + " And CM.FinYearCode='" + SessionFinYearCode + "'";
        SSQL = SSQL + " And CMS.Ccode='" + SessionCcode + "' And CMS.Lcode='" + SessionLcode + "'";
        SSQL = SSQL + " And CMS.FinYearCode='" + SessionFinYearCode + "'";
        if (Transaction_No != "")
        {
            SSQL = SSQL + " And CM.Trans_No='" + Transaction_No + "'";
            SSQL = SSQL + " And CMS.Trans_No='" + Transaction_No + "'";
        }
        if (MachineName != "")
        {
            SSQL = SSQL + " And CMS.MachineName='" + MachineName + "'";
        }
        if (ColorName != "")
        {
            SSQL = SSQL + " And CM.Color='" + ColorName + "'";
        }

        if (FromDate != "" && ToDate != "")
        {
            SSQL = SSQL + " And CONVERT(DATETIME,CM.Trans_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(DATETIME,CM.Trans_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
            SSQL = SSQL + " And CONVERT(DATETIME,CMS.Trans_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(DATETIME,CMS.Trans_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
        }
        Da_Check = objdata.RptEmployeeMultipleDetails(SSQL);

        if (Da_Check.Rows.Count > 0)
        {
            DataTable dtdCompanyAddress = new DataTable();
            SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
            dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);



            for (int i = 0; i < Da_Check.Rows.Count; i++)
            {
                Da_Check.Rows[i]["CompanyName"] = dtdCompanyAddress.Rows[0]["Cname"].ToString();
                Da_Check.Rows[i]["LocationName"] = dtdCompanyAddress.Rows[0]["location"].ToString();
                Da_Check.Rows[i]["CompanyCode"] = SessionCcode;
                Da_Check.Rows[i]["LocationCode"] = SessionLcode;
                Da_Check.Rows[i]["Address1"] = dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString();
                Da_Check.Rows[i]["Address2"] = dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString();
            }



            ds.Tables.Add(Da_Check);
            ReportDocument report = new ReportDocument();
            report.Load(Server.MapPath("~/crystal/CardingDetails.rpt"));

            report.SetDatabaseLogon("username", "password", "sa", "Altius2005");
            report.Database.Tables[0].SetDataSource(ds.Tables[0]);

            if (FromDate != "" && ToDate != "")
            {
                report.DataDefinition.FormulaFields["FromDate"].Text = "'" + FromDate.ToString() + "'";
                report.DataDefinition.FormulaFields["ToDate"].Text = "'" + ToDate.ToString() + "'";
            }

            report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = report;


        }
        else
        {
            Errflag = false;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
        }
        if (!Errflag)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
            lblUploadSuccessfully.Text = "No Records Matched..";
        }
    }


    public void Drawing_Details_Report()
    {
        DataTable Da_Check = new DataTable();
        if (FromDate != "")
        {
            frmDate = Convert.ToDateTime(FromDate);
        }
        if (ToDate != "")
        {
            toDate = Convert.ToDateTime(ToDate);
        }

        SSQL = "select '' as CompanyName,'' as LocationName,'' as CompanyCode,'' as LocationCode,'' as Address1,'' as Address2,";
        SSQL = SSQL + "CM.Trans_No,CM.Trans_Date,CM.Unit,CM.Supervisor,CM.Color,CM.LotNo,CM.Shift,CMS.MachineName,";
        SSQL = SSQL + "CMS.Speed,CMS.Hank,CMS.StdProduction,CMS.ActProduction,CMS.Utilization,CMS.Efficiency,CMS.Silver_Wastage,";
        SSQL = SSQL + "CMS.Stop_Page_Mintus from Drawing_Main CM inner join Drawing_Main_Sub CMS on CM.Trans_No=CMS.Trans_No ";
        SSQL = SSQL + " Where CM.Ccode='" + SessionCcode + "' And CM.Lcode='" + SessionLcode + "'";
        SSQL = SSQL + " And CM.FinYearCode='" + SessionFinYearCode + "'";
        SSQL = SSQL + " And CMS.Ccode='" + SessionCcode + "' And CMS.Lcode='" + SessionLcode + "'";
        SSQL = SSQL + " And CMS.FinYearCode='" + SessionFinYearCode + "'";
        if (Transaction_No != "")
        {
            SSQL = SSQL + " And CM.Trans_No='" + Transaction_No + "'";
            SSQL = SSQL + " And CMS.Trans_No='" + Transaction_No + "'";
        }
        if (MachineName != "")
        {
            SSQL = SSQL + " And CMS.MachineName='" + MachineName + "'";
        }
        if (ColorName != "")
        {
            SSQL = SSQL + " And CM.Color='" + ColorName + "'";
        }

        if (FromDate != "" && ToDate != "")
        {
            SSQL = SSQL + " And CONVERT(DATETIME,CM.Trans_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(DATETIME,CM.Trans_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
            SSQL = SSQL + " And CONVERT(DATETIME,CMS.Trans_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(DATETIME,CMS.Trans_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
        }
        Da_Check = objdata.RptEmployeeMultipleDetails(SSQL);

        if (Da_Check.Rows.Count > 0)
        {
            DataTable dtdCompanyAddress = new DataTable();
            SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
            dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);



            for (int i = 0; i < Da_Check.Rows.Count; i++)
            {
                Da_Check.Rows[i]["CompanyName"] = dtdCompanyAddress.Rows[0]["Cname"].ToString();
                Da_Check.Rows[i]["LocationName"] = dtdCompanyAddress.Rows[0]["location"].ToString();
                Da_Check.Rows[i]["CompanyCode"] = SessionCcode;
                Da_Check.Rows[i]["LocationCode"] = SessionLcode;
                Da_Check.Rows[i]["Address1"] = dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString();
                Da_Check.Rows[i]["Address2"] = dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString();
            }


            ds.Tables.Add(Da_Check);
            ReportDocument report = new ReportDocument();
            report.Load(Server.MapPath("~/crystal/DrawingDetails.rpt"));

            report.SetDatabaseLogon("username", "password", "sa", "Altius2005");
            report.Database.Tables[0].SetDataSource(ds.Tables[0]);

            if (FromDate != "" && ToDate != "")
            {
                report.DataDefinition.FormulaFields["FromDate"].Text = "'" + FromDate.ToString() + "'";
                report.DataDefinition.FormulaFields["ToDate"].Text = "'" + ToDate.ToString() + "'";
            }

            report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = report;

        }
        else
        {
            Errflag = false;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
        }
        if (!Errflag)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
            lblUploadSuccessfully.Text = "No Records Matched..";
        }

    }



    public void OE_Details_Report()
    {
        DataTable Da_Check = new DataTable();
        if (FromDate != "")
        {
            frmDate = Convert.ToDateTime(FromDate);
        }
        if (ToDate != "")
        {
            toDate = Convert.ToDateTime(ToDate);
        }



        SSQL = "select '' as CompanyName,'' as LocationName,'' as CompanyCode,'' as LocationCode,'' as Address1,'' as Address2,";
        SSQL = SSQL + " OM.Trans_No,OM.Trans_Date,OS.Unit,ME.EmpName,OM.Color,OS.LotNo,OM.ShiftName,OS.MachineName,";
        SSQL = SSQL + " OS.NoofRoter,OS.WorkRoter,OS.CountName,isnull(cast(OS.StdProd as int), '0') as StdProd,OS.ActProd,OS.Utility,OS.Efficiency,OS.SliverWaste,";
        SSQL = SSQL + " OS.StopRoter,OS.StoppageMins  from OE_Main OM inner join OE_Sub OS on OM.Trans_No=OS.Trans_No ";
        SSQL = SSQL + " inner join MstEmployee ME on OM.SupervisorName =ME.EmpCode ";
        SSQL = SSQL + " Where OM.Ccode='" + SessionCcode + "' And OM.Lcode='" + SessionLcode + "'";
        SSQL = SSQL + " And OM.FinYearCode='" + SessionFinYearCode + "'";
        SSQL = SSQL + " And OS.Ccode='" + SessionCcode + "' And OS.Lcode='" + SessionLcode + "'";
        SSQL = SSQL + " And ME.Ccode='" + SessionCcode + "' And ME.Lcode='" + SessionLcode + "'";
        SSQL = SSQL + " And OS.FinYearCode='" + SessionFinYearCode + "'";
        if (Transaction_No != "")
        {
            SSQL = SSQL + " And OM.Trans_No='" + Transaction_No + "'";
            SSQL = SSQL + " And OS.Trans_No='" + Transaction_No + "'";
        }
        if (MachineName != "")
        {
            SSQL = SSQL + " And OS.MachineName='" + MachineName + "'";
        }
        if (ColorName != "")
        {
            SSQL = SSQL + " And OM.Color='" + ColorName + "'";
        }

        if (FromDate != "" && ToDate != "")
        {
            SSQL = SSQL + " And CONVERT(DATETIME,OM.Trans_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(DATETIME,OM.Trans_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
            SSQL = SSQL + " And CONVERT(DATETIME,OS.Trans_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(DATETIME,OS.Trans_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
        }
        Da_Check = objdata.RptEmployeeMultipleDetails(SSQL);


        if (Da_Check.Rows.Count > 0)
        {
            DataTable dtdCompanyAddress = new DataTable();
            SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
            dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);



            for (int i = 0; i < Da_Check.Rows.Count; i++)
            {
                Da_Check.Rows[i]["CompanyName"] = dtdCompanyAddress.Rows[0]["Cname"].ToString();
                Da_Check.Rows[i]["LocationName"] = dtdCompanyAddress.Rows[0]["location"].ToString();
                Da_Check.Rows[i]["CompanyCode"] = SessionCcode;
                Da_Check.Rows[i]["LocationCode"] = SessionLcode;
                Da_Check.Rows[i]["Address1"] = dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString();
                Da_Check.Rows[i]["Address2"] = dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString();

            }

            ds.Tables.Add(Da_Check);
            ReportDocument report = new ReportDocument();
            report.Load(Server.MapPath("~/crystal/OEDetails.rpt"));

            report.SetDatabaseLogon("username", "password", "sa", "Altius2005");
            report.Database.Tables[0].SetDataSource(ds.Tables[0]);

            if (FromDate != "" && ToDate != "")
            {
                report.DataDefinition.FormulaFields["FromDate"].Text = "'" + FromDate.ToString() + "'";
                report.DataDefinition.FormulaFields["ToDate"].Text = "'" + ToDate.ToString() + "'";
            }

            report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = report;

        }
        else
        {
            Errflag = false;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
        }
        if (!Errflag)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
            lblUploadSuccessfully.Text = "No Records Matched..";
        }

    }
    public void Winding_Details_Report()
    {
        DataTable Da_Check = new DataTable();
        if (FromDate != "")
        {
            frmDate = Convert.ToDateTime(FromDate);
        }
        if (ToDate != "")
        {
            toDate = Convert.ToDateTime(ToDate);
        }




        //SSQL = "select '' as CompanyName,'' as LocationName,'' as CompanyCode,'' as LocationCode,'' as Address1,'' as Address2,";
        //SSQL = SSQL + " WM.Trans_No,WM.Trans_Date,WM.Unit,ME.EmpName,WM.Color,WM.LotNo,WM.Shift,WM.ToGoDownName as Mixing_issue,WS.MachineName,";
        //SSQL = SSQL + " MM.Speed,WS.CountName,'0' as StdProd,WS.Production as ActProd,WS.Utilization,WS.Efficiency,WS.ConeWeight,";
        //SSQL = SSQL + " WS.HardWaste  from Winding_Main WM inner join Winding_Sub WS on WM.Trans_No=WS.Trans_No ";
        //SSQL = SSQL + " inner join MstEmployee ME on WM.PrepaidBy =ME.EmpCode inner join MstMachine MM on WS.MachineName=MM.MachineName  ";
        //SSQL = SSQL + " Where WM.Ccode='" + SessionCcode + "' And WM.Lcode='" + SessionLcode + "'";
        //SSQL = SSQL + " And WM.FinYearCode='" + SessionFinYearCode + "'";
        //SSQL = SSQL + " And WS.Ccode='" + SessionCcode + "' And WS.Lcode='" + SessionLcode + "'";
        //SSQL = SSQL + " And ME.Ccode='" + SessionCcode + "' And ME.Lcode='" + SessionLcode + "'";
        //SSQL = SSQL + " And MM.Ccode='" + SessionCcode + "' And MM.Lcode='" + SessionLcode + "'";
        //SSQL = SSQL + " And WS.FinYearCode='" + SessionFinYearCode + "'";
        //if (Transaction_No != "")
        //{
        //    SSQL = SSQL + " And WM.Trans_No='" + Transaction_No + "'";
        //    SSQL = SSQL + " And WS.Trans_No='" + Transaction_No + "'";
        //}
        //if (MachineName != "")
        //{
        //    SSQL = SSQL + " And WS.MachineName='" + MachineName + "'";
        //}
        //if (ColorName != "")
        //{
        //    SSQL = SSQL + " And WM.Color='" + ColorName + "'";
        //}

        //if (FromDate != "" && ToDate != "")
        //{
        //    SSQL = SSQL + " And CONVERT(DATETIME,WM.Trans_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(DATETIME,WM.Trans_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
        //    SSQL = SSQL + " And CONVERT(DATETIME,WS.Trans_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(DATETIME,WS.Trans_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
        //}



        SSQL = "Select '' as CompanyName,'' as LocationName,'' as CompanyCode,'' as LocationCode,'' as Address1,'' as Address2,";
        SSQL = SSQL + " MachineName,Side as Unit,LotNo,CountName as Color,isnull([Shift I],0)as [shift I] ,";
        SSQL = SSQL + "isnull([Shift II],0)as [shift II],isnull([Shift III],0)as [shift III],(isnull([shift I],0)+isnull([shift II],0)+isnull([shift III],0)) as Total from ";
        SSQL = SSQL + "(Select CM.Shift,CS.MachineName,CS.Side,CS.LotNo,CS.CountName,";
        SSQL = SSQL + "ISNULL(sum(Convert(Decimal(18,2),CS.Production)),0)as Production";
        SSQL = SSQL + " from Winding_Main CM inner join Winding_Sub CS on CM.Trans_No = CS.Trans_No ";


        if (MachineName != "")
        {
            SSQL = SSQL + " And CS.MachineName='" + MachineName + "'";
        }
        if (LotNo != "")
        {
            SSQL = SSQL + " And CS.LotNo='" + ColorName + "'";
        }
        if (FromDate != "" && ToDate != "")
        {
            SSQL = SSQL + " And CONVERT(DATETIME,CM.Trans_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(DATETIME,CM.Trans_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
            SSQL = SSQL + " And CONVERT(DATETIME,CS.Trans_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(DATETIME,CS.Trans_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
        }

        SSQL = SSQL + "Group by CM.Shift,CS.MachineName,CS.Side,CS.LotNo,CS.CountName) src pivot(max(Production) for";
        SSQL = SSQL + " Shift in ([Shift I],[Shift II],[Shift III])) as MaxBookingDays";

        Da_Check = objdata.RptEmployeeMultipleDetails(SSQL);


        if (Da_Check.Rows.Count > 0)
        {
            DataTable dtdCompanyAddress = new DataTable();
            SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
            dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);



            for (int i = 0; i < Da_Check.Rows.Count; i++)
            {
                Da_Check.Rows[i]["CompanyName"] = dtdCompanyAddress.Rows[0]["Cname"].ToString();
                Da_Check.Rows[i]["LocationName"] = dtdCompanyAddress.Rows[0]["location"].ToString();
                Da_Check.Rows[i]["CompanyCode"] = SessionCcode;
                Da_Check.Rows[i]["LocationCode"] = SessionLcode;
                Da_Check.Rows[i]["Address1"] = dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString();
                Da_Check.Rows[i]["Address2"] = dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString();

            }

            ds.Tables.Add(Da_Check);
            ReportDocument report = new ReportDocument();

            report.Load(Server.MapPath("~/crystal/WindingDailyProductionShiftwise.rpt"));
            //  report.Load(Server.MapPath("~/crystal/WindingDetail.rpt"));

            report.SetDatabaseLogon("username", "password", "sa", "Altius2005");
            report.Database.Tables[0].SetDataSource(ds.Tables[0]);

            if (FromDate != "" && ToDate != "")
            {
                report.DataDefinition.FormulaFields["FromDate"].Text = "'" + FromDate.ToString() + "'";
                report.DataDefinition.FormulaFields["ToDate"].Text = "'" + ToDate.ToString() + "'";
            }

            report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = report;

        }
        else
        {
            Errflag = false;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
        }
        if (!Errflag)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
            lblUploadSuccessfully.Text = "No Records Matched..";
        }

    }


    //public void Winding_Details_Report()
    //{
    //    DataTable Da_Check = new DataTable();
    //    if (FromDate != "")
    //    {
    //        frmDate = Convert.ToDateTime(FromDate);
    //    }
    //    if (ToDate != "")
    //    {
    //        toDate = Convert.ToDateTime(ToDate);
    //    }




    //    SSQL = "select '' as CompanyName,'' as LocationName,'' as CompanyCode,'' as LocationCode,'' as Address1,'' as Address2,";
    //    SSQL = SSQL + " WM.Trans_No,WM.Trans_Date,WM.Unit,ME.EmpName,WM.Color,WM.LotNo,WM.Shift,WM.ToGoDownName as Mixing_issue,WS.MachineName,";
    //    SSQL = SSQL + " MM.Speed,WS.CountName,WS.StdProd,WS.ActProd,WS.StopMin,WS.Utilization,WS.Efficiency,WS.ConeWeight,";
    //    SSQL = SSQL + " WS.NoofDoff, WS.NetWeight,WS.HardWaste  from Winding_Main WM inner join Winding_Sub WS on WM.Trans_No=WS.Trans_No ";
    //    SSQL = SSQL + " inner join MstEmployee ME on WM.PrepaidBy =ME.EmpCode inner join MstMachine MM on WS.MachineName=MM.MachineName  ";
    //    SSQL = SSQL + " Where WM.Ccode='" + SessionCcode + "' And WM.Lcode='" + SessionLcode + "'";
    //    SSQL = SSQL + " And WM.FinYearCode='" + SessionFinYearCode + "'";
    //    SSQL = SSQL + " And WS.Ccode='" + SessionCcode + "' And WS.Lcode='" + SessionLcode + "'";
    //    SSQL = SSQL + " And ME.Ccode='" + SessionCcode + "' And ME.Lcode='" + SessionLcode + "'";
    //    SSQL = SSQL + " And MM.Ccode='" + SessionCcode + "' And MM.Lcode='" + SessionLcode + "'";
    //    SSQL = SSQL + " And WS.FinYearCode='" + SessionFinYearCode + "'";
    //    if (Transaction_No != "")
    //    {
    //        SSQL = SSQL + " And WM.Trans_No='" + Transaction_No + "'";
    //        SSQL = SSQL + " And WS.Trans_No='" + Transaction_No + "'";
    //    }
    //    if (MachineName != "")
    //    {
    //        SSQL = SSQL + " And WS.MachineName='" + MachineName + "'";
    //    }
    //    if (ColorName != "")
    //    {
    //        SSQL = SSQL + " And WM.Color='" + ColorName + "'";
    //    }

    //    if (FromDate != "" && ToDate != "")
    //    {
    //        SSQL = SSQL + " And CONVERT(DATETIME,WM.Trans_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(DATETIME,WM.Trans_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
    //        SSQL = SSQL + " And CONVERT(DATETIME,WS.Trans_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(DATETIME,WS.Trans_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
    //    }
    //    Da_Check = objdata.RptEmployeeMultipleDetails(SSQL);


    //    if (Da_Check.Rows.Count > 0)
    //    {
    //        DataTable dtdCompanyAddress = new DataTable();
    //        SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
    //        dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);



    //        for (int i = 0; i < Da_Check.Rows.Count; i++)
    //        {
    //            Da_Check.Rows[i]["CompanyName"] = dtdCompanyAddress.Rows[0]["Cname"].ToString();
    //            Da_Check.Rows[i]["LocationName"] = dtdCompanyAddress.Rows[0]["location"].ToString();
    //            Da_Check.Rows[i]["CompanyCode"] = SessionCcode;
    //            Da_Check.Rows[i]["LocationCode"] = SessionLcode;
    //            Da_Check.Rows[i]["Address1"] = dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString();
    //            Da_Check.Rows[i]["Address2"] = dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString();

    //        }

    //        ds.Tables.Add(Da_Check);
    //        ReportDocument report = new ReportDocument();
    //        report.Load(Server.MapPath("~/crystal/WindingDetail.rpt"));

    //        report.SetDatabaseLogon("username", "password", "sa", "Altius2005");
    //        report.Database.Tables[0].SetDataSource(ds.Tables[0]);

    //        if (FromDate != "" && ToDate != "")
    //        {
    //            report.DataDefinition.FormulaFields["FromDate"].Text = "'" + FromDate.ToString() + "'";
    //            report.DataDefinition.FormulaFields["ToDate"].Text = "'" + ToDate.ToString() + "'";
    //        }

    //        report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
    //        CrystalReportViewer1.ReportSource = report;

    //    }
    //    else
    //    {
    //        Errflag = false;
    //        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
    //    }
    //    if (!Errflag)
    //    {
    //        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
    //        lblUploadSuccessfully.Text = "No Records Matched..";
    //    }

    //}
    //public void Winding_Details_Report()
    //{
    //    DataTable Da_Check = new DataTable();
    //    if (FromDate != "")
    //    {
    //        frmDate = Convert.ToDateTime(FromDate);
    //    }
    //    if (ToDate != "")
    //    {
    //        toDate = Convert.ToDateTime(ToDate);
    //    }




    //    SSQL = "select '' as CompanyName,'' as LocationName,'' as CompanyCode,'' as LocationCode,'' as Address1,'' as Address2,";
    //    SSQL = SSQL + " WM.Trans_No,WM.Trans_Date,WM.Unit,ME.EmpName,WM.Color,WM.LotNo,WM.Shift,WM.ToGoDownName as Mixing_issue,WS.MachineName,";
    //    SSQL = SSQL + " MM.Speed,WS.CountName,'0' as StdProd,WS.Production as ActProd,WS.Utilization,WS.Efficiency,WS.ConeWeight,";
    //    SSQL = SSQL + " WS.HardWaste  from Winding_Main WM inner join Winding_Sub WS on WM.Trans_No=WS.Trans_No ";
    //    SSQL = SSQL + " inner join MstEmployee ME on WM.PrepaidBy =ME.EmpCode inner join MstMachine MM on WS.MachineName=MM.MachineName  ";
    //    SSQL = SSQL + " Where WM.Ccode='" + SessionCcode + "' And WM.Lcode='" + SessionLcode + "'";
    //    SSQL = SSQL + " And WM.FinYearCode='" + SessionFinYearCode + "'";
    //    SSQL = SSQL + " And WS.Ccode='" + SessionCcode + "' And WS.Lcode='" + SessionLcode + "'";
    //    SSQL = SSQL + " And ME.Ccode='" + SessionCcode + "' And ME.Lcode='" + SessionLcode + "'";
    //    SSQL = SSQL + " And MM.Ccode='" + SessionCcode + "' And MM.Lcode='" + SessionLcode + "'";
    //    SSQL = SSQL + " And WS.FinYearCode='" + SessionFinYearCode + "'";
    //    if (Transaction_No != "")
    //    {
    //        SSQL = SSQL + " And WM.Trans_No='" + Transaction_No + "'";
    //        SSQL = SSQL + " And WS.Trans_No='" + Transaction_No + "'";
    //    }
    //    if (MachineName != "")
    //    {
    //        SSQL = SSQL + " And WS.MachineName='" + MachineName + "'";
    //    }
    //    if (ColorName != "")
    //    {
    //        SSQL = SSQL + " And WM.Color='" + ColorName + "'";
    //    }

    //    if (FromDate != "" && ToDate != "")
    //    {
    //        SSQL = SSQL + " And CONVERT(DATETIME,WM.Trans_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(DATETIME,WM.Trans_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
    //        SSQL = SSQL + " And CONVERT(DATETIME,WS.Trans_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(DATETIME,WS.Trans_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
    //    }
    //    Da_Check = objdata.RptEmployeeMultipleDetails(SSQL);


    //    if (Da_Check.Rows.Count > 0)
    //    {
    //        DataTable dtdCompanyAddress = new DataTable();
    //        SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
    //        dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);



    //        for (int i = 0; i < Da_Check.Rows.Count; i++)
    //        {
    //            Da_Check.Rows[i]["CompanyName"] = dtdCompanyAddress.Rows[0]["Cname"].ToString();
    //            Da_Check.Rows[i]["LocationName"] = dtdCompanyAddress.Rows[0]["location"].ToString();
    //            Da_Check.Rows[i]["CompanyCode"] = SessionCcode;
    //            Da_Check.Rows[i]["LocationCode"] = SessionLcode;
    //            Da_Check.Rows[i]["Address1"] = dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString();
    //            Da_Check.Rows[i]["Address2"] = dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString();

    //        }

    //        ds.Tables.Add(Da_Check);
    //        ReportDocument report = new ReportDocument();
    //        report.Load(Server.MapPath("~/crystal/WindingDetail.rpt"));

    //        report.SetDatabaseLogon("username", "password", "sa", "Altius2005");
    //        report.Database.Tables[0].SetDataSource(ds.Tables[0]);

    //        if (FromDate != "" && ToDate != "")
    //        {
    //            report.DataDefinition.FormulaFields["FromDate"].Text = "'" + FromDate.ToString() + "'";
    //            report.DataDefinition.FormulaFields["ToDate"].Text = "'" + ToDate.ToString() + "'";
    //        }

    //        report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
    //        CrystalReportViewer1.ReportSource = report;

    //    }
    //    else
    //    {
    //        Errflag = false;
    //        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
    //    }
    //    if (!Errflag)
    //    {
    //        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
    //        lblUploadSuccessfully.Text = "No Records Matched..";
    //    }

    //}
    public void Packing_Details_Report()
    {
        DataTable Da_Check = new DataTable();
        DataTable Da_Ondate = new DataTable();
        if (FromDate != "")
        {
            frmDate = Convert.ToDateTime(FromDate);
        }
        if (ToDate != "")
        {
            toDate = Convert.ToDateTime(ToDate);
        }

        SSQL = "";
        SSQL = SSQL + "select PS.CountID,PS.CountName,PS.Color,PS.LotNo,PS.BagKG,'' as CompanyName,'' as LocationName,'' as CompanyCode,'' as LocationCode,'' as Address1,'' as Address2, ";
        SSQL = SSQL + "SUM(cast(PS.NoofBag as decimal(18,2))) as UptodateBag,SUM(cast(PS.Qty as decimal(18,2))) as UptodateQty,'' as OndateBag,'' as OndateQty ";
        SSQL = SSQL + "from Packing_Main PM inner join Packing_Sub PS on PM.Trans_No=PS.Trans_No and PM.Lcode=PS.Lcode ";
        SSQL = SSQL + "where CONVERT(datetime,PM.Trans_Date,103)>=CONVERT(datetime,'" + FromDate + "',103) and ";
        SSQL = SSQL + "CONVERT(datetime,PM.Trans_Date,103)<=CONVERT(datetime,'" + ToDate + "',103) and PM.Ccode='" + SessionCcode + "' And PM.Lcode='" + SessionLcode + "' and ";
        SSQL = SSQL + "PS.Ccode='" + SessionCcode + "' And PS.Lcode='" + SessionLcode + "' and PM.FinYearCode='" + SessionFinYearCode + "' ";

        if (Transaction_No != "")
        {
            SSQL = SSQL + " And PS.CountID='" + Transaction_No + "'";
           
        }

        if (LotNo != "")
        {
            SSQL = SSQL + " And PS.LotNo='" + LotNo + "'";
        }
        
        if (ColorName != "")
        {
            SSQL = SSQL + " And PS.Color='" + ColorName + "'";
        }

        if (Supervisor != "")
        {
            SSQL = SSQL + " And PM.PrepaidBy='" + Supervisor + "'";
        }

        if (FromDate != "" && ToDate != "")
        {
            SSQL = SSQL + " And CONVERT(DATETIME,PM.Trans_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(DATETIME,PM.Trans_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
            SSQL = SSQL + " And CONVERT(DATETIME,PS.Trans_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(DATETIME,PS.Trans_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
        }
        SSQL = SSQL + "group by PS.CountID,PS.CountName,PS.Color,PS.LotNo,PS.BagKG ";
        Da_Check = objdata.RptEmployeeMultipleDetails(SSQL);


        if (Da_Check.Rows.Count > 0)
        {
            for (int i = 0; i < Da_Check.Rows.Count; i++)
            {
                SSQL = "select ISNULL(SUM(cast(PS.NoofBag as decimal(18,2))), '0') as OndateBag,isnull(SUM(cast(PS.Qty as decimal(18,2))), '0') as OndateQty ";
                SSQL = SSQL + "from Packing_Main PM inner join Packing_Sub PS on PM.Trans_No=PS.Trans_No and PM.Lcode=PS.Lcode ";
                SSQL = SSQL + "where CONVERT(datetime,PM.Trans_Date,103)=CONVERT(datetime,'" + FromDate + "',103) and ";
                SSQL = SSQL + "CONVERT(datetime,PM.Trans_Date,103)=CONVERT(datetime,'" + FromDate + "',103) and PS.CountID='" + Da_Check.Rows[i]["CountID"].ToString() + "' and ";
                SSQL = SSQL + "PS.Color='" + Da_Check.Rows[i]["Color"].ToString() + "' and LotNo='" + Da_Check.Rows[i]["LotNo"].ToString() + "'and BagKG='" + Da_Check.Rows[i]["BagKG"].ToString() + "' ";
                Da_Ondate = objdata.RptEmployeeMultipleDetails(SSQL);

                if (Da_Ondate.Rows.Count > 0)
                {
                    Da_Check.Rows[i]["OndateBag"] = Da_Ondate.Rows[0]["OndateBag"].ToString();
                    Da_Check.Rows[i]["OndateQty"] = Da_Ondate.Rows[0]["OndateQty"].ToString();
                }
                else
                {
                    Da_Check.Rows[i]["OndateBag"] = "0";
                    Da_Check.Rows[i]["OndateQty"] = "0";
                }
                
            }
            DataTable dtdCompanyAddress = new DataTable();
            SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
            dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);



            for (int i = 0; i < Da_Check.Rows.Count; i++)
            {
                Da_Check.Rows[i]["CompanyName"] = dtdCompanyAddress.Rows[0]["Cname"].ToString();
                Da_Check.Rows[i]["LocationName"] = dtdCompanyAddress.Rows[0]["location"].ToString();
                Da_Check.Rows[i]["CompanyCode"] = SessionCcode;
                Da_Check.Rows[i]["LocationCode"] = SessionLcode;
                Da_Check.Rows[i]["Address1"] = dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString();
                Da_Check.Rows[i]["Address2"] = dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString();

            }

            ds.Tables.Add(Da_Check);
            ReportDocument report = new ReportDocument();
            report.Load(Server.MapPath("~/crystal/Packing.rpt"));

            report.SetDatabaseLogon("username", "password", "sa", "Altius2005");
            report.Database.Tables[0].SetDataSource(ds.Tables[0]);

            if (FromDate != "" && ToDate != "")
            {
                report.DataDefinition.FormulaFields["FromDate"].Text = "'" + FromDate.ToString() + "'";
                report.DataDefinition.FormulaFields["ToDate"].Text = "'" + ToDate.ToString() + "'";
            }

            report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = report;

        }
        else
        {
            Errflag = false;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
        }
        if (!Errflag)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
            lblUploadSuccessfully.Text = "No Records Matched..";
        }

    }

    public void Issue_Details_Report()
    {
        DataTable Da_Check = new DataTable();
        if (FromDate != "")
        {
            frmDate = Convert.ToDateTime(FromDate);
        }
        if (ToDate != "")
        {
            toDate = Convert.ToDateTime(ToDate);
        }

        SSQL = "Select '' as CompanyName,'' as LocationName,'' as CompanyCode,'' as LocationCode,'' as Address1,'' as Address2,";
        SSQL = SSQL + " IEM.YarnLotNo,IEM.TotalBale, CM.BaleWeight as TotalWeight,IEM.ColorName,";
        SSQL = SSQL + " IES.LotNo,IES.LotDate,IES.InvoiceNo,IES.InvoiceDate,IES.CountCode,IES.CountName,IES.VarietyCode,IES.VarietyName,";
        SSQL = SSQL + " IES.Supp_Code, IES.Supp_Name,sum(IES.BaleWeight) as BaleWeight,IES.GoDownID,IES.GoDownName,IES.Percents as BaleNo,CM.CandyWeight as NoOfBale";
        //SSQL = SSQL + " CM.CandyWeight as Rate";
        SSQL = SSQL + " from Issue_Entry_Main IEM inner join Issue_Entry_Main_Sub IES on IEM.Issue_Entry_No=IES.Issue_Entry_No ";
        SSQL = SSQL + " inner join CottonInwards CM on IES.LotNo=CM.LotNo";
        SSQL = SSQL + " Where IEM.Ccode='" + SessionCcode + "' And IEM.Lcode='" + SessionLcode + "'";
        SSQL = SSQL + " And IEM.FinYearCode='" + SessionFinYearCode + "'";
        SSQL = SSQL + " And IES.Ccode='" + SessionCcode + "' And IES.Lcode='" + SessionLcode + "'";
        SSQL = SSQL + " And IES.FinYearCode='" + SessionFinYearCode + "'";
        //SSQL = SSQL + " And CM.Ccode='" + SessionCcode + "' And CM.Lcode='" + SessionLcode + "'";
        //SSQL = SSQL + " And CM.FinYearCode='" + SessionFinYearCode + "'";
        if (TransNo != "")
        {
            SSQL = SSQL + " And IEM.Issue_Entry_No='" + TransNo + "'";
            SSQL = SSQL + " And IES.Issue_Entry_No='" + TransNo + "'";
        }
        if (LotNo != "")
        {
            SSQL = SSQL + " And IES.LotNo='" + LotNo + "'";
        }
        if (VarietyCode != "")
        {
            SSQL = SSQL + " And IES.VarietyCode='" + VarietyCode + "'";
        }

        if (FromDate != "" && ToDate != "")
        {
            SSQL = SSQL + " And CONVERT(DATETIME,IEM.Issue_Entry_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(DATETIME,IEM.Issue_Entry_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
            SSQL = SSQL + " And CONVERT(DATETIME,IES.Issue_Entry_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(DATETIME,IES.Issue_Entry_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
        }

         SSQL = SSQL + " group by IEM.YarnLotNo,IEM.TotalBale, CM.BaleWeight,IEM.ColorName, ";
         SSQL = SSQL + " IES.LotNo,IES.LotDate,IES.InvoiceNo,IES.InvoiceDate,IES.CountCode,IES.CountName,IES.VarietyCode, ";
         SSQL = SSQL + " IES.VarietyName, IES.Supp_Code, IES.Supp_Name ,IES.GoDownID,IES.GoDownName,IES.Percents,CM.CandyWeight ";
      
 
 

 
        Da_Check = objdata.RptEmployeeMultipleDetails(SSQL);


        if (Da_Check.Rows.Count > 0)
        {
            DataTable dtdCompanyAddress = new DataTable();
            SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
            dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);



            for (int i = 0; i < Da_Check.Rows.Count; i++)
            {
                Da_Check.Rows[i]["CompanyName"] = dtdCompanyAddress.Rows[0]["Cname"].ToString();
                Da_Check.Rows[i]["LocationName"] = dtdCompanyAddress.Rows[0]["location"].ToString();
                Da_Check.Rows[i]["CompanyCode"] = SessionCcode;
                Da_Check.Rows[i]["LocationCode"] = SessionLcode;
                Da_Check.Rows[i]["Address1"] = dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString();
                Da_Check.Rows[i]["Address2"] = dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString();

            }

            ds.Tables.Add(Da_Check);
            ReportDocument report = new ReportDocument();
            report.Load(Server.MapPath("~/crystal/IssueDetailReport.rpt"));

            report.SetDatabaseLogon("username", "password", "sa", "Altius2005");
            report.Database.Tables[0].SetDataSource(ds.Tables[0]);

            
            report.DataDefinition.FormulaFields["FromDate"].Text = "'" + FromDate.ToString() + "'";
            report.DataDefinition.FormulaFields["ToDate"].Text = "'" + ToDate.ToString() + "'";
            

            report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = report;

        }
        else
        {
            Errflag = false;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
        }
        if (!Errflag)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
            lblUploadSuccessfully.Text = "No Records Matched..";
        }

    }


    //Cotton_Stock_Details_Report start
    public void Cotton_Stock_Details_Report()
    {
        
        DataTable dt = new DataTable();
        string query = "";
        query = "Select '' as CompanyName,'' as LocationName,'' as CompanyCode,'' as LocationCode,'' as Address1,'' as Address2,";
        query = query + " ST.LotNo,(sum(isnull(ST.PO_Qty , '0'))  - sum(isnull(ST.Issue_Qty , '0')) ) as TotalQty,";
        query = query + "ST.VariteyCode,ST.VariteyName,CN.CandyWeight";
        query = query + " from Stock_Transaction_Ledger ST inner join CottonInwards CN on ST.LotNo=CN.LotNo";
        query = query + " Where ST.Ccode='" + SessionCcode + "' and ST.Lcode='" + SessionLcode + "'";
        query = query + " And CN.Ccode='" + SessionCcode + "' and CN.Lcode='" + SessionLcode + "'";
        if (LotNo.ToString() != "")
        {
            query = query + " and LotNo='" + LotNo + "' ";
        }
        query = query + " Group by ST.LotNo,ST.VariteyCode,ST.VariteyName,CN.CandyWeight having (sum(isnull(ST.PO_Qty , '0'))  - sum(isnull(ST.Issue_Qty , '0')) ) > 0";
        


        dt = objdata.RptEmployeeMultipleDetails(query);
        if (dt.Rows.Count > 0)
        {

            DataTable dtdCompanyAddress = new DataTable();
            SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
            dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);



            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dt.Rows[i]["CompanyName"] = dtdCompanyAddress.Rows[0]["Cname"].ToString();
                dt.Rows[i]["LocationName"] = dtdCompanyAddress.Rows[0]["location"].ToString();
                dt.Rows[i]["CompanyCode"] = SessionCcode;
                dt.Rows[i]["LocationCode"] = SessionLcode;
                dt.Rows[i]["Address1"] = dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString();
                dt.Rows[i]["Address2"] = dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString();

            }

            DataSet ds1 = new DataSet();
            ds1.Tables.Add(dt);
            RD_Report.Load(Server.MapPath("~/crystal/CottonStockDetails.rpt"));
            RD_Report.SetDataSource(ds1.Tables[0]);

            //DataTable dtdCompanyAddress = new DataTable();
            //DataTable Close_Det = new DataTable();

            //SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
            //dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);
            //if (dtdCompanyAddress.Rows.Count != 0)
            //{
            //    RD_Report.DataDefinition.FormulaFields["CompanyName"].Text = "'" + dtdCompanyAddress.Rows[0]["Cname"].ToString() + "'";
            //    RD_Report.DataDefinition.FormulaFields["Address"].Text = "'" + dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString() + "'";
            //    RD_Report.DataDefinition.FormulaFields["Address2"].Text = "'" + dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString() + "'";
            //}
            

            RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = RD_Report;
            CrystalReportViewer1.RefreshReport();
            CrystalReportViewer1.DataBind();
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
            lblUploadSuccessfully.Text = "No Records Matched..";
        }
    }

    //Cotton_Stock_Details_Report end

    public void ShiftWise_Drawing_Details_Report()
    {
        DataTable AutoDT = new DataTable();
        DataTable dt = new DataTable();

        if (FromDate != "")
        {
            frmDate = Convert.ToDateTime(FromDate);
        }

        if (ToDate != "")
        {
            toDate = Convert.ToDateTime(ToDate);
        }

        if (RptName == "ShiftWise Drawing Details Report")
        {
            SSQL = "Select '' as CompanyName,'' as Address1,'' as Address2,Trans_Date,Unit,MachineName,'' as Hank,'' as Speed,'' as StdProd,";
            SSQL = SSQL + "isnull([shift I],0) as [shift I],";
            SSQL = SSQL + "isnull([shift II],0) as [shift II],isnull([shift III],0) as[shift III],";
            SSQL = SSQL + "(isnull([shift I],0)+isnull([shift II],0)+isnull([shift III],0)) as Total,";
            SSQL = SSQL + "'' as Utilization,'' as Waste,'' as Breakage";
            SSQL = SSQL + " from (Select CM.Trans_Date,CM.Unit,CM.Shift,CS.MachineName,";
            SSQL = SSQL + "isnull(sum(CONVERT(Decimal(18,2),CS.ActProduction)),0) as ActProduction";
            SSQL = SSQL + " from Drawing_Main CM inner join Drawing_Main_Sub CS on CM.Trans_No = CS.Trans_No ";
            SSQL = SSQL + " Where CM.Ccode='" + SessionCcode + "' And CM.Lcode='" + SessionLcode + "'";
            SSQL = SSQL + " And CM.FinYearCode='" + SessionFinYearCode + "'";
            SSQL = SSQL + " And CS.Ccode='" + SessionCcode + "' And CS.Lcode='" + SessionLcode + "'";
            SSQL = SSQL + " And CS.FinYearCode='" + SessionFinYearCode + "'";
            if (FromDate != "")
            {
                SSQL = SSQL + " And CONVERT(DATETIME,CM.Trans_Date, 103)=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";
                // SSQL = SSQL + "And CONVERT(DATETIME,CM.Trans_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
                SSQL = SSQL + " And CONVERT(DATETIME,CS.Trans_Date, 103)=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";
                // SSQL = SSQL + "And CONVERT(DATETIME,CS.Trans_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
            }
            if (MachineName != "")
            {
                SSQL = SSQL + " And CS.MachineCode='" + MachineName + "'";
            }
            if (ColorName != "")
            {
                SSQL = SSQL + " And CM.Color='" + ColorName + "'";
            }
            if (LotNo != "")
            {
                SSQL = SSQL + " And CM.LotNo='" + LotNo + "'";
            }
            SSQL = SSQL + " Group by CM.Trans_Date,CM.Unit,CM.Shift,CS.MachineName";
            SSQL = SSQL + ")src pivot(";
            SSQL = SSQL + " max(ActProduction) for Shift in ([shift I], [shift II], [shift III])) as MaxBookingDays order by Trans_Date asc";
            AutoDT = objdata.RptEmployeeMultipleDetails(SSQL);




            if (AutoDT.Rows.Count != 0)
            {
                for (int i = 0; i < AutoDT.Rows.Count; i++)
                {
                    SSQL = "Select CS.Speed,CS.Hank,CS.StdProduction,isnull(sum(CONVERT(Decimal(18,2),CS.Utilization)),0) as Utilization,";
                    SSQL = SSQL + "(isnull(sum(CONVERT(Decimal(18,2),CS.Silver_Wastage)),0)+isnull(sum(CONVERT(Decimal(18,2),CS.FS)),0)) as Waste,";
                    SSQL = SSQL + "isnull(sum(CONVERT(Decimal(18,2),CS.Stop_Page_Mintus)),0) as Stop_Page_Mintus";
                    SSQL = SSQL + " from Drawing_Main CM inner join Drawing_Main_Sub CS on CM.Trans_No = CS.Trans_No";
                    SSQL = SSQL + " Where CM.Ccode='" + SessionCcode + "' And CM.Lcode='" + SessionLcode + "'";
                    SSQL = SSQL + " And CM.FinYearCode='" + SessionFinYearCode + "'";
                    SSQL = SSQL + " And CS.Ccode='" + SessionCcode + "' And CS.Lcode='" + SessionLcode + "'";
                    SSQL = SSQL + " And CS.FinYearCode='" + SessionFinYearCode + "'";
                    SSQL = SSQL + " And CM.Trans_Date='" + AutoDT.Rows[i]["Trans_Date"].ToString() + "'";
                    SSQL = SSQL + " And CS.Trans_Date='" + AutoDT.Rows[i]["Trans_Date"].ToString() + "'";
                    SSQL = SSQL + " And CM.Unit='" + AutoDT.Rows[i]["Unit"].ToString() + "'";
                    SSQL = SSQL + " And CS.MachineName='" + AutoDT.Rows[i]["MachineName"].ToString() + "'";
                    SSQL = SSQL + " Group by CS.Speed,CS.Hank,CS.StdProduction";
                    dt = objdata.RptEmployeeMultipleDetails(SSQL);

                    if (dt.Rows.Count != 0)
                    {
                        AutoDT.Rows[i]["Speed"] = dt.Rows[0]["Speed"].ToString();
                        AutoDT.Rows[i]["Hank"] = dt.Rows[0]["Hank"].ToString();
                        AutoDT.Rows[i]["StdProd"] = dt.Rows[0]["StdProduction"].ToString();
                        AutoDT.Rows[i]["Waste"] = dt.Rows[0]["Waste"].ToString();
                        AutoDT.Rows[i]["Breakage"] = dt.Rows[0]["Stop_Page_Mintus"].ToString();
                        AutoDT.Rows[i]["Utilization"] = dt.Rows[0]["Utilization"].ToString();

                    }


                    DataTable dtdCompanyAddress = new DataTable();
                    SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                    dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);
                    if (dtdCompanyAddress.Rows.Count != 0)
                    {
                        AutoDT.Rows[i]["CompanyName"] = "" + dtdCompanyAddress.Rows[0]["Cname"].ToString() + " - " + SessionLcode + "";
                        AutoDT.Rows[i]["Address1"] = "" + dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString() + "";
                        AutoDT.Rows[i]["Address2"] = "" + dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString() + "";
                    }
                }

                DataSet ds1 = new DataSet();
                ds1.Tables.Add(AutoDT);

                RD_Report.Load(Server.MapPath("~/crystal/DrawingDailyProductionShiftwise.rpt"));
                RD_Report.SetDataSource(ds1.Tables[0]);
                //Company Details Add

                if (FromDate != "")
                {
                    RD_Report.DataDefinition.FormulaFields["FromDate"].Text = "'" + FromDate.ToString() + "'";
                }

                RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
                CrystalReportViewer1.ReportSource = RD_Report;
                CrystalReportViewer1.RefreshReport();
                CrystalReportViewer1.DataBind();

            }
            else
            {
                Errflag = false;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
            }
        }

        if (RptName == "Drawing Abstract Details Report")
        {
            SSQL = "Select '' as CompanyName,'' as Address1,'' as Address2,CM.Unit,";
            SSQL = SSQL + "isnull(sum(CONVERT(Decimal(18,2),CS.ActProduction)),0) as UpToDateProd,";
            SSQL = SSQL + "isnull(sum(CONVERT(Decimal(18,2),CS.Stop_Page_Mintus)),0) as UpToDateBreak,";
            SSQL = SSQL + "'' as OnDateProd,'' as OnDateBreak";
            SSQL = SSQL + " from Drawing_Main CM inner join Drawing_Main_Sub CS on CM.Trans_No = CS.Trans_No ";
            SSQL = SSQL + " Where CM.Ccode='" + SessionCcode + "' And CM.Lcode='" + SessionLcode + "'";
            SSQL = SSQL + " And CM.FinYearCode='" + SessionFinYearCode + "'";
            SSQL = SSQL + " And CS.Ccode='" + SessionCcode + "' And CS.Lcode='" + SessionLcode + "'";
            SSQL = SSQL + " And CS.FinYearCode='" + SessionFinYearCode + "'";
            if (FromDate != "")
            {
                SSQL = SSQL + "And CONVERT(DATETIME,CM.Trans_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";
                SSQL = SSQL + "And CONVERT(DATETIME,CM.Trans_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
                SSQL = SSQL + "And CONVERT(DATETIME,CS.Trans_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";
                SSQL = SSQL + "And CONVERT(DATETIME,CS.Trans_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
            }
            if (MachineName != "")
            {
                SSQL = SSQL + " And CS.MachineCode='" + MachineName + "'";
            }
            if (ColorName != "")
            {
                SSQL = SSQL + " And CM.Color='" + ColorName + "'";
            }
            if (LotNo != "")
            {
                SSQL = SSQL + " And CM.LotNo='" + LotNo + "'";
            }
            SSQL = SSQL + " Group by CM.Unit";
            AutoDT = objdata.RptEmployeeMultipleDetails(SSQL);




            if (AutoDT.Rows.Count != 0)
            {
                for (int i = 0; i < AutoDT.Rows.Count; i++)
                {
                    SSQL = "Select isnull(sum(CONVERT(Decimal(18,2),CS.ActProduction)),0) as OnDateProd,";
                    SSQL = SSQL + "isnull(sum(CONVERT(Decimal(18,2),CS.Stop_Page_Mintus)),0) as OnDateBreak";
                    SSQL = SSQL + " from Drawing_Main CM inner join Drawing_Main_Sub CS on CM.Trans_No = CS.Trans_No ";
                    SSQL = SSQL + " Where CM.Ccode='" + SessionCcode + "' And CM.Lcode='" + SessionLcode + "'";
                    SSQL = SSQL + " And CM.FinYearCode='" + SessionFinYearCode + "'";
                    SSQL = SSQL + " And CS.Ccode='" + SessionCcode + "' And CS.Lcode='" + SessionLcode + "'";
                    SSQL = SSQL + " And CS.FinYearCode='" + SessionFinYearCode + "'";
                    if (FromDate != "")
                    {
                        SSQL = SSQL + "And CONVERT(DATETIME,CM.Trans_Date, 103)=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
                        SSQL = SSQL + "And CONVERT(DATETIME,CS.Trans_Date, 103)=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
                        //SSQL = SSQL + "And CONVERT(DATETIME,CS.Trans_Date, 103)=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";
                        //SSQL = SSQL + "And CONVERT(DATETIME,CS.Trans_Date, 103)=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
                    }
                    SSQL = SSQL + " And CM.Unit='" + AutoDT.Rows[i]["Unit"].ToString() + "'";
                    //SSQL = SSQL + " Group by CS.Speed,CS.Hank,CS.StdProduction";
                    dt = objdata.RptEmployeeMultipleDetails(SSQL);

                    if (dt.Rows.Count != 0)
                    {
                        AutoDT.Rows[i]["OnDateProd"] = dt.Rows[0]["OnDateProd"].ToString();
                        AutoDT.Rows[i]["OnDateBreak"] = dt.Rows[0]["OnDateBreak"].ToString();
                    }


                    DataTable dtdCompanyAddress = new DataTable();
                    SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                    dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);
                    if (dtdCompanyAddress.Rows.Count != 0)
                    {
                        AutoDT.Rows[i]["CompanyName"] = "" + dtdCompanyAddress.Rows[0]["Cname"].ToString() + " - " + SessionLcode + "";
                        AutoDT.Rows[i]["Address1"] = "" + dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString() + "";
                        AutoDT.Rows[i]["Address2"] = "" + dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString() + "";
                    }
                }

                DataSet ds1 = new DataSet();
                ds1.Tables.Add(AutoDT);

                RD_Report.Load(Server.MapPath("~/crystal/DrawingProductionAbstract.rpt"));
                RD_Report.SetDataSource(ds1.Tables[0]);
                //Company Details Add

                if (ToDate != "")
                {
                    RD_Report.DataDefinition.FormulaFields["FromDate"].Text = "'" + ToDate.ToString() + "'";
                }

                RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
                CrystalReportViewer1.ReportSource = RD_Report;
                CrystalReportViewer1.RefreshReport();
                CrystalReportViewer1.DataBind();

            }
            else
            {
                Errflag = false;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
            }
        }

        if (RptName == "Drawing Abstract Between Dates Details Report")
        {
            SSQL = "Select '' as CompanyName,'' as Address1,'' as Address2,CM.Unit,";
            SSQL = SSQL + "isnull(sum(CONVERT(Decimal(18,2),CS.ActProduction)),0) as UpToDateProd,";
            SSQL = SSQL + "isnull(sum(CONVERT(Decimal(18,2),CS.Stop_Page_Mintus)),0) as UpToDateBreak,";
            SSQL = SSQL + "'' as OnDateProd,'' as OnDateBreak";
            SSQL = SSQL + " from Drawing_Main CM inner join Drawing_Main_Sub CS on CM.Trans_No = CS.Trans_No ";
            SSQL = SSQL + " Where CM.Ccode='" + SessionCcode + "' And CM.Lcode='" + SessionLcode + "'";
            SSQL = SSQL + " And CM.FinYearCode='" + SessionFinYearCode + "'";
            SSQL = SSQL + " And CS.Ccode='" + SessionCcode + "' And CS.Lcode='" + SessionLcode + "'";
            SSQL = SSQL + " And CS.FinYearCode='" + SessionFinYearCode + "'";
            if (FromDate != "" && ToDate != "")
            {
                SSQL = SSQL + "And CONVERT(DATETIME,CM.Trans_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";
                SSQL = SSQL + "And CONVERT(DATETIME,CM.Trans_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
                SSQL = SSQL + "And CONVERT(DATETIME,CS.Trans_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";
                SSQL = SSQL + "And CONVERT(DATETIME,CS.Trans_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
            }
            if (MachineName != "")
            {
                SSQL = SSQL + " And CS.MachineCode='" + MachineName + "'";
            }
            if (ColorName != "")
            {
                SSQL = SSQL + " And CM.Color='" + ColorName + "'";
            }
            if (LotNo != "")
            {
                SSQL = SSQL + " And CM.LotNo='" + LotNo + "'";
            }
            SSQL = SSQL + " Group by CM.Unit";
            AutoDT = objdata.RptEmployeeMultipleDetails(SSQL);




            if (AutoDT.Rows.Count != 0)
            {
                for (int i = 0; i < AutoDT.Rows.Count; i++)
                {
                    //    SSQL = "Select isnull(sum(CONVERT(Decimal(18,2),CS.ActProduction)),0) as OnDateProd,";
                    //    SSQL = SSQL + "isnull(sum(CONVERT(Decimal(18,2),CS.Stop_Page_Mintus)),0) as OnDateBreak";
                    //    SSQL = SSQL + " from Drawing_Main CM inner join Drawing_Main_Sub CS on CM.Trans_No = CS.Trans_No ";
                    //    SSQL = SSQL + " Where CM.Ccode='" + SessionCcode + "' And CM.Lcode='" + SessionLcode + "'";
                    //    SSQL = SSQL + " And CM.FinYearCode='" + SessionFinYearCode + "'";
                    //    SSQL = SSQL + " And CS.Ccode='" + SessionCcode + "' And CS.Lcode='" + SessionLcode + "'";
                    //    SSQL = SSQL + " And CS.FinYearCode='" + SessionFinYearCode + "'";
                    //    if (FromDate != "")
                    //    {
                    //        SSQL = SSQL + "And CONVERT(DATETIME,CM.Trans_Date, 103)=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
                    //        SSQL = SSQL + "And CONVERT(DATETIME,CS.Trans_Date, 103)=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
                    //        //SSQL = SSQL + "And CONVERT(DATETIME,CS.Trans_Date, 103)=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";
                    //        //SSQL = SSQL + "And CONVERT(DATETIME,CS.Trans_Date, 103)=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
                    //    }
                    //    SSQL = SSQL + " And CM.Unit='" + AutoDT.Rows[i]["Unit"].ToString() + "'";
                    //    //SSQL = SSQL + " Group by CS.Speed,CS.Hank,CS.StdProduction";
                    //    dt = objdata.RptEmployeeMultipleDetails(SSQL);

                    //    if (dt.Rows.Count != 0)
                    //    {
                    //        AutoDT.Rows[i]["OnDateProd"] = dt.Rows[0]["OnDateProd"].ToString();
                    //        AutoDT.Rows[i]["OnDateBreak"] = dt.Rows[0]["OnDateBreak"].ToString();
                    //    }


                    DataTable dtdCompanyAddress = new DataTable();
                    SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                    dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);
                    if (dtdCompanyAddress.Rows.Count != 0)
                    {
                        AutoDT.Rows[i]["CompanyName"] = "" + dtdCompanyAddress.Rows[0]["Cname"].ToString() + " - " + SessionLcode + "";
                        AutoDT.Rows[i]["Address1"] = "" + dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString() + "";
                        AutoDT.Rows[i]["Address2"] = "" + dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString() + "";
                    }
                }

                DataSet ds1 = new DataSet();
                ds1.Tables.Add(AutoDT);

                RD_Report.Load(Server.MapPath("~/crystal/DrawingProductionBWDatesAbstract.rpt"));
                RD_Report.SetDataSource(ds1.Tables[0]);
                //Company Details Add
                if (FromDate != "")
                {
                    RD_Report.DataDefinition.FormulaFields["FromDate"].Text = "'" + FromDate.ToString() + "'";
                }

                if (ToDate != "")
                {
                    RD_Report.DataDefinition.FormulaFields["ToDate"].Text = "'" + ToDate.ToString() + "'";
                }

                RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
                CrystalReportViewer1.ReportSource = RD_Report;
                CrystalReportViewer1.RefreshReport();
                CrystalReportViewer1.DataBind();

            }
            else
            {
                Errflag = false;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
            }
        }

        if (RptName == "OE Abstract Details Report")
        {
            int dayscount = (int)(toDate - frmDate).TotalDays + 1;


            SSQL = "Select '' as CompanyName,'' as Address1,'' as Address2,CM.Unit,";
            SSQL = SSQL + "isnull(sum(CONVERT(Decimal(18,2),CS.ActProd)),0) as UpToDateProd,";
            SSQL = SSQL + "'' as OnDateProd,";
            //SSQL = SSQL + " (isnull(sum(Convert(Decimal(18,2),CS.NoofRoter)),0)/4) as UpToDateBreak,";
            SSQL = SSQL + " '' as UpToDateBreak,";
            SSQL = SSQL + "(isnull(sum(CONVERT(Decimal(18,2),CS.ActProd)),0)/" + dayscount + ") as OnDateBreak,";
            SSQL = SSQL + " (isnull((CONVERT(Decimal(18,2),AC.ActualCount)),0)) as LotNo";
            SSQL = SSQL + " from OE_Main CM inner join OE_Sub CS on CM.Trans_No = CS.Trans_No ";
            SSQL = SSQL + " inner join MstCount AC on AC.CountID=CS.CountID";
            SSQL = SSQL + " Where CM.Ccode='" + SessionCcode + "' And CM.Lcode='" + SessionLcode + "'";
            SSQL = SSQL + " And CM.FinYearCode='" + SessionFinYearCode + "'";
            SSQL = SSQL + " And CS.Ccode='" + SessionCcode + "' And CS.Lcode='" + SessionLcode + "'";
            SSQL = SSQL + " And CS.FinYearCode='" + SessionFinYearCode + "'";
            SSQL = SSQL + " And AC.Ccode='" + SessionCcode + "' And AC.Lcode='" + SessionLcode + "'";
            if (FromDate != "")
            {
                SSQL = SSQL + "And CONVERT(DATETIME,CM.Trans_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";
                SSQL = SSQL + "And CONVERT(DATETIME,CM.Trans_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
                SSQL = SSQL + "And CONVERT(DATETIME,CS.Trans_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";
                SSQL = SSQL + "And CONVERT(DATETIME,CS.Trans_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
            }
            if (MachineName != "")
            {
                SSQL = SSQL + " And CS.MachineCode='" + MachineName + "'";
            }
            if (ColorName != "")
            {
                SSQL = SSQL + " And CM.Color='" + ColorName + "'";
            }
            if (LotNo != "")
            {
                SSQL = SSQL + " And CM.LotNo='" + LotNo + "'";
            }
            SSQL = SSQL + " Group by CM.Unit,AC.ActualCount";
            AutoDT = objdata.RptEmployeeMultipleDetails(SSQL);




            if (AutoDT.Rows.Count != 0)
            {
                for (int i = 0; i < AutoDT.Rows.Count; i++)
                {
                    SSQL = "Select isnull(sum(CONVERT(Decimal(18,2),CS.ActProd)),0) as OnDateProd,";
                    SSQL = SSQL + " (isnull(sum(Convert(Decimal(18,2),CS.NoofRoter)),0)/3) as UpToDateBreak";
                   // SSQL = SSQL + "isnull(sum(CONVERT(Decimal(18,2),CS.Stop_Page_Mintus)),0) as OnDateBreak";
                    SSQL = SSQL + " from OE_Main CM inner join OE_Sub CS on CM.Trans_No = CS.Trans_No ";
                    SSQL = SSQL + " Where CM.Ccode='" + SessionCcode + "' And CM.Lcode='" + SessionLcode + "'";
                    SSQL = SSQL + " And CM.FinYearCode='" + SessionFinYearCode + "'";
                    SSQL = SSQL + " And CS.Ccode='" + SessionCcode + "' And CS.Lcode='" + SessionLcode + "'";
                    SSQL = SSQL + " And CS.FinYearCode='" + SessionFinYearCode + "'";
                    if (FromDate != "")
                    {
                        SSQL = SSQL + "And CONVERT(DATETIME,CM.Trans_Date, 103)=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
                        SSQL = SSQL + "And CONVERT(DATETIME,CS.Trans_Date, 103)=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
                        //SSQL = SSQL + "And CONVERT(DATETIME,CS.Trans_Date, 103)=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";
                        //SSQL = SSQL + "And CONVERT(DATETIME,CS.Trans_Date, 103)=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
                    }
                    SSQL = SSQL + " And CM.Unit='" + AutoDT.Rows[i]["Unit"].ToString() + "'";
                    //SSQL = SSQL + " Group by CS.Speed,CS.Hank,CS.StdProduction";
                    dt = objdata.RptEmployeeMultipleDetails(SSQL);

                    if (dt.Rows.Count != 0)
                    {
                        AutoDT.Rows[i]["OnDateProd"] = dt.Rows[0]["OnDateProd"].ToString();
                        AutoDT.Rows[i]["UpToDateBreak"] = dt.Rows[0]["UpToDateBreak"].ToString();
                    }


                    DataTable dtdCompanyAddress = new DataTable();
                    SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                    dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);
                    if (dtdCompanyAddress.Rows.Count != 0)
                    {
                        AutoDT.Rows[i]["CompanyName"] = "" + dtdCompanyAddress.Rows[0]["Cname"].ToString() + " - " + SessionLcode + "";
                        AutoDT.Rows[i]["Address1"] = "" + dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString() + "";
                        AutoDT.Rows[i]["Address2"] = "" + dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString() + "";
                    }
                }

                DataSet ds1 = new DataSet();
                ds1.Tables.Add(AutoDT);

                RD_Report.Load(Server.MapPath("~/crystal/OEProductionAbstract.rpt"));
                RD_Report.SetDataSource(ds1.Tables[0]);
                //Company Details Add

                if (ToDate != "")
                {
                    RD_Report.DataDefinition.FormulaFields["FromDate"].Text = "'" + ToDate.ToString() + "'";
                }

                RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
                CrystalReportViewer1.ReportSource = RD_Report;
                CrystalReportViewer1.RefreshReport();
                CrystalReportViewer1.DataBind();

            }
            else
            {
                Errflag = false;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
            }
         }

        if (RptName == "OE Abstract Between Dates Details Report")
        {
            int dayscount = (int)(toDate - frmDate).TotalDays + 1;


            SSQL = "Select '' as CompanyName,'' as Address1,'' as Address2,CM.Unit,";
            SSQL = SSQL + "isnull(sum(CONVERT(Decimal(18,2),CS.ActProd)),0) as UpToDateProd,";
            //SSQL = SSQL + "'' as OnDateProd,";
            SSQL = SSQL + " (isnull(sum(Convert(Decimal(18,2),CS.NoofRoter)),0)/3) as UpToDateBreak,";
            SSQL = SSQL + "(isnull(sum(CONVERT(Decimal(18,2),CS.ActProd)),0)/" + dayscount + ") as OnDateBreak,";
            SSQL = SSQL + " (isnull((CONVERT(Decimal(18,2),AC.ActualCount)),0)) as LotNo";
            SSQL = SSQL + " from OE_Main CM inner join OE_Sub CS on CM.Trans_No = CS.Trans_No ";
            SSQL = SSQL + " inner join MstCount AC on AC.CountID=CS.CountID";
            SSQL = SSQL + " Where CM.Ccode='" + SessionCcode + "' And CM.Lcode='" + SessionLcode + "'";
            SSQL = SSQL + " And CM.FinYearCode='" + SessionFinYearCode + "'";
            SSQL = SSQL + " And CS.Ccode='" + SessionCcode + "' And CS.Lcode='" + SessionLcode + "'";
            SSQL = SSQL + " And CS.FinYearCode='" + SessionFinYearCode + "'";
            SSQL = SSQL + " And AC.Ccode='" + SessionCcode + "' And AC.Lcode='" + SessionLcode + "'";
            if (FromDate != "" && ToDate!="")
            {
                SSQL = SSQL + "And CONVERT(DATETIME,CM.Trans_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";
                SSQL = SSQL + "And CONVERT(DATETIME,CM.Trans_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
                SSQL = SSQL + "And CONVERT(DATETIME,CS.Trans_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";
                SSQL = SSQL + "And CONVERT(DATETIME,CS.Trans_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
            }
            if (MachineName != "")
            {
                SSQL = SSQL + " And CS.MachineCode='" + MachineName + "'";
            }
            if (ColorName != "")
            {
                SSQL = SSQL + " And CM.Color='" + ColorName + "'";
            }
            if (LotNo != "")
            {
                SSQL = SSQL + " And CM.LotNo='" + LotNo + "'";
            }
            SSQL = SSQL + " Group by CM.Unit,AC.ActualCount";
            AutoDT = objdata.RptEmployeeMultipleDetails(SSQL);




            if (AutoDT.Rows.Count != 0)
            {
                for (int i = 0; i < AutoDT.Rows.Count; i++)
                {
                    //SSQL = "Select isnull(sum(CONVERT(Decimal(18,2),CS.ActProd)),0) as OnDateProd";
                    //// SSQL = SSQL + "isnull(sum(CONVERT(Decimal(18,2),CS.Stop_Page_Mintus)),0) as OnDateBreak";
                    //SSQL = SSQL + " from OE_Main CM inner join OE_Sub CS on CM.Trans_No = CS.Trans_No ";
                    //SSQL = SSQL + " Where CM.Ccode='" + SessionCcode + "' And CM.Lcode='" + SessionLcode + "'";
                    //SSQL = SSQL + " And CM.FinYearCode='" + SessionFinYearCode + "'";
                    //SSQL = SSQL + " And CS.Ccode='" + SessionCcode + "' And CS.Lcode='" + SessionLcode + "'";
                    //SSQL = SSQL + " And CS.FinYearCode='" + SessionFinYearCode + "'";
                    //if (FromDate != "")
                    //{
                    //    SSQL = SSQL + "And CONVERT(DATETIME,CM.Trans_Date, 103)=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
                    //    SSQL = SSQL + "And CONVERT(DATETIME,CS.Trans_Date, 103)=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
                    //    //SSQL = SSQL + "And CONVERT(DATETIME,CS.Trans_Date, 103)=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";
                    //    //SSQL = SSQL + "And CONVERT(DATETIME,CS.Trans_Date, 103)=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
                    //}
                    //SSQL = SSQL + " And CM.Unit='" + AutoDT.Rows[i]["Unit"].ToString() + "'";
                    ////SSQL = SSQL + " Group by CS.Speed,CS.Hank,CS.StdProduction";
                    //dt = objdata.RptEmployeeMultipleDetails(SSQL);

                    //if (dt.Rows.Count != 0)
                    //{
                    //    AutoDT.Rows[i]["OnDateProd"] = dt.Rows[0]["OnDateProd"].ToString();
                    //    //AutoDT.Rows[i]["OnDateBreak"] = dt.Rows[0]["OnDateBreak"].ToString();
                    //}


                    DataTable dtdCompanyAddress = new DataTable();
                    SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                    dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);
                    if (dtdCompanyAddress.Rows.Count != 0)
                    {
                        AutoDT.Rows[i]["CompanyName"] = "" + dtdCompanyAddress.Rows[0]["Cname"].ToString() + " - " + SessionLcode + "";
                        AutoDT.Rows[i]["Address1"] = "" + dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString() + "";
                        AutoDT.Rows[i]["Address2"] = "" + dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString() + "";
                    }
                }

                DataSet ds1 = new DataSet();
                ds1.Tables.Add(AutoDT);

                RD_Report.Load(Server.MapPath("~/crystal/OEProductionBWDatesAbstract.rpt"));
                RD_Report.SetDataSource(ds1.Tables[0]);
                //Company Details Add

                if (FromDate != "")
                {
                    RD_Report.DataDefinition.FormulaFields["FromDate"].Text = "'" + FromDate.ToString() + "'";
                }
                if (ToDate != "")
                {
                    RD_Report.DataDefinition.FormulaFields["ToDate"].Text = "'" + ToDate.ToString() + "'";
                }

                RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
                CrystalReportViewer1.ReportSource = RD_Report;
                CrystalReportViewer1.RefreshReport();
                CrystalReportViewer1.DataBind();

            }
            else
            {
                Errflag = false;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
            }
        }

        if (!Errflag)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
            lblUploadSuccessfully.Text = "No Records Matched..";
        }


    }

    public void OE_Daily_Report()
    {
        DataTable Da_Check = new DataTable();
        DataTable dt = new DataTable();
        if (FromDate != "")
        {
            frmDate = Convert.ToDateTime(FromDate);
        }
        if (ToDate != "")
        {
            toDate = Convert.ToDateTime(ToDate);
        }

        SSQL = "Select '' as CompanyName,'' as Address1,'' as Address2,Trans_Date,Unit,MachineName,LotNo,CountName,'' as NoofRoter,'' as WorkRoter,'' as StdProd, ";
        SSQL = SSQL + " isnull([shift I],0) as [shift I],isnull([shift II],0) as [shift II],isnull([shift III],0) as[shift III],";
        SSQL = SSQL + " (isnull([shift I],0)+isnull([shift II],0)+isnull([shift III],0)) as LineTotal,'' as Utility,'' as SliverWaste";
        SSQL = SSQL + " from (Select CM.Trans_Date,CM.Unit,CM.ShiftName,CS.MachineName,CS.LotNo,CS.CountName,isnull(sum(CONVERT(Decimal(18,2),CS.ActProd)),0) as ActProd ";
        SSQL = SSQL + "  from OE_Main CM inner join OE_Sub  CS on CM.Trans_No = CS.Trans_No  ";
        SSQL = SSQL + " Where CM.Ccode='" + SessionCcode + "' And CM.Lcode='" + SessionLcode + "'";
        SSQL = SSQL + " And CM.FinYearCode='" + SessionFinYearCode + "'";
        SSQL = SSQL + " And CS.Ccode='" + SessionCcode + "' And CS.Lcode='" + SessionLcode + "'";
        SSQL = SSQL + " And CS.FinYearCode='" + SessionFinYearCode + "'";
        if (FromDate != "")
        {
            SSQL = SSQL + " And CONVERT(DATETIME,CM.Trans_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";
            SSQL = SSQL + "And CONVERT(DATETIME,CM.Trans_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
            SSQL = SSQL + " And CONVERT(DATETIME,CS.Trans_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";
            SSQL = SSQL + "And CONVERT(DATETIME,CS.Trans_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
        }
        if (MachineName != "")
        {
            SSQL = SSQL + " And CS.MachineCode='" + MachineName + "'";
        }
        if (ColorName != "")
        {
            SSQL = SSQL + " And CM.Color='" + ColorName + "'";
        }
        //if (LotNo != "")
        //{
        //    SSQL = SSQL + " And CM.LotNo='" + LotNo + "'";
        //}

        SSQL = SSQL + "  Group by CM.Trans_Date,CM.Unit,CM.ShiftName,CS.MachineName,CS.LotNo,CS.CountName)src pivot( max(ActProd) ";

        SSQL = SSQL + " for ShiftName in ([shift I], [shift II], [shift III])) as MaxBookingDays order by Trans_Date asc";
        Da_Check = objdata.RptEmployeeMultipleDetails(SSQL);




        if (Da_Check.Rows.Count != 0)
        {
            for (int i = 0; i < Da_Check.Rows.Count; i++)
            {
                SSQL = " Select isnull(sum(CONVERT(Decimal(18,2),CS.NoofRoter)),0) as NoofRoter,isnull(sum(CONVERT(Decimal(18,2),CS.WorkRoter)),0)as WorkRoter,";
                SSQL = SSQL + " CS.StdProd,isnull(sum(CONVERT(Decimal(18,2),CS.Utility)),0) as Utility,";
                SSQL = SSQL + "(isnull(sum(CONVERT(Decimal(18,2),CS.SliverWaste)),0)+isnull(sum(CONVERT(Decimal(18,2),CS.RouterWaste)),0)) as SliverWaste,";
                SSQL = SSQL + "isnull(sum(CONVERT(Decimal(18,2),CS.StoppageMins)),0) as Stop_Page_Mintus from OE_Main CM ";
                SSQL = SSQL + "inner join OE_Sub CS on CM.Trans_No = CS.Trans_No ";

                SSQL = SSQL + " Where CM.Ccode='" + SessionCcode + "' And CM.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And CM.FinYearCode='" + SessionFinYearCode + "'";
                SSQL = SSQL + " And CS.Ccode='" + SessionCcode + "' And CS.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And CS.FinYearCode='" + SessionFinYearCode + "'";
                SSQL = SSQL + " And CM.Trans_Date='" + Da_Check.Rows[i]["Trans_Date"].ToString() + "'";
                SSQL = SSQL + " And CS.Trans_Date='" + Da_Check.Rows[i]["Trans_Date"].ToString() + "'";
                SSQL = SSQL + " And CM.Unit='" + Da_Check.Rows[i]["Unit"].ToString() + "'";
                SSQL = SSQL + " And CS.MachineName='" + Da_Check.Rows[i]["MachineName"].ToString() + "'";
                SSQL = SSQL + " Group by CS.StdProd";
                dt = objdata.RptEmployeeMultipleDetails(SSQL);

                if (dt.Rows.Count != 0)
                {
                    Da_Check.Rows[i]["NoofRoter"] = dt.Rows[0]["NoofRoter"].ToString();
                    Da_Check.Rows[i]["WorkRoter"] = dt.Rows[0]["WorkRoter"].ToString();
                    Da_Check.Rows[i]["StdProd"] = dt.Rows[0]["StdProd"].ToString();
                    Da_Check.Rows[i]["SliverWaste"] = dt.Rows[0]["SliverWaste"].ToString();

                    Da_Check.Rows[i]["Utility"] = dt.Rows[0]["Utility"].ToString();


                }


                DataTable dtdCompanyAddress = new DataTable();
                SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dtdCompanyAddress.Rows.Count != 0)
                {
                    Da_Check.Rows[i]["CompanyName"] = "" + dtdCompanyAddress.Rows[0]["Cname"].ToString() + " - " + SessionLcode + "";
                    Da_Check.Rows[i]["Address1"] = "" + dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString() + "";
                    Da_Check.Rows[i]["Address2"] = "" + dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString() + "";
                }
            }



            ds.Tables.Add(Da_Check);
            ReportDocument report = new ReportDocument();
            report.Load(Server.MapPath("~/crystal/OEDailyDetails.rpt"));

            report.SetDatabaseLogon("username", "password", "sa", "Altius2005");
            report.Database.Tables[0].SetDataSource(ds.Tables[0]);

            if (FromDate != "" && ToDate != "")
            {
                report.DataDefinition.FormulaFields["FromDate"].Text = "'" + FromDate.ToString() + "'";
                //report.DataDefinition.FormulaFields["ToDate"].Text = "'" + ToDate.ToString() + "'";
            }

            report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = report;

        }
        else
        {
            Errflag = false;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
        }
        if (!Errflag)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
            lblUploadSuccessfully.Text = "No Records Matched..";
        }

    }
}

