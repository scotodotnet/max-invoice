﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
public partial class Reports_Rpt_Del_Cheese : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionEnquiryNo;
    string SessionFinYearCode;
    string SessionFinYearVal;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();
        if (!IsPostBack)
        {
            Page.Title = "ERP Module :: Rawmaterial Report";
            HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_RptStock"));
            li.Attributes.Add("class", "droplink active open");

            Load_Trans();
            // Load_LotNo();
            Load_Variety();

        }
    }
    private void Load_Trans()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        txtTrans.Items.Clear();
        query = "Select Trans_No from Cheese_Delivery_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        txtTrans.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["Trans_No"] = "-Select-";
        dr["Trans_No"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        txtTrans.DataTextField = "Trans_No";
        txtTrans.DataValueField = "Trans_No";
        txtTrans.DataBind();
    }

    private void Load_Variety()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlItemName.Items.Clear();
        query = "Select CheeseCode,CheeseName from MstCheese where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlItemName.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["CheeseName"] = "-Select-";
        dr["CheeseCode"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlItemName.DataTextField = "CheeseName";
        ddlItemName.DataValueField = "CheeseCode";
        ddlItemName.DataBind();
    }
    protected void btnReport_Click(object sender, EventArgs e)
    {
        string[] finyear = SessionFinYearVal.Split('_');
        string RptName = "";
        string FromDate_Chk = "01/04/" + finyear[0];
        string ToDate_Chk = "31/03/" + finyear[1];

        string fromDate = txtFromDate.Text;
        string toDate = txtToDate.Text;
        string TransNo = "";
        string LotNo = "";
        string VarietyCode = "";
        string ItemName = "";

        if (txtTrans.SelectedItem.Text != "-Select-")
        {
            TransNo = txtTrans.SelectedValue;
        }
        else
        {
            TransNo = "";
        }



        if (ddlItemName.SelectedItem.Text != "-Select-")
        {
            VarietyCode = ddlItemName.SelectedValue;
        }
        else
        {
            VarietyCode = "";
        }

        if (txtFromDate.Text != "" && txtToDate.Text != "")
        {
            if ((Convert.ToDateTime(fromDate) >= Convert.ToDateTime(FromDate_Chk)) && (Convert.ToDateTime(toDate) <= Convert.ToDateTime(ToDate_Chk)))
            {
                RptName = "Delivery Cheese Report";
                ResponseHelper.Redirect("ReportDisplay.aspx?FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text + "&TransCode=" + TransNo + "&VarietyCode=" + VarietyCode + "&ReportName=" + RptName, "_blank", "");
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check with given Date...');", true);
            }
        }
        else
        {
            RptName = "Delivery Cheese Report";
            ResponseHelper.Redirect("ReportDisplay.aspx?FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text + "&TransCode=" + TransNo + "&VarietyCode=" + VarietyCode + "&ReportName=" + RptName, "_blank", "");
        }
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        txtFromDate.Text = ""; txtToDate.Text = "";
        txtTrans.SelectedValue = "-Select-";
        //txtLotNo.SelectedValue = "-Select-";
        ddlItemName.SelectedValue = "-Select-";
    }
}
