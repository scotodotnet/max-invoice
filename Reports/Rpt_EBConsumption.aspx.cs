﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;


public partial class Reports_Rpt_EBConsumption : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionEnquiryNo;
    string SessionFinYearCode;
    string SessionFinYearVal;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();
        if (!IsPostBack)
        {
            Page.Title = "ERP Module :: Raw Material Report";
            HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_RptStock"));
            li.Attributes.Add("class", "droplink active open");

            Machine_Name_Load();
            Transaction_No_Load();
        }
    }
    private void Machine_Name_Load()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        txtMachineName.Items.Clear();
        query = "Select * from MstMachine where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        txtMachineName.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["MachineCode"] = "-Select-";
        dr["MachineName"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        txtMachineName.DataTextField = "MachineName";
        txtMachineName.DataValueField = "MachineCode";
        txtMachineName.DataBind();
    }
    private void Transaction_No_Load()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        txtTransNo.Items.Clear();
        query = "Select * from EB_Consumption_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        txtTransNo.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["Trans_No"] = "-Select-";
        dr["Trans_No"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        txtTransNo.DataTextField = "Trans_No";
        txtTransNo.DataValueField = "Trans_No";
        txtTransNo.DataBind();
    }

    protected void btnReport_Click(object sender, EventArgs e)
    {
        string RptName = "EB Consumption Details Report";
        string TransNo;
        string MachineCode;


        if (txtTransNo.SelectedItem.Text != "-Select-")
        {
            TransNo = txtTransNo.SelectedItem.Text;
        }
        else
        {
            TransNo = "";
        }
        if (txtMachineName.SelectedItem.Text != "-Select-")
        {
            MachineCode = txtMachineName.SelectedValue;

        }
        else
        {
            MachineCode = "";

        }

        ResponseHelper.Redirect("ReportDisplay.aspx?MachineCode=" + MachineCode + "&TransNo=" + TransNo + "&FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text + "&ReportName=" + RptName, "_blank", "");
   
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        txtTransNo.SelectedValue = "-Select-";
        txtMachineName.SelectedValue = "-Select-";
        txtFromDate.Text = ""; txtToDate.Text = "";
    }
    protected void btnMachineReport_Click(object sender, EventArgs e)
    {
        string RptName = "EB Consumption Machinewise Report";
        string TransNo;
        string MachineCode;


        if (txtTransNo.SelectedItem.Text != "-Select-")
        {
            TransNo = txtTransNo.SelectedItem.Text;
        }
        else
        {
            TransNo = "";
        }
        if (txtMachineName.SelectedItem.Text != "-Select-")
        {
            MachineCode = txtMachineName.SelectedValue;

        }
        else
        {
            MachineCode = "";

        }

        ResponseHelper.Redirect("ReportDisplay.aspx?MachineCode=" + MachineCode + "&TransNo=" + TransNo + "&FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text + "&ReportName=" + RptName, "_blank", "");
   
    }
}
