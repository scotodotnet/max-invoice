﻿<%@ Page Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="Rpt_Del_Cheese.aspx.cs" Inherits="Reports_Rpt_Del_Cheese" Title="" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<script type="text/javascript">
    function SaveMsgAlert(msg) {
        swal(msg);
    }
</script>
<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('.date-picker').datepicker({ format: 'dd/mm/yyyy' });
                $('.js-states').select2();
            }
        });
    };
</script>
<div class="page-breadcrumb">
   <ol class="breadcrumb container">
     <h4><li class="active">Cheese Report</li></h4> 
   </ol>
</div>

               
<div id="main-wrapper" class="container">
 <div class="row">
    <div class="col-md-12">
        <div class="col-md-9">
			<div class="panel panel-white">
			<div class="panel panel-primary">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">Cheese Report</h4>
				</div>
			</div>
				<form class="form-horizontal">
				
				<div class="panel-body">
				  
				   <div class="col-md-12">
					<div class="row">
						        
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">Transaction No</label></label>
                                <asp:DropDownList ID="txtTrans" runat="server" class="js-states form-control">
                                </asp:DropDownList>
                                
					        </div>
				         
                            <div class="form-group col-md-4">
					            <label for="exampleInputName">Item Name</label>
					            <asp:DropDownList ID="ddlItemName" runat="server" class="js-states form-control">
                                </asp:DropDownList>
                             </div>
					    	<div class="form-group col-md-4">
					            <label for="exampleInputName">From Date</label>
                                 <div class="input-group m-b-sm" style="width: 230px;padding-left: 0px; float:left"> <span class="input-group-addon" id="Span3" style="float: left; padding: 9px; width: auto;"><i class="fa fa-calendar"></i></span>
                                 <asp:TextBox ID="txtFromDate" MaxLength="30" class="form-control date-picker" runat="server" style="float: left;width: 86%;"></asp:TextBox>
                                 </div>
					        </div>
					        <div class="form-group col-md-4">
					          <label for="exampleInputName">ToDate</label>
					           <div class="input-group m-b-sm" style="width: 230px;padding-left: 0px; float:left"> <span class="input-group-addon" id="Span1" style="float: left; padding: 9px; width: auto;"><i class="fa fa-calendar"></i></span>
                                <asp:TextBox ID="txtToDate" MaxLength="30" class="form-control date-picker" runat="server" style="float: left;width: 86%;"></asp:TextBox>
                               </div>
                           </div>
				     </div>
			       </div>
	
				   <div class="form-group row"></div>
                   <div class="form-group row"></div>
                    
					<!-- Button start -->   
		                 <div class="txtcenter">
                             <asp:Button ID="btnReport" class="btn btn-success"  runat="server" 
                                 Text="Report" ValidationGroup="Validate_Field" onclick="btnReport_Click" />
                             <asp:Button ID="btnCancel" class="btn btn-danger" runat="server" Text="Cancel" 
                                 onclick="btnCancel_Click"  />
                        </div> 
                    <!-- Button end -->					
				</div><!-- panel body end -->
				</form>
			</div><!-- panel white end -->
			</div><!-- col-9 end -->
		    <!-- Dashboard start -->
		                <div class="col-lg-3 col-md-6">
                            <div class="panel panel-white" style="height: 100%;">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Dashboard Details</h4>
                                    <div class="panel-control">
                                     </div>
                                </div>
                                <div class="panel-body">
                               </div>
                            </div>
                        </div>  
                        
                        <div class="col-lg-3 col-md-6">
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div class="live-tile flip ha" data-mode="flip" data-speed="750" data-delay="3000">
                                       
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div class="live-tile flip ha" data-mode="flip" data-speed="750" data-delay="3000">
                                       
                                    </div>
                                </div>
                            </div>
                      
                        </div> 
		    <!-- Dashboard End -->
		    <div class="col-md-2"></div>
		    <div class="col-md-2"></div>
            
      
     
  </div> <!-- col 12 end -->
 </div><!-- row end -->
  </div><!-- main-wrapper end -->
</asp:Content>

