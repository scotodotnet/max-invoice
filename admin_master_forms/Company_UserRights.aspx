﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="Company_UserRights.aspx.cs" Inherits="admin_master_forms_Company_UserRights" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<script type="text/javascript">
    function SaveMsgAlert(msg) {
        swal(msg);

    }
    
</script>

<form class="form-horizontal">
  <asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
<div class="page-breadcrumb">
                    <ol class="breadcrumb container">
                        <h4><li class="active">User Rights</li></h4> 
                    </ol>
                </div>

<div id="main-wrapper" class="container">
 <div class="row">
    <div class="col-md-12">
        <div class="col-md-9">
			<div class="panel panel-white">
			<div class="panel panel-primary">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">User Rights</h4>
				</div>
				</div>
				
				<div class="panel-body">
				    <div class="col-md-12">
				        <div class="row">
                            <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                <ContentTemplate>
                                    <div class="form-group col-md-3">
						                <label for="exampleInputName">User Name<span class="mandatory">*</span></label>
						                <asp:DropDownList ID="txtUserName" runat="server" class="js-states form-control"
						                 AutoPostBack="true" OnSelectedIndexChanged="txtUserName_SelectedIndexChanged">
                                        </asp:DropDownList>
					                </div>
					            </ContentTemplate>
					        </asp:UpdatePanel>
                            <asp:UpdatePanel ID="UpdatePanel23443" runat="server">
                                <ContentTemplate>
                                    <div class="form-group col-md-4">
						                <label for="exampleInputName">Module Name<span class="mandatory">*</span></label>
						                <asp:DropDownList ID="txtModuleName" runat="server" class="js-states form-control"
						                 AutoPostBack="true" OnSelectedIndexChanged="txtModuleName_SelectedIndexChanged">
                                        </asp:DropDownList>
					                </div>
					            </ContentTemplate>
					        </asp:UpdatePanel>
					        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                <ContentTemplate>
                                    <div class="form-group col-md-4">
						                <label for="exampleInputName">Menu Name<span class="mandatory">*</span></label>
						                <asp:DropDownList ID="txtMenuName" runat="server" class="js-states form-control"
						                 AutoPostBack="true" OnSelectedIndexChanged="txtMenuName_SelectedIndexChanged">
                                        </asp:DropDownList>
					                </div>
					            </ContentTemplate>
					        </asp:UpdatePanel>
					         <div class="form-group col-md-1">
					            <br />
					            <asp:Button ID="btnView" Width="50" Height="30" class="btn-success"  runat="server" Text="View" ValidationGroup="Item_Validate_Field"  OnClick="btnView_Click"/>
					        </div>
				        </div>
				        
				    </div>
				    
				    <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                        <ContentTemplate>
				            <div class="col-md-12">
				                <div class="row">
				                    <div class="form-group col-md-3">
				                    <asp:CheckBox id="chkAll" runat="server" Text="Select / UnSelect" Visible="true" 
                                            oncheckedchanged="chkAll_CheckedChanged" AutoPostBack="true"/>
				                    </div>
				                </div>
				            </div>
				        </ContentTemplate>
				    </asp:UpdatePanel>
				    
                    <div class="col-md-12">
				        <div class="row">
				            <asp:Panel ID="GVPanel" runat="server" ScrollBars="None" Visible="true">
				                <asp:GridView id="GVModule" runat="server" AutoGenerateColumns="false" 
				                ClientIDMode="Static" class="gvv display table">
				                    <Columns>
				                        <asp:TemplateField  HeaderText="FormID" Visible="false">
				                            <ItemTemplate>
				                                <asp:Label id="FormID" runat="server" Text='<%# Eval("FormID") %>'/>
				                            </ItemTemplate>
				                        </asp:TemplateField>
				                        <asp:BoundField DataField="FormName" HeaderText="Form Name" />
				                        <asp:TemplateField  HeaderText="Add">
				                            <ItemTemplate>
				                                <asp:CheckBox id="chkSelect" runat="server"/>
				                            </ItemTemplate>
				                        </asp:TemplateField>
				                        <asp:TemplateField  HeaderText="Modify">
				                            <ItemTemplate>
				                                <asp:CheckBox id="chkModify" runat="server"/>
				                            </ItemTemplate>
				                        </asp:TemplateField>
				                        <asp:TemplateField  HeaderText="Delete">
				                            <ItemTemplate>
				                                <asp:CheckBox id="chkDelete" runat="server"/>
				                            </ItemTemplate>
				                        </asp:TemplateField>
				                        <asp:TemplateField  HeaderText="View">
				                            <ItemTemplate>
				                                <asp:CheckBox id="chkView" runat="server"/>
				                            </ItemTemplate>
				                        </asp:TemplateField>
				                        <asp:TemplateField  HeaderText="Approve">
				                            <ItemTemplate>
				                                <asp:CheckBox id="chkApprove" runat="server"/>
				                            </ItemTemplate>
				                        </asp:TemplateField>
				                        <asp:TemplateField  HeaderText="Printout">
				                            <ItemTemplate>
				                                <asp:CheckBox id="chkPrintout" runat="server"/>
				                            </ItemTemplate>
				                        </asp:TemplateField>
				                    </Columns>
				                </asp:GridView>
				            </asp:Panel>
					    </div>
					</div>
					
					<div class="clearfix"></div>   
					<br /><br />
					<!-- Button start -->
                    <div class="col-md-12">
                        <div class="row">
                            <div class="txtcenter">
                                <asp:Button ID="btnSave" class="btn btn-success"  runat="server" Text="Save" ValidationGroup="Validate_Field" OnClick="btnSave_Click"/>
                                <%--<asp:Button ID="Button1" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg" runat="server" Text="View" />--%>
                                <asp:Button ID="btnCancel" class="btn btn-danger" runat="server" Text="Cancel" OnClick="btnCancel_Click"/>
                            </div>
                        </div>
                    </div>                    
                    <!-- Button end -->   


				</div><!-- panel body end -->
				
			</div><!-- panel white end -->
		    </div><!-- col-9 end -->
		    
		    <!-- Dashboard start -->
		     <div class="col-lg-3 col-md-6">
                            <div class="panel panel-white" style="height: 100%;">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Dashboard Details</h4>
                                    <div class="panel-control">
                                        
                                        
                                    </div>
                                </div>
                                <div class="panel-body">
                                    
                                    
                                </div>
                            </div>
                        </div>  
                        
                        <div class="col-lg-3 col-md-6">
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div class="live-tile flip ha" data-mode="flip" data-speed="750" data-delay="3000">
                                       
                                    </div>
                                </div>
                            </div>
                            
                        </div> 
		    <!-- Dashboard End -->
		    
		    <div class="col-md-2"></div>
		    
            
      
         
  </div><!-- col 12 end -->
  </div><!-- row end -->
 </div><!-- main-wrapper end -->
</ContentTemplate>
</asp:UpdatePanel>
</form>

</asp:Content>

