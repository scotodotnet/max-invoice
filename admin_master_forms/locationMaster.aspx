﻿<%@ Page Title="" Language="C#" MasterPageFile="../MainPage.master" AutoEventWireup="true" CodeFile="locationMaster.aspx.cs" Inherits="admin_master_forms_locationMaster" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
    
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<script src='<%= ResolveUrl("../assets/js/master_list_jquery.min.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/master_list_jquery-ui.min.js") %>'></script>
<link href="<%= ResolveUrl("../assets/css/master_list_jquery-ui.css") %>" rel="stylesheet" type="text/css"/>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#example').dataTable();
            }
        });
    };
</script>

<script type="text/javascript">
    function SaveMsgAlert(msg) {
        swal(msg);
    }
</script>

<%--Code Select List Script Saart--%>
    
    <script type="text/javascript">
         $(document).ready(function () {
            initializer();
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(initializer);
        });
        function initializer() {
            $("#<%=txtLocationCode.ClientID %>").autocomplete({
                 
                 
                source: function (request, response) {
                
                        
                      $.ajax({
 
                          url: '<%=ResolveUrl("../List_Service.asmx/GetLocation_Code") %>',
                          data: "{ 'prefix': '" + request.term + "'}",
                          dataType: "json",
                          type: "POST",
                          dataFilter: function (data) { return data; },
                          contentType: "application/json; charset=utf-8",
                          delay:0,
                          success: function (data) {
 
                              response($.map(data.d, function (item) {
 
                                  return {
                                      label: item.split('-')[0],
                                      val: item.split('-')[1],
                                      
                                  }
 
                              }))
 
                          },
 
                          error: function (response) {
                              alert(response.responseText);
                          },
                          failure: function (response) {
                              alert(response.responseText);
                          }
                      });
                  }, minLength: 1,
                  focus: function (e, i) {
                      $("#<%=hfLocationCode.ClientID %>").val(i.item.val);
                      
                      return false;
                  },
                  select: function (e, i) {
                      $("#<%=hfLocationCode.ClientID %>").val(i.item.label);;    
                      
 
                  }
                  
 
              });
 
          }
      </script>
<%--Code Select List Script Saart--%>

<div class="page-breadcrumb">
    <ol class="breadcrumb container">
   <h4><li class="active">Location</li></h4> 
    </ol>
</div>
<div id="main-wrapper" class="container">
 <div class="row">
    <div class="col-md-12">
      <div class="col-md-9">
			<div class="panel panel-white">
				<div class="panel panel-primary">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">Location</h4>
				</div>
				</div>
				<form class="form-horizontal">
				<div class="panel-body">
					<div class="col-md-12">
					    <div class="row">
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">Location Code <span class="mandatory">*</span></label>
					            <asp:TextBox ID="txtLocationCode" MaxLength="25" class="form-control" runat="server" Style="text-transform: uppercase"></asp:TextBox>
                                <asp:HiddenField ID="hfLocationCode" runat="server" />
                                <asp:RequiredFieldValidator ControlToValidate="txtLocationCode" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator1" runat="server" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
                                
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" FilterType="Numbers, UppercaseLetters, LowercaseLetters, Custom" TargetControlID="txtLocationCode" ValidChars=", "/>

					        </div>
					        <%--<asp:LinkButton ID="btnSearch" class="btn btn-primary btn-addon  btn-rounded btn-sm" runat="server" OnClick="btnSearch_Click"><i class="fa fa-search"></i>Search</asp:LinkButton>--%>
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">Location Name <span class="mandatory">*</span></label>
					            <asp:TextBox ID="txtLocationName" class="form-control" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ControlToValidate="txtLocationName" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator2" runat="server" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
                                
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender13" runat="server" FilterType="Numbers, UppercaseLetters, LowercaseLetters, Custom" TargetControlID="txtLocationName" ValidChars=", "/>

					        </div>
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">Address 1 <span class="mandatory">*</span></label>
					            <asp:TextBox ID="txtAddress1" class="form-control" runat="server"></asp:TextBox>
	                            <asp:RequiredFieldValidator ControlToValidate="txtAddress1" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator3" runat="server" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
                                
                                   <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" FilterType="Numbers, LowercaseLetters, UppercaseLetters, Custom"
                                ValidChars="./, " TargetControlID="txtAddress1" />
                                
					        </div>
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">Address 2</label>
					            <asp:TextBox ID="txtAddress2" class="form-control" runat="server"></asp:TextBox>
					        
					           <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterType="Numbers, LowercaseLetters, UppercaseLetters, Custom"
                                ValidChars="./, " TargetControlID="txtAddress2" />
					        
					        </div>
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">City <span class="mandatory">*</span></label>
					            <asp:TextBox ID="txtCity" class="form-control" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ControlToValidate="txtCity" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator4" runat="server" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
                                
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server" FilterType="LowercaseLetters, UppercaseLetters"
                                 TargetControlID="txtCity" />
                                 
					        </div>
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">Zipcode <span class="mandatory">*</span></label>
					            <asp:TextBox ID="txtZipcode" MaxLength="6" class="form-control" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ControlToValidate="txtZipcode" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator5" runat="server" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="RegExValPin" ValidationExpression="[0-9]{6}" runat="server" class="form_error" 
                                    ControlToValidate="txtZipcode" ValidationGroup="Validate_Field" ErrorMessage="Pincode length 6">
                                </asp:RegularExpressionValidator>
                                <cc1:FilteredTextBoxExtender ID="FilTxtPin1" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtZipcode" ValidChars="0123456789">
                                </cc1:FilteredTextBoxExtender>
					        </div>
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">Phone No</label>
					            <div class="form-group row">
					                <div class="col-sm-4">
							            <asp:TextBox ID="txtStdCode" MaxLength="5" class="form-control" runat="server"></asp:TextBox>
							            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                            TargetControlID="txtStdCode" ValidChars="0123456789">
                                        </cc1:FilteredTextBoxExtender>
							        </div>
							        <div class="col-sm-8">
					                    <asp:TextBox ID="txtPhone" MaxLength="7" class="form-control" runat="server"></asp:TextBox>
					                    <asp:RegularExpressionValidator ID="RegExpValdTelNo" ValidationExpression="[0-9]{7}" runat="server" class="form_error" 
                                            ControlToValidate="txtPhone" ValidationGroup="Validate_Field" ErrorMessage="Phone.No length 7">
                                        </asp:RegularExpressionValidator>
                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                            TargetControlID="txtPhone" ValidChars="0123456789">
                                        </cc1:FilteredTextBoxExtender>
                                    </div>
                                </div>
					        </div>
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">Mobile No</label>
					            <div class="form-group row">
					                <div class="col-sm-4">
					                    <asp:TextBox ID="txtMblCode" Text="+91" class="form-control" runat="server" ReadOnly="true"></asp:TextBox>
					                </div>
					                <div class="col-sm-8">
					                    <asp:TextBox ID="txtMobile" MaxLength="10" class="form-control" runat="server"></asp:TextBox>
					                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ValidationExpression="[0-9]{10}" runat="server" class="form_error" 
                                            ControlToValidate="txtMobile" ValidationGroup="Validate_Field" ErrorMessage="Mobile.No length 10">
                                        </asp:RegularExpressionValidator>
                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                            TargetControlID="txtMobile" ValidChars="0123456789">
                                        </cc1:FilteredTextBoxExtender>
                                    </div>
                                </div>
					        </div>
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">Registration No <span class="mandatory">*</span></label>
					            <asp:TextBox ID="txtReg_No" MaxLength="21" class="form-control" runat="server"></asp:TextBox>
	                            <asp:RequiredFieldValidator ControlToValidate="txtReg_No" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator6" runat="server" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
                                
                                   <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterType="Numbers, LowercaseLetters, UppercaseLetters, Custom"
                                ValidChars="./, " TargetControlID="txtReg_No" />
					        </div>
					    </div>
					</div>
					<!-- Button start -->
					<div class="col-md-12">
					    <div class="row">					        
						    <div class="txtcenter">
						        <asp:Button ID="btnSave" class="btn btn-success"  runat="server" Text="Save" ValidationGroup="Validate_Field" OnClick="btnSave_Click"/>
                                <%--<asp:Button ID="Button1" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg" runat="server" Text="View" />--%>
                                <asp:Button ID="btnCancel" class="btn btn-danger" runat="server" Text="Cancel" OnClick="btnCancel_Click"/>
						    </div>						
					    </div>
					</div>
				    <!-- Button end -->
				    <div class="form-group row"></div>
				    <!-- table start --> 					
		            <div class="form-group row"></div>
		            <div class="col-md-12">
		                <div class="row">
		                    <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
		                        <HeaderTemplate>
                                    <table id="example" class="display table">
                                        <thead>
                                            <tr>
                                                <th>Code</th>
                                                <th>Location Name</th>
                                                <%--<th>Address 1</th>--%>
                                                <th>City</th>
                                                <th>Mobile No</th>
                                                <th>Registration No</th>
                                                <th>Mode</th>
                                            </tr>
                                        </thead>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td><%# Eval("Lcode")%></td>
                                        <td><%# Eval("Location_Name")%></td>
                                        <%--<td><%# Eval("Address1")%></td>--%>
                                        <td><%# Eval("City")%></td>
                                        <td><%# Eval("Mobile_No_Join")%></td>
                                        <td><%# Eval("Reg_No")%></td>
                                        <td>
                                            <asp:LinkButton ID="btnEditGrid" class="btn btn-success btn-sm fa fa-pencil"  runat="server" 
                                                Text="" OnCommand="GridEditClick" CommandArgument="Edit" CommandName='<%# Eval("Lcode")%>'>
                                            </asp:LinkButton>
                                            <asp:LinkButton ID="btnDeleteGrid" class="btn btn-danger btn-sm fa fa-trash-o"  runat="server" 
                                                Text="" OnCommand="GridDeleteClick" CommandArgument="Delete" CommandName='<%# Eval("Lcode")%>' 
                                                CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this cost center details?');">
                                            </asp:LinkButton>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate></table></FooterTemplate>
		                    </asp:Repeater>		                    
		                </div>
		            </div>						
                    <!-- table end -->
				    
				    
				    
				</div><!-- panel body end -->
				</form>
			</div><!-- panel white end -->
		    </div><!-- col-9 end -->
		    
		     <!-- Dashboard start -->
		     <div class="col-lg-3 col-md-6">
                            <div class="panel panel-white" style="height: 100%;">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Dashboard Details</h4>
                                    <div class="panel-control">
                                        
                                        
                                    </div>
                                </div>
                                <div class="panel-body">
                                    
                                    
                                </div>
                            </div>
                        </div>  
                        
                        <div class="col-lg-3 col-md-6">
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div class="live-tile flip ha" data-mode="flip" data-speed="750" data-delay="3000">
                                       
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div class="live-tile flip ha" data-mode="flip" data-speed="750" data-delay="3000">
                                       
                                    </div>
                                </div>
                            </div>
                            
                        </div> 
		    <!-- Dashboard End -->
		    
		    <div class="col-md-2"></div>
		    
            
      
         
      </div><!-- col 12 end -->
  </div><!-- row end -->
 </div><!-- main-wrapper end -->
 
</asp:Content>

