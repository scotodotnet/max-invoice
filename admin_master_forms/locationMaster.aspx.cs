﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


using Altius.BusinessAccessLayer.BALDataAccess;

public partial class admin_master_forms_locationMaster : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        if (!IsPostBack)
        {
            Page.Title = "ERP Module :: Location";
            HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Admin_Master"));
            li.Attributes.Add("class", "droplink active open");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        Load_Data();
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        string SaveMode = "Insert";
        string query = "";
        DataTable DT = new DataTable();
        bool ErrFlag = false;
        bool Rights_Check = false;
        //User Rights Check
        if (btnSave.Text == "Update")
        {
            Rights_Check = CommonClass_Function.ModifyRights_Check(SessionCcode, SessionLcode, SessionUserID, "2", "5", "Location");
            if (Rights_Check == false)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Modify Location Details..');", true);
            }
        }
        else
        {
            Rights_Check = CommonClass_Function.AddRights_Check(SessionCcode, SessionLcode, SessionUserID, "2", "5", "Location");
            if (Rights_Check == false)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Adding New Location..');", true);
            }
        }



        if (!ErrFlag)
        {
            query = "Select * from MstLocation where Ccode='" + SessionCcode + "' And Lcode='" + txtLocationCode.Text + "'";
            DT = objdata.RptEmployeeMultipleDetails(query);
            if (DT.Rows.Count != 0)
            {
                if (btnSave.Text == "Update")
                {
                    SaveMode = "Update";
                    query = "Delete from MstLocation where Ccode='" + SessionCcode + "' And Lcode='" + txtLocationCode.Text + "'";
                    objdata.RptEmployeeMultipleDetails(query);
                }
                else
                {
                    SaveMode = "Error";
                }
            }

            query = "Select * from MstLocation where Ccode='" + SessionCcode + "' And Reg_No='" + txtReg_No.Text + "'";
            DT = objdata.RptEmployeeMultipleDetails(query);
            if (DT.Rows.Count != 0)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Location Already have this registration number..');", true);
                SaveMode = "Error";
            }

            if (SaveMode != "Error")
            {
                query = "Insert Into MstLocation(Ccode,Lcode,Location_Name,Address1,Address2,City,PinCode,Std_Code,Phone_No,Mobile_Code,Mobile_No,Reg_No,User_ID,User_Name)";
                query = query + " Values('" + SessionCcode + "','" + txtLocationCode.Text + "','" + txtLocationName.Text + "','" + txtAddress1.Text + "','" + txtAddress2.Text + "','" + txtCity.Text + "',";
                query = query + "'" + txtZipcode.Text + "','" + txtStdCode.Text + "','" + txtPhone.Text + "','" + txtMblCode.Text + "','" + txtMobile.Text + "','" + txtReg_No.Text + "','" + SessionUserID + "','" + SessionUserName + "')";
                objdata.RptEmployeeMultipleDetails(query);

                if (SaveMode == "Insert")
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Location Details Saved Successfully');", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Location Details Updated Successfully');", true);
                }

                Load_Data();
                Clear_All_Field();
                btnSave.Text = "Save";
                txtLocationCode.Enabled = true;
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Location Details Already Saved Successfully');", true);
            }
        }
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }

    private void Clear_All_Field()
    {
        txtLocationCode.Text = ""; txtLocationName.Text = ""; txtAddress1.Text = "";
        txtAddress2.Text = ""; txtCity.Text = ""; txtZipcode.Text = ""; txtPhone.Text = "";
        txtMobile.Text = ""; txtReg_No.Text = "";
        txtStdCode.Text = ""; txtMblCode.Text = "+91";
        btnSave.Text = "Save";
        txtLocationCode.Enabled = true;
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select * from MstLocation where Ccode='" + SessionCcode + "' And Lcode='" + txtLocationCode.Text + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            txtLocationName.Text = DT.Rows[0]["Location_Name"].ToString();
            txtAddress1.Text = DT.Rows[0]["Address1"].ToString();
            txtAddress2.Text = DT.Rows[0]["Address2"].ToString();
            txtCity.Text = DT.Rows[0]["City"].ToString();
            txtZipcode.Text = DT.Rows[0]["PinCode"].ToString();
            txtStdCode.Text = DT.Rows[0]["Std_Code"].ToString();
            txtPhone.Text = DT.Rows[0]["Phone_No"].ToString();
            txtMblCode.Text = DT.Rows[0]["Mobile_Code"].ToString();
            txtMobile.Text = DT.Rows[0]["Mobile_No"].ToString();
            txtReg_No.Text = DT.Rows[0]["Reg_No"].ToString();
            txtLocationCode.Enabled = false;
            btnSave.Text = "Update";
        }
        else
        {
            Clear_All_Field();
        }
    }

    private void Load_Data()
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select Lcode,Location_Name,City,(Mobile_Code + '-' + Mobile_No) as Mobile_No_Join,Reg_No from MstLocation where Ccode='" + SessionCcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        Repeater1.DataSource = DT;
        Repeater1.DataBind();
    }

    protected void GridEditClick(object sender, CommandEventArgs e)
    {
        txtLocationCode.Text = e.CommandName.ToString();
        btnSearch_Click(sender, e);
    }
    protected void GridDeleteClick(object sender, CommandEventArgs e)
    {
        string query = "";
        //User Rights Check
        bool ErrFlag = false;
        bool Rights_Check = false;
        Rights_Check = CommonClass_Function.ModifyRights_Check(SessionCcode, SessionLcode, SessionUserID, "2", "5", "Location");
        if (Rights_Check == false)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Delete Location Details..');", true);
        }
        if (!ErrFlag)
        {
            DataTable DT = new DataTable();
            query = "Select * from MstLocation where Ccode='" + SessionCcode + "' And Lcode='" + e.CommandName.ToString() + "'";
            DT = objdata.RptEmployeeMultipleDetails(query);
            if (DT.Rows.Count != 0)
            {
                query = "Delete from MstLocation where Ccode='" + SessionCcode + "' And Lcode='" + e.CommandName.ToString() + "'";
                objdata.RptEmployeeMultipleDetails(query);
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Location Details Deleted Successfully');", true);
                Load_Data();
                Clear_All_Field();

            }
        }
    }

}
