﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="User_Default_LoadPage.aspx.cs" Inherits="admin_master_forms_User_Default_LoadPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<script src='<%= ResolveUrl("../assets/js/master_list_jquery.min.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/master_list_jquery-ui.min.js") %>'></script>
<link href="<%= ResolveUrl("../assets/css/master_list_jquery-ui.css") %>" rel="stylesheet" type="text/css"/>
<link href="<%= ResolveUrl("assets/css/custom.css") %>" rel="stylesheet" type="text/css"/>
    
<script type="text/javascript">
    //On Page Load
//    $(function() {
//        $('.gvv').dataTable();
//    });

    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#example').dataTable();               
            }
        });
    };

//    if (prm != null) {
//        prm.add_endRequest(function(sender, e) {
//            if (sender._postBackSettings.panelsToUpdate != null) {
//                $('.gvv').dataTable();
//            }
//        });
//    };

</script>


<script type="text/javascript">
    function SaveMsgAlert(msg) {
        swal(msg);
        
    }
    
</script>


<form class="form-horizontal">
  <asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
<div class="page-breadcrumb">
                    <ol class="breadcrumb container">
                   <h4><li class="active">User Default Load Page</li></h4> 
                    </ol>
                </div>

<div id="main-wrapper" class="container">
 <div class="row">
    <div class="col-md-12">
        <div class="col-md-9">
			<div class="panel panel-white">
			<div class="panel panel-primary">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">User Default Load Page</h4>
				</div>
				</div>
				
				<div class="panel-body">
					    <div class="col-md-12">
					        <div class="row">
					            <div class="form-group col-md-4">
							        <label for="exampleInputName">User Name<span class="mandatory">*</span></label>
					                <asp:DropDownList ID="txtUserName" runat="server" class="js-states form-control"
					                 AutoPostBack="true" OnSelectedIndexChanged="txtUserName_SelectedIndexChanged">
                                    </asp:DropDownList>							   
                                </div>
                                <asp:UpdatePanel ID="UpdatePanel23443" runat="server">
                                    <ContentTemplate>
                                        <div class="form-group col-md-4">
						                    <label for="exampleInputName">Module Name<span class="mandatory">*</span></label>
						                    <asp:DropDownList ID="txtModuleName" runat="server" class="js-states form-control"
						                     AutoPostBack="true" OnSelectedIndexChanged="txtModuleName_SelectedIndexChanged">
                                            </asp:DropDownList>
					                    </div>
					                </ContentTemplate>
					            </asp:UpdatePanel>
						        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                    <ContentTemplate>
                                        <div class="form-group col-md-4">
						                    <label for="exampleInputName">Menu Name<span class="mandatory">*</span></label>
						                    <asp:DropDownList ID="txtMenuName" runat="server" class="js-states form-control"
						                     AutoPostBack="true" OnSelectedIndexChanged="txtMenuName_SelectedIndexChanged">
                                            </asp:DropDownList>
					                    </div>
					                </ContentTemplate>
					            </asp:UpdatePanel>
					            <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                    <ContentTemplate>
                                        <div class="form-group col-md-4">
						                    <label for="exampleInputName">Form Name<span class="mandatory">*</span></label>
						                    <asp:DropDownList ID="txtFormName" runat="server" class="js-states form-control">
                                            </asp:DropDownList>
					                    </div>
					                </ContentTemplate>
					            </asp:UpdatePanel>
					        </div>
					        
					    </div>
						
                    <!-- Button start -->
                    <div class="col-md-12">
                        <div class="row">
                            <div class="txtcenter">
                                <asp:Button ID="btnSave" class="btn btn-success"  runat="server" Text="Save" ValidationGroup="Validate_Field" OnClick="btnSave_Click"/>
                                <%--<asp:Button ID="Button1" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg" runat="server" Text="View" />--%>
                                <asp:Button ID="btnCancel" class="btn btn-danger" runat="server" Text="Cancel" OnClick="btnCancel_Click"/>
                            </div>
                        </div>
                    </div>                    
                     <!-- Button end -->   
                     <br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><%--<br /><br /><br /><br />--%>
                     <div class="col-md-12">
                        <div class="row">
                            <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
                                <HeaderTemplate>
                                    <table id="example" class="display table">
                                        <thead>
                                            <tr>
                                            <th>UserName</th>
                                            <th>Module Name</th>
                                            <th>Menu Name</th>
                                            <th>Form Name</th>
                                            <th>Mode</th>
                                            </tr>
                                        </thead>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <%--<tbody>--%>
                                    <tr>
                                        <td><%# Eval("UserName")%></td>
                                        <td><%# Eval("ModuleName")%></td>
                                        <td><%# Eval("MenuName")%></td>
                                        <td><%# Eval("FormName")%></td>
                                        <td>
                                            <asp:LinkButton ID="btnEditGrid" class="btn btn-success btn-sm fa fa-pencil"  runat="server" 
                                                Text="" OnCommand="GridEditClick" CommandArgument="Edit" CommandName='<%# Eval("Auto_ID")%>'>
                                                </asp:LinkButton>
                                                <asp:LinkButton ID="btnDeleteGrid" class="btn btn-danger btn-sm fa fa-trash-o"  runat="server" 
                                                Text="" OnCommand="GridDeleteClick" CommandArgument="Delete" CommandName='<%# Eval("Auto_ID")%>' 
                                                CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this details?');">
                                                </asp:LinkButton>
                                        </td>
                                    </tr>
                                    <%--</tbody>--%>
                                </ItemTemplate>
                                <FooterTemplate></table></FooterTemplate>
                             </asp:Repeater>
                             
                             
                         </div>
                     </div>
                     
                     
                         <%--<div>
					            <asp:GridView id="GVDepartment" runat="server" AutoGenerateColumns="false" 
					            ClientIDMode="Static" OnPreRender="GRID_PreRender" 
					            class="gvv display table">
					                <Columns>
					                    <asp:BoundField DataField="DeptCode" HeaderText="Dept Code" />
					                    <asp:BoundField DataField="DeptName" HeaderText="Name" />
					                    <asp:BoundField DataField="Dept_Desc" HeaderText="Description" />					            
					                </Columns>
					            </asp:GridView>
					    </div>--%>


				</div><!-- panel body end -->
				
			</div><!-- panel white end -->
		    </div><!-- col-9 end -->
		    
		    <!-- Dashboard start -->
		     <div class="col-lg-3 col-md-6">
                            <div class="panel panel-white" style="height: 100%;">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Dashboard Details</h4>
                                    <div class="panel-control">
                                        
                                        
                                    </div>
                                </div>
                                <div class="panel-body">
                                    
                                    
                                </div>
                            </div>
                        </div>  
                        
                        <div class="col-lg-3 col-md-6">
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div class="live-tile flip ha" data-mode="flip" data-speed="750" data-delay="3000">
                                       
                                    </div>
                                </div>
                            </div>
                            
                        </div> 
		    <!-- Dashboard End -->
		    
		    <div class="col-md-2"></div>
		    
            
      
         
  </div><!-- col 12 end -->
  </div><!-- row end -->
 </div><!-- main-wrapper end -->
</ContentTemplate>
</asp:UpdatePanel>
</form>

</asp:Content>

