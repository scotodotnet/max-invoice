﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Security.Cryptography;
using System.IO;
using System.Text;

using Altius.BusinessAccessLayer.BALDataAccess;

public partial class admin_master_forms_User_Default_LoadPage : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    string SessionCcode;
    string SessionLcode;
    string SessionCompanyName;
    string SessionAdmin;
    string SessionLocationName;
    string SessionRights;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();
        SessionRights = Session["Rights"].ToString();
        //SessionCompanyName = Session["CompanyName"].ToString();
        //SessionLocationName = Session["LocationName"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "ERP Module :: User Default Page Load Assign";
            HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Admin_Master"));
            li.Attributes.Add("class", "droplink active open");
            UserName_Add();
            txtUserName_SelectedIndexChanged(sender, e);
            txtModuleName_SelectedIndexChanged(sender, e);
            txtMenuName_SelectedIndexChanged(sender, e);
        }
        Load_Data();
    }

    private void UserName_Add()
    {
        //User Name Add
        txtUserName.Items.Clear();
        DataTable dtUser = new DataTable();
        string query = "";
        query = "Select * from [" + SessionRights + "]..MstUsers where CompCode='" + SessionCcode + "' And LocationCode='" + SessionLcode + "' And UserCode <> 'Scoto' And IsAdmin='1' order by UserCode Asc";
        dtUser = objdata.RptEmployeeMultipleDetails(query);
        txtUserName.DataSource = dtUser;
        txtUserName.DataTextField = "UserCode";
        txtUserName.DataValueField = "UserCode";
        txtUserName.DataBind();
    }

    protected void txtUserName_SelectedIndexChanged(object sender, EventArgs e)
    {
        //Module Name Add
        txtModuleName.Items.Clear();
        string query = "";
        DataTable dtcate = new DataTable();
        query = "Select Distinct ModuleName from [" + SessionRights + "]..Company_Module_User_Rights where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' and UserName='" + txtUserName.SelectedValue + "' order by ModuleName Asc";
        dtcate = objdata.RptEmployeeMultipleDetails(query);
        txtModuleName.DataSource = dtcate;
        txtModuleName.DataTextField = "ModuleName";
        txtModuleName.DataValueField = "ModuleName";
        txtModuleName.DataBind();
    }

    protected void txtModuleName_SelectedIndexChanged(object sender, EventArgs e)
    {
        ////Menu Name Add
        txtMenuName.Items.Clear();
        string query = "";
        DataTable dtcate = new DataTable();
        query = "Select Distinct MenuName from [" + SessionRights + "]..Company_Module_User_Rights where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' and UserName='" + txtUserName.SelectedValue + "' And ModuleName='" + txtModuleName.SelectedValue + "' order by MenuName Asc";
        dtcate = objdata.RptEmployeeMultipleDetails(query);
        txtMenuName.DataSource = dtcate;
        txtMenuName.DataTextField = "MenuName";
        txtMenuName.DataValueField = "MenuName";
        txtMenuName.DataBind();
    }

    protected void txtMenuName_SelectedIndexChanged(object sender, EventArgs e)
    {
        ////Form Name Add
        txtFormName.Items.Clear();
        string query = "";
        DataTable dtcate = new DataTable();
        query = "Select Distinct FormName from [" + SessionRights + "]..Company_Module_User_Rights where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' and UserName='" + txtUserName.SelectedValue + "' And ModuleName='" + txtModuleName.SelectedValue + "' And MenuName='" + txtMenuName.SelectedValue + "' order by FormName Asc";
        dtcate = objdata.RptEmployeeMultipleDetails(query);
        txtFormName.DataSource = dtcate;
        txtFormName.DataTextField = "FormName";
        txtFormName.DataValueField = "FormName";
        txtFormName.DataBind();
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        string query = "";
        string form_Link_get = "";
        DataTable DT = new DataTable();
        query = "Select * from [" + SessionRights + "]..Module_Menu_Form_List where ModuleName='" + txtModuleName.SelectedValue + "' And MenuName='" + txtMenuName.SelectedValue + "' And FormName='" + txtFormName.SelectedValue + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            form_Link_get = DT.Rows[0]["Form_Link_url"].ToString();

            query = "Select * from [" + SessionRights + "]..Company_user_default_page where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And UserName='" + txtUserName.SelectedValue + "' And ModuleName='" + txtModuleName.SelectedValue + "'";
            DT = objdata.RptEmployeeMultipleDetails(query);
            if (DT.Rows.Count != 0)
            {
                query = "Delete from [" + SessionRights + "]..Company_user_default_page where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And UserName='" + txtUserName.SelectedValue + "' And ModuleName='" + txtModuleName.SelectedValue + "'";
                objdata.RptEmployeeMultipleDetails(query);
            }
            query = "insert into [" + SessionRights + "]..Company_user_default_page(CompCode,LocCode,UserName,ModuleName,MenuName,FormName,FormLink) Values('" + SessionCcode + "',";
            query = query + " '" + SessionLcode + "','" + txtUserName.SelectedValue + "','" + txtModuleName.SelectedValue + "','" + txtMenuName.SelectedValue + "','" + txtFormName.SelectedValue + "','" + form_Link_get + "')";
            objdata.RptEmployeeMultipleDetails(query);

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Data Inserted Successfully...');", true);
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Contact Admin...');", true);
        }
        Load_Data();
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        UserName_Add();
        txtUserName_SelectedIndexChanged(sender, e);
        txtModuleName_SelectedIndexChanged(sender, e);
        txtMenuName_SelectedIndexChanged(sender, e);
        Load_Data();
    }

    protected void GridEditClick(object sender, CommandEventArgs e)
    {
        string Auto_ID_str = e.CommandName.ToString();
        string query = "";
        DataTable DT = new DataTable();
        query = "Select * from [" + SessionRights + "]..Company_user_default_page where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And Auto_ID='" + Auto_ID_str + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            txtUserName.SelectedValue = DT.Rows[0]["UserName"].ToString();
            txtUserName_SelectedIndexChanged(sender, e);
            txtModuleName.SelectedValue = DT.Rows[0]["ModuleName"].ToString();
            txtModuleName_SelectedIndexChanged(sender, e);
            txtMenuName.SelectedValue = DT.Rows[0]["MenuName"].ToString();
            txtMenuName_SelectedIndexChanged(sender, e);
            txtFormName.SelectedValue = DT.Rows[0]["FormName"].ToString();
        }
        else
        {
            UserName_Add();
            txtUserName_SelectedIndexChanged(sender, e);
            txtModuleName_SelectedIndexChanged(sender, e);
            txtMenuName_SelectedIndexChanged(sender, e);
        }
        Load_Data();
    }
    protected void GridDeleteClick(object sender, CommandEventArgs e)
    {
        string Auto_ID_str = e.CommandName.ToString();
        string query = "";
        DataTable DT = new DataTable();
        query = "Select * from [" + SessionRights + "]..Company_user_default_page where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And Auto_ID='" + Auto_ID_str + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            //Delete
            query = "Delete from [" + SessionRights + "]..Company_user_default_page where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And Auto_ID='" + Auto_ID_str + "'";
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('This user details deleted successfully...');", true);
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Record Found...');", true);
        }
        Load_Data();
    }

    private void Load_Data()
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select * from [" + SessionRights + "]..Company_user_default_page where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        Repeater1.DataSource = DT;
        Repeater1.DataBind();
        //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "dataTable_Reload();", true);
        //GVDepartment.DataSource = DT;
        //GVDepartment.DataBind();


    }
}
