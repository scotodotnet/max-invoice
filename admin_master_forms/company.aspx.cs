﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


using Altius.BusinessAccessLayer.BALDataAccess;


public partial class master_forms_company : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionGatePassINNo;
    string SessionFinYearCode;
    string SessionFinYearVal;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();
        if (!IsPostBack)
        {
            Page.Title = "ERP Module :: Company";
            HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Admin_Master"));
            li.Attributes.Add("class", "droplink active open");
        }
        
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        string query = "";
        string SaveMode = "Insert";
        bool ErrFlag = false;
        bool Rights_Check = false;
        DataTable DT = new DataTable();

        //User Rights Check
        if (btnSave.Text == "Update")
        {
            Rights_Check = CommonClass_Function.ModifyRights_Check(SessionCcode, SessionLcode, SessionUserID, "2", "5", "Company");
            if (Rights_Check == false)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Modify Company Details..');", true);
            }
        }
        else
        {
            Rights_Check = CommonClass_Function.AddRights_Check(SessionCcode, SessionLcode, SessionUserID, "2", "5", "Company");
            if (Rights_Check == false)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Adding New Company..');", true);
            }
        }

        if (!ErrFlag)
        {
            query = "Select * from MstCompany where Ccode='" + CompanyCode.Text + "'";
            DT = objdata.RptEmployeeMultipleDetails(query);
            if (DT.Rows.Count != 0)
            {
                SaveMode = "Update";
                query = "Delete from MstCompany where Ccode='" + CompanyCode.Text + "'";
                objdata.RptEmployeeMultipleDetails(query);
            }
            string Class_Rdb = RdbClass.SelectedValue.ToString();
            //Insert Compnay Details
            query = "Insert Into MstCompany(Ccode,CompanyName,Address1,Address2,Address3,PinCode,City,State,Country,Tel_No_Code,Tel_No,";
            query = query + "Mobile_Code,Mobile,Mail_ID,Category,SubCategory,Class,ROC_Code,Incorp_Date,Roc_Reg_No,Service_Tax_No,Tin_No,Tin_date,";
            query = query + "CST_No,Pan_No,Others1,Others2,Others3,UserID,UserName) Values('" + CompanyCode.Text + "','" + CompanyName.Text + "',";
            query = query + "'" + Address1.Text + "','" + Address2.Text + "','" + Address3.Text + "','" + Pincode.Text + "','" + City.Text + "',";
            query = query + "'" + State.Text + "','" + Country.Text + "','" + txtStdCode.Text + "','" + Tel_no.Text + "','" + txtMblCode.Text + "','" + Mobile.Text + "','" + Mail_Id.Text + "',";
            query = query + "'" + category.Text + "','" + sub_category.Text + "','" + Class_Rdb + "','" + roc_code.Text + "','" + incorp_date.Text + "',";
            query = query + "'" + roc_reg.Text + "','" + service_tax.Text + "','" + tin_no.Text + "','" + tin_date.Text + "','" + cst_no.Text + "',";
            query = query + "'" + pan_no.Text + "','" + others1.Text + "','" + others2.Text + "','" + others3.Text + "',";
            query = query + "'" + Session["UserId"].ToString() + "','" + Session["Usernmdisplay"].ToString() + "')";
            objdata.RptEmployeeMultipleDetails(query);

            if (SaveMode == "Insert")
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Company Details Saved Successfully');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Company Details Updated Successfully');", true);
            }

            Clear_All_Field();
            btnSave.Text = "Save";
            CompanyCode.Enabled = true;
        }


    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
        btnSave.Text = "Save";
        CompanyCode.Enabled = true;
    }

    protected void btnComCodeSearch_Click(object sender, EventArgs e)
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select * from MstCompany where Ccode='" + CompanyCode.Text + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            CompanyCode.Text = DT.Rows[0]["Ccode"].ToString();
            CompanyName.Text = DT.Rows[0]["CompanyName"].ToString();

            Address1.Text = DT.Rows[0]["Address1"].ToString();
            Address2.Text = DT.Rows[0]["Address2"].ToString();
            Address3.Text = DT.Rows[0]["Address3"].ToString();
            Pincode.Text = DT.Rows[0]["PinCode"].ToString();
            City.Text = DT.Rows[0]["City"].ToString();
            State.Text = DT.Rows[0]["State"].ToString();
            Country.Text = DT.Rows[0]["Country"].ToString();
            txtStdCode.Text = DT.Rows[0]["Tel_No_Code"].ToString();
            Tel_no.Text = DT.Rows[0]["Tel_No"].ToString();
            txtMblCode.Text = DT.Rows[0]["Mobile_Code"].ToString();
            Mobile.Text = DT.Rows[0]["Mobile"].ToString();
            Mail_Id.Text = DT.Rows[0]["Mail_ID"].ToString();
            category.Text = DT.Rows[0]["Category"].ToString();
            sub_category.Text = DT.Rows[0]["SubCategory"].ToString();
            RdbClass.SelectedValue = DT.Rows[0]["Class"].ToString();
            roc_code.Text = DT.Rows[0]["ROC_Code"].ToString();
            incorp_date.Text = DT.Rows[0]["Incorp_Date"].ToString();
            roc_reg.Text = DT.Rows[0]["Roc_Reg_No"].ToString();
            service_tax.Text = DT.Rows[0]["Service_Tax_No"].ToString();
            tin_no.Text = DT.Rows[0]["Tin_No"].ToString();
            tin_date.Text = DT.Rows[0]["Tin_date"].ToString();
            cst_no.Text = DT.Rows[0]["CST_No"].ToString();
            pan_no.Text = DT.Rows[0]["Pan_No"].ToString();
            others1.Text = DT.Rows[0]["Others1"].ToString();
            others2.Text = DT.Rows[0]["Others2"].ToString();
            others3.Text = DT.Rows[0]["Others3"].ToString();


            btnSave.Text = "Update";
            CompanyCode.Enabled = false;
        }
        else
        {
            Clear_All_Field();
            btnSave.Text = "Save";

        }
    }
    private void Clear_All_Field()
    {
        CompanyCode.Text = "";CompanyName.Text = "";
        Address1.Text = "";
        Address2.Text = "";
        Address3.Text = "";
        Pincode.Text = "";
        City.Text = "";
        State.Text = "";
        Country.Text = "";
        txtStdCode.Text = "";
        Tel_no.Text = "";
        txtMblCode.Text = "+91";
        Mobile.Text = "";
        Mail_Id.Text = "";
        category.Text = "";
        sub_category.Text = "";
        RdbClass.SelectedValue = "";
        roc_code.Text = "";
        incorp_date.Text = "";
        roc_reg.Text = "";
        service_tax.Text = "";
        tin_no.Text = "";
        tin_date.Text = "";
        cst_no.Text = "";
        pan_no.Text = "";
        others1.Text = "";
        others2.Text = "";
        others3.Text = "";
    }
    
}
