﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Security.Cryptography;
using System.IO;
using System.Text;

using Altius.BusinessAccessLayer.BALDataAccess;

public partial class admin_master_forms_Company_Module_Rights : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    string SessionCcode;
    string SessionLcode;
    string SessionCompanyName;
    string SessionAdmin;
    string SessionLocationName;
    string SessionRights;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();
        //SessionCompanyName = Session["CompanyName"].ToString();
        //SessionLocationName = Session["LocationName"].ToString();
        SessionRights = Session["Rights"].ToString();
        if (!IsPostBack)
        {
            Page.Title = "ERP Module :: Module Rights";
            HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Admin_Master"));
            li.Attributes.Add("class", "droplink active open");

            Load_Module_Details();
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        //GVModule.Rows[1].FindControl("chkAdd"). = "true";

        string query = "";
        DataTable DT_Check = new DataTable();
        DataTable qry_dt = new DataTable();
        string SaveMode = "";
        bool ErrFlag = false;

        query = "Delete from [" + SessionRights + "]..Company_Module_Rights where";
        query = query + " CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        objdata.RptEmployeeMultipleDetails(query);

        foreach (GridViewRow gvsal in GVModule.Rows)
        {
            SaveMode = "Insert";
            string Module_ID_Encrypt = "0";
            string Module_ID = "0";
            string Module_Name = "0";

            CheckBox ChkSelect_chk = (CheckBox)gvsal.FindControl("chkSelect");

            Label ModuleID_lbl = (Label)gvsal.FindControl("ModuleID");
            Module_ID = ModuleID_lbl.Text.ToString();
            Module_Name = gvsal.Cells[1].Text.ToString();

            Module_ID_Encrypt = Encrypt(Module_ID).ToString();

            if (ChkSelect_chk.Checked == true)
            {
                //Get Module Link
                string Module_Link_Str = "";
                DataTable DT = new DataTable();
                query = "Select * from [" + SessionRights + "]..Module_List where ModuleID='" + Module_ID + "'";
                DT = objdata.RptEmployeeMultipleDetails(query);
                if (DT.Rows.Count != 0)
                {
                    Module_Link_Str = DT.Rows[0]["ModuleLink"].ToString();
                }

                //Insert User Rights
                query = "Insert Into [" + SessionRights + "]..Company_Module_Rights(CompCode,LocCode,ModuleID,ModuleName,ModuleLink) Values('" + SessionCcode + "','" + SessionLcode + "','" + Module_ID_Encrypt + "',";
                query = query + " '" + Module_Name + "','" + Module_Link_Str + "')";
                objdata.RptEmployeeMultipleDetails(query);
            }
        }

        if (SaveMode == "Insert")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Module Access Rights Details Saved Successfully');", true);
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Module Access Rights Details Not Saved Properly...');", true);
        }

    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }

    private void Clear_All_Field()
    {
        Load_Module_Details();
    }

    private void Load_Module_Details()
    {
        string query = "";
        DataTable qry_dt = new DataTable();
        bool ErrFlag = false;

        DataTable DT = new DataTable();
        query = "Select ModuleID,ModuleName from [" + SessionRights + "]..Module_List order by ModuleID Asc";
        DT = objdata.RptEmployeeMultipleDetails(query);
        GVModule.DataSource = DT;
        GVModule.DataBind();

        if (GVModule.Rows.Count != 0)
        {
            GVPanel.Visible = true;
        }
        else
        {
            GVPanel.Visible = false;
        }
        Load_Module_Rights();

    }

    private void Load_Module_Rights()
    {

        foreach (GridViewRow gvsal in GVModule.Rows)
        {
            string ModuleName = "";
            string ModuleID = "0";
            string ModuleID_Encrypt = "0";

            Label ModuleID_lbl = (Label)gvsal.FindControl("ModuleID");
            ModuleID = ModuleID_lbl.Text.ToString();
            ModuleName = gvsal.Cells[1].Text.ToString();
            ModuleID_Encrypt = Encrypt(ModuleID).ToString();
            //Get Company Module Rights
            string query = "";
            DataTable DT = new DataTable();
            query = "Select * from [" + SessionRights + "]..Company_Module_Rights where ModuleID='" + ModuleID_Encrypt + "'";
            query = query + " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
            DT = objdata.RptEmployeeMultipleDetails(query);
            if (DT.Rows.Count != 0)
            {
                //User Rights Update in Grid
                ((CheckBox)gvsal.FindControl("chkSelect")).Checked = true;
            }
        }
    }

    private string Encrypt(string clearText)
    {
        string EncryptionKey = "MAKV2SPBNI99212";
        byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
        using (Aes encryptor = Aes.Create())
        {
            Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
            encryptor.Key = pdb.GetBytes(32);
            encryptor.IV = pdb.GetBytes(16);
            using (MemoryStream ms = new MemoryStream())
            {
                using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                {
                    cs.Write(clearBytes, 0, clearBytes.Length);
                    cs.Close();
                }
                clearText = Convert.ToBase64String(ms.ToArray());
            }
        }
        return clearText;
    }

    private string Decrypt(string cipherText)
    {
        string EncryptionKey = "MAKV2SPBNI99212";
        byte[] cipherBytes = Convert.FromBase64String(cipherText);
        using (Aes encryptor = Aes.Create())
        {
            Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
            encryptor.Key = pdb.GetBytes(32);
            encryptor.IV = pdb.GetBytes(16);
            using (MemoryStream ms = new MemoryStream())
            {
                using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                {
                    cs.Write(cipherBytes, 0, cipherBytes.Length);
                    cs.Close();
                }
                cipherText = Encoding.Unicode.GetString(ms.ToArray());
            }
        }
        return cipherText;
    }
}
