﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="MstNewUser.aspx.cs" Inherits="admin_master_forms_MstNewUser" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#example').dataTable();
            }
        });
    };
</script>

<script type="text/javascript">
    function SaveMsgAlert(msg) {
        swal(msg);
    }
</script>


<div class="page-breadcrumb">
    <ol class="breadcrumb container">
        <h4><li class="active">User Registration</li></h4> 
    </ol>
</div>
<div id="main-wrapper" class="container">
<div class="row">
    <div class="col-md-12">
        <div class="col-md-9">
			<div class="panel panel-white">
			<div class="panel panel-primary">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">User Registration</h4>
				</div>
			</div>
				<form class="form-horizontal">
				<div class="panel-body">
					<div class="col-md-12">
					    <div class="row">
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">User ID<span class="mandatory">*</span></label>
					            <asp:TextBox ID="txtUserID" MaxLength="30" class="form-control" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ControlToValidate="txtUserID" ValidationGroup="Validate_Field" class="form_error" ID="validate1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
					        </div>
					        <%--<asp:LinkButton ID="btnSearch" class="btn btn-primary btn-addon  btn-rounded btn-sm" runat="server" OnClick="btnSearch_Click"><i class="fa fa-search"></i>Search</asp:LinkButton>--%>
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">Full Name<span class="mandatory">*</span></label>
					            <asp:TextBox ID="txtUserFullName" class="form-control" runat="server"></asp:TextBox>
							    <asp:RequiredFieldValidator ControlToValidate="txtUserFullName" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
					        </div>
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">User Type</label>
					            <asp:DropDownList ID="txtUserType" runat="server" class="form-control">
                                    <asp:ListItem Value="1" Text="Admin"></asp:ListItem>
                                    <asp:ListItem Value="2" Text="Management"></asp:ListItem>
                                    <asp:ListItem Value="3" Text="Manager"></asp:ListItem>
                                    <asp:ListItem Value="4" Text="Stores"></asp:ListItem>
                                    <asp:ListItem Value="5" Text="End User"></asp:ListItem>
                                </asp:DropDownList>
					        </div>					        
					    </div>
					</div>
					<div class="col-md-12">
					    <div class="row">
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">New Password<span class="mandatory">*</span></label>
					            <asp:TextBox ID="txtNewPassword" class="form-control" runat="server" TextMode="Password"></asp:TextBox>
					            <asp:RequiredFieldValidator ControlToValidate="txtNewPassword" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator2" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
					        </div>
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">Conform Password<span class="mandatory">*</span></label>
					            <asp:TextBox ID="txtConformPassword" class="form-control" runat="server" TextMode="Password"></asp:TextBox>
					            <asp:RequiredFieldValidator ControlToValidate="txtConformPassword" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator3" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
					        </div>
					        <asp:UpdatePanel ID="UpdatePanel3342" runat="server">
                                <ContentTemplate>
					                <div id="Div1" class="form-group col-md-4" runat="server"  visible="false">
						                <label for="exampleInputName">Default Load<span class="mandatory">*</span></label>
						                <asp:DropDownList ID="txtFormName" runat="server" class="js-states form-control" Visible="false">
                                        </asp:DropDownList>
					                </div>
					            </ContentTemplate>
					        </asp:UpdatePanel>
					        
					        
					    </div>
					</div>
					<!-- Button start -->
					<div class="col-md-12">
					    <div class="row">
					        <div class="txtcenter">
                                <div class="col-sm-11">
                                    <asp:Button ID="btnSave" class="btn btn-success"  runat="server" Text="Save" ValidationGroup="Validate_Field" OnClick="btnSave_Click"/>
                                    <%--<asp:Button ID="Button1" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg" runat="server" Text="View" />--%>
                                    <asp:Button ID="btnCancel" class="btn btn-danger" runat="server" Text="Cancel" OnClick="btnCancel_Click"/>
                                </div>
                            </div>
					    </div>
					</div>                            
                    <!-- Button End -->
                    <div class="form-group row"></div>
                    
                    <!-- table start -->
                    <div class="col-md-12">
		                <div class="row">
		                    <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
		                        <HeaderTemplate>
                                    <table id="example" class="display table">
                                        <thead>
                                            <tr>
                                                <th>User ID</th>
                                                <th>Name</th>
                                                <th>Type</th>
                                                <%--<th>Password</th>--%>
                                                <th>Mode</th>
                                            </tr>
                                        </thead>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td><%# Eval("UserCode")%></td>
                                        <td><%# Eval("UserName")%></td>
                                        <td><%# Eval("IsAdmin")%></td>
                                        <%--<td><%# Eval("NewPassword")%></td>--%>
                                        <td>
                                            <asp:LinkButton ID="btnEditGrid" class="btn btn-success btn-sm fa fa-pencil"  runat="server" 
                                                Text="" OnCommand="GridEditClick" CommandArgument="Edit" CommandName='<%# Eval("UserCode")%>'>
                                            </asp:LinkButton>
                                            <asp:LinkButton ID="btnDeleteGrid" class="btn btn-danger btn-sm fa fa-trash-o"  runat="server" 
                                                Text="" OnCommand="GridDeleteClick" CommandArgument="Delete" CommandName='<%# Eval("UserCode")%>' 
                                                CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this new user details?');">
                                            </asp:LinkButton>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate></table></FooterTemplate>
		                    </asp:Repeater>		                    
		                </div>
		            </div>
                    <!-- table End -->
					
				    </div><!-- panel body end -->
				</form>
			</div><!-- panel white end -->
		</div><!-- col-9 end -->
		    
	    <div class="col-md-2"></div>
		<!-- Dashboard start -->
		<div class="col-lg-3 col-md-6">
            <div class="panel panel-white" style="height: 100%;">
                <div class="panel-heading">
                    <h4 class="panel-title">Dashboard Details</h4>
                    <div class="panel-control">
                    </div>
                </div>
                <div class="panel-body">
                </div>
            </div>
        </div>  
                        
        <div class="col-lg-3 col-md-6">
            <div class="panel panel-white">
                <div class="panel-body">
                    <div class="live-tile flip ha" data-mode="flip" data-speed="750" data-delay="3000">
                       
                    </div>
                </div>
            </div>
            
        </div> 
		<!-- Dashboard End -->
		<div class="col-md-2"></div>
		
      </div><!-- col 12 end -->
  </div><!-- row end -->
 </div><!-- main-wrapper end -->
 
</asp:Content>

