﻿<%@ Page Title="" Language="C#" MasterPageFile="../MainPage.master" AutoEventWireup="true" CodeFile="numberingsetup.aspx.cs" Inherits="admin_master_forms_numberingsetup" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#example').dataTable();
            }
        });
    };
</script>

<script type="text/javascript">
    function SaveMsgAlert(msg) {
        swal(msg);
    }
</script>

<div class="page-breadcrumb">
    <ol class="breadcrumb container">
       <h4><li class="active">Number Setup</li></h4> 
    </ol>
</div>
<div id="main-wrapper" class="container">
 <div class="row">
    <div class="col-md-12">
        
            <div class="col-md-9">
			<div class="panel panel-white">
			<div class="panel panel-primary">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">Number Setup</h4>
				</div>
				</div>
				<form class="form-horizontal">
				<div class="panel-body">
					<div class="col-md-12">
					    <div class="row">
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">Form Name</label>
                                <asp:DropDownList ID="txtFormName" runat="server" class="js-states form-control"
                                    AutoPostBack="True" 
                                    onselectedindexchanged="txtFormName_SelectedIndexChanged">
                                </asp:DropDownList>
					        </div>
					        <div class="form-group col-md-2">
					            <label for="exampleInputName">Prefix</label>
                                <asp:TextBox ID="txtPrefix" class="form-control" Text="0" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ControlToValidate="txtPrefix" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator2" runat="server" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
					        </div>
					        
					        <div class="form-group col-md-2">
					            <label for="exampleInputName">NumberFrom</label>
                                <asp:TextBox ID="txtEndNo" class="form-control" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ControlToValidate="txtEndNo" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator4" runat="server" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtEndNo" ValidChars="0123456789">
                                </cc1:FilteredTextBoxExtender>
					        </div>
					        <div class="form-group col-md-2">
					            <label for="exampleInputName">Suffix</label>
					            <asp:Label ID="txtSuffix" runat="server" class="form-control"></asp:Label>
					        </div>
					        <div class="form-group col-md-2">
					           <%-- <label for="exampleInputName">StartNo</label>--%>
                                <asp:TextBox ID="txtStartNo" class="form-control" runat="server" Visible="false"  Text="0"></asp:TextBox>
                                
					        </div>
					    </div>
					</div>
						
					<!-- Button start -->
					<div class="col-md-12">
					    <div class="row">					        
						    <div class="txtcenter">
						        <asp:Button ID="btnSave" class="btn btn-success"  runat="server" Text="Save" ValidationGroup="Validate_Field" OnClick="btnSave_Click"/>
                                <%--<asp:Button ID="Button1" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg" runat="server" Text="View" />--%>
                                <asp:Button ID="btnCancel" class="btn btn-danger" runat="server" Text="Cancel" OnClick="btnCancel_Click"/>
						    </div>						
					    </div>
					</div>
				    <!-- Button end -->
                    
                    <div class="form-group row"></div>
				    <!-- table start --> 					
		            <div class="form-group row"></div>
		            <div class="col-md-12">
		                <div class="row">
		                    <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
		                        <HeaderTemplate>
                                    <table id="example" class="display table">
                                        <thead>
                                            <tr>
                                                <th>Form Name</th>
                                                <%--<th>Prefix</th>--%>
                                                <%--<th>StartNo</th>--%>
                                                <th>NumberFrom</th>
                                                <th>Suffix</th>
                                                <th>Mode</th>
                                            </tr>
                                        </thead>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td><%# Eval("FormName")%></td>
                                        <%--<td><%# Eval("Prefix")%></td>--%>
                                        <%--<td><%# Eval("StartNo")%></td>--%>
                                        <td><%# Eval("EndNo")%></td>
                                        <td><%# Eval("Suffix")%></td>
                                        <td>
                                            <asp:LinkButton ID="btnEditGrid" class="btn btn-success btn-sm fa fa-pencil"  runat="server" 
                                                Text="" OnCommand="GridEditClick" CommandArgument="Edit" CommandName='<%# Eval("FormName")%>'>
                                            </asp:LinkButton>
                                            <asp:LinkButton ID="btnDeleteGrid" class="btn btn-danger btn-sm fa fa-trash-o"  runat="server" 
                                                Text="" OnCommand="GridDeleteClick" CommandArgument="Delete" CommandName='<%# Eval("FormName")%>' 
                                                CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this cost center details?');">
                                            </asp:LinkButton>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate></table></FooterTemplate>
		                    </asp:Repeater>		                    
		                </div>
		            </div>						
                    <!-- table end -->
                                        
				</div><!-- panel body end -->
				</form>
			</div><!-- panel white end -->
		    </div><!-- col-9 end -->
		    <!-- Dashboard start -->
		     <div class="col-lg-3 col-md-6">
                            <div class="panel panel-white" style="height: 100%;">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Dashboard Details</h4>
                                    <div class="panel-control">
                                        
                                        
                                    </div>
                                </div>
                                <div class="panel-body">
                                    
                                    
                                </div>
                            </div>
                        </div>  
                        
                        <div class="col-lg-3 col-md-6">
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div class="live-tile flip ha" data-mode="flip" data-speed="750" data-delay="3000">
                                       
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div class="live-tile flip ha" data-mode="flip" data-speed="750" data-delay="3000">
                                       
                                    </div>
                                </div>
                            </div>
                            
                        </div> 
		    <!-- Dashboard End -->
		    <div class="col-md-2"></div>
	
  </div><!-- col 12 end -->
  </div><!-- row end -->
 </div><!-- main-wrapper end -->
 
 
</asp:Content>

