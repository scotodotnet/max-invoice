﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Web.Services;


using Altius.BusinessAccessLayer.BALDataAccess;

public partial class master_forms_location : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    string UserID = "";
    string Session_UserName_Display = "";
    string SessionCcode;
    string SessionLcode;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        if (!IsPostBack)
        {
            Page.Title = "ERP Module :: Location";
            HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Admin_Master"));
            li.Attributes.Add("class", "droplink active open");
            
            
        }
        UserID = Session["UserId"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        Session_UserName_Display = Session["Usernmdisplay"].ToString();
    }
    //protected void btnSave_Click(object sender, EventArgs e)
    //{
    //    btnSave.Attributes.Add("onclick", "SaveRecord();return false");
    //}
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        
    }


    [WebMethod(EnableSession = true)]
    public static bool Insert_Details(string Loc_Code, string Loc_Name, string Add1, string Add2, string City, string PinCode, string Phone, string Mobile, string Reg_No)
    {
        bool status;

        BALDataAccess objdata = new BALDataAccess();
        objdata.RptEmployeeMultipleDetails("");

        master_forms_location foo = new master_forms_location();
        string Save_St = foo.SaveLocation_Data(Loc_Code, Loc_Name, Add1, Add2, City, PinCode, Phone, Mobile, Reg_No);

        if (Save_St == "Insert" || Save_St == "Update")
        {
            status = true;
        }
        else
        {
            status = false;
        }
        
        return status;

    }
    public void Fun_Call()
    {
        

    }
    public string SaveLocation_Data(string Loc_Code, string Loc_Name, string Add1, string Add2, string City, string PinCode, string Phone, string Mobile, string Reg_No)
    {
        string status="";

        UserID = Session["UserId"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        Session_UserName_Display = Session["Usernmdisplay"].ToString();

        string query = "";
        DataTable DT = new DataTable();
        query = "Select * from MstLocation where Ccode='" + SessionCcode + "' And Lcode='" + Loc_Code + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            status = "Update";
            query = "Delete from MstLocation where Ccode='" + SessionCcode + "' And Lcode='" + Loc_Code + "'";
            objdata.RptEmployeeMultipleDetails(query);
        }
        else
        {
            status = "Insert";
        }
        
        //Insert Compnay Details

        //query = "Insert Into MstLocation(Ccode,Lcode,Location_Name) Values('" + SessionCcode + "','" + Loc_Code + "','" + Loc_Name + "')";

        query = "Insert Into MstLocation(Ccode,Lcode,Location_Name,Address1,Address2,City,PinCode,Phone_No,Mobile_No,Reg_No,User_ID,User_Name)";
        query = query + " Values('" + SessionCcode + "','" + Loc_Code + "','" + Loc_Name + "','" + Add1 + "','" + Add2 + "','" + City + "',";
        query = query + "'" + PinCode + "','" + Phone + "','" + Mobile + "','" + Reg_No + "','" + UserID + "','" + Session_UserName_Display + "')";
        objdata.RptEmployeeMultipleDetails(query);

        //txtAddress1.Text = "";
        return status;
    }

    protected void btnlocation_search_Click(object sender, EventArgs e)
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select * from MstLocation where Lcode='" + txtLocationCode.Text + "' And Ccode='" + SessionCcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            txtLocationCode.Text = DT.Rows[0]["Lcode"].ToString();
            txtLocationName.Text = DT.Rows[0]["Location_Name"].ToString();

            txtAddress1.Text = DT.Rows[0]["Address1"].ToString();
            txtAddress2.Text = DT.Rows[0]["Address2"].ToString();
            txtCity.Text = DT.Rows[0]["City"].ToString();
            txtZipcode.Text = DT.Rows[0]["PinCode"].ToString();
            txtPhone.Text = DT.Rows[0]["Phone_No"].ToString();
            txtMobile.Text = DT.Rows[0]["Mobile_No"].ToString();
            txtReg_No.Text = DT.Rows[0]["Reg_No"].ToString();

            btnSave.Text = "Update";
            txtLocationCode.Enabled = false;
        }
        else
        {
            Clear_All_Field();
            btnSave.Text = "Save";

        }
    }

    private void Clear_All_Field()
    {
        txtLocationName.Text = "";
        txtAddress1.Text = "";
        txtAddress2.Text = "";
        txtCity.Text = "";
        txtZipcode.Text = "";
        txtPhone.Text = "";
        txtMobile.Text = "";
        txtReg_No.Text = "";
        txtLocationCode.Enabled = true;
    }
}
