﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


using Altius.BusinessAccessLayer.BALDataAccess;

public partial class admin_master_forms_numberingsetup : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionFinYearCode;
    string SessionFinYearVal;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "ERP Module :: Number Setup";
            HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Admin_Master"));
            li.Attributes.Add("class", "droplink active open");
            Form_Name_Load();
            Fin_Year_Code_Join();
        }
        

        Load_Data();

    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        string SaveMode = "Insert";
        string query = "";
        DataTable DT = new DataTable();
        bool ErrFlag = false;
        if (txtFormName.Text.ToString() == "-Select-")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select Form Name..');", true);
        }

        //Check Prefix Already Save with Another Form
        //query = "Select * from MstNumberingSetup where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FormName <> '" + txtFormName.Text + "' And EndNo='"+ txtEndNo.Text +"'";
        //DT = objdata.RptEmployeeMultipleDetails(query);
        //if (DT.Rows.Count != 0)
        //{
        //    ErrFlag = true;
        //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('This Prefix No Already Asign to Another Form..');", true);
        //}

        //User Rights Check
        //bool Rights_Check = false;
        //if (btnSave.Text == "Update")
        //{
        //    Rights_Check = CommonClass_Function.ModifyRights_Check(SessionCcode, SessionLcode, SessionUserID, "2", "5", "Number Setup");
        //    if (Rights_Check == false)
        //    {
        //        ErrFlag = true;
        //        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Modify Number Setup Details..');", true);
        //    }
        //}
        //else
        //{
        //    Rights_Check = CommonClass_Function.AddRights_Check(SessionCcode, SessionLcode, SessionUserID, "2", "5", "Number Setup");
        //    if (Rights_Check == false)
        //    {
        //        ErrFlag = true;
        //        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Adding New Number Setup..');", true);
        //    }
        //}

        if (!ErrFlag)
        {
            query = "Select * from MstNumberingSetup where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FormName='" + txtFormName.Text + "'";
            DT = objdata.RptEmployeeMultipleDetails(query);
            if (DT.Rows.Count != 0)
            {
                SaveMode = "Update";
                query = "Delete from MstNumberingSetup where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FormName='" + txtFormName.Text + "'";
                objdata.RptEmployeeMultipleDetails(query);
            }

            query = "Insert Into MstNumberingSetup(Ccode,Lcode,FormName,Prefix,StartNo,EndNo,Suffix,Username)";
            query = query + " Values('" + SessionCcode + "','" + SessionLcode + "','" + txtFormName.Text + "','" + txtPrefix.Text + "',";
            query = query + "'0','" + txtEndNo.Text + "',";
            query = query + "'" + txtSuffix.Text + "','" + SessionUserName + "')";
            objdata.RptEmployeeMultipleDetails(query);

            if (SaveMode == "Insert")
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Number Setup Details Saved Successfully');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Number Setup Details Updated Successfully');", true);
            }
            Load_Data();
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }

    private void Clear_All_Field()
    {
        txtFormName.Text = "-Select-"; txtStartNo.Text = "";
        txtEndNo.Text = "";
        txtPrefix.Text = "";
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select * from MstNumberingSetup where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FormName='" + txtFormName.Text + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            txtPrefix.Text = DT.Rows[0]["Prefix"].ToString();
            txtStartNo.Text = DT.Rows[0]["StartNo"].ToString();
            txtEndNo.Text = DT.Rows[0]["EndNo"].ToString();
            //txtSuffix.Text = DT.Rows[0]["Suffix"].ToString();
        }
        else
        {
            Clear_All_Field();
        }
    }

    private void Load_Data()
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select FormName,Prefix,StartNo,EndNo,Suffix from MstNumberingSetup where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        Repeater1.DataSource = DT;
        Repeater1.DataBind();
    }

    protected void GridEditClick(object sender, CommandEventArgs e)
    {
        txtFormName.Text = e.CommandName.ToString();
        btnSearch_Click(sender, e);
    }
    protected void GridDeleteClick(object sender, CommandEventArgs e)
    {
        string query = "";
        //User Rights Check
        bool ErrFlag = false;
        bool Rights_Check = false;
        Rights_Check = CommonClass_Function.ModifyRights_Check(SessionCcode, SessionLcode, SessionUserID, "2", "5", "Number Setup");
        if (Rights_Check == false)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Delete Number Setup Details..');", true);
        }
        if (!ErrFlag)
        {
            DataTable DT = new DataTable();
            query = "Select * from MstNumberingSetup where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FormName='" + e.CommandName.ToString() + "'";
            DT = objdata.RptEmployeeMultipleDetails(query);
            if (DT.Rows.Count != 0)
            {
                query = "Delete from MstNumberingSetup where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FormName='" + e.CommandName.ToString() + "'";
                objdata.RptEmployeeMultipleDetails(query);
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Number Setup Details Deleted Successfully');", true);
                Load_Data();
                Clear_All_Field();

            }
        }
    }

    private void Form_Name_Load()
    {
        string query = "";
        DataTable Main_DT = new DataTable();
        txtFormName.Items.Clear();
        query = "Select * from MstFormName order by FormID Asc";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtFormName.Items.Add("-Select-");
        for (int i = 0; i < Main_DT.Rows.Count; i++)
        {
            txtFormName.Items.Add(Main_DT.Rows[i]["FormName"].ToString());
        }
    }
    protected void txtFormName_SelectedIndexChanged(object sender, EventArgs e)
    {
        string query = "";
        DataTable DT = new DataTable();
        if (txtFormName.Text != "-Select-")
        {
            query = "Select * from MstNumberingSetup where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FormName='" + txtFormName.Text + "'";
            DT = objdata.RptEmployeeMultipleDetails(query);
            if (DT.Rows.Count != 0)
            {
                //txtPrefix.Text = DT.Rows[0]["Prefix"].ToString();
                txtStartNo.Text = DT.Rows[0]["StartNo"].ToString();
                txtEndNo.Text = DT.Rows[0]["EndNo"].ToString();
                //txtSuffix.Text = DT.Rows[0]["Suffix"].ToString();
            }
            else
            {
               txtStartNo.Text = ""; txtEndNo.Text = "";
            }
        }
        else
        {
           txtStartNo.Text = ""; txtEndNo.Text = "";
        }
    }

    private void Fin_Year_Code_Join()
    {
        string[] Fin_year_Split=SessionFinYearVal.Split('_');
        TransactionNoGenerate FinYearVal_RightLeft_Check = new TransactionNoGenerate();
        string Fin_year = "/" + FinYearVal_RightLeft_Check.RightVal(Fin_year_Split[0],2) + "-" + FinYearVal_RightLeft_Check.RightVal(Fin_year_Split[1],2);
        txtSuffix.Text = Fin_year;
    }
}
