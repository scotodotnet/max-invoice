﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


using Altius.BusinessAccessLayer.BALDataAccess;

public partial class admin_master_forms_Access_Rights : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "ERP Module :: Access Rights";
            HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Admin_Master"));
            li.Attributes.Add("class", "droplink active open");
            Load_User_Name();
            Load_Module_Name();
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        //GVModule.Rows[1].FindControl("chkAdd"). = "true";

        string query = "";
        DataTable DT_Check = new DataTable();
        DataTable qry_dt = new DataTable();
        string SaveMode = "";
        bool ErrFlag = false;

        //Check User Name
        if (txtUserName.SelectedValue.ToString() == "0")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select User Name...');", true);
        }

        //Check Module Name
        if (txtModuleName.SelectedValue.ToString() == "0")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select Module Name...');", true);
        }

        //Check Menu Name
        if (txtMenuName.SelectedValue.ToString() == "0")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select Menu Name...');", true);
        }
        else
        {
            query = "Select * from MstModuleFormDet where ModuleID='" + txtModuleName.SelectedValue + "' And MenuID='" + txtMenuName.SelectedValue + "'";
            qry_dt = objdata.RptEmployeeMultipleDetails(query);
            if (qry_dt.Rows.Count == 0)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('This Menu Name you do not have rights to access...');", true);
            }
        }

        if (!ErrFlag)
        {


            string ModuleName = txtModuleName.SelectedItem.Text.ToString();
            string ModuleID = txtModuleName.SelectedValue.ToString();

            string MenuName = txtMenuName.SelectedItem.Text.ToString();
            string MenuID = txtMenuName.SelectedValue.ToString();

            string UserName_Str = txtUserName.SelectedItem.Text.ToString();
            string UserID_Str = txtUserName.SelectedValue.ToString();

            query = "Select * from MstModuleFormUserRights where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And UserID='" + UserID_Str + "' And ModuleID='" + ModuleID + "' And MenuID='" + txtMenuName.SelectedValue + "'";
            DT_Check = objdata.RptEmployeeMultipleDetails(query);
            if (DT_Check.Rows.Count != 0)
            {
                query = "Delete from MstModuleFormUserRights where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And UserID='" + UserID_Str + "' And ModuleID='" + ModuleID + "' And MenuID='" + txtMenuName.SelectedValue + "'";
                objdata.RptEmployeeMultipleDetails(query);
            }

            foreach (GridViewRow gvsal in GVModule.Rows)
            {
                SaveMode = "Insert";

                string AddCheck = "0";
                string ModifyCheck = "0";
                string DeleteCheck = "0";
                string ViewCheck = "0";
                string ApproveCheck = "0";
                string PrintCheck = "0";

                string FormName = "";
                string FormID = "0";

                CheckBox ChkAdd = (CheckBox)gvsal.FindControl("chkAdd");
                CheckBox ChkModify = (CheckBox)gvsal.FindControl("chkModify");
                CheckBox ChkDelete = (CheckBox)gvsal.FindControl("chkDelete");
                CheckBox ChkView = (CheckBox)gvsal.FindControl("chkView");
                CheckBox ChkApprove = (CheckBox)gvsal.FindControl("chkApprove");
                CheckBox ChkPrint = (CheckBox)gvsal.FindControl("chkPrintout");
                
                Label FormID_lbl = (Label)gvsal.FindControl("FormID");
                FormID = FormID_lbl.Text.ToString();
                FormName = gvsal.Cells[1].Text.ToString();

                if (ChkAdd.Checked == true) { AddCheck = "1"; }
                if (ChkModify.Checked == true) { ModifyCheck = "1"; }
                if (ChkDelete.Checked == true) { DeleteCheck = "1"; }
                if (ChkView.Checked == true) { ViewCheck = "1"; }
                if (ChkApprove.Checked == true) { ApproveCheck = "1"; }
                if (ChkPrint.Checked == true) { PrintCheck = "1"; }

                //Insert User Rights
                query = "Insert Into MstModuleFormUserRights(Ccode,Lcode,UserID,UserName,ModuleID,ModuleName,MenuID,MenuName,FormID,FormName,AddRights,ModifyRights,DeleteRights,ViewRights,ApproveRights,PrintRights) Values('" + SessionCcode + "',";
                query = query + " '" + SessionLcode + "','" + UserID_Str + "','" + UserName_Str + "','" + ModuleID + "','" + ModuleName + "','" + MenuID + "','" + MenuName + "','" + FormID + "','" + FormName + "','" + AddCheck + "',";
                query = query + " '" + ModifyCheck + "','" + DeleteCheck + "','" + ViewCheck + "','" + ApproveCheck + "','" + PrintCheck + "')";
                objdata.RptEmployeeMultipleDetails(query);
                
            }

            if (SaveMode == "Insert")
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('User Access Rights Details Saved Successfully');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('User Access Rights Details Not Saved Properly...');", true);
            }

            clear();
        }
    }


    public void clear()
    {
        //txtMenuName.SelectedValue = "0";
        //Load_Module_Menu_Form_Data();
        chkAll.Checked = false;
        foreach (GridViewRow gvsal in GVModule.Rows)
        {
            ((CheckBox)gvsal.FindControl("chkAdd")).Checked = false;
            ((CheckBox)gvsal.FindControl("chkModify")).Checked = false;
            ((CheckBox)gvsal.FindControl("chkDelete")).Checked = false;
            ((CheckBox)gvsal.FindControl("chkView")).Checked = false;
            ((CheckBox)gvsal.FindControl("chkApprove")).Checked = false;
            ((CheckBox)gvsal.FindControl("chkPrintout")).Checked = false;
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }

    private void Clear_All_Field()
    {
        txtUserName.SelectedValue = "0"; txtModuleName.SelectedValue = "0"; txtMenuName.SelectedValue = "0";
        Load_Module_Menu_Form_Data();
    }

    protected void btnView_Click(object sender, EventArgs e)
    {
        string query = "";
        DataTable qry_dt = new DataTable();
        bool ErrFlag = false;

        //Check User Name
        if (txtUserName.SelectedValue.ToString() == "0")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select User Name...');", true);
        }

        //Check Module Name
        if (txtModuleName.SelectedValue.ToString() == "0")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select Module Name...');", true);
        }

        //Check Menu Name
        if (txtMenuName.SelectedValue.ToString() == "0")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select Menu Name...');", true);
        }
        else
        {
            query = "Select * from MstModuleFormDet where ModuleID='" + txtModuleName.SelectedValue + "' And MenuID='" + txtMenuName.SelectedValue + "'";
            qry_dt = objdata.RptEmployeeMultipleDetails(query);
            if (qry_dt.Rows.Count == 0)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('This Menu Name you do not have rights to access...');", true);
            }
        }

        if (!ErrFlag)
        {
            Load_Module_Menu_Form_Data();
        }
    }

    private void Load_User_Name()
    {
        DataTable dtempty = new DataTable();
        txtUserName.DataSource = dtempty;
        txtUserName.DataBind();
        DataTable dt = new DataTable();

        string query = "";
        query = "Select '0' as UserID,'-Select-' as UserName union";
        query = query + " Select UserID,UserName from MstUser_Register where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And UserID <> 'Altius' Order by UserName Asc";
        dt = objdata.RptEmployeeMultipleDetails(query);

        txtUserName.DataSource = dt;
        txtUserName.DataTextField = "UserName";
        txtUserName.DataValueField = "UserID";
        txtUserName.DataBind();
    }

    private void Load_Module_Name()
    {
        DataTable dtempty = new DataTable();
        txtModuleName.DataSource = dtempty;
        txtModuleName.DataBind();
        DataTable dt = new DataTable();

        string query = "";
        query = "Select 0 as ModuleID,'-Select-' as ModuleName union";
        query = query + " Select ModuleID,ModuleName from MstModuleName Order by ModuleID Asc";
        dt = objdata.RptEmployeeMultipleDetails(query);

        txtModuleName.DataSource = dt;
        txtModuleName.DataTextField = "ModuleName";
        txtModuleName.DataValueField = "ModuleID";
        txtModuleName.DataBind();
    }

    private void Load_Menu_Name()
    {
        DataTable dtempty = new DataTable();
        txtMenuName.DataSource = dtempty;
        txtMenuName.DataBind();
        DataTable dt = new DataTable();

        string query = "";
        query = "Select 0 as MenuID,'-Select-' as MenuName union";
        query = query + " Select MenuID,MenuName from MstModuleMenuDet where ModuleID='" + txtModuleName.SelectedValue + "' Order by MenuID Asc";
        dt = objdata.RptEmployeeMultipleDetails(query);

        txtMenuName.DataSource = dt;
        txtMenuName.DataTextField = "MenuName";
        txtMenuName.DataValueField = "MenuID";
        txtMenuName.DataBind();
    }

    private void Load_Module_Menu_Form_Data()
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select FormID,FormName from MstModuleFormDet where ModuleID='" + txtModuleName.SelectedValue + "' And MenuID='" + txtMenuName.SelectedValue + "' Order by FormID Asc";
        DT = objdata.RptEmployeeMultipleDetails(query);
        GVModule.DataSource = DT;
        GVModule.DataBind();

        if (GVModule.Rows.Count != 0)
        {
            GVPanel.Visible = true;
        }
        else
        {
            GVPanel.Visible = false;
        }
        Load_User_Rights();
    }

    protected void txtModuleName_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_Menu_Name();
        Load_Module_Menu_Form_Data();
    }

    protected void txtMenuName_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_Module_Menu_Form_Data();
    }

    private void Load_User_Rights()
    {
        string ModuleName = txtModuleName.SelectedItem.Text.ToString();
        string ModuleID = txtModuleName.SelectedValue.ToString();

        string MenuName = txtMenuName.SelectedItem.Text.ToString();
        string MenuID = txtMenuName.SelectedValue.ToString();

        string UserName_Str = txtUserName.SelectedItem.Text.ToString();
        string UserID_Str = txtUserName.SelectedValue.ToString();

        chkAll.Visible = true;

        foreach (GridViewRow gvsal in GVModule.Rows)
        {
            string FormName = "";
            string FormID = "0";

            Label FormID_lbl = (Label)gvsal.FindControl("FormID");
            FormID = FormID_lbl.Text.ToString();
            FormName = gvsal.Cells[1].Text.ToString();

            //Get user Rights for Particular Form
            string query = "";
            DataTable DT = new DataTable();
            query = "Select * from MstModuleFormUserRights where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
            query = query + " And UserID='" + UserID_Str + "' And ModuleID='" + ModuleID + "' And MenuID='" + MenuID + "' And FormID='" + FormID + "'";
            DT = objdata.RptEmployeeMultipleDetails(query);
            if (DT.Rows.Count != 0)
            {
                //User Rights Update in Grid
                if (DT.Rows[0]["AddRights"].ToString() == "1") { ((CheckBox)gvsal.FindControl("chkAdd")).Checked = true; }
                if (DT.Rows[0]["ModifyRights"].ToString() == "1") { ((CheckBox)gvsal.FindControl("chkModify")).Checked = true; }
                if (DT.Rows[0]["DeleteRights"].ToString() == "1") { ((CheckBox)gvsal.FindControl("chkDelete")).Checked = true; }
                if (DT.Rows[0]["ViewRights"].ToString() == "1") { ((CheckBox)gvsal.FindControl("chkView")).Checked = true; }
                if (DT.Rows[0]["ApproveRights"].ToString() == "1") { ((CheckBox)gvsal.FindControl("chkApprove")).Checked = true; }
                if (DT.Rows[0]["PrintRights"].ToString() == "1") { ((CheckBox)gvsal.FindControl("chkPrintout")).Checked = true; }
            }
        }
    }

    protected void chkAll_CheckedChanged(object sender, EventArgs e)
    {
        foreach (GridViewRow gvsal in GVModule.Rows)
        {
            //User Rights Update in Grid
            if (chkAll.Checked == true)
            {
                ((CheckBox)gvsal.FindControl("chkAdd")).Checked = true;
                ((CheckBox)gvsal.FindControl("chkModify")).Checked = true;
                ((CheckBox)gvsal.FindControl("chkDelete")).Checked = true;
                ((CheckBox)gvsal.FindControl("chkView")).Checked = true;
                ((CheckBox)gvsal.FindControl("chkApprove")).Checked = true;
                ((CheckBox)gvsal.FindControl("chkPrintout")).Checked = true;
            }
            else
            {
                ((CheckBox)gvsal.FindControl("chkAdd")).Checked = false;
                ((CheckBox)gvsal.FindControl("chkModify")).Checked = false;
                ((CheckBox)gvsal.FindControl("chkDelete")).Checked = false;
                ((CheckBox)gvsal.FindControl("chkView")).Checked = false;
                ((CheckBox)gvsal.FindControl("chkApprove")).Checked = false;
                ((CheckBox)gvsal.FindControl("chkPrintout")).Checked = false;
            }
        }
    }
}
