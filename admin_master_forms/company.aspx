﻿<%@ Page Title="" Language="C#" MasterPageFile="../MainPage.master" AutoEventWireup="true" CodeFile="company.aspx.cs" Inherits="master_forms_company" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <script src='<%= ResolveUrl("../assets/js/master_list_jquery.min.js") %>'></script>
    <script src='<%= ResolveUrl("../assets/js/master_list_jquery-ui.min.js") %>'></script>
    <link href="<%= ResolveUrl("../assets/css/master_list_jquery-ui.css") %>" rel="stylesheet" type="text/css"/>
    
<script type="text/javascript">
    function SaveMsgAlert(msg) {
        swal(msg);
    }
</script>
<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('.date-picker').datepicker({ format: 'dd/mm/yyyy' });
            }
        });
    };
</script>

<%--Company Code Select List Script Saart--%>
    
    <script type="text/javascript">
        $(function () {
            initializer();
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(initializer);
        });
        function initializer() {
            $("#<%=CompanyCode.ClientID %>").autocomplete({
 
                source: function (request, response) {
 
                      $.ajax({
 
                          url: '<%=ResolveUrl("../List_Service.asmx/GetCompany_Code") %>',
                          data: "{ 'prefix': '" + request.term + "'}",
                          dataType: "json",
                          type: "POST",
                          dataFilter: function (data) { return data; },
                          contentType: "application/json; charset=utf-8",
                          delay:0,
                          success: function (data) {
 
                              response($.map(data.d, function (item) {
 
                                  return {
                                      label: item.split('-')[0],
                                      val: item.split('-')[1],
                                  }
 
                              }))
 
                          },
 
                          error: function (response) {
                              alert(response.responseText);
                          },
                          failure: function (response) {
                              alert(response.responseText);
                          }
                      });
                  }, minLength: 1,
                  focus: function (e, i) {
                      $("#<%=hfCompanyCode.ClientID %>").val(i.item.val);
                      return false;
                  },
                  select: function (e, i) {
                      $("#<%=hfCompanyCode.ClientID %>").val(i.item.val);
 
                  }
 
              });
 
          }
      </script>
<%--Company Code Select List Script Saart--%>

<%--After Save Notification--%>


    
<%--After Save Notification End--%>

<div class="page-breadcrumb">
    <ol class="breadcrumb container">
        <li><a href="../Dashboard.aspx">Home</a></li>
        <li><a href="#">Admin Master</a></li>
    </ol>
</div>
<div class="page-title">
    <div class="container">
        <h3>Company</h3>
    </div>
</div>
<div id="main-wrapper" class="container">
<form class="form-horizontal">

 
 <div class="row form-horizontal">
    <div class="col-md-12">
                      
            <div class="col-md-6">
			<div class="panel panel-white">
			    <div class="panel panel-primary">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">Main</h4>
				</div>
				</div>
				
				<div class="panel-body">
					
						<div class="form-group row">
							<label for="input-Default" class="col-sm-3 control-label">Company Code <span class="mandatory">*</span></label>
							<div class="col-sm-6">
                                <asp:TextBox ID="CompanyCode" MaxLength="20" class="form-control" runat="server" AutoPostBack="True" Style="text-transform: uppercase"></asp:TextBox>
                                <asp:HiddenField ID="hfCompanyCode" runat="server" />
                                <asp:RequiredFieldValidator ControlToValidate="CompanyCode" ValidationGroup="Validate_Field" class="form_error" ID="validate1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" FilterType="Numbers, UppercaseLetters, LowercaseLetters, Custom" TargetControlID="CompanyCode" ValidChars=", "/>
                                
							</div>
                            
                            <asp:LinkButton ID="btnComCodeSearch" class="btn btn-primary btn-addon  btn-rounded btn-sm" runat="server" OnClick="btnComCodeSearch_Click"><i class="fa fa-search"></i>Search</asp:LinkButton>
                            
                    
						</div>
						<div class="form-group row">
							<label for="input-Default" class="col-sm-3 control-label">Company Name <span class="mandatory">*</span></label>
							<div class="col-sm-9">
                                <asp:TextBox ID="CompanyName" class="form-control" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ControlToValidate="CompanyName" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator1" runat="server" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
                                
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender13" runat="server" FilterType="Numbers, UppercaseLetters, LowercaseLetters, Custom" TargetControlID="CompanyName" ValidChars=", "/>

                                
							</div>
						</div>
						<div class="form-group row">
							<label for="input-Default" class="col-sm-3 control-label">Address <span class="mandatory">*</span></label>
							<div class="col-sm-9">
	                            <asp:TextBox ID="Address1" class="form-control" runat="server"></asp:TextBox>
	                            <asp:RequiredFieldValidator ControlToValidate="Address1" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator2" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
                                 
                                 <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" FilterType="Numbers, LowercaseLetters, UppercaseLetters, Custom"
                                ValidChars="./, " TargetControlID="Address1" />
                                 
	                            <br />
                                <asp:TextBox ID="Address2" class="form-control" runat="server"></asp:TextBox><br />
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender9" runat="server" FilterType="Numbers, LowercaseLetters, UppercaseLetters, Custom"
                                ValidChars="./, " TargetControlID="Address2" />
                                
                                <asp:TextBox ID="Address3" class="form-control" runat="server"></asp:TextBox> 
                                
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender10" runat="server" FilterType="Numbers, LowercaseLetters, UppercaseLetters, Custom"
                                ValidChars="./, " TargetControlID="Address3" />                               
							</div>
						</div>
						
						<div class="form-group row">
							<label for="input-Default" class="col-sm-3 control-label">City <span class="mandatory">*</span></label>
							<div class="col-sm-4">
                                <asp:TextBox ID="City" class="form-control" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ControlToValidate="City" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator3" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
                                
                                
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server" FilterType="LowercaseLetters, UppercaseLetters"
                                 TargetControlID="City" />
                                
							</div>
							<label for="input-Default" class="col-sm-2 control-label">Pincode <span class="mandatory">*</span></label>
							<div class="col-sm-3">
                                <asp:TextBox ID="Pincode" MaxLength="6" class="form-control" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ControlToValidate="Pincode" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator4" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="RegExValPin" ValidationExpression="[0-9]{6}" runat="server" class="form_error" 
                                    ControlToValidate="Pincode" ValidationGroup="Validate_Field" ErrorMessage="Pincode length 6">
                                </asp:RegularExpressionValidator>
                                <cc1:FilteredTextBoxExtender ID="FilTxtPin1" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="Pincode" ValidChars="0123456789">
                                </cc1:FilteredTextBoxExtender>
							</div>
						</div>
						<div class="clearfix visible-xs-block"></div>
						<div class="form-group row">
							<label for="input-Default" class="col-sm-3 control-label">State <span class="mandatory">*</span></label>
							<div class="col-sm-4">
                                <asp:DropDownList ID="State" runat="server" class="form-control">
                                <asp:ListItem Value="1" Text="Tamil Nadu"></asp:ListItem>
                                </asp:DropDownList>
							</div>
							<label for="input-Default" class="col-sm-2 control-label">Country <span class="mandatory">*</span></label>
							<div class="col-sm-3">
                                <asp:DropDownList ID="Country" runat="server" class="form-control">
                                <asp:ListItem Value="1" Text="India"></asp:ListItem>
                                </asp:DropDownList>
							</div>
						</div>
						<div class="form-group row">
							<label for="input-Default" class="col-sm-3 control-label">Tel No.</label>
							<div class="col-sm-3">
							    <asp:TextBox ID="txtStdCode" MaxLength="5" class="form-control" runat="server"></asp:TextBox>
							    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtStdCode" ValidChars="0123456789">
                                </cc1:FilteredTextBoxExtender>
							</div>
							<div class="col-sm-6">
                                <asp:TextBox ID="Tel_no" MaxLength="7" class="form-control" runat="server"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegExpValdTelNo" ValidationExpression="[0-9]{7}" runat="server" class="form_error" 
                                    ControlToValidate="Tel_no" ValidationGroup="Validate_Field" ErrorMessage="Tel.No length 7">
                                </asp:RegularExpressionValidator>
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="Tel_no" ValidChars="0123456789">
                                </cc1:FilteredTextBoxExtender>
							</div>							
						</div>
						<div class="form-group row">
						    <label for="input-Default" class="col-sm-3 control-label">Mobile <span class="mandatory">*</span></label>
							<div class="col-sm-3">
							    <asp:TextBox ID="txtMblCode" Text="+91" class="form-control" runat="server" ReadOnly="true"></asp:TextBox>
							</div>
							<div class="col-sm-6">
                                <asp:TextBox ID="Mobile" MaxLength="10" class="form-control" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ControlToValidate="Mobile" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator5" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ValidationExpression="[0-9]{10}" runat="server" class="form_error" 
                                    ControlToValidate="Mobile" ValidationGroup="Validate_Field" ErrorMessage="Mobile.No length 10">
                                </asp:RegularExpressionValidator>
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="Mobile" ValidChars="0123456789">
                                </cc1:FilteredTextBoxExtender>
							</div>
						</div>
						<div class="form-group row">
							<label for="input-Default" class="col-sm-3 control-label">Mail ID <span class="mandatory">*</span></label>
							<div class="col-sm-9">
                                <asp:TextBox ID="Mail_Id" class="form-control" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ControlToValidate="Mail_Id" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator6" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" class="form_error" runat="server" ControlToValidate="Mail_Id" 
                                    Display ="Dynamic" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" 
                                    ValidationGroup="Validate_Field" ErrorMessage="enter valid email address. Eg. Something@domain.com">
                                </asp:RegularExpressionValidator>
							</div>
						</div>
						
						<%--<div class="txtcenter">
						<asp:Button ID="Button1" class="btn btn-success"  runat="server" Text="Apply" />
                        <asp:Button ID="Button2" class="btn btn-primary" runat="server" Text="Reset" />
                        <asp:Button ID="Button3" class="btn btn-primary" runat="server" Text="Clear" />
                        <asp:Button ID="Button4" class="btn btn-primary" runat="server" Text="Cancel" />
						</div>--%>
						
				</div><!-- panel body end -->
				
			</div><!-- panel white end -->
		    </div><!-- col-6 end -->
		    
            <div class="col-md-6">
			<div class="panel panel-white">
			    <div class="panel panel-primary">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">Other</h4>
				</div>
				</div>
				
				<div class="panel-body">
					
					<%--<div class="form-group">
						<label for="input-Default" class="col-sm-3 control-label">Currency </label>
						<div class="col-sm-6">
                            <asp:TextBox ID="currency" class="form-control"  runat="server"></asp:TextBox>
						</div>
					</div>--%>
					<div class="form-group row">
						<label for="input-Default" class="col-sm-2 control-label">Category <span class="mandatory">*</span></label>
						<div class="col-sm-4">
                            <asp:TextBox ID="category" class="form-control"  runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ControlToValidate="category" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator7" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                            </asp:RequiredFieldValidator>
						</div>
					<%--</div>
					<div class="form-group">--%>
						<label for="input-Default" class="col-sm-2 control-label">Sub-Category <span class="mandatory">*</span></label>
						<div class="col-sm-4">
                            <asp:TextBox ID="sub_category" class="form-control"  runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ControlToValidate="sub_category" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator8" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                            </asp:RequiredFieldValidator>
						</div>
					</div>
					<div class="form-group">
						<label for="input-Default" class="col-sm-2 control-label">Class</label>
						<div class="col-sm-9"> 
                            <asp:RadioButtonList ID="RdbClass" runat="server"  class="col-sm-9" RepeatColumns="3" >
                            <asp:ListItem Value="1" Text="None" Selected="True"></asp:ListItem>
                            <asp:ListItem Value="2" Text="Private"></asp:ListItem>
                            <asp:ListItem Value="3" Text="Public"></asp:ListItem>
                            </asp:RadioButtonList>
                         </div>
       				</div>
					<div class="form-group">
						<label for="input-Default" class="col-sm-2 control-label">ROC Code <span class="mandatory">*</span></label>
						<div class="col-sm-4">
                            <asp:TextBox ID="roc_code" class="form-control" MaxLength="21" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ControlToValidate="roc_code" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator9" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
						</div>
						<label for="input-Default" class="col-sm-2 control-label">Incorp Date <span class="mandatory">*</span></label>
						<div class="col-sm-4">
                            <asp:TextBox ID="incorp_date" class="form-control date-picker"  runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ControlToValidate="incorp_date" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator10" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                            </asp:RequiredFieldValidator>
						</div>
					</div>
					<div class="form-group">
						<label for="input-Default" class="col-sm-2 control-label">ROC Reg No <span class="mandatory">*</span></label>
						<div class="col-sm-4">
                            <asp:TextBox ID="roc_reg" class="form-control"  runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ControlToValidate="roc_reg" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator11" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                            </asp:RequiredFieldValidator>
						</div>
						<label for="input-Default" class="col-sm-2 control-label">Service TAX No <span class="mandatory">*</span></label>
						<div class="col-sm-4">
                            <asp:TextBox ID="service_tax" MaxLength="15" class="form-control"  runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ControlToValidate="service_tax" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator12" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                            </asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator3" ValidationExpression="[a-zA-Z0-9]{15}" runat="server" class="form_error" 
                                    ControlToValidate="service_tax" ValidationGroup="Validate_Field" ErrorMessage="Service Tax No: 15 digit AlphaNumeric">
                            </asp:RegularExpressionValidator>
                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR4" runat="server" FilterMode="ValidChars"
                                FilterType="UppercaseLetters,LowercaseLetters,Custom" TargetControlID="service_tax"
                                ValidChars="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789">
                            </cc1:FilteredTextBoxExtender>
						</div>
						
					</div>
					<div class="form-group">
						<label for="input-Default" class="col-sm-2 control-label">TIN No <span class="mandatory">*</span></label>
						<div class="col-sm-4">
                            <asp:TextBox ID="tin_no" class="form-control" MaxLength="11"  runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ControlToValidate="tin_no" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator13" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                            </asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegExp1" runat="server" ErrorMessage="TIN NO length 11" class="form_error" 
                                ControlToValidate="tin_no" ValidationExpression="[0-9]{11}" ValidationGroup="Validate_Field"/>
                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR11" runat="server" FilterMode="ValidChars"
                                FilterType="Custom,Numbers" TargetControlID="tin_no" ValidChars="0123456789">
                            </cc1:FilteredTextBoxExtender>
                            
                            
                            
						</div>
						<label for="input-Default" class="col-sm-2 control-label">TIN Date <span class="mandatory">*</span></label>
						<div class="col-sm-4">
                            <asp:TextBox ID="tin_date" class="form-control date-picker"  runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ControlToValidate="tin_date" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator14" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                            </asp:RequiredFieldValidator>
                            <%--<cc1:CalendarExtender ID="Caleder_txtdob" runat="server" TargetControlID="tin_date"
                                Format="dd-MM-yyyy">
                            </cc1:CalendarExtender>--%>
						</div>
					</div>
					<div class="form-group">
					    <label for="input-Default" class="col-sm-2 control-label">CST No <span class="mandatory">*</span></label>
						<div class="col-sm-4">
                            <asp:TextBox ID="cst_no" class="form-control"  runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ControlToValidate="cst_no" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator15" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                            </asp:RequiredFieldValidator>
                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender12" runat="server" FilterType="Numbers, LowercaseLetters, UppercaseLetters, Custom"
                            ValidChars="./, " TargetControlID="cst_no" />          
						</div>
						<label for="input-Default" class="col-sm-2 control-label">PAN No <span class="mandatory">*</span></label>
						<div class="col-sm-4">
                            <asp:TextBox ID="pan_no" class="form-control" MaxLength="10" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ControlToValidate="pan_no" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator16" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                            </asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator4" ValidationExpression="[a-zA-Z0-9]{10}" runat="server" class="form_error" 
                                    ControlToValidate="pan_no" ValidationGroup="Validate_Field" ErrorMessage="Pan No: 10 digit AlphaNumeric">
                            </asp:RegularExpressionValidator>
                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR3" runat="server" FilterMode="ValidChars"
                                FilterType="UppercaseLetters,LowercaseLetters,Custom" TargetControlID="pan_no"
                                ValidChars="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789">
                            </cc1:FilteredTextBoxExtender>
						</div>
						
					</div>
					<div class="form-group">
					    <label for="input-Default" class="col-sm-2 control-label">Others-1</label>
						<div class="col-sm-10">
                            <asp:TextBox ID="others1" class="form-control"  runat="server"></asp:TextBox>
						</div>
					</div>
					<div class="form-group">
						<label for="input-Default" class="col-sm-2 control-label">Others-2</label>
						<div class="col-sm-10">
                            <asp:TextBox ID="others2" class="form-control"  runat="server"></asp:TextBox>
						</div>
						</div>
					<div class="form-group">
						<label for="input-Default" class="col-sm-2 control-label">Others-3</label>
						<div class="col-sm-10">
                            <asp:TextBox ID="others3" class="form-control"  runat="server"></asp:TextBox>
						</div>
					</div>
	                <br />
		          <%--  <div class="txtcenter">
                    <asp:Button ID="Button5" class="btn btn-primary"  runat="server" Text="Apply" />
                    <asp:Button ID="Button6" class="btn btn-primary" runat="server" Text="Reset" />
                    <asp:Button ID="Button7" class="btn btn-primary" runat="server" Text="Clear" />
                    <asp:Button ID="Button8" class="btn btn-primary" runat="server" Text="Cancel" />
	                </div>--%>
						
				</div><!-- panel body end -->
				
			</div><!-- panel white end -->
		    </div><!-- col-6 end -->		    
            
      
         
      </div><!-- col 12 end -->
  </div><!-- row end -->
  
  
<div class="row">
    <div class="col-md-12">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <div class="panel panel-white">
                <div class="panel-body">
                    <div class="txtcenter">
                    <asp:Button ID="btnSave" class="btn btn-success"  runat="server" Text="Save" ValidationGroup="Validate_Field" 
                            onclick="btnSave_Click" ClientIDMode="Static" />
                    <asp:Button ID="BtnCancel" class="btn btn-danger" runat="server" Text="Cancel" OnClick="btnCancel_Click"/>
                    </div>
                </div>
            </div><!-- panel white end -->
        </div>
        <div class="col-md-3"></div>
    </div><!-- col 12 end -->
  </div><!-- row end -->
  


  
  </form>
 </div><!-- main-wrapper end -->
 
 
 
 <!-- popup content  start -->
 
 <div id="main_search_content" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
               
            </div>
            <div class="modal-body">
               <div class="panel-body">
                                <div class="panel panel-white">
                                <div class="panel-heading clearfix">
                                    <h4 class="panel-title">Company Details</h4>
                                    <div class="form-group row content_right">
                                    <h4>Search&nbsp;&nbsp;</h4><asp:TextBox ID="searchbox" class="form-control"  runat="server"></asp:TextBox>
                                    </div>
                                </div><br />
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Comp_Code</th>
                                                    <th>Comp_Name</th>
                                                    <th>Comp_Add1</th>
                                                    <th>Comp_Add2</th>
                                                    <th>Comp_Add3</th>
                                                    <th>Comp_City</th>
                                                    <th>Comp_Zipcode</th>
                                                    <th>Comp_State</th>
                                                    <th>Comp_Country</th>
                                                    <th>Comp_Phone_No</th>
                                                    <th>Comp_Fax_No</th>
                                                    <th>Comp_Mail_ID</th>
                                                    <th>Comp_Currency</th>
                                                    <th>Comp_Category</th>
                                                    <th>Comp_Subcategory</th>
                                                    <th>Comp_Class</th>
                                                    <th>Comp_ROC_Code</th>
                                                    <th>Comp_ROC_Reg_No</th>
                                                    <th>Comp_Incorp_Date</th>
                                                    <th>Comp_PAN_No</th>
                                                    <th>Comp_TIN_No</th>
                                                    <th>Comp_CST_No</th>
                                                    <th>Comp_Service_Tax_Reg_No</th>
                                                    <th>Comp_Others_1</th>
                                                    <th>Comp_Others_2</th>
                                                    <th>Comp_Others_3</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <th scope="row">1</th>
                                                    <td>Table cell</td>
                                                    <td>Table cell</td>
                                                    <td>Table cell</td>
                                                    <td>Table cell</td>
                                                    <td>Table cell</td>
                                                    <td>Table cell</td>
                                                    <td>Table cell</td>
                                                    <td>Table cell</td>
                                                    <td>Table cell</td>
                                                    <td>Table cell</td>
                                                    <td>Table cell</td>
                                                    <td>Table cell</td>
                                                    <td>Table cell</td>
                                                    <td>Table cell</td>
                                                    <td>Table cell</td>
                                                    <td>Table cell</td>
                                                    <td>Table cell</td>
                                                    <td>Table cell</td>
                                                    <td>Table cell</td>
                                                    <td>Table cell</td>
                                                    <td>Table cell</td>
                                                    <td>Table cell</td>
                                                    <td>Table cell</td>
                                                    <td>Table cell</td>
                                                    <td>Table cell</td>
                                                    <td>Table cell</td>
                                                    
                                                </tr>
                                                <tr>
                                                    <th scope="row">2</th>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    
                                                </tr>
                                                
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                                </div>
                            </div>
            <div class="modal-footer">
                <div class="form-group row content_right">
                <h4>Choose Fields&nbsp;&nbsp;</h4>
                <asp:DropDownList ID="DropDownList1" runat="server">
                <asp:ListItem Value="1" Text="Comp_Code"></asp:ListItem>
                <asp:ListItem Value="2" Text="Comp_Name"></asp:ListItem>
                <asp:ListItem Value="3" Text="Comp_Add1"></asp:ListItem>
                <asp:ListItem Value="4" Text="Comp_Add2"></asp:ListItem>
                <asp:ListItem Value="5" Text="Comp_Add3"></asp:ListItem>
                <asp:ListItem Value="6" Text="Comp_City"></asp:ListItem>
                <asp:ListItem Value="7" Text="Comp_Zipcode"></asp:ListItem>
                <asp:ListItem Value="8" Text="Comp_State"></asp:ListItem>
                <asp:ListItem Value="9" Text="Comp_Country"></asp:ListItem>
                <asp:ListItem Value="10" Text="Comp_Phone_No"></asp:ListItem>
                <asp:ListItem Value="12" Text="Comp_Fax_No"></asp:ListItem>
                <asp:ListItem Value="13" Text="Comp_Mail_ID"></asp:ListItem>
                <asp:ListItem Value="14" Text="Comp_Currency"></asp:ListItem>
                <asp:ListItem Value="15" Text="Comp_Category"></asp:ListItem>
                <asp:ListItem Value="16" Text="Comp_Subcategory"></asp:ListItem>
                <asp:ListItem Value="17" Text="Comp_Class"></asp:ListItem>
                <asp:ListItem Value="18" Text="Comp_ROC_Code"></asp:ListItem>
                <asp:ListItem Value="19" Text="Comp_ROC_Reg_No"></asp:ListItem>
                <asp:ListItem Value="20" Text="Comp_Incorp_Date"></asp:ListItem>
                <asp:ListItem Value="21" Text="Comp_PAN_No"></asp:ListItem>
                <asp:ListItem Value="22" Text="Comp_TIN_No"></asp:ListItem>
                <asp:ListItem Value="23" Text="Comp_CST_No"></asp:ListItem>
                <asp:ListItem Value="24" Text="Comp_Service_Tax_Reg_No"></asp:ListItem>
                <asp:ListItem Value="25" Text="Comp_Others_1"></asp:ListItem>
                <asp:ListItem Value="26" Text="Comp_Others_2"></asp:ListItem>
                <asp:ListItem Value="26" Text="Comp_Others_3"></asp:ListItem>
                </asp:DropDownList>
                </div>
                
            </div>
        </div>
    </div>
</div>
<!-- popup content  end -->
</asp:Content>

