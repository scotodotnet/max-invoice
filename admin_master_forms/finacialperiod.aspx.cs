﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


using Altius.BusinessAccessLayer.BALDataAccess;

public partial class admin_master_forms_finacialperiod : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "ERP Module :: Finacial Period";
            HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Admin_Master"));
            li.Attributes.Add("class", "droplink active open");
            Get_Year_Code();
        }
        LoadDataINGrid();
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        string SaveMode = "Insert";
        string query = "";
        DataTable DT = new DataTable();

        //Check Financial Year Start

        string[] StartDateSpilit;
        bool ErrFlag = false;
        string EndingDate = "";
        string EndingMonth = "";
        string EndingYear = "";
        StartDateSpilit = txtStartingPeriod.Text.Split('/');

        //Starting Date check
        if (Convert.ToDecimal(StartDateSpilit[0].ToString()) != 1)
        {
            Page.Focus();
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check with Starting Date...');", true);
        }

        //Starting Date check with Month
        if (Convert.ToDecimal(StartDateSpilit[1].ToString()) != 4)
        {
            Page.Focus();
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check with Starting Date Month...');", true);
        }
        //get Current Financial year
        int currentYear = GetFinancialYear;
        if (Convert.ToDecimal(currentYear) < Convert.ToDecimal(StartDateSpilit[2].ToString())) //Financial Year Check
        {
            Page.Focus();
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check with Starting Date Year...');", true);
        }

        bool Rights_Check = false;
        ////User Rights Check
        //if (btnSave.Text == "Update")
        //{
        //    Rights_Check = CommonClass_Function.ModifyRights_Check(SessionCcode, SessionLcode, SessionUserID, "2", "5", "Fin. Year");
        //    if (Rights_Check == false)
        //    {
        //        ErrFlag = true;
        //        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Modify Finacial Period Details..');", true);
        //    }
        //}
        //else
        //{
        //    Rights_Check = CommonClass_Function.AddRights_Check(SessionCcode, SessionLcode, SessionUserID, "2", "5", "Fin. Year");
        //    if (Rights_Check == false)
        //    {
        //        ErrFlag = true;
        //        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Adding New Finacial Period..');", true);
        //    }
        //}

        if (!ErrFlag)
        {
            Page.Focus();
            EndingDate = "31";
            EndingMonth = (Convert.ToDecimal(StartDateSpilit[1].ToString()) - Convert.ToDecimal(1)).ToString();
            if (EndingMonth.Length == 1)
            {
                EndingMonth = "0" + EndingMonth;
            }
            EndingYear = (Convert.ToDecimal(StartDateSpilit[2].ToString()) + Convert.ToDecimal(1)).ToString();
            txtEndingPeriod.Text = EndingDate + "/" + EndingMonth + "/" + EndingYear;
            txtFinacialYear.Text = StartDateSpilit[2].ToString() + "_" + EndingYear;
        }

        //Check Financial Year End
        if (!ErrFlag)
        {
            query = "Select * from MstFinacialPeriod where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And YearCode='" + txtYearCode.Text + "'";
            DT = objdata.RptEmployeeMultipleDetails(query);
            if (DT.Rows.Count != 0)
            {
                SaveMode = "Update";
                query = "Delete from MstFinacialPeriod where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And YearCode='" + txtYearCode.Text + "'";
                objdata.RptEmployeeMultipleDetails(query);
            }
            query = "Insert Into MstFinacialPeriod(Ccode,Lcode,YearCode,StartingPeriod,EndingPeriod,FinacialYear,ActiveMode,UserID,UserName)";
            query = query + " Values('" + SessionCcode + "','" + SessionLcode + "','" + txtYearCode.Text + "','" + txtStartingPeriod.Text + "',";
            query = query + " '" + txtEndingPeriod.Text + "','" + txtFinacialYear.Text + "','" + RdpFinMode.SelectedValue + "','" + SessionUserID + "','" + SessionUserName + "')";
            objdata.RptEmployeeMultipleDetails(query);

            if (SaveMode == "Update")
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Finacial Period Details Updated Successfully');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Finacial Period Details Saved Successfully');", true);
            }

            LoadDataINGrid();
            Clear_All_Field();
            btnSave.Text = "Save";
        }
        //Page.ClientScript.RegisterStartupScript(this.GetType(), "reset", " Confirm();", true);
        //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "Confirm();", true);
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }

    private void Clear_All_Field()
    {
        txtYearCode.Text = ""; txtStartingPeriod.Text = ""; txtEndingPeriod.Text = ""; txtFinacialYear.Text = "";
        RdpFinMode.SelectedValue = "1";
        Get_Year_Code();
    }

    protected void GridEditEnquiryClick(object sender, CommandEventArgs e)
    {
        txtYearCode.Text = e.CommandName.ToString();
        btnSearch_Click(sender, e);
        
    }
    protected void GridDeleteEnquiryClick(object sender, CommandEventArgs e)
    {
        string query = "";
        //User Rights Check
        bool ErrFlag = false;
        bool Rights_Check = false;
        Rights_Check = CommonClass_Function.ModifyRights_Check(SessionCcode, SessionLcode, SessionUserID, "2", "5", "Fin. Year");
        if (Rights_Check == false)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Delete Finacial Period Details..');", true);
        }
        if (!ErrFlag)
        {
            DataTable DT = new DataTable();
            query = "Select * from MstFinacialPeriod where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And YearCode='" + e.CommandName.ToString() + "'";
            DT = objdata.RptEmployeeMultipleDetails(query);
            if (DT.Rows.Count != 0)
            {
                //Delete MstFinacialPeriod
                query = "Delete from MstFinacialPeriod where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And YearCode='" + e.CommandName.ToString() + "'";
                objdata.RptEmployeeMultipleDetails(query);

                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Finacial Period Details Deleted Successfully');", true);
                LoadDataINGrid();
                Clear_All_Field();
            }
        }
    }

    private void LoadDataINGrid()
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select * from MstFinacialPeriod where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        Repeater1.DataSource = DT;
        Repeater1.DataBind();
    }

    private void Get_Year_Code()
    {
        string Year_Code = "1";
        string query = "";
        DataTable DT = new DataTable();
        query = "Select * from MstFinacialPeriod where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' Order by YearCode Desc";
        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            Year_Code = (Convert.ToDecimal(DT.Rows[0]["YearCode"].ToString()) + Convert.ToDecimal(1)).ToString();
        }
        else
        {
            Year_Code = "1";
        }
        txtYearCode.Text = Year_Code;


    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        //Search Finacial Period
        string query = "";
        DataTable Main_DT = new DataTable();
        query = "Select * from MstFinacialPeriod where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And YearCode='" + txtYearCode.Text + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        if (Main_DT.Rows.Count != 0)
        {
            txtStartingPeriod.Text = Main_DT.Rows[0]["StartingPeriod"].ToString();
            txtEndingPeriod.Text = Main_DT.Rows[0]["EndingPeriod"].ToString();
            txtFinacialYear.Text = Main_DT.Rows[0]["FinacialYear"].ToString();
            RdpFinMode.SelectedValue = Main_DT.Rows[0]["ActiveMode"].ToString();

            btnSave.Text = "Update";
        }
        else
        {
            Clear_All_Field();
        }
    }

    protected void txtStartingPeriod_TextChanged(object sender, EventArgs e)
    {
        string[] StartDateSpilit;
        bool ErrFlag = false;
        string EndingDate = "";
        string EndingMonth = "";
        string EndingYear = "";

        if (txtStartingPeriod.Text != "")
        {
            StartDateSpilit = txtStartingPeriod.Text.Split('/');

            //Starting Date check
            if (Convert.ToDecimal(StartDateSpilit[0].ToString()) != 1)
            {
                Page.Focus();
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check with Starting Date...');", true);
            }

            //Starting Date check with Month
            if (Convert.ToDecimal(StartDateSpilit[1].ToString()) != 4)
            {
                Page.Focus();
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check with Starting Date Month...');", true);
            }
            //get Current Financial year
            int currentYear = GetFinancialYear;
            if (Convert.ToDecimal(currentYear) < Convert.ToDecimal(StartDateSpilit[2].ToString())) //Financial Year Check
            {
                Page.Focus();
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check with Starting Date Year...');", true);
            }

            if (!ErrFlag)
            {
                Page.Focus();
                EndingDate = "01";
                EndingMonth = (Convert.ToDecimal(StartDateSpilit[1].ToString()) - Convert.ToDecimal(1)).ToString();
                if (EndingMonth.Length == 1)
                {
                    EndingMonth = "0" + EndingMonth;
                }
                EndingYear = (Convert.ToDecimal(StartDateSpilit[2].ToString()) + Convert.ToDecimal(1)).ToString();
                txtEndingPeriod.Text = EndingDate + "/" + EndingMonth + "/" + EndingYear;
                txtFinacialYear.Text = StartDateSpilit[2].ToString() + "_" + EndingYear;
            }
        }
    }

    public static int GetFinancialYear
    {
        get
        {
            DateTime date = System.DateTime.Today;
            if (date.Month >= 1 && date.Month <= 3)
                return date.Year - 1;
            else
                return date.Year;
        }
    }
}
