﻿<%@ Page Title="" Language="C#" MasterPageFile="../MainPage.master" AutoEventWireup="true" CodeFile="finacialperiod.aspx.cs" Inherits="admin_master_forms_finacialperiod" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<link href="<%= ResolveUrl("assets/plugins/toastr/toastr.min.css") %>" rel="stylesheet" type="text/css"/>
<script src='<%= ResolveUrl("assets/js/master_list_jquery.min.js") %>'></script>
    <script src='<%= ResolveUrl("assets/js/master_list_jquery-ui.min.js") %>'></script>
    <link href="<%= ResolveUrl("assets/css/master_list_jquery-ui.css") %>" rel="stylesheet" type="text/css"/>
    

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#example').dataTable();
            }
        });
    };
</script>

<script type="text/javascript">
    function SaveMsgAlert(msg) {
        swal(msg);
        //$('.date-picker').datepicker();
        $('.datepicker').hide();
//        $('.date-picker').Close();
    }
    
</script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('.date-picker').datepicker({ format: 'dd/mm/yyyy' });
                $('.js-states').select2();
            }
        });
    };
</script>

<script type="text/javascript">
        function resetFields() {
            //alert("Test"); //do all the reset work here
            ShowPopup("Saved");
        }
    </script>
    
<script type="text/javascript">
//    function ShowPopup(message) {
//        $(function() {
//            //Command: toastr["success"]("Have fun storming the castle!")

//            toastr.options = {
//                "closeButton": true,
//                "debug": false,
//                "newestOnTop": false,
//                "progressBar": false,
//                "positionClass": "toast-bottom-center",
//                "preventDuplicates": false,
//                "onclick": null,
//                "showDuration": "300",
//                "hideDuration": "1000",
//                "timeOut": "5000",
//                "extendedTimeOut": "1000",
//                "showEasing": "swing",
//                "hideEasing": "linear",
//                "showMethod": "fadeIn",
//                "hideMethod": "fadeOut"
//            };
//            toastr.success('Location details saved successfully.');
//        });
//    };



    function Confirm() {
//        ShowPopup("Saved");
        //        alert("Tested");

        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-bottom-center",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };
        toastr.success('Location details saved successfully.');

        };

</script>

<script type="text/javascript">
    function ShowPopup(message) {
        $(function() {
            //Command: toastr["success"]("Have fun storming the castle!")

            toastr.options = {
                "closeButton": true,
                "debug": false,
                "newestOnTop": false,
                "progressBar": false,
                "positionClass": "toast-bottom-center",
                "preventDuplicates": false,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            };
            toastr.success('Location details saved successfully.');
        });
    };
</script>
<%--After Save Notification End--%>
    
<div class="page-breadcrumb">
                    <ol class="breadcrumb container">
                       <h4><li class="active">Finacial Period</li></h4> 
                    </ol>
                </div>
<div id="main-wrapper" class="container">
 <div class="row">
    <div class="col-md-12">
        
           
            <div class="col-md-9">
			<div class="panel panel-white">
			<div class="panel panel-primary">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">Finacial Period</h4>
				</div>
			</div>	
				<form class="form-horizontal">
			
				<div class="panel-body">
					<div class="col-md-12">
					    <div class="row">
					        <div class="form-group col-md-2">
					            <label for="exampleInputName">Year Code</label>
					            <asp:Label ID="txtYearCode" runat="server" class="form-control"></asp:Label>
					        </div>
					        <div class="form-group col-md-2">
					            <label for="exampleInputName">Starting Period<span class="mandatory">*</span></label>
					            <asp:TextBox ID="txtStartingPeriod" class="form-control date-picker" 
                                    runat="server" ontextchanged="txtStartingPeriod_TextChanged" AutoPostBack="true"></asp:TextBox><br/>
					        </div>
					        <div class="form-group col-md-2">
					            <label for="exampleInputName">Ending Period</label>
					            <asp:Label ID="txtEndingPeriod" runat="server" class="form-control"></asp:Label>
					        </div>
					        <div class="form-group col-md-3">
					            <label for="exampleInputName">Finacial Year</label>
					            <asp:Label ID="txtFinacialYear" runat="server" class="form-control"></asp:Label>
					        </div>
					        <div class="form-group col-md-3">
					            <label for="exampleInputName">Mode</label>
					            <asp:RadioButtonList ID="RdpFinMode" class="form-control" runat="server" RepeatColumns="2">
                                    <asp:ListItem Value="1" Text="Active" style="padding-right:20px" Selected="True"></asp:ListItem>
                                    <asp:ListItem Value="2" Text="Deactive" ></asp:ListItem>
                                </asp:RadioButtonList>
					        </div>
					    </div>
					</div>
					
					
                    <!-- Button start -->
                    <div class="txtcenter">
                        <asp:Button ID="btnSave" class="btn btn-success"  runat="server" Text="Save" ValidationGroup="Validate_Field" OnClick="btnSave_Click"/>
                        <%--<asp:Button ID="Button1" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg" runat="server" Text="View" />--%>
                        <asp:Button ID="btnCancel" class="btn btn-danger" runat="server" Text="Cancel" OnClick="btnCancel_Click"/>
                    </div>
                    <!-- Button end -->
                    <div class="form-group row"></div>	
					<!-- table start -->
					<div class="col-md-12">
					    <div class="row">
					        <asp:Panel ID="PanelmaterialRequestItem" runat="server">
					            <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
			                        <HeaderTemplate>
                                        <table class="table table-hover">
                                            <thead>
                                                <tr>
                                                    <th>S.No</th>
                                                    <th>Year Code</th>
                                                    <th>Starting</th>
                                                    <th>Ending</th>
                                                    <th>Fin. Year</th>
                                                    <th>Mode</th>
                                                </tr>
                                            </thead>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td><%# Container.ItemIndex + 1 %></td>
                                            <td><%# Eval("YearCode")%></td>
                                            <td><%# Eval("StartingPeriod")%></td>
                                            <td><%# Eval("EndingPeriod")%></td>
                                            <td><%# Eval("FinacialYear")%></td>
                                            <td>
                                                <asp:LinkButton ID="btnEditEnquiry_Grid" class="btn btn-success btn-sm fa fa-pencil"  runat="server" 
                                                        Text="" OnCommand="GridEditEnquiryClick" CommandArgument="Edit" CommandName='<%# Eval("YearCode")%>'>
                                                    </asp:LinkButton>
                                                <asp:LinkButton ID="btnDeleteEnquiry_Grid" class="btn btn-danger btn-sm fa fa-trash-o"  runat="server" 
                                                    Text="" OnCommand="GridDeleteEnquiryClick" CommandArgument="Delete" CommandName='<%# Eval("YearCode")%>' 
                                                    CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this Finacial Period details?');">
                                                </asp:LinkButton>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate></table></FooterTemplate>                                
			                    </asp:Repeater>
			                </asp:Panel>
					    </div>
					</div>
					<!-- table End -->
					
				</div><!-- panel body end -->

				</form>
			</div><!-- panel white end -->
		    </div><!-- col-9 end -->
		    
		    <!-- Dashboard start -->
		     <div class="col-lg-3 col-md-6">
                            <div class="panel panel-white" style="height: 100%;">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Dashboard Details</h4>
                                    <div class="panel-control">
                                        
                                        
                                    </div>
                                </div>
                                <div class="panel-body">
                                    
                                    
                                </div>
                            </div>
                        </div>  
                        
                        <div class="col-lg-3 col-md-6">
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div class="live-tile flip ha" data-mode="flip" data-speed="750" data-delay="3000">
                                       
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div class="live-tile flip ha" data-mode="flip" data-speed="750" data-delay="3000">
                                       
                                    </div>
                                </div>
                            </div>
                            
                        </div> 
		    <!-- Dashboard End -->
		    
		     
		    
		   <div class="col-md-2"></div>
	
  </div><!-- col 12 end -->
  </div><!-- row end -->
 </div><!-- main-wrapper end -->
 
</asp:Content>

