﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;


public partial class Transaction_YarnDelivery : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionEnquiryNo;
    string SessionFinYearCode;
    string SessionFinYearVal;
    string SessionTrans_No;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "ERP Module :: Yarn Delivery";
            HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Transaction"));
            li.Attributes.Add("class", "droplink active open");
            Initial_Data_Referesh();

            Load_Unit();
            Load_FromGoDown();
            Load_ToGoDown();
            Load_LotNo();
            Load_Variety();

            if (Session["Trans_No"] == null)
            {
                SessionTrans_No = "";
            }
            else
            {
                SessionTrans_No = Session["Trans_No"].ToString();
                txtTransNo.Text = SessionTrans_No;
                btnSearch_Click(sender, e);
            }

        }
        Load_OLD_data();
    }


    protected void btnSearch_Click(object sender, EventArgs e)
    {
        //Search Gate_Pass_Out
        string query = "";
        DataTable Main_DT = new DataTable();
        query = "Select * from YarnDelivery_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Trans_No='" + txtTransNo.Text + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        if (Main_DT.Rows.Count != 0)
        {
            RdpIssueType.SelectedValue = Main_DT.Rows[0]["Trans_Type"].ToString();
            txtTransNo.Text = Main_DT.Rows[0]["Trans_No"].ToString();
            txtDate.Text = Main_DT.Rows[0]["Trans_Date"].ToString();
            txtUnit.SelectedValue = Main_DT.Rows[0]["ToUnit"].ToString();
            txtFromGoDown.SelectedValue = Main_DT.Rows[0]["FromGoDownID"].ToString();
            txtToGoDown.SelectedValue = Main_DT.Rows[0]["ToGoDownID"].ToString();
            txtRemarks.Text = Main_DT.Rows[0]["Remarks"].ToString();

            RdpIssueType_SelectedIndexChanged(sender, e);

            //Unplanned_Receipt_Main_Sub Table Load
            DataTable dt = new DataTable();
            query = "Select *from YarnDelivery_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Trans_No='" + txtTransNo.Text + "'";
            dt = objdata.RptEmployeeMultipleDetails(query);
            ViewState["ItemTable"] = dt;
            Repeater1.DataSource = dt;
            Repeater1.DataBind();

            Totalsum();

            btnSave.Text = "Update";
        }
        else
        {
            Clear_All_Field();
        }
    }

    private void Load_LotNo()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlLotNo.Items.Clear();
        query = "Select LotNo from Yarn_Stock_Ledger_All where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        query = query + " And GoDownID='" + txtFromGoDown.SelectedValue + "'";
        query = query + " And CountID='" + ddlCountName.SelectedValue + "'";
        query = query + " group by LotNo having ((Sum(Add_Bag)-Sum(Minus_Bag)) >0 )";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlLotNo.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["LotNo"] = "-Select-";
        dr["LotNo"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlLotNo.DataTextField = "LotNo";
        ddlLotNo.DataValueField = "LotNo";
        ddlLotNo.DataBind();
    }
    private void Load_Variety()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlCountName.Items.Clear();
        query = "Select CountID,CountName from Yarn_Stock_Ledger_All where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        query = query + " And GoDownID='" + txtFromGoDown.SelectedValue + "'";
        query = query + "  group by CountID,CountName having ((Sum(Add_Bag)-Sum(Minus_Bag)) >0 )";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlCountName.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["CountName"] = "-Select-";
        dr["CountID"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlCountName.DataTextField = "CountName";
        ddlCountName.DataValueField = "CountID";
        ddlCountName.DataBind();
    }
    private void Load_OLD_data()
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        Repeater1.DataSource = dt;
        Repeater1.DataBind();
    }

    private void Initial_Data_Referesh()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add(new DataColumn("LotNo", typeof(string)));
        dt.Columns.Add(new DataColumn("CountID", typeof(string)));
        dt.Columns.Add(new DataColumn("CountName", typeof(string)));
        dt.Columns.Add(new DataColumn("Qty", typeof(string)));
        dt.Columns.Add(new DataColumn("NoOfBag", typeof(string)));
        dt.Columns.Add(new DataColumn("AvlBag", typeof(string)));
        dt.Columns.Add(new DataColumn("Rate", typeof(string)));
        dt.Columns.Add(new DataColumn("LineTotal", typeof(string)));

        Repeater1.DataSource = dt;
        Repeater1.DataBind();
        ViewState["ItemTable"] = Repeater1.DataSource;

        //dt = Repeater1.DataSource;
    }
    private void Load_Unit()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        txtUnit.Items.Clear();
        query = "Select *from MstLocation where Ccode='" + SessionCcode + "' And Lcode!='" + SessionLcode + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        txtUnit.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["Lcode"] = "-Select-";
        dr["Lcode"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        txtUnit.DataTextField = "Lcode";
        txtUnit.DataValueField = "Lcode";
        txtUnit.DataBind();
    }
    protected void RdpIssueType_SelectedIndexChanged(object sender, EventArgs e)
    {
         if (RdpIssueType.SelectedValue == "2")
        {
            txtToGoDown.Enabled = true;
            txtUnit.Enabled = true;
        }
        else if (RdpIssueType.SelectedValue == "1")
        {
            txtToGoDown.Enabled = true;
            txtUnit.Enabled = false;
        }
        Load_ToGoDown();
    }
    private void Load_FromGoDown()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        txtFromGoDown.Items.Clear();
        query = "Select *from MstGoDown where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        txtFromGoDown.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["GoDownName"] = "-Select-";
        dr["GoDownID"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        txtFromGoDown.DataTextField = "GoDownName";
        txtFromGoDown.DataValueField = "GoDownID";
        txtFromGoDown.DataBind();
    }

    private void Load_ToGoDown()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        txtToGoDown.Items.Clear();
        query = "Select *from MstGoDown where Ccode='" + SessionCcode + "' ";
        if (RdpIssueType.SelectedValue == "2")
        {
            query = query + "And Lcode='" + txtUnit.SelectedValue + "'";
        }
        else
        {
            query = query + "And Lcode='" + SessionLcode + "'";
        }
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        txtToGoDown.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["GoDownName"] = "-Select-";
        dr["GoDownID"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        txtToGoDown.DataTextField = "GoDownName";
        txtToGoDown.DataValueField = "GoDownID";
        txtToGoDown.DataBind();
    }
    protected void txtFromGoDown_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_Variety();
    }
    protected void GridDeleteClick(object sender, CommandEventArgs e)
    {
        DataTable qry_dt = new DataTable();
        bool ErrFlag = false;
        string qry = "";


        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            if ((dt.Rows[i]["LotNo"].ToString() == e.CommandName.ToString()) && (dt.Rows[i]["CountID"].ToString() == e.CommandArgument.ToString()))
            {
                dt.Rows.RemoveAt(i);
                dt.AcceptChanges();
            }
        }
        ViewState["ItemTable"] = dt;
        Load_OLD_data();
        Totalsum();

    }
    protected void ddlCountName_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_LotNo();
    }
    protected void ddlLotNo_SelectedIndexChanged(object sender, EventArgs e)
    {
        string query = "";
        DataTable dtdsupp = new DataTable();

        query = "Select (Sum(Add_Bag)-Sum(Minus_Bag)) as Bag from Yarn_Stock_Ledger_All where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        query = query + " And GoDownID='" + txtFromGoDown.SelectedValue + "' And CountID='" + ddlCountName.SelectedValue + "'";
        query = query + " And LotNo='" + ddlLotNo.SelectedValue + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        if (dtdsupp.Rows.Count != 0)
        {
            txtAvlBag.Text = dtdsupp.Rows[0]["Bag"].ToString();
        }
        else
        {
            txtAvlBag.Text = "0";
        }
    }
    protected void txtRate_TextChanged(object sender, EventArgs e)
    {
        if (txtNoOfBag.Text != "")
        {
            txtAmt.Text = (Convert.ToDecimal(txtNoOfBag.Text) * Convert.ToDecimal(txtRate.Text)).ToString();
            txtAmt.Text = (Math.Round(Convert.ToDecimal(txtAmt.Text), 2, MidpointRounding.AwayFromZero)).ToString();
            
        }
    }
    protected void btnAddItem_Click(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        DataTable qry_dt = new DataTable();
        bool ErrFlag = false;
        DataRow dr = null;
        string query = "";

        if (txtNoOfBag.Text == "0.0" || txtNoOfBag.Text == "0" || txtNoOfBag.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the No Of Bag...');", true);
        }
        if (txtRate.Text == "0.0" || txtRate.Text == "0" || txtRate.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the Rate...');", true);
        }
        if (RdpIssueType.SelectedValue == "1")
        {
            if (txtFromGoDown.SelectedValue == "-Select-")
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select the From Godown.');", true);
            }
        }



        if (Convert.ToDecimal(txtAvlBag.Text) < Convert.ToDecimal(txtNoOfBag.Text))
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check with Qty. It exceeds Stock..');", true);
        }

        if (!ErrFlag)
        {
            string Qty = "";

            DataTable DT_Qty = new DataTable();
            query = "Select KgConv from MstKgConvEBRate where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
            DT_Qty = objdata.RptEmployeeMultipleDetails(query);

            if (DT_Qty.Rows.Count != 0)
            {
                Qty = ((Convert.ToDecimal(txtNoOfBag.Text)) * Convert.ToDecimal(DT_Qty.Rows[0]["KgConv"].ToString())).ToString();
                Qty = (Math.Round(Convert.ToDecimal(Qty), 0, MidpointRounding.AwayFromZero)).ToString();

            }
            else
            {
                Qty = "0";
            }


            // check view state is not null  
            if (ViewState["ItemTable"] != null)
            {
             
                //get datatable from view state   
                dt = (DataTable)ViewState["ItemTable"];

                //check Item Already add or not
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if ((dt.Rows[i]["LotNo"].ToString().ToUpper() == ddlLotNo.SelectedValue.ToString().ToUpper()) && (dt.Rows[i]["CountID"].ToString().ToUpper() == ddlCountName.SelectedValue.ToString().ToUpper()))
                    {
                        ErrFlag = true;
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('This Count Already Added..');", true);
                    }
                }
                if (!ErrFlag)
                {
                    dr = dt.NewRow();
                    dr["LotNo"] = ddlLotNo.SelectedValue;
                    dr["CountID"] = ddlCountName.SelectedValue;
                    dr["CountName"] = ddlCountName.SelectedItem.Text;
                    dr["NoOfBag"] = txtNoOfBag.Text;
                    dr["AvlBag"] = txtAvlBag.Text;
                    dr["Rate"] = txtRate.Text;
                    dr["LineTotal"] = txtAmt.Text;
                    dr["Qty"] = Qty;
                    dt.Rows.Add(dr);
                    ViewState["ItemTable"] = dt;
                    Repeater1.DataSource = dt;
                    Repeater1.DataBind();

                    Totalsum();


                    ddlLotNo.SelectedValue = "-Select-"; ddlCountName.SelectedValue = "-Select-";

                    txtNoOfBag.Text = "0"; txtAvlBag.Text = "0";
                    txtRate.Text = "0"; txtAmt.Text = "0";


                }
            }
            else
            {
                dr = dt.NewRow();
                dr["LotNo"] = ddlLotNo.SelectedValue;
                dr["CountID"] = ddlCountName.SelectedValue;
                dr["CountName"] = ddlCountName.SelectedItem.Text;
                dr["NoOfBag"] = txtNoOfBag.Text;
                dr["AvlBag"] = txtAvlBag.Text;
                dr["Rate"] = txtRate.Text;
                dr["LineTotal"] = txtAmt.Text;
                dr["Qty"] = Qty;
                dt.Rows.Add(dr);
                ViewState["ItemTable"] = dt;
                Repeater1.DataSource = dt;
                Repeater1.DataBind();

                Totalsum();


                ddlLotNo.SelectedValue = "-Select-"; ddlCountName.SelectedValue = "-Select-";
                txtNoOfBag.Text = "0"; txtAvlBag.Text = "0";
                txtRate.Text = "0"; txtAmt.Text = "0";
            }
        }
    }
    public void Totalsum()
    {
        string sum = "0";
        string bag = "0";
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        if (dt.Rows.Count != 0)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                bag = (Convert.ToDecimal(bag) + Convert.ToDecimal(dt.Rows[i]["NoOfBag"])).ToString();
                txtTotBag.Text = bag;
                sum = (Convert.ToDecimal(sum) + Convert.ToDecimal(dt.Rows[i]["LineTotal"])).ToString();
                txtTotWt.Text = sum;

            }
        }
        else
        {
            txtTotBag.Text = "0";
            txtTotWt.Text = "0";
        }
     }
    protected void btnSave_Click(object sender, EventArgs e)
    {

        string query = "";
        DataTable DT_Check = new DataTable();
        DataTable qry_dt = new DataTable();
        string SaveMode = "Insert";
        bool ErrFlag = false;

        //check with Item Details Add with Grid
        DT_Check = (DataTable)ViewState["ItemTable"];
        if (DT_Check.Rows.Count == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You have to add atleast one Item Details..');", true);
        }
        if (RdpIssueType.SelectedValue == "2")
        {
            if (txtUnit.SelectedValue == "-Select-")
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select the Unit.');", true);
            }
        }

        //User Rights Check Start
        bool Rights_Check = false;
        //if (btnSave.Text == "Update")
        //{
        //    Rights_Check = CommonClass_Function.ModifyRights_Check(SessionCcode, SessionLcode, SessionUserID, "7", "2", "Spares Receipt");
        //    if (Rights_Check == false)
        //    {
        //        ErrFlag = true;
        //        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Modify Spares Receipt Details..');", true);
        //    }
        //}
        //else
        //{
        //    Rights_Check = CommonClass_Function.AddRights_Check(SessionCcode, SessionLcode, SessionUserID, "7", "2", "Spares Receipt");
        //    if (Rights_Check == false)
        //    {
        //        ErrFlag = true;
        //        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Adding New Spares Receipt..');", true);
        //    }
        //}
        //User Rights Check End


        //Auto generate Transaction Function Call
        if (btnSave.Text != "Update")
        {
            if (!ErrFlag)
            {
                TransactionNoGenerate TransNO = new TransactionNoGenerate();
                string Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup(SessionCcode, SessionLcode, "Yarn Delivery", SessionFinYearVal);
                if (Auto_Transaction_No == "")
                {
                    ErrFlag = true;
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Auto Generate Transaction No Error... Contact Server Admin');", true);
                }
                else
                {
                    txtTransNo.Text = Auto_Transaction_No;
                }
            }
        }




        //Check With Already Approved Start
        DataTable DT_Check_Apprv = new DataTable();
        query = "Select * from YarnDelivery_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Trans_No='" + txtTransNo.Text + "' And Status='1'";
        DT_Check_Apprv = objdata.RptEmployeeMultipleDetails(query);
        if (DT_Check_Apprv.Rows.Count != 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Do not Yarn Delivery Details Already Approved..');", true);
        }




        if (!ErrFlag)
        {
            query = "Select * from YarnDelivery_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Trans_No='" + txtTransNo.Text + "'";
            DT_Check = objdata.RptEmployeeMultipleDetails(query);
            if (DT_Check.Rows.Count != 0)
            {
                SaveMode = "Update";
                query = "Delete from YarnDelivery_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Trans_No='" + txtTransNo.Text + "'";
                objdata.RptEmployeeMultipleDetails(query);
                query = "Delete from YarnDelivery_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Trans_No='" + txtTransNo.Text + "'";
                objdata.RptEmployeeMultipleDetails(query);
            }

            //Response.Write(strValue);
            //Insert Main Table
            query = "Insert Into YarnDelivery_Main(Ccode,Lcode,FinYearCode,FinYearVal,Trans_No,Trans_Date,Trans_Type,ToUnit,FromGoDownID,FromGoDownName,ToGoDownID,ToGoDownName,";
            query = query + "Remarks,TotNoOfBag,TotalAmount,UserID,UserName) Values('" + SessionCcode + "','" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + txtTransNo.Text + "','" + txtDate.Text + "',";
            query = query + " '" + RdpIssueType.SelectedValue + "','" + txtUnit.SelectedValue + "','" + txtFromGoDown.SelectedValue + "','" + txtFromGoDown.SelectedItem.Text + "','" + txtToGoDown.SelectedValue + "','" + txtToGoDown.SelectedItem.Text + "',";
            query = query + "'" + txtRemarks.Text + "','" + txtTotBag.Text + "','" + txtTotWt.Text + "','" + SessionUserID + "','" + SessionUserName + "')";
            objdata.RptEmployeeMultipleDetails(query);

            DataTable dt = new DataTable();
            dt = (DataTable)ViewState["ItemTable"];
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                query = "Insert Into YarnDelivery_Main_Sub(Ccode,Lcode,FinYearCode,FinYearVal,Trans_No,Trans_Date,LotNo,CountID,CountName,NoOfBag,AvlBag,";
                query = query + "Rate,LineTotal,Qty,UserID,UserName) Values('" + SessionCcode + "',";
                query = query + " '" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + txtTransNo.Text + "','" + txtDate.Text + "','" + dt.Rows[i]["LotNo"].ToString() + "','" + dt.Rows[i]["CountID"].ToString() + "',";
                query = query + " '" + dt.Rows[i]["CountName"].ToString() + "','" + dt.Rows[i]["NoOfBag"].ToString() + "','" + dt.Rows[i]["AvlBag"].ToString() + "','" + dt.Rows[i]["Rate"].ToString() + "',";
                query = query + "'" + dt.Rows[i]["LineTotal"].ToString() + "','" + dt.Rows[i]["Qty"].ToString() + "','" + SessionUserID + "','" + SessionUserName + "')";
                objdata.RptEmployeeMultipleDetails(query);
            }



            if (SaveMode == "Insert")
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Yarn Delivery Details Saved Successfully');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Yarn Delivery Details Updated Successfully');", true);
            }
            //Clear_All_Field();
            Session["Trans_No"] = txtTransNo.Text;
            btnSave.Text = "Update";
            //Load_Data_Enquiry_Grid();
        }
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }
    protected void btnBackEnquiry_Click(object sender, EventArgs e)
    {
        Session.Remove("Trans_No");
        Response.Redirect("YarnDelivery_Main.aspx");
    }
    protected void btnApprove_Click(object sender, EventArgs e)
    {
        string query = "";
        DataTable DT_Check = new DataTable();
        bool ErrFlag = false;

        //User Rights Check Start
        bool Rights_Check = false;
        //Rights_Check = CommonClass_Function.ApproveRights_Check(SessionCcode, SessionLcode, SessionUserID, "7", "2", "Unplanned Receipt");
        //if (Rights_Check == false)
        //{
        //    ErrFlag = true;
        //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Approve Unplanned Receipt...');", true);
        //}
        //User Rights Check End

        query = "Select * from YarnDelivery_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Trans_No='" + txtTransNo.Text + "'";
        DT_Check = objdata.RptEmployeeMultipleDetails(query);
        if (DT_Check.Rows.Count == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('First You have to Register this Yarn Delivery Details..');", true);
        }

        query = "Select * from YarnDelivery_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Trans_No='" + txtTransNo.Text + "' And Status='1'";
        DT_Check = objdata.RptEmployeeMultipleDetails(query);
        if (DT_Check.Rows.Count != 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Yarn Delivery Details Already Approved..');", true);
        }

        if (!ErrFlag)
        {
            query = "Update YarnDelivery_Main set Status='1' where Ccode='" + SessionCcode + "'";
            query = query + "  And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
            query = query + "  And Trans_No='" + txtTransNo.Text + "'";
            objdata.RptEmployeeMultipleDetails(query);

            Stock_Add();

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Yarn Delivery Details Approved Successfully..');", true);
        }
    }

    private void Clear_All_Field()
    {
        RdpIssueType.SelectedValue = "1";
        txtTransNo.Text = ""; txtDate.Text = ""; txtUnit.SelectedValue = "-Select-";
        txtFromGoDown.SelectedValue = "-Select-"; txtToGoDown.SelectedValue = "-Select-";

        txtTotWt.Text = "0"; txtTotBag.Text = "0";

        ddlLotNo.SelectedValue = "-Select-"; ddlCountName.SelectedValue = "-Select-";
        txtNoOfBag.Text = "0"; txtAvlBag.Text = "0";
        txtRate.Text = "0"; txtAmt.Text = "0";

        btnSave.Text = "Save";
        Initial_Data_Referesh();
        Session.Remove("Trans_No");
        //Load_Data_Enquiry_Grid();
    }
    private void Stock_Add()
    {
        string query = "";
        DataTable dt_check = new DataTable();
        DataTable qry_dt = new DataTable();


        DateTime transDate = Convert.ToDateTime(txtDate.Text);

        //Insert Stock_Transaction_Ledger
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            

            query = "select * from Yarn_Stock_Ledger_All where Trans_No='" + txtTransNo.Text + "' and CountID='" + dt.Rows[i]["CountID"].ToString() + "' and LotNo='" + dt.Rows[i]["LotNo"].ToString() + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
            dt_check = objdata.RptEmployeeMultipleDetails(query);
            if (dt_check.Rows.Count > 0)
            {
                query = "delete from Yarn_Stock_Ledger_All where Trans_No='" + txtTransNo.Text + "' and CountID='" + dt.Rows[i]["CountID"].ToString() + "' and LotNo='" + dt.Rows[i]["LotNo"].ToString() + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                objdata.RptEmployeeMultipleDetails(query);
            }

            
           
            if (RdpIssueType.SelectedValue == "1")
            {
                //Insert Into Stock_Ledger_All
                query = "insert into Yarn_Stock_Ledger_All(Ccode,Lcode,FinYearCode,FinYearVal,Trans_No,Trans_Date,Trans_Date_Str,Trans_Type,LotNo,CountID,";
                query = query + "CountName,Add_Qty,Add_Bag,Add_LossKg,Minus_Qty,Minus_Bag,Minus_LossKg,GoDownID,GoDownName,UserID,UserName) Values('" + SessionCcode + "',";
                query = query + " '" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + txtTransNo.Text + "','" + transDate.ToString("yyyy/MM/dd") + "','" + txtDate.Text + "','YARN DELIVERY',";
                query = query + " '" + dt.Rows[i]["LotNo"].ToString() + "','" + dt.Rows[i]["CountID"].ToString() + "','" + dt.Rows[i]["CountName"].ToString() + "','0','0','0',";
                query = query + "'" + dt.Rows[i]["Qty"].ToString() + "','" + dt.Rows[i]["NoofBag"].ToString() + "','0',";
                query = query + "'" + txtFromGoDown.SelectedValue + "','" + txtFromGoDown.SelectedItem.Text + "','" + SessionUserID + "','" + SessionUserName + "')";
                objdata.RptEmployeeMultipleDetails(query);

                //Insert Into Stock_Ledger_All
                query = "insert into Yarn_Stock_Ledger_All(Ccode,Lcode,FinYearCode,FinYearVal,Trans_No,Trans_Date,Trans_Date_Str,Trans_Type,LotNo,CountID,";
                query = query + "CountName,Add_Qty,Add_Bag,Add_LossKg,Minus_Qty,Minus_Bag,Minus_LossKg,GoDownID,GoDownName,UserID,UserName) Values('" + SessionCcode + "',";
                query = query + " '" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + txtTransNo.Text + "','" + transDate.ToString("yyyy/MM/dd") + "','" + txtDate.Text + "','YARN DELIVERY',";
                query = query + " '" + dt.Rows[i]["LotNo"].ToString() + "','" + dt.Rows[i]["CountID"].ToString() + "','" + dt.Rows[i]["CountName"].ToString() + "',";
                query = query + "'" + dt.Rows[i]["Qty"].ToString() + "','" + dt.Rows[i]["NoofBag"].ToString() + "','0','0','0','0',";
                query = query + "'" + txtToGoDown.SelectedValue + "','" + txtToGoDown.SelectedItem.Text + "','" + SessionUserID + "','" + SessionUserName + "')";
                objdata.RptEmployeeMultipleDetails(query);

            }
            else if (RdpIssueType.SelectedValue == "2")
            {
                //Insert Into Stock_Ledger_All
                query = "insert into Yarn_Stock_Ledger_All(Ccode,Lcode,FinYearCode,FinYearVal,Trans_No,Trans_Date,Trans_Date_Str,Trans_Type,LotNo,CountID,";
                query = query + "CountName,Add_Qty,Add_Bag,Add_LossKg,Minus_Qty,Minus_Bag,Minus_LossKg,GoDownID,GoDownName,UserID,UserName) Values('" + SessionCcode + "',";
                query = query + " '" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + txtTransNo.Text + "','" + transDate.ToString("yyyy/MM/dd") + "','" + txtDate.Text + "','YARN DELIVERY',";
                query = query + " '" + dt.Rows[i]["LotNo"].ToString() + "','" + dt.Rows[i]["CountID"].ToString() + "','" + dt.Rows[i]["CountName"].ToString() + "','0','0','0',";
                query = query + "'" + dt.Rows[i]["Qty"].ToString() + "','" + dt.Rows[i]["NoofBag"].ToString() + "','0',";
                query = query + "'" + txtFromGoDown.SelectedValue + "','" + txtFromGoDown.SelectedItem.Text + "','" + SessionUserID + "','" + SessionUserName + "')";
                objdata.RptEmployeeMultipleDetails(query);

                //Insert Into Stock_Ledger_All
                query = "insert into Yarn_Stock_Ledger_All(Ccode,Lcode,FinYearCode,FinYearVal,Trans_No,Trans_Date,Trans_Date_Str,Trans_Type,LotNo,CountID,";
                query = query + "CountName,Add_Qty,Add_Bag,Add_LossKg,Minus_Qty,Minus_Bag,Minus_LossKg,GoDownID,GoDownName,UserID,UserName) Values('" + SessionCcode + "',";
                query = query + " '" + txtUnit.SelectedValue + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + txtTransNo.Text + "','" + transDate.ToString("yyyy/MM/dd") + "','" + txtDate.Text + "','YARN DELIVERY',";
                query = query + " '" + dt.Rows[i]["LotNo"].ToString() + "','" + dt.Rows[i]["CountID"].ToString() + "','" + dt.Rows[i]["CountName"].ToString() + "',";
                query = query + "'" + dt.Rows[i]["Qty"].ToString() + "','" + dt.Rows[i]["NoofBag"].ToString() + "','0','0','0','0',";
                query = query + "'" + txtToGoDown.SelectedValue + "','" + txtToGoDown.SelectedItem.Text + "','" + SessionUserID + "','" + SessionUserName + "')";
                objdata.RptEmployeeMultipleDetails(query);

            }
        }

    }
}
