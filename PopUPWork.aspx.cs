﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using Altius.BusinessAccessLayer.BALDataAccess;

public partial class PopUPWork : System.Web.UI.Page
{

    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionFinYearCode;
    string SessionFinYearVal;
    string SessionStdPOOrderNo;


    protected void Page_Load(object sender, EventArgs e)
    {

        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();


        Load_Data_Empty_Grid();
    }
    protected void btnDept_Click(object sender, EventArgs e)
    {
        modalPop.Show();
    }
    protected void GridViewClick(object sender, CommandEventArgs e)
    {
        txtItemCode.Text = Convert.ToString(e.CommandArgument);
        txtItemName.Text = Convert.ToString(e.CommandName);
    }


    private void Load_Data_Empty_Grid()
    {

        string query = "";
        DataTable DT = new DataTable();
        DataTable DT1 = new DataTable();
        DataTable DT2 = new DataTable();


        DT.Columns.Add("ItemCode");
        DT.Columns.Add("ItemName");




        query = "Select ItemCode,ItemShortName as ItemName from MstItemMaster where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        Repeater_StdPurOrder.DataSource = DT;
        Repeater_StdPurOrder.DataBind();
        Repeater_StdPurOrder.Visible = true;

    }

}
