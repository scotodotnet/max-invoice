﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

/// <summary>
/// Summary description for AdvanceAmount
/// </summary>
public class AdvanceAmount
{
    private string _EmpNo;
    private string _Exist;
    //private string _Transdate;
    private string _Months;
    private string _Amount;
    private string _Monthlydeduction;
    private string _ccode;
    private string _Lcode;

	public AdvanceAmount()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public string EmpNo
    {
        get { return _EmpNo; }
        set { _EmpNo = value; }
    }
    public string Exist
    {
        get { return _Exist; }
        set { _Exist = value; }
    }
    public string Months
    {
        get { return _Months; }
        set { _Months = value; }
    }
    public string Amount
    {
        get { return _Amount; }
        set { _Amount = value; }
    }
    public string MontlyDeduction
    {
        get { return _Monthlydeduction; }
        set { _Monthlydeduction = value; }
    }
    public string Ccode
    {
        get { return _ccode; }
        set { _ccode = value; }
    }
    public string Lcode
    {
        get { return _Lcode; }
        set { _Lcode = value; }
    }
}
