﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.DataAccessLayer.DALDataAccess;


/// <summary>
/// Summary description for BALDataAccess
/// </summary>
namespace Altius.BusinessAccessLayer.BALDataAccess
{
    public class BALDataAccess : BaseBusiness
    {
        DALDataAccess objdata = new DALDataAccess();

       //// public void InsertDataInto_PublishProd(AdminClass objAdmin, string UID)
       // {
       //     objdata.InsertDataInto_PublishProd(objAdmin, UID);
       // }

        

        public void mst(sample objs)
        {
             objdata.mst(objs);
        }
       
        public void MstCompany(MastersClass objDis)
        {
            objdata.MstCompany(objDis);
        }
        public string CompanyMaxValue()
        {
            return objdata.CompanyMaxValue();
        }
        public DataTable MstCompanyDisplay()
        {
            return objdata.MstCompanyDisplay();
        }
        public DataTable MstDropDownDepartmentDisplay()
        {
            return objdata.MstDropDownDepartmentDisplay();
        }
        public DataTable DDdEPARTMENT_LOAD()
        {
            return objdata.DDdEPARTMENT_LOAD();
        }
        public DataTable DropDownDepartment()
        {
            return objdata.DropDownDepartment();
        }
        public DataTable DropDownDepartment_category(string category, string Ccode, string Lcode)
        {
            return objdata.DropDownDepartment_category(category, Ccode, Lcode);
        }

        public DataTable salaryhistory_DropDowndept_category(string category, string Ccode, string Lcode, string Activemode)
        {
            return objdata.salaryhistory_DropDowndept_category(category, Ccode, Lcode,Activemode);
        }

        public DataTable DropDownFood()
        {
            return objdata.DropDownFood();
        }

        public DataTable MstDepartmentGridDisplay()
        {
            return objdata.MstDepartmentGridDisplay();
        }
        public DataTable MstInsuranceDisplay()
        {
            return objdata.MstInsuranceDisplay();
        }
        public DataTable EmployeeNoandName(string staff, string Dept, string Ccode, string Lcode)
        {
            return objdata.EmployeeNoandName(staff, Dept, Ccode, Lcode);
        }
        public DataTable EmployeeNoandName_user(string staff, string Dept, string Ccode, string Lcode)
        {
            return objdata.EmployeeNoandName_user(staff, Dept, Ccode, Lcode);
        }
        public DataTable DropDownEmployeeName(string EmpNo, string Ccode, string Lcode)
        {
            return objdata.DropDownEmployeeName(EmpNo, Ccode, Lcode);
        }
        public DataTable DropDownEmployeeNumber(string EmpName, string Ccode, string Lcode)
        {
            return objdata.DropDownEmployeeNumber(EmpName, Ccode, Lcode);
        }
        public DataTable MstProbationDisplay()
        {
            return objdata.MstProbationDisplay();
        }
        public void MstDistrict(MastersClass  objDis)
        {
             objdata.MstDistrict(objDis);
        }
        public void MstInsurance(MastersClass objIns)
        {
            objdata.MstInsurance(objIns);
        }
        public void MstProbation(MastersClass objPro)
        {
            objdata.MstProbation(objPro);
        }
        public void MstDepartment(MastersClass objDip)
        {
            objdata.MstDepartment(objDip);
        }
        public void MstTaluk(MastersClass objTaluk)
        {
            objdata.MstTaluk(objTaluk);
        }
        public void allocation(MastersClass objleave)
        {
            objdata.allocation(objleave);
        }
        public DataTable MstDistrictDisplay()
        {
            return objdata.MstDistrictDisplay();
        }
        
        public DataTable MstTalukDisplay()
        {
            return objdata.MstTalukDisplay();
        }
       
        public void UpdateTaluk(MastersClass objTaluk)
        {
            objdata.UpdateTaluk(objTaluk);
        }

        public void UpdateMstdistrict(MastersClass objDis)
        {
            objdata.UpdateDistrict(objDis);
        }
        public void UpdateMstCompany(MastersClass objCom)
        {
            objdata.UpdateMstCompany(objCom);
        }
        public void UpdateMstInsurance(MastersClass objIns)
        {
            objdata.UpdateMstInsurance(objIns);
        }
        public void UpdateMstProbation(MastersClass objProb)
        {
            objdata.UpdateMstProbation(objProb);
        }
        public void UpdateMstDepartment(MastersClass objCom)
        {
            objdata.UpdateMstDepartment(objCom);
        }
        public void MstQualification(MastersClass objMas)
        {
            objdata.MstQualification(objMas);
        }
        public DataTable MstQualificationDisplay()
        {
            return objdata.MstQualificationDisplay();
        }
        public void UpdateQualification(MastersClass objMas)
        {
            objdata.UpdateQualification(objMas);
        }
        public void MstLeave(MastersClass objMas)
        {
            objdata.MstLeave(objMas);
        }
        public DataTable MstLeaveDisplay()
        {
            return objdata.MstLeaveDisplay();
        }
        public DataTable EmployeeDetailsDisplay()
        {
            return objdata.EmployeeDetailsDisplay();
        }
        public DataTable MstLeavePayMasterDisplay()
        {
            return objdata.MstLeavePayMasterDisplay();
        }
        public void UpdateLeave(MastersClass objMas)
        {
            objdata.UpdateLeave(objMas);
        }
        public void UpdateLeaveDaysMaster(string Leaveid, string Days)
        {
            objdata.UpdateLeaveDaysMaster(Leaveid, Days);
        }
        public void MstEmployeeType(MastersClass objMas)
        {
            objdata.MstEmployeeType(objMas);
        }
        public DataTable MstEmployeeTypeDisplay()
        {
            return objdata.MstEmployeeTypeDisplay();
        }
        public void UpdateEmployeeType(MastersClass objMas)
        {
            objdata.UpdateEmployeeType(objMas);
        }
        public DataTable DropDownDistrict()
        {
           return  objdata.DropDownDistrict();
        }
        public DataTable DropDownCategory()
        {
            return objdata.DropDownCategory();
        }
        public DataTable DropDownDistrictCheckBox(string Districtcd)
        {
            return objdata.DropDownDistrictCheckBox(Districtcd);
        }

        public DataTable DropDownTalukCheckBox(string Districtcd, string TalukCd)
        {
            return objdata.DropDownTalukCheckBox(Districtcd,TalukCd);
        }

        public DataTable DropDownPSTaluk(string Districtcd)
        {
            return objdata.DropDownPSTaluk(Districtcd);
        }

        public DataTable DropDownPMTaluk(string Districtcd)
        {
            return objdata.DropdownPMTaluk(Districtcd);
        }
        public DataTable EmployeeDropDown(string category)
        {
            return objdata.EmployeeDropDown(category);
        }
        public DataTable EmployeeDropDown_no(string type)
        {
            return objdata.EmployeeDropDown_no(type);
        }
        public DataTable LabourDropDown()
        {
            return objdata.LabourDropDown();
        }
        public DataTable Qualification()
        {
            return objdata.Qualification();
        }

        public string MaxEmpNo()
        {
            return objdata.MaxEmpNo();
        }

        public void InsertEmployeeDetails(EmployeeClass objEmp, int Regno, string dob, string stafflabor, string NonPFGrade,string RoleCode)
        {
            objdata.InsertEmployeeDetails(objEmp, Regno,dob,stafflabor,NonPFGrade,RoleCode);
        }
        public DataTable FillEmployeeRegistration(string EmpNo, string stafflabour, string Ccode, string Lcode)
        {
            return objdata.FillEmployeeRegistration(EmpNo,stafflabour, Ccode,Lcode);
        }
        public DataTable FillEmployeeRegistration_Existno(string existNo, string stafflabour, string Ccode, string Lcode)
        {
            return objdata.FillEmployeeRegistration_Existno(existNo, stafflabour, Ccode, Lcode);
        }
        public void UpdateEmployeeRegistration(EmployeeClass objemp, string EmpNo, string stafflabor, string DOB, string NonPFGrade)
        {

            objdata.UpdateEmployeeRegistration(objemp, EmpNo, stafflabor, DOB, NonPFGrade);
        }
        public DataTable DropDownPSTaluk_ForEdit(int Districtcd, int TalukCd)
        {
            return objdata.DropDownPSTaluk_ForEdit(Districtcd, TalukCd);
        }
        public string EmployeeVerify(string EmpNo)
        {
            return objdata.EmployeeVerify(EmpNo);
        }
        public string EmployeeName(string EmpCode)
        {
            return objdata.EmployeeName(EmpCode);
        }
        public string ExisistingNo(string EmpNo)
        {
            return objdata.ExisistingNo(EmpNo);
        }
        public DataTable EmployeeDeactivate_loadgrid(string EmpNo, string Ccode, string Lcode)
        {
            return objdata.EmployeeDeactivate_loadgrid(EmpNo, Ccode, Lcode);
        }
        public DataTable EmployeeDeactivate_loadgrid_exist(string EmpNo, string Ccode, string Lcode)
        {
            return objdata.EmployeeDeactivate_loadgrid_exist(EmpNo, Ccode, Lcode);
        }
        public void UpdateEmployeeActivateDeactivate(string EmpNo, string Isactive, string Ccode, string Lcode)
        {
            objdata.UpdateEmployeeActivateDeactivate(EmpNo, Isactive, Ccode, Lcode);
        }

        public void UpdateEmployeeDeactivate(string EmpNo, DateTime deactivedate, string Isactive, string Ccode, string Lcode)
        {
            objdata.UpdateEmployeeDeactivate(EmpNo, deactivedate, Isactive, Ccode, Lcode);
        }

        public string isAdmin(string Username)
        {
            return objdata.IsAdmin(Username);
        }
        public string username(UserRegistrationClass objuser)
        {
            return objdata.username(objuser);
        }
        public string usernameDisplay(UserRegistrationClass objuser)
        {
            return objdata.usernameDisplay(objuser);
        }
        public DataTable EmployeeVerifyforLeaveAllocation(string EmpNo, string dept, string stafforlabour, string Ccode, string Lcode)
        {
            return objdata.EmployeeVerifyforLeaveAllocation(EmpNo, dept, stafforlabour, Ccode, Lcode);
        }
        public DataTable EmployeeVerifyforLeaveAllocationForExistingNo(string ExistingNo, string dept, string stafforlabour, string Ccode, string Lcode)
        {
            return objdata.EmployeeVerifyforLeaveAllocationForExistingNo(ExistingNo, dept, stafforlabour, Ccode, Lcode);
        }
        public DataTable EmployeeVerifyforLeaveAllocExistingNoandEmpNo(string ExistingNo, string EmpNo, string dept, string stafforlabour, string Ccode, string Lcode)
        {
            return objdata.EmployeeVerifyforLeaveAllocExistingNoandEmpNo(ExistingNo, EmpNo, dept, stafforlabour, Ccode, Lcode);
        }
        public DataTable EmployeeLeaveAllocForReterivew(string EmpNo, string Ccode, string Lcode)
        {
            return objdata.EmployeeLeaveAllocForReterivew(EmpNo, Ccode, Lcode);
        }
        public string UserPassword(UserRegistrationClass objuser)
        {
            return objdata.UserPassword(objuser);
        }
        public DataTable MstBankDropDown()
        {
            return objdata.MstBankDropDown();
        }
        public void SalaryDetailsForLabor(string EmpNo, string Workeddays, string Flatrate, DateTime Salarydate, Decimal Totamout, string Month, string Year, string Ccode, string Lcode)
       {
           objdata.SalaryDetailsForLabor(EmpNo, Workeddays, Flatrate, Salarydate, Totamout, Month, Year, Ccode, Lcode);
        }
        public void SalaryDetails(SalaryClass objSal, string EmpNo, string ExisistingNo, DateTime Mydate, string MyMonth, string Year, string FinaYear, DateTime TransDate, DateTime fromdt, DateTime Todt, string Cash_Bank, string WagesType)
        {
            objdata.SalaryDetails(objSal, EmpNo, ExisistingNo, Mydate, MyMonth, Year, FinaYear, TransDate, fromdt, Todt, Cash_Bank, WagesType);
        }
        public void LeaveDetails(LeaveClass objLeave, DateTime Mydate, string EmpNo,string month)
        {
            objdata.LeaveDetails(objLeave,Mydate,EmpNo,month);
        }
        public void LeaveDetailsUpdateRowGrid(LeaveClass objLeave, DateTime Mydate, string EmpNo, string month, string Year)
        {
            objdata.LeaveDetailsUpdateRowGrid(objLeave, Mydate, EmpNo, month,Year);
        }
        public DataTable DropDwonLeaveType()
        {
            return objdata.DropDwonLeaveType();
        }
        public DataTable LeaveDatailsDisplayFromGridView(string Month)
        {
            return objdata.LeaveDatailsDisplayFromGridView(Month);
        }
        public DataTable LeaveDatailsDisplayFromGridViewAllNewEntry()
        {
            return objdata.LeaveDatailsDisplayFromGridViewAllNewEntry();
        }
        public DataTable LeaveallocationfromGrid()
        {
            return objdata.LeaveallocationfromGrid();
        }
       
        public string Rolecd(string EmpNo)
        {
            return objdata.Rolecd(EmpNo);
        }
        public string EmpGrade(string EmpNo)
        {
            return objdata.EmpGrade(EmpNo);
        }
        public string SalaryMonth(string EmpNo, string FinaYear, string Ccode, string Lcode, string month, DateTime fromdt, DateTime todt)
        {
            return objdata.SalaryMonth(EmpNo, FinaYear, Ccode, Lcode, month, fromdt, todt);
        }
        public DataTable DropDownBank()
        {
            return objdata.DropDownBank();
        }
        public DataTable OfficialProfileGridViewForAdmin(string staff, string Dept, string Ccode, string Lcode)
        {
            return objdata.OfficialProfileGridViewForAdmin(staff, Dept, Ccode, Lcode);
        }
        public DataTable OfficialProfileGridView(string staff,string Dept, string Ccode,string Lcode)
        {
            return objdata.OfficialProfileGridView(staff, Dept, Ccode,Lcode);
        }
        public DataTable DropDownProbationPriod()
        {
            return objdata.DropDownProbationPriod();
        }
        public DataTable DropDownInsurance()
        {
            return objdata.DropDownInsuracne();
        }
        public void OfficalProfile(OfficialprofileClass objOff, string empno, DateTime DofJoin, DateTime DofConfirm, DateTime ExpiryDate, DateTime contractStartingDate, DateTime ContractEndingDate, string pf_type, DateTime PFDOJ, DateTime ESIDOJ)
        {
            objdata.OfficalProfile(objOff, empno, DofJoin, DofConfirm, ExpiryDate, contractStartingDate, ContractEndingDate, pf_type, PFDOJ, ESIDOJ);
        }


        public void UserRegistration(UserRegistrationClass objReg)
        {
            objdata.UserRegistration(objReg);
        }
        public void UserRegistration_update(UserRegistrationClass objreg)
        {
            objdata.UserRegistration_update(objreg);
        }
        public string EmployeeNoFromOfficialProfile(string empno)
        {
            return objdata.EmployeeNoFromOfficialProfile(empno);
        }

        public string SalaryMonthFromLeave(string empno, string FinaYear, string Ccode, string Lcode, string Mont, DateTime dfrom, DateTime dtto)
        {
            return objdata.SalaryMonthFromLeave(empno, FinaYear, Ccode, Lcode, Mont, dfrom, dtto);
        }
        public string SalaryPayDay()
        {
            return objdata.SalaryPayDay();
        }
        public string LeaveDaysFromSalary(string empNo, string FinaYear, string Month, string Ccode, string Lcode)
        {
            return objdata.LeaveDaysFromSalary(empNo, FinaYear, Month, Ccode, Lcode);
        }
        public string LeaveDaysFromSalary_ForLabour(string empNo, string FinaYear, string Month, string Ccode, string Lcode)
        {
            return objdata.LeaveDaysFromSalary_ForLabour(empNo, FinaYear, Month, Ccode, Lcode);
        }
        public DataTable TakenLeaveFromMonth(string EmpNo, string Month, string FinaYear)
        {
            return objdata.TakenLeaveFromMonth(EmpNo, Month, FinaYear);
        }
        public string SalaryType_ForLabour(string EmpNo, string FinaYear, string Ccode, string Lcode)
        {
            return objdata.SalaryType_ForLabour(EmpNo, FinaYear, Ccode, Lcode);
        }
        public string EligibleForESI(string EmpNo, string FinaYear, string Ccode, string Lcode)
        {
            return objdata.EligibleForESI(EmpNo, FinaYear, Ccode, Lcode);
        }
        public void Bank(MastersClass objMas)
        {
            objdata.Bank(objMas);

        }
        public DataTable BankDisplay()
        {
            return objdata.BankDisplay();
        }
        public void UpdateBank(MastersClass objMas)
        {
            objdata.UpdateBank(objMas);
        }

        public DataTable PFMonthlyStatement(string EmpNo, string Month, string Year)
        {
            return objdata.PFMonthlyStatement(EmpNo, Month, Year);
        }

        public void InsertPFFormonthlyStatement(PFMonthlySatementClass objPFM,string Month, string Year,DateTime PFDate)
        {
            objdata.InsertPFMonthlyStatement(objPFM,Month,Year, PFDate);
 
        }
        public void InsertPFFor3AStatement(PFForm3AClass objPF3A, string Month, string Year, DateTime PFDate)
        {
            objdata.InsertPFMonthlyStatement(objPF3A, Month, Year, PFDate);

        }
        public DataTable ESIMonthlyStatement(string EmpNo, string Month, string Year)
        {
            return objdata.ESIMonthlyStatement(EmpNo, Month, Year);
        }
        public void InsertESIMonthlyStatement(ESIMonthlyStatementClass objESI, string Month, string Year, DateTime ESIDate, DateTime ChallenDate)
        {
            objdata.InsertESIMonthlyStatement(objESI, Month, Year, ESIDate,ChallenDate);

        }
        public DataTable ESIHalfYarlSearchStatement(string EmpNo, string Month, string Year)
        {
            return objdata.ESIHalfYarlySearchStatement(EmpNo, Month, Year);
        }
        public void InsertESIForm6Statement(ESIHalfYarlyForm6Class objESI6, string Month, string Year, DateTime ESIDate)
        {
            objdata.InsertESIForm6AStatement(objESI6, Month, Year, ESIDate);

        }
        public DataTable AlreadyTakenLeave(string EmpNo, string FinaYear, string Ccode, string Lcode)
        {
            return objdata.AlreadyTakenLeave(EmpNo, FinaYear, Ccode, Lcode);
        }
        public DataTable AlreadyTakenLeaveForLabour(string EmpNo, string FinaYear, string Ccode, string Lcode)
        {
            return objdata.AlreadyTakenLeaveForLabour(EmpNo, FinaYear, Ccode, Lcode);
        }
        public DataTable LeaveAllocationEmployeeForEdit(string stafforLabour, string dept, string Ccode, string Lcode)
        {
            return objdata.LeaveAllocationEmployeeForEdit(stafforLabour, dept, Ccode, Lcode);
        }
        public DataTable LeaveAllocationEmployeeForEdit_user(string stafforLabour, string dept, string Ccode, string Lcode)
        {
            return objdata.LeaveAllocationEmployeeForEdit_user(stafforLabour, dept, Ccode, Lcode);
        }
        public DataTable LeaveAllocationEmployee(string stafforLabour, string dept, string Ccode, string Lcode)
        {
            return objdata.LeaveAllocationEmployee(stafforLabour, dept, Ccode, Lcode);
        }
        public DataTable LeaveAllocationLabour_user(string stafforLabour, string dept, string Ccode, string Lcode)
        {
            return objdata.LeaveAllocationLabour_user(stafforLabour, dept, Ccode, Lcode);
        }
        public DataTable LeaveAllocationLabour(string stafforLabour, string dept, string Ccode, string Lcode)
        {
            return objdata.LeaveAllocationLabour(stafforLabour, dept, Ccode, Lcode);
        }
        public DataTable LeaveAllocation_User(string stafforLabour, string dept, string Ccode, string Lcode)
        {
            return objdata.LeaveAllocation_User(stafforLabour, dept, Ccode, Lcode);
        }
        public string EmployeeLeaveallocationupdate(string EmpCode)
        {
            return objdata.EmployeeLeaveallocationupdate(EmpCode);
        }
        public void LeaveallocationUpdate(string empCode, string LeaveAllocation, string Designation)
        {
            objdata.LeaveallocationUpdate(empCode, LeaveAllocation, Designation);
        }
        public void InsertLeaveAllocation(LeaveAllocation objla)
        {
            objdata.InsertLeaveAllocation(objla);
        }
        public void UpdateLeaveAllocation(LeaveAllocation objla)
        {
            objdata.UpdateLeaveAllocation(objla);
        }
        #region ApplyForLeave
        public DataTable ApplyForLeaveinEmployee(string EmpNo, string FinaYear, string Ccode, string Lcode)
        {
            return objdata.ApplyForLeaveinEmployee(EmpNo, FinaYear, Ccode, Lcode);
        }
        public DataTable ApplyForLeaveinLabour_1(string EmpNo, string FinaYear, string Ccode, string Lcode)
        {
            return objdata.ApplyForLeaveinLabour_1(EmpNo, FinaYear, Ccode, Lcode);
        }
        public void ApplyForLeave(string EmpNo, string Leavetype, string ApplyLeave, string TotalWorkingdays, string Leavecode, DateTime FromDate, DateTime ToDate, string month, string Year, string AllocatedLeave, string AlreadyTakenLeave, string ExcessLEave, string Approvedby, DateTime ApprovedDate, string FinYear, string Maternity, string Ccode, string Lcode)
        {
            objdata.ApplyForLeave(EmpNo, Leavetype, ApplyLeave, TotalWorkingdays, Leavecode, FromDate, ToDate, month, Year, AllocatedLeave, AlreadyTakenLeave, ExcessLEave, Approvedby, ApprovedDate, FinYear, Maternity, Ccode, Lcode);
        }
        public void ApplyForLeaveinLabour(string EmpNo, string Leavetype, string ApplyLeave, string TotalWorkingdays, string Leavecode, DateTime FromDate, DateTime ToDate, string month, string Year, string Approvedby, DateTime ApprovedDate, string FinYear, string Ccode, string Lcode)
        {
            objdata.ApplyForLeaveinLabour(EmpNo, Leavetype, ApplyLeave, TotalWorkingdays, Leavecode, FromDate, ToDate, month, Year, Approvedby, ApprovedDate, FinYear, Ccode, Lcode);
        }
        public DataTable AllReadyApplyForLeaveDate(string EmpNo, DateTime FromDate, DateTime ToDate, string FinaYear, string Ccode, string Lcode)
        {
            return objdata.AllReadyApplyForLeaveDate(EmpNo, FromDate, ToDate, FinaYear, Ccode, Lcode);
        }
        public string LeaveCode()
        {
            return objdata.Leavecode();
        }
        public string Female(string empno, string Ccode, string Lcode)
        {
            return objdata.Female(empno, Ccode, Lcode);
        }
        public string AvailableMaternityLeave(string empno, string FinancialYr, string Ccode, string Lcode)
        {
            return objdata.AvailableMaternityLeave(empno, FinancialYr, Ccode, Lcode);
        }
        public DataTable FillApplyForGridView()
        {
            return objdata.FillApplyForGridView();
        }
        public DataTable FillApplyForGridViewForStatus(string Month)
        {
            return objdata.FillApplyForGridViewForStatus(Month);
        }
        public DataTable FillApplyForGridViewForLeaveCode(string Leavecd)
        {
            return objdata.FillApplyForGridViewForLeaveCode(Leavecd);
        }
        public DataTable FillMonth()
        {
            return objdata.FillMonth();
        }
        public void UpdateForApplyLeave(string empno,string Status,string levaecd,string noofleave)
        {
            objdata.UpdateForApplyLeave(empno, Status, levaecd, noofleave);
        }
        public string LeavecountMessage()
        {
            return objdata.LeavecountMessage();
        }
        #endregion

        public DataTable SalaryEmpIDLoad(string stafflabour, string department, string Ccode, string Lcode)
        {
            return objdata.SalaryEmpIDLoad(stafflabour, department, Ccode, Lcode);
        }
        public DataTable EmployeeNameandNumber_SalaryDetails(string stafflabour, string department, string Ccode, string Lcode, string Wages)
        {
            return objdata.EmployeeNameandNumber_SalaryDetails(stafflabour, department, Ccode, Lcode, Wages);
        }
        //public DataTable BonusEmployeeName(string empID)
        //{
        //    return objdata.BonusEmployeeName(empID);
        //}
        public DataTable BasicAmountForSalary(string EmpNo, string FinaYear)
        {
            return objdata.BasicAmountForSalary(EmpNo, FinaYear);
        }
        #region From Ragu
        public DataTable EditOfficialprofile_Load(string empID)
        {
            return objdata.EditOfficialprofile_Load(empID);
        }

        public void UpdateOfficalProfile(OfficialprofileClass objOff, string empno, DateTime DofJoin, DateTime DofConfirm, DateTime ExpiryDate, DateTime contractStartingDate, DateTime ContractEndingDate, string pf_type, DateTime PFDOJ, DateTime ESIDOJ)
        {
            objdata.UpdateOfficalProfile(objOff, empno, DofJoin, DofConfirm, ExpiryDate, contractStartingDate, ContractEndingDate, pf_type, PFDOJ, ESIDOJ);
        }
        public DataTable EditOfficialProfileGridView(string staff, string Dept, string Ccode, string Lcode)
        {
            return objdata.EditOfficialProfileGridView(staff, Dept, Ccode, Lcode);
        }
        public DataTable EditOfficialProfileGridView_TokenNoWise(string staff, string TokenNo, string Ccode, string Lcode)
        {
            return objdata.EditOfficialProfileGridView_TokenNoWise(staff, TokenNo, Ccode, Lcode);
        }
        public DataTable EditOfficialProfileGridView_user(string staff, string Dept, string Ccode, string Lcode)
        {
            return objdata.EditOfficialProfileGridView_user(staff, Dept, Ccode, Lcode);
        }
        public DataTable OfficalProfileempType(string empCategory)
        {
            return objdata.OfficalProfileempType(empCategory);
        }
        public DataTable Contractbreak_EmployeeLoad(string department, string cate, string Ccode, string Lcode)
        {
            return objdata.Contractbreak_EmployeeLoad(department, cate, Ccode, Lcode);
        }
        public DataTable Contractbreak_EmployeeLoad_user(string department, string cate, string Ccode, string Lcode)
        {
            return objdata.Contractbreak_EmployeeLoad_user(department, cate,Ccode,Lcode);
        }
        public DataTable Contractbreak_EmployeeSearch(string department, string empNo, string cate, string isadmin, string Ccode, string Lcode)
        {
            return objdata.Contractbreak_EmployeeSearch(department, empNo, cate, isadmin, Ccode, Lcode);
        }
        public string ContractExcessLeave(string empNo)
        {
            return objdata.ContractExcessLeave(empNo);
        }
        public string ServerDate()
        {
            return objdata.ServerDate();
        }
        public string ExistingNO_Verify(string ExistingNo, string Ccode, string Lcode)
        {
            return objdata.ExistingNO_Verify(ExistingNo, Ccode, Lcode);
        }
        public string OLDNO_Verify(string OLDNo, string Ccode, string Lcode)
        {
            return objdata.OLDNO_Verify(OLDNo, Ccode, Lcode);
        }

        //public void BonusforallInsert(BonusforAllclass objbonus)
        //{
        //    objdata.BonusforallInsert(objbonus);
        //}
      
        
        
        //public DataTable SalaryMasterEmpLoad(string EmpCode)
        //{
        //    return objdata.SalaryMasterEmpLoad(EmpCode);
        //}
        //public DataTable SalaryMasterProfile(string EmpCode)
        //{
        //    return objdata.SalaryMasterProfile(EmpCode);
        //}
        public DataTable StaffBonusLoad(string empNo, string Ccode, string Lcode)
        {
            return objdata.StaffBonusLoad(empNo, Ccode, Lcode);
        }
        public DataTable LabourBonus_Load(string empNo, string Ccode, string Lcode)
        {
            return objdata.LabourBonus_Load(empNo,Ccode,Lcode);
        }
        //public DataTable StandardBonusEmpLoad(string department, string staffLabour)
        //{
        //    return objdata.StandardBonusEmpLoad(department, staffLabour);
        //}
        //public DataTable StandardBonusLabour_Load(string department, string Wages)
        //{
        //    return objdata.StandardBonusLabour_Load(department, Wages);
        //}
      #endregion

        #region BonusCalculation
        public string TotalWorkingDays(string empno,DateTime FromDate, DateTime Todate)
        {
            return objdata.TotalWorkingDays(empno, FromDate, Todate);
        }
        public void InsertBonus(BonusInsertClass objBonusIns, DateTime fromdate, DateTime ToDate)
        {
            objdata.InsertBonus(objBonusIns, fromdate, ToDate);
        }
        #endregion

        #region Attendance Report
        public DataTable RptAttendanceSingleEmpForMonth(string EmpNo, DateTime FromDate, DateTime ToDate, string FinaYear, string Dept, string Ccode, string Lcode)
        {
            return objdata.RptAttendanceSingleEmpForMonth(EmpNo, FromDate, ToDate, FinaYear, Dept, Ccode, Lcode);
        }
        public DataTable RptAttendanceSingleEmpForMonth_user(string EmpNo, DateTime FromDate, DateTime ToDate, string FinaYear, string Dept, string Ccode, string Lcode)
        {
            return objdata.RptAttendanceSingleEmpForMonth_user(EmpNo, FromDate, ToDate, FinaYear, Dept, Ccode, Lcode);
        }
        public DataTable RptAttendanceSingleEmpForYear(string EmpNo, string FinaYear, string Dept, string Ccode, string Lcode)
        {
            return objdata.RptAttendanceSingleEmpForYear(EmpNo, FinaYear, Dept, Ccode, Lcode);
        }
        public DataTable RptAttendanceSingleEmpForYear_user(string EmpNo, string FinaYear, string Dept, string Ccode, string Lcode)
        {
            return objdata.RptAttendanceSingleEmpForYear_user(EmpNo, FinaYear, Dept, Ccode, Lcode);
        }
        public DataTable RptAttendanceAllEmpForMonth(DateTime FromDate, DateTime ToDate, string FinaYear, string Dept, string Ccode, string Lcode)
        {
            return objdata.RptAttendanceAllEmpForMonth(FromDate, ToDate, FinaYear, Dept, Ccode, Lcode);
        }
        public DataTable RptAttendanceAllEmpForMonth_user(DateTime FromDate, DateTime ToDate, string FinaYear, string Dept, string Ccode, string Lcode)
        {
            return objdata.RptAttendanceAllEmpForMonth_user(FromDate, ToDate, FinaYear, Dept, Ccode, Lcode);
        }
        public DataTable RptAttendanceAllEmpForYear(string FinaYear, string Dept, string Ccode, string Lcode)
        {
            return objdata.RptAttendanceAllEmpForYear(FinaYear, Dept, Ccode, Lcode);
        }
        public DataTable RptAttendanceAllEmpForYear_user(string FinaYear, string Dept, string Ccode, string Lcode)
        {
            return objdata.RptAttendanceAllEmpForYear_user(FinaYear, Dept, Ccode, Lcode);
        }
        public DataTable RptAttendanceSingleLabourForMonth(string EmpNo, DateTime FromDate, DateTime ToDate, string FinaYear, string Dept, string Cate, string Ccode, string Lcode)
        {
            return objdata.RptAttendanceSingleLabourForMonth(EmpNo, FromDate, ToDate, FinaYear, Dept, Cate, Ccode, Lcode);
        }
        public DataTable RptAttendanceSingleLabourForMonth_user(string EmpNo, DateTime FromDate, DateTime ToDate, string FinaYear, string Dept, string Cate, string Ccode, string Lcode)
        {
            return objdata.RptAttendanceSingleLabourForMonth_user(EmpNo, FromDate, ToDate, FinaYear, Dept, Cate, Ccode, Lcode);
        }
        public DataTable RptAttendanceSingleLabourForYear(string EmpNo, string FinaYear, string Dept, string Cate, string Ccode, string Lcode)
        {
            return objdata.RptAttendanceSingleLabourForYear(EmpNo, FinaYear, Dept, Cate, Ccode, Lcode);
        }
        public DataTable RptAttendanceSingleLabourForYear_user(string EmpNo, string FinaYear, string Dept, string Cate, string Ccode, string Lcode)
        {
            return objdata.RptAttendanceSingleLabourForYear_user(EmpNo, FinaYear, Dept, Cate, Ccode, Lcode);
        }
        public DataTable RptAttendanceAllLabourForMonth(DateTime FromDate, DateTime ToDate, string FinaYear, string Dept, string Cate, string Ccode, string Lcode)
        {
            return objdata.RptAttendanceAllLabourForMonth(FromDate, ToDate, FinaYear, Dept, Cate, Ccode, Lcode);
        }
        public DataTable RptAttendanceAllLabourForMonth_user(DateTime FromDate, DateTime ToDate, string FinaYear, string Dept, string Cate, string Ccode, string Lcode)
        {
            return objdata.RptAttendanceAllLabourForMonth_user(FromDate, ToDate, FinaYear, Dept, Cate,Ccode,Lcode);
        }
        public DataTable RptAttendanceAllLabourForYear(string FinaYear, string Dept, string Cate, string Ccode, string Lcode)
        {
            return objdata.RptAttendanceAllLabourForYear(FinaYear, Dept, Cate, Ccode, Lcode);
        }
        public DataTable RptAttendanceAllLabourForYear_user(string FinaYear, string Dept, string Cate, string Ccode, string Lcode)
        {
            return objdata.RptAttendanceAllLabourForYear_user(FinaYear, Dept, Cate, Ccode, Lcode);
        }
        public DataTable DepartwiseEmployee(string department, string gender, string IsAdmin, string Cate, string Ccode, string Lcode, string pfgrade)
        {
            return objdata.DepartmentwiseEmployee(department, gender, IsAdmin, Cate, Ccode, Lcode, pfgrade);
        }
        public DataTable DepartmentwiseEmployee_user(string department, string gender, string Cate)
        {
            return objdata.DepartmentwiseEmployee_user(department,gender, Cate);
        }
        public DataTable Departmentwise(string department, string empNo)
        {
            return objdata.Departmentwise(department, empNo);
        }
        public DataTable Probationperiod(string department, string Cate, string Ccode, string Lcode)
        {
            return objdata.Probationperiod(department, Cate, Ccode, Lcode);
        }
        public DataTable Probationperiod_user(string department, string Cate, string Ccode, string Lcode)
        {
            return objdata.Probationperiod_user(department, Cate, Ccode, Lcode);
        }
        public DataTable Probationperiodsearch(string department, string empNo, string Cate, string Ccode, string Lcode)
        {
            return objdata.Probationperiodsearch(department, empNo, Cate, Ccode, Lcode);
        }
        public DataTable Probationperiodsearch_user(string department, string empNo, string Cate, string Ccode, string Lcode)
        {
            return objdata.Probationperiodsearch_user(department, empNo, Cate, Ccode, Lcode);
        }
        #endregion
        #region StandardBonus For All

        public DataTable StandardBonusEmpLoad(string department, string staffLabour, string finance, string Ccode, string Lcode)
        {
            return objdata.StandardBonusEmpLoad(department, staffLabour, finance, Ccode, Lcode);
        }
        public DataTable StandardBonusConfirmEmpLoad(string department, string staffLabour, string finance, string Ccode, string Lcode)
        {
            return objdata.StandardBonusConfirmEmpLoad(department, staffLabour, finance, Ccode, Lcode);
        }
        public DataTable StandardBonusProbationEmpLoad(string department, string staffLabour, string financial, string Ccode, string Lcode)
        {
            return objdata.StandardBonusProbationEmpLoad(department, staffLabour, financial, Ccode, Lcode);
        }
        public DataTable StandardBonuLabour(string department, string staffLabour, string finance, string day,string Ccode,string Lcode)
        {
            return objdata.StandardBonuLabour(department, staffLabour, finance, day,Ccode,Lcode);
        }

        public void BonusforallInsert(BonusforAllclass objbonus)
        {
            objdata.BonusforallInsert(objbonus);
        }

        public DataTable StandardBonuswages_Load(string department, string Wages)
        {
            return objdata.StandardBonuswages_Load(department, Wages);
        }

        public DataTable StandardBonusLabour_Load(string department, string Wages)
        {
            return objdata.StandardBonusLabour_Load(department, Wages);
        }

        public string BonusFinancialYear(string Empno, string finance, string Ccode, string Lcode)
        {
            return objdata.BonusFinancialYear(Empno, finance, Ccode, Lcode);
        }

        #endregion
        #region SalaryMasters
        public DataTable BonusEmployeeID(string stafflabour, string department)
        {
            return objdata.BonusEmployeeID(stafflabour, department);
        }
        public DataTable EmployeeLoadSalaryMaster(string stafflabour, string department, string Ccode, string Lcode)
        {
            return objdata.EmployeeLoadSalaryMaster(stafflabour, department, Ccode, Lcode);
        }
        public DataTable EmployeeLoadSalaryMasterAllDept(string stafflabour, string Ccode, string Lcode, string TokenNo)
        {
            return objdata.EmployeeLoadSalaryMasterAllDept(stafflabour, Ccode, Lcode, TokenNo);
        }
        public DataTable EmployeeLoadSalaryMaster_user(string stafflabour, string department, string Ccode, string Lcode)
        {
            return objdata.EmployeeLoadSalaryMaster_user(stafflabour, department, Ccode, Lcode);
        }
        public DataTable BonusEmployeeName(string empID,string Ccode,string Lcode)
        {
            return objdata.BonusEmployeeName(empID,Ccode,Lcode);
        }
        public DataTable SalaryMasterProfile(string EmpCode,string Ccode,string Lcode)
        {
            return objdata.SalaryMasterProfile(EmpCode,Ccode,Lcode);
        }
       
        public string SalaryMasterVerify(string EmpCode,string Ccode,string Lcode)
        {
            return objdata.SalaryMasterVerify(EmpCode,Ccode,Lcode);
        }
        public DataTable SalaryMasterEmpLoad(string EmpCode,string Ccode, string Lcode)
        {
            return objdata.SalaryMasterEmpLoad(EmpCode, Ccode,Lcode);
        }
        public DataTable SalaryMasterEmpLoad_ForLabour(string EmpCode,string Ccode, string Lcode)
        {
            return objdata.SalaryMasterEmpLoad_ForLabour(EmpCode,Ccode,Lcode);
        }
        public DataTable SalaryMasterEmpLoad_Labour(string EmpCode, string Ccode, string Lcode)
        {
            return objdata.SalaryMasterEmpLoad_Labour(EmpCode, Ccode, Lcode);
        }
        public void SalaryMaster_Update(SalaryMasterClass objsal)
        {
            objdata.SalaryMaster_Update(objsal);
        }
        public void SalaryMaster_Insert(SalaryMasterClass objsal)
        {
            objdata.SalaryMaster_Insert(objsal);
        }
        public DataTable SalaryMasterLabour_Load(string department, string wages,string Ccode, string Lcode)
        {
            return objdata.SalaryMasterLabour_Load(department, wages, Ccode,Lcode);
        }
        public DataTable SalaryMasterLabour_ForSearchingExisiting(string department,string ExisistingNo,string Cate, string Ccode, string Lcode, string Admin)
        {
            return objdata.SalaryMasterLabour_ForSearchingExisiting(department, ExisistingNo,Cate, Ccode, Lcode, Admin);
        }
        public DataTable SalaryMasterLabour_ForSearchingExisiting_New(string department, string ExisistingNo, string Cate, string Ccode, string Lcode, string Admin, string Wages)
        {
            return objdata.SalaryMasterLabour_ForSearchingExisiting_New(department, ExisistingNo, Cate, Ccode, Lcode, Admin, Wages);
        }
        public DataTable SalaryMasterLabour_ForSearchingExisiting_user(string department, string ExisistingNo, string Cate, string Ccode, string Lcode)
        {
            return objdata.SalaryMasterLabour_ForSearchingExisiting_user(department, ExisistingNo, Cate, Ccode, Lcode);
        }
        #endregion

        public void BankLoanInsertRecord(Bankloanclass objBankloan)
        {
            objdata.BankLoanInsertRecord(objBankloan);
        }
        public DataTable Bankloan(string EmpNo, string Department)
        {
            return objdata.Bankloan(EmpNo, Department);
        }
        public DataTable Empdetails(string EmpNo)
        {
            return objdata.Empdetails(EmpNo);
        }

        public string RegularLabore(string EmpNo, string Ccode, string Lcode)
        {
            return objdata.RegularLaboure(EmpNo, Ccode, Lcode);
        }
        public string MessDeductionType(string EmpNo, string Ccode, string Lcode)
        {
            return objdata.MessDeductionType(EmpNo,Ccode, Lcode);
        }
        public DataTable StandardBonuLabourconform(string department, string staffLabour, string finance, string day, string Ccode, string Lcode)
        {
            return objdata.StandardBonuLabourconform(department, staffLabour, finance, day, Ccode, Lcode);
        }
        public DataTable StandardBonuLabourProbation(string department, string staffLabour, string day, string finance, string Ccode, string Lcode)
        {
            return objdata.StandardBonuLabourProbation(department, staffLabour, day, finance, Ccode, Lcode);
        }
        public string SearchEmployee_save_SP(string empNo, string Ccode, string Lcode)
        {
            return objdata.SearchEmployee_save_SP(empNo, Ccode, Lcode);
        }
        public string Wages_SP(string Empno,string Ccode, string Lcode)
        {
            return objdata.labourWges(Empno,Ccode,Lcode);
        }
        public DataTable AlreadyLeaveDateLoad(string EmpNo, string Financial, string Ccode, string Lcode)
        {
            return objdata.AlreadyLeaveDateLoad(EmpNo, Financial, Ccode, Lcode);
        }
        public DataTable BonusRetrive(string EmpNo, string Category, string FinancialYear, string Ccode, string Lcode)
        {
            return objdata.BonusRetrive(EmpNo, Category, FinancialYear, Ccode, Lcode);
        }
        public DataTable LoadEmployeeForBonus(string stafflabour, string department, string Ccode, string Lcode)
        {
            return objdata.LoadEmployeeForBonus(stafflabour, department, Ccode, Lcode);
        }
        public DataTable LoadEmployeeForBonus_Salary_History(string stafflabour, string department, string Ccode, string Lcode, string ActiveMode)
        {
            return objdata.LoadEmployeeForBonus_Salary_History(stafflabour, department, Ccode, Lcode, ActiveMode);
        }
        public DataTable EmpLoadFORBonus_USER(string stafflabour, string department, string Ccode, string Lcode)
        {
            return objdata.EmpLoadFORBonus_USER(stafflabour, department, Ccode, Lcode);
        }
        public DataTable EmpLoadFORBonus_USER_SalaryHistory(string stafflabour, string department, string Ccode, string Lcode, string ActiveMode)
        {
            return objdata.EmpLoadFORBonus_USER_SalaryHistory(stafflabour, department, Ccode, Lcode, ActiveMode);
        }
        public DataTable ExistNoSearchfor_bonus(string department, string stafflabour, string existno, string Ccode, string Lcode)
        {
            return objdata.ExistNoSearchfor_bonus(department, stafflabour, existno, Ccode, Lcode);
        }
        public DataTable ExistNoSearchfor_bonus_USER(string department, string stafflabour, string existno, string Ccode, string Lcode)
        {
            return objdata.ExistNoSearchfor_bonus_USER(department, stafflabour, existno, Ccode, Lcode);
        }
        public void Bonus_Update(BonusforAllclass objbonus, string Empno)
        {
            objdata.Bonus_Update(objbonus, Empno);
        }
        public string BankName_verify_value(string bankname)
        {
            return objdata.BankName_verify_value(bankname);
        }
        public string BankCd_verify_value(string bankcd)
        {
            return objdata.BankCd_verify_value(bankcd);
        }
        public string Bank_Delete(string bankcode)
        {
            return objdata.Bank_Delete(bankcode);
        }
        public string DepartmentName_check(string deptnm)
        {
            return objdata.DepartmentName_check(deptnm);
        }
        public string Departmentcode_check(string deptcode)
        {
            return objdata.Departmentcode_check(deptcode);
        }
        public string Departmentcode_delete(string deptcode)
        {
            return objdata.Departmentcode_delete(deptcode);
        }
        public string EmpType_check(string empType, string staff)
        {
            return objdata.EmpType_check(empType, staff);
        }
        public string EmpTypeName_check(string empType, string staff)
        {
            return objdata.EmpTypeName_check(empType, staff);
        }
        public string EmpType_delete(string empType)
        {
            return objdata.EmpType_delete(empType);
        }
        public string Insuranceid_check(string id)
        {
            return objdata.Insuranceid_check(id);
        }
        public string Insurancename_check(string InsName)
        {
            return objdata.Insurancename_check(InsName);
        }
        public string Insurance_delete(string insid)
        {
            return objdata.Insurance_delete(insid);
        }
        public string Leaveid_check(string id1)
        {
            return objdata.Leaveid_check(id1);
        }
        public string Leavename_check(string name)
        {
            return objdata.Leavename_check(name);
        }
        public string Leave_delete(string id1)
        {
            return objdata.Leave_delete(id1);
        }
        public string Probationid_check(string Pid)
        {
            return objdata.Probationid_check(Pid);
        }
        public string Probationmonth_check(string month)
        {
            return objdata.Probationmonth_check(month);
        }
        public string Probation_delete(string Pid)
        {
            return objdata.Probation_delete(Pid);
        }
        public string qualificationid_check(string Qid)
        {
            return objdata.qualificationid_check(Qid);
        }
        public string qualificationname_check(string qname)
        {
            return objdata.qualificationname_check(qname);
        }
        public string qualification_delete(string Qid)
        {
            return objdata.qualification_delete(Qid);
        }
        public DataTable Deactive_Load_all(string Staff, string Ccode, string Lcode)
        {
            return objdata.Deactive_Load_all(Staff, Ccode, Lcode);
        }
        public DataTable user_gridLoad(string Ccode, string Lcode)
        {
            return objdata.user_gridLoad(Ccode, Lcode);
        }
        public DataTable user_edit(string userid)
        {
            return objdata.user_edit(userid);
        }
        public string user_verify(string user, string Ccode, string Lcode)
        {
            return objdata.user_verify(user, Ccode, Lcode);
        }
        public DataTable Loadgrid_leaveApply(string Ccode,string Lcode)
        {
            return objdata.Loadgrid_leaveApply(Ccode, Lcode);
        }
        public DataTable retrive_leaveApply(string EmpNo)
        {
            return objdata.retrive_leaveApply(EmpNo);
        }
        public void leaveCancel(string tid)
        {
            objdata.leaveCancel(tid);
        }
        public DataTable AlreadyLeaveDateLoad_edit(string EmpNo, string Financial, string tid, string Ccode, string Lcode)
        {
            return objdata.AlreadyLeaveDateLoad_edit(EmpNo, Financial, tid, Ccode, Lcode);
        }
        public DataTable AllReadyApplyForLeaveDate_edit(string EmpNo, DateTime FromDate, DateTime ToDate, string FinaYear, string tid, string Ccode, string Lcode)
        {
            return objdata.AllReadyApplyForLeaveDate_edit(EmpNo, FromDate, ToDate, FinaYear, tid, Ccode, Lcode);
        }
        public void leaveupdate_verify(string tid, string Ccode, string Lcode)
        {
            objdata.leaveupdate_verify(tid, Ccode, Lcode);
        }
        public void Rpt_contractLoad(string department)
        {
            objdata.Rpt_contractLoad(department);
        }
        public DataTable Contractbreak_EmployeeSearch_exist(string department, string exist, string cate, string isadmin, string Ccode, string Lcode)
        {
            return objdata.Contractbreak_EmployeeSearch_exist(department, exist, cate, isadmin, Ccode, Lcode);
        }
        public DataTable Probationperiodsearch_exist(string department, string exist, string Cate, string Ccode, string Lcode)
        {
            return objdata.Probationperiodsearch_exist(department, exist, Cate, Ccode, Lcode);
        }
        public DataTable Probationperiodsearch_exist_user(string department, string exist, string Cate, string Ccode, string Lcode)
        {
            return objdata.Probationperiodsearch_exist_user(department, exist, Cate, Ccode, Lcode);
        }
        public DataTable AdvanceSalary_EmpNo(string EmpNo, string Ccode, string Lcode)
        {
            return objdata.AdvanceSalary_EmpNo(EmpNo, Ccode, Lcode);
        }
        public DataTable AdvanceSalary_exist(string Exist, string department, string Ccode, string Lcode)
        {
            return objdata.AdvanceSalary_exist(Exist, department, Ccode, Lcode);
        }
        public DataTable AdvanceSalary_exist_user(string Exist, string department, string Ccode, string Lcode)
        {
            return objdata.AdvanceSalary_exist_user(Exist, department, Ccode, Lcode);
        }
        public string Advance_employee_verify(string Exist, string empName)
        {
            return objdata.Advance_employee_verify(Exist, empName);
        }
        public string Advance_insert_verify(string empNo, string Ccode, string Lcode)
        {
            return objdata.Advance_insert_verify(empNo, Ccode, Lcode);
        }
        public void Advance_insert(AdvanceAmount objsal, DateTime transdate)
        {
            objdata.Advance_insert(objsal, transdate);
        }
        public DataTable Salary_advance_retrive(string empNo, string Ccode, string Lcode)
        {
            return objdata.Salary_advance_retrive(empNo, Ccode, Lcode);
        }
        public void Advance_repayInsert(string EmpNo, DateTime Transdate, string Months, string Amount, string AdvId, string Ccode, string Lcode)
        {
            objdata.Advance_repayInsert(EmpNo, Transdate, Months, Amount, AdvId, Ccode, Lcode);
        }
        public void Advance_update(string EmpNo, string BalanceAmount, string IncMonth, string DecMonth, string Id, string completed, DateTime modifiedDate, string Ccode, string Lcode)
        {
            objdata.Advance_update(EmpNo, BalanceAmount, IncMonth, DecMonth, Id, completed, modifiedDate, Ccode, Lcode);
        }
        public DataTable ADV_ALL_REPORT(string EmpNo)
        {
           return objdata.ADV_ALL_REPORT(EmpNo);
        }
        public DataTable Adv_history(string id, string Ccode, string Lcode)
        {
            return objdata.Adv_history(id, Ccode, Lcode);
        }
        public DataTable Depart_Labour()
        {
            return objdata.Depart_Labour();
        }
        public DataTable repay_load(string EmpNo, string Ccode, string Lcode)
        {
            return objdata.repay_load(EmpNo, Ccode, Lcode);
        }
        public DataTable months_load()
        {
            return objdata.months_load();
        }
        public void Attenance_insert(AttenanceClass objAtt, DateTime fromDate, DateTime Todate, string modeval)
        {
            objdata.Attenance_insert(objAtt, fromDate, Todate, modeval);
        }
        public void Attenance_Update(AttenanceClass objAtt)
        {
            objdata.Attenance_Update(objAtt);
        }
        public DataTable Salary_attenanceDetails(string EmpNo, string Months, string Finance, string Department, string Ccode, string Lcode, DateTime dfrom, DateTime dto)
        {
            return objdata.Salary_attenanceDetails(EmpNo, Months, Finance, Department, Ccode, Lcode, dfrom, dto);
        }
        public DataTable Settle_EmpNo_load(string EmpNo, string Ccode, string Lcode)
        {
            return objdata.Settle_EmpNo_load(EmpNo, Ccode, Lcode);
        }
        public DataTable Settle_Exist_load(string EmpNo, string category, string department, string Ccode, string Lcode)
        {
            return objdata.Settle_Exist_load(EmpNo, category, department, Ccode, Lcode);
        }
        public void settle_insert(resignClass rej, DateTime doj, DateTime resigndate)
        {
            objdata.settle_insert(rej, doj, resigndate);
        }
        public string pf_Verify(string EmpNo, string Ccode, string Lcode)
        {
            return objdata.pf_Verify(EmpNo, Ccode, Lcode);
        }
        public DataTable PF_form5(string dpt, string staff)
        {
            return objdata.PF_form5(dpt, staff);
        }
        public DataTable PF_form5_user(string dpt, string staff)
        {
            return objdata.PF_form5_user(dpt, staff);
        }
        public DataTable Form10_sp(string cate, string dpt, string months, string finance)
        {
            return objdata.Form10_sp(cate,dpt,months,finance);
        }
        public DataTable Form10_sp_user(string cate, string dpt, string months, string finance)
        {
            return objdata.Form10_sp_user(cate, dpt, months, finance);
        }
        public DataTable Emp_Load_PF(string cate, string dpt)
        {
            return objdata.Emp_Load_PF(cate, dpt);
        }
        public DataTable Emp_Load_PF_user(string cate, string dpt)
        {
            return objdata.Emp_Load_PF_user(cate, dpt);
        }
        public DataTable Form3A_sp(string EmpNo, string finance)
        {
            return objdata.Form3A_sp(EmpNo, finance);
        }
        public DataTable Emp_PFnumber_load(string EmpNo)
        {
            return objdata.Emp_PFnumber_load(EmpNo);
        }
        public DataTable Form6A_New_SP(string cate, string dpt, string finance)
        {
            return objdata.Form6A_New_SP(cate, dpt, finance);
        }
        public DataTable Form6A_New_SP_user(string cate, string dpt, string finance)
        {
            return objdata.Form6A_New_SP_user(cate, dpt, finance);
        }
        public DataTable Emp_load_ESI(string cate, string department)
        {
            return objdata.Emp_load_ESI(cate, department);
        }
        public DataTable Emp_load_ESI_user(string cate, string department)
        {
            return objdata.Emp_load_ESI_user(cate, department);
        }
        public DataTable Form7_sp(string EmpNo)
        {
            return objdata.Form7_sp(EmpNo);
        }
        public DataTable rpt_resign_yr(string fromdate, string todate, string Ccode, string Lcode)
        {
            return objdata.rpt_resign_yr(fromdate, todate, Ccode, Lcode);
        }
        public DataTable rpt_resign_yr_user(string fromdate, string todate, string Ccode, string Lcode)
        {
            return objdata.rpt_resign_yr_user(fromdate, todate, Ccode,Lcode);
        }
        public DataTable rpt_resign_month(string fromdate, string todate, string months, string Ccode, string Lcode)
        {
            return objdata.rpt_resign_month(fromdate, todate, months,Ccode, Lcode);
        }
        public DataTable rpt_resign_month_user(string fromdate, string todate, string months, string Ccode, string Lcode)
        {
            return objdata.rpt_resign_month_user(fromdate, todate, months,Ccode,Lcode);
        }
        public DataTable rpt_resign_empNo(string EmpNo, string Ccode, string Lcode)
        {
            return objdata.rpt_resign_empNo(EmpNo, Ccode, Lcode);
        }
        public DataTable rpt_resign_empNo_user(string EmpNo, string Ccode, string Lcode)
        {
            return objdata.rpt_resign_empNo_user(EmpNo, Ccode, Lcode);
        }
        public DataTable rpt_resign_exist(string exist, string Ccode, string Lcode)
        {
            return objdata.rpt_resign_exist(exist, Ccode, Lcode);
        }
        public DataTable rpt_resign_exist_user(string exist, string Ccode, string Lcode)
        {
            return objdata.rpt_resign_exist_user(exist, Ccode, Lcode);
        }
        public DataTable LeaveAllocation_default(string dept, string Ccode, string Lcode)
        {
            return objdata.LeaveAllocation_default(dept, Ccode, Lcode);
        }
        public string CompnyCode_verify(string Ccode)
        {
            return objdata.CompnyCode_verify(Ccode);
        }
        public string LocCode_verify(string Lcode)
        {
            return objdata.LocCode_verify(Lcode);
        }
        public void AdminRights_Insert(Administratorclass Ad)
        {
            objdata.AdminRights_Insert(Ad);
        }
        public DataTable AdminRights_load()
        {
            return objdata.AdminRights_load();
        }
        public DataTable Admin_Retrive(string Ccode)
        {
            return objdata.Admin_Retrive(Ccode);
        }
        public void AdminRights_Update(Administratorclass AD)
        {
            objdata.AdminRights_Update(AD);
        }
        public DataTable Dropdown_Company()
        {
            return objdata.Dropdown_Company();
        }
        public DataTable dropdown_loc(string Ccode)
        {
            return objdata.dropdown_loc(Ccode);
        }
        public DataTable AltiusLogin(UserRegistrationClass objuser)
        {
            return objdata.AltiusLogin(objuser);
        }
        public DataTable UserLogin(UserRegistrationClass objuser)
        {
            return objdata.UserLogin(objuser);
        }
        public DataTable Company_retrive(string Ccode, string Lcode)
        {
            return objdata.Company_retrive(Ccode, Lcode);
        }
        public DataTable Employee_EmpNo_load(string Ccode, String Lcode, string type, string staff)
        {
            return objdata.Employee_EmpNo_load(Ccode, Lcode, type, staff);
        }
        public string carry_load(string EmpNo, string ccode, string Lcode)
        {
            return objdata.carry_load(EmpNo, ccode, Lcode);
        }
        public string Bonus_attenance_Load(string EmpNo, string Dept, string finance, string Ccode, string Lcode)
        {
            return objdata.Bonus_attenance_Load(EmpNo, Dept, finance, Ccode, Lcode);
        }
        public string Bonus_attenance_Load_months(string EmpNo, string Dept, string finance, string Ccode, string Lcode)
        {
            return objdata.Bonus_attenance_Load_months(EmpNo, Dept, finance, Ccode, Lcode);
        }
        public DataTable OverTime_Load(string dept, string Admin, string Ccode, string Lcode, string Wagestype)
        {
            return objdata.OverTime_Load(dept, Admin, Ccode, Lcode, Wagestype);
        }
        public DataTable Overtime_Details(string dept, string EmpNo, string Ccode, string Lcode)
        {
            return objdata.Overtime_Details(dept, EmpNo, Ccode, Lcode);
        }
        public DataTable OverTime_Exist(string dept, string EmpNo, string Ccode, string Lcode, string admin, string wages)
        {
            return objdata.OverTime_Exist(dept, EmpNo, Ccode, Lcode, admin, wages);
        }
        public string OV_Exist_verify(string EmpNo, string Dept, string Ccode, string Lcode)
        {
            return objdata.OV_Exist_verify(EmpNo, Dept, Ccode, Lcode);
        }
        public string Overtime_verify(string EmpNo, string Dept, string Month, string finance, string Ccode, string Lcode, DateTime fromdt, DateTime todt)
        {
            return objdata.Overtime_verify(EmpNo, Dept, Month, finance, Ccode, Lcode, fromdt, todt);
        }
        public void OverTime_insert(Overtimeclass objot, string Ccode, string Lcode, DateTime fromdt, DateTime todt)
        {
            objdata.OverTime_insert(objot, Ccode, Lcode, fromdt, todt);
        }
        public DataTable Attenance_ot(string EmpNo, string Months, string Finance, string dept, string Ccode, string Lcode, DateTime fromdt, DateTime toDt)
        {
            return objdata.Attenance_ot(EmpNo, Months, Finance, dept, Ccode, Lcode, fromdt, toDt);
        }
        public DataTable PF_Download(string Months, string Finance, string Ccode, string Lcode, string Admin, string stafflabor)
        {
            return objdata.PF_Download(Months, Finance, Ccode, Lcode, Admin, stafflabor);
        }
        public DataTable Employee_Download(string Ccode, string Lcode, string Admin)
        {
            return objdata.Employee_Download(Ccode, Lcode, Admin);
        }
        public DataTable Advance_restore(string EmpNo, string Ccode, string Lcode)
        {
            return objdata.Advance_restore(EmpNo, Ccode, Lcode);
        }
        public void AdvanceAmount_Update_Loan(string EmpNo, string Months, string Balance, string Ccode, string Lcode, string due)
        {
            objdata.AdvanceAmount_Update_Loan(EmpNo, Months, Balance, Ccode, Lcode, due);
        }
        public DataTable ESI_PF_Load(string Ccode, string Lcode)
        {
            return objdata.ESI_PF_Load(Ccode, Lcode);
        }
        public string ESIPF_verify(string Ccode, string Lcode)
        {
            return objdata.ESIPF_verify(Ccode, Lcode);
        }
        public void ESIPF_Insert(string PF, string ESI, string Sal, string Ccode, string Lcode, string Stamp, string VDA1, string VDA2, string Union, string spinning, string halfNight, string FullNight, string DayIncentive, string Three, string EmployeerPFone, string EmployeerPFtwo, string EmployeerESI)
        {
            objdata.ESIPF_Insert(PF, ESI, Sal, Ccode, Lcode, Stamp, VDA1, VDA2, Union, spinning, halfNight, FullNight, DayIncentive, Three,EmployeerPFone,EmployeerPFtwo,EmployeerESI);
        }
        public void ESIPF_Update(string PF, string ESI, string Sal, string Ccode, string Lcode, string Stamp, string VDA1, string VDA2, string Union, string spinning, string halfNight, string FullNight, string DayIncentive, string three, string EmployeerPFone, string EmployeerPFtwo, string EmployeerESI)
        {
            objdata.ESIPF_Update(PF, ESI, Sal, Ccode, Lcode, Stamp, VDA1, VDA2, Union, spinning, halfNight, FullNight, DayIncentive, three, EmployeerPFone, EmployeerPFtwo, EmployeerESI);
        }
        public DataTable Salary_ExistNo(string department, string ExisistingNo, string Cate, string Ccode, string Lcode, string Admin)
        {
            return objdata.Salary_ExistNo(department, ExisistingNo, Cate, Ccode, Lcode, Admin);
        }
        public DataTable NewSlary_Load(string Ccode, string Lcode, string Empcode)
        {
            return objdata.NewSlary_Load(Ccode, Lcode, Empcode);
        }
        public DataTable Load_pf_details(string Ccode, string Lcode)
        {
            return objdata.Load_pf_details(Ccode, Lcode);
        }
        public DataTable EligibleESI_PF(string Ccode, string Lcode, string EmpNo)
        {
            return objdata.EligibleESI_PF(Ccode, Lcode, EmpNo);
        }
        public string OT_Salary(string EmpNo, string Ccode, string Lcode, string Months, DateTime Mydate1, DateTime Mydate2)
        {
            return objdata.OT_Salary(EmpNo, Ccode, Lcode, Months,Mydate1,Mydate2 );
        }
        public DataTable PF_ESI_Amt(string EmpNo, string Ccode, string Lcode)
        {
            return objdata.PF_ESI_Amt(EmpNo, Ccode, Lcode);
        }
        public DataTable Rpt_overtime(string Ccode, string Lcode, string Finance, string Month)
        {
            return objdata.Rpt_overtime(Ccode, Lcode, Finance, Month);
        }
        public DataTable Sample_EmpDownload(string Ccode, string Lcode)
        {
            return objdata.Sample_EmpDownload(Ccode, Lcode);
        }
        public DataTable Sample_Attenance(string Ccode, string Lcode, string Salaryvalue, string staff)
        {
            return objdata.Sample_Attenance(Ccode, Lcode, Salaryvalue, staff);
        }
        public DataTable Sample_Attenance_Download(string Ccode, string Lcode, string Salaryvalue, string staff, string EmployeeType)
        {
            return objdata.Sample_Attenance_Download(Ccode, Lcode, Salaryvalue, staff, EmployeeType);
        }
        public DataTable Wages_Download(string Ccode, string Lcode, string Salaryvalue, string staff, string EmployeeType)
        {
            return objdata.Wages_Download(Ccode, Lcode, Salaryvalue, staff, EmployeeType);
        }
        public DataTable Sample_Leave(string Ccode, string Lcode)
        {
            return objdata.Sample_Leave(Ccode, Lcode);
        }
        public DataTable ESI_Download(string Months, string Finance, string Ccode, string Lcode, string Admin, string staffLabour)
        {
            return objdata.ESI_Download(Months, Finance, Ccode, Lcode, Admin, staffLabour);
        }
        public DataTable OT_Download(string Ccode, string Lcode, string salarytype)
        {
            return objdata.OT_Download(Ccode, Lcode, salarytype);
        }
        public DataTable OT_Download_EmpTypeWise(string Ccode, string Lcode, string salarytype, string EmployeeType)
        {
            return objdata.OT_Download_EmpTypeWise(Ccode, Lcode, salarytype, EmployeeType);
        }
        public DataTable SalaryDetails_rpt(string Months, string finance, string Ccode, string Lcode, string admin, string stafflabor)
        {
            return objdata.SalaryDetails_rpt(Months, finance, Ccode, Lcode, admin, stafflabor);
        }
        public DataTable SalarySummary(string Finance, string Ccode, string Lcode)
        {
            return objdata.SalarySummary(Finance, Ccode, Lcode);
        }
        public DataTable IncentiveAmt(string Finance, string Months, string StaffLabour, string Admin, string Ccode, string Lcode, string department)
        {
            return objdata.IncentiveAmt(Finance, Months, StaffLabour, Admin, Ccode, Lcode, department);
        }
        public DataTable Employee_muster(string Ccode, string Lcode, string department, string staff, string admin)
        {
            return objdata.Employee_muster(Ccode, Lcode, department, staff, admin);
        }
        public DataTable Salary_total_bonus(string EmpNo, int Finance, string Ccode, string Lcode)
        {
            return objdata.Salary_total_bonus(EmpNo, Finance, Ccode, Lcode);
        }
        public DataTable BonusRpt(string Ccode, string Lcode, string finance, string admin, string Stafflabour)
        {
            return objdata.BonusRpt(Ccode, Lcode, finance, admin, Stafflabour);
        }
        public DataTable Salary_retrive(string EmpNo, string finance, string Month, string Ccode, string Lcode)
        {
            return objdata.Salary_retrive(EmpNo, finance, Month, Ccode, Lcode);
        }
        public DataTable Advance_Edit(string EmpNo, string Months, string Amount, string Ccode, string Lcode)
        {
            return objdata.Advance_Edit(EmpNo, Months, Amount, Ccode, Lcode);
        }
        public void SalaryDetails_update(SalaryClass objSal, string EmpNo, string ExisistingNo, DateTime Mydate, string MyMonth, string Year, string FinaYear, DateTime TransDate, DateTime fromdt, DateTime Todt)
        {
            objdata.SalaryDetails_update(objSal, EmpNo, ExisistingNo, Mydate, MyMonth, Year, FinaYear, TransDate,fromdt, Todt);
        }
        public DataTable group_employee(string Months, string Finance, string Ccode, string Lcode, string EmpType, string stafflabour)
        {
            return objdata.group_employee(Months, Finance, Ccode, Lcode, EmpType, stafflabour);
        }
        public DataTable GroupSalary_Load_attenance(string EmpNo, string Ccode, string Lcode, string Months, string Finance)
        {
            return objdata.GroupSalary_Load_attenance(EmpNo, Ccode, Lcode, Months, Finance);
        }
        public DataTable Group_salary_load(string EmpNo, string Ccode, string Lcode)
        {
            return objdata.Group_salary_load(EmpNo, Ccode, Lcode);
        }
        public DataTable Group_ot_load(string EmpNo, string Months, string Finance, string Ccode, string Lcode)
        {
            return objdata.Group_ot_load(EmpNo, Months, Finance, Ccode, Lcode);
        }
        public string Group_Existing(string EmpNo, string Ccode, string Lcode)
        {
            return objdata.Group_Existing(EmpNo, Ccode, Lcode);
        }
        public DataTable Form3A_Load(string EmpNo, string Ccode, string Lcode)
        {
            return objdata.Form3A_Load(EmpNo, Ccode, Lcode);
        }
        public DataTable PF_Load1(string EmpNo, string staff, string Ccode, string Lcode, string Finance)
        {
            return objdata.PF_Load1(EmpNo, staff, Ccode, Lcode, Finance);
        }
        public DataTable PF_EmpLoad(string Ccode, string Lcode, string EmpType)
        {
            return objdata.PF_EmpLoad(Ccode, Lcode, EmpType);
        }
        public DataTable PF_Load(string EmpNo, string staff, string Ccode, string Lcode, string Finance)
        {
            return objdata.PF_Load(EmpNo, staff, Ccode, Lcode, Finance);
        }
        public DataTable Form6A_load(string Ccode, string Lcode, string Finance, string Finance1)
        {
            return objdata.Form6A_load(Ccode, Lcode, Finance, Finance1);
        }
        public string Establishment(string Ccode, string Lcode)
        {
            return objdata.Establishment(Ccode, Lcode);
        }
        public void contract_insert_sp(ContractClass clss)
        {
            objdata.contract_insert_sp(clss);
        }
        public string Contract_verify(string contractname)
        {
            return objdata.Contract_verify(contractname);
        }
        public DataTable Contract_retrive()
        {
            return objdata.Contract_retrive();
        }
        public DataTable Contract_Edit(string ContractCode)
        {
            return objdata.Contract_Edit(ContractCode);
        }
        public void Contract_Update_sp(ContractClass clss, string code)
        {
            objdata.Contract_Update_sp(clss, code);
        }
        public DataTable DropDown_contract()
        {
            return objdata.DropDown_contract();
        }
        public string Emp_contract(string EmpNo)
        {
            return objdata.Emp_contract(EmpNo);
        }
        public DataTable Official_contract(string Code)
        {
            return objdata.Official_contract(Code);
        }
        public void Insert_ContractDet(OfficialprofileClass objoff, string Date1)
        {
            objdata.Insert_ContractDet(objoff, Date1);
        }
        public DataTable Contract_Report(string Ccode, string Lcode, string ContractType)
        {
            return objdata.Contract_Report(Ccode, Lcode, ContractType);
        }
        public string Contract_GetEmpDet(string EmpNo, string Ccode, string Lcode)
        {
            return objdata.Contract_GetEmpDet(EmpNo, Ccode, Lcode);
        }
        public DataTable Contract_getPlanDetails(string EmpNo, string Ccode, string Lcode)
        {
            return objdata.Contract_getPlanDetails(EmpNo, Ccode, Lcode);
        }
        public void Contract_Reducing(string BalanceDays, string EmpNo, string val, string Ccode, string Lcode)
        {
            objdata.Contract_Reducing(BalanceDays, EmpNo, val, Ccode, Lcode);
        }
        public void Contract_Reducing_Month(string BalanceDays, string EmpNo, string val, string Ccode, string Lcode, string months)
        {
            objdata.Contract_Reducing_Month(BalanceDays, EmpNo, val, Ccode, Lcode, months);
        }
        public void Not_Contract_Reducing_Month(string EmpNo, string val, string Ccode, string Lcode)
        {
            objdata.Not_Contract_Reducing_Month(EmpNo, val, Ccode, Lcode);
        }
        public DataTable Contract_CompleteMonths(string Ccode, string Lcode, string ContractType)
        {
            return objdata.Contract_CompleteMonths(Ccode, Lcode, ContractType);
        }
        public DataTable Contract_CompleteDays(string Ccode, string Lcode, string ContractType)
        {
            return objdata.Contract_CompleteDays(Ccode, Lcode, ContractType);
        }
        public DataTable Contract_Details_CompletedReport(string EmpNo, string Ccode, string Lcode)
        {
            return objdata.Contract_Details_CompletedReport(EmpNo, Ccode, Lcode);
        }
        public void Contract_CompletedReport(OfficialprofileClass objclss, string Ccode, string Lcode, string startDate)
        {
            objdata.Contract_CompletedReport(objclss, Ccode, Lcode, startDate);        
        }
        public void DeleteContract(string EmpNo, string Ccode, string Lcode, string ContractType, string ContractName)
        {
            objdata.DeleteContract(EmpNo, Ccode, Lcode, ContractType, ContractName);
        }
        public DataTable Contract_Load()
        {
            return objdata.Contract_Load();
        }
        public DataTable Contract_Load_Employee(string Ccode, string Lcode)
        {
            return objdata.Contract_Load_Employee(Ccode, Lcode);
        }
        public string PF_Eligible_Salary(string EmpNo, string Ccode, string Lcode)
        {
            return objdata.PF_Eligible_Salary(EmpNo, Ccode, Lcode);
        }
        public string Verification_verify()
        {
            return objdata.Verification_verify();
        }
        public DataTable Value_Verify()
        {
            return objdata.Value_Verify();
        }
        public void Insert_verificationValue()
        {
            objdata.Insert_verificationValue();
        }
        public DataTable Employee_Payslip_Load(string Ccode, string Lcode, string Month, string FromDate, string ToDate, string Finance, string SalaryType)
        {
            return objdata.Employee_Payslip_Load(Ccode, Lcode, Month, FromDate, ToDate, Finance, SalaryType);
        }
        public string TeamLeader_Count(string Ccode, string Lcode)
        {
            return objdata.TeamLeader_Count(Ccode, Lcode);
        }
        public DataTable TeamLeader_New(string Ccode, string Lcode, string Department)
        {
            return objdata.TeamLeader_New(Ccode, Lcode, Department);
        }
        public DataTable TeamLeader_Exist(string Ccode, string Lcode, string Department)
        {
            return objdata.TeamLeader_Exist(Ccode, Lcode, Department);
        }
        public DataTable Team_Selection_Exist(string department, string ExisistingNo, string Cate, string Ccode, string Lcode)
        {
            return objdata.Team_Selection_Exist(department, ExisistingNo, Cate, Ccode, Lcode);
        }
        public DataTable Team_Selection_EmpName(string department, string EmpNo, string Cate, string Ccode, string Lcode)
        {
            return objdata.Team_Selection_EmpName(department, EmpNo, Cate, Ccode, Lcode);
        }
        public void TeamLeder_Insert(string EmpNo, string ID, string EmpName, string Ccode, string Lcode, string Department)
        {
            objdata.TeamLeder_Insert(EmpNo, ID, EmpName, Ccode, Lcode, Department);
        }
        public DataTable TeamLeader_Grid(string Ccode, string Lcode)
        {
            return objdata.TeamLeader_Grid(Ccode, Lcode);
        }
        public string TeamLeader_Remove_verify(string EmpNo, string Ccode, string Lcode)
        {
            return objdata.TeamLeader_Remove_verify(EmpNo, Ccode, Lcode);
        }
        public void TeamLeader_Remove(string EmpNo, string Ccode, string Lcode)
        {
            objdata.TeamLeader_Remove(EmpNo, Ccode, Lcode);
        }
        public DataTable TL_Load(string Ccode, string Lcode)
        {
            return objdata.TL_Load(Ccode, Lcode);
        }
        public DataTable TL_Search_ID(string EmpID, string Ccode, string Lcode)
        {
            return objdata.TL_Search_ID(EmpID, Ccode, Lcode);
        }
        public DataTable TL_Search_Name(string EmpID, string Ccode, string Lcode)
        {
            return objdata.TL_Search_Name(EmpID, Ccode, Lcode);
        }
        public DataTable TL_Loadgrid_ID(string EmpNo, string Ccode, string Lcode)
        {
            return objdata.TL_Loadgrid_ID(EmpNo, Ccode, Lcode);
        }
        public DataTable TeamMembers_Load(string Ccode, string Lcode, string Department)
        {
            return objdata.TeamMembers_Load(Ccode, Lcode, Department);
        }
        public void TeamMember_Insert(string tlEmpNo, string tmEmpNo, string Ccode, string Lcode, string TMname, string TMID, string TLID)
        {
            objdata.TeamMember_Insert(tlEmpNo, tmEmpNo, Ccode, Lcode, TMname, TMID, TLID);
        }
        public void Team_Member_Remove(string EmpNo, string Ccode, string Lcode)
        {
            objdata.Team_Member_Remove(EmpNo, Ccode, Lcode);
        }
        public void TeamLeaderIncentive_Insert(string Ccode, string Lcode, string TLCommission, string TLIncentive, string TLDays, string TLOT)
        {
            objdata.TeamLeaderIncentive_Insert(Ccode, Lcode, TLCommission, TLIncentive, TLDays, TLOT);
        }
        public string TeamLeaderIncentive_Verify(string Ccode, string Lcode)
        {
            return objdata.TeamLeaderIncentive_Verify(Ccode, Lcode);
        }
        public void TeamLeaderIncentive_Update(string Ccode, string Lcode, string TLCommission, string TLIncentive, string TLDays, string TLOT)
        {
            objdata.TeamLeaderIncentive_Update(Ccode, Lcode, TLCommission, TLIncentive, TLDays, TLOT);
        }
        public DataTable TeamIncentive_Load(string Ccode, string Lcode)
        {
            return objdata.TeamIncentive_Load(Ccode, Lcode);
        }
        public void WorkerIncentive_Insert(string Ccode, string Lcode, string WorkerDays, string Amt, string MonthDays)
        {
            objdata.WorkerIncentive_Insert(Ccode, Lcode, WorkerDays, Amt, MonthDays);
        }
        public string WorkerIncentive_Verify(string Ccode, string Lcode, string Days, string MonthDays)
        {
            return objdata.WorkerIncentive_Verify(Ccode, Lcode, Days, MonthDays);
        }


        public DataTable Wk_Load(string Ccode, string Lcode)
        {
            return objdata.Wk_Load(Ccode, Lcode);
        }

        public DataTable CivilIncentive_Load(string Ccode, string Lcode)
        {
            return objdata.CivilIncentive_Load(Ccode, Lcode);
        }

        public void WorkerIncentive_Update(string Ccode, string Lcode, string WorkerDays, string Amt, string MonthDays)
        {
            objdata.WorkerIncentive_Update(Ccode, Lcode, WorkerDays, Amt, MonthDays);
        }
        public void WorkerIncentive_Delete(string Ccode, string Lcode, string Days)
        {
            objdata.WorkerIncentive_Delete(Ccode, Lcode, Days);
        }
        public DataTable TeamMemberAttenance_Download(string Ccode, string Lcode, string MonthVal, string FinYearVal)
        {
            return objdata.TeamMemberAttenance_Download(Ccode, Lcode, MonthVal, FinYearVal);
        }
        public void TeamAttenance_Insert(string Ccode, string Lcode, string EmpNo, string ID, string Days, string OT, string Months, string FYear, string Year_1)
        {
            objdata.TeamAttenance_Insert(Ccode, Lcode, EmpNo, ID, Days, OT, Months, FYear, Year_1);
        }
        public DataTable TeamMember_Load_Incentive(string Ccode, string Lcode, string EmpNo, string Months, string FYear)
        {
            return objdata.TeamMember_Load_Incentive(Ccode, Lcode, EmpNo, Months, FYear);
        }
        public string TL_Data_Verify(string Ccode, string Lcode, string Months, string FYear, string EmpNo, string EmpNo_1)
        {
            return objdata.TL_Data_Verify(Ccode, Lcode, Months, FYear, EmpNo, EmpNo_1);
        }
        public string TL_Data_Process(string Ccode, string Lcode, string Months, string FYear, string EmpNo, string EmpNo_1)
        {
            return objdata.TL_Data_Process(Ccode, Lcode, Months, FYear, EmpNo, EmpNo_1);
        }
        public void TL_Data_Delete(string Ccode, string Lcode, string Months, string FYear, string EmpNo, string EmpNo_1)
        {
            objdata.TL_Data_Delete(Ccode, Lcode, Months, FYear, EmpNo, EmpNo_1);
        }
        public void Team_Leader_Data_Delete_Bulk(string Ccode, string Lcode, string Months, string FYear, string EmpNo)
        {
            objdata.Team_Leader_Data_Delete_Bulk(Ccode, Lcode, Months, FYear, EmpNo);
        }
        public void TL_Data_Insert(TeamLeaderIncentive obj)
        {
            objdata.TL_Data_Insert(obj);
        }
        public DataTable TL_Data_Report(string Ccode, string Lcode, string EmpNo, string Months, string FYear)
        {
            return objdata.TL_Data_Report(Ccode, Lcode, EmpNo, Months, FYear);
        }
        public DataTable TL_Data_All_Report(string Ccode, string Lcode, string Months, string FYear)
        {
            return objdata.TL_Data_All_Report(Ccode, Lcode, Months, FYear);
        }
               
        public DataTable Emp_Load_Worker_Incentive(string Ccode, string Lcode, string Department)
        {
            return objdata.Emp_Load_Worker_Incentive(Ccode, Lcode, Department);
        }
        public void Emp_Load_Worker_Update(string Ccode, string Lcode, string EmpNo, string Checkval, string Department)
        {
            objdata.Emp_Load_Worker_Update(Ccode, Lcode, EmpNo, Checkval, Department);
        }
        public DataTable Worker_Incentive_Retrive(string Ccode, string Lcode, string Months, string Finance)
        {
            return objdata.Worker_Incentive_Retrive(Ccode, Lcode, Months, Finance);
        }
        public string Worker_Cal(string Ccode, string Lcode, string Days)
        {
            return objdata.Worker_Cal(Ccode, Lcode, Days);
        }
        public void Work_Incentive_Insert(string Ccode, string Lcode, string EmpNo, string Days, string Months, string FinanceYear, string Amt)
        {
            objdata.Work_Incentive_Insert(Ccode, Lcode, EmpNo, Days, Months, FinanceYear, Amt);
        }
        public string Work_Incentive_Process(string Ccode, string Lcode, string EmpNo, string Months, string FinanceYear)
        {
            return objdata.Work_Incentive_Process(Ccode, Lcode, EmpNo, Months, FinanceYear);
        }
        public string Work_Incentive_Process_Edit(string Ccode, string Lcode, string EmpNo, string Months, string FinanceYear)
        {
            return objdata.Work_Incentive_Process_Edit(Ccode, Lcode, EmpNo, Months, FinanceYear);
        }
        public void Work_Incentive_Delete(string Ccode, string Lcode, string EmpNo, string Months, string FinanceYear)
        {
            objdata.Work_Incentive_Delete(Ccode, Lcode, EmpNo, Months, FinanceYear);
        }
        public DataTable Worker_Incentive_Report(string Ccode, string Lcode, string Months, string Finance)
        {
            return objdata.Worker_Incentive_Report(Ccode, Lcode, Months, Finance);
        }
        public void Worker_incentive_confirm(string Ccode, string Lcode, string Months, string Finance)
        {
            objdata.Worker_incentive_confirm(Ccode, Lcode, Months, Finance);
        }
        public void TL_Confirm(string Ccode, string Lcode, string EmpNo, string Months, string FYear)
        {
            objdata.TL_Confirm(Ccode, Lcode, EmpNo, Months, FYear);
        }
        public DataTable Contract_Scheme()
        {
            return objdata.Contract_Scheme();
        }
        public DataTable SalarySlab_Load(string SchemeName)
        {
            return objdata.SalarySlab_Load(SchemeName);
        }
        public string Salaryslab_Verify(string Slabname, string Scheme)
        {
            return objdata.Salaryslab_Verify(Slabname, Scheme);
        }
        public string SalarySlab_ID(string Schemename)
        {
            return objdata.SalarySlab_ID(Schemename);
        }
        public void SalarySlab_Insert(SalarySlab objSalSlab)
        {
            objdata.SalarySlab_Insert(objSalSlab);
        }
        public void SalarySlab_Update(string Schemename, string Amt, string ID, string SlabName)
        {
            objdata.SalarySlab_Update(Schemename, Amt, ID, SlabName);
        }
        public DataTable Contract_SalaryCal(string EmpNo, string Ccode, string Lcode)
        {
            return objdata.Contract_SalaryCal(EmpNo, Ccode, Lcode);
        }
        public string HostelIncentive_Verify(string Ccode, string Lcode, string Amt)
        {
            return objdata.HostelIncentive_Verify(Ccode, Lcode, Amt);
        }
        public string CivilIncentive_Verify(string Ccode, string Lcode, string ElbDays, string Amt)
        {
            return objdata.CivilIncentive_Verify(Ccode, Lcode, ElbDays, Amt);
        }
        public void HostelAmt_Insert(string Ccode, string Lcode, string Amt, string Wdays, string HAllowed, string OTDays, string NFHDays, string CalWdays, string CalHAllowed, string CalOTDays, string CalNFHDays, string NFHWorkDays, string CalNFHWorkDays)
        {
            objdata.HostelAmt_Insert(Ccode, Lcode, Amt, Wdays, HAllowed, OTDays, NFHDays, CalWdays, CalHAllowed, CalOTDays, CalNFHDays, NFHWorkDays, CalNFHWorkDays);
        }

        public void CivilAmt_Insert(string Ccode, string Lcode, string ElbDays, string Amt)
        {
            objdata.CivilAmt_Insert(Ccode, Lcode, ElbDays, Amt);
        }

        public void HostelAmt_Update(string Ccode, string Lcode, string Amt, string Wdays, string HAllowed, string OTDays, string NFHDays, string CalWdays, string CalHAllowed, string CalOTDays, string CalNFHDays, string NFHWorkDays, string CalNFHWorkDays)
        {
            objdata.HostelAmt_Update(Ccode, Lcode, Amt, Wdays, HAllowed, OTDays, NFHDays, CalWdays, CalHAllowed, CalOTDays, CalNFHDays, NFHWorkDays, CalNFHWorkDays);
        }

        public void CivilAmt_Update(string Ccode, string Lcode, string ElbDays, string Amt)
        {
            objdata.CivilAmt_Update(Ccode, Lcode, ElbDays, Amt);
        }

        public DataTable HostelIncentive_Load(string Ccode, string Lcode)
        {
            return objdata.HostelIncentive_Load(Ccode, Lcode);
        }
        public void HostelIncentive_Delete(string Ccode, string Lcode, string Amt)
        {
            objdata.HostelIncentive_Delete(Ccode, Lcode, Amt);
        }
        public DataTable Hoste_Load_Employee(string Ccode, string Lcode, string Months, string Finance)
        {
            return objdata.Hoste_Load_Employee(Ccode, Lcode, Months, Finance);
        }
        public string Hostel_AmtCount(string Ccode, string Lcode)
        {
            return objdata.Hostel_AmtCount(Ccode, Lcode);
        }
        public void HostelIncentive_Data_insert(string Ccode, string Lcode, string EmpNo, string OldID, string NewID, string EmpName, string Days, string Amt, string Months, string Finance)
        {
            objdata.HostelIncentive_Data_insert(Ccode, Lcode, EmpNo, OldID, NewID, EmpName, Days, Amt, Months, Finance);
        }
        public string Hostel_data_verify(string Ccode, string Lcode, string EmpNo, string months, string Finance)
        {
            return objdata.Hostel_data_verify(Ccode, Lcode, EmpNo, months, Finance);
        }
        public string Hostel_data_verify_1(string Ccode, string Lcode, string EmpNo, string months, string Finance)
        {
            return objdata.Hostel_data_verify_1(Ccode, Lcode, EmpNo, months, Finance);
        }
        public void Hostel_data_Delete(string Ccode, string Lcode, string EmpNo, string months, string Finance)
        {
            objdata.Hostel_data_Delete(Ccode, Lcode, EmpNo, months, Finance);
        }
        public DataTable Hostel_Incentive_Data_report(string Ccode, string Lcode, string months, string Finance)
        {
            return objdata.Hostel_Incentive_Data_report(Ccode, Lcode, months, Finance);
        }
        public void Hostel_Update(string Ccode, string Lcode, string months, string Finance)
        {
            objdata.Hostel_Update(Ccode, Lcode, months, Finance);
        }
        public void ESICode_Insert(string ESI)
        {
            objdata.ESICode_Insert(ESI);
        }
        public string ESICode_verify(string ESI)
        {
            return objdata.ESICode_verify(ESI);
        }
        public DataTable ESICode_GRID()
        {
            return objdata.ESICode_GRID();
        }
        public DataTable MstDepartment_Short(string Ccode, string Lcode)
        {
            return objdata.MstDepartment_Short(Ccode, Lcode);
        }
        public DataTable Slab_cal_salary(string Scheme)
        {
            return objdata.Slab_cal_salary(Scheme);
        }


        public DataTable RptEmployeeMultipleDetails(string Query_Val)
        {
            return objdata.RptEmployeeMultipleDetails(Query_Val);
        }


        public DataTable Emp_Designation()
        {
            return objdata.Emp_Designation();
        }
      
        #region LoanDetails
        public DataTable LoanDisplay()
        {
            return objdata.LoanDisplay();
        }
        public void UpdateLoan(Bankloanclass objmas)
        {
            objdata.UpdateLoan(objmas);
        }

        public void LoanMaster_Insert(Bankloanclass objmas)
        {
            objdata.LoanMaster_Insert(objmas);
        }

        #endregion
        #region Rports
        public DataTable SearchPFNumberForForm5(string PFNumber)
        {
            return objdata.SearchPFNumberForForm5(PFNumber);
        }
        #endregion





        public DataTable Deptdrop1()
        {
            return objdata.Deptdrop1();
        }

        public DataTable Deptdrop_Worker_Incentive(string Ccode,string Lcode)
        {
            return objdata.Deptdrop_Worker_Incentive(Ccode,Lcode);
        }



        public DataTable sal_his_all_report(string Ccode, string Lcode, string FinancialYear)
        {
            return objdata.sal_his_all_report(Ccode, Lcode, FinancialYear);
        }



        public DataTable deptdeactive(string Ccode, string Deparment)
        {
            return objdata.deptdeactive(Ccode, Deparment);
        }



        public DataTable DropDownCategory(string SessionCcode, string p)
        {
            throw new NotImplementedException();
        }

        public DataTable Deactive(string SessionCcode, string SessionLcode, string p, string p_4)
        {
            throw new NotImplementedException();
        }

        public DataTable Load_Incentive_Details(string Ccode, string Lcode, string MonthDays)
        {
            return objdata.Load_Incentive_Details(Ccode, Lcode, MonthDays);
        }

        public string BsaicDetails_Verify(string Ccode, string Lcode, string Category, string EmployeeType)
        {
            return objdata.BsaicDetails_Verify(Ccode, Lcode, Category, EmployeeType);
        }
        public void BsaicDetails_Insert_New(string Ccode, string Lcode, string Category, string EmployeeType, string BasicDA, string HRA, string ConvAllow, string EduAllow, string MediAllow, string RAI, string WashingAllow)
        {
            objdata.BsaicDetails_Insert_New(Ccode, Lcode, Category, EmployeeType, BasicDA, HRA, ConvAllow, EduAllow, MediAllow, RAI, WashingAllow);
        }

        public void BsaicDetails_Update(string Ccode, string Lcode, string Category, string EmployeeType, string BasicDA, string HRA, string ConvAllow, string EduAllow, string MediAllow, string RAI, string WashingAllow)
        {
            objdata.BsaicDetails_Update(Ccode, Lcode, Category, EmployeeType, BasicDA, HRA, ConvAllow, EduAllow, MediAllow, RAI, WashingAllow);
        }
        public DataTable BsaicDetails_Load(string Ccode, string Lcode, string Category, string EmployeeType)
        {
            return objdata.BsaicDetails_Load(Ccode, Lcode, Category, EmployeeType);
        }

        public DataTable GetMachineNo(string Machine, string Ccode, string Lcode)
        {
            return objdata.GetMachineNo(Machine, Ccode, Lcode);
        }

        public string SalaryMasterUploadVerify(string EmpCode, string Ccode, string Lcode)
        {
            return objdata.SalaryMasterUploadVerify(EmpCode, Ccode, Lcode);
        }

        public DataTable DeleteSM_Sp(string EmpNo, string Ccode, string Lcode)
        {
            return objdata.DeleteSM_Sp(EmpNo, Ccode, Lcode);
        }

        public void SalaryMasterUpload_Insert(SalaryMasterClass objsal)
        {
            objdata.SalaryMasterUpload_Insert(objsal);
        }

        public DataTable DeleOff_SP(string EmpNo, string Ccode, string Lcode)
        {
            return objdata.DeleOff_SP(EmpNo, Ccode, Lcode);
        }

        public void OfficalProfile_Upload(OfficialprofileClass objOff)
        {
            objdata.OfficalProfile_Upload(objOff);
        }
    }



}
