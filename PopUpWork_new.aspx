﻿<%@ Page Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="PopUpWork_new.aspx.cs" Inherits="PopUpWork_new" Title="" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<link href="assets/advanced-datatable/media/css/demo_table.css" rel="stylesheet" />

<asp:UpdatePanel ID="UpdatePanel1" runat="server">
<ContentTemplate> 
                                    
                                    
<div class="page-breadcrumb">
                    <ol class="breadcrumb container">
                   <h4><li class="active">Department Details</li>
                       <h4>
                       </h4>
                        <h4>
                       </h4>
                        </h4> 
                    </ol>
                </div>
 
<div id="main-wrapper" class="container">
<div class="row">
    <div class="col-md-12">
              
            <div class="col-md-9">
			<div class="panel panel-white">
			    <div class="panel panel-primary">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">Department</h4>
				</div>
				</div>
				<form class="form-horizontal">
				<div class="panel-body">
                    <asp:TextBox ID="txtItemCode" runat="server"></asp:TextBox>
                    <asp:TextBox ID="txtItemName" runat="server"></asp:TextBox>
				
            <asp:Button ID="BtnAdd" runat="server" Text="Add" class="btn btn-success" OnClick="BtnAdd_Check_Click"></asp:Button>
             <cc1:ModalPopupExtender ID="ModalPopupExtender1" runat="server" PopupControlID="Panel2" TargetControlID="BtnAdd"
                                    CancelControlID="BtnClear" BackgroundCssClass="modalBackground">
                                    </cc1:ModalPopupExtender>
             
             <div class="modal-dialog">
                                      <div class="modal-content">
                                          <div class="modal-header">
                                              <asp:LinkButton ID="LinkButton1" type="button" class="close" data-dismiss="modal" aria-hidden="true" runat="server"
                                              OnClientClick="javascript:$find('ModalPopupExtender1').hide();" >&times;</asp:LinkButton>
                                              <h4 class="modal-title">Company Master</h4>
                                          </div>
                                          <div class="modal-body">
                                            <div class="row">
                                            <div class="col-lg-12">
                                            
                                             <div class="col-lg-6">
                                             
                                             <!-- table start -->
					        <div class="col-md-12">
					            <div class="row">
					                <asp:Repeater ID="Repeater_StdPurOrder" runat="server" EnableViewState="false">
					                    <HeaderTemplate>
                                            <table id="example" class="display table">
                                                <thead>
                                                    <tr>
                                               <th>ItemCode</th>
                                                <th>ItemName</th>
                                               
                                                <th>View</th>
                                                    </tr>
                                                </thead>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                        <td><%# Eval("ItemCode")%></td>
                                        <td><%# Eval("ItemName")%></td>
                                                                                     
                                                <td>
                                                    <asp:LinkButton ID="btnEditEnquiry_Grid" class="btn btn-success btn-sm fa fa-pencil"  runat="server" 
                                                        Text="" OnCommand="GridViewClick" CommandArgument='<%# Eval("ItemCode")%>' CommandName='<%# Eval("ItemName")%>'>
                                                    </asp:LinkButton>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                        <FooterTemplate></table></FooterTemplate>                                
					                </asp:Repeater>
					            </div>
					        </div>
					        <!-- table End -->
                               </div>
                               </div>
                               </div>
                               </div>
                               </div>
                               </div>              
                        
                          </div>
                          </form>
                          </div>
                          </div>
                          </div>
                          </div>
 </div>
 
 
</ContentTemplate>
</asp:UpdatePanel>


</asp:Content>

