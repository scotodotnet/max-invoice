﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Payroll;
using Payroll.Configuration;
using Payroll.Data;
using System.IO;


using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Text;

public partial class MainPage : System.Web.UI.MasterPage
{
    protected System.Resources.ResourceManager rm;
    protected string PayrollRegisterContentMeta, pageTitle;
    BALDataAccess objdata = new BALDataAccess();
    OfficialprofileClass objOff = new OfficialprofileClass();
    EmployeeClass objEmp = new EmployeeClass();
    System.Globalization.CultureInfo culterInfo = new System.Globalization.CultureInfo("en-GB");
    // static string ExisitingNo = "";
    static string UserID = "", SesRoleCode = "", SessionAdmin = "";
    string SessionCcode;
    string SessionLcode;

    //Parthi Merge Start
    string Data;
    string SSQL;
    string SessionUserType;
    //BALDataAccess objdata = new BALDataAccess();
    DataTable dtd = new DataTable();

    string DashboardM;
    string masterpageM;
    string UserCreationM;
    string AdministrationRightM;
    string UserRightsM;
    string DepartmentDetailsM;
    string DesginationDetailsM;
    string EmployeeTypeDetailsM;
    string WagesTypeDetailsM;
    string BankDetailsM;
    string EmployeeDetailsM;
    string LeaveMasterM;

    string PermissionMasterM;
    string GraceTimeM;
    string EarlyOutMasterM;
    string LateINMasterM;
    string EmployeeM;
    string EmployeeApprovalM;
    string EmployeeStatusM;
    string LeaveManagementM;
    string TimeDeleteM;
    string LeaveDetailsM;

    string PermissionDetailM;
    string ManualAttendanceM;
    string ManualShiftM;
    string ManualShiftDetailsM;
    string UploadDownloadM;
    string DeductionM;
    string IncentiveM;
    string ReportM;
    string DownloadClearM;
    string CompanyMasterM = "0";
    string SuperAdminCreationM = "0";
    string SalaryProcessM;
    string SessionSpay;
    string SessionEpay;
    string SessionRights;
    string SessionUser;

    //Parthi Merge End

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("/Default.aspx");
            Response.Write("Your session expired");
        }
        UserID = Session["UserId"].ToString();
        SesRoleCode = Session["RoleCode"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionRights = Session["Rights"].ToString();
        SessionUser = Session["Usernmdisplay"].ToString();
        lblUserNameDisp.Text = SessionLcode + " - " + Session["Usernmdisplay"].ToString();
        //lblCompName.Text = Session["CompName"].ToString();
        //lblCompName.Text = "Anangoor Textiles";

        if (!IsPostBack)
        {
            string SQL = "select ModuleLink from [Max_Rights]..Module_List where ModuleID='5'";
            DataTable dt = new DataTable();
            dt = objdata.RptEmployeeMultipleDetails(SQL);
            if (dt.Rows.Count > 0)
            {
               // MainPage_Link.Attributes["href"] = dt.Rows[0]["ModuleLink"].ToString();
            }
            Login_User_Photo_Load();
            if (SesRoleCode != "1")
            {
                All_Module_Disable();
                NonAdmin_User_Rights_Check();

                //Menu_Rights_Check();
                //NonAdmin_User_Rights_Check();
                //Mnu_FinYear.Visible = false;
                //Mnu_NumberSetup.Visible = false;
                //Mnu_NewUser.Visible = false;
                //Mnu_AccessRights.Visible = false;
                //All_Module_Disable();
                //Non_Admin_ModuleLink_Check();
                //NonAdmin_User_Rights_Check();
                //Mnu_User_Default_Page.Visible = false;

            }
            else
            {
                //Payroll_ModuleLink_Check();
                if (SessionUser == "Scoto")
                {
                    All_Module_Disable();
                    //NonAdmin_User_Rights_Check();
                }
                else
                {
                    All_Module_Disable();
                    Admin_User_Rights_Check();
                    //All_Module_Disable();
                    //ALL_Menu_Header_Disable();
                    //ALL_Menu_Header_Forms_Disable();
                    //Admin_User_Rights_Check();
                    //Admin_ModuleLink_Check();
                    //Admin_User_Rights_Check();
                }

            }
        }
        
    }

    private void Admin_ModuleLink_Check()
    {

        string ModuleID = Encrypt("4").ToString();
        DataTable DT_Head = new DataTable();
        //Check with Module Rights
        string query = "select distinct ML.FormLink as btnLink_ID from [" + SessionRights + "]..Module_List ML ";
        query = query + " inner join [" + SessionRights + "]..Company_Module_Rights MR on ML.ModuleName=MR.ModuleName";
        query = query + " where MR.CompCode='" + SessionCcode + "' And MR.LocCode='" + SessionLcode + "' and MR.ModuleName <> 'Production'";
        query = query + " and ML.ModuleName <> 'Production'";
        DT_Head = objdata.RptEmployeeMultipleDetails(query);
        for (int i = 0; i < DT_Head.Rows.Count; i++)
        {
            this.FindControl(DT_Head.Rows[i]["btnLink_ID"].ToString()).Visible = true;
        }

    }

    private void Non_Admin_ModuleLink_Check()
    {

        string ModuleID = Encrypt("4").ToString();
        DataTable DT_Head = new DataTable();
        //Check with Module Rights
        //string query = "select distinct MR.btnLink_ID from [" + SessionRights + "]..Company_Module_Rights MR inner join [" + SessionRights + "]..Company_Module_User_Rights MUR on MR.ModuleName=MUR.ModuleName ";
        //query = query + "where MUR.UserName='" + SessionUser + "' And MR.CompCode='" + SessionCcode + "' And MR.LocCode='" + SessionLcode + "' and MR.ModuleName <> 'Stores'";
        string query = "select distinct ML.FormLink as btnLink_ID from [" + SessionRights + "]..Module_List ML";
        query = query + " inner join [" + SessionRights + "]..Company_Module_Rights MR on ML.ModuleName=MR.ModuleName";
        query = query + " inner join [" + SessionRights + "]..Company_Module_User_Rights MUR on MR.ModuleName=MUR.ModuleName And  ML.ModuleName=MUR.ModuleName";
        query = query + " where MUR.UserName='" + SessionUser + "' And MR.CompCode='" + SessionCcode + "' And MR.LocCode='" + SessionLcode + "'";
        query = query + " and MR.ModuleName <> 'Production' and ML.ModuleName <> 'Production'";
        DT_Head = objdata.RptEmployeeMultipleDetails(query);
        for (int i = 0; i < DT_Head.Rows.Count; i++)
        {
            this.FindControl(DT_Head.Rows[i]["btnLink_ID"].ToString()).Visible = true;
        }

    }


    private void All_Module_Disable()
    {
        this.FindControl("btnSales_Link").Visible = false;
        this.FindControl("btnStores_Link").Visible = false;
        this.FindControl("btnMain_Lint").Visible = false;
        this.FindControl("btnProduction").Visible = false;
        this.FindControl("btneAlert_Link").Visible = false;
        this.FindControl("btnePayroll_Link").Visible = false;
        this.FindControl("btnQuality_Analysis_Link").Visible = false;

    }
    private void Admin_User_Rights_Check()
    {
        ALL_Menu_Header_Disable();
        ALL_Menu_Header_Forms_Disable();
        string ModuleID = Encrypt("4").ToString();
        string MenuID = "";
        DataTable DT_Head = new DataTable();
        DataTable DT_Form = new DataTable();
        //Check with Header Menu
        string query = "Select Distinct Menu_LI_ID,MenuID from [" + SessionRights + "]..Company_Module_MenuHead_Rights where ModuleID='" + ModuleID + "'";
        query = query + " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        DT_Head = objdata.RptEmployeeMultipleDetails(query);
        for (int i = 0; i < DT_Head.Rows.Count; i++)
        {
            this.FindControl(DT_Head.Rows[i]["Menu_LI_ID"].ToString()).Visible = true;

            MenuID = DT_Head.Rows[i]["MenuID"].ToString();
            query = "Select Distinct Form_LI_ID from [" + SessionRights + "]..Company_Module_Menu_Form_Rights where ModuleID='" + ModuleID + "' And MenuID='" + MenuID + "'";
            query = query + " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
            DT_Form = objdata.RptEmployeeMultipleDetails(query);
            for (int J = 0; J < DT_Form.Rows.Count; J++)
            {
                this.FindControl(DT_Form.Rows[J]["Form_LI_ID"].ToString()).Visible = true;
            }
        }
    }
    private void NonAdmin_User_Rights_Check()
    {
        ALL_Menu_Header_Disable();
        ALL_Menu_Header_Forms_Disable();
        string ModuleID = Encrypt("4").ToString();
        string MenuID = "";
        DataTable DT_Head = new DataTable();
        DataTable DT_Form = new DataTable();
        //Check with Header Menu
        string query = "Select Distinct Menu_LI_ID,MenuID from [" + SessionRights + "]..Company_Module_User_Rights where ModuleID='" + ModuleID + "' And UserName='" + SessionUser.Trim().ToString() + "'";
        query = query + " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        DT_Head = objdata.RptEmployeeMultipleDetails(query);
        for (int i = 0; i < DT_Head.Rows.Count; i++)
        {
            this.FindControl(DT_Head.Rows[i]["Menu_LI_ID"].ToString()).Visible = true;

            MenuID = DT_Head.Rows[i]["MenuID"].ToString();
            query = "Select Distinct Form_LI_ID from [" + SessionRights + "]..Company_Module_User_Rights where ModuleID='" + ModuleID + "' And MenuID='" + MenuID + "' And UserName='" + SessionUser.Trim().ToString() + "'";
            query = query + " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
            DT_Form = objdata.RptEmployeeMultipleDetails(query);
            for (int J = 0; J < DT_Form.Rows.Count; J++)
            {
                this.FindControl(DT_Form.Rows[J]["Form_LI_ID"].ToString()).Visible = true;
            }
        }
    }
    private void ALL_Menu_Header_Disable()
    {
        this.FindControl("Link_DashBoard").Visible = false;
        this.FindControl("Link_Admin_Master").Visible = false;
        this.FindControl("Link_Master").Visible = false;
        this.FindControl("Link_Transaction").Visible = false;

        this.FindControl("Link_RptStock").Visible = false;

        

    }
    private void ALL_Menu_Header_Forms_Disable()
    {
        this.FindControl("Link_DashBoard").Visible = false;

        //Admin Master
        this.FindControl("Mnu_Company").Visible = false;
        this.FindControl("Mnu_Location").Visible = false;
        this.FindControl("Mnu_NewUser").Visible = false;
        this.FindControl("Mnu_UserRights").Visible = false;
        this.FindControl("Mnu_FinYear").Visible = false;
        this.FindControl("Mnu_NumberSetup").Visible = false;

        this.FindControl("Mnu_Company_Module_Rights").Visible = false;
        this.FindControl("Mnu_Company_Module_MenuHead_Rights").Visible = false;
        this.FindControl("Mnu_Module_Form_Rights").Visible = false;
        this.FindControl("Mnu_AccessRights").Visible = false;


        //Master
        this.FindControl("Mnu_ItemMst").Visible = false;
        this.FindControl("Mnu_Supplier").Visible = false;
        this.FindControl("Mnu_Employee").Visible = false;
        this.FindControl("Mnu_GSTMst").Visible = false;

        //this.FindControl("Mnu_AgentMst").Visible = false;
        //this.FindControl("Mnu_GoDown").Visible = false;
        

        //Transacion

        this.FindControl("Mnu_Inventry").Visible = false;

        //this.FindControl("Mnu_Spares").Visible = false;
        //this.FindControl("Mnu_Carding").Visible = false;
        //this.FindControl("Mnu_CardingApproval").Visible = false;
        //this.FindControl("Mnu_Drawing").Visible = false;
        //this.FindControl("Mnu_DrawingApproval").Visible = false;
        //this.FindControl("Mnu_OE").Visible = false;
        //this.FindControl("Mnu_OEApproval").Visible = false;
        //this.FindControl("Mnu_YarnProd").Visible = false;
        //this.FindControl("Mnu_WindApproval").Visible = false;
        //this.FindControl("Mnu_Packing").Visible = false;
        //this.FindControl("Mnu_WasteDet").Visible = false;
        //this.FindControl("Mnu_EBConsumption").Visible = false;
       


        //Reports
        this.FindControl("Rpt_Inventry").Visible = false;

        //this.FindControl("Rpt_IssueEntry").Visible = false;
        //this.FindControl("Rpt_CottonStock").Visible = false;


        ////Purchase Report
        //this.FindControl("Rpt_DetailCarding").Visible = false;
        //this.FindControl("Rpt_DetailDrawing").Visible = false;
        //this.FindControl("Rpt_DetailOE").Visible = false;
        //this.FindControl("Rpt_DetailWinding").Visible = false;
        //this.FindControl("Rpt_EBConsume").Visible = false;
        




    }

    private string Encrypt(string clearText)
    {
        string EncryptionKey = "MAKV2SPBNI99212";
        byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
        using (Aes encryptor = Aes.Create())
        {
            Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
            encryptor.Key = pdb.GetBytes(32);
            encryptor.IV = pdb.GetBytes(16);
            using (MemoryStream ms = new MemoryStream())
            {
                using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                {
                    cs.Write(clearBytes, 0, clearBytes.Length);
                    cs.Close();
                }
                clearText = Convert.ToBase64String(ms.ToArray());
            }
        }
        return clearText;
    }

    private string Decrypt(string cipherText)
    {
        string EncryptionKey = "MAKV2SPBNI99212";
        byte[] cipherBytes = Convert.FromBase64String(cipherText);
        using (Aes encryptor = Aes.Create())
        {
            Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
            encryptor.Key = pdb.GetBytes(32);
            encryptor.IV = pdb.GetBytes(16);
            using (MemoryStream ms = new MemoryStream())
            {
                using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                {
                    cs.Write(cipherBytes, 0, cipherBytes.Length);
                    cs.Close();
                }
                cipherText = Encoding.Unicode.GetString(ms.ToArray());
            }
        }
        return cipherText;
    }

    private void Login_User_Photo_Load()
    {
        //string file_path = "assets/images/Login_user_photo/";
        //file_path = file_path + Session["Lcode"].ToString() + "/";
        //file_path = file_path + Session["UserId"].ToString() + ".jpg";
        ////File Check
        //if (File.Exists(Server.MapPath(file_path)))
        //{
        //    EmpPhoto.ImageUrl = (file_path);
        //}
        //else
        //{
        //    EmpPhoto.ImageUrl = "assets/images/Login_user_photo/NoImage.gif";
        //}
    }
    private void Company_Logo_Load()
    {
        //    string file_path = "assets/images/common/logo.png";
        //    //File Check
        //    if (File.Exists(Server.MapPath(file_path)))
        //    {
        //        ImgCompanyLogo.ImageUrl = (file_path);
        //    }
        //    else
        //    {
        //        ImgCompanyLogo.ImageUrl = "assets/images/common/logo.png";
        //    }
    }

    private void Menu_Rights_Check()
    {
        string query = "";
        bool Mnu_Check_Bool = false;
        bool Mnu_Check_Purchase_bool = false;
        DataTable dt = new DataTable();
        //Admin Menu Check Start
        query = "Select * from MstModuleFormUserRights where ModuleID='2' And MenuID='2' And UserID='" + UserID + "' And Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        dt = objdata.RptEmployeeMultipleDetails(query);
        if (dt.Rows.Count != 0)
        {
            Mnu_AccessRights.Visible = false;

            query = "Select MF.Link_ID,UR.FormID,UR.FormName,UR.AddRights,UR.ModifyRights,UR.DeleteRights,UR.ViewRights,";
            query = query + " UR.ApproveRights,UR.PrintRights from MstModuleFormUserRights UR";
            query = query + " inner join MstModuleFormDet MF on MF.ModuleID=UR.ModuleID And MF.MenuID=UR.MenuID And MF.FormID=UR.FormID";
            query = query + " where UR.Ccode='" + SessionCcode + "' And UR.Lcode='" + SessionLcode + "' And UR.UserID='" + UserID + "'";
            query = query + " And UR.MenuID='2' And UR.ModuleID='2' Order by UR.ModuleID,UR.MenuID,UR.FormID Asc";
            dt = objdata.RptEmployeeMultipleDetails(query);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (dt.Rows[i]["AddRights"].ToString() == "1" || dt.Rows[i]["ModifyRights"].ToString() == "1" || dt.Rows[i]["DeleteRights"].ToString() == "1" || dt.Rows[i]["ViewRights"].ToString() == "1")
                {
                    //Skip
                    Mnu_Check_Bool = true;
                }
                else
                {
                    ((HtmlGenericControl)this.FindControl(dt.Rows[i]["Link_ID"].ToString())).Visible = false;
                }
            }
            if (Mnu_Check_Bool == false)
            {
                Link_Admin_Master.Visible = false;
            }
        }
        else
        {
            Link_Admin_Master.Visible = false;
        }
        //Admin Menu Check End

        //Master Menu Check Start
        Mnu_Check_Bool = false;
        query = "Select * from MstModuleFormUserRights where ModuleID='2' And MenuID='3' And UserID='" + UserID + "' And Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        dt = objdata.RptEmployeeMultipleDetails(query);
        if (dt.Rows.Count != 0)
        {
            query = "Select MF.Link_ID,UR.FormID,UR.FormName,UR.AddRights,UR.ModifyRights,UR.DeleteRights,UR.ViewRights,";
            query = query + " UR.ApproveRights,UR.PrintRights from MstModuleFormUserRights UR";
            query = query + " inner join MstModuleFormDet MF on MF.ModuleID=UR.ModuleID And MF.MenuID=UR.MenuID And MF.FormID=UR.FormID";
            query = query + " where UR.Ccode='" + SessionCcode + "' And UR.Lcode='" + SessionLcode + "' And UR.UserID='" + UserID + "'";
            query = query + " And UR.MenuID='3' And UR.ModuleID='2' Order by UR.ModuleID,UR.MenuID,UR.FormID Asc";
            dt = objdata.RptEmployeeMultipleDetails(query);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (dt.Rows[i]["AddRights"].ToString() == "1" || dt.Rows[i]["ModifyRights"].ToString() == "1" || dt.Rows[i]["DeleteRights"].ToString() == "1" || dt.Rows[i]["ViewRights"].ToString() == "1")
                {
                    //Skip
                    Mnu_Check_Bool = true;
                }
                else
                {
                    ((HtmlGenericControl)this.FindControl(dt.Rows[i]["Link_ID"].ToString())).Visible = false;
                }
            }
            if (Mnu_Check_Bool == false)
            {
                Link_Master.Visible = false;
            }
        }
        else
        {
            Link_Master.Visible = false;
        }
        //Master Menu Check End



        //Transaction Menu Check Start
        Mnu_Check_Bool = false;
        query = "Select * from MstModuleFormUserRights where ModuleID='2' And MenuID='8' And UserID='" + UserID + "' And Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        dt = objdata.RptEmployeeMultipleDetails(query);
        if (dt.Rows.Count != 0)
        {
            query = "Select MF.Link_ID,UR.FormID,UR.FormName,UR.AddRights,UR.ModifyRights,UR.DeleteRights,UR.ViewRights,";
            query = query + " UR.ApproveRights,UR.PrintRights from MstModuleFormUserRights UR";
            query = query + " inner join MstModuleFormDet MF on MF.ModuleID=UR.ModuleID And MF.MenuID=UR.MenuID And MF.FormID=UR.FormID";
            query = query + " where UR.Ccode='" + SessionCcode + "' And UR.Lcode='" + SessionLcode + "' And UR.UserID='" + UserID + "'";
            query = query + " And UR.MenuID='8' And UR.ModuleID='2' Order by UR.ModuleID,UR.MenuID,UR.FormID Asc";
            dt = objdata.RptEmployeeMultipleDetails(query);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (dt.Rows[i]["AddRights"].ToString() == "1" || dt.Rows[i]["ModifyRights"].ToString() == "1" || dt.Rows[i]["DeleteRights"].ToString() == "1" || dt.Rows[i]["ViewRights"].ToString() == "1")
                {
                    //Skip
                    Mnu_Check_Bool = true;
                }
                else
                {
                    ((HtmlGenericControl)this.FindControl(dt.Rows[i]["Link_ID"].ToString())).Visible = false;
                }
            }
            if (Mnu_Check_Bool == false)
            {
                Link_Transaction.Visible = false;

            }
        }
        else
        {
            Link_Transaction.Visible = false;
        }
        //Transaction Menu Check End

        ////Inventory Reports Menu Check Start
        //Mnu_Check_Bool = false;
        //query = "Select * from MstModuleFormUserRights where ModuleID='2' And MenuID='9' And UserID='" + UserID + "' And Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        //dt = objdata.RptEmployeeMultipleDetails(query);
        //if (dt.Rows.Count != 0)
        //{
        //    query = "Select MF.Link_ID,UR.FormID,UR.FormName,UR.AddRights,UR.ModifyRights,UR.DeleteRights,UR.ViewRights,";
        //    query = query + " UR.ApproveRights,UR.PrintRights from MstModuleFormUserRights UR";
        //    query = query + " inner join MstModuleFormDet MF on MF.ModuleID=UR.ModuleID And MF.MenuID=UR.MenuID And MF.FormID=UR.FormID";
        //    query = query + " where UR.Ccode='" + SessionCcode + "' And UR.Lcode='" + SessionLcode + "' And UR.UserID='" + UserID + "'";
        //    query = query + " And UR.MenuID='9' And UR.ModuleID='2' Order by UR.ModuleID,UR.MenuID,UR.FormID Asc";
        //    dt = objdata.RptEmployeeMultipleDetails(query);
        //    for (int i = 0; i < dt.Rows.Count; i++)
        //    {
        //        if (dt.Rows[i]["PrintRights"].ToString() == "1")
        //        {
        //            //Skip
        //            Mnu_Check_Bool = true;
        //        }
        //        else
        //        {

        //            ((HtmlGenericControl)this.FindControl(dt.Rows[i]["Link_ID"].ToString())).Visible = false;
        //        }
        //    }
        //    if (Mnu_Check_Bool == false)
        //    {
        //        Link_RptInventory.Visible= false;
        //    }
        //}
        //else
        //{
        //    Link_RptInventory.Visible = false;
        //}
        ////Inventory Reports Menu Check End


        ////Purchase Reports Menu Check Start
        //Mnu_Check_Bool = false;
        //query = "Select * from MstModuleFormUserRights where ModuleID='2' And MenuID='10' And UserID='" + UserID + "' And Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        //dt = objdata.RptEmployeeMultipleDetails(query);
        //if (dt.Rows.Count != 0)
        //{
        //    query = "Select MF.Link_ID,UR.FormID,UR.FormName,UR.AddRights,UR.ModifyRights,UR.DeleteRights,UR.ViewRights,";
        //    query = query + " UR.ApproveRights,UR.PrintRights from MstModuleFormUserRights UR";
        //    query = query + " inner join MstModuleFormDet MF on MF.ModuleID=UR.ModuleID And MF.MenuID=UR.MenuID And MF.FormID=UR.FormID";
        //    query = query + " where UR.Ccode='" + SessionCcode + "' And UR.Lcode='" + SessionLcode + "' And UR.UserID='" + UserID + "'";
        //    query = query + " And UR.MenuID='10' And UR.ModuleID='2' Order by UR.ModuleID,UR.MenuID,UR.FormID Asc";
        //    dt = objdata.RptEmployeeMultipleDetails(query);
        //    for (int i = 0; i < dt.Rows.Count; i++)
        //    {
        //        if (dt.Rows[i]["PrintRights"].ToString() == "1")
        //        {
        //            //Skip
        //            Mnu_Check_Bool = true;
        //        }
        //        else
        //        {
        //            ((HtmlGenericControl)this.FindControl(dt.Rows[i]["Link_ID"].ToString())).Visible = false;
        //        }
        //    }
        //    if (Mnu_Check_Bool == false)
        //    {
        //        Link_RptPurchase.Visible = false;
        //    }
        //}
        //else
        //{
        //    Link_RptPurchase.Visible = false;
        //}
        ////Purchase Reports Menu Check End

        //Stock Reports Menu Check Start
        Mnu_Check_Bool = false;
        query = "Select * from MstModuleFormUserRights where ModuleID='2' And MenuID='11' And UserID='" + UserID + "' And Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        dt = objdata.RptEmployeeMultipleDetails(query);
        if (dt.Rows.Count != 0)
        {
            query = "Select MF.Link_ID,UR.FormID,UR.FormName,UR.AddRights,UR.ModifyRights,UR.DeleteRights,UR.ViewRights,";
            query = query + " UR.ApproveRights,UR.PrintRights from MstModuleFormUserRights UR";
            query = query + " inner join MstModuleFormDet MF on MF.ModuleID=UR.ModuleID And MF.MenuID=UR.MenuID And MF.FormID=UR.FormID";
            query = query + " where UR.Ccode='" + SessionCcode + "' And UR.Lcode='" + SessionLcode + "' And UR.UserID='" + UserID + "'";
            query = query + " And UR.MenuID='11' And UR.ModuleID='2' Order by UR.ModuleID,UR.MenuID,UR.FormID Asc";
            dt = objdata.RptEmployeeMultipleDetails(query);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (dt.Rows[i]["PrintRights"].ToString() == "1")
                {
                    //Skip
                    Mnu_Check_Bool = true;
                }
                else
                {
                    ((HtmlGenericControl)this.FindControl(dt.Rows[i]["Link_ID"].ToString())).Visible = false;
                }
            }
            if (Mnu_Check_Bool == false)
            {
                Link_RptStock.Visible = false;
            }
        }
        else
        {
            Link_RptStock.Visible = false;
        }
        //Stock Reports Menu Check End


    }


    protected void btnEditEnquiry_Grid_Click(object sender, EventArgs e)
    {
        string query = "";
        DataTable dt = new DataTable();
        query = "Select FormName,Link_Url from MstUser_Register where UserID='" + UserID + "'";
        query = query + " And Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        dt = objdata.RptEmployeeMultipleDetails(query);


        string formName = dt.Rows[0]["FormName"].ToString().Trim();
        string link_Url = dt.Rows[0]["Link_Url"].ToString().Trim();


        if (link_Url != "")
        {
            Response.Redirect("~/" + link_Url + "");
        }
        else
        {
            Response.Redirect("~/Dashboard.aspx");
        }
    }
    protected void btnProduction_Click(object sender, ImageClickEventArgs e)
    {
        //Get Production Link
        DataTable DT_Link = new DataTable();
        string query = "Select * from [" + SessionRights + "]..Module_List where ModuleID='4'";
        DT_Link = objdata.RptEmployeeMultipleDetails(query);
        if (DT_Link.Rows.Count != 0)
        {
            string Link_Open = DT_Link.Rows[0]["ModuleLink"].ToString();
            query = "Delete from [" + SessionRights + "]..Module_Open_User";
            objdata.RptEmployeeMultipleDetails(query);
            //Get user Password
            query = "Select * from [" + SessionRights + "]..MstUsers where UserCode='" + SessionUser.Trim() + "' and IsAdmin='" + SessionAdmin + "' And LocationCode='" + SessionLcode + "' And CompCode='" + SessionCcode + "'";
            DT_Link = objdata.RptEmployeeMultipleDetails(query);
            if (DT_Link.Rows.Count != 0)
            {
                string Password_Str = DT_Link.Rows[0]["Password"].ToString();
                //Insert User Details
                query = "Insert Into [" + SessionRights + "]..Module_Open_User(CompCode,LocCode,UserName,Password) Values";
                query = query + " ('" + SessionCcode + "','" + SessionLcode + "','" + SessionUser + "','" + Password_Str + "')";
                objdata.RptEmployeeMultipleDetails(query);
                Response.Redirect(Link_Open);
            }

        }
    }
    protected void btnStores_Link_Click(object sender, ImageClickEventArgs e)
    {
        //Get Stores

        DataTable DT_Link = new DataTable();
        string query = "Select * from [" + SessionRights + "]..Module_List where ModuleID='2'";
        DT_Link = objdata.RptEmployeeMultipleDetails(query);
        if (DT_Link.Rows.Count != 0)
        {
            string Link_Open = DT_Link.Rows[0]["ModuleLink"].ToString();
            query = "Delete from [" + SessionRights + "]..Module_Open_User";
            objdata.RptEmployeeMultipleDetails(query);
            //Get user Password
            query = "Select * from [" + SessionRights + "]..MstUsers where UserCode='" + SessionUser.Trim() + "' and IsAdmin='" + SessionAdmin + "' And LocationCode='" + SessionLcode + "' And CompCode='" + SessionCcode + "'";
            DT_Link = objdata.RptEmployeeMultipleDetails(query);
            if (DT_Link.Rows.Count != 0)
            {
                string Password_Str = DT_Link.Rows[0]["Password"].ToString();
                //Insert User Details
                query = "Insert Into [" + SessionRights + "]..Module_Open_User(CompCode,LocCode,UserName,Password) Values";
                query = query + " ('" + SessionCcode + "','" + SessionLcode + "','" + SessionUser + "','" + Password_Str + "')";
                objdata.RptEmployeeMultipleDetails(query);
                Response.Redirect(Link_Open);
            }

        }
    }
    protected void btnMain_Lint_Click(object sender, ImageClickEventArgs e)
    {
        //Get Maintenance Link
        DataTable DT_Link = new DataTable();
        string query = "Select * from [" + SessionRights + "]..Module_List where ModuleID='3'";
        DT_Link = objdata.RptEmployeeMultipleDetails(query);
        if (DT_Link.Rows.Count != 0)
        {
            string Link_Open = DT_Link.Rows[0]["ModuleLink"].ToString();
            query = "Delete from [" + SessionRights + "]..Module_Open_User";
            objdata.RptEmployeeMultipleDetails(query);
            //Get user Password
            query = "Select * from [" + SessionRights + "]..MstUsers where UserCode='" + SessionUser.Trim() + "' and IsAdmin='" + SessionAdmin + "' And LocationCode='" + SessionLcode + "' And CompCode='" + SessionCcode + "'";
            DT_Link = objdata.RptEmployeeMultipleDetails(query);
            if (DT_Link.Rows.Count != 0)
            {
                string Password_Str = DT_Link.Rows[0]["Password"].ToString();
                //Insert User Details
                query = "Insert Into [" + SessionRights + "]..Module_Open_User(CompCode,LocCode,UserName,Password) Values";
                query = query + " ('" + SessionCcode + "','" + SessionLcode + "','" + SessionUser + "','" + Password_Str + "')";
                objdata.RptEmployeeMultipleDetails(query);
                Response.Redirect(Link_Open);
            }

        }
    }

    protected void btnSales_Link_Click(object sender, ImageClickEventArgs e)
    {
        //Get Stores

        DataTable DT_Link = new DataTable();
        string query = "Select * from [" + SessionRights + "]..Module_List where ModuleID='1'";
        DT_Link = objdata.RptEmployeeMultipleDetails(query);
        if (DT_Link.Rows.Count != 0)
        {
            string Link_Open = DT_Link.Rows[0]["ModuleLink"].ToString();
            query = "Delete from [" + SessionRights + "]..Module_Open_User";
            objdata.RptEmployeeMultipleDetails(query);
            //Get user Password
            query = "Select * from [" + SessionRights + "]..MstUsers where UserCode='" + SessionUser.Trim() + "' and IsAdmin='" + SessionAdmin + "' And LocationCode='" + SessionLcode + "' And CompCode='" + SessionCcode + "'";
            DT_Link = objdata.RptEmployeeMultipleDetails(query);
            if (DT_Link.Rows.Count != 0)
            {
                string Password_Str = DT_Link.Rows[0]["Password"].ToString();
                //Insert User Details
                query = "Insert Into [" + SessionRights + "]..Module_Open_User(CompCode,LocCode,UserName,Password) Values";
                query = query + " ('" + SessionCcode + "','" + SessionLcode + "','" + SessionUser + "','" + Password_Str + "')";
                objdata.RptEmployeeMultipleDetails(query);
                Response.Redirect(Link_Open);
            }

        }

    }

    protected void btneAlert_Link_Click(object sender, ImageClickEventArgs e)
    {
        //Get Cotton Link
        DataTable DT_Link = new DataTable();
        string query = "Select * from [" + SessionRights + "]..Module_List where ModuleID='7'";
        DT_Link = objdata.RptEmployeeMultipleDetails(query);
        if (DT_Link.Rows.Count != 0)
        {
            string Link_Open = DT_Link.Rows[0]["ModuleLink"].ToString();
            query = "Delete from [" + SessionRights + "]..Module_Open_User";
            objdata.RptEmployeeMultipleDetails(query);
            //Get user Password
            query = "Select * from [" + SessionRights + "]..MstUsers where UserCode='" + SessionUser.Trim() + "' and IsAdmin='" + SessionAdmin + "' And LocationCode='" + SessionLcode + "' And CompCode='" + SessionCcode + "'";
            DT_Link = objdata.RptEmployeeMultipleDetails(query);
            if (DT_Link.Rows.Count != 0)
            {
                string Password_Str = DT_Link.Rows[0]["Password"].ToString();
                //Insert User Details
                query = "Insert Into [" + SessionRights + "]..Module_Open_User(CompCode,LocCode,UserName,Password) Values";
                query = query + " ('" + SessionCcode + "','" + SessionLcode + "','" + SessionUser + "','" + Password_Str + "')";
                objdata.RptEmployeeMultipleDetails(query);
                Response.Redirect(Link_Open);
            }

        }
    }

    protected void btnePayroll_Link_Click(object sender, ImageClickEventArgs e)
    {
        //Get Cotton Link
        DataTable DT_Link = new DataTable();
        string query = "Select * from [" + SessionRights + "]..Module_List where ModuleID='8'";
        DT_Link = objdata.RptEmployeeMultipleDetails(query);
        if (DT_Link.Rows.Count != 0)
        {
            string Link_Open = DT_Link.Rows[0]["ModuleLink"].ToString();
            query = "Delete from [" + SessionRights + "]..Module_Open_User";
            objdata.RptEmployeeMultipleDetails(query);
            //Get user Password
            query = "Select * from [" + SessionRights + "]..MstUsers where UserCode='" + SessionUser.Trim() + "' and IsAdmin='" + SessionAdmin + "' And LocationCode='" + SessionLcode + "' And CompCode='" + SessionCcode + "'";
            DT_Link = objdata.RptEmployeeMultipleDetails(query);
            if (DT_Link.Rows.Count != 0)
            {
                string Password_Str = DT_Link.Rows[0]["Password"].ToString();
                //Insert User Details
                query = "Insert Into [" + SessionRights + "]..Module_Open_User(CompCode,LocCode,UserName,Password) Values";
                query = query + " ('" + SessionCcode + "','" + SessionLcode + "','" + SessionUser + "','" + Password_Str + "')";
                objdata.RptEmployeeMultipleDetails(query);
                Response.Redirect(Link_Open);
            }

        }
    }

    protected void btnQuality_Analysis_Link_Click(object sender, ImageClickEventArgs e)
    {
        //Get Cotton Link
        DataTable DT_Link = new DataTable();
        string query = "Select * from [" + SessionRights + "]..Module_List where ModuleID='9'";
        DT_Link = objdata.RptEmployeeMultipleDetails(query);
        if (DT_Link.Rows.Count != 0)
        {
            string Link_Open = DT_Link.Rows[0]["ModuleLink"].ToString();
            query = "Delete from [" + SessionRights + "]..Module_Open_User";
            objdata.RptEmployeeMultipleDetails(query);
            //Get user Password
            query = "Select * from [" + SessionRights + "]..MstUsers where UserCode='" + SessionUser.Trim() + "' and IsAdmin='" + SessionAdmin + "' And LocationCode='" + SessionLcode + "' And CompCode='" + SessionCcode + "'";
            DT_Link = objdata.RptEmployeeMultipleDetails(query);
            if (DT_Link.Rows.Count != 0)
            {
                string Password_Str = DT_Link.Rows[0]["Password"].ToString();
                //Insert User Details
                query = "Insert Into [" + SessionRights + "]..Module_Open_User(CompCode,LocCode,UserName,Password) Values";
                query = query + " ('" + SessionCcode + "','" + SessionLcode + "','" + SessionUser + "','" + Password_Str + "')";
                objdata.RptEmployeeMultipleDetails(query);
                Response.Redirect(Link_Open);
            }

        }
    }

    protected void btnModule_Home_Click(object sender, ImageClickEventArgs e)
    {
        //Get Cotton Link
        DataTable DT_Link = new DataTable();
        string query = "Select * from [" + SessionRights + "]..Module_List where ModuleID='6'";
        DT_Link = objdata.RptEmployeeMultipleDetails(query);
        if (DT_Link.Rows.Count != 0)
        {
            string Link_Open = DT_Link.Rows[0]["ModuleLink"].ToString();
            query = "Delete from [" + SessionRights + "]..Module_Open_User";
            objdata.RptEmployeeMultipleDetails(query);
            //Get user Password
            query = "Select * from [" + SessionRights + "]..MstUsers where UserCode='" + SessionUser.Trim() + "' and IsAdmin='" + SessionAdmin + "' And LocationCode='" + SessionLcode + "' And CompCode='" + SessionCcode + "'";
            DT_Link = objdata.RptEmployeeMultipleDetails(query);
            if (DT_Link.Rows.Count != 0)
            {
                string Password_Str = DT_Link.Rows[0]["Password"].ToString();
                //Insert User Details
                query = "Insert Into [" + SessionRights + "]..Module_Open_User(CompCode,LocCode,UserName,Password) Values";
                query = query + " ('" + SessionCcode + "','" + SessionLcode + "','" + SessionUser + "','" + Password_Str + "')";
                objdata.RptEmployeeMultipleDetails(query);
                Response.Redirect(Link_Open);
            }

        }
    }
}
