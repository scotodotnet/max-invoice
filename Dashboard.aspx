﻿<%@ Page Language="C#" MasterPageFile="MainPage.master" AutoEventWireup="true" CodeFile="Dashboard.aspx.cs" Inherits="Dashboard" Title="" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<!-- Content Page Code Start Area -->

<div class="page-breadcrumb">
    <ol class="breadcrumb container">
        <%-- <li><a href="#">Home</a></li>--%>
        <li><a href="Dashboard.aspx">Dashboard</a></li>
    </ol>
</div>
<div class="page-title">
    <div class="container">
        <h3>Dashboard</h3>
    </div>
</div>
<div id="main-wrapper" class="container">
    <div class="row">
        <div class="col-sm-6"><!-- Station And Vendor Wise Purchase Start -->
            <div class="panel panel-white">
                <div class="panel-heading">
                    <h4 class="panel-title">Station And Vendor Wise Purchase</h4>  
                </div>
                <div class="panel-body">
                    <div>
                        <div class="row">
                            <div class="form-group col-md-4">
					            <label for="exampleInputName">Type</label>
					            <asp:RadioButtonList ID="RdpChartType" class="form-control" runat="server" RepeatColumns="2">
                                    <asp:ListItem Value="1" Text="Station" Selected="True"></asp:ListItem>
                                    <asp:ListItem Value="2" Text="Vendor"></asp:ListItem>
                                </asp:RadioButtonList>
					        </div>
					        <div class="form-group col-md-3">
					            <label for="exampleInputName">Month</label>
                                <asp:DropDownList ID="txtMonth" runat="server" class="form-control">
                                    <asp:ListItem Value="--Select--" Text="--Select--"></asp:ListItem>
                                    <asp:ListItem Value="January" Text="January"></asp:ListItem>
                                    <asp:ListItem Value="February" Text="February"></asp:ListItem>
                                    <asp:ListItem Value="March" Text="March"></asp:ListItem>
                                    <asp:ListItem Value="April" Text="April"></asp:ListItem>
                                    <asp:ListItem Value="May" Text="May"></asp:ListItem>
                                    <asp:ListItem Value="June" Text="June"></asp:ListItem>
                                    <asp:ListItem Value="July" Text="July"></asp:ListItem>
                                    <asp:ListItem Value="August" Text="August"></asp:ListItem>
                                    <asp:ListItem Value="September" Text="September"></asp:ListItem>
                                    <asp:ListItem Value="October" Text="October"></asp:ListItem>
                                    <asp:ListItem Value="November" Text="November"></asp:ListItem>
                                    <asp:ListItem Value="December" Text="December"></asp:ListItem>
                                </asp:DropDownList>
					        </div>
					        <div class="form-group col-md-3">
					            <label for="exampleInputName">Year</label>
                                <asp:DropDownList ID="txtYear" runat="server" class="form-control"></asp:DropDownList>
					        </div>
					        <div class="form-group col-md-2">
					            <br />
					            <asp:Button ID="btnSearch" class="btn-success" runat="server" Text="View" OnClick="btnSearch_Click"
					                Width="80" Height="40"/>
					        </div>
                        </div>                    
                        <div>
                            <asp:Chart ID="Station_Vendor_Chart" runat="server" Compression="100" 
                                EnableViewState="True" Width="490px">
                                <series>
                                    <asp:Series Name="Series1" ChartArea="ChartArea1" ChartType="Pie"  Label="#VALX: #VALY"
                                        IsXValueIndexed="True">
                                    </asp:Series>
                                </series>
                                <chartareas>
                                    <asp:ChartArea Name="ChartArea1"></asp:ChartArea>
                                </chartareas>
                            </asp:Chart>
                        </div>
                    </div>   
                </div>
            </div>
        </div><!-- col-sm-6 --><!-- Station And Vendor Wise Purchase End -->
        
        <div class="col-sm-6"><!-- Cotton Varieties Wise Issue Start -->
            <div class="panel panel-white">
                <div class="panel-heading">
                    <h4 class="panel-title">Varieties And Vendor Wise Issue</h4>  
                </div>
                <div class="panel-body">
                    <div>
                        <div class="row">
                            <div class="form-group col-md-4">
					            <label for="exampleInputName">Type</label>
					            <asp:RadioButtonList ID="RdpChartIssueType" class="form-control" runat="server" RepeatColumns="2">
                                    <asp:ListItem Value="1" Text="Variety" Selected="True"></asp:ListItem>
                                    <asp:ListItem Value="2" Text="Vendor"></asp:ListItem>
                                </asp:RadioButtonList>
					        </div>
                            <div class="form-group col-md-3">
					            <label for="exampleInputName">Month</label>
                                <asp:DropDownList ID="txtMonth_Var" runat="server" class="form-control">
                                    <asp:ListItem Value="--Select--" Text="--Select--"></asp:ListItem>
                                    <asp:ListItem Value="January" Text="January"></asp:ListItem>
                                    <asp:ListItem Value="February" Text="February"></asp:ListItem>
                                    <asp:ListItem Value="March" Text="March"></asp:ListItem>
                                    <asp:ListItem Value="April" Text="April"></asp:ListItem>
                                    <asp:ListItem Value="May" Text="May"></asp:ListItem>
                                    <asp:ListItem Value="June" Text="June"></asp:ListItem>
                                    <asp:ListItem Value="July" Text="July"></asp:ListItem>
                                    <asp:ListItem Value="August" Text="August"></asp:ListItem>
                                    <asp:ListItem Value="September" Text="September"></asp:ListItem>
                                    <asp:ListItem Value="October" Text="October"></asp:ListItem>
                                    <asp:ListItem Value="November" Text="November"></asp:ListItem>
                                    <asp:ListItem Value="December" Text="December"></asp:ListItem>
                                </asp:DropDownList>
					        </div>
					        <div class="form-group col-md-3">
					            <label for="exampleInputName">Year</label>
                                <asp:DropDownList ID="txtYear_Var" runat="server" class="form-control"></asp:DropDownList>
					        </div>
					        <div class="form-group col-md-2">
					            <br />
					            <asp:Button ID="btnVar_Search" class="btn-success" runat="server" Text="View" OnClick="btnVar_Search_Click"
					                Width="80" Height="40"/>
					        </div>
                        </div>                    
                        <div>
                            <asp:Chart ID="Varieties_Chart" runat="server" Compression="100" 
                                EnableViewState="True" Width="490px">
                                <series>
                                    <asp:Series Name="Series1" ChartArea="ChartArea1" ChartType="Pie"  Label="#VALX: #VALY"
                                        IsXValueIndexed="True">
                                    </asp:Series>
                                </series>
                                <chartareas>
                                    <asp:ChartArea Name="ChartArea1"></asp:ChartArea>
                                </chartareas>
                            </asp:Chart>
                        </div>
                    </div>   
                </div>
            </div>
        </div><!-- col-sm-6 --><!-- Cotton Varieties Wise Issue Start End -->
    </div><!-- Row -->
    
    <div class="row">
        <div class="col-sm-6"><!-- Varieties Wise Current Stock Start -->
            <div class="panel panel-white">
                <div class="panel-heading">
                    <h4 class="panel-title">Varieties Wise Current Stock</h4>  
                </div>
                <div class="panel-body">
                    <div>
                        <div>
                            <asp:Chart ID="Varieties_Current_Stock_Chart" runat="server" Compression="100" 
                                EnableViewState="True" Width="490px">
                                <series>
                                    <asp:Series Name="Series1" ChartArea="ChartArea1" ChartType="Pie"  Label="#VALX: #VALY"
                                        IsXValueIndexed="True">
                                    </asp:Series>
                                </series>
                                <chartareas>
                                    <asp:ChartArea Name="ChartArea1"></asp:ChartArea>
                                </chartareas>
                            </asp:Chart>
                        </div>
                    </div>   
                </div>
            </div>
        </div><!-- col-sm-6 --><!-- Varieties Wise Current Stock End -->
        
        <div class="col-sm-6"><!-- Agent Wise Purchase Start -->
            <div class="panel panel-white">
                <div class="panel-heading">
                    <h4 class="panel-title">Agent Wise Purchase</h4>  
                </div>
                <div class="panel-body">
                    <div>
                        <div class="row">
					        <div class="form-group col-md-3">
					            <label for="exampleInputName">Month</label>
                                <asp:DropDownList ID="txtAgentMonth" runat="server" class="form-control">
                                    <asp:ListItem Value="--Select--" Text="--Select--"></asp:ListItem>
                                    <asp:ListItem Value="January" Text="January"></asp:ListItem>
                                    <asp:ListItem Value="February" Text="February"></asp:ListItem>
                                    <asp:ListItem Value="March" Text="March"></asp:ListItem>
                                    <asp:ListItem Value="April" Text="April"></asp:ListItem>
                                    <asp:ListItem Value="May" Text="May"></asp:ListItem>
                                    <asp:ListItem Value="June" Text="June"></asp:ListItem>
                                    <asp:ListItem Value="July" Text="July"></asp:ListItem>
                                    <asp:ListItem Value="August" Text="August"></asp:ListItem>
                                    <asp:ListItem Value="September" Text="September"></asp:ListItem>
                                    <asp:ListItem Value="October" Text="October"></asp:ListItem>
                                    <asp:ListItem Value="November" Text="November"></asp:ListItem>
                                    <asp:ListItem Value="December" Text="December"></asp:ListItem>
                                </asp:DropDownList>
					        </div>
					        <div class="form-group col-md-3">
					            <label for="exampleInputName">Year</label>
                                <asp:DropDownList ID="txtAgent_Year" runat="server" class="form-control"></asp:DropDownList>
					        </div>
					        <div class="form-group col-md-2">
					            <br />
					            <asp:Button ID="btnAgentPurchaseChart" class="btn-success" runat="server" Text="View" OnClick="btnAgentPurchaseChart_Click"
					                Width="80" Height="40"/>
					        </div>
                        </div>                    
                        <div>
                            <asp:Chart ID="Agent_Purchase_Chart" runat="server" Compression="100" 
                                EnableViewState="True" Width="490px">
                                <series>
                                    <asp:Series Name="Series1" ChartArea="ChartArea1" ChartType="Pie"  Label="#VALX: #VALY"
                                        IsXValueIndexed="True">
                                    </asp:Series>
                                </series>
                                <chartareas>
                                    <asp:ChartArea Name="ChartArea1"></asp:ChartArea>
                                </chartareas>
                            </asp:Chart>
                        </div>
                    </div>   
                </div>
            </div>
        </div><!-- col-sm-6 --><!-- Agent Wise Purchase End -->
    </div><!-- Row -->
</div><!-- Main Wrapper -->
        
</asp:Content>

