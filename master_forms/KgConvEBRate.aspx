﻿<%@ Page Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="KgConvEBRate.aspx.cs" Inherits="master_forms_KgConvEBRate" Title="" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<script type="text/javascript">
    function SaveMsgAlert(msg) {
        swal(msg);
    }
</script>

<div class="page-breadcrumb">
<ol class="breadcrumb container">
 <h4><li class="active">Kg Conversion / EB Rate</li></h4> 
 </ol>
</div>

<div id="main-wrapper" class="container">
 <div class="row">
    <div class="col-md-12">
    <div class="col-md-9">
			<div class="panel panel-white">
				<div class="panel panel-primary">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">Kg Conversion / EB Rate</h4>
				</div>
				</div>
				<form class="form-horizontal">
				<div class="panel-body">
						    
			<div class="timeline-options">
			  <h4 class="panel-title">Kg Conversion</h4>
			</div>
					<div class="col-md-12">
					    <div class="row">
					        <div class="form-group col-md-6">
					            <label for="exampleInputName">Total Kg <span class="mandatory">*</span></label>
					            <asp:TextBox ID="txtTotKg" class="form-control" runat="server"></asp:TextBox>
					            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender13" runat="server" FilterType="Numbers, Custom" TargetControlID="txtTotKg" ValidChars="."/>
                                <asp:RequiredFieldValidator ControlToValidate="txtTotKg" ValidationGroup="Validate_Field" class="form_error" ID="validate1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>					            
					        </div>
					    				        
					    </div>
					</div>
					<div class="clearfix"></div>				    
			<div class="timeline-options">
			  <h4 class="panel-title">EB Rate</h4>
			</div>
					<div class="col-md-12">
					  <div class="row">
					     <div class="form-group col-md-6">
					            <label for="exampleInputName">Rate <span class="mandatory">*</span></label>
					             <asp:TextBox ID="txtEBRate" class="form-control" runat="server" ></asp:TextBox>
					            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterType="Numbers, Custom" TargetControlID="txtEBRate" ValidChars="."/>
                                <asp:RequiredFieldValidator ControlToValidate="txtEBRate" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
                            </div>
					    
					  </div>
					</div>
					
					<!-- Button start -->
					<div class="col-md-12">
					    <div class="row">					            
					        <div class="txtcenter">
					            <div class="col-sm-11">
					               <asp:Button ID="btnSave" class="btn btn-success"  runat="server" Text="Save" 
                                        ValidationGroup="Validate_Field" onclick="btnSave_Click"/>
                                   <asp:Button ID="btnCancel" class="btn btn-danger" runat="server" Text="Cancel" 
                                        onclick="btnCancel_Click" />
                                </div>
					        </div>
					    </div>
					</div>
					<!-- Button end -->
					<div class="form-group row"></div>
			
						
						
						
						
				</div><!-- panel body end -->
				</form>
			</div><!-- panel white end -->
		    </div><!-- col-9 end -->
		    
		     <!-- Dashboard start -->
		     <div class="col-lg-3 col-md-6">
                            <div class="panel panel-white" style="height: 100%;">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Dashboard Details</h4>
                                    <div class="panel-control">
                                        
                                        
                                    </div>
                                </div>
                                <div class="panel-body">
                                    
                                    
                                </div>
                            </div>
                        </div>  
                        
                        <div class="col-lg-3 col-md-6">
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div class="live-tile flip ha" data-mode="flip" data-speed="750" data-delay="3000">
                                       
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div class="live-tile flip ha" data-mode="flip" data-speed="750" data-delay="3000">
                                       
                                    </div>
                                </div>
                            </div>
                            
                        </div> 
		    <!-- Dashboard End -->
		    
		    <div class="col-md-2"></div>  
            
      
         
      </div><!-- col 12 end -->
  </div><!-- row end -->
 </div><!-- main-wrapper end -->
</asp:Content>

