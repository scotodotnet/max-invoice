﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
public partial class master_forms_ItemSub : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionSupplierCode;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "ERP Module :: Item Master";
            HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Master"));
            li.Attributes.Add("class", "droplink active open");

            //Load_RandomID();
            if (Session["ItemCode"] == null)
            {
                SessionSupplierCode = "";
            }
            else
            {

                SessionSupplierCode = Session["ItemCode"].ToString();
                txtItemCode.Text = SessionSupplierCode.ToString();
                btnSearch_Click(sender, e);
            }
        }

        //    Load_Data();
    }
    private void Load_RandomID()
    {
        string query = "";
        DataTable DT = new DataTable();
        string DeptCode = "";
        Decimal DeptCode_Dec = 0;
        query = "Select MAX(CAST(SuppCode AS int)) AS SuppCode from MstSupplier where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        if (DT.Rows.Count != 0)
        {
            DeptCode = DT.Rows[0]["SuppCode"].ToString();
            if (DeptCode != "")
            {
                DeptCode_Dec = Convert.ToDecimal(DeptCode) + 1;
            }
            else
            {
                DeptCode_Dec = 1;
            }

            txtItemCode.Text = DeptCode_Dec.ToString();

        }
        else
        {
            txtItemCode.Text = "1";
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        string query = "";
        DataTable DT = new DataTable();
        string SaveMode = "Insert";

        //User Rights Check Start
        bool ErrFlag = false;
        bool Rights_Check = false;
        if (txtItemCode.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the Item Code...');", true);
        }
        else if (txtItemName.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the Item Name...');", true);
        }
        //else
        //{
        //    Rights_Check = CommonClass_Function.AddRights_Check(SessionCcode, SessionLcode, SessionUserID, "3", "5", "Supplier Master");
        //    if (Rights_Check == false)
        //    {
        //        ErrFlag = true;
        //        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Adding New Supplier...');", true);
        //    }
        //}
        //User Rights Check End




        if (!ErrFlag)
        {
            query = "Select * from ItemMaster where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ItemCode='" + txtItemCode.Text + "'";
            DT = objdata.RptEmployeeMultipleDetails(query);
            if (DT.Rows.Count != 0)
            {
                if (btnSave.Text == "Update")
                {
                    SaveMode = "Update";
                    query = "Delete from ItemMaster where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ItemCode='" + txtItemCode.Text + "'";
                    objdata.RptEmployeeMultipleDetails(query);
                }
                else
                {
                    SaveMode = "Error";
                }
            }
            if (SaveMode != "Error")
            {
                int ActiveMode = 0;
              
                //Insert Compnay Details
                query = "Insert Into ItemMaster(Ccode,Lcode,ItemCode,ItemName,Description,ItemQty,Amount,HSNcode)";
                query = query + " Values ('" + SessionCcode + "',";
                query = query + "'" + SessionLcode + "','" + txtItemCode.Text + "','" + txtItemName.Text + "','" + txtDescription.Text + "',";
                query = query + "'"+txtqty.Text+"','" + txtAmount.Text + "','" + txtHsn.Text + "')";
                objdata.RptEmployeeMultipleDetails(query);

                if (SaveMode == "Insert")
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Item Details Saved Successfully');", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Item Details Updated Successfully');", true);
                }

                //Load_Data();
                Clear_All_Field();
                btnSave.Text = "Save";
                txtItemCode.Enabled = true;
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Item Details Already Exisits');", true);
            }
        }

        Response.Redirect("ItemMain.aspx");

    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }
    private void Clear_All_Field()
    {
        txtItemCode.Text = ""; txtItemName.Text = ""; txtDescription.Text = "";
        txtAmount.Text = ""; txtHsn.Text = ""; txtqty.Text = "";

        //txtTel_no.Text = ""; txtMobile_no.Text = ""; txttin_no.Text = ""; txtcst_no.Text = ""; txtMail_Id.Text = "";
        //txtdescription.Text = "";
        //txtStdCode.Text = ""; txtMblCode.Text = "+91";
        btnSave.Text = "Save";
        txtItemCode.Enabled = true;
        //Load_RandomID();
        //txtState.SelectedValue = "1";
        //txtCountry.SelectedValue = "1";
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select * from ItemMaster where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ItemCode='" + txtItemCode.Text + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            //txtItemCode.Text = DT.Rows[0]["SuppCode"].ToString();
            txtItemName.Text = DT.Rows[0]["ItemName"].ToString();
            //RdpActiveMode.SelectedValue = DT.Rows[0]["ActiveMode"].ToString();
            txtDescription.Text = DT.Rows[0]["Description"].ToString();
            txtqty.Text = DT.Rows[0]["ItemQty"].ToString();
            txtAmount.Text = DT.Rows[0]["Amount"].ToString();
            txtHsn.Text = DT.Rows[0]["HSNcode"].ToString();
            
            txtSup_Code_Hide.Text = "";
            txtItemCode.Enabled = false;
            btnSave.Text = "Update";
        }
        else
        {
            Clear_All_Field();
        }
    }

    //private void Load_Data()
    //{
    //    string query = "";
    //    DataTable DT = new DataTable();
    //    query = "Select SuppName,City,(Mobile_Code + '-' + MobileNo) as MobileNo_Join,TinNo,CstNo,SuppCode from MstSupplier where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
    //    DT = objdata.RptEmployeeMultipleDetails(query);
    //    Repeater1.DataSource = DT;
    //    Repeater1.DataBind();

    //}

    protected void GridEditClick(object sender, CommandEventArgs e)
    {
        txtItemCode.Text = e.CommandName.ToString();
        btnSearch_Click(sender, e);
    }
    protected void GridDeleteClick(object sender, CommandEventArgs e)
    {
        string query = "";

        //User Rights Check Start
        bool ErrFlag = false;
        bool Rights_Check = false;

        Rights_Check = CommonClass_Function.DeleteRights_Check(SessionCcode, SessionLcode, SessionUserID, "3", "5", "Supplier Master");
        if (Rights_Check == false)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Delete New Supplier..');", true);
        }
        //User Rights Check End

        if (!ErrFlag)
        {
            //DataTable DTBlanket = new DataTable();
            //query = "select Supp_Code,Supp_Name from Blanket_Purchase_Order_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' and Supp_Code='" + e.CommandName.ToString() + "'";
            //DTBlanket = objdata.RptEmployeeMultipleDetails(query);
            //if (DTBlanket.Rows.Count > 0)
            //{
            //    ErrFlag = false;
            //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Supplier Available in Blanket Purchase Order Table');", true);

            //}


            //DataTable DTGatePassIN = new DataTable();
            //query = "select Supp_Code,Supp_Name from GatePass_IN_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' and Supp_Code='" + e.CommandName.ToString() + "'";
            //DTGatePassIN = objdata.RptEmployeeMultipleDetails(query);
            //if (DTGatePassIN.Rows.Count > 0)
            //{
            //    ErrFlag = false;
            //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Supplier Available in Gate Pass In Table');", true);

            //}


            //DataTable DTGatePassOut = new DataTable();
            //query = "select Supp_Code,Supp_Name from GatePass_Out_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' and Supp_Code='" + e.CommandName.ToString() + "'";
            //DTGatePassOut = objdata.RptEmployeeMultipleDetails(query);
            //if (DTGatePassOut.Rows.Count > 0)
            //{
            //    ErrFlag = false;
            //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Supplier Available in Gate Pass Out Table');", true);

            //}


            //DataTable DTGeneralPurchaseOrder = new DataTable();
            //query = "select Supp_Code,Supp_Name from General_Purchase_Order_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' and Supp_Code='" + e.CommandName.ToString() + "'";
            //DTGeneralPurchaseOrder = objdata.RptEmployeeMultipleDetails(query);
            //if (DTGeneralPurchaseOrder.Rows.Count > 0)
            //{
            //    ErrFlag = false;
            //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Supplier Available in General Purchase Order Main');", true);

            //}


            //DataTable DTPurEnqMain = new DataTable();
            //query = "select SuppCode1,SuppCode2,SuppCode3,SuppCode4,SuppCode5 from Pur_Enq_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' and (SuppCode1='" + e.CommandName.ToString() + "' or SuppCode2='" + e.CommandName.ToString() + "' or SuppCode3='" + e.CommandName.ToString() + "' or SuppCode4='" + e.CommandName.ToString() + "' or SuppCode5='" + e.CommandName.ToString() + "')";
            //DTPurEnqMain = objdata.RptEmployeeMultipleDetails(query);
            //if (DTPurEnqMain.Rows.Count > 0)
            //{
            //    ErrFlag = false;
            //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Supplier Available in Purchase Enquiry');", true);

            //}

            //DataTable DTPurOrderReceiptMain = new DataTable();
            //query = "select Supp_Code,Supp_Name from Pur_Order_Receipt_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' and Supp_Code='" + e.CommandName.ToString() + "'";
            //DTPurOrderReceiptMain = objdata.RptEmployeeMultipleDetails(query);
            //if (DTPurOrderReceiptMain.Rows.Count > 0)
            //{
            //    ErrFlag = false;
            //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Supplier Available in Purchase Order Receipt');", true);

            //}


            //DataTable DTPurReturnMain = new DataTable();
            //query = "select Supp_Code,Supp_Name from Purc_Return_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' and Supp_Code='" + e.CommandName.ToString() + "'";
            //DTPurReturnMain = objdata.RptEmployeeMultipleDetails(query);
            //if (DTPurReturnMain.Rows.Count > 0)
            //{
            //    ErrFlag = false;
            //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Supplier Available in Purchase Return');", true);

            //}

            //DataTable DTStdPurchaseOrder = new DataTable();
            //query = "select Supp_Code,Supp_Name from Std_Purchase_Order_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' and Supp_Code='" + e.CommandName.ToString() + "'";
            //DTStdPurchaseOrder = objdata.RptEmployeeMultipleDetails(query);
            //if (DTStdPurchaseOrder.Rows.Count > 0)
            //{
            //    ErrFlag = false;
            //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Supplier Available in Standard Purchase Order');", true);

            //}




            //DataTable DTStdPurchaseOrderMain = new DataTable();
            //query = "select Supp_Code,Supp_Name from Std_Purchase_Order_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' and Supp_Code='" + e.CommandName.ToString() + "'";
            //DTStdPurchaseOrderMain = objdata.RptEmployeeMultipleDetails(query);
            //if (DTStdPurchaseOrderMain.Rows.Count > 0)
            //{
            //    ErrFlag = false;
            //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Supplier Available in Standard Purchase Order');", true);

            //}


            //DataTable DTStockLedgerAll = new DataTable();
            //query = "select Supp_Code,Supp_Name from Stock_Ledger_All where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' and Supp_Code='" + e.CommandName.ToString() + "'";
            //DTStockLedgerAll = objdata.RptEmployeeMultipleDetails(query);
            //if (DTStockLedgerAll.Rows.Count > 0)
            //{
            //    ErrFlag = false;
            //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Supplier Available in Stock Ledger');", true);

            //}


            //DataTable DTStockTransLedger = new DataTable();
            //query = "select Supp_Code,Supp_Name from Stock_Transaction_Ledger where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' and Supp_Code='" + e.CommandName.ToString() + "'";
            //DTStockTransLedger = objdata.RptEmployeeMultipleDetails(query);
            //if (DTStockTransLedger.Rows.Count > 0)
            //{
            //    ErrFlag = false;
            //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Supplier Available in Stock Trans Ledger');", true);

            //}


            //DataTable DTSuppOutMain = new DataTable();
            //query = "select SuppCode,SuppName from Supp_Qut_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' and SuppCode='" + e.CommandName.ToString() + "'";
            //DTSuppOutMain = objdata.RptEmployeeMultipleDetails(query);
            //if (DTSuppOutMain.Rows.Count > 0)
            //{
            //    ErrFlag = false;
            //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Supplier Available in Supplier Out');", true);

            //}


            //DataTable DTUnplannedReceiptMain = new DataTable();
            //query = "select Supp_Code,Supp_Name from Unplanned_Receipt_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' and Supp_Code='" + e.CommandName.ToString() + "'";
            //DTUnplannedReceiptMain = objdata.RptEmployeeMultipleDetails(query);
            //if (DTUnplannedReceiptMain.Rows.Count > 0)
            //{
            //    ErrFlag = false;
            //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Supplier Available in UnPlanned Receipt Main ');", true);

            //}

            if (ErrFlag == true)
            {
                DataTable DT = new DataTable();
                query = "Select * from MstSupplier where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And SuppCode='" + e.CommandName.ToString() + "'";
                DT = objdata.RptEmployeeMultipleDetails(query);
                if (DT.Rows.Count != 0)
                {
                    query = "Delete from MstSupplier where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And SuppCode='" + e.CommandName.ToString() + "'";
                    objdata.RptEmployeeMultipleDetails(query);
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Supplier Details Deleted Successfully');", true);
                    //Load_Data();
                    Clear_All_Field();
                }
            }
        }
    }

}
