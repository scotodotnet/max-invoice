﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="Station_Master.aspx.cs" Inherits="master_forms_Station_Master" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    
<script src='<%= ResolveUrl("../assets/js/master_list_jquery.min.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/master_list_jquery-ui.min.js") %>'></script>
<link href="<%= ResolveUrl("../assets/css/master_list_jquery-ui.css") %>" rel="stylesheet" type="text/css"/>
<link href="<%= ResolveUrl("../assets/css/Popup.css") %>" rel="stylesheet" type="text/css"/>  

  <style>
.add-on .input-group-btn > .btn {
    border-left-width: 0;
    left:-2px;
    -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
}
/* stop the glowing blue shadow */
.add-on .form-control:focus {
    -webkit-box-shadow: none; 
            box-shadow: none;
    border-color:#cccccc; 
}
</style>

  
  

<script src='<%= ResolveUrl("../assets/js/master_list_jquery.min.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/master_list_jquery-ui.min.js") %>'></script>
<link href="<%= ResolveUrl("../assets/css/master_list_jquery-ui.css") %>" rel="stylesheet" type="text/css"/>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function(sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    $('#TblStationMaster').dataTable();
                }
            });
        };
</script>

     <script>
         $(document).ready(function() {
         $('#TblStationMaster').dataTable();
         });
    </script>


    <script type="text/javascript">
     function SaveMsgAlert(msg)
     {
        swal(msg);
     }
</script>
 

   
 <form class="form-horizontal">
  <asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
                <div class="page-breadcrumb">
                    <ol class="breadcrumb container">
                   <h4><li class="active">Station Master</li></h4> 
                    </ol>
                </div>

<div id="main-wrapper" class="container">
 <div class="row">
    <div class="col-md-12">
        <div class="col-md-9">
			<div class="panel panel-white">
			
			 <div class="panel panel-primary">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">Station Master</h4>
				</div>
				</div>
				
		       <div class="panel-body">
		       
		             <div class="col-md-12" >
		                    <div class="form-group col-md-3" align="center">
                                 <asp:Button ID="btnAdd" runat="server" Text="Add New Station" class="btn btn-success" OnClick="btnAdd_Click"/>
                            </div>
                      </div> 
		       
		       
		     
		            <div class="col-md-12" >
                                   
                                      <cc1:ModalPopupExtender ID="MdpSatationMaster" runat="server" PopupControlID="pnlPopup" TargetControlID="btnAdd"
                                         CancelControlID="" BackgroundCssClass="modalBackground">
                                      </cc1:ModalPopupExtender>
                                      <asp:Panel ID="pnlPopup" runat="server" CssClass="modalPopup" Style="display: none">
                                         <div class="header">
                                            Station New Entry
                                          </div>
                                          
                                          <div class="body" align="center">
                                            <div class="col-md-12 headsize">
                                                 <div class="form-group col-md-6" align="center">
				                                     <label for="exampleInputName">Station Code<span class="mandatory">*</span></label>
                                                       <asp:TextBox ID="txtStationCode" Enabled="false" runat="server" CssClass="form-control" Style="text-transform: uppercase"></asp:TextBox>
                                                       <asp:HiddenField ID="HiddenStationCode" runat="server" />
                                                       <asp:RequiredFieldValidator ControlToValidate="txtStationCode" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator2" runat="server" ErrorMessage="This field is required.">
                                                       </asp:RequiredFieldValidator>
                				                      <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Numbers, LowercaseLetters, UppercaseLetters, Custom"
                                                           ValidChars=", " TargetControlID="txtStationCode" />		
				                                   </div>
				                                  
				                                   <div class="form-group col-md-6" align="center">
				                                             <label for="exampleInputName">Station Name<span class="mandatory" Style="text-transform: uppercase">*</span></label>
                                                               <asp:TextBox ID="txtStationName" runat="server" CssClass="form-control"></asp:TextBox>
                                                               <asp:HiddenField ID="HiddentxtStationName" runat="server" />
                                                               <asp:RequiredFieldValidator ControlToValidate="txtStationName" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator3" runat="server" ErrorMessage="This field is required.">
                                                               </asp:RequiredFieldValidator>
                				                      <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterType="Numbers, LowercaseLetters, UppercaseLetters, Custom"
                                                           ValidChars=", " TargetControlID="txtStationName" />		
				                                   </div>
                                            </div>
                                          </div>
                                          <div class="footer" align="center">
                                              <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" ValidationGroup="Validate_Field" class="btn btn-success"   />
                                              <asp:Button ID="btncancel" runat="server" Text="Cancel" class="btn btn-danger" OnClick="btnCancel_Click"/>
                                          </div>
                                      </asp:Panel>
                                   
                                </div>
		            
		                
					
                    <div class="col-md-12">
                        <div class="row">
                            <asp:Repeater ID="RptStationMaster" runat="server" EnableViewState="false">
                                <HeaderTemplate>
                                    <table id="TblStationMaster" class="display table">
                                        <thead>
                                            <tr>
                                              <th>Station Code</th>
                                              <th>Station Name</th>
                                              <th>Mode</th>
                                            </tr>
                                        </thead>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <%--<tbody>--%>
                                    <tr>
                                        <td><%# Eval("StationID")%></td>
                                        <td><%# Eval("StationName")%></td>
                                       
                                        <td>
                                               <asp:LinkButton ID="btnEditGrid" class="btn btn-success btn-sm fa fa-pencil"  runat="server" 
                                                     Text="" OnCommand="GridEditClick" CommandArgument="Edit" CommandName='<%# Eval("StationID")%>'>
                                                </asp:LinkButton>
                                                <asp:LinkButton ID="btnDeleteGrid" class="btn btn-danger btn-sm fa fa-trash-o"  runat="server" 
                                                     Text="" OnCommand="GridDeleteClick" CommandArgument="Delete" CommandName='<%# Eval("StationID")%>' 
                                                     CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this station details?');">
                                                </asp:LinkButton>
                                        </td>
                                    </tr>
                                    <%--</tbody>--%>
                                </ItemTemplate>
                                <FooterTemplate></table></FooterTemplate>
                             </asp:Repeater>
                         </div>
                     </div>
             
				</div><!-- panel body end -->
				
			</div><!-- panel white end -->
		    </div><!-- col-9 end -->
		    
		    <!-- Dashboard start -->
		      <div class="col-lg-3 col-md-6">
                            <div class="panel panel-white" style="height: 100%;">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Dashboard Details</h4>
                                    <div class="panel-control">
                                        
                                        
                                    </div>
                                </div>
                                <div class="panel-body">
                                    
                                    
                                </div>
                            </div>
                        </div>  
                        
             <div class="col-lg-3 col-md-6">
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div class="live-tile flip ha" data-mode="flip" data-speed="750" data-delay="3000">
                                       
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div class="live-tile flip ha" data-mode="flip" data-speed="750" data-delay="3000">
                                       
                                    </div>
                                </div>
                            </div>
                            
                        </div> 
		    <!-- Dashboard End -->
		    
		    <div class="col-md-2"></div>
		    
    </div><!-- col 12 end -->
  </div><!-- row end -->
 </div><!-- main-wrapper end -->
</ContentTemplate>
</asp:UpdatePanel>
</form> 
</asp:Content>
