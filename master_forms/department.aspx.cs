﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


using Altius.BusinessAccessLayer.BALDataAccess;

public partial class master_forms_department : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string Dept_Code_Delete = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        if (!IsPostBack)
        {
            Page.Title = "ERP Module :: Department";
            HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Master"));
            li.Attributes.Add("class", "droplink active open");
            Load_Data();
            Load_RandomID();
        }
       
        Load_Data();
        
        //GVDepartment.UseAccessibleHeader = true;

        ////adds <thead> and <tbody> elements

        //GVDepartment.HeaderRow.TableSection = TableRowSection.TableHeader;

    }

    private void Load_RandomID()
    {
        string query = "";
        DataTable DT = new DataTable();
        string DeptCode = "";
        Decimal DeptCode_Dec = 0;
        query = "Select MAX(CAST(DeptCode AS int)) AS DeptCode from MstDepartment where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        if (DT.Rows.Count != 0)
        {
            DeptCode = DT.Rows[0]["DeptCode"].ToString();
            if (DeptCode != "")
            {
                DeptCode_Dec = Convert.ToDecimal(DeptCode) + 1;
            }
            else
            {
                DeptCode_Dec = 1;
            }

            txtDeptCode.Text = DeptCode_Dec.ToString();

        }
        else
        {
            txtDeptCode.Text = "1";
        }
    }


    protected void btnSave_Click(object sender, EventArgs e)
    {
        string query = "";
        DataTable DT = new DataTable();
        string SaveMode = "Insert";
        //User Rights Check Start
        bool ErrFlag = false;
        bool Rights_Check = false;
        //if (btnSave.Text == "Update")
        //{
        //    Rights_Check = CommonClass_Function.ModifyRights_Check(SessionCcode, SessionLcode, SessionUserID, "3", "2", "Department");
        //    if (Rights_Check == false)
        //    {
        //        ErrFlag = true;
        //        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Modify Department Details...');", true);
        //    }
        //}
        //else
        //{
        //    Rights_Check = CommonClass_Function.AddRights_Check(SessionCcode, SessionLcode, SessionUserID, "3", "2", "Department");
        //    if (Rights_Check == false)
        //    {
        //        ErrFlag = true;
        //        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Adding New Department...');", true);
        //    }
        //}
        //User Rights Check End
        if (!ErrFlag)
        {
            query = "Select * from MstDepartment where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And DeptCode='" + txtDeptCode.Text + "' Or DeptName='" + txtDeptName.Text + "'";
            DT = objdata.RptEmployeeMultipleDetails(query);
            if (DT.Rows.Count != 0)
            {
                if (btnSave.Text == "Update")
                {
                    SaveMode = "Update";
                    query = "Delete from MstDepartment where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And DeptCode='" + txtDeptCode.Text + "'";
                    objdata.RptEmployeeMultipleDetails(query);
                }
                else
                {
                    SaveMode = "Error";
                }
            }

            if (SaveMode != "Error")
            {
                //Insert Compnay Details
                query = "Insert Into MstDepartment(Ccode,Lcode,DeptCode,DeptName,Dept_Desc,UserID,Username)";
                query = query + " Values('" + SessionCcode + "','" + SessionLcode + "','" + txtDeptCode.Text + "',";
                query = query + "'" + txtDeptName.Text + "','" + txtNatureWork.Text + "','" + SessionUserID + "','" + SessionUserName + "')";
                objdata.RptEmployeeMultipleDetails(query);

                if (SaveMode == "Insert")
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Department Details Saved Successfully');", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Department Details Updated Successfully');", true);
                }


                Clear_All_Field();
                btnSave.Text = "Save";
                //txtDeptCode.Enabled = true;
                Load_Data();
                
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Department Details Already Saved');", true);
            }
        }

    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }
    private void Clear_All_Field()
    {
        txtDeptCode.Text = ""; txtDeptName.Text = ""; txtNatureWork.Text = "";
        btnSave.Text = "Save";
        Load_RandomID();
        //txtDeptCode.Enabled = true;
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select * from MstDepartment where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And DeptCode='" + txtDeptCode.Text + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            txtDeptName.Text = DT.Rows[0]["DeptName"].ToString();
            txtNatureWork.Text = DT.Rows[0]["Dept_Desc"].ToString();
            txtDept_Code_Hide.Text = "";
            //txtDeptCode.Enabled = false;
            btnSave.Text = "Update";
        }
        else
        {
            Clear_All_Field();
        }
    }

    protected void GRID_PreRender(object sender, EventArgs e)
    {
        //string query = "";
        //DataTable DT = new DataTable();
        //query = "Select * from MstDepartment where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        //DT = objdata.RptEmployeeMultipleDetails(query);
        //GVDepartment.DataSource = DT;
        //GVDepartment.DataBind();

        //if (GVDepartment.Rows.Count > 0)
        //{
        //    //This replaces <td> with <th> and adds the scope attribute
        //    GVDepartment.UseAccessibleHeader = true;

        //    //This will add the <thead> and <tbody> elements
        //    GVDepartment.HeaderRow.TableSection = TableRowSection.TableHeader;

        //    //This adds the <tfoot> element. 
        //    //Remove if you don't have a footer row
        //    GVDepartment.FooterRow.TableSection = TableRowSection.TableFooter;
        //}
        //base.OnPreRender(e);

        ////MakeAccessible(GVDepartment);

    }

    private void Load_Data()
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select * from MstDepartment where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        Repeater1.DataSource = DT;
        Repeater1.DataBind();
        //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "dataTable_Reload();", true);
        //GVDepartment.DataSource = DT;
        //GVDepartment.DataBind();

    }

    public static void MakeAccessible(GridView grid)
    {
        if (grid.Rows.Count <= 0) return;
        grid.UseAccessibleHeader = true;
        grid.HeaderRow.TableSection = TableRowSection.TableHeader;
        if (grid.ShowFooter)
            grid.FooterRow.TableSection = TableRowSection.TableFooter;
    }
    protected void Repeater1_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "Save")
        {
            txtDeptName.Text = "asas";
        }
    }

    protected void GridEditClick(object sender, CommandEventArgs e)
    {
        txtDeptCode.Text = e.CommandName.ToString();
        btnSearch_Click(sender, e);
    }
    protected void GridDeleteClick(object sender, CommandEventArgs e)
    {
        string query = "";
        
        //User Rights Check Start
        bool ErrFlag = false;
        bool Rights_Check = false;

        Rights_Check = CommonClass_Function.DeleteRights_Check(SessionCcode, SessionLcode, SessionUserID, "3", "2", "Department");
        if (Rights_Check == false)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Delete New Department..');", true);
        }
        //User Rights Check End

        if (!ErrFlag)
        {
            DataTable dtdcheckGeneral = new DataTable();
            DataTable dtdcheckBlanket = new DataTable();
            DataTable dtdcheckStandard = new DataTable();
            query = "select DeptCode,DeptName from General_Purchase_Order_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' and DeptCode='" + e.CommandName.ToString() + "'";
            dtdcheckGeneral = objdata.RptEmployeeMultipleDetails(query);

            query = "select DeptCode,DeptName from Blanket_Purchase_Order_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' and DeptCode='" + e.CommandName.ToString() + "'";
            dtdcheckBlanket = objdata.RptEmployeeMultipleDetails(query);

            query = "select DeptCode,DeptName from Std_Purchase_Order_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' and DeptCode='" + e.CommandName.ToString() + "'";
            dtdcheckStandard = objdata.RptEmployeeMultipleDetails(query);

            if ((dtdcheckGeneral.Rows.Count == 0) && (dtdcheckBlanket.Rows.Count == 0) && (dtdcheckStandard.Rows.Count == 0))
            {
                DataTable DT = new DataTable();
                query = "Select * from MstDepartment where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And DeptCode='" + e.CommandName.ToString() + "'";
                DT = objdata.RptEmployeeMultipleDetails(query);
                if (DT.Rows.Count != 0)
                {
                    query = "Delete from MstDepartment where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And DeptCode='" + e.CommandName.ToString() + "'";
                    objdata.RptEmployeeMultipleDetails(query);
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Department Details Deleted Successfully');", true);
                    Load_Data();
                    Clear_All_Field();

                }
            }
            else
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Sorry Department Available in some other table..');", true);
            }
        }
    }

}
