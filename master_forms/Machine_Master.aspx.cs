﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class master_forms_Machine_Master : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string Dept_Code_Delete = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        if (!IsPostBack)
        {
            LoadUnit();
            Load_Dept();
            Load_Count();
            Page.Title = "ERP Module :: Machine Master";

            HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Master"));
            li.Attributes.Add("class", "droplink active open");
            //Load_Department();
            Load_RandomID();
        }
       
        Load_Data();
    }
    private void Load_Dept()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlDepartment.Items.Clear();
        query = "Select * from MstDepartment where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlDepartment.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["DeptName"] = "-Select-";
        dr["DeptCode"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlDepartment.DataTextField = "DeptName";
        ddlDepartment.DataValueField = "DeptCode";
        ddlDepartment.DataBind();
    }
    public void LoadUnit()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlUnit.Items.Clear();
        query = "Select *from MstUnit where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlUnit.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["UnitName"] = "-Select-";
        dr["UnitName"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlUnit.DataTextField = "UnitName";
        ddlUnit.DataValueField = "UnitName";
        ddlUnit.DataBind();
    }

    private void Load_Data()
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select * from MstMachine where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        Repeater1.DataSource = DT;
        Repeater1.DataBind();
    }
    private void Load_Count()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlCount.Items.Clear();
        query = "Select * from MstCount where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlCount.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["CountName"] = "-Select-";
        dr["CountID"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlCount.DataTextField = "CountName";
        ddlCount.DataValueField = "CountID";
        ddlCount.DataBind();
    }

    private void Load_Department()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlDepartment.Items.Clear();
        query = "Select *from MstDepartment where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlDepartment.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["DeptName"] = "-Select-";
        dr["DeptCode"] = "0";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlDepartment.DataTextField = "DeptName";
        ddlDepartment.DataValueField = "DeptCode";
        ddlDepartment.DataBind();
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        string query = "";
        DataTable DT = new DataTable();
        string SaveMode = "Insert";
        //User Rights Check Start
        bool ErrFlag = false;
        bool Rights_Check = false;
        //if (btnSave.Text == "Update")
        //{
        //    Rights_Check = CommonClass_Function.ModifyRights_Check(SessionCcode, SessionLcode, SessionUserID, "3", "2", "Item Master");
        //    if (Rights_Check == false)
        //    {
        //        ErrFlag = true;
        //        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Modify Item Details...');", true);
        //    }
        //}
        //else
        //{
        //    Rights_Check = CommonClass_Function.AddRights_Check(SessionCcode, SessionLcode, SessionUserID, "3", "2", "Item Master");
        //    if (Rights_Check == false)
        //    {
        //        ErrFlag = true;
        //        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Adding New Item...');", true);
        //    }
        //}
        //User Rights Check End


        if (ddlDepartment.SelectedItem.Text == "OE")
        {
            Final_Calculate();
        }

        if (!ErrFlag)
        {
            query = "Select * from MstMachine where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And MachineCode='" + txtMachineID.Text + "'";
            DT = objdata.RptEmployeeMultipleDetails(query);
            if (DT.Rows.Count != 0)
            {
                if (btnSave.Text == "Update")
                {
                    SaveMode = "Update";
                    query = "Delete from MstMachine where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And MachineCode='" + txtMachineID.Text + "'";
                    objdata.RptEmployeeMultipleDetails(query);
                }
                else
                {
                    SaveMode = "Error";
                }
            }
            if (SaveMode != "Error")
            {
                

                //Insert Compnay Details
                query = "Insert Into MstMachine(Ccode,Lcode,MachineCode,MachineName,DeptCode,DeptName,";
                query = query + " Make,Model,ProdTarget,Speed,StdHank,EBUnitTarget,Description,Unit,";
                query = query + " UserID,UserName,WorkRoter,Counts,Side,MainCount,TPI,Eff) Values('" + SessionCcode + "',";
                query = query + " '" + SessionLcode + "','" + txtMachineID.Text + "','" + txtMachineName.Text + "',";
                query = query + " '" + ddlDepartment.SelectedValue + "','" + ddlDepartment.SelectedItem.Text + "',";
                query = query + " '" + txtMake.Text + "','" + txtModel.Text + "','" + txtProdTraget.Text + "',";
                query = query + "'" + txtSpeed.Text + "','" + txtStdHank.Text + "','" + txtEBUnitTarget.Text + "',";
                query = query + " '" + txtDesc.Text + "','" + ddlUnit.SelectedValue + "','" + SessionUserID + "','" + SessionUserName + "','" + txtWorkRoter.Text + "',";
                query = query + " '" + ddlCount.SelectedValue + "','" + ddlSide.SelectedItem.Text + "','" + txtMainCount.Text + "','" + txtTPI.Text + "','" + txtEff.Text + "')";
                objdata.RptEmployeeMultipleDetails(query);

                if (SaveMode == "Insert")
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Machine Master Details Saved Successfully');", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Machine Master Details Updated Successfully');", true);
                }

                Load_Data();
                Clear_All_Field();
                btnSave.Text = "Save";
                //txtitem_code.Enabled = true;
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Machine Master Details Already Saved Successfully');", true);
            }
        }
    }

    protected void GridEditClick(object sender, CommandEventArgs e)
    {
        txtMachineID.Text = e.CommandName.ToString();
        btnSearch_Click(sender, e);
    }

    protected void GridDeleteClick(object sender, CommandEventArgs e)
    {
        string query = "";

        //User Rights Check Start
        bool ErrFlag = false;
        bool Rights_Check = false;

        //Rights_Check = CommonClass_Function.DeleteRights_Check(SessionCcode, SessionLcode, SessionUserID, "3", "2", "Item Master");
        //if (Rights_Check == false)
        //{
        //    ErrFlag = true;
        //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Delete New Item..');", true);
        //}
        //User Rights Check End

                DataTable DT = new DataTable();
                query = "Select * from MstMachine where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And MachineCode='" + e.CommandName.ToString() + "'";
                DT = objdata.RptEmployeeMultipleDetails(query);
                if (DT.Rows.Count != 0)
                {
                    query = "Delete from MstMachine where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And MachineCode='" + e.CommandName.ToString() + "'";
                    objdata.RptEmployeeMultipleDetails(query);
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Machine Details Deleted Successfully');", true);
                    Load_Data();
                    Clear_All_Field();
                }
                else
                {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Sorry Check Machine Details..');", true);

                }
       
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }

    private void Load_RandomID()
    {
        string query = "";
        DataTable DT = new DataTable();
        string ItemCode = "";
        Decimal ItemCode_Dec = 0;
        query = "Select MAX(CAST(MachineCode AS int)) AS MachineCode from MstMachine where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        if (DT.Rows.Count != 0)
        {
            ItemCode = DT.Rows[0]["MachineCode"].ToString();
            if (ItemCode != "")
            {
                ItemCode_Dec = Convert.ToDecimal(ItemCode) + 1;
            }
            else
            {
                ItemCode_Dec = 1;
            }

            txtMachineID.Text = ItemCode_Dec.ToString();

        }
        else
        {
            txtMachineID.Text = "1";
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select * from MstMachine where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And MachineCode='" + txtMachineID.Text + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            ddlDepartment.SelectedValue = DT.Rows[0]["DeptCode"].ToString();
            txtMachineName.Text = DT.Rows[0]["MachineName"].ToString();
            txtMake.Text = DT.Rows[0]["Make"].ToString();
            txtModel.Text = DT.Rows[0]["Model"].ToString();
            txtProdTraget.Text = DT.Rows[0]["ProdTarget"].ToString();
            txtDesc.Text = DT.Rows[0]["Description"].ToString();
            txtSpeed.Text = DT.Rows[0]["Speed"].ToString();
            txtStdHank.Text = DT.Rows[0]["StdHank"].ToString();
            txtEBUnitTarget.Text = DT.Rows[0]["EBUnitTarget"].ToString();
            ddlUnit.SelectedValue = DT.Rows[0]["Unit"].ToString().ToString();
            txtWorkRoter.Text = DT.Rows[0]["WorkRoter"].ToString();
            ddlCount.SelectedValue = DT.Rows[0]["Counts"].ToString();
            ddlSide.SelectedItem.Text = DT.Rows[0]["Side"].ToString();

            txtMainCount.Text = DT.Rows[0]["MainCount"].ToString();
            txtTPI.Text = DT.Rows[0]["TPI"].ToString();
            txtEff.Text = DT.Rows[0]["Eff"].ToString();

            //txtitem_code.Enabled = false;
            btnSave.Text = "Update";
        }
        else
        {
            Clear_All_Field();
        }
    }

    private void Clear_All_Field()
    {
        txtMachineID.Text = "";
        txtMachineName.Text = "";
        ddlSide.SelectedValue = "0";
        txtMake.Text = "";
        txtModel.Text = "";
        txtProdTraget.Text = "";
        txtDesc.Text = "";
        txtSpeed.Text = "";
        txtStdHank.Text = "";
        txtWorkRoter.Text = "";
        txtEBUnitTarget.Text = "";

        txtMainCount.Text = "";
        txtTPI.Text = "";
        txtEff.Text = "";

        btnSave.Text = "Save";
        Load_RandomID();
        LoadUnit();
        Load_Dept();
        Load_Count();
        //Load_Department();
    }

    protected void ddlDepartment_SelectedIndexChanged(object sender, EventArgs e)
    {
        if ((ddlDepartment.SelectedItem.Text == "WINDING DEPT") || (ddlDepartment.SelectedItem.Text == "CONE WINDING") || (ddlDepartment.SelectedItem.Text == "WINDING"))
        {
            ddlUnit.Enabled = false;
        }
        else
        {
            ddlUnit.Enabled = true;
        }
    }


    public void Final_Calculate()
    {
        string Final_Amt = "0";
        string Tot_Amt = "0";
        string MainCount = "0";
        string TPI = "0";
        string Eff = "0";
        string Speed = "0";
        string WorkedRouter = "0";


        if (txtMainCount.Text != "0" || txtMainCount.Text != "0.0" || txtMainCount.Text!="")
        {
            MainCount = txtMainCount.Text;
        }

        if (txtTPI.Text != "0" || txtTPI.Text != "0.0" || txtTPI.Text != "")
        {
            TPI = txtTPI.Text;
        }

        if (txtEff.Text != "0" || txtEff.Text != "0.0" || txtEff.Text != "")
        {
            Eff = txtEff.Text;
        }

        if (txtSpeed.Text != "0" || txtSpeed.Text != "0.0" || txtSpeed.Text != "")
        {
            Speed = txtSpeed.Text;
        }

        if (txtWorkRoter.Text != "0" || txtWorkRoter.Text != "0.0" || txtWorkRoter.Text != "")
        {
            WorkedRouter = txtWorkRoter.Text;
        }

        Tot_Amt = ((Convert.ToDecimal(Speed) * Convert.ToDecimal(60) * Convert.ToDecimal(8) * Convert.ToDecimal(Eff)) / (Convert.ToDecimal(TPI) * Convert.ToDecimal(36) * Convert.ToDecimal(840) * Convert.ToDecimal(MainCount) * Convert.ToDecimal(2.2046) * Convert.ToDecimal(100))).ToString();
        Final_Amt = (Convert.ToDecimal(Tot_Amt) * Convert.ToDecimal(WorkedRouter)).ToString();
        Final_Amt = Math.Round(Convert.ToDecimal(Final_Amt), 2).ToString();

        txtProdTraget.Text = Final_Amt.ToString();

    }
}
