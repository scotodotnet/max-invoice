﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


using Altius.BusinessAccessLayer.BALDataAccess;

public partial class master_forms_CustomerMst : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;


    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");

        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        if (!IsPostBack)
        {
            Load_RandomID();
            Page.Title = "ERP Module :: Customer Master";
            HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Master"));
            li.Attributes.Add("class", "droplink active open");
        }
        
        Load_Data();
    }
    private void Load_RandomID()
    {
        string query = "";
        DataTable DT = new DataTable();
        string DeptCode = "";
        Decimal DeptCode_Dec = 0;
        query = "Select MAX(CAST(CustomerID AS int)) AS CustomerID from MstCustomer where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        if (DT.Rows.Count != 0)
        {
            DeptCode = DT.Rows[0]["CustomerID"].ToString();
            if (DeptCode != "")
            {
                DeptCode_Dec = Convert.ToDecimal(DeptCode) + 1;
            }
            else
            {
                DeptCode_Dec = 1;
            }

            txtCustomerID.Text = DeptCode_Dec.ToString();

        }
        else
        {
            txtCustomerID.Text = "1";
        }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        string query = "";
        DataTable DT = new DataTable();
        string SaveMode = "Insert";

        //User Rights Check Start
        bool ErrFlag = false;
        bool Rights_Check = true;
        if (btnSave.Text == "Update")
        {
            //Rights_Check = CommonClass_Function.ModifyRights_Check(SessionCcode, SessionLcode, SessionUserID, "3", "5", "Customer");
            //if (Rights_Check == false)
            //{
            //    ErrFlag = true;
            //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Modify Customer Details...');", true);
            //}
        }
        else
        {
            //Rights_Check = CommonClass_Function.AddRights_Check(SessionCcode, SessionLcode, SessionUserID, "3", "5", "Customer");
            //if (Rights_Check == false)
            //{
            //    ErrFlag = true;
            //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Adding New Customer...');", true);
            //}
        }
        //User Rights Check End




        if (!ErrFlag)
        {
            query = "Select * from MstCustomer where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And CustomerID='" + txtCustomerID.Text + "'";
            DT = objdata.RptEmployeeMultipleDetails(query);
            if (DT.Rows.Count != 0)
            {
                if (btnSave.Text == "Update")
                {
                    SaveMode = "Update";
                    query = "Delete from MstCustomer where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And CustomerID='" + txtCustomerID.Text + "'";
                    objdata.RptEmployeeMultipleDetails(query);
                }
                else
                {
                    SaveMode = "Error";
                }
            }
            if (SaveMode != "Error")
            {
                int ActiveMode = 0;
                if (RdpActiveMode.SelectedValue == "1")
                {
                    ActiveMode = 1;
                }
                else
                {
                    ActiveMode = 2;
                }
                //Insert Compnay Details
                query = "Insert Into MstCustomer(Ccode,Lcode,CustomerID,CustomerName,ActiveMode,Address1,Address2,City,Pincode,State,";
                query = query + " Country,Std_Code,TelNo,Mobile_Code,MobileNo,TinNo,CstNo,MailID,Description,UserID,UserName) Values ('" + SessionCcode + "',";
                query = query + " '" + SessionLcode + "','" + txtCustomerID.Text.ToUpper() + "','" + txtCustomerName.Text + "','" + ActiveMode + "',";
                query = query + "'" + txtAddress1.Text + "','" + txtAddress2.Text + "','" + txtCity.Text + "','" + txtPincode.Text + "',";
                query = query + "'" + txtState.Text + "','" + txtCountry.Text + "','" + txtStdCode.Text + "','" + txtTel_no.Text + "','" + txtMblCode.Text + "','" + txtMobile_no.Text + "',";
                query = query + "'" + txttin_no.Text + "','" + txtcst_no.Text + "','" + txtMail_Id.Text + "','" + txtdescription.Text + "',";
                query = query + "'" + SessionUserID + "','" + SessionUserName + "')";
                objdata.RptEmployeeMultipleDetails(query);

                if (SaveMode == "Insert")
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Customer Details Saved Successfully');", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Customer Details Updated Successfully');", true);
                }

                Load_Data();
                Clear_All_Field();
                btnSave.Text = "Save";

                txtCustomerID.Enabled = false;
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Customer Details Already Exisits');", true);
            }
        }

    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }
    private void Clear_All_Field()
    {
        txtCustomerID.Text = ""; txtCustomerName.Text = ""; RdpActiveMode.SelectedValue = "1"; txtAddress1.Text = "";
        txtAddress2.Text = ""; txtCity.Text = ""; txtPincode.Text = ""; txtState.Text = ""; txtCountry.Text = "";
        txtTel_no.Text = ""; txtMobile_no.Text = ""; txttin_no.Text = ""; txtcst_no.Text = ""; txtMail_Id.Text = "";
        txtdescription.Text = "";
        txtStdCode.Text = ""; txtMblCode.Text = "+91";
        btnSave.Text = "Save";
        //txtCustomerID.Enabled = false;
        Load_RandomID();
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select * from MstCustomer where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And CustomerID='" + txtCustomerID.Text + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            //txtSupplierCode.Text = DT.Rows[0]["SuppCode"].ToString();
            txtCustomerName.Text = DT.Rows[0]["CustomerName"].ToString();
            RdpActiveMode.SelectedValue = DT.Rows[0]["ActiveMode"].ToString();
            txtAddress1.Text = DT.Rows[0]["Address1"].ToString();
            txtAddress2.Text = DT.Rows[0]["Address2"].ToString();
            txtCity.Text = DT.Rows[0]["City"].ToString();
            txtPincode.Text = DT.Rows[0]["Pincode"].ToString();
            txtState.Text = DT.Rows[0]["State"].ToString();
            txtCountry.Text = DT.Rows[0]["Country"].ToString();
            txtStdCode.Text = DT.Rows[0]["Std_Code"].ToString();
            txtTel_no.Text = DT.Rows[0]["TelNo"].ToString();
            txtMblCode.Text = DT.Rows[0]["Mobile_Code"].ToString();
            txtMobile_no.Text = DT.Rows[0]["MobileNo"].ToString();
            txttin_no.Text = DT.Rows[0]["TinNo"].ToString();
            txtcst_no.Text = DT.Rows[0]["CstNo"].ToString();
            txtMail_Id.Text = DT.Rows[0]["MailID"].ToString();
            txtdescription.Text = DT.Rows[0]["Description"].ToString();



            txtCustomerID.Enabled = false;
            btnSave.Text = "Update";
        }
        else
        {
            Clear_All_Field();
        }
    }

    private void Load_Data()
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select CustomerName,City,(Mobile_Code + '-' + MobileNo) as MobileNo_Join,TinNo,CstNo,CustomerID from MstCustomer where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        Repeater1.DataSource = DT;
        Repeater1.DataBind();

    }

    protected void GridEditClick(object sender, CommandEventArgs e)
    {
        txtCustomerID.Text = e.CommandName.ToString();
        btnSearch_Click(sender, e);
    }
    protected void GridDeleteClick(object sender, CommandEventArgs e)
    {
        string query = "";

        //User Rights Check Start
        bool ErrFlag = false;
        bool Rights_Check = false;

        Rights_Check = CommonClass_Function.DeleteRights_Check(SessionCcode, SessionLcode, SessionUserID, "3", "5", "Customer");
        if (Rights_Check == false)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Delete Customer..');", true);
        }
        //User Rights Check End

        if (!ErrFlag)
        {
           
                DataTable DT = new DataTable();
                query = "Select * from MstCustomer where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And CustomerID='" + e.CommandName.ToString() + "'";
                DT = objdata.RptEmployeeMultipleDetails(query);
                if (DT.Rows.Count != 0)
                {
                    query = "Delete from MstCustomer where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And CustomerID='" + e.CommandName.ToString() + "'";
                    objdata.RptEmployeeMultipleDetails(query);
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Customer Details Deleted Successfully');", true);
                    Load_Data();
                    Clear_All_Field();
                }
            
        }
    }

}
