﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class master_forms_Station_Master : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string Dept_Code_Delete = "";

   
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        if (!IsPostBack)
        {
            Load_RandomID();
            Page.Title = "ERP Module :: Station Master";
           
            HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Master"));
            li.Attributes.Add("class", "droplink active open");
            Load_Data();
        }
        
        Load_Data();
    }
    private void Load_RandomID()
    {
        string query = "";
        DataTable DT = new DataTable();
        string DeptCode = "";
        Decimal DeptCode_Dec = 0;
        query = "Select MAX(CAST(StationID AS int)) AS StationID from MstStation where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        if (DT.Rows.Count != 0)
        {
            DeptCode = DT.Rows[0]["StationID"].ToString();
            if (DeptCode != "")
            {
                DeptCode_Dec = Convert.ToDecimal(DeptCode) + 1;
            }
            else
            {
                DeptCode_Dec = 1;
            }

            txtStationCode.Text = DeptCode_Dec.ToString();

        }
        else
        {
            txtStationCode.Text = "1";
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        string query = "";
        DataTable DT = new DataTable();
        string SaveMode = "Insert";
        bool ErrFlag = false;

        //User Rights Check Start

        bool Rights_Check = false;
        //if (btnSave.Text == "Update")
        //{
        //    Rights_Check = CommonClass_Function.ModifyRights_Check(SessionCcode, SessionLcode, SessionUserID, "3", "5", "Station Master");
        //    if (Rights_Check == false)
        //    {
        //        ErrFlag = true;
        //        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Modify Station Master...');", true);
        //    }
        //}
        //else
        //{
        //    Rights_Check = CommonClass_Function.AddRights_Check(SessionCcode, SessionLcode, SessionUserID, "3", "5", "Station Master");
        //    if (Rights_Check == false)
        //    {
        //        ErrFlag = true;
        //        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Adding New Station Master...');", true);
        //    }
        //}
        //User Rights Check End




        if (!ErrFlag)
        {
           query = "Select * from MstStation where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And StationID='" + txtStationCode.Text + "'";
           DT = objdata.RptEmployeeMultipleDetails(query);

           if (DT.Rows.Count != 0)
           {
               if (btnSave.Text == "Update")
               {
                   SaveMode = "Update";
                   query = "Delete from MstStation where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And StationID='" + txtStationCode.Text + "'";
                   objdata.RptEmployeeMultipleDetails(query);
               }

               else
               {
                   SaveMode = "Error";
               }
           }
           
            if (SaveMode != "Error")
            {
               
                //Insert Station Details
                query = "Insert Into MstStation(Ccode,Lcode,StationID,StationName,UserID,UserName)Values ('" + SessionCcode + "',";
                query = query + " '" + SessionLcode + "','" + txtStationCode.Text.ToUpper() + "','" + txtStationName.Text.ToUpper() + "',";
                query = query + "'" + SessionUserID + "','" + SessionUserName + "')";
                objdata.RptEmployeeMultipleDetails(query);

                if (SaveMode == "Insert")
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Station Master Saved Successfully');", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Station Master Updated Successfully');", true);
                }

                Load_Data();
                Clear_All_Field();
                btnSave.Text = "Save";

            
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Station Details Already Exisits');", true);
            }
        }


    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }
    private void Clear_All_Field()
    {
        txtStationName.Text = "";
        txtStationCode.Text = "";
        HiddenStationCode.Value = "";
        HiddentxtStationName.Value = "";
        Load_RandomID();

    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select * from MstStation where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And StationID='" + txtStationCode.Text + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            //txtSupplierCode.Text = DT.Rows[0]["SuppCode"].ToString();
            txtStationCode.Text = DT.Rows[0]["StationID"].ToString();
            txtStationName.Text = DT.Rows[0]["StationName"].ToString();
            btnSave.Text = "Update";
        }
        else
        {
            Clear_All_Field();
        }
    }

 
    private void Load_Data()
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select StationID,StationName from MstStation where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        RptStationMaster.DataSource = DT;
        RptStationMaster.DataBind();
    }

    public static void MakeAccessible(GridView grid)
    {

    }
    protected void RptStationMaster_ItemCommand(object source, RepeaterCommandEventArgs e)
    {

    }

    protected void GridEditClick(object sender, CommandEventArgs e)
    {
        MdpSatationMaster.Show();
   
        txtStationCode.Text = e.CommandName.ToString();
        btnSearch_Click(sender, e);
    }

    protected void btnAdd_Click(object sender, EventArgs e)
    {
        txtStationCode.Text = "";
        txtStationName.Text = "";
        MdpSatationMaster.Show();
    }


    protected void GridDeleteClick(object sender, CommandEventArgs e)
    {
        string query = "";

        //User Rights Check Start
        bool ErrFlag = false;
        bool Rights_Check = false;

        Rights_Check = CommonClass_Function.DeleteRights_Check(SessionCcode, SessionLcode, SessionUserID, "3", "5", "Station Master");
        if (Rights_Check == false)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Delete Station Master..');", true);
        }
        //User Rights Check End

        if (!ErrFlag)
        {

            DataTable DT = new DataTable();
            query = "Select * from MstStation where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And StationID='" + e.CommandName.ToString() + "'";
            DT = objdata.RptEmployeeMultipleDetails(query);
            if (DT.Rows.Count != 0)
            {
                query = "Delete from MstStation where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And StationID='" + e.CommandName.ToString() + "'";
                objdata.RptEmployeeMultipleDetails(query);
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Station Details Deleted Successfully');", true);
                Load_Data();
                Clear_All_Field();
            }

        }

    }

}