﻿<%@ Page Title="" Language="C#" MasterPageFile="../MainPage.master" AutoEventWireup="true" CodeFile="transporter.aspx.cs" Inherits="master_forms_transporter" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<script src='<%= ResolveUrl("../assets/js/master_list_jquery.min.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/master_list_jquery-ui.min.js") %>'></script>
<link href="<%= ResolveUrl("../assets/css/master_list_jquery-ui.css") %>" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#example').dataTable();
                $('.js-states').select2();      
            }
        });
    };
</script>

<script type="text/javascript">
    function SaveMsgAlert(msg) {
        swal(msg);
    }
</script>

<form class="form-horizontal">
<div class="page-breadcrumb">
    <ol class="breadcrumb container">
        <h4><li class="active">Transporter</li></h4> 
    </ol>
</div>

<div id="main-wrapper" class="container">
 <div class="row">
    <div class="col-md-12">
        <div class="col-md-9">
			<div class="panel panel-white">
				<div class="panel panel-primary">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">Main</h4>
				</div>
				</div>
				
				<div class="panel-body">
				
					<div class="col-md-12">
					    <div class="row">
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">Transporter Code <span class="mandatory">*</span></label>
					            <asp:TextBox ID="txtTransporterCode" MaxLength="30" Enabled="false" class="form-control" runat="server" Style="text-transform: uppercase"></asp:TextBox>
                                <asp:TextBox ID="txtTransporter_Code_Hide" class="form-control" runat="server" Visible="false"></asp:TextBox>
                                <asp:RequiredFieldValidator ControlToValidate="txtTransporterCode" ValidationGroup="Validate_Field" class="form_error" ID="validate1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
                                
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterType="Numbers, LowercaseLetters, UppercaseLetters, Custom"
                                ValidChars=", " TargetControlID="txtTransporterCode" />		
					        </div>
					        <%--<asp:LinkButton ID="btnSearch" class="btn btn-primary btn-addon  btn-rounded btn-sm" runat="server" OnClick="btnSearch_Click"><i class="fa fa-search"></i>Search</asp:LinkButton>--%>
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">Active Mode</label>
                                <asp:RadioButtonList ID="RdpActiveMode" class="form-control" runat="server" RepeatColumns="2" >
                                <asp:ListItem Value="1" Text="Own" style="padding-right:40px" Selected="True"></asp:ListItem>
                                <asp:ListItem Value="2" Text="Private" ></asp:ListItem>
                                </asp:RadioButtonList>
					        </div>
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">Transporter Name <span class="mandatory">*</span></label>
							    <asp:TextBox ID="txtTransporterName" class="form-control" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ControlToValidate="txtTransporterName" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
                                
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterType="Numbers, LowercaseLetters, UppercaseLetters, Custom"
                                 ValidChars=", " TargetControlID="txtTransporterName" />
					        </div>
					        
					      </div>
					 </div>
					        
					        
					  <div class="col-md-12">
					    <div class="row">
					          <div class="form-group col-md-4">
					            <label for="exampleInputName">Lorry No<span class="mandatory">*</span></label>
					            <asp:TextBox ID="txtLorryNo" MaxLength="30" class="form-control" runat="server" Style="text-transform: uppercase"></asp:TextBox>
                                <asp:TextBox ID="txtLorry_No_Hide" class="form-control" runat="server" Visible="false"></asp:TextBox>
                                <asp:RequiredFieldValidator ControlToValidate="txtLorryNo" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator9" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>	
                                
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" FilterType="Numbers, LowercaseLetters, UppercaseLetters, Custom"
                                ValidChars=", " TargetControlID="txtLorryNo" />
                                				            
					        </div>
					  
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">Address 1 <span class="mandatory">*</span></label>
	                            <asp:TextBox ID="txtAddress1" class="form-control" runat="server"></asp:TextBox>
	                            <asp:RequiredFieldValidator ControlToValidate="txtAddress1" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator2" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>    
                                
                                 <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" FilterType="Numbers, LowercaseLetters, UppercaseLetters, Custom"
                                 ValidChars=", " TargetControlID="txtAddress1" />
                                                            
					        </div>
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">Address 2</label>
					            <asp:TextBox ID="txtAddress2" class="form-control" runat="server"></asp:TextBox>
					            
					            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server" FilterType="Numbers, LowercaseLetters, UppercaseLetters, Custom"
                                 ValidChars=", " TargetControlID="txtAddress2" />
					        </div>
					        
					        </div>
					   </div>
					   
					   <div class="col-md-12">
					    <div class="row">     
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">City <span class="mandatory">*</span></label>
					            <asp:TextBox ID="txtCity" class="form-control" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ControlToValidate="txtCity" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator3" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
					        </div>
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">Pincode <span class="mandatory">*</span></label>
                                <asp:TextBox ID="txtPincode" class="form-control" MaxLength="6" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ControlToValidate="txtPincode" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator4" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="RegExValPin" ValidationExpression="[0-9]{6}" runat="server" class="form_error" 
                                    ControlToValidate="txtPincode" ValidationGroup="Validate_Field" ErrorMessage="Pincode length 6">
                                </asp:RegularExpressionValidator>
                                <cc1:FilteredTextBoxExtender ID="FilTxtPin1" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtPincode" ValidChars="0123456789">
                                </cc1:FilteredTextBoxExtender>
					        </div>
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">State <span class="mandatory">*</span></label>
                                <asp:DropDownList ID="txtState" runat="server" class="form-control js-states">
                                <asp:ListItem Value="1" Text="Tamil Nadu"></asp:ListItem>
                                </asp:DropDownList>
					        </div>
					        
					        </div>
					     </div>
					        
					   <div class="col-md-12">
					    <div class="row">  
					        <div class="form-group col-md-4">					            
							    <label for="exampleInputName">Country <span class="mandatory">*</span></label>
                                <asp:DropDownList ID="txtCountry" runat="server" class="form-control js-states">
                                <asp:ListItem Value="1" Text="India"></asp:ListItem>
                                </asp:DropDownList>
					        </div>
					        
					  
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">Tel No.</label>
					            <div class="form-group row">
                                    <div class="col-sm-4">
							            <asp:TextBox ID="txtStdCode" MaxLength="5" class="form-control" runat="server"></asp:TextBox>
							            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                            TargetControlID="txtStdCode" ValidChars="0123456789">
                                        </cc1:FilteredTextBoxExtender>
							        </div>
							        <div class="col-sm-8">
                                        <asp:TextBox ID="txtTel_no" MaxLength="7" class="form-control" runat="server"></asp:TextBox>
                                        <asp:RegularExpressionValidator ID="RegExpValdTelNo" Display="Dynamic"  ValidationExpression="[0-9]{7}" runat="server" class="form_error" 
                                            ControlToValidate="txtTel_no" ValidationGroup="Validate_Field" ErrorMessage="Tel.No length 7">
                                        </asp:RegularExpressionValidator>
                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server"  FilterMode="ValidChars" FilterType="Custom,Numbers"
                                            TargetControlID="txtTel_no" ValidChars="0123456789">
                                        </cc1:FilteredTextBoxExtender>
                                    </div>
                                </div>
							</div>
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">Mobile No <span class="mandatory">*</span></label>
					            <div class="form-group row">
					                <div class="col-sm-4">
							            <asp:TextBox ID="txtMblCode" Text="+91" class="form-control" runat="server" ReadOnly="true"></asp:TextBox>
							        </div>
							        <div class="col-sm-8">
                                        <asp:TextBox ID="txtMobile_no" MaxLength="10" class="form-control" runat="server"></asp:TextBox>
                                        <asp:RequiredFieldValidator  ControlToValidate="txtMobile_no" Display="Dynamic"  ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator5" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                        </asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" Display="Dynamic" ValidationExpression="[0-9]{10}" runat="server" class="form_error" 
                                            ControlToValidate="txtMobile_no" ValidationGroup="Validate_Field" ErrorMessage="Mobile.No length 10">
                                        </asp:RegularExpressionValidator>
                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                            TargetControlID="txtMobile_no" ValidChars="0123456789">
                                        </cc1:FilteredTextBoxExtender>
                                    </div>
                                </div>
					        </div>
					   </div>
					</div>
					
					<div class="col-md-12">
					    <div class="row">
					        
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">TIN No <span class="mandatory">*</span></label>
                                <asp:TextBox ID="txttin_no" MaxLength="11" class="form-control"  runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ControlToValidate="txttin_no" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator6" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="RegExp1" runat="server" ErrorMessage="TIN NO length 11" class="form_error" 
                                    ControlToValidate="txttin_no" ValidationExpression="[0-9]{11}" ValidationGroup="Validate_Field"/>
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR11" runat="server" FilterMode="ValidChars"
                                    FilterType="Custom,Numbers" TargetControlID="txttin_no" ValidChars="0123456789">
                                </cc1:FilteredTextBoxExtender>
					        </div>
					  
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">CST No <span class="mandatory">*</span></label>
					            <asp:TextBox ID="txtcst_no" class="form-control"  runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ControlToValidate="txtcst_no" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator7" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
                                
                                 <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender9" runat="server" FilterType="Numbers, LowercaseLetters, UppercaseLetters, Custom"
                                ValidChars=", " TargetControlID="txtcst_no" />	
                                
					        </div>
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">Mail ID <span class="mandatory">*</span></label>
                                <asp:TextBox ID="txtMail_Id" class="form-control" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ControlToValidate="txtMail_Id" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator8" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" class="form_error" runat="server" ControlToValidate="txtMail_Id" 
                                    Display ="Dynamic" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" 
                                    ValidationGroup="Validate_Field" ErrorMessage="enter valid email address. Eg. Something@domain.com">
                                </asp:RegularExpressionValidator>
					        </div>
					        
					          </div>
					</div>
					
					<div class="col-md-12">
					    <div class="row">
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">Description</label>
                                <asp:TextBox ID="txtdescription" runat="server" TextMode="MultiLine" Height="60" class="col-sm-12 form-control"></asp:TextBox>
					        </div>
					    </div>
					</div>
					
					<!-- Button start -->
					<div class="col-md-12">
					    <div class="row">
					        <div class="txtcenter">
                                <div class="col-sm-11">
                                    <asp:Button ID="btnSave" class="btn btn-success"  runat="server" Text="Save" ValidationGroup="Validate_Field" OnClick="btnSave_Click"/>
                                    <%--<asp:Button ID="Button1" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg" runat="server" Text="View" />--%>
                                    <asp:Button ID="btnCancel" class="btn btn-danger" runat="server" Text="Cancel" OnClick="btnCancel_Click"/>
                                </div>
                            </div>
					    </div>
					</div>                            
                    <!-- Button End -->
                    <div class="form-group row"></div>
                    
                    <!-- table start -->
                    	
                    <!-- table End --> 
						
				</div><!-- panel body end -->
				
			</div><!-- panel white end -->
		    </div><!-- col-9 end -->
		    
		    <!-- Dashboard start -->
		     <div class="col-lg-3 col-md-6">
                            <div class="panel panel-white" style="height: 100%;">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Dashboard Details</h4>
                                    <div class="panel-control">
                                     </div>
                                </div>
                                <div class="panel-body">
                               </div>
                            </div>
                        </div>  
                        
                        <div class="col-lg-3 col-md-6">
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div class="live-tile flip ha" data-mode="flip" data-speed="750" data-delay="3000">
                                       
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div class="live-tile flip ha" data-mode="flip" data-speed="750" data-delay="3000">
                                       
                                    </div>
                                </div>
                            </div>
                            
                        </div> 
		    <!-- Dashboard End -->
		    
		    <div class="col-md-2"></div> 	    
            
      
         
      </div><!-- col 12 end -->
  </div><!-- row end -->
 </div><!-- main-wrapper end -->
 </form>
</asp:Content>

