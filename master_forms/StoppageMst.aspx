﻿<%@ Page Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="StoppageMst.aspx.cs" Inherits="master_forms_StoppageMst" Title="" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<script src='<%= ResolveUrl("../assets/js/master_list_jquery.min.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/master_list_jquery-ui.min.js") %>'></script>
<link href="<%= ResolveUrl("../assets/css/master_list_jquery-ui.css") %>" rel="stylesheet" type="text/css"/>
<link href="<%= ResolveUrl("assets/css/custom.css") %>" rel="stylesheet" type="text/css"/>
    
<script type="text/javascript">
    //On Page Load
//    $(function() {
//        $('.gvv').dataTable();
//    });

    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#example').dataTable();               
            }
        });
    };

//    if (prm != null) {
//        prm.add_endRequest(function(sender, e) {
//            if (sender._postBackSettings.panelsToUpdate != null) {
//                $('.gvv').dataTable();
//            }
//        });
//    };

</script>


<script type="text/javascript">
    function SaveMsgAlert(msg) {
        swal(msg);
        
    }
    
</script>
<%--Code Select List Script Saart--%>
    
    <script type="text/javascript">
         $(document).ready(function () {
            initializer();
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(initializer);
        });
        function initializer() {
            $("#<%=txtDeptCode.ClientID %>").autocomplete({
                 
                 
                source: function (request, response) {
                
                        
                      $.ajax({
 
                          url: '<%=ResolveUrl("../List_Service.asmx/GetDept_Det") %>',
                          data: "{ 'prefix': '" + request.term + "'}",
                          dataType: "json",
                          type: "POST",
                          dataFilter: function (data) { return data; },
                          contentType: "application/json; charset=utf-8",
                          delay:0,
                          success: function (data) {
 
                              response($.map(data.d, function (item) {
 
                                  return {
                                      label: item.split('-')[0],
                                      val: item.split('-')[1],
                                      
                                  }
 
                              }))
 
                          },
 
                          error: function (response) {
                              alert(response.responseText);
                          },
                          failure: function (response) {
                              alert(response.responseText);
                          }
                      });
                  }, minLength: 1,
                  focus: function (e, i) {
                      $("#<%=hfCompanyCode.ClientID %>").val(i.item.val);
                      //$("#<%=txtDept_Code_Hide.ClientID%>").val('s');
                      
                      return false;
                  },
                  select: function (e, i) {
                  //alert($.trim($(i.item).val()));
                  
                      //$("#<%=txtDeptCode.ClientID %>").val(i.item.label);
                      $("#<%=txtDept_Code_Hide.ClientID %>").val(i.item.label);
                      
                      $("#<%=hfCompanyCode.ClientID %>").val(i.item.val);
                      //__doPostBack("txtDeptCode", "TextChanged");
                      //DoPostBack();    
                      
 
                  }
                  
 
              });
 
          }
      </script>
<%--Code Select List Script Saart--%>

<%--<script type="text/javascript">
    $(document).ready(function() {
        $("#GRID").prepend($("<thead></thead>").append($(this).find("tr:first"))).dataTable();
        $("#GRID").dataTable();
    });
</script>--%>


<form class="form-horizontal">
  <asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
<div class="page-breadcrumb">
                    <ol class="breadcrumb container">
                   <h4><li class="active">Stoppage</li></h4> 
                    </ol>
                </div>

<div id="main-wrapper" class="container">
 <div class="row">
    <div class="col-md-12">
        <div class="col-md-9">
			<div class="panel panel-white">
			<div class="panel panel-primary">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">Stoppage</h4>
				</div>
				</div>
				
				<div class="panel-body">
					    <div class="col-md-12">
					        <div class="row">
					            <div class="form-group col-md-6">
							        <label for="exampleInputName">Stoppage Code<span class="mandatory">*</span></label>
							        <asp:Label ID="txtDeptCode" class="form-control" runat="server" Style="text-transform: uppercase" BackColor="#F3F3F3"></asp:Label>
                                    <asp:TextBox ID="txtDept_Code_Hide" class="form-control" runat="server" 
                                        Visible="false"></asp:TextBox>
                                    <asp:HiddenField ID="hfCompanyCode" runat="server" />
                                    <%--<asp:RequiredFieldValidator ControlToValidate="txtDeptCode" ValidationGroup="Validate_Field" class="form_error" ID="validate1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                    </asp:RequiredFieldValidator>
                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" FilterType="Numbers, UppercaseLetters, LowercaseLetters, Custom" TargetControlID="txtDeptCode" ValidChars=" "/>--%>
													   
                                </div>
                                <div class="form-group col-md-6">
							        <label for="exampleInputName">Stoppage Reason<span class="mandatory">*</span></label>
							        <asp:TextBox ID="txtDeptName" class="form-control" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ControlToValidate="txtDeptName" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                    </asp:RequiredFieldValidator>
                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Numbers, UppercaseLetters, LowercaseLetters, Custom" TargetControlID="txtDeptName" ValidChars=" "/>
						
						        </div>
						     
					        </div>
					        
					    </div>
						
                    <!-- Button start -->
                    <div class="col-md-12">
                        <div class="row">
                            <div class="txtcenter">
                                <asp:Button ID="btnSave" class="btn btn-success"  runat="server" Text="Save" ValidationGroup="Validate_Field" OnClick="btnSave_Click"/>
                                <%--<asp:Button ID="Button1" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg" runat="server" Text="View" />--%>
                                <asp:Button ID="btnCancel" class="btn btn-danger" runat="server" Text="Cancel" OnClick="btnCancel_Click"/>
                            </div>
                        </div>
                    </div>                    
                     <!-- Button end -->   
                     <br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><%--<br /><br /><br /><br />--%>
                     <div class="col-md-12">
                        <div class="row">
                            <asp:Repeater ID="Repeater1" runat="server" OnItemCommand="Repeater1_ItemCommand" EnableViewState="false">
                                <HeaderTemplate>
                                    <table id="example" class="display table">
                                        <thead>
                                            <tr>
                                            <th>Stoppage Code</th>
                                            <th>Stoppage Reason</th>
                                            
                                            <th>Mode</th>
                                            </tr>
                                        </thead>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <%--<tbody>--%>
                                    <tr>
                                        <td><%# Eval("StopppageCode")%></td>
                                        <td><%# Eval("StopppageResone")%></td>
                                       
                                        <td>
                                            <asp:LinkButton ID="btnEditGrid" class="btn btn-success btn-sm fa fa-pencil"  runat="server" 
                                                Text="" OnCommand="GridEditClick" CommandArgument="Edit" CommandName='<%# Eval("StopppageCode")%>'>
                                                </asp:LinkButton>
                                                <asp:LinkButton ID="btnDeleteGrid" class="btn btn-danger btn-sm fa fa-trash-o"  runat="server" 
                                                Text="" OnCommand="GridDeleteClick" CommandArgument="Delete" CommandName='<%# Eval("StopppageCode")%>' 
                                                CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this department details?');">
                                                </asp:LinkButton>
                                        </td>
                                    </tr>
                                    <%--</tbody>--%>
                                </ItemTemplate>
                                <FooterTemplate></table></FooterTemplate>
                             </asp:Repeater>
                             
                             
                         </div>
                     </div>
                     
                     
                         <%--<div>
					            <asp:GridView id="GVDepartment" runat="server" AutoGenerateColumns="false" 
					            ClientIDMode="Static" OnPreRender="GRID_PreRender" 
					            class="gvv display table">
					                <Columns>
					                    <asp:BoundField DataField="DeptCode" HeaderText="Dept Code" />
					                    <asp:BoundField DataField="DeptName" HeaderText="Name" />
					                    <asp:BoundField DataField="Dept_Desc" HeaderText="Description" />					            
					                </Columns>
					            </asp:GridView>
					    </div>--%>


				</div><!-- panel body end -->
				
			</div><!-- panel white end -->
		    </div><!-- col-9 end -->
		    
		    <!-- Dashboard start -->
		     <div class="col-lg-3 col-md-6">
                            <div class="panel panel-white" style="height: 100%;">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Dashboard Details</h4>
                                    <div class="panel-control">
                                        
                                        
                                    </div>
                                </div>
                                <div class="panel-body">
                                    
                                    
                                </div>
                            </div>
                        </div>  
                        
                        <div class="col-lg-3 col-md-6">
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div class="live-tile flip ha" data-mode="flip" data-speed="750" data-delay="3000">
                                       
                                    </div>
                                </div>
                            </div>
                            
                        </div> 
		    <!-- Dashboard End -->
		    
		    <div class="col-md-2"></div>
		    
            
      
         
  </div><!-- col 12 end -->
  </div><!-- row end -->
 </div><!-- main-wrapper end -->
</ContentTemplate>
</asp:UpdatePanel>
</form> 
</asp:Content>

