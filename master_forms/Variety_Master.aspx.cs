﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class master_forms_Variety_Master : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string Dept_Code_Delete = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        if (!IsPostBack)
        {
            LoadColor();
            Page.Title = "ERP Module :: Variety Master";
           
            HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Master"));
            li.Attributes.Add("class", "droplink active open");
            Load_Data();
            Load_RandomID();
        }
        
        Load_Data();
    }
    private void Load_RandomID()
    {
        string query = "";
        DataTable DT = new DataTable();
        string DeptCode = "";
        Decimal DeptCode_Dec = 0;
        query = "Select MAX(CAST(VarietyID AS int)) AS VarietyID from MstVariety where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        if (DT.Rows.Count != 0)
        {
            DeptCode = DT.Rows[0]["VarietyID"].ToString();
            if (DeptCode != "")
            {
                DeptCode_Dec = Convert.ToDecimal(DeptCode) + 1;
            }
            else
            {
                DeptCode_Dec = 1;
            }

            txtVarietyCode.Text = DeptCode_Dec.ToString();

        }
        else
        {
            txtVarietyCode.Text = "1";
        }
    }
    public void LoadColor()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlColor.Items.Clear();
        query = "Select * from MstColor where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlColor.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["ColorName"] = "-Select-";
        dr["ColorName"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlColor.DataTextField = "ColorName";
        ddlColor.DataValueField = "ColorName";
        ddlColor.DataBind();
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        string query = "";
        DataTable DT = new DataTable();
        string SaveMode = "Insert";
        bool ErrFlag = false;

       // User Rights Check Start
        
        bool Rights_Check = false;
        //if (btnSave.Text == "Update")
        //{
        //    Rights_Check = CommonClass_Function.ModifyRights_Check(SessionCcode, SessionLcode, SessionUserID, "3", "5", "Variety Master");
        //    if (Rights_Check == false)
        //    {
        //        ErrFlag = true;
        //        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Modify Veriety Master Details...');", true);
        //    }
        //}
        //else
        //{
        //    Rights_Check = CommonClass_Function.AddRights_Check(SessionCcode, SessionLcode, SessionUserID, "3", "5", "Variety Master");
        //    if (Rights_Check == false)
        //    {
        //        ErrFlag = true;
        //        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Adding New Veriety Master...');", true);
        //    }
        //}
        //User Rights Check End




        if (!ErrFlag)
        {
           query = "Select * from MstVariety where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And VarietyID='" + txtVarietyCode.Text + "'";
           DT = objdata.RptEmployeeMultipleDetails(query);
           
            if (DT.Rows.Count != 0)
            {
                if (btnSave.Text == "Update")
                {
                    SaveMode = "Update";
                    query = "Delete from MstVariety where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And VarietyID='" + txtVarietyCode.Text + "'";
                    objdata.RptEmployeeMultipleDetails(query);
                }
                else
                {
                    SaveMode = "Error";
                }
            }
            if (SaveMode != "Error")
            {
               
                //Insert Variety Details
                query = "Insert Into MstVariety(Ccode,Lcode,VarietyID,VarietyName,RatePerCandy,ColorName,UserID,UserName)Values ('" + SessionCcode + "',";
                query = query + " '" + SessionLcode + "','" + txtVarietyCode.Text.ToUpper() + "','" + txtVarietyName.Text.ToUpper() + "',";
                query = query + "'" + txtRatePerCandy.Text + "','" + ddlColor.SelectedValue + "','" + SessionUserID + "','" + SessionUserName + "')";
                objdata.RptEmployeeMultipleDetails(query);

                if (SaveMode == "Insert")
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Variety Master Saved Successfully');", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Variety Master Updated Successfully');", true);
                }

                Load_Data();
                Clear_All_Field();
                btnSave.Text = "Save";
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Variety Details Already Exisits');", true);
            }
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }
    private void Clear_All_Field()
    {
        txtVarietyName.Text = "";
        txtVarietyCode.Text = "";
        HiddenVarietyCode.Value = "";
        HiddenVarietyName.Value = "";
        //btnAdd.Text = "Save";
        btnSave.Text = "Save";
        txtRatePerCandy.Text = "";
        Load_RandomID();
        LoadColor();
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select * from MstVariety where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And VarietyID='" + txtVarietyCode.Text + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            //txtSupplierCode.Text = DT.Rows[0]["SuppCode"].ToString();
            txtVarietyCode.Text = DT.Rows[0]["VarietyID"].ToString();
            txtVarietyName.Text = DT.Rows[0]["VarietyName"].ToString();
            txtRatePerCandy.Text = DT.Rows[0]["RatePerCandy"].ToString();
            ddlColor.SelectedValue = DT.Rows[0]["ColorName"].ToString();
            btnSave.Text = "Update";
        }
        else
        {
            Clear_All_Field();
        }
    }

 
    private void Load_Data()
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select VarietyID,VarietyName,RatePerCandy,ColorName from MstVariety where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        RptVarietyMaster.DataSource = DT;
        RptVarietyMaster.DataBind();
    }

    protected void GridEditClick(object sender, CommandEventArgs e)
    {
        mpeVarietyMaster.Show();
        txtVarietyCode.Text = e.CommandName.ToString();
        btnSearch_Click(sender, e);
       
    }

    protected void btnAdd_Click(object sender, EventArgs e)
    {
        btnAdd.Text = "Save";
        txtVarietyCode.Text = "";
        txtVarietyName.Text = "";
        txtRatePerCandy.Text = "";
        mpeVarietyMaster.Show();

    }


    protected void GridDeleteClick(object sender, CommandEventArgs e)
    {
        string query = "";

        //User Rights Check Start
        bool ErrFlag = false;
        bool Rights_Check = false;

        Rights_Check = CommonClass_Function.DeleteRights_Check(SessionCcode, SessionLcode, SessionUserID, "3", "5", "Variety Master");
        if (Rights_Check == false)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Delete Veriety Master..');", true);
        }
        //User Rights Check End

        if (!ErrFlag)
        {


            DataTable dtdcheckPurchase = new DataTable();
            DataTable dtdcheckCottonInwards = new DataTable();
            DataTable dtdcheckWeight = new DataTable();
            DataTable dtdcurrentStock = new DataTable();
          
            //query = "select VarietyID,VarietyName from Customer_Purchase_Order_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' and VarietyID='" + e.CommandName.ToString() + "'";
            //dtdcheckPurchase = objdata.RptEmployeeMultipleDetails(query);

            //query = "select VarietyCode,VarietyName from CottonInwards where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' and VarietyCode='" + e.CommandName.ToString() + "'";
            //dtdcheckCottonInwards = objdata.RptEmployeeMultipleDetails(query);

            //query = "select VarietyCode,VarietyName from Weight_List_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' and VarietyCode='" + e.CommandName.ToString() + "'";
            //dtdcheckWeight = objdata.RptEmployeeMultipleDetails(query);

            //query = "select VariteyCode,VariteyName from Temp_Upload_Stock where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' and VariteyCode='" + e.CommandName.ToString() + "'";
            //dtdcurrentStock = objdata.RptEmployeeMultipleDetails(query);


            if ((dtdcheckPurchase.Rows.Count == 0) && (dtdcheckCottonInwards.Rows.Count == 0) && (dtdcheckWeight.Rows.Count == 0) && (dtdcurrentStock.Rows.Count == 0))
            {
                DataTable DT = new DataTable();
                query = "Select * from MstVariety where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And VarietyID='" + e.CommandName.ToString() + "'";
                DT = objdata.RptEmployeeMultipleDetails(query);
                if (DT.Rows.Count != 0)
                {
                    query = "Delete from MstVariety where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And VarietyID='" + e.CommandName.ToString() + "'";
                    objdata.RptEmployeeMultipleDetails(query);
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Variety Details Deleted Successfully');", true);
                    Load_Data();
                    Clear_All_Field();
                }
            }
            else
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Sorry Variety Available in some other table..');", true);

            }
        }
    }
}