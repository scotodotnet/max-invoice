﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
public partial class master_forms_ShiftMain : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        if (!IsPostBack)
        {
            Page.Title = "ERP Module :: Shift Master";
            HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Master"));
            li.Attributes.Add("class", "droplink active open");
        }

        Load_Data();
    }
    private void Load_Data()
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select ShiftCode,ShiftName,StartTime,EndTime,TotalHours,TotalMins from MstShift where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        Repeater1.DataSource = DT;
        Repeater1.DataBind();

    }

    protected void GridEditClick(object sender, CommandEventArgs e)
    {
        //txtSupplierCode.Text = e.CommandName.ToString();
        //btnSearch_Click(sender, e);

        string Supplier_No = e.CommandName.ToString();
        Session.Remove("ShiftCode");
        Session["ShiftCode"] = Supplier_No;
        Response.Redirect("ShiftDetails.aspx");

    }
    protected void GridDeleteClick(object sender, CommandEventArgs e)
    {
        string query = "";

        //User Rights Check Start
        bool ErrFlag = false;
        bool Rights_Check = false;

        //Rights_Check = CommonClass_Function.DeleteRights_Check(SessionCcode, SessionLcode, SessionUserID, "3", "5", "Shift Master");
        //if (Rights_Check == false)
        //{
        //    ErrFlag = true;
        //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Delete New Shift..');", true);
        //}
        //User Rights Check End


        if (ErrFlag == false)
        {
            DataTable DT = new DataTable();
            query = "Select * from MstShift where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ShiftCode='" + e.CommandName.ToString() + "'";
            DT = objdata.RptEmployeeMultipleDetails(query);
            if (DT.Rows.Count != 0)
            {
                query = "Delete from MstShift where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ShiftCode='" + e.CommandName.ToString() + "'";
                objdata.RptEmployeeMultipleDetails(query);
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Shift Details Deleted Successfully');", true);
                Load_Data();
                //Clear_All_Field();
            }
        }
    }
    protected void btnAdd_Click(object sender, EventArgs e)
    {
        Session.Remove("ShiftCode");
        Response.Redirect("ShiftDetails.aspx");
    }
}
