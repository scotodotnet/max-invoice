﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="Count_Master.aspx.cs" Inherits="master_forms_Count_Master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    
<script src='<%= ResolveUrl("../assets/js/master_list_jquery.min.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/master_list_jquery-ui.min.js") %>'></script>
<link href="<%= ResolveUrl("../assets/css/master_list_jquery-ui.css") %>" rel="stylesheet" type="text/css"/>
<link href="<%= ResolveUrl("../assets/css/Popup.css") %>" rel="stylesheet" type="text/css"/>  
   
     
    <style>
.add-on .input-group-btn > .btn {
    border-left-width: 0;
    left:-2px;
    -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
}
/* stop the glowing blue shadow */
.add-on .form-control:focus {
    -webkit-box-shadow: none; 
            box-shadow: none;
    border-color:#cccccc; 
}
</style>
   
   
    <script type="text/javascript">
    //On Page Load
//    $(function() {
//        $('.gvv').dataTable();
//    });

    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#TblCountMaster').dataTable();               
            }
        });
    };

//    if (prm != null) {
//        prm.add_endRequest(function(sender, e) {
//            if (sender._postBackSettings.panelsToUpdate != null) {
//                $('.gvv').dataTable();
//            }
//        });
//    };

</script>

  <script>
      $(document).ready(function() {
      $('#TblCountMaster').dataTable();
      });
    </script>

    <script type="text/javascript">
    function SaveMsgAlert(msg) {
        swal(msg);
        
    }
    
</script>

  <form class="form-horizontal">
  <asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
<div class="page-breadcrumb">
                    <ol class="breadcrumb container">
                   <h4><li class="active">Count Master</li></h4> 
                    </ol>
                </div>

<div id="main-wrapper" class="container">
 <div class="row">
    <div class="col-md-12">
        <div class="col-md-9">
			<div class="panel panel-white">
			
			 <div class="panel panel-primary">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">Count Master</h4>
				</div>
				</div>
				
		         <div class="panel-body">
		            
		                  <div class="col-md-12">
                                   <asp:Button ID="btnAdd" runat="server" Text="Add New Count" class="btn btn-success" />
                                   
                                      <cc1:ModalPopupExtender ID="MdpCountMaster" runat="server" PopupControlID="pnlPopup" TargetControlID="btnAdd"
                                         CancelControlID="" BackgroundCssClass="modalBackground">
                                      </cc1:ModalPopupExtender>
                                      
                                      <asp:Panel ID="pnlPopup" runat="server" CssClass="modalPopup" Style="display: none">
                                         <div class="header">
                                            Count New Entry
                                          </div>
                                          
                                            <div class="body">
                                                <div class="col-md-12 headsize">
                                                    <div class="form-group col-md-6">
							                            <label for="exampleInputName">Count Code<span class="mandatory">*</span></label>
							                             <asp:TextBox ID="txtCountCode" Enabled="false" class="form-control" runat="server" Style="text-transform: uppercase"></asp:TextBox>
                                                         <asp:TextBox ID="txtCount_Code_Hide" class="form-control" runat="server" 
                                                          Visible="false"></asp:TextBox>
                                                         <asp:HiddenField ID="HiddenCountCode" runat="server" />
                                                         <asp:RequiredFieldValidator ControlToValidate="txtCountCode" ValidationGroup="Validate_Field" class="form_error" ID="validate1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                         </asp:RequiredFieldValidator>	
                                                      
                                                         <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server" FilterType="Numbers, LowercaseLetters, UppercaseLetters, Custom"
                                                           ValidChars=", " TargetControlID="txtCountCode" />						   
                                                     </div>
                                                      <div class="form-group col-md-6">
							                              <label for="exampleInputName">Count Name<span class="mandatory">*</span></label>
							                              <asp:TextBox ID="txtCountName" class="form-control" runat="server" Style="text-transform: uppercase"></asp:TextBox>
                                                          <asp:RequiredFieldValidator ControlToValidate="txtCountName" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                          </asp:RequiredFieldValidator>
                                                          
                                                          <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Numbers, LowercaseLetters, UppercaseLetters, Custom"
                                                           ValidChars=", " TargetControlID="txtCountName" />		
						                              </div>
						                              <div class="form-group col-md-6">
							                              <label for="exampleInputName">Actual Count<span class="mandatory">*</span></label>
							                              <asp:TextBox ID="txtActualCount" class="form-control" runat="server" Style="text-transform: uppercase"></asp:TextBox>
                                                          <asp:RequiredFieldValidator ControlToValidate="txtActualCount" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator2" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                          </asp:RequiredFieldValidator>
                                                          
                                                          <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterType="Numbers,Custom"
                                                           ValidChars="." TargetControlID="txtActualCount" />		
						                              </div>
						                              <div class="form-group col-md-6">
							                              <label for="exampleInputName">Shade No<span class="mandatory">*</span></label>
							                              <asp:TextBox ID="txtShadeNo" class="form-control" runat="server" TextMode="MultiLine" ></asp:TextBox>
                                                         		
						                              </div>
						                              <div class="form-group col-md-6">
							                              <label for="exampleInputName">Unit<span class="mandatory">*</span></label>
                                                          <asp:DropDownList ID="ddlUnit" runat="server" class="form-control" Width="100%">
                                                          </asp:DropDownList>
                                                         		
						                              </div>
                                                   </div>
                                             </div>
                                             <div class="footer" align="center">
                                              <!-- Button start -->
                                                 <asp:Button ID="btnSave" class="btn btn-success"  runat="server" Text="Save" ValidationGroup="Validate_Field" OnClick="btnSave_Click"/>
                                                 <asp:Button ID="btnCancel" class="btn btn-danger" runat="server" Text="Cancel" OnClick="btnCancel_Click"/>
                                              <!-- Button end -->  
                                             </div>
                                      </asp:Panel>
                                   
                                </div>
                                 
                         <div class="form-group row"></div>
		                   
                          <div class="col-md-12">
                        <div class="row">
                            <asp:Repeater ID="RptCountMaster" runat="server" EnableViewState="false">
                                <HeaderTemplate>
                                    <table id="TblCountMaster" class="display table">
                                        <thead>
                                            <tr>
                                              <th>Count Code</th>
                                              <th>Count Name</th>
                                              <th>Actual Count</th>
                                              <th>Mode</th>
                                            </tr>
                                        </thead>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <%--<tbody>--%>
                                    <tr>
                                        <td><%# Eval("CountID")%></td>
                                        <td><%# Eval("CountName")%></td>
                                        <td><%# Eval("ActualCount")%></td>
                                        <td>
                                            <asp:LinkButton ID="btnEditGrid" class="btn btn-success btn-sm fa fa-pencil"  runat="server" 
                                                Text="" OnCommand="GridEditClick" CommandArgument="Edit" CommandName='<%# Eval("CountID")%>'>
                                                </asp:LinkButton>
                                                <asp:LinkButton ID="btnDeleteGrid" class="btn btn-danger btn-sm fa fa-trash-o"  runat="server" 
                                                Text="" OnCommand="GridDeleteClick" CommandArgument="Delete" CommandName='<%# Eval("CountID")%>' 
                                                CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this Count details?');">
                                                </asp:LinkButton>
                                        </td>
                                    </tr>
                                    <%--</tbody>--%>
                                </ItemTemplate>
                                <FooterTemplate></table></FooterTemplate>
                             </asp:Repeater>
                             
                             
                         </div>
                     </div>
             
				</div><!-- panel body end -->
				
			</div><!-- panel white end -->
		    </div><!-- col-9 end -->
		    
		    <!-- Dashboard start -->
		     <div class="col-lg-3 col-md-6">
                            <div class="panel panel-white" style="height: 100%;">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Dashboard Details</h4>
                                    <div class="panel-control">
                                        
                                        
                                    </div>
                                </div>
                                <div class="panel-body">
                                    
                                    
                                </div>
                            </div>
                        </div>  
                        
             <div class="col-lg-3 col-md-6">
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div class="live-tile flip ha" data-mode="flip" data-speed="750" data-delay="3000">
                                       
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div class="live-tile flip ha" data-mode="flip" data-speed="750" data-delay="3000">
                                       
                                    </div>
                                </div>
                            </div>
                            
                        </div> 
		    <!-- Dashboard End -->
		    
		    <div class="col-md-2"></div>
		    
    </div><!-- col 12 end -->
  </div><!-- row end -->
 </div><!-- main-wrapper end -->
</ContentTemplate>
</asp:UpdatePanel>
</form> 
</asp:Content>

