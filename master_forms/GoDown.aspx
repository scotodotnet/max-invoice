﻿<%@ Page Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="GoDown.aspx.cs" Inherits="master_forms_GoDown" Title="" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>



<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">


<script src='<%= ResolveUrl("../assets/js/master_list_jquery.min.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/master_list_jquery-ui.min.js") %>'></script>
<link href="<%= ResolveUrl("../assets/css/master_list_jquery-ui.css") %>" rel="stylesheet" type="text/css"/>



<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#example').dataTable();               
            }
        });
    };
</script>
<script type="text/javascript">
    function SaveMsgAlert(msg) {
        swal(msg);

    }
</script>


<div class="page-breadcrumb">
                    <ol class="breadcrumb container">
                   <h4><li class="active">GoDown</li></h4> 
                    </ol>
                </div>


<div id="main-wrapper" class="container">
 <div class="row">
    <div class="col-md-12">
        <div class="col-md-9">
			<div class="panel panel-white">
				<div class="panel panel-primary">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">GoDown</h4>
				</div>
				</div>
				<form class="form-horizontal">
				<div class="panel-body">
					<div class="col-md-12">
					    <div class="row">
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">GoDown ID <span class="mandatory">*</span></label>
					            <asp:TextBox ID="txtGoDownID" MaxLength="30" Enabled="false" class="form-control" runat="server" Style="text-transform: uppercase"></asp:TextBox>
					            <asp:RequiredFieldValidator ControlToValidate="txtGoDownID" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator3" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator> 
                                 <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" FilterType="Numbers, LowercaseLetters, UppercaseLetters, Custom"
                                ValidChars=", " TargetControlID="txtGoDownID" />
                              
					        </div>
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">GoDown Name <span class="mandatory">*</span></label>
					            <asp:TextBox ID="txtGoDownName" class="form-control" runat="server"></asp:TextBox>
					            <asp:RequiredFieldValidator ControlToValidate="txtGoDownName" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>  
                                
                               <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Numbers, LowercaseLetters, UppercaseLetters, Custom"
                                ValidChars=", " TargetControlID="txtGoDownName" />
					        </div>
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">Location <span class="mandatory">*</span></label>
					           <asp:TextBox ID="txtLocation" MaxLength="30" class="form-control" runat="server" Style="text-transform: uppercase"></asp:TextBox>
					            <asp:RequiredFieldValidator ControlToValidate="txtLocation" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator2" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator> 
                                
                               <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterType="Numbers, LowercaseLetters, UppercaseLetters, Custom"
                                ValidChars=",./ " TargetControlID="txtLocation" />
					        </div>
					        
					        <div class="form-group col-md-4">
					        <label for="exampleInputName">Unit <span class="mandatory">*</span></label>
                                <asp:DropDownList ID="ddlUnit" runat="server" class="form-control">
                                </asp:DropDownList>
					        </div>
					        
					       
					    </div>
					</div>
					<div class="form-group row"></div>  
					<!-- Button start -->
					<div class="col-md-12">
					    <div class="row">
					        <div class="txtcenter">
                                <div class="col-sm-11">
                                    <asp:Button ID="btnSave" class="btn btn-success"  runat="server" Text="Save" 
                                        ValidationGroup="Validate_Field" onclick="btnSave_Click"/>
                                    <%--<asp:Button ID="Button1" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg" runat="server" Text="View" />--%>
                                    <asp:Button ID="btnCancel" class="btn btn-danger" runat="server" Text="Cancel" 
                                        onclick="btnCancel_Click"/>
                                </div>
                            </div>
					    </div>
					</div>					                          
                    <!-- Button End -->
					<div class="form-group row"></div> 
					<!-- table start --> 
					
		            <div class="form-group row"></div>
		            <div class="col-md-12">
		                <div class="row">
		                    <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
		                        <HeaderTemplate>
                                    <table id="example" class="display table">
                                        <thead>
                                            <tr>
                                                <th>GoDown ID</th>
                                                <th>GoDown Name</th>
                                                <th>Location</th>
                                                <th>Unit</th>
                                                <th>Mode</th>
                                            </tr>
                                        </thead>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td><%# Eval("GoDownID")%></td>
                                        <td><%# Eval("GoDownName")%></td>
                                        <td><%# Eval("Location")%></td>
                                        <td><%# Eval("Unit")%></td>
                                     
                                        <td>
                                            <asp:LinkButton ID="btnEditGrid" class="btn btn-success btn-sm fa fa-pencil"  runat="server" 
                                                Text="" OnCommand="GridEditClick" CommandArgument="Edit" CommandName='<%# Eval("GoDownID")%>'>
                                            </asp:LinkButton>
                                            <asp:LinkButton ID="btnDeleteGrid" class="btn btn-danger btn-sm fa fa-trash-o"  runat="server" 
                                                Text="" OnCommand="GridDeleteClick" CommandArgument="Delete" CommandName='<%# Eval("GoDownID")%>' 
                                                CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this warehouse details?');">
                                            </asp:LinkButton>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate></table></FooterTemplate>
		                    </asp:Repeater>		                    
		                </div>
		            </div>						
                    <!-- table end -->  
						
				</div><!-- panel body end -->
				</form>
			</div><!-- panel white end -->
		    </div><!-- col-9 end -->
		    
		     <!-- Dashboard start -->
		     <div class="col-lg-3 col-md-6">
                            <div class="panel panel-white" style="height: 100%;">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Dashboard Details</h4>
                                    <div class="panel-control">
                                     </div>
                                </div>
                                <div class="panel-body">
                               </div>
                            </div>
                        </div>  
                        
                        <div class="col-lg-3 col-md-6">
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div class="live-tile flip ha" data-mode="flip" data-speed="750" data-delay="3000">
                                       
                                    </div>
                                </div>
                            </div>
                            
                        </div> 
		    <!-- Dashboard End -->
		    <div class="col-md-2"></div>
		  
      </div><!-- col 12 end -->
  </div><!-- row end -->
 </div>




</asp:Content>

