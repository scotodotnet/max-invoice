﻿<%@ Page Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="AgentMst.aspx.cs" Inherits="master_forms_AgentMst" Title="" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<script src='<%= ResolveUrl("../assets/js/master_list_jquery.min.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/master_list_jquery-ui.min.js") %>'></script>
<link href="<%= ResolveUrl("../assets/css/master_list_jquery-ui.css") %>" rel="stylesheet" type="text/css"/>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#example').dataTable();
            }
        });
    };
</script>

<script type="text/javascript">
    function SaveMsgAlert(msg) {
        swal(msg);
    }
</script>



<div class="page-breadcrumb">
                    <ol class="breadcrumb container">
                   <h4><li class="active">Agent</li></h4> 
                    </ol>
                </div>
<div id="main-wrapper" class="container">
 <div class="row">
    <div class="col-md-12">
    <div class="col-md-9">
			<div class="panel panel-white">
				<div class="panel panel-primary">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">Agent</h4>
				</div>
				</div>
				<form class="form-horizontal">
				<div class="panel-body">
					<div class="col-md-12">
					    <div class="row">
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">Agent ID <span class="mandatory">*</span></label>
					            <asp:TextBox ID="txtAgentID" MaxLength="30" Enabled="false" class="form-control" runat="server" Style="text-transform: uppercase"></asp:TextBox>
                              
                                <asp:RequiredFieldValidator ControlToValidate="txtAgentID" Display="Dynamic"  ValidationGroup="Validate_Field" class="form_error" ID="validate1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>	
                                
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterType="Numbers, LowercaseLetters, UppercaseLetters, Custom"
                                ValidChars=", " TargetControlID="txtAgentID" />					            
					        </div>
					        <%--<asp:LinkButton ID="btnSearch" class="btn btn-primary btn-addon  btn-rounded btn-sm" runat="server" OnClick="btnSearch_Click"><i class="fa fa-search"></i>Search</asp:LinkButton>--%>
					        
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">Agent Name <span class="mandatory">*</span></label>
							    <asp:TextBox ID="txtAgentName" class="form-control" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ControlToValidate="txtAgentName" Display="Dynamic"  ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
                                
                                  <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterType="Numbers, LowercaseLetters, UppercaseLetters, Custom"
                                ValidChars=", " TargetControlID="txtAgentName" />	
                                
					        </div>
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">Address 1 <span class="mandatory">*</span></label>
					            <asp:TextBox ID="txtAddress1" class="form-control" runat="server"></asp:TextBox>
	                            <asp:RequiredFieldValidator ControlToValidate="txtAddress1" Display="Dynamic"  ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator2" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
                                
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" FilterType="Numbers, LowercaseLetters, UppercaseLetters, Custom"
                                ValidChars=",./ " TargetControlID="txtAddress1" />
                                
					        </div>
					        
					        </div>
					        </div>
		<div class="col-md-12">
					    <div class="row">			        
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">Address 2</label>
					            <asp:TextBox ID="txtAddress2" class="form-control" runat="server"></asp:TextBox>
					             <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" FilterType="Numbers, LowercaseLetters, UppercaseLetters, Custom"
                                ValidChars=",./ " TargetControlID="txtAddress2" />
                                
					        
					        </div>
					        
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">Pincode <span class="mandatory">*</span></label>
					            <asp:TextBox ID="txtPincode"  MaxLength="6" class="form-control" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ControlToValidate="txtPincode" Display="Dynamic"  ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator4" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="RegExValPin" Display="Dynamic"  ValidationExpression="[0-9]{6}" runat="server" class="form_error" 
                                    ControlToValidate="txtPincode" ValidationGroup="Validate_Field" ErrorMessage="Pincode length 6">
                                </asp:RegularExpressionValidator>
                                <cc1:FilteredTextBoxExtender ID="FilTxtPin1" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtPincode" ValidChars="0123456789">
                                </cc1:FilteredTextBoxExtender>
					        </div>
					        
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">Tel No.</label>
					            <div class="form-group row">
					                <div class="col-sm-4">
							            <asp:TextBox ID="txtStdCode" MaxLength="5" class="form-control" runat="server"></asp:TextBox>
							            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                            TargetControlID="txtStdCode" ValidChars="0123456789">
                                        </cc1:FilteredTextBoxExtender>
							        </div>
							        <div class="col-sm-8">
					                    <asp:TextBox ID="txtTel_no" MaxLength="7" class="form-control" runat="server"></asp:TextBox>
					                    <asp:RegularExpressionValidator ID="RegExpValdTelNo" Display="Dynamic" ValidationExpression="[0-9]{7}" runat="server" class="form_error" 
                                            ControlToValidate="txtTel_no" ValidationGroup="Validate_Field" ErrorMessage="Tel.No length 7">
                                        </asp:RegularExpressionValidator>
                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                            TargetControlID="txtTel_no" ValidChars="0123456789">
                                        </cc1:FilteredTextBoxExtender>
					                </div>
					            </div>
					        </div>
					        				        
					    </div>
					</div>
					<div class="col-md-12">
					    <div class="row">
					        
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">Mobile No <span class="mandatory">*</span></label>					            
					            <div class="form-group row">
					                <div class="col-sm-4">
							            <asp:TextBox ID="txtMblCode" Text="+91" class="form-control" runat="server" ReadOnly="true"></asp:TextBox>
							        </div>
							        <div class="col-sm-8">
                                        <asp:TextBox ID="txtMobile_no" MaxLength="10" class="form-control" runat="server"></asp:TextBox>
                                        <asp:RequiredFieldValidator ControlToValidate="txtMobile_no" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator5" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                        </asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" Display="Dynamic" ValidationExpression="[0-9]{10}" runat="server" class="form_error" 
                                            ControlToValidate="txtMobile_no" ValidationGroup="Validate_Field" ErrorMessage="Mobile.No length 10">
                                        </asp:RegularExpressionValidator>
                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                            TargetControlID="txtMobile_no" ValidChars="0123456789">
                                        </cc1:FilteredTextBoxExtender>
                                    </div>
                                </div>
					        </div>
					        					        
				       
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">Mail ID <span class="mandatory">*</span></label>
                                <asp:TextBox ID="txtMail_Id" class="form-control" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ControlToValidate="txtMail_Id" ValidationGroup="Validate_Field" Display="Dynamic"  class="form_error" ID="RequiredFieldValidator8" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" class="form_error" runat="server"  ControlToValidate="txtMail_Id" 
                                    Display ="Dynamic" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" 
                                    ValidationGroup="Validate_Field" ErrorMessage="enter valid email address. Eg. Something@domain.com">
                                </asp:RegularExpressionValidator>
					        </div>
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">Area</label>
                                <asp:TextBox ID="txtArea" runat="server"  class="form-control"></asp:TextBox>
					        </div>
					    </div>
					</div>
					<!-- Button start -->
					<div class="col-md-12">
					    <div class="row">					            
					        <div class="txtcenter">
					            <div class="col-sm-11">
					                <asp:Button ID="btnSave" class="btn btn-success"  runat="server" Text="Save" ValidationGroup="Validate_Field" OnClick="btnSave_Click"/>
                                    <%--<asp:Button ID="Button1" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg" runat="server" Text="View" />--%>
                                    <asp:Button ID="btnCancel" class="btn btn-danger" runat="server" Text="Cancel" OnClick="btnCancel_Click"/>
                                </div>
					        </div>
					    </div>
					</div>
					<!-- Button end -->
					<div class="form-group row"></div>
					<!-- table start -->
					<div class="col-md-12">
					    <div class="row">
					        <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
					            <HeaderTemplate>
                                    <table id="example" class="display table">
                                        <thead>
                                            <tr>
                                                <th>Agent Name</th>
                                                <th>Address</th>
                                                <th>Mobile No</th>
                                                <%--<th>Tin No</th>
                                                <th>CST No</th>--%>
                                                <th>Mode</th>
                                            </tr>
                                        </thead>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td><%# Eval("AgentName")%></td>
                                        <td><%# Eval("Address2")%></td>
                                        <td><%# Eval("MobileNo_Join")%></td>
                                       <%-- <td><%# Eval("TinNo")%></td>--%>
                                        <%--<td><%# Eval("CstNo")%></td>--%>
                                        <td>
                                            <asp:LinkButton ID="btnEditGrid" class="btn btn-success btn-sm fa fa-pencil"  runat="server" 
                                                Text="" OnCommand="GridEditClick" CommandArgument="Edit" CommandName='<%# Eval("AgentID")%>'>
                                            </asp:LinkButton>
                                            <asp:LinkButton ID="btnDeleteGrid" class="btn btn-danger btn-sm fa fa-trash-o"  runat="server" 
                                                Text="" OnCommand="GridDeleteClick" CommandArgument="Delete" CommandName='<%# Eval("AgentID")%>' 
                                                CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this Agent Details?');">
                                            </asp:LinkButton>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate></table></FooterTemplate>                                
					        </asp:Repeater>
					    </div>
					</div>
					<!-- table End --> 
						
						
						
						
				</div><!-- panel body end -->
				</form>
			</div><!-- panel white end -->
		    </div><!-- col-9 end -->
		    
		     <!-- Dashboard start -->
		     <div class="col-lg-3 col-md-6">
                            <div class="panel panel-white" style="height: 100%;">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Dashboard Details</h4>
                                    <div class="panel-control">
                                        
                                        
                                    </div>
                                </div>
                                <div class="panel-body">
                                    
                                    
                                </div>
                            </div>
                        </div>  
                        
                        <div class="col-lg-3 col-md-6">
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div class="live-tile flip ha" data-mode="flip" data-speed="750" data-delay="3000">
                                       
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div class="live-tile flip ha" data-mode="flip" data-speed="750" data-delay="3000">
                                       
                                    </div>
                                </div>
                            </div>
                            
                        </div> 
		    <!-- Dashboard End -->
		    
		    <div class="col-md-2"></div>  
            
      
         
      </div><!-- col 12 end -->
  </div><!-- row end -->
 </div><!-- main-wrapper end -->
 



</asp:Content>

