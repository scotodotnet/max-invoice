﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
public partial class master_forms_GstMst : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionSupplierCode;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "ERP Module :: GST Master";
            HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Master"));
            li.Attributes.Add("class", "droplink active open");

        }

        Load_Data();
    }
    private void Load_Data()
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select GstType,Cgst,Sgst,Igst from MstGst";
        DT = objdata.RptEmployeeMultipleDetails(query);
        Repeater1.DataSource = DT;
        Repeater1.DataBind();
    }
   

    protected void btnSave_Click(object sender, EventArgs e)
    {
        string query = "";
        DataTable DT = new DataTable();
        string SaveMode = "Insert";

        //User Rights Check Start
        bool ErrFlag = false;
        bool Rights_Check = false;
        if (txtGstType.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the GST Name...');", true);
        }
        else if (txtCgst.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the Item CGST...');", true);
        }
        else if (txtSgst.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the Item SGST...');", true);

        }
        else if (txtIgst.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the Item IGST...');", true);
        }
      
        if (!ErrFlag)
        {
            query = "Select * from MstGst where GstType='" + txtGstType.Text + "'";
            DT = objdata.RptEmployeeMultipleDetails(query);
            if (DT.Rows.Count != 0)
            {
                SaveMode = "Update";
                query = "Delete from MstGst where  GstType='" + txtGstType.Text + "'";
                objdata.RptEmployeeMultipleDetails(query);
            }
            if (SaveMode != "Error")
            {
                int ActiveMode = 0;

                //Insert Compnay Details
                query = "Insert Into MstGst(GstType,Cgst,Sgst,Igst)";
                query = query + " Values ('" + txtGstType.Text + "',";
                query = query + " '" + txtCgst.Text + "','" + txtSgst.Text + "','" + txtIgst.Text + "')";
                //query = query + "'" + txtAmount.Text + "','" + txtHsn.Text + "')";
                objdata.RptEmployeeMultipleDetails(query);
                Load_Data();
                if (SaveMode == "Insert")
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('GST Details Saved Successfully');", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('GST Details Updated Successfully');", true);
                }

                //Load_Data();
                Clear_All_Field();
                btnSave.Text = "Save";
                //txtItemCode.Enabled = true;
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('GST Details Already Exisits');", true);
            }
        }

        //Response.Redirect("SupplierMaster_Main.aspx");

    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }
    private void Clear_All_Field()
    {
        txtGstType.Text = ""; txtSgst.Text = ""; txtCgst.Text = "";
        txtIgst.Text = ""; //txtHsn.Text = "";

        //txtTel_no.Text = ""; txtMobile_no.Text = ""; txttin_no.Text = ""; txtcst_no.Text = ""; txtMail_Id.Text = "";
        //txtdescription.Text = "";
        //txtStdCode.Text = ""; txtMblCode.Text = "+91";
        btnSave.Text = "Save";
        //txtItemCode.Enabled = true;
        //Load_RandomID();
        //txtState.SelectedValue = "1";
        //txtCountry.SelectedValue = "1";
    }
    protected void GridEditClick(object sender, CommandEventArgs e)
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select GstType,Cgst,Sgst,Igst from MstGst where GstType='" + e.CommandName.ToString() + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            txtGstType.Text = DT.Rows[0]["GstType"].ToString();
            txtCgst.Text = DT.Rows[0]["Cgst"].ToString();
            txtSgst.Text = DT.Rows[0]["Sgst"].ToString();
            txtIgst.Text = DT.Rows[0]["Igst"].ToString();
        }
    }
    protected void GridDeleteClick(object sender, CommandEventArgs e)
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Delete from MstGst where GstType='" + e.CommandName.ToString() + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('GST Details Deleted Successfully');", true);
        Load_Data();
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select * from ItemMaster where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And GstType='" + txtGstType.Text + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            //txtItemCode.Text = DT.Rows[0]["SuppCode"].ToString();
            txtGstType.Text = DT.Rows[0]["ItemName"].ToString();
            //RdpActiveMode.SelectedValue = DT.Rows[0]["ActiveMode"].ToString();
            txtCgst.Text = DT.Rows[0]["Description"].ToString();
            txtSgst.Text = DT.Rows[0]["Amount"].ToString();
            txtIgst.Text = DT.Rows[0]["HSNcode"].ToString();




            //txtSup_Code_Hide.Text = "";
            //txtItemCode.Enabled = false;
            btnSave.Text = "Update";
        }
        else
        {
            Clear_All_Field();
        }
    }

   

    

}
