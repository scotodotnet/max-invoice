﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using Altius.BusinessAccessLayer.BALDataAccess;


public partial class master_forms_AgentMst : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;


    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        if (!IsPostBack)
        {
            Load_RandomID();
            Page.Title = "ERP Module :: Agent Master";
            HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Master"));
            li.Attributes.Add("class", "droplink active open");
        }
      
        Load_Data();
    }

    private void Load_RandomID()
    {
        string query = "";
        DataTable DT = new DataTable();
        string DeptCode = "";
        Decimal DeptCode_Dec = 0;
        query = "Select MAX(CAST(AgentID AS int)) AS AgentID from MstAgent where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        if (DT.Rows.Count != 0)
        {
            DeptCode = DT.Rows[0]["AgentID"].ToString();
            if (DeptCode != "")
            {
                DeptCode_Dec = Convert.ToDecimal(DeptCode) + 1;
            }
            else
            {
                DeptCode_Dec = 1;
            }

            txtAgentID.Text = DeptCode_Dec.ToString();

        }
        else
        {
            txtAgentID.Text = "1";
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        string query = "";
        DataTable DT = new DataTable();
        string SaveMode = "Insert";

        //User Rights Check Start
        bool ErrFlag = false;
        bool Rights_Check = false;
        //if (btnSave.Text == "Update")
        //{
        //    Rights_Check = CommonClass_Function.ModifyRights_Check(SessionCcode, SessionLcode, SessionUserID, "3", "5", "Agent");
        //    if (Rights_Check == false)
        //    {
        //        ErrFlag = true;
        //        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Modify Agent Details...');", true);
        //    }
        //}
        //else
        //{
        //    Rights_Check = CommonClass_Function.AddRights_Check(SessionCcode, SessionLcode, SessionUserID, "3", "5", "Agent");
        //    if (Rights_Check == false)
        //    {
        //        ErrFlag = true;
        //        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Adding New Agent...');", true);
        //    }
        //}
        //User Rights Check End




        if (!ErrFlag)
        {
            query = "Select * from MstAgent where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And AgentID='" + txtAgentID.Text + "'";
            DT = objdata.RptEmployeeMultipleDetails(query);
            if (DT.Rows.Count != 0)
            {
                if (btnSave.Text == "Update")
                {
                    SaveMode = "Update";
                    query = "Delete from MstAgent where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And AgentID='" + txtAgentID.Text + "'";
                    objdata.RptEmployeeMultipleDetails(query);
                }
                else
                {
                    SaveMode = "Error";
                }
            }
            if (SaveMode != "Error")
            {
                
                //Insert Agent Details
                query = "Insert Into MstAgent(Ccode,Lcode,AgentID,AgentName,Address1,Address2,Pincode,";
                query = query + " Std_Code,TelNo,Mobile_Code,MobileNo,MailID,Area,UserID,UserName) Values ('" + SessionCcode + "',";
                query = query + " '" + SessionLcode + "','" + txtAgentID.Text.ToUpper() + "','" + txtAgentName.Text + "',";
                query = query + "'" + txtAddress1.Text + "','" + txtAddress2.Text + "','" + txtPincode.Text + "',";
                query = query + "'" + txtStdCode.Text + "','" + txtTel_no.Text + "','" + txtMblCode.Text + "','" + txtMobile_no.Text + "',";
                query = query + "'" + txtMail_Id.Text + "','" + txtArea.Text + "',";
                query = query + "'" + SessionUserID + "','" + SessionUserName + "')";
                objdata.RptEmployeeMultipleDetails(query);

                if (SaveMode == "Insert")
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Agent Details Saved Successfully');", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Agent Details Updated Successfully');", true);
                }

                Load_Data();
                Clear_All_Field();
                btnSave.Text = "Save";

                txtAgentID.Enabled = false;
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Agent Details Already Exisits');", true);
            }
        }

    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }
    private void Clear_All_Field()
    {
        txtAgentID.Text = ""; txtAgentName.Text = ""; txtAddress1.Text = "";
        txtAddress2.Text = ""; txtPincode.Text = ""; txtTel_no.Text = ""; txtMobile_no.Text = ""; txtMail_Id.Text = "";
        txtArea.Text = "";
        txtStdCode.Text = ""; txtMblCode.Text = "+91";
        btnSave.Text = "Save";
        txtAgentID.Enabled = false;
        Load_RandomID();
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select * from MstAgent where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And AgentID='" + txtAgentID.Text + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            //txtSupplierCode.Text = DT.Rows[0]["SuppCode"].ToString();
            txtAgentName.Text = DT.Rows[0]["AgentName"].ToString();
           
            txtAddress1.Text = DT.Rows[0]["Address1"].ToString();
            txtAddress2.Text = DT.Rows[0]["Address2"].ToString();
         
            txtPincode.Text = DT.Rows[0]["Pincode"].ToString();
             txtStdCode.Text = DT.Rows[0]["Std_Code"].ToString();
            txtTel_no.Text = DT.Rows[0]["TelNo"].ToString();
            txtMblCode.Text = DT.Rows[0]["Mobile_Code"].ToString();
            txtMobile_no.Text = DT.Rows[0]["MobileNo"].ToString();
           
            txtMail_Id.Text = DT.Rows[0]["MailID"].ToString();
            txtArea.Text = DT.Rows[0]["Area"].ToString();



            txtAgentID.Enabled = false;
            btnSave.Text = "Update";
        }
        else
        {
            Clear_All_Field();
        }
    }

    private void Load_Data()
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select AgentName,Address2,(Mobile_Code + '-' + MobileNo) as MobileNo_Join,AgentID from MstAgent where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        Repeater1.DataSource = DT;
        Repeater1.DataBind();

    }

    protected void GridEditClick(object sender, CommandEventArgs e)
    {
        txtAgentID.Text = e.CommandName.ToString();
        btnSearch_Click(sender, e);
    }
    protected void GridDeleteClick(object sender, CommandEventArgs e)
    {
        string query = "";

        //User Rights Check Start
        bool ErrFlag = false;
        bool Rights_Check = false;

        Rights_Check = CommonClass_Function.DeleteRights_Check(SessionCcode, SessionLcode, SessionUserID, "3", "5", "Agent");
        if (Rights_Check == false)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Delete Agent..');", true);
        }
        //User Rights Check End

        if (!ErrFlag)
        {

            DataTable DT = new DataTable();
            query = "Select * from MstAgent where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And AgentID='" + e.CommandName.ToString() + "'";
            DT = objdata.RptEmployeeMultipleDetails(query);
            if (DT.Rows.Count != 0)
            {
                query = "Delete from MstAgent where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And AgentID='" + e.CommandName.ToString() + "'";
                objdata.RptEmployeeMultipleDetails(query);
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Agent Details Deleted Successfully');", true);
                Load_Data();
                Clear_All_Field();
            }

        }
    }

}
