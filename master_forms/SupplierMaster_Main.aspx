﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="SupplierMaster_Main.aspx.cs" Inherits="master_forms_SupplierMaster_Main" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    
 <script src='<%= ResolveUrl("../assets/js/master_list_jquery.min.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/master_list_jquery-ui.min.js") %>'></script>
<link href="<%= ResolveUrl("../assets/css/master_list_jquery-ui.css") %>" rel="stylesheet" type="text/css"/>
<link href="<%= ResolveUrl("../assets/css/Popup.css") %>" rel="stylesheet" type="text/css"/>  
   
     
    <style>
.add-on .input-group-btn > .btn {
    border-left-width: 0;
    left:-2px;
    -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
}
/* stop the glowing blue shadow */
.add-on .form-control:focus {
    -webkit-box-shadow: none; 
            box-shadow: none;
    border-color:#cccccc; 
}
</style>
   
   
    <script type="text/javascript">
     //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#example').dataTable();               
            }
        });
    };

</script>

    <script type="text/javascript">
    function SaveMsgAlert(msg) {
        swal(msg);
        
    }
    
</script>

  <form class="form-horizontal">
  <asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
<div class="page-breadcrumb">
                    <ol class="breadcrumb container">
                   <h4><li class="active">Supplier Master</li>
                       <h4>
                       </h4>
                        <h4>
                       </h4>
                        </h4> 
                    </ol>
                </div>

<div id="main-wrapper" class="container">
 <div class="row">
    <div class="col-md-12">
        <div class="col-md-9">
			<div class="panel panel-white">
			
			 <div class="panel panel-primary">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">Supplier Master</h4>
				</div>
		    </div>
				
		         <div class="panel-body">
		            
		                <div class="col-md-12">
                          <asp:Button ID="btnAdd" runat="server" Text="Add New Entry" 
                                class="btn btn-success" onclick="btnAdd_Click"/>
                        </div>
                                 
                        <div class="form-group row"></div>
		                   
                        <div class="col-md-12">
					    <div class="row">
					        <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
					            <HeaderTemplate>
                                    <table id="example" class="display table">
                                        <thead>
                                            <tr>
                                                <th>Supplier Name</th>
                                                <th>City</th>
                                                <th>Mobile No</th>
                                                <th>Tin No</th>
                                                <%--<th>CST No</th>--%>
                                                <th>Mode</th>
                                            </tr>
                                        </thead>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td><%# Eval("SuppName")%></td>
                                        <td><%# Eval("City")%></td>
                                        <td><%# Eval("MobileNo_Join")%></td>
                                        <td><%# Eval("TinNo")%></td>
                                        <%--<td><%# Eval("CstNo")%></td>--%>
                                        <td>
                                            <asp:LinkButton ID="btnEditGrid" class="btn btn-success btn-sm fa fa-pencil"  runat="server" 
                                                Text="" OnCommand="GridEditClick" CommandArgument="Edit" CommandName='<%# Eval("SuppCode")%>'>
                                            </asp:LinkButton>
                                            <asp:LinkButton ID="btnDeleteGrid" class="btn btn-danger btn-sm fa fa-trash-o"  runat="server"
                                                Text="" OnCommand="GridDeleteClick" CommandArgument="Delete" CommandName='<%# Eval("SuppCode")%>' 
                                                CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this supplier details?');">
                                            </asp:LinkButton>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate></table></FooterTemplate>                                
					        </asp:Repeater>
					    </div>
					</div>
             
				</div><!-- panel body end -->
				
			</div><!-- panel white end -->
		    </div><!-- col-9 end -->
		    
		    <!-- Dashboard start -->
		           <div class="col-lg-3 col-md-6">
                            <div class="panel panel-white" style="height: 100%;">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Dashboard Details</h4>
                                    <div class="panel-control">
                                        
                                        
                                    </div>
                                </div>
                                <div class="panel-body">
                                    
                                    
                                </div>
                            </div>
                        </div>  
                        
                       <div class="col-lg-3 col-md-6">
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div class="live-tile flip ha" data-mode="flip" data-speed="750" data-delay="3000">
                                       
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div class="live-tile flip ha" data-mode="flip" data-speed="750" data-delay="3000">
                                       
                                    </div>
                                </div>
                            </div>
                            
                        </div> 
		    <!-- Dashboard End -->
		    
		    <div class="col-md-2"></div>
		    
    </div><!-- col 12 end -->
  </div><!-- row end -->
 </div><!-- main-wrapper end -->
</ContentTemplate>
</asp:UpdatePanel>
</form> 
</asp:Content>

