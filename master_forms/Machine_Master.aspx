<%@ Page Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="Machine_Master.aspx.cs" Inherits="master_forms_Machine_Master" Title="" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#example').dataTable();
                $('.js-states').select2();       
            }
        });
    };
</script>

<script type="text/javascript">
    function SaveMsgAlert(msg) {
        swal(msg);
    }
</script>


  <div class="page-breadcrumb">
      <ol class="breadcrumb container">
       <h4><li class="active">Machine Master</li></h4> 
       </ol>
  </div>


<div id="main-wrapper" class="container">
 <div class="row">
    <div class="col-md-12">
        <div class="col-md-9">
			<div class="panel panel-white">
				<div class="panel panel-primary">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">Machine Master</h4>
				</div>
				</div>
				<form class="form-horizontal">
				<div class="panel-body">
					<div class="col-md-12">
					    <div class="row">
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">Machine ID <span class="mandatory">*</span></label>
					            <asp:TextBox ID="txtMachineID" class="form-control" runat="server" Enabled="false"></asp:TextBox>
					            <asp:RequiredFieldValidator ControlToValidate="txtMachineID" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator3" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator> 
                                 <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" FilterType="Numbers, LowercaseLetters, UppercaseLetters, Custom"
                                ValidChars=", " TargetControlID="txtMachineID" />
                              
					        </div>
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">Machine Name <span class="mandatory">*</span></label>
					            <asp:TextBox ID="txtMachineName" class="form-control" runat="server"></asp:TextBox>
					            <asp:RequiredFieldValidator ControlToValidate="txtMachineName" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>  
                                
                               <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Numbers, LowercaseLetters, UppercaseLetters, Custom"
                                ValidChars=", " TargetControlID="txtMachineName" />
					        </div>
					         <div class="form-group col-md-4">
					            <label for="exampleInputName">Department <span class="mandatory">*</span></label>
					            <asp:DropDownList ID="ddlDepartment" runat="server" 
                                     class="js-states form-control" AutoPostBack="true"
                                     onselectedindexchanged="ddlDepartment_SelectedIndexChanged">
					            
                                </asp:DropDownList>
					            <asp:RequiredFieldValidator ControlToValidate="ddlDepartment" Display="Dynamic" InitialValue="0" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator5" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
                               
					        </div>
					          </div>
					</div>
				
					<div class="col-md-12">
					    <div class="row">
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">Make <span class="mandatory">*</span></label>
					           <asp:TextBox ID="txtMake" MaxLength="30" class="form-control" runat="server" Style="text-transform: uppercase"></asp:TextBox>
					            <asp:RequiredFieldValidator ControlToValidate="txtMake" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator2" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator> 
                                
                               <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterType="Numbers, LowercaseLetters, UppercaseLetters, Custom"
                                ValidChars=",./ " TargetControlID="txtMake" />
					        </div>
					       <div class="form-group col-md-4">
					            <label for="exampleInputName">Model <span class="mandatory">*</span></label>
					           <asp:TextBox ID="txtModel" MaxLength="30" class="form-control" runat="server" Style="text-transform: uppercase"></asp:TextBox>
					            <asp:RequiredFieldValidator ControlToValidate="txtModel" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator4" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator> 
                                
                               <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterType="Numbers, LowercaseLetters, UppercaseLetters, Custom"
                                ValidChars=",./ " TargetControlID="txtModel" />
					        </div>
					         <div class="form-group col-md-4">
					            <label for="exampleInputName">Std Production </label>
					           <asp:TextBox ID="txtProdTraget" class="form-control" runat="server" Text="0"></asp:TextBox>
					           <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterType="Numbers,Custom"
                                ValidChars="." TargetControlID="txtProdTraget" />
					        </div>
					         </div>
					</div>
				
					<div class="col-md-12">
					    <div class="row">
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">Speed </label>
					           <asp:TextBox ID="txtSpeed" class="form-control" runat="server"></asp:TextBox>
					           <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" FilterType="Numbers,Custom"
                                ValidChars="." TargetControlID="txtSpeed" />
					        </div>
					         <div class="form-group col-md-4">
					            <label for="exampleInputName">Std Hank </label>
					           <asp:TextBox ID="txtStdHank" class="form-control" runat="server"></asp:TextBox>
					           <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" FilterType="Numbers,Custom"
                                ValidChars="." TargetControlID="txtStdHank" />
					        </div>
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">EB Unit Target </label>
					           <asp:TextBox ID="txtEBUnitTarget" class="form-control" runat="server"></asp:TextBox>
					           <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server" FilterType="Numbers,Custom"
                                ValidChars="." TargetControlID="txtEBUnitTarget" />
					        </div>
					         </div>
					</div>
				
					<div class="col-md-12">
					    <div class="row">
					      <div class="form-group col-md-4">
					            <label for="exampleInputName">Main Count </label>
					           <asp:TextBox ID="txtMainCount" class="form-control" runat="server"></asp:TextBox>
					           <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender10" runat="server" FilterType="Numbers,Custom"
                                ValidChars="." TargetControlID="txtMainCount" />
					        </div>
					         <div class="form-group col-md-4">
					            <label for="exampleInputName">TPI </label>
					           <asp:TextBox ID="txtTPI" class="form-control" runat="server"></asp:TextBox>
					           <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender11" runat="server" FilterType="Numbers,Custom"
                                ValidChars="." TargetControlID="txtTPI" />
					        </div>
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">Efficiency </label>
					           <asp:TextBox ID="txtEff" class="form-control" runat="server"></asp:TextBox>
					           <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender12" runat="server" FilterType="Numbers,Custom"
                                ValidChars="." TargetControlID="txtEff" />
					        </div>
					         </div>
					</div>
				
					<div class="col-md-12">
					    <div class="row">
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">Unit</label>
					            <asp:DropDownList ID="ddlUnit" runat="server" class="form-control">
                                </asp:DropDownList>
					         
					        </div>
					          <div class="form-group col-md-4">
					            <label for="exampleInputName">Worked Roter </label>
					           <asp:TextBox ID="txtWorkRoter" class="form-control" runat="server"></asp:TextBox>
					           <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender9" runat="server" FilterType="Numbers,Custom"
                                ValidChars="." TargetControlID="txtWorkRoter" />
					        </div>
					        
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">Count</label>
					            <asp:DropDownList ID="ddlCount" runat="server" class="form-control">
                                </asp:DropDownList>
					         
					        </div>
					         </div>
					</div>
				
					<div class="col-md-12">
					    <div class="row">
					         <div class="form-group col-md-4">
					            <label for="exampleInputName">Side</label>
					            <asp:DropDownList ID="ddlSide" runat="server" class="form-control">
					            <asp:ListItem Value="0" Text="-Select-"></asp:ListItem>
					            <asp:ListItem Value="1" Text="LEFT"></asp:ListItem>
					            <asp:ListItem Value="2" Text="RIGHT"></asp:ListItem>
                                </asp:DropDownList>
					         
					        </div>
					          <div class="form-group col-md-4">
					            <label for="exampleInputName">Description</label>
					            <asp:TextBox ID="txtDesc" class="form-control" runat="server" TextMode="MultiLine"></asp:TextBox>
					         
					        </div>
					        
					    </div>
					</div>
					<div class="form-group row"></div>  
					<!-- Button start -->
					<div class="col-md-12">
					    <div class="row">
					        <div class="txtcenter">
                                <div class="col-sm-11">
                                    <asp:Button ID="btnSave" class="btn btn-success"  runat="server" Text="Save" 
                                        ValidationGroup="Validate_Field" onclick="btnSave_Click" />
                                    <%--<asp:Button ID="Button1" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg" runat="server" Text="View" />--%>
                                    <asp:Button ID="btnCancel" class="btn btn-danger" runat="server" Text="Cancel" 
                                        onclick="btnCancel_Click" />
                                </div>
                            </div>
					    </div>
					</div>					                          
                    <!-- Button End -->
					<div class="form-group row"></div> 
					
						<!-- table start -->
                    <div class="col-md-12">
		                <div class="row">
		                    <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
		                        <HeaderTemplate>
                                    <table id="example" class="display table">
                                        <thead>
                                            <tr>
                                                <th>MachineCode</th>
                                                <th>MachineName</th>
                                                <th>Department</th>
                                                <th>Unit</th>
                                                <th>Mode</th>
                                            </tr>
                                        </thead>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td><%# Eval("MachineCode")%></td>
                                        <td><%# Eval("MachineName")%></td>
                                        <td><%# Eval("DeptName")%></td>
                                        <td><%# Eval("Unit")%></td>
                                        <td>
                                            <asp:LinkButton ID="btnEditGrid" class="btn btn-success btn-sm fa fa-pencil"  runat="server" 
                                                Text="" OnCommand="GridEditClick" CommandArgument="Edit" CommandName='<%# Eval("MachineCode")%>'>
                                            </asp:LinkButton>
                                            <asp:LinkButton ID="btnDeleteGrid" class="btn btn-danger btn-sm fa fa-trash-o"  runat="server" 
                                                Text="" OnCommand="GridDeleteClick" CommandArgument="Delete" CommandName='<%# Eval("MachineCode")%>' 
                                                CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this Machine details?');">
                                            </asp:LinkButton>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate></table></FooterTemplate>
		                    </asp:Repeater>		                    
		                </div>
		            </div>
                    <!-- table End -->
				</div><!-- panel body end -->
				</form>
			</div><!-- panel white end -->
		    </div><!-- col-9 end -->
		    
		     <!-- Dashboard start -->
		     <div class="col-lg-3 col-md-6">
                            <div class="panel panel-white" style="height: 100%;">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Dashboard Details</h4>
                                    <div class="panel-control">
                                     </div>
                                </div>
                                <div class="panel-body">
                               </div>
                            </div>
                        </div>  
                        
                        <div class="col-lg-3 col-md-6">
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div class="live-tile flip ha" data-mode="flip" data-speed="750" data-delay="3000">
                                       
                                    </div>
                                </div>
                            </div>
                            
                        </div> 
		    <!-- Dashboard End -->
		    <div class="col-md-2"></div>
		  
      </div><!-- col 12 end -->
  </div><!-- row end -->
 </div>



</asp:Content>

