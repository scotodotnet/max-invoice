﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using Altius.BusinessAccessLayer.BALDataAccess;


public partial class master_forms_GoDown : System.Web.UI.Page
{

    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();

        if (!IsPostBack)
        {
            LoadUnit();
            Load_RandomID();
            Page.Title = "ERP Module :: GoDown";
            HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Master"));
            li.Attributes.Add("class", "droplink active open");
        }
       
        Load_Data();

    }
    private void Load_RandomID()
    {
        string query = "";
        DataTable DT = new DataTable();
        string DeptCode = "";
        Decimal DeptCode_Dec = 0;
        query = "Select MAX(CAST(GoDownID AS int)) AS GoDownID from MstGoDown where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        if (DT.Rows.Count != 0)
        {
            DeptCode = DT.Rows[0]["GoDownID"].ToString();
            if (DeptCode != "")
            {
                DeptCode_Dec = Convert.ToDecimal(DeptCode) + 1;
            }
            else
            {
                DeptCode_Dec = 1;
            }

            txtGoDownID.Text = DeptCode_Dec.ToString();

        }
        else
        {
            txtGoDownID.Text = "1";
        }
    }

    public void LoadUnit()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlUnit.Items.Clear();
        query = "Select *from MstUnit where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlUnit.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["UnitName"] = "-Select-";
        dr["UnitName"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlUnit.DataTextField = "UnitName";
        ddlUnit.DataValueField = "UnitName";
        ddlUnit.DataBind();
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        string query = "";
        DataTable DT = new DataTable();
        string SaveMode = "Insert";

        //User Rights Check Start
        bool ErrFlag = false;
        bool Rights_Check = false;
        //if (btnSave.Text == "Update")
        //{
        //    Rights_Check = CommonClass_Function.ModifyRights_Check(SessionCcode, SessionLcode, SessionUserID, "3", "5", "GoDown");
        //    if (Rights_Check == false)
        //    {
        //        ErrFlag = true;
        //        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Modify GoDown Details...');", true);
        //    }
        //}
        //else
        //{
        //    Rights_Check = CommonClass_Function.AddRights_Check(SessionCcode, SessionLcode, SessionUserID, "3", "5", "GoDown");
        //    if (Rights_Check == false)
        //    {
        //        ErrFlag = true;
        //        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Adding New GoDown...');", true);
        //    }
        //}
        //User Rights Check End

        if (!ErrFlag)
        {
            query = "Select * from MstGoDown where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And GoDownID='" + txtGoDownID.Text + "'";
            DT = objdata.RptEmployeeMultipleDetails(query);
            if (DT.Rows.Count != 0)
            {
                if (btnSave.Text == "Update")
                {
                    SaveMode = "Update";
                    query = "Delete from MstGoDown where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And GoDownID='" + txtGoDownID.Text + "'";
                    objdata.RptEmployeeMultipleDetails(query);
                }
                else
                {
                    SaveMode = "Error";

                }
            }
            if (SaveMode != "Error")
            {
                //Insert Compnay Details
                query = "Insert Into MstGoDown(Ccode,Lcode,GoDownID,GoDownName,Location,Unit,UserID,Username)";
                query = query + " Values('" + SessionCcode + "','" + SessionLcode + "','" + txtGoDownID.Text.ToUpper() + "',";
                query = query + "'" + txtGoDownName.Text + "','" + txtLocation.Text + "','" + ddlUnit.SelectedValue + "','" + SessionUserID + "','" + SessionUserName + "')";
                objdata.RptEmployeeMultipleDetails(query);

                if (SaveMode == "Insert")
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('GoDown Details Saved Successfully');", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('GoDown Details Updated Successfully');", true);
                }


                Clear_All_Field();
                btnSave.Text = "Save";
                txtGoDownID.Enabled = false;
                Load_Data();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('GoDown Details Already Saved');", true);
            }
        }

    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }


    protected void btnSearch_Click(object sender, EventArgs e)
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select * from MstGoDown where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And GoDownID='" + txtGoDownID.Text + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            txtGoDownID.Text= DT.Rows[0]["GoDownID"].ToString();
            txtGoDownName.Text=DT.Rows[0]["GoDownName"].ToString();
            txtLocation.Text=DT.Rows[0]["Location"].ToString();
            ddlUnit.SelectedValue = DT.Rows[0]["Unit"].ToString().Trim();
            txtGoDownID.Enabled=false;
           
            btnSave.Text = "Update";
        }
        else
        {
            Clear_All_Field();
        }
    }

    private void Load_Data()
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select * from MstGoDown where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        Repeater1.DataSource = DT;
        Repeater1.DataBind();
    }

    private void Clear_All_Field()
    {
        txtGoDownID.Text = ""; txtGoDownName.Text = ""; txtLocation.Text = "";
        btnSave.Text = "Save";
        txtGoDownID.Enabled = false;
        Load_RandomID();
        LoadUnit();
    }
    protected void GridEditClick(object sender, CommandEventArgs e)
    {
        txtGoDownID.Text = e.CommandName.ToString();
        btnSearch_Click(sender, e);
    }
    protected void GridDeleteClick(object sender, CommandEventArgs e)
    {
        string query = "";

        //User Rights Check Start
        bool ErrFlag = false;
        bool Rights_Check = false;

        Rights_Check = CommonClass_Function.DeleteRights_Check(SessionCcode, SessionLcode, SessionUserID, "3", "5", "GoDown");
        if (Rights_Check == false)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Delete GoDown..');", true);
        }
        //User Rights Check End

        if (!ErrFlag)
        {
            
                DataTable DT = new DataTable();
                query = "Select * from MstGoDown where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And GoDownID='" + e.CommandName.ToString() + "'";
                DT = objdata.RptEmployeeMultipleDetails(query);
                if (DT.Rows.Count != 0)
                {
                    query = "Delete from MstGoDown where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And GoDownID='" + e.CommandName.ToString() + "'";
                    objdata.RptEmployeeMultipleDetails(query);
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('GoDown Details Deleted Successfully');", true);
                    Load_Data();
                    Clear_All_Field();
                }
            }
           
        }
    
}
