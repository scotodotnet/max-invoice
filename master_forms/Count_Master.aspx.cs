﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class master_forms_Count_Master : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string Dept_Code_Delete = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        if (!IsPostBack)
        {
            LoadUnit();
            Load_RandomID();
            Page.Title = "ERP Module :: Count Master";
            HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Master"));
            li.Attributes.Add("class", "droplink active open");
            Load_Data();
        }
       
        Load_Data();
        
    }

    private void Load_RandomID()
    {
        string query = "";
        DataTable DT = new DataTable();
        string DeptCode = "";
        Decimal DeptCode_Dec = 0;
        query = "Select MAX(CAST(CountID AS int)) AS CountID from MstCount where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        if (DT.Rows.Count != 0)
        {
            DeptCode = DT.Rows[0]["CountID"].ToString();
            if (DeptCode != "")
            {
                DeptCode_Dec = Convert.ToDecimal(DeptCode) + 1;
            }
            else
            {
                DeptCode_Dec = 1;
            }

            txtCountCode.Text = DeptCode_Dec.ToString();

        }
        else
        {
            txtCountCode.Text = "1";
        }
    }
    public void LoadUnit()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlUnit.Items.Clear();
        query = "Select *from MstUnit where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlUnit.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["UnitName"] = "-Select-";
        dr["UnitName"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlUnit.DataTextField = "UnitName";
        ddlUnit.DataValueField = "UnitName";
        ddlUnit.DataBind();
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        string query = "";
        DataTable DT = new DataTable();
        string SaveMode = "Insert";
        bool ErrFlag = false;

        //User Rights Check Start

        bool Rights_Check = false;
        //if (btnSave.Text == "Update")
        //{
        //    Rights_Check = CommonClass_Function.ModifyRights_Check(SessionCcode, SessionLcode, SessionUserID, "3", "5", "Count Master");
        //    if (Rights_Check == false)
        //    {
        //        ErrFlag = true;
        //        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Modify Count Master...');", true);
        //    }
        //}
        //else
        //{
        //    Rights_Check = CommonClass_Function.AddRights_Check(SessionCcode, SessionLcode, SessionUserID, "3", "5", "Count Master");
        //    if (Rights_Check == false)
        //    {
        //        ErrFlag = true;
        //        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Adding New Count Master...');", true);
        //    }
        //}
        //User Rights Check End
        if (!ErrFlag)
        {
            query = "Select * from MstCount where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And CountID='" + txtCountCode.Text + "'";
            DT = objdata.RptEmployeeMultipleDetails(query);

            if (DT.Rows.Count != 0)
            {
                if (btnSave.Text == "Update")
                {
                    SaveMode = "Update";
                    query = "Delete from MstCount where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And CountID='" + txtCountCode.Text + "'";
                    objdata.RptEmployeeMultipleDetails(query);
                }
               
                else
                {
                    SaveMode = "Error";
                }
            }
           
            if (SaveMode != "Error")
            {

                //Insert Count Details
                query = "Insert Into MstCount(Ccode,Lcode,CountID,CountName,ActualCount,ShadeNo,Unit,UserID,UserName)Values ('" + SessionCcode + "',";
                query = query + " '" + SessionLcode + "','" + txtCountCode.Text.ToUpper() + "','" + txtCountName.Text.ToUpper() + "',";
                query = query + "'" + txtActualCount.Text + "','" + txtShadeNo.Text + "','" + ddlUnit.SelectedValue + "',";
                query = query + "'" + SessionUserID + "','" + SessionUserName + "')";
                objdata.RptEmployeeMultipleDetails(query);

                if (SaveMode == "Insert")
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Count Master Saved Successfully');", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Count Master Updated Successfully');", true);
                }

                Load_Data();
                Clear_All_Field();
                btnSave.Text = "Save";


            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Count Details Already Exisits');", true);
            }
        }



    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }
    private void Clear_All_Field()
    {
        txtCountCode.Text = "";
        txtCountName.Text = "";
        HiddenCountCode.Value = "";
        txtActualCount.Text = "";
        txtShadeNo.Text = "";
        LoadUnit();
        Load_RandomID();
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select * from MstCount where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And CountID='" + txtCountCode.Text + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            txtCountCode.Text = DT.Rows[0]["CountID"].ToString();
            txtCountName.Text = DT.Rows[0]["CountName"].ToString();
            txtActualCount.Text = DT.Rows[0]["ActualCount"].ToString();
            txtShadeNo.Text = DT.Rows[0]["ShadeNo"].ToString();
            ddlUnit.SelectedValue = DT.Rows[0]["Unit"].ToString();
            btnSave.Text = "Update";
        }
        else
        {
            Clear_All_Field();
        }
    }

  

    private void Load_Data()
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select *from MstCount where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        RptCountMaster.DataSource = DT;
        RptCountMaster.DataBind();
    }

   

    protected void GridEditClick(object sender, CommandEventArgs e)
    {
        MdpCountMaster.Show();
        txtCountCode.Text = e.CommandName.ToString();
        btnSearch_Click(sender, e);

    }

    protected void btnAdd_Click(object sender, EventArgs e)
    {
        txtCountCode.Text = "";
        txtCountName.Text = "";
        txtActualCount.Text = "";
        txtShadeNo.Text = "";
        MdpCountMaster.Show(); 
    }


    protected void GridDeleteClick(object sender, CommandEventArgs e)
    {
        string query = "";

        //User Rights Check Start
        bool ErrFlag = false;
        bool Rights_Check = false;

        Rights_Check = CommonClass_Function.DeleteRights_Check(SessionCcode, SessionLcode, SessionUserID, "3", "5", "Count Master");
        if (Rights_Check == false)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Delete Count Master..');", true);
        }
        //User Rights Check End

        if (!ErrFlag)
        {
            DataTable dtdcheckRejection = new DataTable();
            DataTable dtdcheckIssue = new DataTable();
            DataTable dtdcheckWeight = new DataTable();
            DataTable dtdcurrentStock = new DataTable();

            query = "select CountCode,CountName from Rejection_Entry_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' and CountCode='" + e.CommandName.ToString() + "'";
            dtdcheckRejection = objdata.RptEmployeeMultipleDetails(query);

            query = "select CountCode,CountName from Issue_Entry_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' and CountCode='" + e.CommandName.ToString() + "'";
            dtdcheckIssue = objdata.RptEmployeeMultipleDetails(query);


            if ((dtdcheckRejection.Rows.Count == 0) && (dtdcheckIssue.Rows.Count == 0))
            {
                DataTable DT = new DataTable();
                query = "Select * from MstCount where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And CountID='" + e.CommandName.ToString() + "'";
                DT = objdata.RptEmployeeMultipleDetails(query);
                if (DT.Rows.Count != 0)
                {
                    query = "Delete from MstCount where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And CountID='" + e.CommandName.ToString() + "'";
                    objdata.RptEmployeeMultipleDetails(query);
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Count Details Deleted Successfully');", true);
                    Load_Data();
                    Clear_All_Field();
                }
            }
            else
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Sorry Count Available in some other table..');", true);

            }

        }

      }

    }