﻿<%@ Page Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="ItemSub.aspx.cs" Inherits="master_forms_ItemSub" Title="Untitled Page" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<script src='<%= ResolveUrl("../assets/js/master_list_jquery.min.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/master_list_jquery-ui.min.js") %>'></script>
<link href="<%= ResolveUrl("../assets/css/master_list_jquery-ui.css") %>" rel="stylesheet" type="text/css"/>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#example').dataTable();
                $('.js-states').select2();
            }
        });
    };
</script>

<script type="text/javascript">
    function SaveMsgAlert(msg) {
        swal(msg);
    }
</script>


<div class="page-breadcrumb">
                    <ol class="breadcrumb container">
                   <h4><li class="active">Item Master</li></h4> 
                    </ol>
                </div>
<div id="main-wrapper" class="container">
 <div class="row">
    <div class="col-md-12">
    <div class="col-md-9">
			<div class="panel panel-white">
				<div class="panel panel-primary">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">Item Master</h4>
				</div>
				</div>
				<form class="form-horizontal">
				<div class="panel-body">
					<div class="col-md-12">
					    <div class="row">
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">Item Code <span class="mandatory">*</span></label>
					            <asp:TextBox ID="txtItemCode" MaxLength="30" class="form-control" runat="server" Style="text-transform: uppercase" ></asp:TextBox>
					            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender13" runat="server" FilterType="Numbers, UppercaseLetters, LowercaseLetters, Custom" TargetControlID="txtItemCode" ValidChars=", "/>
                                <asp:TextBox ID="txtSup_Code_Hide" class="form-control" runat="server" Visible="false"></asp:TextBox>
                                <asp:RequiredFieldValidator ControlToValidate="txtItemCode" ValidationGroup="Validate_Field" class="form_error" ID="validate1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>					            
					        </div>
					        
					       
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">Item Name <span class="mandatory">*</span></label>
							    <asp:TextBox ID="txtItemName" class="form-control" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ControlToValidate="txtItemName" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterType="Numbers, UppercaseLetters, LowercaseLetters, Custom" TargetControlID="txtItemName" ValidChars=", "/>

					        </div>
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">Description <span class="mandatory">*</span></label>
					            <asp:TextBox ID="txtDescription" class="form-control" runat="server"></asp:TextBox>
					        </div>
					      				        
					    </div>
					    <div class="row">
                             <div class="form-group col-md-4">
					            <label for="exampleInputName">Type Of Quantity <span class="mandatory">*</span></label>
					            <asp:TextBox ID="txtqty" class="form-control" runat="server"></asp:TextBox>
                                 <asp:RequiredFieldValidator ControlToValidate="txtqty" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator2" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="UppercaseLetters, LowercaseLetters, Custom" TargetControlID="txtqty" ValidChars="/"/>
					        </div>
					    <div class="form-group col-md-4">
					            <label for="exampleInputName">Amount</label>
					            <asp:TextBox ID="txtAmount" class="form-control" runat="server"></asp:TextBox>
					        </div>
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">HSN CODE </label>
                                <asp:TextBox ID="txtHsn" class="form-control" runat="server"></asp:TextBox>
					        </div>
					    </div>
					</div>
					
					<!-- Button start -->
					<div class="col-md-12">
					    <div class="row">					            
					        <div class="txtcenter">
					            <div class="col-sm-11">
					                <asp:Button ID="btnSave" class="btn btn-success"  runat="server" Text="Save" ValidationGroup="Validate_Field" OnClick="btnSave_Click"/>
                                    <%--<asp:Button ID="Button1" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg" runat="server" Text="View" />--%>
                                    <asp:Button ID="btnCancel" class="btn btn-danger" runat="server" Text="Cancel" OnClick="btnCancel_Click"/>
                                </div>
					        </div>
					    </div>
					</div>
					<!-- Button end -->
					<div class="form-group row"></div>
					<!-- table start -->
					<%--<div class="col-md-12">
					    <div class="row">
					        <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
					            <HeaderTemplate>
                                    <table id="example" class="display table">
                                        <thead>
                                            <tr>
                                                <th>Supplier Name</th>
                                                <th>City</th>
                                                <th>Mobile No</th>
                                                <th>Tin No</th>
                                                <th>Mode</th>
                                            </tr>
                                        </thead>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td><%# Eval("SuppName")%></td>
                                        <td><%# Eval("City")%></td>
                                        <td><%# Eval("MobileNo_Join")%></td>
                                        <td><%# Eval("TinNo")%></td>
                                       
                                        <td>
                                            <asp:LinkButton ID="btnEditGrid" class="btn btn-success btn-sm fa fa-pencil"  runat="server" 
                                                Text="" OnCommand="GridEditClick" CommandArgument="Edit" CommandName='<%# Eval("SuppCode")%>'>
                                            </asp:LinkButton>
                                            <asp:LinkButton ID="btnDeleteGrid" class="btn btn-danger btn-sm fa fa-trash-o"  runat="server" 
                                                Text="" OnCommand="GridDeleteClick" CommandArgument="Delete" CommandName='<%# Eval("SuppCode")%>' 
                                                CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this supplier details?');">
                                            </asp:LinkButton>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate></table></FooterTemplate>                                
					        </asp:Repeater>
					    </div>
					</div>--%>
					<!-- table End --> 
						
						
						
						
				</div><!-- panel body end -->
				</form>
			</div><!-- panel white end -->
		    </div><!-- col-9 end -->
		    
		     <!-- Dashboard start -->
		     <div class="col-lg-3 col-md-6">
                            <div class="panel panel-white" style="height: 100%;">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Dashboard Details</h4>
                                    <div class="panel-control">
                                        
                                        
                                    </div>
                                </div>
                                <div class="panel-body">
                                    
                                    
                                </div>
                            </div>
                        </div>  
                        
                        <div class="col-lg-3 col-md-6">
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div class="live-tile flip ha" data-mode="flip" data-speed="750" data-delay="3000">
                                       
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div class="live-tile flip ha" data-mode="flip" data-speed="750" data-delay="3000">
                                       
                                    </div>
                                </div>
                            </div>
                            
                        </div> 
		    <!-- Dashboard End -->
		    
		    <div class="col-md-2"></div>  
            
      
         
      </div><!-- col 12 end -->
  </div><!-- row end -->
 </div><!-- main-wrapper end -->
</asp:Content>

