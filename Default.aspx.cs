﻿using System;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Security.Cryptography;
using System.Collections;
using System.Collections.Specialized;
using System.Text;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class _Default : System.Web.UI.Page 
{
    protected System.Resources.ResourceManager rm;
    protected string PayrollRegisterContentMeta, pageTitle;
    System.Globalization.CultureInfo culterInfo = new System.Globalization.CultureInfo("en-GB");
    BALDataAccess objdata = new BALDataAccess();
    protected void Page_Load(object sender, EventArgs e)
    {
        Session.Remove("UserId");
        Session.Remove("RoleCode");
        Session.Remove("Isadmin");
        Session.Remove("Usernmdisplay");
        Session.Remove("Ccode");
        Session.Remove("Lcode");
        Session.Remove("FinYearCode");
        Session.Remove("FinYear");
        if (!IsPostBack)
        {
            ErrorDisplay.Visible = false;
            Load_company();
            ddlCode_SelectedIndexChanged(sender, e);
            ddlLocation_SelectedIndexChanged(sender, e);
            //Financial_Year();
            Module_Login_Table(sender, e);
        }
    }

    private void Module_Login_Table(object sender, EventArgs e)
    {
        string query = "";
        DataTable DT_Module = new DataTable();
        query = "Select * from [Max_Rights]..Module_Open_User";
        DT_Module = objdata.RptEmployeeMultipleDetails(query);
        if (DT_Module.Rows.Count != 0)
        {
            ddlCode.SelectedValue = DT_Module.Rows[0]["CompCode"].ToString();
            ddlLocation.SelectedValue = DT_Module.Rows[0]["LocCode"].ToString();
            txtusername.Text = DT_Module.Rows[0]["UserName"].ToString();
            txtpassword.Text = UTF8Decryption(DT_Module.Rows[0]["Password"].ToString()).ToString();
            ddlFinYear.SelectedValue = DT_Module.Rows[0]["Fin_Year"].ToString();
            query = "Delete from [Max_Rights]..Module_Open_User";
            objdata.RptEmployeeMultipleDetails(query);

            btnLogin_Click(sender, e);

        }
    }
    private static string UTF8Encryption(string password)
    {
        string strmsg = string.Empty;
        byte[] encode = new byte[password.Length];
        encode = Encoding.UTF8.GetBytes(password);
        strmsg = Convert.ToBase64String(encode);
        return strmsg;
    }

    private static string UTF8Decryption(string encryptpwd)
    {
        string decryptpwd = string.Empty;
        UTF8Encoding encodepwd = new UTF8Encoding();
        Decoder Decode = encodepwd.GetDecoder();
        byte[] todecode_byte = Convert.FromBase64String(encryptpwd);
        int charCount = Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);
        char[] decoded_char = new char[charCount];
        Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);
        decryptpwd = new String(decoded_char);
        return decryptpwd;
    }
    public void Load_company()
    {
        DataTable dt = new DataTable();
        dt = objdata.Dropdown_Company();
        ddlCode.DataSource = dt;
        //DataRow dr = dt.NewRow();
        //dr["Ccode"] = "Company Name";
        //dr["Cname"] = "Company Name";
        //dt.Rows.InsertAt(dr, 0);
        ddlCode.DataTextField = "Cname";
        ddlCode.DataValueField = "Ccode";
        ddlCode.DataBind();
    }

    public void Load_Location()
    {
        //DataTable dtempty = new DataTable();
        //ddlLocation.DataSource = dtempty;
        //ddlLocation.DataBind();
        //DataTable dt = new DataTable();
        ////string[] Company;
        ////Company = ddlCode.SelectedItem.Text.ToString().Split('-');
        //dt = objdata.dropdown_loc(ddlCode.SelectedValue);
        //ddlLocation.DataSource = dt;
        //DataRow dr = dt.NewRow();
        //dr["Location"] = "Location";
        //dr["LCode"] = "Location";
        //dt.Rows.InsertAt(dr, 0);

        //ddlLocation.DataTextField = "Location";
        //ddlLocation.DataValueField = "LCode";
        //ddlLocation.DataBind();
    }

    protected void btnLogin_Click(object sender, EventArgs e)
    {
        string query = "";
        bool ErrFlag = false;
        ErrorDisplay.Visible = false;
        string Verification = objdata.Verification_verify();
        UserRegistrationClass objuser = new UserRegistrationClass();
        if (txtusername.Text.Trim() == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter the User Name ');", true);
            ErrFlag = true;
        }
        else if (txtpassword.Text.Trim() == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter the Password ');", true);
            ErrFlag = true;
        }

        if (!ErrFlag)
        {
            Session["Rights"] = "Max_Rights";
            DataTable dt_v = new DataTable();
            if (txtusername.Text.Trim() == "Scoto")
            {
                Session["UserId"] = txtusername.Text.Trim();
                objuser.UserCode = txtusername.Text.Trim();
                objuser.Password = UTF8Encryption(txtpassword.Text.Trim());
                string pwd = UTF8Encryption(txtpassword.Text);
                DataTable dt = new DataTable();
                query = "Select UserCode,UserName,Password,IsAdmin,LocationCode,CompCode,FormID,FormName from [Max_Rights]..MstUsers where UserCode='" + objuser.UserCode + "' and Password='" + objuser.Password + "'";
                dt = objdata.RptEmployeeMultipleDetails(query);
                //dt = objdata.AltiusLogin(objuser);
                if (dt.Rows.Count > 0)
                {

                    if ((dt.Rows[0]["UserCode"].ToString().Trim() == txtusername.Text.Trim()) && (dt.Rows[0]["Password"].ToString().Trim() == pwd))
                    {
                        Session["Isadmin"] = dt.Rows[0]["IsAdmin"].ToString().Trim();
                        Session["Usernmdisplay"] = txtusername.Text.Trim();
                        Session["Ccode"] = ddlCode.SelectedValue;
                        Session["Lcode"] = ddlLocation.SelectedValue;
                        //string[] CompName = ddlCode.SelectedItem.Text.Split('-');
                        //Session["CompName"] = CompName[1].ToString();
                        //string formID = dt.Rows[0]["FormID"].ToString().Trim();
                        //string formName = dt.Rows[0]["FormName"].ToString().Trim();
                        Session["FinYearCode"] = ddlFinYear.SelectedValue;
                        Session["FinYear"] = ddlFinYear.SelectedItem.Text.ToString();

                        if (Session["Isadmin"].ToString() == "1")
                        {
                            Session["RoleCode"] = "1";
                        }
                        else
                        {
                            Session["RoleCode"] = dt.Rows[0]["IsAdmin"].ToString().Trim();
                        }
    
                        Response.Redirect("Dashboard.aspx");

                    }
                    else
                    {
                        ErrorDisplay.Visible = true;
                        txtusername.Focus();
                        //ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('User Name and password Incorrect ');", true);
                    }
                }
            }
            else
            {
                string date_1 = "";
                dt_v = objdata.Value_Verify();
                if (dt_v.Rows.Count > 0)
                {
                    date_1 = (dt_v.Rows[0]["No_dVal"].ToString() + "-" + dt_v.Rows[0]["No_MVal"].ToString() + "-" + dt_v.Rows[0]["No_YVal"].ToString()).ToString();
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Please Update your Licence Key');", true);
                    return;
                }
                if (Verification.Trim() != "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Please Update your Licence Key');", true);
                    return;
                }
                string dtserver = objdata.ServerDate();
                if (Convert.ToDateTime(dtserver) < Convert.ToDateTime(date_1))
                {
                    if (txtusername.Text.Trim() == "")
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter the User Name ');", true);
                        ErrFlag = true;
                    }
                    else if (txtpassword.Text.Trim() == "")
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter the Password ');", true);
                        ErrFlag = true;
                    }
                    else if (ddlLocation.SelectedValue == "")
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select the Location ');", true);
                        ErrFlag = true;
                    }
                    else if (ddlFinYear.SelectedValue == "")
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select the Fin. Year');", true);
                        ErrFlag = true;
                    }
                    if (!ErrFlag)
                    {
                        Session["UserId"] = txtusername.Text.Trim();
                        objuser.UserCode = txtusername.Text.Trim();
                        objuser.Password = UTF8Encryption(txtpassword.Text.Trim());
                        string pwd = UTF8Encryption(txtpassword.Text);
                        objuser.Ccode = ddlCode.SelectedValue;
                        objuser.Lcode = ddlLocation.SelectedValue;
                        DataTable dt = new DataTable();
                        query = "Select UserCode,UserName,Password,IsAdmin,LocationCode,CompCode,FormID,FormName,Link_Url from [Max_Rights]..MstUsers where UserCode='" + objuser.UserCode + "' and Password='" + objuser.Password + "'";
                        query = query + " And CompCode='" + objuser.Ccode + "' And LocationCode='" + objuser.Lcode + "'";
                        dt = objdata.RptEmployeeMultipleDetails(query);
                        //dt = objdata.UserLogin(objuser);
                        if (dt.Rows.Count > 0)
                        {
                            if ((dt.Rows[0]["UserCode"].ToString().Trim() == txtusername.Text.Trim()) && (dt.Rows[0]["Password"].ToString().Trim() == pwd) && (dt.Rows[0]["CompCode"].ToString().Trim() == ddlCode.SelectedValue) && (dt.Rows[0]["LocationCode"].ToString().Trim() == ddlLocation.SelectedValue))
                            {
                                Session["Isadmin"] = dt.Rows[0]["IsAdmin"].ToString().Trim();
                                Session["Usernmdisplay"] = txtusername.Text.Trim();
                                Session["Ccode"] = dt.Rows[0]["CompCode"].ToString().Trim();
                                Session["Lcode"] = dt.Rows[0]["LocationCode"].ToString().Trim();
                                //string[] CompName = ddlCode.SelectedItem.Text.Split('-');
                                //Session["CompName"] = CompName[1].ToString();
                                //string formID = dt.Rows[0]["FormID"].ToString().Trim();
                                //string formName = dt.Rows[0]["FormName"].ToString().Trim();
                                //string link_Url = dt.Rows[0]["Link_Url"].ToString().Trim();

                                Session["FinYearCode"] = ddlFinYear.SelectedValue;
                                Session["FinYear"] = ddlFinYear.SelectedItem.Text.ToString();

                                if (Session["Isadmin"].ToString() == "1")
                                {
                                    Session["RoleCode"] = "1";
                                }
                                else
                                {
                                    Session["RoleCode"] = dt.Rows[0]["IsAdmin"].ToString().Trim();
                                }
                                //get User After Login Default Page Load link Start
                                if (Session["Isadmin"].ToString() == "1")
                                {
                                    Response.Redirect("Dashboard.aspx");
                                }
                                else
                                {
                                    string link_Url = "Dashboard.aspx";
                                    //get User After Login Default Page Load link
                                    DataTable dt1 = new DataTable();
                                    DataTable dt2 = new DataTable();
                                    query = "select * from [Max_Rights]..Company_user_default_page where CompCode='" + objuser.Ccode + "' And LocCode='" + objuser.Lcode + "' And UserName='" + objuser.UserCode + "'";
                                    query = query + " And ModuleName='Stores'";
                                    dt1 = objdata.RptEmployeeMultipleDetails(query);

                                    if (dt1.Rows.Count > 0)
                                    {
                                        //Get Rights Check
                                        if (dt1.Rows[0]["FormLink"].ToString() != "")
                                        {
                                            query = "Select * from [Max_Rights]..Company_Module_User_Rights where CompCode='" + objuser.Ccode + "' And LocCode='" + objuser.Lcode + "'";
                                            query = query + " And ModuleName='Stores' And MenuName='" + dt1.Rows[0]["MenuName"].ToString() + "' And FormName='" + dt1.Rows[0]["FormName"].ToString() + "'";
                                            dt2 = objdata.RptEmployeeMultipleDetails(query);
                                            if (dt2.Rows.Count != 0)
                                            {
                                                if (dt2.Rows[0]["AddRights"].ToString() == "1" || dt2.Rows[0]["ModifyRights"].ToString() == "1" || dt2.Rows[0]["DeleteRights"].ToString() == "1" || dt2.Rows[0]["ViewRights"].ToString() == "1" || dt2.Rows[0]["ApproveRights"].ToString() == "1" || dt2.Rows[0]["PrintRights"].ToString() == "1")
                                                {
                                                    link_Url = dt1.Rows[0]["FormLink"].ToString();
                                                    Response.Redirect("" + link_Url + "");
                                                }
                                                else
                                                {
                                                    Response.Redirect("Dashboard.aspx");
                                                }
                                            }
                                            else
                                            {
                                                Response.Redirect("Dashboard.aspx");
                                            }
                                        }
                                        else
                                        {
                                            Response.Redirect("Dashboard.aspx");
                                        }
                                    }
                                    else
                                    {
                                        Response.Redirect("Dashboard.aspx");
                                    }
                                }
                                //get User After Login Default Page Load link End
                                //Response.Redirect("Dashboard.aspx");
                            }
                        }
                        else
                        {
                            ErrorDisplay.Visible = true;
                            txtusername.Focus();
                            //ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('User Name and password Incorrect ');", true);
                        }
                    }
                }
                else
                {
                    //objdata.Insert_verificationValue();
                    //ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Please Update your Licence Key');", true);
                }

                //Session["UserId"] = txtusername.Text;
                //objuser.UserCode = txtusername.Text;
                //objuser.Password = s_hex_md5(txtpassword.Text);
                //objuser.Ccode = ddlCode.SelectedValue.Trim();
                //objuser.Lcode = ddlLocation.SelectedValue.Trim();
                //string pwd = s_hex_md5(txtpassword.Text);
                //string UserName = objdata.username(objuser);
                //string password = objdata.UserPassword(objuser);

                //if (UserName == txtusername.Text && password == pwd)
                //{

                //    string UserNameDis = objdata.usernameDisplay(objuser);
                //    string isadmin = objdata.isAdmin(txtusername.Text);
                //    string see = isadmin;
                //    int dd = Convert.ToInt32(see);
                //    Session["Isadmin"] = Convert.ToString(dd);
                //    Session["Usernmdisplay"] = UserNameDis;
                //    if (see.Trim() == "3")
                //    {
                //        Response.Redirect("Administrator.aspx");
                //    }
                //    else
                //    {
                //        if (UserName == "Admin")
                //        {
                //            Session["RoleCode"] = "1";
                //        }
                //        else
                //        {
                //            Session["RoleCode"] = "2";
                //        }
                //        Response.Redirect("EmployeeRegistration.aspx");
                //    }
                //}
                //else
                //{
                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('User Name and password Incorrect ');", true);
                //}
            }
        }

        //if (isadmin = "1")
        //{

        //}



    }

    public static String s_hex_md5(String originalPassword)
    {
        UTF8Encoding encoder = new UTF8Encoding();
        MD5 md5 = new MD5CryptoServiceProvider();

        Byte[] hashedbytes = md5.ComputeHash(encoder.GetBytes(originalPassword));
        return BitConverter.ToString(hashedbytes).Replace("-", "").ToLower();
    }
    //protected void ddlLocation_SelectedIndexChanged(object sender, EventArgs e)
    //{
        
       
    //}
    protected void ddlCode_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable dtempty = new DataTable();
        ddlLocation.DataSource = dtempty;
        ddlLocation.DataBind();
        DataTable dt = new DataTable();
        //string[] Company;
        //Company = ddlCode.SelectedItem.Text.ToString().Split('-');
        dt = objdata.dropdown_loc(ddlCode.SelectedValue);
        ddlLocation.DataSource = dt;
        //DataRow dr = dt.NewRow();
        //dr["Location"] = "Location";
        //dr["LCode"] = "Location";
        //dt.Rows.InsertAt(dr, 0);
       
        ddlLocation.DataTextField = "Location";
        ddlLocation.DataValueField = "LCode";
        ddlLocation.DataBind();
    }

    protected void ddlLocation_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable dtempty = new DataTable();
        ddlFinYear.DataSource = dtempty;
        ddlFinYear.DataBind();
        DataTable dt = new DataTable();

        string query = "";
        query = "Select YearCode,FinacialYear from MstFinacialPeriod where Ccode='" + ddlCode.SelectedValue + "' And Lcode='" + ddlLocation.SelectedValue + "' Order by YearCode desc";
        dt = objdata.RptEmployeeMultipleDetails(query);

        ddlFinYear.DataSource = dt;
        ddlFinYear.DataTextField = "FinacialYear";
        ddlFinYear.DataValueField = "YearCode";
        ddlFinYear.DataBind();
    }

    public void Financial_Year()
    {
        //String CurrentYear1;
        //int CurrentYear;
        //CurrentYear1 = DateTime.Now.Year.ToString();
        //CurrentYear = Convert.ToUInt16(CurrentYear1.ToString());
        //if (DateTime.Now.Month >= 1 && DateTime.Now.Month <= 3)
        //{
        //    CurrentYear = CurrentYear - 1;
        //}
        //for (int i = 0; i < 1; i++)
        //{
        //    string tt = (CurrentYear1 + "_" + (CurrentYear + 1));
        //    ddlFinYear.Items.Add(tt.ToString());
        //    ////ddlFinancialYeartab3.Items.Add(tt.ToString());
        //    ddlFinYear.SelectedValue = "1";
        //    CurrentYear = CurrentYear - 1;
        //    string cy = Convert.ToString(CurrentYear);
        //    CurrentYear1 = cy;
        //}

        DataTable dtempty = new DataTable();
        ddlFinYear.DataSource = dtempty;
        ddlFinYear.DataBind();
        DataTable dt = new DataTable();

        string query = "";
        query = "Select YearCode,FinacialYear from MstFinacialPeriod where Ccode='" + ddlCode.SelectedValue + "' And Lcode='" + ddlLocation.SelectedValue + "' Order by YearCode Asc";
        dt = objdata.RptEmployeeMultipleDetails(query);

        ddlFinYear.DataSource = dt;
        ddlFinYear.DataTextField = "FinacialYear";
        ddlFinYear.DataValueField = "YearCode";
        ddlFinYear.DataBind();
    }




    //protected void ddlLocation_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    DataTable dtempty = new DataTable();
    //    ddlFinYear.DataSource = dtempty;
    //    ddlFinYear.DataBind();
    //    DataTable dt = new DataTable();

    //    string query = "";
    //    query = "Select YearCode,FinacialYear from MstFinacialPeriod where Ccode='" + ddlCode.SelectedValue + "' And Lcode='" + ddlLocation.SelectedValue + "' Order by YearCode Asc";
    //    dt = objdata.RptEmployeeMultipleDetails(query);

    //    ddlFinYear.DataSource = dt;
    //    ddlFinYear.DataTextField = "FinacialYear";
    //    ddlFinYear.DataValueField = "YearCode";
    //    ddlFinYear.DataBind();
    //}





}
