﻿<%@ WebHandler Language="C#" Class="Handler" %>
using System;
using System.Web;
using System.IO;
using System.Data;
using System.Data.SqlClient;

public class Handler : IHttpHandler {  

        public void ProcessRequest(HttpContext context)
        {
            if ((context.Session["ImageBytes"]) != null)
        {
            byte[] image = (byte[])(context.Session["ImageBytes"]);
            context.Response.ContentType = "image/JPEG";
            context.Response.BinaryWrite(image);
        }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }