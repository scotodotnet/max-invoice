﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Web.UI.DataVisualization.Charting;
using System.Globalization;

using Payroll;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class Dashboard : System.Web.UI.Page
{

    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionEnquiryNo;
    string SessionFinYearCode;
    string SessionFinYearVal;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }

        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();
        if (!IsPostBack)
        {
            Page.Title = Page.Title = "ERP Module";
            HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_DashBoard"));
            li.Attributes.Add("class", "droplink active open");
            Year_Add();
            //StationAndVendor_Chart_Load();
            //Varieties_Chart_Load_Full();
            //Varieties_Current_Stock_Chart_Load_Full();
            //Agent_Purchase_Chart_Load();
        }
    }

    private void Year_Add()
    {
        txtYear.Items.Clear();
        int currentYear = Utility.GetFinancialYear;
        for (int i = 0; i < 10; i++)
        {
            txtYear.Items.Add(new ListItem(currentYear.ToString(), currentYear.ToString()));
            currentYear = currentYear - 1;
        }
        //Varieties Year Add
        txtYear_Var.Items.Clear();
        currentYear = Utility.GetFinancialYear;
        for (int i = 0; i < 10; i++)
        {
            txtYear_Var.Items.Add(new ListItem(currentYear.ToString(), currentYear.ToString()));
            currentYear = currentYear - 1;
        }

        //txtAgent_Year Add
        txtAgent_Year.Items.Clear();
        currentYear = Utility.GetFinancialYear;
        for (int i = 0; i < 10; i++)
        {
            txtAgent_Year.Items.Add(new ListItem(currentYear.ToString(), currentYear.ToString()));
            currentYear = currentYear - 1;
        }
    }

    private void StationAndVendor_Chart_Load()
    {
        DataTable dt = new DataTable();
        string query = "";
        query = "Select sum(NoOfBale) as Bale_Count,StationName from CottonInwards";
        query = query + " where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        query = query + " And FinYearCode='" + SessionFinYearCode + "'";
        query = query + " group by StationName Order by StationName Asc";
        dt = objdata.RptEmployeeMultipleDetails(query);
        Station_Vendor_Chart.DataSource = dt;
        Station_Vendor_Chart.Series["Series1"].XValueMember = "StationName";
        Station_Vendor_Chart.Series["Series1"].YValueMembers = "Bale_Count";
        this.Station_Vendor_Chart.Series[0]["PieLabelStyle"] = "Outside";
        this.Station_Vendor_Chart.ChartAreas[0].Area3DStyle.Enable3D = true;
        this.Station_Vendor_Chart.ChartAreas[0].Area3DStyle.Inclination = 1;
        this.Station_Vendor_Chart.Series[0].BorderWidth = 1;
        this.Station_Vendor_Chart.Series[0].BorderColor = System.Drawing.Color.FromArgb(26, 59, 105);
        this.Station_Vendor_Chart.Legends.Add("Legend1");
        this.Station_Vendor_Chart.Legends[0].Enabled = true;
        this.Station_Vendor_Chart.Legends[0].Docking = Docking.Bottom;
        this.Station_Vendor_Chart.Legends[0].Alignment = System.Drawing.StringAlignment.Center;
        this.Station_Vendor_Chart.Series[0].LegendText = "#PERCENT";
        //this.DeptChart.DataManipulator.Sort(PointSortOrder.Descending, Chart2.Series[0]);
        Station_Vendor_Chart.DataBind();
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {

        Station_Vendor_Chart.Legends.Clear();
        DataTable dt = new DataTable();
        string query = "";
        if (RdpChartType.SelectedValue == "1")
        {
            query = "Select StationName,sum(NoOfBale) as Bale_Count from CottonInwards";
            query = query + " where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
            query = query + " And FinYearCode='" + SessionFinYearCode + "'";
            query = query + " And year(CONVERT(DATETIME,Ctn_Inwards_Date, 103))='" + txtYear.Text + "'";

            if (txtMonth.Text != "--Select--")
            {
                query = query + " And DATENAME(MONTH, CONVERT(DATETIME,Ctn_Inwards_Date, 103))='" + txtMonth.Text + "'";
            }
            query = query + " group by StationName";
        }
        else
        {
            query = "Select Supp_Name as StationName,sum(NoOfBale) as Bale_Count from CottonInwards";
            query = query + " where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
            query = query + " And FinYearCode='" + SessionFinYearCode + "'";
            query = query + " And year(CONVERT(DATETIME,Ctn_Inwards_Date, 103))='" + txtYear.Text + "'";

            if (txtMonth.Text != "--Select--")
            {
                query = query + " And DATENAME(MONTH, CONVERT(DATETIME,Ctn_Inwards_Date, 103))='" + txtMonth.Text + "'";
            }
            query = query + " group by Supp_Name";
        }
        dt = objdata.RptEmployeeMultipleDetails(query);
        Station_Vendor_Chart.DataSource = dt;
        Station_Vendor_Chart.Series["Series1"].XValueMember = "StationName";
        Station_Vendor_Chart.Series["Series1"].YValueMembers = "Bale_Count";
        this.Station_Vendor_Chart.Series[0]["PieLabelStyle"] = "Outside";
        this.Station_Vendor_Chart.ChartAreas[0].Area3DStyle.Enable3D = true;
        this.Station_Vendor_Chart.ChartAreas[0].Area3DStyle.Inclination = 1;
        this.Station_Vendor_Chart.Series[0].BorderWidth = 1;
        this.Station_Vendor_Chart.Series[0].BorderColor = System.Drawing.Color.FromArgb(26, 59, 105);
        this.Station_Vendor_Chart.Legends.Add("Legend1");
        this.Station_Vendor_Chart.Legends[0].Enabled = true;
        this.Station_Vendor_Chart.Legends[0].Docking = Docking.Bottom;
        this.Station_Vendor_Chart.Legends[0].Alignment = System.Drawing.StringAlignment.Center;
        this.Station_Vendor_Chart.Series[0].LegendText = "#PERCENT";
        //this.DeptChart.DataManipulator.Sort(PointSortOrder.Descending, Chart2.Series[0]);
        Station_Vendor_Chart.DataBind();

    }

    private void Varieties_Chart_Load_Full()
    {
        DataTable dt = new DataTable();
        string query = "";
        query = "Select SUM(cast(IM.TotalBale as decimal(18,2))) as Bale_Count,IMS.VarietyName from Issue_Entry_Main IM";
        query = query + " inner join Issue_Entry_Main_Sub IMS on IM.Ccode=IMS.Ccode And IM.Lcode=IMS.Lcode";
        query = query + " And IM.FinYearCode=IMS.FinYearCode And IM.Issue_Entry_No=IMS.Issue_Entry_No";
        query = query + " where IM.Ccode='" + SessionCcode + "' And IM.Lcode='" + SessionLcode + "' And IM.FinYearCode='" + SessionFinYearCode + "'";
        query = query + " And IMS.Ccode='" + SessionCcode + "' And IMS.Lcode='" + SessionLcode + "' And IMS.FinYearCode='" + SessionFinYearCode + "'";
        query = query + " group by VarietyName Order by VarietyName";
        dt = objdata.RptEmployeeMultipleDetails(query);
        Varieties_Chart.DataSource = dt;
        Varieties_Chart.Series["Series1"].XValueMember = "VarietyName";
        Varieties_Chart.Series["Series1"].YValueMembers = "Bale_Count";
        this.Varieties_Chart.Series[0]["PieLabelStyle"] = "Outside";
        this.Varieties_Chart.ChartAreas[0].Area3DStyle.Enable3D = true;
        this.Varieties_Chart.ChartAreas[0].Area3DStyle.Inclination = 1;
        this.Varieties_Chart.Series[0].BorderWidth = 1;
        this.Varieties_Chart.Series[0].BorderColor = System.Drawing.Color.FromArgb(26, 59, 105);
        this.Varieties_Chart.Legends.Add("Legend1");
        this.Varieties_Chart.Legends[0].Enabled = true;
        this.Varieties_Chart.Legends[0].Docking = Docking.Bottom;
        this.Varieties_Chart.Legends[0].Alignment = System.Drawing.StringAlignment.Center;
        this.Varieties_Chart.Series[0].LegendText = "#PERCENT";
        //this.Varieties_Chart.DataManipulator.Sort(PointSortOrder.Descending, Chart2.Series[0]);
        Varieties_Chart.DataBind();
    }

    protected void btnVar_Search_Click(object sender, EventArgs e)
    {

        Varieties_Chart.Legends.Clear();
        DataTable dt = new DataTable();
        string query = "";
        if (RdpChartIssueType.SelectedValue == "1")
        {
            query = "Select SUM(cast(IM.TotalBale as decimal(18,2))) as Bale_Count,IMS.VarietyName  from Issue_Entry_Main IM";
            query = query + " inner join Issue_Entry_Main_Sub IMS on IM.Ccode=IMS.Ccode And IM.Lcode=IMS.Lcode";
            query = query + " And IM.FinYearCode=IMS.FinYearCode And IM.Issue_Entry_No=IMS.Issue_Entry_No";
            query = query + " where IM.Ccode='" + SessionCcode + "' And IM.Lcode='" + SessionLcode + "'";
            query = query + " And IM.FinYearCode='" + SessionFinYearCode + "'";
            query = query + " And IMS.Ccode='" + SessionCcode + "' And IMS.Lcode='" + SessionLcode + "'";
            query = query + " And IMS.FinYearCode='" + SessionFinYearCode + "'";
            query = query + " And year(CONVERT(DATETIME,IM.Issue_Entry_Date, 103))='" + txtYear.Text + "'";
            query = query + " And year(CONVERT(DATETIME,IMS.Issue_Entry_Date, 103))='" + txtYear.Text + "'";

            if (txtMonth.Text != "--Select--")
            {
                query = query + " And DATENAME(MONTH, CONVERT(DATETIME,IM.Issue_Entry_Date, 103))='" + txtMonth.Text + "'";
                query = query + " And DATENAME(MONTH, CONVERT(DATETIME,IMS.Issue_Entry_Date, 103))='" + txtMonth.Text + "'";
            }
            query = query + " group by IMS.VarietyName Order by IMS.VarietyName";
        }
        else
        {
            query = "Select SUM(cast(IM.TotalBale as decimal(18,2))) as Bale_Count,IMS.Supp_Name as VarietyName  from Issue_Entry_Main IM";
            query = query + " inner join Issue_Entry_Main_Sub IMS on IM.Ccode=IMS.Ccode And IM.Lcode=IMS.Lcode";
            query = query + " And IM.FinYearCode=IMS.FinYearCode And IM.Issue_Entry_No=IMS.Issue_Entry_No";
            query = query + " where IM.Ccode='" + SessionCcode + "' And IM.Lcode='" + SessionLcode + "'";
            query = query + " And IM.FinYearCode='" + SessionFinYearCode + "'";
            query = query + " And IMS.Ccode='" + SessionCcode + "' And IMS.Lcode='" + SessionLcode + "'";
            query = query + " And IMS.FinYearCode='" + SessionFinYearCode + "'";
            query = query + " And year(CONVERT(DATETIME,IM.Issue_Entry_Date, 103))='" + txtYear.Text + "'";
            query = query + " And year(CONVERT(DATETIME,IMS.Issue_Entry_Date, 103))='" + txtYear.Text + "'";

            if (txtMonth.Text != "--Select--")
            {
                query = query + " And DATENAME(MONTH, CONVERT(DATETIME,IM.Issue_Entry_Date, 103))='" + txtMonth.Text + "'";
                query = query + " And DATENAME(MONTH, CONVERT(DATETIME,IMS.Issue_Entry_Date, 103))='" + txtMonth.Text + "'";
            }
            query = query + " group by IMS.Supp_Name Order by IMS.Supp_Name";
        }
        dt = objdata.RptEmployeeMultipleDetails(query);
        Varieties_Chart.DataSource = dt;
        Varieties_Chart.Series["Series1"].XValueMember = "VarietyName";
        Varieties_Chart.Series["Series1"].YValueMembers = "Bale_Count";
        this.Varieties_Chart.Series[0]["PieLabelStyle"] = "Outside";
        this.Varieties_Chart.ChartAreas[0].Area3DStyle.Enable3D = true;
        this.Varieties_Chart.ChartAreas[0].Area3DStyle.Inclination = 1;
        this.Varieties_Chart.Series[0].BorderWidth = 1;
        this.Varieties_Chart.Series[0].BorderColor = System.Drawing.Color.FromArgb(26, 59, 105);
        this.Varieties_Chart.Legends.Add("Legend1");
        this.Varieties_Chart.Legends[0].Enabled = true;
        this.Varieties_Chart.Legends[0].Docking = Docking.Bottom;
        this.Varieties_Chart.Legends[0].Alignment = System.Drawing.StringAlignment.Center;
        this.Varieties_Chart.Series[0].LegendText = "#PERCENT";
        //this.Varieties_Chart.DataManipulator.Sort(PointSortOrder.Descending, Chart2.Series[0]);
        Varieties_Chart.DataBind();

    }

    private void Varieties_Current_Stock_Chart_Load_Full()
    {
        DataTable dt = new DataTable();
        string query = "";
        query = "Select * from (";
        query = query + " Select MV.VarietyName as V_Name,";
        query = query + " (Select COUNT(BaleNo) from Temp_Upload_Stock TS where TS.VariteyName=MV.VarietyName And TS.Ccode=MV.Ccode And TS.Lcode=MV.Lcode";
        query = query + " And TS.Ccode='" + SessionCcode + "' And TS.Lcode='" + SessionLcode + "')";
        query = query + " as OP_Stock,";
        query = query + " (Select COUNT(BaleNo) from Weight_List_Main_Sub WL where WL.VarietyName=MV.VarietyName And WL.Ccode=MV.Ccode And WL.Lcode=MV.Lcode";
        query = query + " And WL.Ccode='" + SessionCcode + "' And WL.Lcode='" + SessionLcode + "' And WL.FinYearCode='" + SessionFinYearCode + "')";
        query = query + " as Pur_Stock,";
        query = query + " (Select COUNT(BaleNo) from Issue_Entry_Main_Sub ISE where ISE.VarietyName=MV.VarietyName And ISE.Ccode=MV.Ccode And ISE.Lcode=MV.Lcode";
        query = query + " And ISE.Ccode='" + SessionCcode + "' And ISE.Lcode='" + SessionLcode + "' And ISE.FinYearCode='" + SessionFinYearCode + "')";
        query = query + " as Issue_Stock,";
        query = query + " ((";
        query = query + " (Select COUNT(BaleNo) from Temp_Upload_Stock TS where TS.VariteyName=MV.VarietyName And TS.Ccode=MV.Ccode And TS.Lcode=MV.Lcode";
        query = query + " And TS.Ccode='" + SessionCcode + "' And TS.Lcode='" + SessionLcode + "')";
        query = query + " +";
        query = query + " (Select COUNT(BaleNo) from Weight_List_Main_Sub WL where WL.VarietyName=MV.VarietyName And WL.Ccode=MV.Ccode And WL.Lcode=MV.Lcode";
        query = query + " And WL.Ccode='" + SessionCcode + "' And WL.Lcode='" + SessionLcode + "' And WL.FinYearCode='" + SessionFinYearCode + "')";
        query = query + " ) -";
        query = query + " (Select COUNT(BaleNo) from Issue_Entry_Main_Sub ISE where ISE.VarietyName=MV.VarietyName And ISE.Ccode=MV.Ccode And ISE.Lcode=MV.Lcode";
        query = query + " And ISE.Ccode='" + SessionCcode + "' And ISE.Lcode='" + SessionLcode + "' And ISE.FinYearCode='" + SessionFinYearCode + "')";
        query = query + " ) as Current_Stock";
        query = query + " from MstVariety MV where MV.Ccode='" + SessionCcode + "' And MV.Lcode='" + SessionLcode + "'";
        query = query + " ) as P where Current_Stock > 0 order by V_Name Asc";

        dt = objdata.RptEmployeeMultipleDetails(query);
        Varieties_Current_Stock_Chart.DataSource = dt;
        Varieties_Current_Stock_Chart.Series["Series1"].XValueMember = "V_Name";
        Varieties_Current_Stock_Chart.Series["Series1"].YValueMembers = "Current_Stock";
        this.Varieties_Current_Stock_Chart.Series[0]["PieLabelStyle"] = "Outside";
        this.Varieties_Current_Stock_Chart.ChartAreas[0].Area3DStyle.Enable3D = true;
        this.Varieties_Current_Stock_Chart.ChartAreas[0].Area3DStyle.Inclination = 1;
        this.Varieties_Current_Stock_Chart.Series[0].BorderWidth = 1;
        this.Varieties_Current_Stock_Chart.Series[0].BorderColor = System.Drawing.Color.FromArgb(26, 59, 105);
        this.Varieties_Current_Stock_Chart.Legends.Add("Legend1");
        this.Varieties_Current_Stock_Chart.Legends[0].Enabled = true;
        this.Varieties_Current_Stock_Chart.Legends[0].Docking = Docking.Bottom;
        this.Varieties_Current_Stock_Chart.Legends[0].Alignment = System.Drawing.StringAlignment.Center;
        this.Varieties_Current_Stock_Chart.Series[0].LegendText = "#PERCENT";
        //this.Varieties_Current_Stock_Chart.DataManipulator.Sort(PointSortOrder.Descending, Chart2.Series[0]);
        Varieties_Current_Stock_Chart.DataBind();
    }

    protected void btnAgentPurchaseChart_Click(object sender, EventArgs e)
    {

        Agent_Purchase_Chart.Legends.Clear();
        DataTable dt = new DataTable();
        string query = "";
        query = "Select AgentName,sum(NoOfBale) as Bale_Count from CottonInwards";
        query = query + " where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        query = query + " And FinYearCode='" + SessionFinYearCode + "'";
        query = query + " And year(CONVERT(DATETIME,Ctn_Inwards_Date, 103))='" + txtYear.Text + "'";

        if (txtMonth.Text != "--Select--")
        {
            query = query + " And DATENAME(MONTH, CONVERT(DATETIME,Ctn_Inwards_Date, 103))='" + txtMonth.Text + "'";
        }
        query = query + " group by AgentName";

        dt = objdata.RptEmployeeMultipleDetails(query);
        Agent_Purchase_Chart.DataSource = dt;
        Agent_Purchase_Chart.Series["Series1"].XValueMember = "AgentName";
        Agent_Purchase_Chart.Series["Series1"].YValueMembers = "Bale_Count";
        this.Agent_Purchase_Chart.Series[0]["PieLabelStyle"] = "Outside";
        this.Agent_Purchase_Chart.ChartAreas[0].Area3DStyle.Enable3D = true;
        this.Agent_Purchase_Chart.ChartAreas[0].Area3DStyle.Inclination = 1;
        this.Agent_Purchase_Chart.Series[0].BorderWidth = 1;
        this.Agent_Purchase_Chart.Series[0].BorderColor = System.Drawing.Color.FromArgb(26, 59, 105);
        this.Agent_Purchase_Chart.Legends.Add("Legend1");
        this.Agent_Purchase_Chart.Legends[0].Enabled = true;
        this.Agent_Purchase_Chart.Legends[0].Docking = Docking.Bottom;
        this.Agent_Purchase_Chart.Legends[0].Alignment = System.Drawing.StringAlignment.Center;
        this.Agent_Purchase_Chart.Series[0].LegendText = "#PERCENT";
        //this.Agent_Purchase_Chart.DataManipulator.Sort(PointSortOrder.Descending, Chart2.Series[0]);
        Agent_Purchase_Chart.DataBind();

    }

    private void Agent_Purchase_Chart_Load()
    {
        DataTable dt = new DataTable();
        string query = "";
        query = "Select AgentName,sum(NoOfBale) as Bale_Count from CottonInwards";
        query = query + " where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        query = query + " And FinYearCode='" + SessionFinYearCode + "'";
        query = query + " And year(CONVERT(DATETIME,Ctn_Inwards_Date, 103))='" + txtYear.Text + "'";



        if (txtMonth.Text != "--Select--")
        {
            query = query + " And DATENAME(MONTH, CONVERT(DATETIME,Ctn_Inwards_Date, 103))='" + txtMonth.Text + "'";
        }
        query = query + " group by AgentName";
        dt = objdata.RptEmployeeMultipleDetails(query);
        Agent_Purchase_Chart.Legends.Clear();
        Agent_Purchase_Chart.DataSource = dt;
        Agent_Purchase_Chart.Series["Series1"].XValueMember = "AgentName";
        Agent_Purchase_Chart.Series["Series1"].YValueMembers = "Bale_Count";
        this.Agent_Purchase_Chart.Series[0]["PieLabelStyle"] = "Outside";
        this.Agent_Purchase_Chart.ChartAreas[0].Area3DStyle.Enable3D = true;
        this.Agent_Purchase_Chart.ChartAreas[0].Area3DStyle.Inclination = 1;
        this.Agent_Purchase_Chart.Series[0].BorderWidth = 1;
        this.Agent_Purchase_Chart.Series[0].BorderColor = System.Drawing.Color.FromArgb(26, 59, 105);
        this.Agent_Purchase_Chart.Legends.Add("Legend1");
        this.Agent_Purchase_Chart.Legends[0].Enabled = true;
        this.Agent_Purchase_Chart.Legends[0].Docking = Docking.Bottom;
        this.Agent_Purchase_Chart.Legends[0].Alignment = System.Drawing.StringAlignment.Center;
        this.Agent_Purchase_Chart.Series[0].LegendText = "#PERCENT";
        //this.Agent_Purchase_Chart.DataManipulator.Sort(PointSortOrder.Descending, Chart2.Series[0]);
        Agent_Purchase_Chart.DataBind();
    }
}
