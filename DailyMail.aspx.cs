﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;
using System.Net.Mail;
using CrystalDecisions.Shared;
public partial class DailyMail : System.Web.UI.Page
{
    string SSQL = "";
    BALDataAccess objdata = new BALDataAccess();
    DataTable StockDT = new DataTable();
    DataTable Department_DT = new DataTable();
    DataTable Item_DT = new DataTable();
    DataTable Open_Stck = new DataTable();
    DataTable Purchase_DT = new DataTable();
    DataTable Issue_DT = new DataTable();
    DataTable ZoneBin_DT = new DataTable();
    DataTable Item_UOM = new DataTable();
    DataTable GenReceiptDT = new DataTable();
    DataTable Scrab_DT = new DataTable();
    DataTable Transfer_DT = new DataTable();
    DataTable Rejection_DT = new DataTable();
    ReportDocument RD_Report = new ReportDocument();

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    DateTime frmDate;
    DateTime toDate;
    string FinYearVal = "2017_2018";
    string SessionFinYearCode = "2";
    string SessionFinYearVal = "2017_2018";
    protected void Page_Load(object sender, EventArgs e)
    {
        SessionCcode = "ATM";
        SessionLcode = "UNIT I";

        string FromDate = DateTime.Now.ToString("dd/MM/yyyy");
        string ToDate = System.DateTime.Now.AddDays(Convert.ToDouble(-1)).ToShortDateString();

        frmDate = Convert.ToDateTime(ToDate);
        toDate = Convert.ToDateTime(ToDate);

        string[] finyear = SessionFinYearVal.Split('_');
        string FromDate_Chk = "01/04/" + "2017";


        if (Convert.ToDateTime(frmDate) == Convert.ToDateTime(FromDate_Chk))
        {
            SSQL = "Delete  CMS_Stock_Details_Print";
            objdata.RptEmployeeMultipleDetails(SSQL);

            SSQL = "select LotNo as VarietyID,VarietyName from CottonInwards where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "' order by cast(LotNo as int) asc";
            Item_DT = objdata.RptEmployeeMultipleDetails(SSQL);
            for (int i = 0; i < Item_DT.Rows.Count; i++)
            {
                //SSQL = "Select Distinct DeptName from Stock_Transaction_Ledger where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "' And ItemName='" + Item_DT.Rows[i]["ItemName"].ToString() + "'";
                //Department_DT = objdata.RptEmployeeMultipleDetails(SSQL);
                //for (int j = 0; j < Department_DT.Rows.Count; j++)
                //{

                SSQL = "Select isnull(Count(Open_Bale),0) as Open_Bale,isnull(sum(Open_Qty),0) as Open_Qty from Stock_Transaction_Ledger where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "'";
                SSQL = SSQL + "And VariteyCode='" + Item_DT.Rows[i]["VarietyID"].ToString() + "'";
                SSQL = SSQL + "And Trans_Type='OPENING RECEIPT' group by VariteyCode";
                //SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date_Str, 103)=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";

                Open_Stck = objdata.RptEmployeeMultipleDetails(SSQL);

                SSQL = "select isnull(Count(BaleNo),0) as PO_Bale,isnull(sum(PO_Qty),0) as PO_Qty";
                SSQL = SSQL + " from Stock_Transaction_Ledger";
                SSQL = SSQL + " where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "'";
                SSQL = SSQL + " And Trans_Type='WEIGHT LIST'";
                SSQL = SSQL + " And LotNo='" + Item_DT.Rows[i]["VarietyID"].ToString() + "'";
                SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date_Str, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";
                SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date_Str, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103) group by VariteyCode";

                Purchase_DT = objdata.RptEmployeeMultipleDetails(SSQL);

                SSQL = "select  isnull(Count(Issue_Bale),0) as Issue_Bale,isnull(sum(Issue_Qty),0) as Issue_Qty";
                SSQL = SSQL + " from Stock_Transaction_Ledger";
                SSQL = SSQL + " where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "'";
                SSQL = SSQL + " And Trans_Type='ISSUE ENTRY'";
                SSQL = SSQL + " And LotNo='" + Item_DT.Rows[i]["VarietyID"].ToString() + "'";
                SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date_Str, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";
                SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date_Str, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103) group by VariteyCode";

                Issue_DT = objdata.RptEmployeeMultipleDetails(SSQL);


                SSQL = "select isnull(Count(Scrab_Bale),0) as Scrab_Bale,isnull(sum(Scrab_Qty),0) as Scrab_Qty";
                SSQL = SSQL + " from Stock_Transaction_Ledger";
                SSQL = SSQL + " where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "'";
                SSQL = SSQL + " And Trans_Type='SCRAB ENTRY'";
                SSQL = SSQL + " And LotNo='" + Item_DT.Rows[i]["VarietyID"].ToString() + "'";
                SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date_Str, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";
                SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date_Str, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103) group by VariteyCode";

                Scrab_DT = objdata.RptEmployeeMultipleDetails(SSQL);

                //Scrab Details
                SSQL = "select isnull(sum(Scrab_Qty),0) as Scrab_Qty,isnull(count(Scrab_Bale),0) as Scrab_Bale";
                SSQL = SSQL + " from Stock_Transaction_Ledger";
                SSQL = SSQL + " where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "'";
                SSQL = SSQL + " And VariteyName='" + Item_DT.Rows[i]["VarietyName"].ToString() + "'";
                SSQL = SSQL + " And Trans_Type='SCRAB ENTRY'";
                SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date_Str, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";
                SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date_Str, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";

                Scrab_DT = objdata.RptEmployeeMultipleDetails(SSQL);


                //Rejection Details
                SSQL = "select isnull(sum(Reject_Qty),0) as Reject_Qty,isnull(count(Reject_Bale),0) as Reject_Bale";
                SSQL = SSQL + " from Stock_Transaction_Ledger";
                SSQL = SSQL + " where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "'";
                SSQL = SSQL + " And VariteyName='" + Item_DT.Rows[i]["VarietyName"].ToString() + "'";
                SSQL = SSQL + " And Trans_Type='REJECTION ENTRY'";
                SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date_Str, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";
                SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date_Str, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";


                Rejection_DT = objdata.RptEmployeeMultipleDetails(SSQL);


                //Cotton Transfer  Details
                SSQL = "select isnull(sum(Transfer_Qty),0) as Transfer_Qty,isnull(count(Transfer_Bale),0) as Transfer_Bale";
                SSQL = SSQL + " from Stock_Transaction_Ledger";
                SSQL = SSQL + " where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "'";
                SSQL = SSQL + " And VariteyName='" + Item_DT.Rows[i]["VarietyName"].ToString() + "'";
                SSQL = SSQL + " And Trans_Type='TRANSFER ENTRY'";
                SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date_Str, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";
                SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date_Str, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";

                Transfer_DT = objdata.RptEmployeeMultipleDetails(SSQL);

                //Insert Temp Table
                SSQL = "Insert Into CMS_Stock_Details_Print(Ccode,Lcode,FinYearCode,FinYearVal,LotNo,VarietyName,Opening_Qty,Opening_Bale,PO_Qty,PO_Bale,";
                SSQL = SSQL + "Issue_Qty,Issue_Bale,Scrab_Qty,Scrab_Bale,Reject_Qty,Reject_Bale,Transfer_Qty,Transfer_Bale,From_Date,To_Date,UserID,UserName) Values('" + SessionCcode + "','" + SessionLcode + "',";
                SSQL = SSQL + " '" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + Item_DT.Rows[i]["VarietyID"].ToString() + "','" + Item_DT.Rows[i]["VarietyName"].ToString() + "',";
                if (Open_Stck.Rows.Count != 0)
                {
                    if (Open_Stck.Rows[0]["Open_Qty"].ToString() != "")
                    {
                        SSQL = SSQL + " '" + Open_Stck.Rows[0]["Open_Qty"] + "','" + Open_Stck.Rows[0]["Open_Bale"] + "',";
                    }
                    else
                    {
                        SSQL = SSQL + " '0.00','0.00',";
                    }
                }
                else
                {
                    SSQL = SSQL + " '0.00','0.00',";
                }

                if (Purchase_DT.Rows.Count != 0)
                {
                    if (Purchase_DT.Rows[0]["PO_Qty"].ToString() != "")
                    {
                        SSQL = SSQL + "'" + Purchase_DT.Rows[0]["PO_Qty"] + "','" + Purchase_DT.Rows[0]["PO_Bale"] + "',";
                    }
                    else
                    {
                        SSQL = SSQL + "'0.00','0.00',";
                    }
                }
                else
                {
                    SSQL = SSQL + "'0.00','0.00',";
                }
                if (Issue_DT.Rows.Count != 0)
                {
                    if (Issue_DT.Rows[0]["Issue_Qty"].ToString() != "")
                    {
                        SSQL = SSQL + "'" + Issue_DT.Rows[0]["Issue_Qty"] + "','" + Issue_DT.Rows[0]["Issue_Bale"] + "',";
                    }
                    else
                    {
                        SSQL = SSQL + "'0.00','0.00',";
                    }
                }
                else
                {
                    SSQL = SSQL + "'0.00','0.00',";
                }
                if (Scrab_DT.Rows.Count != 0)
                {
                    if (Scrab_DT.Rows[0]["Scrab_Qty"].ToString() != "")
                    {
                        SSQL = SSQL + "'" + Scrab_DT.Rows[0]["Scrab_Qty"] + "','" + Scrab_DT.Rows[0]["Scrab_Bale"] + "',";
                    }
                    else
                    {
                        SSQL = SSQL + "'0.00','0.00',";
                    }
                }
                else
                {
                    SSQL = SSQL + "'0.00','0.00',";
                }

                if (Rejection_DT.Rows.Count != 0)
                {
                    if (Rejection_DT.Rows[0]["Reject_Qty"].ToString() != "")
                    {
                        SSQL = SSQL + "'" + Rejection_DT.Rows[0]["Reject_Qty"] + "','" + Rejection_DT.Rows[0]["Reject_Bale"] + "',";
                    }
                    else
                    {
                        SSQL = SSQL + "'0.00','0.00',";
                    }
                }
                else
                {
                    SSQL = SSQL + "'0.00','0.00',";
                }

                if (Transfer_DT.Rows.Count != 0)
                {
                    if (Transfer_DT.Rows[0]["Transfer_Qty"].ToString() != "")
                    {
                        SSQL = SSQL + "'" + Transfer_DT.Rows[0]["Transfer_Qty"] + "','" + Transfer_DT.Rows[0]["Transfer_Bale"] + "',";
                    }
                    else
                    {
                        SSQL = SSQL + "'0.00','0.00',";
                    }
                }
                else
                {
                    SSQL = SSQL + "'0.00','0.00',";
                }


                SSQL = SSQL + " '" + FromDate + "','" + ToDate + "',";
                SSQL = SSQL + "'" + SessionUserID + "','" + SessionUserName + "')";
                objdata.RptEmployeeMultipleDetails(SSQL);
                //}
            }


            //Get Closing Stock
            SSQL = "Select LotNo,VarietyName,Opening_Qty as OStockKg,Opening_Bale as OStockBale,PO_Qty as WListKg,PO_Bale as WListBale,Issue_Qty as IssueKg,Issue_Bale as IssueBale,";
            SSQL = SSQL + " Scrab_Qty as ScrabKg,Scrab_Bale as ScrabBale,From_Date as FromDate,To_Date as ToDate,Reject_Qty as RejectKG,Reject_Bale as RejectBale,Transfer_Qty as TransferKG,Transfer_Bale as TransferBale from CMS_Stock_Details_Print where";
            SSQL = SSQL + " Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
            SSQL = SSQL + " And CONVERT(DATETIME,From_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";
            SSQL = SSQL + " And CONVERT(DATETIME,To_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
            SSQL = SSQL + " order by VarietyName";

            GenReceiptDT = objdata.RptEmployeeMultipleDetails(SSQL);
            if (GenReceiptDT.Rows.Count != 0)
            {

                //if (RptName == "Closing Stock Statement")
                //{

                DataSet ds1 = new DataSet();
                ds1.Tables.Add(GenReceiptDT);
                RD_Report.Load(Server.MapPath("~/crystal/BaleStockConsuptionReport.rpt"));
                RD_Report.SetDatabaseLogon("username", "password", "sa", "Altius2005");

                RD_Report.SetDataSource(ds1.Tables[0]);
                //Company Details Add
                DataTable dtdCompanyAddress = new DataTable();
                DataTable Close_Det = new DataTable();

                SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dtdCompanyAddress.Rows.Count != 0)
                {
                    RD_Report.DataDefinition.FormulaFields["CompanyName"].Text = "'" + dtdCompanyAddress.Rows[0]["Cname"].ToString() + "'";
                    RD_Report.DataDefinition.FormulaFields["Address1"].Text = "'" + dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString() + "'";
                    RD_Report.DataDefinition.FormulaFields["Address2"].Text = "'" + dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString() + "'";
                }



                //Sent Mail Code Start
                MailMessage mail = new MailMessage();
                //mail.To.Add("kalyan@scoto.in");
                mail.To.Add("vijayamurtha@gmail.com");

                mail.CC.Add("aatm2005@gmail.com");
                mail.Bcc.Add("padmanabhanscoto@gmail.com");

                mail.From = new MailAddress("aatm2005@gmail.com");
                mail.Subject = "MONTHLY SCHEDULE COMPLETE REPORTS";
                mail.Body = "Find the attachment for DAILY STOCK CONSUPTION REPORTS";
                mail.IsBodyHtml = true;

                //mail.Attachments.Add(new Attachment(ms, "DayWise1.rpt", "crystal/DayWise1.rpt"));
                mail.Attachments.Add(new Attachment(RD_Report.ExportToStream(ExportFormatType.PortableDocFormat), "CottonConsuption.pdf"));
                SmtpClient smtp = new SmtpClient();
                smtp.Host = "smtp.gmail.com"; //Or Your SMTP Server Address
                smtp.Credentials = new System.Net.NetworkCredential("aatm2005@gmail.com", "Altius2005");  //Or your Smtp Email ID and Password
                smtp.EnableSsl = true;
                smtp.Send(mail);

            }
            else
            {


            }
        }
        else
        {
            SSQL = "Delete CMS_Stock_Details_Print";
            objdata.RptEmployeeMultipleDetails(SSQL);

            SSQL = "select LotNo as VarietyID,VarietyName from CottonInwards where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "' order by cast(LotNo as int) asc";
            Item_DT = objdata.RptEmployeeMultipleDetails(SSQL);
            for (int i = 0; i < Item_DT.Rows.Count; i++)
            {
                //if (Item_DT.Rows[i]["ItemName"].ToString() == "CHEMAX WAX ROLLS")
                //{
                //    SSQL = SSQL;
                //}
                //SSQL = "Select Distinct DeptName from Stock_Transaction_Ledger where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "' And ItemName='" + Item_DT.Rows[i]["ItemName"].ToString() + "'";
                //Department_DT = objdata.RptEmployeeMultipleDetails(SSQL);
                //for (int j = 0; j < Department_DT.Rows.Count; j++)
                //{



                SSQL = "select ((isnull(sum(Open_Qty),0)+ isnull(sum(PO_Qty),0))-(isnull(sum(Issue_Qty),0) + isnull(sum(Scrab_Qty),0))) as Open_Qty,((isnull(count(Open_Bale),0)+ isnull(count(BaleNo),0))-(isnull(count(Issue_Bale),0)+ isnull(count(Scrab_Bale),0))) as Open_Bale from Stock_Transaction_Ledger where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "'";
                SSQL = SSQL + " And LotNo='" + Item_DT.Rows[i]["VarietyID"].ToString() + "'";
                SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date, 103)<=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";


                Open_Stck = objdata.RptEmployeeMultipleDetails(SSQL);


                //Weight List Details
                SSQL = "select isnull(sum(PO_Qty),0) as PO_Qty,isnull(count(BaleNo),0) as PO_Bale";
                SSQL = SSQL + " from Stock_Transaction_Ledger";
                SSQL = SSQL + " where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "'";
                SSQL = SSQL + " And LotNo='" + Item_DT.Rows[i]["VarietyID"].ToString() + "'";
                SSQL = SSQL + " And Trans_Type='WEIGHT LIST'";
                SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date_Str, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";
                SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date_Str, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";

                Purchase_DT = objdata.RptEmployeeMultipleDetails(SSQL);

                //Issuse Details
                SSQL = "select isnull(sum(Issue_Qty),0) as Issue_Qty,isnull(count(Issue_Bale),0) as Issue_Bale";
                SSQL = SSQL + " from Stock_Transaction_Ledger";
                SSQL = SSQL + " where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "'";
                SSQL = SSQL + " And LotNo='" + Item_DT.Rows[i]["VarietyID"].ToString() + "'";
                SSQL = SSQL + " And Trans_Type='ISSUE ENTRY'";
                SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date_Str, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";
                SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date_Str, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";


                Issue_DT = objdata.RptEmployeeMultipleDetails(SSQL);


                //Scrab Details
                SSQL = "select isnull(sum(Scrab_Qty),0) as Scrab_Qty,isnull(count(Scrab_Bale),0) as Scrab_Bale";
                SSQL = SSQL + " from Stock_Transaction_Ledger";
                SSQL = SSQL + " where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "'";
                SSQL = SSQL + " And LotNo='" + Item_DT.Rows[i]["VarietyID"].ToString() + "'";
                SSQL = SSQL + " And Trans_Type='SCRAB ENTRY'";
                SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date_Str, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";
                SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date_Str, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";


                Scrab_DT = objdata.RptEmployeeMultipleDetails(SSQL);


                //Rejection Details
                SSQL = "select isnull(sum(Reject_Qty),0) as Reject_Qty,isnull(count(Reject_Bale),0) as Reject_Bale";
                SSQL = SSQL + " from Stock_Transaction_Ledger";
                SSQL = SSQL + " where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "'";
                SSQL = SSQL + " And VariteyName='" + Item_DT.Rows[i]["VarietyID"].ToString() + "'";
                SSQL = SSQL + " And LotNo='REJECTION ENTRY'";
                SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date_Str, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";
                SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date_Str, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
                Rejection_DT = objdata.RptEmployeeMultipleDetails(SSQL);


                //Cotton Transfer  Details
                SSQL = "select isnull(sum(Transfer_Qty),0) as Transfer_Qty,isnull(count(Transfer_Bale),0) as Transfer_Bale";
                SSQL = SSQL + " from Stock_Transaction_Ledger";
                SSQL = SSQL + " where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "'";
                SSQL = SSQL + " And LotNo='" + Item_DT.Rows[i]["VarietyID"].ToString() + "'";
                SSQL = SSQL + " And Trans_Type='TRANSFER ENTRY'";

                SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date_Str, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";
                SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date_Str, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";

                Transfer_DT = objdata.RptEmployeeMultipleDetails(SSQL);

                //Insert Temp Table
                SSQL = "Insert Into CMS_Stock_Details_Print(Ccode,Lcode,FinYearCode,FinYearVal,LotNo,VarietyName,Opening_Qty,Opening_Bale,PO_Qty,PO_Bale,";
                SSQL = SSQL + "Issue_Qty,Issue_Bale,Scrab_Qty,Scrab_Bale,Reject_Qty,Reject_Bale,Transfer_Qty,Transfer_Bale,From_Date,To_Date,UserID,UserName) Values('" + SessionCcode + "','" + SessionLcode + "',";
                SSQL = SSQL + " '" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + Item_DT.Rows[i]["VarietyID"].ToString() + "','" + Item_DT.Rows[i]["VarietyName"].ToString() + "',";
                if (Open_Stck.Rows.Count != 0)
                {
                    if (Open_Stck.Rows[0]["Open_Qty"].ToString() != "")
                    {
                        SSQL = SSQL + " '" + Open_Stck.Rows[0]["Open_Qty"] + "','" + Open_Stck.Rows[0]["Open_Bale"] + "',";
                    }
                    else
                    {
                        SSQL = SSQL + " '0.00','0.00',";
                    }
                }
                else
                {
                    SSQL = SSQL + " '0.00','0.00',";
                }

                if (Purchase_DT.Rows.Count != 0)
                {
                    if (Purchase_DT.Rows[0]["PO_Qty"].ToString() != "")
                    {
                        SSQL = SSQL + "'" + Purchase_DT.Rows[0]["PO_Qty"] + "','" + Purchase_DT.Rows[0]["PO_Bale"] + "',";
                    }
                    else
                    {
                        SSQL = SSQL + "'0.00','0.00',";
                    }
                }
                else
                {
                    SSQL = SSQL + "'0.00','0.00',";
                }
                if (Issue_DT.Rows.Count != 0)
                {
                    if (Issue_DT.Rows[0]["Issue_Qty"].ToString() != "")
                    {
                        SSQL = SSQL + "'" + Issue_DT.Rows[0]["Issue_Qty"] + "','" + Issue_DT.Rows[0]["Issue_Bale"] + "',";
                    }
                    else
                    {
                        SSQL = SSQL + "'0.00','0.00',";
                    }
                }
                else
                {
                    SSQL = SSQL + "'0.00','0.00',";
                }
                if (Scrab_DT.Rows.Count != 0)
                {
                    if (Scrab_DT.Rows[0]["Scrab_Qty"].ToString() != "")
                    {
                        SSQL = SSQL + "'" + Scrab_DT.Rows[0]["Scrab_Qty"] + "','" + Scrab_DT.Rows[0]["Scrab_Bale"] + "',";
                    }
                    else
                    {
                        SSQL = SSQL + "'0.00','0.00',";
                    }
                }
                else
                {
                    SSQL = SSQL + "'0.00','0.00',";
                }

                if (Rejection_DT.Rows.Count != 0)
                {
                    if (Rejection_DT.Rows[0]["Reject_Qty"].ToString() != "")
                    {
                        SSQL = SSQL + "'" + Rejection_DT.Rows[0]["Reject_Qty"] + "','" + Rejection_DT.Rows[0]["Reject_Bale"] + "',";
                    }
                    else
                    {
                        SSQL = SSQL + "'0.00','0.00',";
                    }
                }
                else
                {
                    SSQL = SSQL + "'0.00','0.00',";
                }

                if (Transfer_DT.Rows.Count != 0)
                {
                    if (Transfer_DT.Rows[0]["Transfer_Qty"].ToString() != "")
                    {
                        SSQL = SSQL + "'" + Transfer_DT.Rows[0]["Transfer_Qty"] + "','" + Transfer_DT.Rows[0]["Transfer_Bale"] + "',";
                    }
                    else
                    {
                        SSQL = SSQL + "'0.00','0.00',";
                    }
                }
                else
                {
                    SSQL = SSQL + "'0.00','0.00',";
                }



                SSQL = SSQL + " '" + FromDate + "','" + ToDate + "',";
                SSQL = SSQL + "'" + SessionUserID + "','" + SessionUserName + "')";
                objdata.RptEmployeeMultipleDetails(SSQL);
                //}

            }



            //Get Closing Stock
            SSQL = "Select LotNo,VarietyName,Opening_Qty as OStockKg,Opening_Bale as OStockBale,PO_Qty as WListKg,PO_Bale as WListBale,Issue_Qty as IssueKg,Issue_Bale as IssueBale,";
            SSQL = SSQL + " Scrab_Qty as ScrabKg,Scrab_Bale as ScrabBale,From_Date as FromDate,To_Date as ToDate,Reject_Qty as RejectKG,Reject_Bale as RejectBale,Transfer_Qty as TransferKG,Transfer_Bale as TransferBale from CMS_Stock_Details_Print where";
            SSQL = SSQL + " Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
            SSQL = SSQL + " And CONVERT(DATETIME,From_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";
            SSQL = SSQL + " And CONVERT(DATETIME,To_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
            SSQL = SSQL + " order by VarietyName";

            GenReceiptDT = objdata.RptEmployeeMultipleDetails(SSQL);
            if (GenReceiptDT.Rows.Count != 0)
            {

                DataSet ds1 = new DataSet();
                ds1.Tables.Add(GenReceiptDT);
                RD_Report.Load(Server.MapPath("~/crystal/BaleStockConsuptionReport.rpt"));
                RD_Report.SetDatabaseLogon("username", "password", "sa", "Altius2005");
                RD_Report.SetDataSource(ds1.Tables[0]);
                //Company Details Add
                DataTable dtdCompanyAddress = new DataTable();
                DataTable Close_Det = new DataTable();
                SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dtdCompanyAddress.Rows.Count != 0)
                {
                    RD_Report.DataDefinition.FormulaFields["CompanyName"].Text = "'" + dtdCompanyAddress.Rows[0]["Cname"].ToString() + "'";
                    RD_Report.DataDefinition.FormulaFields["Address1"].Text = "'" + dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString() + "'";
                    RD_Report.DataDefinition.FormulaFields["Address2"].Text = "'" + dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString() + "'";
                }

                //Sent Mail Code Start
                MailMessage mail = new MailMessage();
                //mail.To.Add("kalyan@scoto.in");
                mail.To.Add("vijayamurtha@gmail.com");

                mail.CC.Add("aatm2005@gmail.com");
                mail.Bcc.Add("padmanabhanscoto@gmail.com");

                mail.From = new MailAddress("aatm2005@gmail.com");
                mail.Subject = "MONTHLY SCHEDULE COMPLETE REPORTS";
                mail.Body = "Find the attachment for DAILY STOCK CONSUPTION REPORTS";
                mail.IsBodyHtml = true;

                //mail.Attachments.Add(new Attachment(ms, "DayWise1.rpt", "crystal/DayWise1.rpt"));
                mail.Attachments.Add(new Attachment(RD_Report.ExportToStream(ExportFormatType.PortableDocFormat), "CottonConsuption.pdf"));
                SmtpClient smtp = new SmtpClient();
                smtp.Host = "smtp.gmail.com"; //Or Your SMTP Server Address
                smtp.Credentials = new System.Net.NetworkCredential("aatm2005@gmail.com", "Altius2005");  //Or your Smtp Email ID and Password
                smtp.EnableSsl = true;
                smtp.Send(mail);



            }
            else
            {

            }
        }


    }
}
