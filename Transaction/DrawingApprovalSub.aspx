﻿<%@ Page Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="DrawingApprovalSub.aspx.cs" Inherits="Transaction_DrawingApprovalSub" Title="" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<script type="text/javascript">
    $(function() {
        $('.cls').bind("keydown", function(e) {
            //alert("ji");
            var n = $(".cls").length;
            if (e.which == 13) { //Enter key
                // alert($(this).attr('class'));
                var class1 = $(this).attr('class');
                var split_cls = class1.split(" ");
                if (split_cls[1] == 'txt') {
                    //alert("in");
                    var Std_Production = $(this).closest("tr").find(".txt1").text();
                    var Act_Production = $(this).val();
                    var efficiency = parseFloat((parseFloat(Act_Production) / parseFloat(Std_Production)) * 100).toFixed(2);
                    $(this).closest("tr").find(".tot").val(efficiency);
                }
                e.preventDefault(); //to skip default behavior of the enter key
                var nextIndex = $('.cls').index(this) + 1;
                if (nextIndex < n)
                    $('.cls')[nextIndex].focus();
                else {
                    $('.cls')[nextIndex - 1].blur();
                    $("#<%=btnSave.ClientID %>").focus();

                }
            }
        });
    });   
</script>
    <script src='<%= ResolveUrl("../assets/js/master_list_jquery.min.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/master_list_jquery-ui.min.js") %>'></script>
<link href="<%= ResolveUrl("../assets/css/master_list_jquery-ui.css") %>" rel="stylesheet" type="text/css"/>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#example').dataTable();
            }
        });
    };
</script>

<script type="text/javascript">
    function SaveMsgAlert(msg) {
        swal(msg);
    }
</script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('.date-picker').datepicker({ format: 'dd/mm/yyyy' });
                $('.js-states').select2();
            }
        });
    };
</script>


<div class="page-breadcrumb">
    <ol class="breadcrumb container">
       <h4><li class="active">Drawing Approval</li></h4> 
    </ol>
</div>
                
                        
<div id="main-wrapper" class="container">
 <div class="row">
    <div class="col-md-12">
     
    
    <div class="page-inner">
    
      <div class="col-md-12">
		<div class="panel panel-white">
		  <div class="panel panel-primary">
			<div class="panel-heading clearfix">
			  <h4 class="panel-title">Drawing Approval</h4>
			</div>
		   </div>
		<form class="form-horizontal">
		  <div class="panel-body">
			<div class="col-md-12">
		      <div class="row">
				<div class="form-group col-md-3">
				   <label for="exampleInputName">Trans No<span class="mandatory">*</span></label>
				   <asp:Label ID="txtTransNo" runat="server" class="form-control" BackColor="#F3F3F3"></asp:Label>
			    </div>
				<div class="form-group col-md-3">
				  <label for="exampleInputName">Date</label>
				  <asp:TextBox ID="txtDate" MaxLength="20" class="form-control date-picker" runat="server"></asp:TextBox>
				  <asp:RequiredFieldValidator ControlToValidate="txtDate" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                  </asp:RequiredFieldValidator>
                  <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                     TargetControlID="txtDate" ValidChars="0123456789./">
                  </cc1:FilteredTextBoxExtender>
				</div>
				<div class="form-group col-md-3">
					<label for="exampleInputName">Yarn LotNo</label>
                    <asp:TextBox ID="txtYarnLot" class="form-control" runat="server"></asp:TextBox>
					
				   <asp:RequiredFieldValidator ControlToValidate="txtYarnLot" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator3" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                   </asp:RequiredFieldValidator>
			    </div>
			  	<div class="form-group col-md-3">
			           <label for="exampleInputName">Unit</label>
				       <asp:DropDownList ID="ddlUnit" runat="server" AutoPostBack="true" class="js-states form-control" onselectedindexchanged="ddlUnit_SelectedIndexChanged" 
                           >
				                       
				       </asp:DropDownList>
                       <asp:RequiredFieldValidator ControlToValidate="ddlUnit" Display="Dynamic" InitialValue="-Select-" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator13" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                       </asp:RequiredFieldValidator>
			   </div>
			  
			 </div>
			</div>
					        
			<div class="col-md-12">
		     <div class="row">  
		      	<div class="form-group col-md-3">
					<label for="exampleInputName">Color</label>
					
					 <asp:DropDownList ID="ddlColor" runat="server" class="js-states form-control" >
                       </asp:DropDownList>
								
				   <asp:RequiredFieldValidator ControlToValidate="ddlColor" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator2" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                   </asp:RequiredFieldValidator>
			    </div>
					         <div class="form-group col-md-3">
			      <label for="exampleInputName">Shift</label>
				 <asp:DropDownList ID="ddlShift" runat="server" 
                            class="js-states form-control" >
                       </asp:DropDownList>
                       <asp:RequiredFieldValidator ControlToValidate="ddlShift" Display="Dynamic" InitialValue="-Select-" ValidationGroup="Item_Validate_Field" class="form_error" ID="RequiredFieldValidator4" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                       </asp:RequiredFieldValidator>
			   </div>
				
			<div class="form-group col-md-3">
			        <label for="exampleInputName">Supervisor</label>
					<asp:DropDownList ID="txtSupervisor" runat="server" class="js-states form-control">
                    </asp:DropDownList>  
                    <asp:RequiredFieldValidator ControlToValidate="txtSupervisor" Display="Dynamic" InitialValue="-Select-" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator6" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                    </asp:RequiredFieldValidator>
			   </div>
			   
			          					        
			  </div>
			</div>
			
			
					
			<div class="clearfix"></div>				    
			<div class="timeline-options">
			  <h4 class="panel-title">Machine Production</h4>
			</div>
		   
                    
                    <!-- table start -->
					<div class="col-md-12">
					    <div class="row">
					    <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
			                    <HeaderTemplate>
                                    <table id="WasteReceipt" class="display table">
                                        <thead>
                                            <tr>
                                                                                                                       
                                                <th>Machine Name</th>
                                                <th>Speed</th>
                                                <th>Hank</th>
                                                <th>Std Production</th> 
                                                <th>Act Production</th>
                                                <th>Utilization</th>
                                                <th>Efficiency</th>
                                                <th>Sliver Waste</th>
                                                <th>Other Waste</th>
                                                <th>Stoppage Minutes</th> 
                                                <th>Reason</th>
                                                           
                                            </tr>
                                        </thead>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                       
                                        <td>
                                             <asp:Label ID="txtMachineCode" runat="server" name="std" class="" Text='<%# DataBinder.Eval(Container.DataItem, "MachineCode")%>' Visible="false"></asp:Label>
                                             <asp:Label ID="txtMachineName" runat="server" name="std" class="" Text='<%# DataBinder.Eval(Container.DataItem, "MachineName")%>' ></asp:Label>
                                             
                                             
                                         </td>
                                         <td>
                                         <asp:Label ID="txtSpeed" runat="server" name="std" class="" Text='<%# DataBinder.Eval(Container.DataItem, "Speed")%>'></asp:Label>
                                         </td>
                                         <td>
                                         <asp:Label ID="txtHank" runat="server" name="std" class="" Text='<%# DataBinder.Eval(Container.DataItem, "Hank")%>' ></asp:Label>
                                         </td>
                                        <td>
                                           <asp:Label ID="txtStdProd" runat="server" name="std" class="txt1" Text='<%# DataBinder.Eval(Container.DataItem, "StdProduction")%>' ></asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtActProd"  runat="server" name="Act" Class="cls txt" Text='<%# DataBinder.Eval(Container.DataItem, "ActProduction")%>' Width="60"></asp:TextBox>
                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                              TargetControlID="txtActProd" ValidChars="0123456789.">
                                            </cc1:FilteredTextBoxExtender>
                                        </td> 
                                         <td>
                                            <asp:TextBox ID="txtUtilization"  runat="server" name="Act" Class="cls" Text='<%# DataBinder.Eval(Container.DataItem, "Utilization")%>' Width="60"></asp:TextBox>
                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                              TargetControlID="txtUtilization" ValidChars="0123456789.">
                                            </cc1:FilteredTextBoxExtender>
                                        </td>   
                                        <td>
                                             <asp:TextBox ID="txtEfficency" runat="server" name="Eff" Class="cls tot" Text='<%# DataBinder.Eval(Container.DataItem, "Efficiency")%>' Width="60"></asp:TextBox>
                                             <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                  TargetControlID="txtEfficency" ValidChars="0123456789.">
                                             </cc1:FilteredTextBoxExtender>
                                        </td> 
                                       <td>
                                             <asp:TextBox ID="txtSilver" runat="server" name="Eff" Class="cls tot" Text='<%# DataBinder.Eval(Container.DataItem, "Silver_Wastage")%>' Width="60"></asp:TextBox>
                                             <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                  TargetControlID="txtSilver" ValidChars="0123456789.">
                                             </cc1:FilteredTextBoxExtender>
                                        </td>
                                        
                                         <td>
                                             <asp:TextBox ID="txtFS" runat="server" name="Eff" Class="cls tot" Text='<%# DataBinder.Eval(Container.DataItem, "FS")%>' Width="60"></asp:TextBox>
                                             <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                  TargetControlID="txtFS" ValidChars="0123456789.">
                                             </cc1:FilteredTextBoxExtender>
                                        </td>
                                        <td>
                                             <asp:TextBox ID="txtStoppageMin" runat="server" Class="cls" Text='<%# DataBinder.Eval(Container.DataItem, "Stop_Page_Mintus")%>' Width="60"></asp:TextBox>
                                             <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                  TargetControlID="txtStoppageMin" ValidChars="0123456789.">
                                             </cc1:FilteredTextBoxExtender>  
                                        </td> 
                                        <td>
                                            <asp:TextBox ID="txtReason" runat="server"  Class="cls" Text='<%# DataBinder.Eval(Container.DataItem, "Reason")%>' TextMode="MultiLine" Height="30"  Width="150"></asp:TextBox> 
                                        </td>                                    
                                        
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate></table></FooterTemplate>                                
			                </asp:Repeater>
					        <%--<asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
			                    <HeaderTemplate>
                                    <table id="example" class="display table">
                                        <thead>
                                            <tr>
                                                <th>S.No</th>
                                                <th>MachineName</th>
                                                <th>StdProd</th>
                                                <th>Utilization</th>
                                                <th>Efficiency</th>
                                                <th>Mode</th>
                                            </tr>
                                        </thead>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td><%# Container.ItemIndex + 1 %></td>
                                        <td><%# Eval("MachineName")%></td>
                                        <td><%# Eval("StdProduction")%></td>
                                        <td><%# Eval("Utilization")%></td>
                                        <td><%# Eval("Efficiency")%></td>
                                        <td>
                                            <asp:LinkButton ID="btnDeleteGrid" class="btn btn-danger btn-sm fa fa-trash-o"  runat="server" 
                                                Text="" OnCommand="GridDeleteClick" CommandArgument='Delete' CommandName='<%# Eval("MachineCode")%>' 
                                                CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this Cheese details?');">
                                            </asp:LinkButton>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate></table></FooterTemplate>                                
			                </asp:Repeater>--%>
					    </div>
					</div>
					<!-- table End -->
					
					
					
					
					
					<div class="form-group row"></div>	
                    <!-- Button start -->
                    <div class="txtcenter">
                        <asp:Button ID="btnSave" class="btn btn-success" Visible="false"  runat="server" Text="Save" 
                            ValidationGroup="Validate_Field" onclick="btnSave_Click"/>
                            <asp:Button ID="btnApprove" class="btn btn-success" runat="server" 
                            Text="Approve" onclick="btnApprove_Click" />
                        <asp:Button ID="btnCancel" class="btn btn-danger" runat="server" Text="Cancel" onclick="btnCancel_Click"/>
                        <asp:Button ID="btnBackEnquiry" class="btn btn-success" runat="server" 
                            Text="Back" onclick="btnBackEnquiry_Click" />
                        
                    </div>
                    <!-- Button end -->
                  </div>      
				</form>
			
			</div><!-- panel white end -->
		    </div>
		    
		    <div class="col-md-2"></div>
		    
            
      
 </div> <!-- col-9 end -->
 </div><!-- col 12 end -->
      
  </div><!-- row end -->
 </div>
</asp:Content>

