﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
public partial class Transaction_WindingEntryApprovalSub : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionUnplanReceiptNo;
    string SessionFinYearCode;
    string SessionFinYearVal;
    static string sum;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "ERP Module :: Yarn Production";
            HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Transaction"));
            li.Attributes.Add("class", "droplink active open");
            Initial_Data_Referesh();
            Load_Supplier();
            Load_Shift();
            Load_GoDown();
            Load_Count();
            Load_Unit();
            Load_Machine();
            if (Session["Trans_No"] == null)
            {
                SessionUnplanReceiptNo = "";
            }
            else
            {
                SessionUnplanReceiptNo = Session["Trans_No"].ToString();
                txtReceiptNo.Text = SessionUnplanReceiptNo;
                btnSearch_Click(sender, e);
                //btnSearch_Click(sender, e);
            }
        }
        Load_OLD_data();
    }
    public void Load_OLD_data()
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        Repeater1.DataSource = dt;
        Repeater1.DataBind();
    }
    private void Initial_Data_Referesh()
    {
        DataTable dt = new DataTable();

        dt.Columns.Add(new DataColumn("MachineCode", typeof(string)));
        dt.Columns.Add(new DataColumn("MachineName", typeof(string)));
        dt.Columns.Add(new DataColumn("CountID", typeof(string)));
        dt.Columns.Add(new DataColumn("CountName", typeof(string)));
        dt.Columns.Add(new DataColumn("Side", typeof(string)));
        dt.Columns.Add(new DataColumn("LotNo", typeof(string)));
        dt.Columns.Add(new DataColumn("DrumFrom", typeof(string)));
        dt.Columns.Add(new DataColumn("DrumTo", typeof(string)));
        dt.Columns.Add(new DataColumn("DrumAllot", typeof(string)));
        dt.Columns.Add(new DataColumn("Unit", typeof(string)));
        dt.Columns.Add(new DataColumn("MainCount", typeof(string)));
        dt.Columns.Add(new DataColumn("DrumSpeed", typeof(string)));
        dt.Columns.Add(new DataColumn("DoffCone", typeof(string)));
        dt.Columns.Add(new DataColumn("ConeWeight", typeof(string)));
        dt.Columns.Add(new DataColumn("Production", typeof(string)));
        dt.Columns.Add(new DataColumn("Utilization", typeof(string)));
        dt.Columns.Add(new DataColumn("Efficiency", typeof(string)));
        dt.Columns.Add(new DataColumn("HardWaste", typeof(string)));
        dt.Columns.Add(new DataColumn("Remarks", typeof(string)));



        Repeater1.DataSource = dt;
        Repeater1.DataBind();
        ViewState["ItemTable"] = Repeater1.DataSource;

        //dt = Repeater1.DataSource;
    }
    private void Load_Supplier()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        txtSupplierName.Items.Clear();
        query = "Select * from MstEmployee where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        txtSupplierName.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["EmpName"] = "-Select-";
        dr["EmpCode"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        txtSupplierName.DataTextField = "EmpName";
        txtSupplierName.DataValueField = "EmpCode";
        txtSupplierName.DataBind();
    }
    private void Load_GoDown()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlGodown.Items.Clear();
        query = "Select * from MstGoDown where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlGodown.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["GoDownName"] = "-Select-";
        dr["GoDownID"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlGodown.DataTextField = "GoDownName";
        ddlGodown.DataValueField = "GoDownID";
        ddlGodown.DataBind();
    }


    private void Load_Shift()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlShift.Items.Clear();
        query = "Select ShiftName from MstShift where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlShift.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["ShiftName"] = "-Select-";
        dr["ShiftName"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlShift.DataTextField = "ShiftName";
        ddlShift.DataValueField = "ShiftName";
        ddlShift.DataBind();
    }
    private void Load_Unit()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlUnit.Items.Clear();
        query = "select UnitName from MstUnit where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlUnit.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["UnitName"] = "-Select-";
        dr["UnitName"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlUnit.DataTextField = "UnitName";
        ddlUnit.DataValueField = "UnitName";
        ddlUnit.DataBind();
    }
    private void Load_Count()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlCount.Items.Clear();
        query = "Select * from MstCount where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlCount.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["CountName"] = "-Select-";
        dr["CountID"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlCount.DataTextField = "CountName";
        ddlCount.DataValueField = "CountID";
        ddlCount.DataBind();
    }
    private void Load_Machine()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlMachine.Items.Clear();
        query = "Select Distinct MachineName from MstMachine where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' and DeptCode<>'1' and DeptCode<>'2' and DeptCode<>'3' and DeptCode<>'-Select-'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlMachine.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["MachineName"] = "-Select-";
        dr["MachineName"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlMachine.DataTextField = "MachineName";
        ddlMachine.DataValueField = "MachineName";
        ddlMachine.DataBind();
    }
    protected void ddlMachine_SelectedIndexChanged(object sender, EventArgs e)
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlSide.Items.Clear();
        query = "select Side from MstMachine where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' and MachineName='" + ddlMachine.SelectedValue + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlSide.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["Side"] = "-Select-";
        dr["Side"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlSide.DataTextField = "Side";
        ddlSide.DataValueField = "Side";
        ddlSide.DataBind();
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        string query = "";
        DataTable DT_Check = new DataTable();
        DataTable dtd1 = new DataTable();
        string SaveMode = "Insert";
        bool ErrFlag = false;

        if ((ddlShift.SelectedValue == "") || (ddlShift.SelectedValue == "-Select-"))
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select any one Shift..');", true);
        }
        else if ((ddlGodown.SelectedValue == "") || (ddlGodown.SelectedValue == "-Select-"))
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select any one GoDown..');", true);
        }
        else if ((txtSupplierName.SelectedValue == "") || (txtSupplierName.SelectedValue == "-Select-"))
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select any one Prepared By..');", true);
        }
        DT_Check = (DataTable)ViewState["ItemTable"];
        if (DT_Check.Rows.Count == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You have to add atleast one  Winding Details..');", true);
        }
        bool Rights_Check = false;
        //Auto generate Transaction Function Call
        if (btnSave.Text != "Update")
        {
            if (!ErrFlag)
            {
                TransactionNoGenerate TransNO = new TransactionNoGenerate();
                string Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup(SessionCcode, SessionLcode, "Winding", SessionFinYearVal);
                if (Auto_Transaction_No == "")
                {
                    ErrFlag = true;
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Auto Generate Transaction No Error... Contact Server Admin');", true);
                }
                else
                {
                    txtReceiptNo.Text = Auto_Transaction_No;
                }
            }
        }
        if (!ErrFlag)
        {
            query = "Select * from Winding_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Trans_No='" + txtReceiptNo.Text + "'";
            DT_Check = objdata.RptEmployeeMultipleDetails(query);
            if (DT_Check.Rows.Count != 0)
            {
                SaveMode = "Update";
                query = "Delete from Winding_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Trans_No='" + txtReceiptNo.Text + "'";
                objdata.RptEmployeeMultipleDetails(query);
                query = "Delete from Winding_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Trans_No='" + txtReceiptNo.Text + "'";
                objdata.RptEmployeeMultipleDetails(query);
            }

            //Insert Main Table
            query = "Insert into Winding_Main(Ccode,Lcode,FinYearCode,FinYearVal,Trans_No,Trans_Date,";
            query = query + "Shift,PrepaidBy,ToGoDownID,ToGoDownName,Status,UserID,UserName)Values('" + SessionCcode + "','" + SessionLcode + "',";
            query = query + "'" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + txtReceiptNo.Text + "','" + txtDate.Text + "','" + ddlShift.SelectedItem.Text + "',";
            query = query + "'" + txtSupplierName.SelectedValue + "','" + ddlGodown.SelectedValue + "','" + ddlGodown.SelectedItem.Text + "','0','admin','admin')";
            objdata.RptEmployeeMultipleDetails(query);

            dtd1 = (DataTable)ViewState["ItemTable"];
            for (int i = 0; i < dtd1.Rows.Count; i++)
            {
                query = "insert into Winding_Sub(Ccode,Lcode,FinYearCode,FinYearVal,Trans_No,Trans_Date,MachineCode,MachineName,";
                query = query + "CountID,CountName,Side,LotNo,DrumFrom,DrumTo,DrumAllot,Unit,MainCount,DrumSpeed,";
                query = query + "DoffCone,ConeWeight,Production,Utilization,Efficiency,HardWaste,Reason,UserID,UserName)values(";
                query = query + "'" + SessionCcode + "','" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + txtReceiptNo.Text + "','" + txtDate.Text + "','" + dtd1.Rows[i]["MachineCode"].ToString() + "','" + dtd1.Rows[i]["MachineName"].ToString() + "',";
                query = query + "'" + dtd1.Rows[i]["CountID"].ToString() + "','" + dtd1.Rows[i]["CountName"].ToString() + "','" + dtd1.Rows[i]["Side"].ToString() + "','" + dtd1.Rows[i]["LotNo"].ToString() + "','" + dtd1.Rows[i]["DrumFrom"].ToString() + "','" + dtd1.Rows[i]["DrumTo"].ToString() + "','" + dtd1.Rows[i]["DrumAllot"].ToString() + "','" + dtd1.Rows[i]["Unit"].ToString() + "','" + dtd1.Rows[i]["MainCount"].ToString() + "','" + dtd1.Rows[i]["DrumSpeed"].ToString() + "',";
                query = query + "'" + dtd1.Rows[i]["DoffCone"].ToString() + "','" + dtd1.Rows[i]["ConeWeight"].ToString() + "','" + dtd1.Rows[i]["Production"].ToString() + "','" + dtd1.Rows[i]["Utilization"].ToString() + "','" + dtd1.Rows[i]["Efficiency"].ToString() + "','" + dtd1.Rows[i]["HardWaste"].ToString() + "','" + dtd1.Rows[i]["Remarks"].ToString() + "','admin','admin')";
                objdata.RptEmployeeMultipleDetails(query);
            }
            if (SaveMode == "Insert")
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Winding Details Saved Successfully');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Winding Details Updated Successfully');", true);
            }
            Session["Trans_No"] = txtReceiptNo.Text;
            btnSave.Text = "Update";
            Load_OLD_data();
            //EventFire = true;
            //BtnSubmit_Click(sender, e);

            Response.Redirect("WindingEntryApprovalMain.aspx");


        }

    }
    protected void GridDeleteClick(object sender, CommandEventArgs e)
    {

    }
    protected void lbtnAdd_Click(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        DataTable qry_dt = new DataTable();
        bool ErrFlag = false;
        DataRow dr = null;
        string query = "";

        if ((ddlMachine.SelectedValue == "-Select-") || (ddlMachine.SelectedValue == "0"))
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select Machine Name...');", true);
        }
        else if ((ddlCount.SelectedValue == "-Select-") || (ddlCount.SelectedValue == "0"))
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select the Count...');", true);
        }
        else if ((ddlUnit.SelectedValue == "-Select-") || (ddlUnit.SelectedValue == "0"))
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select the Unit Name...');", true);
        }
        else if ((ddlSide.SelectedValue == "-Select-") || (ddlSide.SelectedValue == "0"))
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select the Side...');", true);
        }
        else if (txtDrumFrom.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the Drum From...');", true);
        }
        else if (txtDrumTo.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the Drum To...');", true);
        }
        else if (txtMainCount.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the Main Count...');", true);
        }
        else if (txtDrumSpeed.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the Drum Speed...');", true);
        }
        else if (txtDoffCone.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the Doff Cone...');", true);
        }
        else if (txtConeWeight.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the Cone Weight...');", true);
        }


        if (!ErrFlag)
        {
            txtAllotDrum.Value = ((Convert.ToDecimal(txtDrumTo.Text) - Convert.ToDecimal(txtDrumFrom.Text)) + Convert.ToDecimal(1)).ToString();
            if (Convert.ToDecimal(txtAllotDrum.Value) > Convert.ToDecimal(54))
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Allot Drum To High...');", true);
            }

            // check view state is not null  
            if (ViewState["ItemTable"] != null)
            {
                string Temp_Drum = "0";
                //get datatable from view state   
                dt = (DataTable)ViewState["ItemTable"];

                //check Item Already add or not
                for (int i = 0; i < dt.Rows.Count; i++)
                {


                    if ((dt.Rows[i]["MachineCode"].ToString().ToUpper() == ddlMachine.SelectedValue.ToString().ToUpper()) && (dt.Rows[i]["Side"].ToString().ToUpper() == ddlSide.SelectedItem.Text.ToString().ToUpper()))
                    {
                        Temp_Drum = (Convert.ToDecimal(Temp_Drum) + Convert.ToDecimal(txtAllotDrum.Value) + Convert.ToDecimal(dt.Rows[i]["DrumAllot"].ToString())).ToString();
                        if (Convert.ToDecimal(Temp_Drum) >= Convert.ToDecimal(55))
                        {
                            ErrFlag = true;
                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Allot Drum To High...');", true);
                        }

                    }
                }
                if (!ErrFlag)
                {


                    txtProduction.Value = (Convert.ToDecimal(txtDoffCone.Text) * Convert.ToDecimal(txtConeWeight.Text)).ToString();
                    To_Sum();
                    dr = dt.NewRow();
                    dr["MachineCode"] = ddlMachine.SelectedValue;
                    dr["MachineName"] = ddlMachine.SelectedItem.Text;
                    dr["CountID"] = ddlCount.SelectedValue; ;
                    dr["CountName"] = ddlCount.SelectedItem.Text;
                    dr["Side"] = ddlSide.SelectedItem.Text;
                    dr["Unit"] = ddlUnit.SelectedItem.Text;
                    dr["LotNo"] = txtLotNo.Text;
                    dr["DrumFrom"] = txtDrumFrom.Text;
                    dr["DrumTo"] = txtDrumTo.Text;
                    dr["DrumAllot"] = txtAllotDrum.Value;
                    dr["MainCount"] = txtMainCount.Text;
                    dr["DrumSpeed"] = txtDrumSpeed.Text;
                    dr["DoffCone"] = txtDoffCone.Text;
                    dr["ConeWeight"] = txtConeWeight.Text;
                    dr["Production"] = txtProduction.Value;
                    dr["Utilization"] = txtUtilization.Text;
                    dr["Efficiency"] = txtEfficiency.Value;
                    dr["HardWaste"] = txtHardWaste.Text;
                    dr["Remarks"] = txtRemarks.Text;


                    dt.Rows.Add(dr);
                    ViewState["ItemTable"] = dt;
                    Repeater1.DataSource = dt;
                    Repeater1.DataBind();


                    Load_Count();
                    Load_Machine();
                    Load_Unit();
                    ddlSide.Items.Clear();
                    txtLotNo.Text = ""; txtDrumFrom.Text = ""; txtDrumTo.Text = ""; txtMainCount.Text = ""; txtDrumSpeed.Text = "";
                    txtDoffCone.Text = ""; txtConeWeight.Text = ""; txtUtilization.Text = ""; txtHardWaste.Text = ""; txtRemarks.Text = "";

                }
            }
            else
            {


                txtProduction.Value = (Convert.ToDecimal(txtDoffCone.Text) * Convert.ToDecimal(txtConeWeight.Text)).ToString();
                To_Sum();
                dr = dt.NewRow();
                dr["MachineCode"] = ddlMachine.SelectedValue;
                dr["MachineName"] = ddlMachine.SelectedItem.Text;
                dr["CountID"] = ddlCount.SelectedValue; ;
                dr["CountName"] = ddlCount.SelectedItem.Text;
                dr["Side"] = ddlSide.SelectedItem.Text;
                dr["Unit"] = ddlUnit.SelectedItem.Text;
                dr["LotNo"] = txtLotNo.Text;
                dr["DrumFrom"] = txtDrumFrom.Text;
                dr["DrumTo"] = txtDrumTo.Text;
                dr["DrumAllot"] = txtDrumTo.Text;
                dr["MainCount"] = txtMainCount.Text;
                dr["DrumSpeed"] = txtDrumSpeed.Text;
                dr["DoffCone"] = txtDoffCone.Text;
                dr["ConeWeight"] = txtConeWeight.Text;
                dr["Production"] = txtUtilization.Text;
                dr["Utilization"] = txtUtilization.Text;
                dr["Efficiency"] = txtEfficiency.Value;
                dr["HardWaste"] = txtHardWaste.Text;
                dr["Remarks"] = txtRemarks.Text;


                dt.Rows.Add(dr);
                ViewState["ItemTable"] = dt;
                Repeater1.DataSource = dt;
                Repeater1.DataBind();

                Load_Count();
                Load_Machine();
                Load_Unit();
                ddlSide.Items.Clear();
                txtLotNo.Text = ""; txtDrumFrom.Text = ""; txtDrumTo.Text = ""; txtMainCount.Text = ""; txtDrumSpeed.Text = "";
                txtDoffCone.Text = ""; txtConeWeight.Text = ""; txtUtilization.Text = ""; txtHardWaste.Text = ""; txtRemarks.Text = "";
            }

        }
    }
    public void To_Sum()
    {
        string MainCount = "";
        string DrumSpeed = "";
        string Production = "";
        string AllotDrum = "";
        string Total_One = "", Total_Two = "", Total_Three = "";

        if (txtDrumSpeed.Text == "") { DrumSpeed = "0"; } else { DrumSpeed = txtDrumSpeed.Text; }
        if (txtMainCount.Text == "") { MainCount = "0"; } else { MainCount = txtMainCount.Text; }
        if (txtProduction.Value == "") { Production = "0"; } else { Production = txtProduction.Value; }
        if (txtAllotDrum.Value == "") { AllotDrum = "0"; } else { AllotDrum = txtAllotDrum.Value; }

        Total_One = ((Convert.ToDecimal(DrumSpeed) * Convert.ToDecimal(1.0936)) * Convert.ToDecimal(480)).ToString();
        Total_Two = ((Convert.ToDecimal(Total_One) / Convert.ToDecimal(840)) / Convert.ToDecimal(MainCount)).ToString();
        Total_Three = ((Convert.ToDecimal(Total_Two) / Convert.ToDecimal(2.2046)) * Convert.ToDecimal(AllotDrum)).ToString();
        txtEfficiency.Value = ((Convert.ToDecimal(Production) / Convert.ToDecimal(Total_Three)) * Convert.ToDecimal(100)).ToString();
        txtEfficiency.Value = (Math.Round(Convert.ToDecimal(txtEfficiency.Value), 2, MidpointRounding.AwayFromZero)).ToString();





    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        //Search Purchase Enquiry
        string query = "";
        DataTable Main_DT = new DataTable();
        query = "Select * from Winding_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Trans_No='" + txtReceiptNo.Text + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        if (Main_DT.Rows.Count != 0)
        {
            txtDate.Text = Main_DT.Rows[0]["Trans_Date"].ToString();
            ddlGodown.SelectedValue = Main_DT.Rows[0]["ToGoDownID"].ToString();
            ddlShift.Text = Main_DT.Rows[0]["Shift"].ToString();
            txtSupplierName.Text = Main_DT.Rows[0]["PrepaidBy"].ToString().Trim();

            DataTable dt = new DataTable();
            query = "Select MachineCode,MachineName,CountID,CountName,Side,LotNo,DrumFrom,DrumTo,DrumAllot,Unit,MainCount,DrumSpeed,DoffCone,ConeWeight,Production,Utilization,Efficiency,HardWaste,(Reason) as Remarks from Winding_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Trans_No='" + txtReceiptNo.Text + "'";
            dt = objdata.RptEmployeeMultipleDetails(query);
            ViewState["ItemTable"] = dt;
            Repeater1.DataSource = dt;
            Repeater1.DataBind();
            btnSave.Text = "Update";
        }
    }
    public void Clear()
    {
        Initial_Data_Referesh();
        Load_GoDown();
        Load_Shift();
        Load_Supplier();
        txtDate.Text = "";
        txtReceiptNo.Text = "";
        Load_Count();
        Load_Machine();
        Load_Unit();
        ddlSide.Items.Clear();
        txtLotNo.Text = ""; txtDrumFrom.Text = ""; txtDrumTo.Text = ""; txtMainCount.Text = ""; txtDrumSpeed.Text = "";
        txtDoffCone.Text = ""; txtConeWeight.Text = ""; txtUtilization.Text = ""; txtHardWaste.Text = ""; txtRemarks.Text = "";
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Clear();
    }
    protected void btnBackEnquiry_Click(object sender, EventArgs e)
    {
        Response.Redirect("WindingEntryApprovalMain.aspx");
    }

    protected void btnApprove_Click(object sender, EventArgs e)
    {
        string query = "";
        DataTable DT_Check = new DataTable();
        bool ErrFlag = false;

        //User Rights Check Start
        bool Rights_Check = false;

        query = "Select * from Winding_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Trans_No='" + txtReceiptNo.Text + "'";
        DT_Check = objdata.RptEmployeeMultipleDetails(query);
        if (DT_Check.Rows.Count == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('First You have to Register this Service Receipt Details..');", true);
        }

        query = "Select * from Winding_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Trans_No='" + txtReceiptNo.Text + "' And Status='1'";
        DT_Check = objdata.RptEmployeeMultipleDetails(query);
        if (DT_Check.Rows.Count != 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Service Receipt Details Already Approved..');", true);
        }
        if (!ErrFlag)
        {
            query = "Update Winding_Main set Status='1' where Ccode='" + SessionCcode + "'";
            query = query + "  And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
            query = query + "  And Trans_No='" + txtReceiptNo.Text + "'";
            objdata.RptEmployeeMultipleDetails(query);

            //query = "Update LotNo_Stage set Types='OE' where YarnLotNo='" + ddlYarnLotNo.SelectedValue + "' and Unit='" + ddlUnit.SelectedValue + "' and ColorName='" + ddlColor.SelectedValue + "'";
            //objdata.RptEmployeeMultipleDetails(query);

            ////Inert Data to LotNo_Stage
            //query = "insert into LotNo_Stage(Unit,ColorName,YarnLotNo,Types,StartDate) Values(";
            //query = query + "'" + ddlUnit.SelectedValue + "','" + ddlColor.SelectedValue + "','" + ddlYarnLotNo.SelectedValue + "',";
            //query = query + "'Drawing','" + txtDate.Text + "')";
            //objdata.RptEmployeeMultipleDetails(query);





            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert(' Drawing Details Approved Successfully..');", true);
        }
        Session.Remove("Trans_No");
        Response.Redirect("WindingEntryApprovalMain.aspx");
    }
}
