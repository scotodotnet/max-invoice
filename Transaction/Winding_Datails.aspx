<%@ Page Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="Winding_Datails.aspx.cs" Inherits="Transaction_Winding_Datails" Title="" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<script src='<%= ResolveUrl("../assets/js/master_list_jquery.min.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/master_list_jquery-ui.min.js") %>'></script>
<link href="<%= ResolveUrl("../assets/css/master_list_jquery-ui.css") %>" rel="stylesheet" type="text/css"/>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#example').dataTable();
            }
        });
    };
</script>

<script type="text/javascript">
    function SaveMsgAlert(msg) {
        swal(msg);
    }
</script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('.date-picker').datepicker({ format: 'dd/mm/yyyy' });
                $('.js-states').select2();
            }
        });
    };
</script>
<script type="text/javascript">
    $(function() {
        $('#WasteReceipt').Scrollable({
            ScrollHeight: 100
        });
    });
</script>

<div class="page-breadcrumb">
    <ol class="breadcrumb container">
       <h4><li class="active">Winding</li></h4> 
    </ol>
</div>
                
                        
<div id="main-wrapper" class="container">
 <div class="row">
    <div class="col-md-12">
     
    
    <div class="page-inner">
    
      <div class="col-md-12">
		<div class="panel panel-white">
		  <div class="panel panel-primary">
			<div class="panel-heading clearfix">
			  <h4 class="panel-title">Winding </h4>
			</div>
		   </div>
		<form class="form-horizontal">
		  <div class="panel-body">
			<div class="col-md-12">
		      <div class="row">
				<div class="form-group col-md-4">
				   <label for="exampleInputName">Trans No<span class="mandatory">*</span></label>
				   <asp:Label ID="txtReceiptNo" runat="server" class="form-control" BackColor="#F3F3F3"></asp:Label>
			    </div>
			     
				<div class="form-group col-md-2">
				  <label for="exampleInputName">Date</label>
				  <asp:TextBox ID="txtDate" MaxLength="20" class="form-control date-picker" runat="server"></asp:TextBox>
				  <asp:RequiredFieldValidator ControlToValidate="txtDate" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                  </asp:RequiredFieldValidator>
                  <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                     TargetControlID="txtDate" ValidChars="0123456789./">
                  </cc1:FilteredTextBoxExtender>
				</div>
				<%--<div class="form-group col-md-3">
					                    <label for="exampleInputName">Yarn LotNo</label>
					                     <asp:DropDownList ID="ddlYarnLotNo" runat="server" class="form-control" AutoPostBack="true"
                                            onselectedindexchanged="ddlYarnLotNo_SelectedIndexChanged">
                                         </asp:DropDownList>
					                  
					                    <asp:RequiredFieldValidator ControlToValidate="ddlYarnLotNo" Display="Dynamic" InitialValue="-Select-" ValidationGroup="Item_Validate_Field" class="form_error" ID="RequiredFieldValidator11" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                        </asp:RequiredFieldValidator>
                                   
					                </div>--%>
					                
				 <div class="form-group col-md-3">
					          <label for="exampleInputName">Unit<span class="mandatory">*</span></label>
                                <asp:DropDownList ID="ddlUnit" class="form-control" runat="server" 
                                  AutoPostBack="true" onselectedindexchanged="ddlUnit_SelectedIndexChanged">
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ControlToValidate="ddlUnit" Display="Dynamic" InitialValue="-Select-" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator2" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                       </asp:RequiredFieldValidator>
					        </div> 
					        
					          <div class="form-group col-md-3">
			                          <label for="exampleInputName">Shift</label>
				                     <asp:DropDownList ID="ddlShift" runat="server" 
                                                class="js-states form-control" >
                                           </asp:DropDownList>
                                           <asp:RequiredFieldValidator ControlToValidate="ddlShift" Display="Dynamic" InitialValue="-Select-" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator4" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                           </asp:RequiredFieldValidator>
			                       </div>
			          
			 </div>
			</div>
					        
			<div class="col-md-12">
		     <div class="row">  
					         
					           
				                 
				
			<div class="form-group col-md-3">
			    <label for="exampleInputName">Prepared By</label>
					<asp:DropDownList ID="txtSupplierName" runat="server" class="js-states form-control">
                    </asp:DropDownList>  
                    <asp:RequiredFieldValidator ControlToValidate="txtSupplierName" Display="Dynamic" InitialValue="-Select-" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator6" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                    </asp:RequiredFieldValidator>
			   </div>
			   <div class="form-group col-md-3">
			       <label for="exampleInputName">GoDown</label>
					<asp:DropDownList ID="ddlGodown" runat="server" class="js-states form-control">
                    </asp:DropDownList>  
                    <asp:RequiredFieldValidator ControlToValidate="ddlGodown" Display="Dynamic" InitialValue="-Select-" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator16" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                    </asp:RequiredFieldValidator>
			   </div>
			  
			  
			   
			  
					        
			  </div>
			</div>
			
			
		
			
					
			<div class="clearfix"></div>				    
			<div class="timeline-options">
			  <h4 class="panel-title">Count Details</h4>
			</div>
					  
                 <!-- table start -->
					<div class="col-md-12">
					    <div class="row">
					    <asp:Panel ScrollBars="Vertical" ID="Panel" runat="server">
					    <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false" OnItemDataBound="ItemDataBound">
			                    <HeaderTemplate>
                                    <table id="WasteReceipt" class="display table" style="overflow-x:scroll;width:100%;">
                                        <thead>
                                            <tr>
                                                <th>Machine Name</th>
                                                <th>Speed</th>
                                                <th>Std Production</th> 
                                                <th>Count</th>
                                                <th>Yarn LotNo</th>
                                                <th>Color</th>
                                                <th>Act Production</th>
                                                <th>Utilization</th>
                                                <th>Efficiency</th>
                                                <th>No of Cone</th>
                                                <th>No of Doff</th>
                                                <th>Cone Weight</th>
                                                <th>Net Weight</th>
                                                <th>Hard Waste</th>
                                                <th>Stoppage Minutes</th> 
                                                <th>Reason</th>
                                            </tr>
                                        </thead>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                         <td>
                                             <asp:Label ID="txtMachineCode" runat="server" name="std" class="" Text='<%# DataBinder.Eval(Container.DataItem, "MachineCode")%>' Visible="false"></asp:Label>
                                             <asp:Label ID="txtMachineName" runat="server" name="std" class="" Text='<%# DataBinder.Eval(Container.DataItem, "MachineName")%>' ></asp:Label>
                                         </td>
                                         <td>
                                           <asp:Label ID="txtSpeed" runat="server" name="std" class="txt1" Text='<%# DataBinder.Eval(Container.DataItem, "Speed")%>' ></asp:Label>
                                         </td>
                                         <td>
                                         <asp:Label ID="txtStdProd" runat="server" name="std" class="txt1" Text='<%# DataBinder.Eval(Container.DataItem, "StdProd")%>' ></asp:Label>
                                         </td>
                                         <td>
                                          <asp:DropDownList ID="ddlCount" runat="server" CssClass="js-states" >
                                          </asp:DropDownList>
                                         </td>
                                       <td>
                                       <asp:DropDownList ID="ddlYarnLotNo" runat="server" CssClass="js-states">
                                        </asp:DropDownList>
                                       </td>
                                       <td>
                                        <asp:DropDownList ID="ddlColor" runat="server" CssClass="js-states" >
                                       </asp:DropDownList>
                                       </td>
                                        <td>
                                            <asp:TextBox ID="txtActProd"  runat="server" name="Act" Class="cls txt" Text='<%# DataBinder.Eval(Container.DataItem, "ActProd")%>' Width="60"></asp:TextBox>
                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                              TargetControlID="txtActProd" ValidChars="0123456789.">
                                            </cc1:FilteredTextBoxExtender>
                                        </td> 
                                          <td>
                                            <asp:TextBox ID="txtUtilization"  runat="server" name="Act" Class="cls" Text='<%# DataBinder.Eval(Container.DataItem, "Utilization")%>' Width="60"></asp:TextBox>
                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                              TargetControlID="txtUtilization" ValidChars="0123456789.">
                                            </cc1:FilteredTextBoxExtender>
                                        </td> 
                                         <td>
                                             <asp:TextBox ID="txtEfficency" runat="server" name="Eff" Class="cls tot" Text='<%# DataBinder.Eval(Container.DataItem, "Efficiency")%>' Width="60"></asp:TextBox>
                                             <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                  TargetControlID="txtEfficency" ValidChars="0123456789.">
                                             </cc1:FilteredTextBoxExtender>
                                        </td> 
                                          <td>
                                             <asp:TextBox ID="txtNoofCode" runat="server" name="Eff" Class="cls tot" Text='<%# DataBinder.Eval(Container.DataItem, "NoofCode")%>' Width="60"></asp:TextBox>
                                             <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender10" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                  TargetControlID="txtNoofCode" ValidChars="0123456789.">
                                             </cc1:FilteredTextBoxExtender>
                                        </td>
                                           <td>
                                             <asp:TextBox ID="txtNoofDoff" runat="server" name="Eff" Class="cls tot" Text='<%# DataBinder.Eval(Container.DataItem, "NoofDoff")%>' Width="60"></asp:TextBox>
                                             <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender11" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                  TargetControlID="txtNoofDoff" ValidChars="0123456789.">
                                             </cc1:FilteredTextBoxExtender>
                                        </td>
                                          <td>
                                             <asp:TextBox ID="txtConeWeight" runat="server" name="Eff" Class="cls tot" Text='<%# DataBinder.Eval(Container.DataItem, "ConeWeight")%>' Width="60"></asp:TextBox>
                                             <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                  TargetControlID="txtConeWeight" ValidChars="0123456789.">
                                             </cc1:FilteredTextBoxExtender>
                                        </td>
                                          <td>
                                            <asp:TextBox ID="txtHardWaste"  runat="server" name="Act" Class="cls txt" Text='<%# DataBinder.Eval(Container.DataItem, "HardWaste")%>' Width="60"></asp:TextBox>
                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender9" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                              TargetControlID="txtHardWaste" ValidChars="0123456789.">
                                            </cc1:FilteredTextBoxExtender>
                                        </td> 
                                         <td>
                                         <asp:TextBox ID="txtNetWeight"  runat="server" name="Act" Class="cls txt" Text='<%# DataBinder.Eval(Container.DataItem, "NetWeight")%>' Width="60"></asp:TextBox>
                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                              TargetControlID="txtNetWeight" ValidChars="0123456789.">
                                            </cc1:FilteredTextBoxExtender>
                                        
                                         </td>
                                          <td>
                                             <asp:TextBox ID="txtStoppageMin" runat="server" Class="cls" Text='<%# DataBinder.Eval(Container.DataItem, "StopMin")%>' Width="60"></asp:TextBox>
                                             <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                  TargetControlID="txtStoppageMin" ValidChars="0123456789.">
                                             </cc1:FilteredTextBoxExtender>  
                                        </td> 
                                        <td>
                                            <asp:TextBox ID="txtReason" runat="server"  Class="cls" Text='<%# DataBinder.Eval(Container.DataItem, "StopReason")%>' TextMode="MultiLine" Height="30"  Width="150"></asp:TextBox> 
                                        </td>               
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate></table></FooterTemplate>                                
			                </asp:Repeater>
					    </asp:Panel>
					        
					    </div>
					</div>
					<!-- table End -->
					
					
					
					
					
					<div class="form-group row"></div>	
                    <!-- Button start -->
                    <div class="txtcenter">
                        <asp:Button ID="btnSave" class="btn btn-success"  runat="server" Text="Save" 
                            ValidationGroup="Validate_Field" onclick="btnSave_Click"/>
                        <asp:Button ID="btnCancel" class="btn btn-danger" runat="server" Text="Cancel" onclick="btnCancel_Click"/>
                        <asp:Button ID="btnBackEnquiry" class="btn btn-success" runat="server" 
                            Text="Back" onclick="btnBackEnquiry_Click" />
                        <asp:Button ID="btnApprove" class="btn btn-success" runat="server" 
                            Text="Approve" onclick="btnApprove_Click" />
                    </div>
                    <!-- Button end -->
                  </div>      
				</form>
			
			</div><!-- panel white end -->
		    </div>
		    
		    <div class="col-md-2"></div>
		    
            
      
 </div> <!-- col-9 end -->
 </div><!-- col 12 end -->
      
  </div><!-- row end -->
 </div>
</asp:Content>

