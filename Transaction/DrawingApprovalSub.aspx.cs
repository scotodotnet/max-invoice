﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
public partial class Transaction_DrawingApprovalSub : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionUnplanReceiptNo;
    string SessionFinYearCode;
    string SessionFinYearVal;
    static string sum;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "ERP Module :: Drawing";
            HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Transaction"));
            li.Attributes.Add("class", "droplink active open");
            Initial_Data_Referesh();
            Load_Shift();
            Load_Supervisor();
            //Load_MachineName();
            Color();
            //Load_YarnLot();
            Load_Unit();
            if (Session["Trans_No"] == null)
            {
                SessionUnplanReceiptNo = "";
            }
            else
            {
                SessionUnplanReceiptNo = Session["Trans_No"].ToString();
                txtTransNo.Text = SessionUnplanReceiptNo;
                btnSearch_Click(sender, e);
                btnSave.Text = "Update";
            }
        }
        Load_OLD_data();
    }
    //private void Load_YarnLot()
    //{
    //    string query = "";
    //    DataTable dtdsupp = new DataTable();
    //    ddlYarnLotNo.Items.Clear();
    //    query = "Select Distinct YarnLotNo from LotNo_Stage where Types='Drawing' ";
    //    dtdsupp = objdata.RptEmployeeMultipleDetails(query);
    //    ddlYarnLotNo.DataSource = dtdsupp;
    //    DataRow dr = dtdsupp.NewRow();
    //    dr["YarnLotNo"] = "-Select-";
    //    dr["YarnLotNo"] = "-Select-";
    //    dtdsupp.Rows.InsertAt(dr, 0);
    //    ddlYarnLotNo.DataTextField = "YarnLotNo";
    //    ddlYarnLotNo.DataValueField = "YarnLotNo";
    //    ddlYarnLotNo.DataBind();

    //}

    private void Load_Unit()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlUnit.Items.Clear();
        query = "Select *from MstUnit where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlUnit.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["UnitCode"] = "-Select-";
        dr["UnitName"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlUnit.DataTextField = "UnitName";
        ddlUnit.DataValueField = "UnitName";
        ddlUnit.DataBind();

    }

    private void Load_Supervisor()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        txtSupervisor.Items.Clear();
        query = "Select *from MstEmployee where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        txtSupervisor.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["EmpName"] = "-Select-";
        dr["EmpName"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        txtSupervisor.DataTextField = "EmpName";
        txtSupervisor.DataValueField = "EmpName";
        txtSupervisor.DataBind();
    }
    private void Load_Shift()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlShift.Items.Clear();
        query = "Select *from MstShift where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlShift.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["ShiftName"] = "-Select-";
        dr["ShiftName"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlShift.DataTextField = "ShiftName";
        ddlShift.DataValueField = "ShiftName";
        ddlShift.DataBind();
    }
    private void Color()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlColor.Items.Clear();
        query = "Select *from MstColor where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlColor.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["ColorName"] = "-Select-";
        dr["ColorName"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlColor.DataTextField = "ColorName";
        ddlColor.DataValueField = "ColorName";
        ddlColor.DataBind();
    }


    //private void Load_MachineName()
    //{
    //    string query = "";
    //    DataTable dtdsupp = new DataTable();
    //    ddlMachineName.Items.Clear();
    //    query = "Select *from MstMachine where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And DeptName='DRAWING'";
    //    dtdsupp = objdata.RptEmployeeMultipleDetails(query);
    //    ddlMachineName.DataSource = dtdsupp;
    //    DataRow dr = dtdsupp.NewRow();
    //    dr["MachineName"] = "-Select-";
    //    dr["MachineCode"] = "-Select-";
    //    dtdsupp.Rows.InsertAt(dr, 0);
    //    ddlMachineName.DataTextField = "MachineName";
    //    ddlMachineName.DataValueField = "MachineCode";
    //    ddlMachineName.DataBind();
    //}
    private void Load_OLD_data()
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        Repeater1.DataSource = dt;
        Repeater1.DataBind();
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        //Search Gate_Pass_Out
        string query = "";
        DataTable Main_DT = new DataTable();
        query = "Select * from Drawing_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Trans_No='" + txtTransNo.Text + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        if (Main_DT.Rows.Count != 0)
        {
            txtTransNo.Text = Main_DT.Rows[0]["Trans_No"].ToString();
            txtDate.Text = Main_DT.Rows[0]["Trans_Date"].ToString();
            txtYarnLot.Text = Main_DT.Rows[0]["LotNo"].ToString().Trim();
            ddlColor.SelectedValue = Main_DT.Rows[0]["Color"].ToString();
            ddlShift.SelectedValue = Main_DT.Rows[0]["Shift"].ToString().Trim();
            ddlUnit.SelectedValue = Main_DT.Rows[0]["Unit"].ToString().Trim();
            txtSupervisor.SelectedValue = Main_DT.Rows[0]["Supervisor"].ToString();
            //txtSilver.Text = Main_DT.Rows[0]["Silver_Wastage"].ToString();
            //txtStopPage.Text = Main_DT.Rows[0]["Stop_Page_Mintus"].ToString();
            //txtRemarks.Text = Main_DT.Rows[0]["Reason"].ToString();


            //Unplanned_Receipt_Main_Sub Table Load
            DataTable dt = new DataTable();
            query = "Select *from Drawing_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Trans_No='" + txtTransNo.Text + "'";
            dt = objdata.RptEmployeeMultipleDetails(query);

            DataTable Temp = (DataTable)ViewState["ItemTable"];

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                Temp.NewRow();
                Temp.Rows.Add();
                Temp.Rows[Temp.Rows.Count - 1]["MachineCode"] = dt.Rows[i]["MachineCode"].ToString();
                Temp.Rows[Temp.Rows.Count - 1]["MachineName"] = dt.Rows[i]["MachineName"].ToString();
                Temp.Rows[Temp.Rows.Count - 1]["Speed"] = dt.Rows[i]["Speed"].ToString();
                Temp.Rows[Temp.Rows.Count - 1]["Hank"] = dt.Rows[i]["Hank"].ToString();
                Temp.Rows[Temp.Rows.Count - 1]["StdProduction"] = dt.Rows[i]["StdProduction"].ToString();
                Temp.Rows[Temp.Rows.Count - 1]["ActProduction"] = dt.Rows[i]["ActProduction"].ToString();
                Temp.Rows[Temp.Rows.Count - 1]["Efficiency"] = dt.Rows[i]["Efficiency"].ToString();
                Temp.Rows[Temp.Rows.Count - 1]["Utilization"] = dt.Rows[i]["Utilization"].ToString();
                Temp.Rows[Temp.Rows.Count - 1]["Silver_Wastage"] = dt.Rows[i]["Silver_Wastage"].ToString();
                Temp.Rows[Temp.Rows.Count - 1]["FS"] = dt.Rows[i]["FS"].ToString();
                Temp.Rows[Temp.Rows.Count - 1]["Stop_Page_Mintus"] = dt.Rows[i]["Stop_Page_Mintus"].ToString();
                Temp.Rows[Temp.Rows.Count - 1]["Reason"] = dt.Rows[i]["Reason"].ToString();
            }


            ViewState["ItemTable"] = Temp;
            Repeater1.DataSource = Temp;
            Repeater1.DataBind();



            btnSave.Text = "Update";
        }
        else
        {
            Clear_All_Field();
        }
    }
    private void Clear_All_Field()
    {
        txtTransNo.Text = ""; txtDate.Text = ""; txtSupervisor.SelectedValue = "-Select-";
        ddlShift.SelectedValue = "-Select-";
        txtYarnLot.Text = "";
        //txtSilver.Text = ""; 
        //txtStopPage.Text = ""; txtRemarks.Text = "";

        //ddlMachineName.SelectedValue = "-Select-"; ddlSide.SelectedValue = "-Select-";
        //txtSpeed.Text = "0"; txtHank.Text = "0"; txtStdProd.Text = "0.0";
        // txtActProd.Text = "0.0"; txtUtilization.Text = "0.0"; txtEfficiency.Text = "0.0";

        btnSave.Text = "Save";
        Initial_Data_Referesh();
        Session.Remove("Trans_No");
        //Load_Data_Enquiry_Grid();
    }
    protected void GridDeleteClick(object sender, CommandEventArgs e)
    {

    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        string query = "";
        DataTable DT_Check = new DataTable();
        DataTable qry_dt = new DataTable();
        string SaveMode = "Insert";
        bool ErrFlag = false;
        DataRow dr = null;

        if (ViewState["ItemTable"] != null)
        {
            DataTable dt = (DataTable)ViewState["ItemTable"];

            int i = 0;

            dt.Rows.Clear();

            int count = 0;

            foreach (RepeaterItem item in Repeater1.Items)
            {
                Label txtMachineCode = (Label)item.FindControl("txtMachineCode");
                Label txtMachineName = (Label)item.FindControl("txtMachineName");
                //Label txtSide = (Label)item.FindControl("txtSide");
                Label txtSpeed = (Label)item.FindControl("txtSpeed");
                Label txtHank = (Label)item.FindControl("txtHank");
                Label txtStdProd = (Label)item.FindControl("txtStdProd");
                TextBox txtActProd = (TextBox)item.FindControl("txtActProd");

                TextBox txtUtilization = (TextBox)item.FindControl("txtUtilization");
                TextBox txtSilver = (TextBox)item.FindControl("txtSilver");
                TextBox txtFS = (TextBox)item.FindControl("txtFS");


                TextBox txtStoppageMin = (TextBox)item.FindControl("txtStoppageMin");
                TextBox txtEfficency = (TextBox)item.FindControl("txtEfficency");
                TextBox txtReason = (TextBox)item.FindControl("txtReason");

                if (count == 0)
                {
                    if (txtActProd.Text == "" && txtStoppageMin.Text == "" && txtEfficency.Text == "" && txtReason.Text == "")
                    {
                        ErrFlag = true;
                        break;

                    }
                    else
                    {
                        dr = dt.NewRow();
                        dr["MachineCode"] = txtMachineCode.Text;
                        dr["MachineName"] = txtMachineName.Text;
                        dr["Speed"] = txtSpeed.Text;
                        dr["Hank"] = txtHank.Text;
                        dr["StdProduction"] = txtStdProd.Text;
                        dr["ActProduction"] = txtActProd.Text;
                        dr["Utilization"] = txtUtilization.Text;
                        dr["Efficiency"] = txtEfficency.Text;
                        dr["Silver_Wastage"] = txtSilver.Text;
                        dr["FS"] = txtFS.Text;
                        dr["Stop_Page_Mintus"] = txtStoppageMin.Text;
                        dr["Reason"] = txtReason.Text;

                        dt.Rows.Add(dr);
                        ViewState["ItemTable"] = dt;

                        txtMachineName.Text = "";
                        //txtSide.Text = "";
                        txtStdProd.Text = "";
                        txtActProd.Text = "";
                        txtEfficency.Text = "";
                        txtStoppageMin.Text = "";
                        txtReason.Text = "";
                        txtFS.Text = "";
                        txtSilver.Text = "";
                    }
                }
                else
                {
                    dr = dt.NewRow();
                    dr["MachineCode"] = txtMachineCode.Text;
                    dr["MachineName"] = txtMachineName.Text;
                    dr["Speed"] = txtSpeed.Text;
                    dr["Hank"] = txtHank.Text;
                    dr["StdProduction"] = txtStdProd.Text;
                    dr["ActProduction"] = txtActProd.Text;
                    dr["Utilization"] = txtUtilization.Text;
                    dr["Efficiency"] = txtEfficency.Text;
                    dr["Silver_Wastage"] = txtSilver.Text;
                    dr["FS"] = txtFS.Text;
                    dr["Stop_Page_Mintus"] = txtStoppageMin.Text;
                    dr["Reason"] = txtReason.Text;

                    dt.Rows.Add(dr);
                    ViewState["ItemTable"] = dt;

                    txtMachineName.Text = "";
                    //txtSide.Text = "";
                    txtStdProd.Text = "";
                    txtActProd.Text = "";
                    txtEfficency.Text = "";
                    txtStoppageMin.Text = "";
                    txtReason.Text = "";
                    txtFS.Text = "";
                    txtSilver.Text = "";
                }
                count = count + 1;
            }
        }
        //check with Item Details Add with Grid
        DT_Check = (DataTable)ViewState["ItemTable"];
        if (DT_Check.Rows.Count == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You have to add atleast one Item Details..');", true);
        }


        //User Rights Check Start
        bool Rights_Check = false;
        //if (btnSave.Text == "Update")
        //{
        //    Rights_Check = CommonClass_Function.ModifyRights_Check(SessionCcode, SessionLcode, SessionUserID, "7", "2", "Spares Receipt");
        //    if (Rights_Check == false)
        //    {
        //        ErrFlag = true;
        //        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Modify Spares Receipt Details..');", true);
        //    }
        //}
        //else
        //{
        //    Rights_Check = CommonClass_Function.AddRights_Check(SessionCcode, SessionLcode, SessionUserID, "7", "2", "Spares Receipt");
        //    if (Rights_Check == false)
        //    {
        //        ErrFlag = true;
        //        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Adding New Spares Receipt..');", true);
        //    }
        //}
        //User Rights Check End


        //Auto generate Transaction Function Call
        if (btnSave.Text != "Update")
        {
            if (!ErrFlag)
            {
                TransactionNoGenerate TransNO = new TransactionNoGenerate();
                string Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup(SessionCcode, SessionLcode, "Drawing", SessionFinYearVal);
                if (Auto_Transaction_No == "")
                {
                    ErrFlag = true;
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Auto Generate Transaction No Error... Contact Server Admin');", true);
                }
                else
                {
                    txtTransNo.Text = Auto_Transaction_No;
                }
            }
        }




        //Check With Already Approved Start
        DataTable DT_Check_Apprv = new DataTable();
        query = "Select * from Drawing_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Trans_No='" + txtTransNo.Text + "' And Status='1'";
        DT_Check_Apprv = objdata.RptEmployeeMultipleDetails(query);
        if (DT_Check_Apprv.Rows.Count != 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Do not Drawing Details Already Approved..');", true);
        }




        if (!ErrFlag)
        {
            query = "Select * from Drawing_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Trans_No='" + txtTransNo.Text + "'";
            DT_Check = objdata.RptEmployeeMultipleDetails(query);
            if (DT_Check.Rows.Count != 0)
            {
                SaveMode = "Update";
                query = "Delete from Drawing_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Trans_No='" + txtTransNo.Text + "'";
                objdata.RptEmployeeMultipleDetails(query);
                query = "Delete from Drawing_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Trans_No='" + txtTransNo.Text + "'";
                objdata.RptEmployeeMultipleDetails(query);
            }

            //Response.Write(strValue);
            //Insert Main Table
            query = "Insert Into Drawing_Main(Ccode,Lcode,FinYearCode,FinYearVal,Trans_No,Trans_Date,LotNo,Shift,Color,Supervisor,Silver_Wastage,Stop_Page_Mintus,Reason,Unit,Status) Values('" + SessionCcode + "',";
            query = query + " '" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + txtTransNo.Text + "','" + txtDate.Text + "','" + txtYarnLot.Text + "',";
            query = query + " '" + ddlShift.SelectedValue + "','" + ddlColor.SelectedValue + "','" + txtSupervisor.SelectedValue + "','0','0',";
            query = query + " '0','" + ddlUnit.SelectedValue + "','0')";
            objdata.RptEmployeeMultipleDetails(query);

            DataTable dt = new DataTable();
            dt = (DataTable)ViewState["ItemTable"];
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                query = "Insert Into Drawing_Main_Sub(Ccode,Lcode,FinYearCode,FinYearVal,Trans_No,Trans_Date,MachineCode,MachineName,Speed,Hank,";
                query = query + "StdProduction,ActProduction,Utilization,Efficiency,Silver_Wastage,FS,Stop_Page_Mintus,Reason) Values('" + SessionCcode + "',";
                query = query + " '" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + txtTransNo.Text + "','" + txtDate.Text + "','" + dt.Rows[i]["MachineCode"].ToString() + "','" + dt.Rows[i]["MachineName"].ToString() + "',";
                query = query + " '" + dt.Rows[i]["Speed"].ToString() + "','" + dt.Rows[i]["Hank"].ToString() + "','" + dt.Rows[i]["StdProduction"].ToString() + "',";
                query = query + "'" + dt.Rows[i]["ActProduction"].ToString() + "','" + dt.Rows[i]["Utilization"].ToString() + "','" + dt.Rows[i]["Efficiency"].ToString() + "',";
                query = query + "'" + dt.Rows[i]["Silver_Wastage"].ToString() + "','" + dt.Rows[i]["FS"].ToString() + "','" + dt.Rows[i]["Stop_Page_Mintus"].ToString() + "','" + dt.Rows[i]["Reason"].ToString() + "')";
                objdata.RptEmployeeMultipleDetails(query);
            }


            if (SaveMode == "Insert")
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Drawing Details Saved Successfully');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Drawing Details Updated Successfully');", true);
            }
            //Clear_All_Field();
            Session["Trans_No"] = txtTransNo.Text;
            btnSave.Text = "Update";
            Response.Redirect("DrawingApprovalMain.aspx");
            //Load_Data_Enquiry_Grid();
        }
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }
    //protected void ddlMachineName_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    string query = "";
    //    DataTable DT = new DataTable();

    //    query = "Select *from MstMachine where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
    //    query = query + " And MachineCode='" + ddlMachineName.SelectedValue + "'";
    //    DT = objdata.RptEmployeeMultipleDetails(query);

    //    if (DT.Rows.Count != 0)
    //    {
    //        txtStdProd.Text = DT.Rows[0]["ProdTarget"].ToString();
    //        txtSpeed.Text = DT.Rows[0]["Speed"].ToString();
    //        txtHank.Text = DT.Rows[0]["Speed"].ToString();
    //    }
    //    else
    //    {
    //        txtStdProd.Text = "0";
    //        txtSpeed.Text = "0";
    //        txtHank.Text = "0";
    //    }
    //}

    protected void txtActProd_TextChanged(object sender, EventArgs e)
    {
        //Act_Calc();
    }

    //public void Act_Calc()
    //{
    //    string Act_Prod = "";
    //    if (txtActProd.Text != "" && txtStdProd.Text != "")
    //    {
    //        Act_Prod = ((Convert.ToDecimal(txtActProd.Text) / (Convert.ToDecimal(txtStdProd.Text))) * (100)).ToString();
    //        Act_Prod = (Math.Round(Convert.ToDecimal(Act_Prod), 2, MidpointRounding.AwayFromZero)).ToString();
    //    }

    //    txtEfficiency.Text = Act_Prod;
    //}
    protected void btnBackEnquiry_Click(object sender, EventArgs e)
    {
        Session.Remove("Trans_No");
        Response.Redirect("DrawingEntry_Main.aspx");
    }

    protected void btnApprove_Click(object sender, EventArgs e)
    {
        string query = "";
        DataTable DT_Check = new DataTable();
        bool ErrFlag = false;

        //User Rights Check Start
        bool Rights_Check = false;

        query = "Select * from Drawing_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Trans_No='" + txtTransNo.Text + "'";
        DT_Check = objdata.RptEmployeeMultipleDetails(query);
        if (DT_Check.Rows.Count == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('First You have to Register this Service Receipt Details..');", true);
        }

        query = "Select * from Drawing_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Trans_No='" + txtTransNo.Text + "' And Status='1'";
        DT_Check = objdata.RptEmployeeMultipleDetails(query);
        if (DT_Check.Rows.Count != 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Service Receipt Details Already Approved..');", true);
        }
        if (!ErrFlag)
        {
            query = "Update Drawing_Main set Status='1' where Ccode='" + SessionCcode + "'";
            query = query + "  And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
            query = query + "  And Trans_No='" + txtTransNo.Text + "'";
            objdata.RptEmployeeMultipleDetails(query);

            //query = "Update LotNo_Stage set Types='OE' where YarnLotNo='" + ddlYarnLotNo.SelectedValue + "' and Unit='" + ddlUnit.SelectedValue + "' and ColorName='" + ddlColor.SelectedValue + "'";
            //objdata.RptEmployeeMultipleDetails(query);

            ////Inert Data to LotNo_Stage
            //query = "insert into LotNo_Stage(Unit,ColorName,YarnLotNo,Types,StartDate) Values(";
            //query = query + "'" + ddlUnit.SelectedValue + "','" + ddlColor.SelectedValue + "','" + ddlYarnLotNo.SelectedValue + "',";
            //query = query + "'Drawing','" + txtDate.Text + "')";
            //objdata.RptEmployeeMultipleDetails(query);





            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert(' Drawing Details Approved Successfully..');", true);
        }
        Session.Remove("Trans_No");
        Response.Redirect("DrawingApprovalMain.aspx");
    }

    private void Initial_Data_Referesh()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add(new DataColumn("MachineCode", typeof(string)));
        dt.Columns.Add(new DataColumn("MachineName", typeof(string)));
        dt.Columns.Add(new DataColumn("Side", typeof(string)));
        dt.Columns.Add(new DataColumn("Speed", typeof(string)));
        dt.Columns.Add(new DataColumn("Hank", typeof(string)));
        dt.Columns.Add(new DataColumn("StdProduction", typeof(string)));
        dt.Columns.Add(new DataColumn("ActProduction", typeof(string)));
        dt.Columns.Add(new DataColumn("Utilization", typeof(string)));
        dt.Columns.Add(new DataColumn("Efficiency", typeof(string)));
        dt.Columns.Add(new DataColumn("Silver_Wastage", typeof(string)));
        dt.Columns.Add(new DataColumn("FS", typeof(string)));
        dt.Columns.Add(new DataColumn("Stop_Page_Mintus", typeof(string)));
        dt.Columns.Add(new DataColumn("Reason", typeof(string)));

        Repeater1.DataSource = dt;
        Repeater1.DataBind();
        ViewState["ItemTable"] = Repeater1.DataSource;

        //dt = Repeater1.DataSource;
    }

    //protected void btnAddItem_Click(object sender, EventArgs e)
    //{
    //    DataTable dt = new DataTable();
    //    DataTable qry_dt = new DataTable();
    //    bool ErrFlag = false;
    //    DataRow dr = null;
    //    string query = "";

    //    if (txtSpeed.Text == "0.0" || txtSpeed.Text == "0" || txtSpeed.Text == "")
    //    {
    //        ErrFlag = true;
    //        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the Speed...');", true);
    //    }
    //    if (txtHank.Text == "0.0" || txtHank.Text == "0" || txtHank.Text == "")
    //    {
    //        ErrFlag = true;
    //        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the Hank...');", true);
    //    }

    //    if (txtActProd.Text == "0.0" || txtActProd.Text == "0" || txtActProd.Text == "")
    //    {
    //        ErrFlag = true;
    //        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the ActProd...');", true);
    //    }
    //    if (txtUtilization.Text == "0.0" || txtUtilization.Text == "0" || txtUtilization.Text == "")
    //    {
    //        ErrFlag = true;
    //        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the Utilization...');", true);
    //    }


    //    if (!ErrFlag)
    //    {

    //        //string ValuationType = qry_dt.Rows[0]["ValuationType"].ToString();
    //        // check view state is not null  
    //        if (ViewState["ItemTable"] != null)
    //        {
    //            //get datatable from view state   
    //            dt = (DataTable)ViewState["ItemTable"];

    //            //check Item Already add or not
    //            for (int i = 0; i < dt.Rows.Count; i++)
    //            {
    //                if (dt.Rows[i]["MachineCode"].ToString().ToUpper() == ddlMachineName.SelectedValue.ToString().ToUpper())
    //                {
    //                    ErrFlag = true;
    //                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('This Machine Already Added..');", true);
    //                }
    //            }
    //            if (!ErrFlag)
    //            {
    //                dr = dt.NewRow();
    //                dr["MachineCode"] = ddlMachineName.SelectedValue;
    //                dr["MachineName"] = ddlMachineName.SelectedItem.Text;
    //                dr["Side"] = ddlSide.SelectedValue;
    //                dr["Speed"] = txtSpeed.Text;
    //                dr["Hank"] = txtHank.Text;
    //                dr["StdProduction"] = txtStdProd.Text;
    //                dr["ActProduction"] = txtActProd.Text;
    //                dr["Utilization"] = txtUtilization.Text;
    //                dr["Efficiency"] = txtEfficiency.Text;
    //                dr["Silver_Wastage"] = txtSilver.Text;
    //                dr["FS"] = txtFS.Text;
    //                dr["Stop_Page_Mintus"] = txtStopPage.Text;
    //                dr["Reason"] = txtRemarks.Text;

    //                dt.Rows.Add(dr);
    //                ViewState["ItemTable"] = dt;
    //                Repeater1.DataSource = dt;
    //                Repeater1.DataBind();


    //                ddlMachineName.SelectedValue = "-Select-"; ddlSide.SelectedValue = "-Select-";
    //                txtSpeed.Text = "0"; txtHank.Text = "0"; txtStdProd.Text = "0.0";
    //                txtActProd.Text = "0.0"; txtUtilization.Text = "0.0"; txtEfficiency.Text = "0.0";

    //            }
    //        }
    //        else
    //        {
    //            dr = dt.NewRow();
    //            dr["MachineCode"] = ddlMachineName.SelectedValue;
    //            dr["MachineName"] = ddlMachineName.SelectedItem.Text;
    //            dr["Side"] = ddlSide.SelectedValue;
    //            dr["Speed"] = txtSpeed.Text;
    //            dr["Hank"] = txtHank.Text;
    //            dr["StdProduction"] = txtStdProd.Text;
    //            dr["ActProduction"] = txtActProd.Text;
    //            dr["Utilization"] = txtUtilization.Text;
    //            dr["Efficiency"] = txtEfficiency.Text;
    //            dr["Silver_Wastage"] = txtSilver.Text;
    //            dr["FS"] = txtFS.Text;
    //            dr["Stop_Page_Mintus"] = txtStopPage.Text;
    //            dr["Reason"] = txtRemarks.Text;

    //            dt.Rows.Add(dr);
    //            ViewState["ItemTable"] = dt;
    //            Repeater1.DataSource = dt;
    //            Repeater1.DataBind();


    //            ddlMachineName.SelectedValue = "-Select-"; ddlSide.SelectedValue = "-Select-";
    //            txtSpeed.Text = "0"; txtHank.Text = "0"; txtStdProd.Text = "0.0";
    //            txtActProd.Text = "0.0"; txtUtilization.Text = "0.0"; txtEfficiency.Text = "0.0";
    //        }
    //    }
    //}

    public void Get_Repeater(string Units)
    {
        DataTable dt = new DataTable();
        //DataTable dt_Empty = new DataTable();
        //ViewState["ItemTable"] = dt_Empty;
        Initial_Data_Referesh();
        bool ErrFlag = false;
        DataRow dr = null;
        string query = "";
        DataTable dtdsupp = new DataTable();

        //ddlMachineName.Items.Clear();
        query = "Select * from MstMachine where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And DeptName='DRAWING' and Unit='" + Units + "' order by cast(MachineCode as int) asc";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);


        //get datatable from view state   
        dt = (DataTable)ViewState["ItemTable"];

        for (int i = 0; i < dtdsupp.Rows.Count; i++)
        {
            dr = dt.NewRow();
            dr["MachineCode"] = dtdsupp.Rows[i]["MachineCode"].ToString();
            dr["MachineName"] = dtdsupp.Rows[i]["MachineName"].ToString();
            //dr["Side"] = "A";
            dr["Speed"] = dtdsupp.Rows[i]["Speed"].ToString();
            dr["Hank"] = dtdsupp.Rows[i]["StdHank"].ToString();
            dr["StdProduction"] = dtdsupp.Rows[i]["ProdTarget"].ToString();
            dr["ActProduction"] = "0";
            dr["Utilization"] = "0";
            dr["Efficiency"] = "0";
            dr["Silver_Wastage"] = "0";
            dr["FS"] = "0";
            dr["Stop_Page_Mintus"] = "0";
            dt.Rows.Add(dr);

        }

        ViewState["ItemTable"] = dt;
        Repeater1.DataSource = dt;
        Repeater1.DataBind();
    }
    protected void ddlUnit_SelectedIndexChanged(object sender, EventArgs e)
    {
        Get_Repeater(ddlUnit.SelectedValue);
        ////Get_Repeater(ddlUnit.SelectedValue);
        //bool ErrFlag = false;
        //if ((ddlYarnLotNo.SelectedValue == "0") || (ddlYarnLotNo.SelectedItem.Text == "-Select-"))
        //{
        //    ErrFlag = true;
        //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select the YarnLot No..');", true);
        //    Color();
        //    Load_Unit();
        //}
        //if (!ErrFlag)
        //{
        //    DataTable dt = new DataTable();
        //    string Query = "Select Unit,ColorName from LotNo_Stage where YarnLotNo='" + ddlYarnLotNo.SelectedValue + "' and Types='Drawing' and unit='" + ddlUnit.SelectedValue + "'";
        //    dt = objdata.RptEmployeeMultipleDetails(Query);
        //    if (dt.Rows.Count > 0)
        //    {
        //        //string query = "";
        //        //DataTable dtdsupp = new DataTable();
        //        //ddlColor.Items.Clear();
        //        //query = "Select *from MstColor where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        //        //dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        //        ddlColor.DataSource = dt;
        //        DataRow dr = dt.NewRow();
        //        dr["ColorName"] = "-Select-";
        //        dr["ColorName"] = "-Select-";
        //        dt.Rows.InsertAt(dr, 0);
        //        ddlColor.DataTextField = "ColorName";
        //        ddlColor.DataValueField = "ColorName";
        //        ddlColor.DataBind();

        //        Get_Repeater(ddlUnit.SelectedValue);

        //    }
        //    else
        //    {
        //        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('This YarnLot No and Unit did not have Color ..');", true);
        //        DataTable dt_Empty = new DataTable();
        //        ddlColor.DataSource = dt_Empty;
        //        ddlColor.DataBind();
        //        Load_Unit();
        //        Initial_Data_Referesh();
        //    }


        //}
    }

}
