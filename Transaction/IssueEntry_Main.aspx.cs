﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class Transaction_IssueEntry_Main : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionFinYearCode;
    string SessionFinYearVal;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        if (!IsPostBack)
        {
            Page.Title = "CMS | Issue Entry";
            HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Transaction"));
            li.Attributes.Add("class", "droplink active open");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();
        Load_Data_IssueEntry();
    }

    public void Load_Data_IssueEntry()
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select Issue_Entry_No,Issue_Entry_Date,YarnLotNo,Unit,ColorName from Issue_Entry_Main";
        query = query + " where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        RptIssueEntry.DataSource = DT;
        RptIssueEntry.DataBind();
   


    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {

    }

    protected void btnBackEnquiry_Click(object sender, EventArgs e)
    {

    }

    protected void btnAddNew_Click(object sender, EventArgs e)
    {
        Session.Remove("Issue_Entry_No");
        Response.Redirect("IssueEntry.aspx");
    }

    protected void GridEditIssueEntryClick(object sender, CommandEventArgs e)
    {
        string query = "";
        bool ErrFlag = false;

        DataTable dtdpurchase = new DataTable();
        query = "select * from Issue_Entry_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Issue_Entry_No='" + e.CommandName.ToString() + "'";
        dtdpurchase = objdata.RptEmployeeMultipleDetails(query);
        string status = dtdpurchase.Rows[0]["Issue_Entry_Status"].ToString();

        if (status == "" || status == "0")
        {
            string Issue_Entry_No_Str = e.CommandName.ToString();
            Session.Remove("Issue_Entry_No");
            Session["Issue_Entry_No"] = Issue_Entry_No_Str;
            Response.Redirect("IssueEntry.aspx");
        }
        else
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Sorry Issue Entry Approved..');", true);
        }
    }
    protected void GridDeleteIssueEntryClick(object sender, CommandEventArgs e)
    {
        string query = "";

        //User Rights Check Start
        bool ErrFlag = false;
        bool Rights_Check = false;

        Rights_Check = CommonClass_Function.DeleteRights_Check(SessionCcode, SessionLcode, SessionUserID, "4", "5", "Issue Entry");
        if (Rights_Check == false)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Delete Issue Entry List..');", true);
        }
        //User Rights Check End




        //Check With Already Approved Start
        DataTable DT_Check = new DataTable();
        query = "Select * from Issue_Entry_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Issue_Entry_No ='" + e.CommandName.ToString() + "' And D_Status='1'";
        DT_Check = objdata.RptEmployeeMultipleDetails(query);
        if (DT_Check.Rows.Count != 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Do not Delete Issue Entry Details Already Approved..');", true);
        }


        if (!ErrFlag)
        {

            DataTable dtdpurchase = new DataTable();
            query = "select D_Status from Issue_Entry_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Issue_Entry_No='" + e.CommandName.ToString() + "'";
            dtdpurchase = objdata.RptEmployeeMultipleDetails(query);
            string status = dtdpurchase.Rows[0]["Issue_Entry_Status"].ToString();

            if (status == "" || status == "0")
            {
                DataTable DT = new DataTable();
                query = "Select * from Issue_Entry_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Issue_Entry_No='" + e.CommandName.ToString() + "'";
                DT = objdata.RptEmployeeMultipleDetails(query);
                if (DT.Rows.Count != 0)
                {
                    //Delete Main Table
                    query = "Delete from Issue_Entry_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Issue_Entry_No='" + e.CommandName.ToString() + "'";
                    objdata.RptEmployeeMultipleDetails(query);

                    //Delete Main Sub Table
                    query = "Delete from Issue_Entry_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Issue_Entry_No='" + e.CommandName.ToString() + "'";
                    objdata.RptEmployeeMultipleDetails(query);

                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Issue Entry Details Deleted Successfully');", true);
                    Load_Data_IssueEntry();
                }
            }
        }
    }
    



       
}
        
    


