﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
public partial class Transaction_InvoiceMain : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionFinYearCode;
    string SessionFinYearVal;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();
        if (!IsPostBack)
        {
            Page.Title = "ERP Module :: Item Master";
            HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Master"));
            li.Attributes.Add("class", "droplink active open");
        }

        Load_Data();

    }
    private void Load_Data()
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select Invoice_Entry_No,Invoice_Entry_Date,SupplierID,SupplierName,OrderNum,NetAmount from Invoice_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        Repeater1.DataSource = DT;
        Repeater1.DataBind();
    }

    protected void GridEditClick(object sender, CommandEventArgs e)
    {
        string query = "";
        bool ErrFlag = false;

        DataTable dtdpurchase = new DataTable();
        query = "select * from Invoice_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Invoice_Entry_No='" + e.CommandName.ToString() + "'";
        dtdpurchase = objdata.RptEmployeeMultipleDetails(query);
        string status = dtdpurchase.Rows[0]["SO_Status"].ToString();

        if (status == "" || status == "0")
        {
            string Enquiry_No_Str = e.CommandName.ToString();
            Session.Remove("Invoice_Code");
            Session["Invoice_Code"] = Enquiry_No_Str;
            Response.Redirect("InvoiceSub.aspx");
        }
        else
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Sorry Invoice Approved..');", true);
        }

    }
    protected void GridDeleteClick(object sender, CommandEventArgs e)
    {
        string query = "";

        //User Rights Check Start
        bool ErrFlag = false;
        bool Rights_Check = false;

        //Check With Already Approved Start
        DataTable DT_Check = new DataTable();
        query = "Select * from Invoice_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Invoice_Entry_No ='" + e.CommandName.ToString() + "' And SO_Status='1'";
        DT_Check = objdata.RptEmployeeMultipleDetails(query);
        if (DT_Check.Rows.Count != 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Do not Delete Invoice Details Already Approved..');", true);
        }
        if (!ErrFlag)
        {
            DataTable DT = new DataTable();
            query = "Select * from Invoice_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Invoice_Entry_No='" + e.CommandName.ToString() + "'";
            DT = objdata.RptEmployeeMultipleDetails(query);
            if (DT.Rows.Count != 0)
            {
                query = "Delete from Invoice_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Invoice_Entry_No='" + e.CommandName.ToString() + "'";
                objdata.RptEmployeeMultipleDetails(query);

                query = "Delete from Invoice_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Invoice_Entry_No='" + e.CommandName.ToString() + "'";
                objdata.RptEmployeeMultipleDetails(query);

                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Invoice Details Deleted Successfully');", true);
                Load_Data();
                //Clear_All_Field();
            }
        }


        
    }
    protected void btnAdd_Click(object sender, EventArgs e)
    {
        Session.Remove("Invoice_Code");
        Response.Redirect("InvoiceSub.aspx");
    }


}
