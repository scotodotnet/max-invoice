﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class Transaction_WeightList_Main : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionFinYearCode;
    string SessionFinYearVal;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        if (!IsPostBack)
        {
            Page.Title = "CMS | Weight List";
            HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Transaction"));
            li.Attributes.Add("class", "droplink active open");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();
        Load_Data_WeightList_Grid();
    }


    private void Load_Data_WeightList_Grid()
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select Weight_List_No,Weight_List_Date,LotNo,NoOfBale,BaleWeight,TareWeight,NetWeight from Weight_List_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' and (WL_Status != '1' or WL_Status != ' ')";
        //query = "Select Weight_List_No,Weight_List_Date,LotNo,NoOfBale,BaleWeight,TareWeight,NetWeight from Weight_List_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' and D_Status='D'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        RptWeightList.DataSource = DT;
        RptWeightList.DataBind();
    }


    protected void btnAddNew_Click(object sender, EventArgs e)
    {
        Session.Remove("Weight_List_No");
        Response.Redirect("Weight_List.aspx");
    }

    protected void GridEditClick(object sender, CommandEventArgs e)
    {
        string query = "";
        bool ErrFlag = false;

        DataTable dtdpurchase = new DataTable();
        query = "select * from Weight_List_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Weight_List_No='" + e.CommandName.ToString() + "'";
        dtdpurchase = objdata.RptEmployeeMultipleDetails(query);
        string status = dtdpurchase.Rows[0]["WL_Status"].ToString();

        if (status == "" || status == "0")
        {
            string Weight_List_No_Str = e.CommandName.ToString();
            Session.Remove("Weight_List_No");
            Session["Weight_List_No"] = Weight_List_No_Str;
            Response.Redirect("Weight_List.aspx");

        }
        else
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Sorry Weight List Approved..');", true);
        }




        
    }
    protected void GridDeleteClick(object sender, CommandEventArgs e)
    {
        string query = "";

        //User Rights Check Start
        bool ErrFlag = false;
        bool Rights_Check = false;

        Rights_Check = CommonClass_Function.DeleteRights_Check(SessionCcode, SessionLcode, SessionUserID, "4", "5", "Weight List");
        if (Rights_Check == false)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Delete Weight List..');", true);
        }
        //User Rights Check End



        //Check With Already Approved Start
        DataTable DT_Check = new DataTable();
        query = "Select * from Weight_List_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Weight_List_No ='" + e.CommandName.ToString() + "' And WL_Status='1'";
        DT_Check = objdata.RptEmployeeMultipleDetails(query);
        if (DT_Check.Rows.Count != 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Do not Delete Weight List Details Already Approved..');", true);
        }





        if (!ErrFlag)
        {
            DataTable DT = new DataTable();
            query = "Select * from Weight_List_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Weight_List_No='" + e.CommandName.ToString() + "'";
            DT = objdata.RptEmployeeMultipleDetails(query);
            if (DT.Rows.Count != 0)
            {
                //Delete Main Table
                query = "Delete from Weight_List_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Weight_List_No='" + e.CommandName.ToString() + "'";
                objdata.RptEmployeeMultipleDetails(query);

                //Delete Main Sub Table
                query = "Delete from Weight_List_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Weight_List_No='" + e.CommandName.ToString() + "'";
                objdata.RptEmployeeMultipleDetails(query);


                //query = "update Weight_List_Main set D_Status='D' where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Weight_List_No='" + e.CommandName.ToString() + "'";
                //objdata.RptEmployeeMultipleDetails(query);

                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Weight List Details Deleted Successfully');", true);
                Load_Data_WeightList_Grid();
            }
        }
    }
   
}

