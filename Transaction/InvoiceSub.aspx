﻿<%@ Page Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="InvoiceSub.aspx.cs" Inherits="Transaction_InvoiceSub" Title="Untitled Page" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<script src='<%= ResolveUrl("../assets/js/master_list_jquery.min.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/master_list_jquery-ui.min.js") %>'></script>
<link href="<%= ResolveUrl("../assets/css/master_list_jquery-ui.css") %>" rel="stylesheet" type="text/css"/>

    <style>
.add-on .input-group-btn > .btn {
    border-left-width: 0;
    left:-2px;
    -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
}
/* stop the glowing blue shadow */
.add-on .form-control:focus {
    -webkit-box-shadow: none; 
            box-shadow: none;
    border-color:#cccccc; 
}
</style>
<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('.date-picker').datepicker({ format: 'dd/mm/yyyy' });
                $('.js-states').select2();
            }
        });
    };
</script>

<script type="text/javascript">
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#TblWeightList').dataTable( {
                    "order": [[1, "Asc"]]
                });
            }
        });
    };
   </script>
    <script>
        $(document).ready(function() {
            $('#TblWeightList').dataTable({
                "order": [[1, "Asc"]]
            });
        });
    </script>  

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#example').dataTable();
                $('.js-states').select2();
            }
        });
    };
</script>
<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('.date-picker').datepicker({ format: 'dd/mm/yyyy' });
                $('.js-states').select2();
            }
        });
    };
</script>

<script type="text/javascript">
    function SaveMsgAlert(msg) {
        swal(msg);
    }
</script>


<div class="page-breadcrumb">
                    <ol class="breadcrumb container">
                   <h4><li class="active">Invoice Entry</li></h4> 
                    </ol>
                </div>
<div id="main-wrapper" class="container">
 <div class="row">
    <div class="col-md-12">
    <div class="col-md-9">
			<div class="panel panel-white">
				<div class="panel panel-primary">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">Invoice Entry</h4>
				</div>
				</div>
				<form class="form-horizontal">
				<div class="panel-body">
					<div class="col-md-12">
					    <div class="row">
					      
					         <div class="form-group col-md-4">
					            <label for="exampleInputName">Invoice No<span class="mandatory">*</span></label>
                                 <asp:TextBox ID="txtInvoiceNo" runat="server" class="form-control"></asp:TextBox>
					            
					        </div>
					        
					        <div class="form-group col-md-4">
					          <label for="exampleInputName">Date<span class="mandatory">*</span></label>
                                <asp:TextBox ID="txtDate" runat="server" class="form-control date-picker"></asp:TextBox>
                                
					        </div>
					           <div class="form-group col-md-4">
    					            
                                    <label for="exampleInputName">Supplier Name<span class="mandatory">*</span></label>
					                <asp:DropDownList ID="ddlSupplier" runat="server" class="form-control">
                                    </asp:DropDownList>
					     </div>
					        
					      				        
					    </div>
					    <div class="row">
					     <div class="form-group col-md-4">
					            <label for="exampleInputName">Order No</label>
                                 <asp:TextBox ID="txtOrderNo" runat="server" class="form-control"></asp:TextBox>
					            
					        </div>
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">Order Date</label>
                                 <asp:TextBox ID="txtOrderDate" runat="server" class="form-control date-picker"></asp:TextBox>
					            
					        </div>
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">Despatched Through</label>
                                 <asp:TextBox ID="txtDespatchThrough" runat="server" class="form-control"></asp:TextBox>
					            
					        </div>
					    </div>
					    
					    <div class="row">
					     <div class="form-group col-md-4">
					            <label for="exampleInputName">L.R. No</label>
                                 <asp:TextBox ID="txtLRNo" runat="server" class="form-control"></asp:TextBox>
					            
					        </div>
					        
					         <div class="form-group col-md-4">
					            <label for="exampleInputName">Documents Through</label>
                                 <asp:TextBox ID="txtDocument" runat="server" class="form-control"></asp:TextBox>
					            
					        </div>
					         <div class="form-group col-md-4">
					            <label for="exampleInputName">Payment Terms</label>
                                 <asp:DropDownList ID="ddlPayemnt" runat="server" class="form-control">
                                 <asp:ListItem Text="-Select-" Value="0"></asp:ListItem>
                                 <asp:ListItem Text="Cash" Value="1"></asp:ListItem>
                                 <asp:ListItem Text="Net Bank" Value="2"></asp:ListItem>
                                 <asp:ListItem Text="Mobile Bank" Value="2"></asp:ListItem>
                                 </asp:DropDownList>
					            
					        </div>
					    </div>
					     <div class="row">
					      <div class="form-group col-md-4">
					            <label for="exampleInputName">E.Way Bill No</label>
                                 <asp:TextBox ID="txtEwayBill" runat="server" class="form-control"></asp:TextBox>
					        </div>
					     </div>
					</div>
					  <div class="clearfix"></div>				    
				    <div class="timeline-options">
				        <h4 class="panel-title">Item Details</h4>
				    </div>
				    
				    <div class="col-md-12">
				    <div class="row">
				     <div class="form-group col-md-4">
					            <label for="exampleInputName">Item Name</label>
                                 <asp:DropDownList ID="ddlItemName" runat="server" class="form-control" onselectedindexchanged="ddlItemName_SelectedIndexChanged" AutoPostBack="true">
                                 </asp:DropDownList>
					            
					        </div>
					        
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">HSN Code</label>
                                 <asp:TextBox ID="txtHSN" runat="server" class="form-control" Enabled="false"></asp:TextBox>
					            
					        </div>
					        
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">Price</label>
                                 <asp:TextBox ID="txtPrice" runat="server" class="form-control"></asp:TextBox>
					            
					        </div>
					        
				    </div>
				    <div class="row">
				    <div class="form-group col-md-2">
					            <label for="exampleInputName">Quantity</label>
                                 <asp:TextBox ID="txtQty" runat="server" class="form-control" AutoPostBack="true" ontextchanged="txtQty_TextChanged"></asp:TextBox>
					             
					        </div>
                         <div class="form-group col-md-2">
					            <label for="exampleInputName">Quantity Type</label>
                                <asp:TextBox ID="txtQtytyp"  Enabled="false" runat="server" class="form-control"></asp:TextBox>
					            
					        </div>
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">Amount</label>
                                 <asp:TextBox ID="txtAmount" runat="server" Enabled="false" class="form-control" ></asp:TextBox>
					            
					        </div>
					        
					        <div class="form-group col-md-4">
					        <br />
                                <asp:LinkButton ID="btnAddItem" runat="server" class="btn btn-success" onclick="btnAddItem_Click">Add</asp:LinkButton>
					        </div>
				    </div>
				    
				    </div>
				    <div class="col-md-12"><div class="row">
                    <div class="form-group col-md-4">
                        <label for="exampleInputName">Consist Item Name</label>
                        <asp:DropDownList ID="ddlConsistItemName" runat="server" class="form-control">
                        </asp:DropDownList>
                    </div>
                        <div class="form-group col-md-4">
					        <br />
                                <asp:LinkButton ID="btnAddConsistitem" runat="server" class="btn btn-success" onclick="btnAddConsistitem_Click">Add</asp:LinkButton>
					        </div>
                        </div></div>
                    <div class="form-group row"></div>
					<div class="col-md-12">
					<div class="row">
					    <asp:Repeater ID="RptInvioceEntry" OnItemCommand="RptInvioceEntry_ItemCommand" runat="server" EnableViewState="false">
			                    <HeaderTemplate>
                                    <table id="TblWeightList" class="display table">
                                        <thead>
                                            <tr>
                                                
                                                <th>Item Name</th>
                                                <th>HSN Code</th>
                                                <th>Price</th>
                                                <th>Quantity</th>
                                                <th>Quantity Type</th>
                                                <th>Amount</th>
                                                <th>Mode</th>
                                             
                                            </tr>
                                        </thead>
                                </HeaderTemplate>
                                <ItemTemplate> 
                                    <tr>
                                         
                                          <td><%# Eval("ItemName")%></td>
                                          <td><%# Eval("HSNCode")%></td>
                                          <td><%# Eval("Price")%></td>
                                          <td><%# Eval("Qty")%></td>
                                          <td><%# Eval("QtyType")%></td>
                                          <td><%# Eval("Amount")%></td>
                                          
                                          <td>
                                                   <%--  <asp:LinkButton ID="btnDeleteEnquiry_Grid" class="btn btn-danger btn-sm fa fa-trash-o"  runat="server" 
                                                        Text="" OnCommand="GridDeleteCottonInwardsClick" CommandArgument="Delete" CommandName='<%# Eval("BaleNo")%>' 
                                                        CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this Cotton Inwards details?');">
                                                    </asp:LinkButton>--%>
                                                    
                                         <%-- <asp:LinkButton ID="btnEdit_VarietyCode" class="btn btn-danger btn-sm fa fa-trash-o"  runat="server" 
                                           Text="" OnCommand="GridViewClick_VarietyCode" CommandArgument='<%# Eval("Lotno")%>' 
                                           CommandName='<%# Eval("InvoiceNo")%>'>
                                          </asp:LinkButton>  --%>    
                                                    
                                           <asp:LinkButton ID="btnDeleteGrid" class="btn btn-danger btn-sm fa fa-trash-o"  runat="server" 
                                                Text="" OnCommand="GridDeleteClick" OnClientClick="return confirm('Are you sure you want to delete this Item ?');" CommandArgument="Delete" CommandName='<%# Eval("ItemCode")%>'> 
                                            </asp:LinkButton>          
                                                    
                                        </td>
                                        
                                        
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate></table></FooterTemplate>                                
			                </asp:Repeater><asp:HiddenField id="hidden_fld" runat="server"></asp:HiddenField>
					    </div>
					</div>
					<div class="form-group row"></div>
					<div class="col-md-12">
					  <div class="row">
                           <div class="form-group col-md-2">
					            <label for="exampleInputName">Packing Amt</label>
					            <asp:TextBox ID="txtPackAmt"  class="form-control" runat="server" 
                                    Text="0" AutoPostBack="true" ontextchanged="txtPackAmt_TextChanged"></asp:TextBox>
					            <cc1:filteredtextboxextender ID="FilteredTextBoxExtender6" runat="server" 
                                    FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtPackAmt" ValidChars="0123456789.">
                                </cc1:filteredtextboxextender>
                                
                                  <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" 
                                FilterType="Numbers,Custom" TargetControlID="txtCourierAmt" ValidChars="."/>
					        </div>
					   <div class="form-group col-md-2">
					            <label for="exampleInputName">Courier Amt</label>
					            <asp:TextBox ID="txtCourierAmt" class="form-control" runat="server" 
                                    Text="0" AutoPostBack="true" ontextchanged="txtCourierAmt_TextChanged"></asp:TextBox>
					            <cc1:filteredtextboxextender ID="FilteredTextBoxExtender3" runat="server" 
                                    FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtCourierAmt" ValidChars="0123456789.">
                                </cc1:filteredtextboxextender>
                                
                                  <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" 
                                FilterType="Numbers,Custom" TargetControlID="txtCourierAmt" ValidChars="."/>
					        </div>
					  <div class="form-group col-md-2">
					            <label for="exampleInputName">Service Amt</label>
					            <asp:TextBox ID="txtService" class="form-control" runat="server" 
                                    Text="0" AutoPostBack="true" ontextchanged="txtService_TextChanged"></asp:TextBox>
					            <cc1:filteredtextboxextender ID="FilteredTextBoxExtender1" runat="server" 
                                    FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtService" ValidChars="0123456789.">
                                </cc1:filteredtextboxextender>
                                
                                  <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" 
                                FilterType="Numbers,Custom" TargetControlID="txtService" ValidChars="."/>
					        </div>
                           
					          <div class="form-group col-md-2">
					            <label for="exampleInputName">Discount (%)</label>
					            <asp:TextBox ID="txtDiscountPercent" class="form-control" runat="server" 
                                    Text="0" AutoPostBack="true" ontextchanged="txtDiscountPercent_TextChanged"></asp:TextBox>
					            <cc1:filteredtextboxextender ID="FilteredTextBoxExtender12" runat="server" 
                                    FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtDiscountPercent" ValidChars="0123456789.">
                                </cc1:filteredtextboxextender>
                                
                                  <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" 
                                FilterType="Numbers,Custom" TargetControlID="txtDiscountPercent" ValidChars="."/>
					        </div>
					         <div class="form-group col-md-2">
					            <label for="exampleInputName">Discount Amt</label>
					            <asp:Label ID="txtDiscountAmt" class="form-control" runat="server" Text="0" BackColor="#F3F3F3"></asp:Label>
					            <%--<cc1:filteredtextboxextender ID="FilteredTextBoxExtender13" runat="server" 
                                    FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtDiscountAmt" ValidChars="0123456789.">
                                </cc1:filteredtextboxextender>--%>
					        </div>
					        
					        <div class="form-group col-md-2">
					            <label for="exampleInputName">Sub Total</label>
                                 <asp:TextBox ID="txtSubTotal" Text="0" runat="server" Enabled="false" class="form-control"></asp:TextBox>
					            
					        </div>
					  </div>
					</div>
					<div class="col-md-12">
					    <div class="row">
					    <div class="form-group col-md-7">
					    </div>
					    <div class="form-group col-md-2">
					     <label for="exampleInputName">GST Type</label>
					    <asp:DropDownList ID="txtTaxName" runat="server" class="js-states form-control" AutoPostBack="true"
                                onselectedindexchanged="txtTaxName_SelectedIndexChanged">
                        </asp:DropDownList> 
					    </div>
					      <div class="form-group col-md-1">
					            <label for="exampleInputName">CGST</label>
					            <asp:Label ID="txtCGST" class="form-control" Text="0" runat="server" BackColor="#F3F3F3"></asp:Label>
					            <%--<cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtTax" ValidChars="0123456789.">
                                </cc1:FilteredTextBoxExtender>--%>
					        </div>
					        <div class="form-group col-md-2">
					            <label for="exampleInputName">CGST Amount</label>
					            <asp:Label ID="txtCGSTAmt" runat="server" class="form-control" Text="0" BackColor="#F3F3F3"></asp:Label>
					        </div>
					       
					    </div>
					    </div>
					<div class="col-md-12">
					    <div class="row">
					    <div class="form-group col-md-4"></div>
					     <div class="form-group col-md-1">
					            <label for="exampleInputName">SGST</label>
					            <asp:Label ID="txtSGST" class="form-control" runat="server" Text="0" BackColor="#F3F3F3"></asp:Label>
					            <%--<cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtTax" ValidChars="0123456789.">
                                </cc1:FilteredTextBoxExtender>--%>
					        </div>
					        <div class="form-group col-md-2">
					            <label for="exampleInputName">SGST Amount</label>
					            <asp:Label ID="txtSGSTAmt" runat="server" class="form-control" Text="0" BackColor="#F3F3F3"></asp:Label>
					        </div>
					        <div class="form-group col-md-1">
					            <label for="exampleInputName">IGST</label>
					            <asp:Label ID="txtIGST" class="form-control" runat="server" Text="0" BackColor="#F3F3F3"></asp:Label>
					            <%--<cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtTax" ValidChars="0123456789.">
                                </cc1:FilteredTextBoxExtender>--%>
					        </div>
					        <div class="form-group col-md-2">
					            <label for="exampleInputName">IGST Amount</label>
					            <asp:Label ID="txtIGSTAmt" runat="server" class="form-control" Text="0" BackColor="#F3F3F3"></asp:Label>
					        </div>
					        
					          <div class="form-group col-md-2">
					            <label for="exampleInputName">Total GST</label>
					            <asp:Label ID="txtTotalGST" runat="server" class="form-control" Text="0" BackColor="#F3F3F3"></asp:Label>
					        </div>
					   
					    </div>
					    </div>
					    
					       <div class="col-md-12">
					    <div class="row">
					      <div class="form-group col-md-10"></div>
					      <div class="form-group col-md-2">
					            <label for="exampleInputName">Net Amt</label>
					            <asp:Label ID="txtNetAmt" class="form-control" runat="server" Text="0.0" BackColor="#F3F3F3"></asp:Label>
					            <%--<cc1:filteredtextboxextender ID="FilteredTextBoxExtender14" runat="server" 
                                    FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtNetAmt" ValidChars="0123456789.">
                                </cc1:filteredtextboxextender>--%>
					        </div>
					    </div>
					    </div>
					
					
					<!-- Button start -->
					<div class="col-md-12">
					    <div class="row">					            
					        <div class="txtcenter">
					            <div class="col-sm-11">
					                <asp:Button ID="btnSave" class="btn btn-success" onclick="btnSave_Click"  runat="server" Text="Save" ValidationGroup="Validate_Field"/>
                                    <%--<asp:Button ID="Button1" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg" runat="server" Text="View" />--%>
                                    <asp:Button ID="btnCancel" class="btn btn-danger" runat="server" Text="Cancel"/>
                                    
                                    <asp:Button ID="btnApproval" class="btn btn-success" onclick="btnApproval_Click" runat="server" Text="Approval" />
                                </div>
					        </div>
					    </div>
					</div>
					<!-- Button end -->
					<div class="form-group row"></div>
					<!-- table start -->
					
					<!-- table End --> 
						
						
						
						
				</div><!-- panel body end -->
				</form>
			</div><!-- panel white end -->
		    </div><!-- col-9 end -->
		    
		     <!-- Dashboard start -->
		     <div class="col-lg-3 col-md-6">
                            <div class="panel panel-white" style="height: 100%;">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Dashboard Details</h4>
                                    <div class="panel-control">
                                        
                                        
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <asp:HiddenField ID="CGSTPHdnFld" runat="server" />
                                    <asp:HiddenField ID="SGSTPHdnFld" runat="server" />
                                    <asp:HiddenField ID="IGSTPHdnFld" runat="server" />
                                    
                                </div>
                            </div>
                        </div>  
                        
                        <div class="col-lg-3 col-md-6">
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div class="live-tile flip ha" data-mode="flip" data-speed="750" data-delay="3000">
                                       
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div class="live-tile flip ha" data-mode="flip" data-speed="750" data-delay="3000">
                                       
                                    </div>
                                </div>
                            </div>
                            
                        </div> 
		    <!-- Dashboard End -->
		    
		    <div class="col-md-2"></div>  
            
      
         
      </div><!-- col 12 end -->
  </div><!-- row end -->
 </div><!-- main-wrapper end -->
</asp:Content>

