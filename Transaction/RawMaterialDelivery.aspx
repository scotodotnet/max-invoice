<%@ Page Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="RawMaterialDelivery.aspx.cs" Inherits="Transaction_RawMaterialDelivery" Title="" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">


<script src='<%= ResolveUrl("../assets/js/master_list_jquery.min.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/master_list_jquery-ui.min.js") %>'></script>
<link href="<%= ResolveUrl("../assets/css/master_list_jquery-ui.css") %>" rel="stylesheet" type="text/css"/>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#example').dataTable();
            }
        });
    };
</script>

<script type="text/javascript">
    function SaveMsgAlert(msg) {
        swal(msg);
    }
</script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('.date-picker').datepicker({ format: 'dd/mm/yyyy' });
                $('.js-states').select2();
            }
        });
    };
</script>

<div class="page-breadcrumb">
    <ol class="breadcrumb container">
       <h4><li class="active">Raw Material</li></h4> 
    </ol>
</div>

                      
<div id="main-wrapper" class="container">
 <div class="row">
    <div class="col-md-12">
     
        
    
    <div class="page-inner">
   
                                       
            
            <div class="col-md-12">
			<div class="panel panel-white">
			<div class="panel panel-primary">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">Raw Material </h4>
				</div>
				</div>
				<form class="form-horizontal">
				<div class="panel-body">
				<div class="col-md-12">
					  <div class="row">
				            <div class="form-group col-md-6">
					            <label for="exampleInputName">Type</label>
					            <asp:RadioButtonList ID="RdpIssueType" class="form-control" runat="server" 
                                    RepeatColumns="3" AutoPostBack="true" 
                                    onselectedindexchanged="RdpIssueType_SelectedIndexChanged" >
                                <asp:ListItem Value="1" Text="Issue" Selected="True" style="padding-right:20px"></asp:ListItem>
                                <asp:ListItem Value="2" Text="GoDown Transfer" style="padding-right:20px" ></asp:ListItem>
                                <asp:ListItem Value="3" Text="Unit Transfer"></asp:ListItem>
                                </asp:RadioButtonList>
					        </div>
					        <div class="form-group col-md-3">
					            <label for="exampleInputName">Trans No<span class="mandatory">*</span></label>
					            <asp:Label ID="txtTransNo" runat="server" class="form-control" BackColor="#F3F3F3"></asp:Label>
					        </div>
					        <div class="form-group col-md-3">
					            <label for="exampleInputName">Date</label>
					            <asp:TextBox ID="txtDate" MaxLength="20" class="form-control date-picker" runat="server"></asp:TextBox>
					            <asp:RequiredFieldValidator ControlToValidate="txtDate" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtDate" ValidChars="0123456789./">
                                </cc1:FilteredTextBoxExtender>
					       </div>
				       </div>
				  </div>
					<div class="col-md-12">
					    <div class="row">
					        
					        <div class="form-group col-md-3">
					            <label for="exampleInputName">Unit</label>
					            <asp:DropDownList ID="txtUnit" runat="server" class="js-states form-control" Enabled="false">
                                </asp:DropDownList>  
                                <%--<asp:RequiredFieldValidator ControlToValidate="txtUnit" Display="Dynamic" InitialValue="-Select-" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator6" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>--%>
					        </div>
					        <div class="form-group col-md-3">
					            <label for="exampleInputName">From GoDown</label>
					            <asp:DropDownList ID="txtFromGoDown" runat="server" 
                                    class="js-states form-control" AutoPostBack="true" 
                                    onselectedindexchanged="txtFromGoDown_SelectedIndexChanged">
                                </asp:DropDownList>  
                                <asp:RequiredFieldValidator ControlToValidate="txtFromGoDown" Display="Dynamic" InitialValue="-Select-" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator3" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
					        </div>
					        <div class="form-group col-md-3">
					            <label for="exampleInputName">To GoDown</label>
					            <asp:DropDownList ID="txtToGoDown" runat="server" class="js-states form-control" Enabled="false">
                                </asp:DropDownList>  
                                <%--<asp:RequiredFieldValidator ControlToValidate="txtToGoDown" Display="Dynamic" InitialValue="-Select-" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator2" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>--%>
					        </div>
					        
					        <div class="form-group col-md-3">
					            <label for="exampleInputName">Remarks</label>
					            <asp:TextBox ID="txtRemarks" TextMode="MultiLine" class="form-control" runat="server"></asp:TextBox>
					        </div> 
					    </div>
					</div>
					        
					  
					<div class="clearfix"></div>				    
				    <div class="timeline-options">
				        <h4 class="panel-title">Item Details</h4>
				    </div>
				    <div class="col-md-12">
					    <div class="row">
					        <div class="form-group col-md-3">
					            <label for="exampleInputName">Lot No</label>
					            <asp:DropDownList ID="ddlLotNo" runat="server" 
                                    class="js-states form-control" AutoPostBack="true" 
                                    onselectedindexchanged="ddlLotNo_SelectedIndexChanged" >
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ControlToValidate="ddlLotNo" Display="Dynamic" InitialValue="-Select-" ValidationGroup="Item_Validate_Field" class="form_error" ID="RequiredFieldValidator5" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
					        </div>
					        <div class="form-group col-md-3">
					                    <label for="exampleInputName">Variety Name<span class="mandatory">*</span></label>
                                        <asp:DropDownList ID="ddlVarietyName" runat="server" 
                                            class="js-states form-control" AutoPostBack="true" 
                                            onselectedindexchanged="ddlVarietyName_SelectedIndexChanged">
                                        </asp:DropDownList>
					                    <asp:RequiredFieldValidator InitialValue="-Select-" Display="Dynamic" ControlToValidate="ddlVarietyName" ValidationGroup="Item_Validate_Field" class="form_error" ID="RequiredFieldValidator22" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                        </asp:RequiredFieldValidator>
                            </div>
					        
					        <div class="form-group col-md-2">
					            <label for="exampleInputName">No Of Bale</label>
					            <asp:TextBox ID="txtNoOfBale" class="form-control" runat="server" Text="0"></asp:TextBox>
					            <asp:RequiredFieldValidator ControlToValidate="txtNoOfBale" Display="Dynamic" InitialValue="" ValidationGroup="Item_Validate_Field" class="form_error" ID="RequiredFieldValidator9" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
					           
					        </div>
					        <div class="form-group col-md-2">
					            <label for="exampleInputName">Avl Bale</label>
					            <asp:Label ID="txtAvlBale" class="form-control" runat="server" Text="0" BackColor="#F3F3F3"></asp:Label>
					        </div>
					        
					        <div class="form-group col-md-2">
					            <label for="exampleInputName">Bale Weight</label>
					            <asp:TextBox ID="txtBaleWt" class="form-control" runat="server" Text="0"></asp:TextBox>
					            <asp:RequiredFieldValidator ControlToValidate="txtBaleWt" Display="Dynamic" InitialValue="" ValidationGroup="Item_Validate_Field" class="form_error" ID="RequiredFieldValidator4" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
					        </div>
					        </div>
					        </div>
					        
					     <div class="col-md-12">
					    <div class="row">   
					      <div class="form-group col-md-2">
					            <label for="exampleInputName">Avl Bale Wt</label>
					            <asp:Label ID="txtAvlBaleWt" class="form-control" runat="server" Text="0" BackColor="#F3F3F3"></asp:Label>
					            <asp:HiddenField ID="txtBaleValue" runat="server" />
					         </div>
					         
					      <div class="form-group col-md-1">
					            <br />
					            <asp:Button ID="btnAddItem" Width="50" Height="30" class="btn-success"  
                                    runat="server" Text="Add" ValidationGroup="Item_Validate_Field" 
                                    onclick="btnAddItem_Click" />
					        </div>
					    </div>
					</div>
				
				     
                    
                    <!-- table start -->
					<div class="col-md-12">
					    <div class="row">
					        <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
			                    <HeaderTemplate>
                                    <table id="example" class="display table">
                                        <thead>
                                            <tr>
                                                <th>S.No</th>
                                                <th>Lot No</th>
                                                <th>Variety Name</th>
                                                <th>No Of Bale</th>
                                                <th>Bale Wt</th>
                                                <th>Mode</th>
                                            </tr>
                                        </thead>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td><%# Container.ItemIndex + 1 %></td>
                                        <td><%# Eval("LotNo")%></td>
                                        <td><%# Eval("VarietyName")%></td>
                                        <td><%# Eval("NoOfBale")%></td>
                                        <td><%# Eval("BaleWeight")%></td>
                                        <td>
                                            <asp:LinkButton ID="btnDeleteGrid" class="btn btn-danger btn-sm fa fa-trash-o"  runat="server" 
                                                Text="" OnCommand="GridDeleteClick" CommandArgument='<%# Eval("VarietyID")%>' CommandName='<%# Eval("LotNo")%>' 
                                                CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this LotNo details?');">
                                            </asp:LinkButton>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate></table></FooterTemplate>                                
			                </asp:Repeater>
					    </div>
					</div>
					<!-- table End -->
					
					<div class="col-md-12">
					    <div class="row">
					     <div class="form-group col-md-8"></div>
					       <div class="form-group col-md-2">
					            <label for="exampleInputName">Total Bale</label>
					            <asp:TextBox ID="txtTotBale" class="form-control" runat="server" Text="0.0"></asp:TextBox>
					            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtTotBale" ValidChars="0123456789.">
                                </cc1:FilteredTextBoxExtender>
					        </div>
					      <div class="form-group col-md-2">
					            <label for="exampleInputName">Total Wt</label>
					            <asp:TextBox ID="txtTotWt" class="form-control" runat="server" Text="0.0"></asp:TextBox>
					            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender9" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtTotWt" ValidChars="0123456789.">
                                </cc1:FilteredTextBoxExtender>
					        </div>
					    </div>
					  </div>
					
				
					
					
					
					<div class="form-group row"></div>	
                    <!-- Button start -->
                    <div class="txtcenter">
                        <asp:Button ID="btnSave" class="btn btn-success"  runat="server" Text="Save" 
                            ValidationGroup="Validate_Field" onclick="btnSave_Click" />
                        <asp:Button ID="btnCancel" class="btn btn-danger" runat="server" Text="Cancel" 
                            onclick="btnCancel_Click" />
                        <asp:Button ID="btnBackEnquiry" class="btn btn-success" runat="server" 
                            Text="Back" onclick="btnBackEnquiry_Click"  />
                        <asp:Button ID="btnApprove" class="btn btn-success" runat="server" 
                            Text="Approve" onclick="btnApprove_Click" />
                    </div>
                    <!-- Button end -->
                  </div>      
				</form>
			
			</div><!-- panel white end -->
		    </div>
		    
		    <div class="col-md-2"></div>
		    
            
      
 </div> <!-- col-9 end -->
 </div><!-- col 12 end -->
      
  </div><!-- row end -->
 </div>




</asp:Content>

