﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
public partial class Transaction_Winding_Datails : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionUnplanReceiptNo;
    string SessionFinYearCode;
    string SessionFinYearVal;
    static string sum;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "ERP Module :: Yarn Production";
            HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Transaction"));
            li.Attributes.Add("class", "droplink active open");
            Initial_Data_Referesh();
            Load_Supplier();
            Load_Shift();
            Load_GoDown();
            //Load_Count();
            //Load_Color();
            //Load_Machine();
            Load_Unit();
            
            if (Session["Trans_No"] == null)
            {
                SessionUnplanReceiptNo = "";
            }
            else
            {
                SessionUnplanReceiptNo = Session["Trans_No"].ToString();
                txtReceiptNo.Text = SessionUnplanReceiptNo;
                btnSearch_Click(sender, e);
            }
        }
        Load_OLD_data();
    }
    private void Load_YarnLot()
    {
        //string query = "";
        //DataTable dtdsupp = new DataTable();
        //ddlYarnLotNo.Items.Clear();
        //query = "Select * from LotNo_Stage where Types='Winding' order by CAST(YarnLotNo as int) asc";
        //dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        //ddlYarnLotNo.DataSource = dtdsupp;
        //DataRow dr = dtdsupp.NewRow();
        //dr["YarnLotNo"] = "-Select-";
        //dr["YarnLotNo"] = "-Select-";
        //dtdsupp.Rows.InsertAt(dr, 0);
        //ddlYarnLotNo.DataTextField = "YarnLotNo";
        //ddlYarnLotNo.DataValueField = "YarnLotNo";
        //ddlYarnLotNo.DataBind();

    }
    private void Load_Unit()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlUnit.Items.Clear();
        query = "Select * from MstUnit where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlUnit.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["UnitName"] = "-Select-";
        dr["UnitName"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlUnit.DataTextField = "UnitName";
        ddlUnit.DataValueField = "UnitName";
        ddlUnit.DataBind();
    }
    private void Load_Supplier()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        txtSupplierName.Items.Clear();
        query = "Select * from MstEmployee where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        txtSupplierName.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["EmpName"] = "-Select-";
        dr["EmpCode"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        txtSupplierName.DataTextField = "EmpName";
        txtSupplierName.DataValueField = "EmpCode";
        txtSupplierName.DataBind();
    }
    private void Load_GoDown()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlGodown.Items.Clear();
        query = "Select * from MstGoDown where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlGodown.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["GoDownName"] = "-Select-";
        dr["GoDownID"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlGodown.DataTextField = "GoDownName";
        ddlGodown.DataValueField = "GoDownID";
        ddlGodown.DataBind();
    }
 

    private void Load_Shift()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlShift.Items.Clear();
        query = "Select ShiftName from MstShift where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlShift.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["ShiftName"] = "-Select-";
        dr["ShiftName"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlShift.DataTextField = "ShiftName";
        ddlShift.DataValueField = "ShiftName";
        ddlShift.DataBind();
    }

   
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        //Search Purchase Enquiry
        string query = "";
        DataTable Main_DT = new DataTable();
        query = "Select * from Winding_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Trans_No='" + txtReceiptNo.Text + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        if (Main_DT.Rows.Count != 0)
        {
            txtDate.Text = Main_DT.Rows[0]["Trans_Date"].ToString();
            //ddlYarnLotNo.SelectedValue = Main_DT.Rows[0]["LotNo"].ToString().Trim();
            ddlGodown.SelectedValue = Main_DT.Rows[0]["ToGoDownID"].ToString();
            //ddlColor.SelectedValue = Main_DT.Rows[0]["Color"].ToString();
            ddlShift.Text = Main_DT.Rows[0]["Shift"].ToString();
            //txtRemarks.Text = Main_DT.Rows[0]["Remarks"].ToString();
            txtSupplierName.SelectedValue = Main_DT.Rows[0]["PrepaidBy"].ToString();

            ddlUnit.SelectedValue = Main_DT.Rows[0]["Unit"].ToString();


            //txtRouter.Text = Main_DT.Rows[0]["RouterWaste"].ToString();

            // txtStopPage.Text = Main_DT.Rows[0]["StoppageMins"].ToString();
            //txtRemarks.Text = Main_DT.Rows[0]["StoppageReason"].ToString();

            //Unplanned_Receipt_Main_Sub Table Load
            DataTable dt = new DataTable();
            query = "Select * from Winding_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Trans_No='" + txtReceiptNo.Text + "'";
            dt = objdata.RptEmployeeMultipleDetails(query);

            DataTable Temp = (DataTable)ViewState["ItemTable"];

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                Temp.NewRow();
                Temp.Rows.Add();
                Temp.Rows[Temp.Rows.Count - 1]["MachineCode"] = dt.Rows[i]["MachineCode"].ToString();
                Temp.Rows[Temp.Rows.Count - 1]["MachineName"] = dt.Rows[i]["MachineName"].ToString();
                Temp.Rows[Temp.Rows.Count - 1]["CountID"] = dt.Rows[i]["CountID"].ToString();
                Temp.Rows[Temp.Rows.Count - 1]["CountName"] = dt.Rows[i]["CountName"].ToString();
                Temp.Rows[Temp.Rows.Count - 1]["Speed"] = dt.Rows[i]["Speed"].ToString();
                Temp.Rows[Temp.Rows.Count - 1]["StdProd"] = dt.Rows[i]["StdProd"].ToString();
                Temp.Rows[Temp.Rows.Count - 1]["ActProd"] = dt.Rows[i]["ActProd"].ToString();
                Temp.Rows[Temp.Rows.Count - 1]["Utilization"] = dt.Rows[i]["Utilization"].ToString();
                Temp.Rows[Temp.Rows.Count - 1]["Efficiency"] = dt.Rows[i]["Efficiency"].ToString();
                Temp.Rows[Temp.Rows.Count - 1]["NoofCode"] = dt.Rows[i]["NoofCode"].ToString();
                Temp.Rows[Temp.Rows.Count - 1]["NoofDoff"] = dt.Rows[i]["NoofDoff"].ToString();
                Temp.Rows[Temp.Rows.Count - 1]["ConeWeight"] = dt.Rows[i]["ConeWeight"].ToString();

                Temp.Rows[Temp.Rows.Count - 1]["NetWeight"] = dt.Rows[i]["NetWeight"].ToString();
                Temp.Rows[Temp.Rows.Count - 1]["HardWaste"] = dt.Rows[i]["HardWaste"].ToString();
                Temp.Rows[Temp.Rows.Count - 1]["StopMin"] = dt.Rows[i]["StopMin"].ToString();
                Temp.Rows[Temp.Rows.Count - 1]["StopReason"] = dt.Rows[i]["StopReason"].ToString();
                Temp.Rows[Temp.Rows.Count - 1]["LotNo"] = dt.Rows[i]["LotNo"].ToString();
                Temp.Rows[Temp.Rows.Count - 1]["Color"] = dt.Rows[i]["Color"].ToString();
            }

            ViewState["ItemTable"] = Temp;
            Repeater1.DataSource = Temp;
            Repeater1.DataBind();

            Temp = (DataTable)ViewState["ItemTable"];
            if (Temp.Rows.Count != 0)
            {
                int k = 0;
                foreach (RepeaterItem itemEquipment in Repeater1.Items)
                {
                    DropDownList yourDropDown = (DropDownList)itemEquipment.FindControl("ddlCount");

                    //to get the selected value of your dropdownlist
                    yourDropDown.SelectedValue = Temp.Rows[k]["CountID"].ToString();


                    DropDownList LotDropDown = (DropDownList)itemEquipment.FindControl("ddlYarnLotNo");

                    //to get the selected value of your dropdownlist
                    LotDropDown.SelectedValue = Temp.Rows[k]["LotNo"].ToString();

                    DropDownList ColorDropDown = (DropDownList)itemEquipment.FindControl("ddlColor");

                    //to get the selected value of your dropdownlist
                    ColorDropDown.SelectedValue = Temp.Rows[k]["Color"].ToString();
                    k++;
                }
            }

            btnSave.Text = "Update";
        }
    }
    //protected void GridDeleteClick(object sender, CommandEventArgs e)
    //{
    //    DataTable qry_dt = new DataTable();
    //    bool ErrFlag = false;
    //    string qry = "";


    //    DataTable dt = new DataTable();
    //    dt = (DataTable)ViewState["ItemTable"];
    //    for (int i = 0; i < dt.Rows.Count; i++)
    //    {
    //        if ((dt.Rows[i]["CountName"].ToString().ToUpper() == ddlCount.SelectedItem.Text.ToString().ToUpper()) && (dt.Rows[i]["LotNo"].ToString().ToUpper() == ddlYarnLotNo.SelectedValue))
    //        {
    //            dt.Rows.RemoveAt(i);
    //            dt.AcceptChanges();
    //        }
    //    }
    //    ViewState["ItemTable"] = dt;
    //    Load_OLD_data();

    //}
    protected void btnSave_Click(object sender, EventArgs e)
    {
        string query = "";
        DataTable DT_Check = new DataTable();
        DataTable qry_dt = new DataTable();
        string SaveMode = "Insert";
        bool ErrFlag = false;
        DataRow dr = null;

        if (ViewState["ItemTable"] != null)
        {
            DataTable dt = (DataTable)ViewState["ItemTable"];

            int i = 0;

            dt.Rows.Clear();

            int count = 0;

            foreach (RepeaterItem item in Repeater1.Items)
            {
                Label txtMachineCode = (Label)item.FindControl("txtMachineCode");
                Label txtMachineName = (Label)item.FindControl("txtMachineName");
                //Label txtCountID = (Label)item.FindControl("txtCountID");
                //Label txtCount = (Label)item.FindControl("txtCount");
                DropDownList CountDropDown = (DropDownList)item.FindControl("ddlCount");
               
                string txtCountID = CountDropDown.SelectedValue;
                //txtCountID.Text = value.ToString();
                string txtCount = CountDropDown.SelectedItem.Text;
                Label txtSpeed = (Label)item.FindControl("txtSpeed");
                Label txtStdProd = (Label)item.FindControl("txtStdProd");
                TextBox txtActProd = (TextBox)item.FindControl("txtActProd");
                TextBox txtUtilization = (TextBox)item.FindControl("txtUtilization");
                TextBox txtEfficency = (TextBox)item.FindControl("txtEfficency");

                DropDownList LotDropDown = (DropDownList)item.FindControl("ddlYarnLotNo");
                string LotNo = LotDropDown.SelectedValue;

                DropDownList ColorDropDown = (DropDownList)item.FindControl("ddlColor");
                string Color = ColorDropDown.SelectedValue;


                TextBox txtNoofCode = (TextBox)item.FindControl("txtNoofCode");





                TextBox txtNoofDoff = (TextBox)item.FindControl("txtNoofDoff");



                TextBox txtConeWeight = (TextBox)item.FindControl("txtConeWeight");
                TextBox txtHardWaste = (TextBox)item.FindControl("txtHardWaste");
                TextBox txtNetWeight = (TextBox)item.FindControl("txtNetWeight");

                TextBox txtStoppageMin = (TextBox)item.FindControl("txtStoppageMin");
                TextBox txtReason = (TextBox)item.FindControl("txtReason");

                if (count == 0)
                {
                    if (txtActProd.Text == "" && txtStoppageMin.Text == "" && txtEfficency.Text == "" && txtReason.Text == "")
                    {
                        ErrFlag = true;
                        break;

                    }
                    else
                    {
                        dr = dt.NewRow();
                        dr["MachineCode"] = txtMachineCode.Text;
                        dr["MachineName"] = txtMachineName.Text;
                        dr["CountID"] = txtCountID;
                        dr["CountName"] = txtCount;
                        dr["LotNo"] = LotNo;
                        dr["Color"] = Color;
                        dr["Speed"] = txtSpeed.Text;
                        dr["StdProd"] = txtStdProd.Text;
                        dr["ActProd"] = txtActProd.Text;
                        dr["Utilization"] = txtUtilization.Text;
                        dr["Efficiency"] = txtEfficency.Text;
                        dr["NoofCode"] = txtNoofCode.Text;
                        dr["NoofDoff"] = txtNoofDoff.Text;
                        dr["ConeWeight"] = txtConeWeight.Text;
                        dr["NetWeight"] = txtNetWeight.Text;
                        dr["HardWaste"] = txtHardWaste.Text;
                        dr["StopMin"] = txtStoppageMin.Text;
                        dr["StopReason"] = txtReason.Text;
                        dt.Rows.Add(dr);
                        ViewState["ItemTable"] = dt;


                        txtMachineCode.Text = "";
                        txtMachineName.Text = "";
                        txtCount = "";
                        txtStdProd.Text = "";
                        txtActProd.Text = "";
                        txtNoofCode.Text = "";
                        txtNoofDoff.Text = "";
                        txtConeWeight.Text = "";
                        txtNetWeight.Text = "";
                        txtHardWaste.Text = "";
                        txtSpeed.Text = "";
                        txtStoppageMin.Text = "";
                        txtReason.Text = "";
                        txtUtilization.Text = "";
                        txtEfficency.Text = "";
                        LotNo = "";
                        Color = "";
                    }
                }
                else
                {
                    dr = dt.NewRow();
                    dr["MachineCode"] = txtMachineCode.Text;
                    dr["MachineName"] = txtMachineName.Text;
                    dr["CountID"] = txtCountID;
                    dr["CountName"] = txtCount;
                    dr["LotNo"] = LotNo;
                    dr["Color"] = Color;
                    dr["Speed"] = txtSpeed.Text;
                    dr["StdProd"] = txtStdProd.Text;
                    dr["ActProd"] = txtActProd.Text;
                    dr["Utilization"] = txtUtilization.Text;
                    dr["Efficiency"] = txtEfficency.Text;
                    dr["NoofCode"] = txtNoofCode.Text;
                    dr["NoofDoff"] = txtNoofDoff.Text;
                    dr["ConeWeight"] = txtConeWeight.Text;
                    dr["NetWeight"] = txtNetWeight.Text;
                    dr["HardWaste"] = txtHardWaste.Text;
                    dr["StopMin"] = txtStoppageMin.Text;
                    dr["StopReason"] = txtReason.Text;
                    dt.Rows.Add(dr);
                    ViewState["ItemTable"] = dt;


                    txtMachineCode.Text = "";
                    txtMachineName.Text = "";
                    txtCount = "";
                    txtStdProd.Text = "";
                    txtActProd.Text = "";
                    txtNoofCode.Text = "";
                    txtNoofDoff.Text = "";
                    txtConeWeight.Text = "";
                    txtNetWeight.Text = "";
                    txtHardWaste.Text = "";
                    txtSpeed.Text = "";
                    txtStoppageMin.Text = "";
                    txtReason.Text = "";
                    txtUtilization.Text = "";
                    txtEfficency.Text = "";
                    LotNo = "";
                    Color = "";
                }
                count = count + 1;
            }
        }

        //check with Item Details Add with Grid
        DT_Check = (DataTable)ViewState["ItemTable"];
        if (DT_Check.Rows.Count == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You have to add atleast one Item Details..');", true);
        }

        //User Rights Check Start
        bool Rights_Check = false;
        //if (btnSave.Text == "Update")
        //{
        //    Rights_Check = CommonClass_Function.ModifyRights_Check(SessionCcode, SessionLcode, SessionUserID, "7", "2", "Unplanned Receipt");
        //    if (Rights_Check == false)
        //    {
        //        ErrFlag = true;
        //        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Modify Unplanned Receipt Details..');", true);
        //    }
        //}
        //else
        //{
        //    Rights_Check = CommonClass_Function.AddRights_Check(SessionCcode, SessionLcode, SessionUserID, "7", "2", "Unplanned Receipt");
        //    if (Rights_Check == false)
        //    {
        //        ErrFlag = true;
        //        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Adding New Unplanned Receipt..');", true);
        //    }
        //}
        //User Rights Check End

        //Auto generate Transaction Function Call
        if (btnSave.Text != "Update")
        {
            if (!ErrFlag)
            {
                TransactionNoGenerate TransNO = new TransactionNoGenerate();
                string Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup(SessionCcode, SessionLcode, "Winding", SessionFinYearVal);
                if (Auto_Transaction_No == "")
                {
                    ErrFlag = true;
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Auto Generate Transaction No Error... Contact Server Admin');", true);
                }
                else
                {
                    txtReceiptNo.Text = Auto_Transaction_No;
                }
            }
        }

        //Check With Already Approved Start
        DataTable DT_Check_Apprv = new DataTable();
        query = "Select * from Winding_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Trans_No='" + txtReceiptNo.Text + "' And Status='1'";
        DT_Check_Apprv = objdata.RptEmployeeMultipleDetails(query);
        if (DT_Check_Apprv.Rows.Count != 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Do not Update Unplanned Receipt Details Already Approved..');", true);
        }

        if (!ErrFlag)
        {
            query = "Select * from Winding_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Trans_No='" + txtReceiptNo.Text + "'";
            DT_Check = objdata.RptEmployeeMultipleDetails(query);
            if (DT_Check.Rows.Count != 0)
            {
                SaveMode = "Update";
                query = "Delete from Winding_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Trans_No='" + txtReceiptNo.Text + "'";
                objdata.RptEmployeeMultipleDetails(query);
                query = "Delete from Winding_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Trans_No='" + txtReceiptNo.Text + "'";
                objdata.RptEmployeeMultipleDetails(query);
            }

            //Insert Main Table
            query = "Insert Into Winding_Main(Ccode,Lcode,FinYearCode,FinYearVal,Trans_No,Trans_Date,Unit,Shift,Color,PrepaidBy,ToGoDownID,ToGoDownName,UserID,UserName,Status) Values('" + SessionCcode + "',";
            query = query + " '" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + txtReceiptNo.Text + "','" + txtDate.Text + "','" + ddlUnit.SelectedValue + "','" + ddlShift.SelectedItem.Text + "',";
            query = query + " '0','" + txtSupplierName.SelectedValue + "','" + ddlGodown.SelectedValue + "','" + ddlGodown.SelectedItem.Text + "','" + SessionUserID + "','" + SessionUserName + "','0')";
            objdata.RptEmployeeMultipleDetails(query);

            DataTable dt = new DataTable();
            dt = (DataTable)ViewState["ItemTable"];
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                query = "Insert Into Winding_Sub(Ccode,Lcode,FinYearCode,FinYearVal,Trans_No,Trans_Date,MachineCode,MachineName,CountID,CountName,StdProd,ActProd,StopMin,StopReason,";
                query = query + "Utilization,Efficiency,ConeWeight,NoofCode,NoofDoff,NetWeight,HardWaste,UserID,UserName,Speed,LotNo,Color) Values('" + SessionCcode + "',";
                query = query + " '" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + txtReceiptNo.Text + "','" + txtDate.Text + "','" + dt.Rows[i]["MachineCode"].ToString() + "','" + dt.Rows[i]["MachineName"].ToString() + "',";
                query = query + " '" + dt.Rows[i]["CountID"].ToString() + "','" + dt.Rows[i]["CountName"].ToString() + "','" + dt.Rows[i]["StdProd"].ToString() + "','" + dt.Rows[i]["ActProd"].ToString() + "','" + dt.Rows[i]["StopMin"].ToString() + "','" + dt.Rows[i]["StopReason"].ToString() + "',";
                query = query + " '" + dt.Rows[i]["Utilization"].ToString() + "','" + dt.Rows[i]["Efficiency"].ToString() + "','" + dt.Rows[i]["ConeWeight"].ToString() + "','" + dt.Rows[i]["NoofCode"].ToString() + "','" + dt.Rows[i]["NoofDoff"].ToString() + "',";
                query = query + "'" + dt.Rows[i]["NetWeight"].ToString() + "','" + dt.Rows[i]["HardWaste"].ToString() + "','" + SessionUserID + "','" + SessionUserName + "','" + dt.Rows[i]["Speed"].ToString() + "','" + dt.Rows[i]["LotNo"].ToString() + "','" + dt.Rows[i]["Color"].ToString() + "')";
                objdata.RptEmployeeMultipleDetails(query);
            }

            if (SaveMode == "Insert")
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Winding Details Saved Successfully');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Winding Details Updated Successfully');", true);
            }
            //Clear_All_Field();
            Session["Trans_No"] = txtReceiptNo.Text;
            btnSave.Text = "Update";
            Response.Redirect("Winding_Main.aspx");
            //Load_Data_Enquiry_Grid();

        }

    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {

    }


    protected void btnBackEnquiry_Click(object sender, EventArgs e)
    {
        Session.Remove("Trans_No");
        Response.Redirect("Winding_Main.aspx");
    }

    protected void btnApprove_Click(object sender, EventArgs e)
    {
        string query = "";
        DataTable DT_Check = new DataTable();
        bool ErrFlag = false;

        //User Rights Check Start
        bool Rights_Check = false;
        //Rights_Check = CommonClass_Function.ApproveRights_Check(SessionCcode, SessionLcode, SessionUserID, "7", "2", "Unplanned Receipt");
        //if (Rights_Check == false)
        //{
        //    ErrFlag = true;
        //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Approve Unplanned Receipt...');", true);
        //}
        //User Rights Check End

        query = "Select * from Winding_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Trans_No='" + txtReceiptNo.Text + "'";
        DT_Check = objdata.RptEmployeeMultipleDetails(query);
        if (DT_Check.Rows.Count == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('First You have to Register this Service Receipt Details..');", true);
        }

        query = "Select * from Winding_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Trans_No='" + txtReceiptNo.Text + "' And Status='1'";
        DT_Check = objdata.RptEmployeeMultipleDetails(query);
        if (DT_Check.Rows.Count != 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Service Receipt Details Already Approved..');", true);
        }
        if (!ErrFlag)
        {
            query = "Update Winding_Main set Status='1' where Ccode='" + SessionCcode + "'";
            query = query + "  And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
            query = query + "  And Trans_No='" + txtReceiptNo.Text + "'";
            objdata.RptEmployeeMultipleDetails(query);

            //Stock_Add();
            DataTable dt = new DataTable();
            DataTable dt_Find = new DataTable();
            dt = (DataTable)ViewState["ItemTable"];
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                query = "select * from Wind_Stock_Ledger_All where Trans_No='" + txtReceiptNo.Text + "' and CountID='" + dt.Rows[i]["CountID"].ToString() + "' and LotNo='" + dt.Rows[i]["LotNo"].ToString() + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                dt_Find = objdata.RptEmployeeMultipleDetails(query);
                if (dt_Find.Rows.Count > 0)
                {
                    query = "delete from Wind_Stock_Ledger_All where Trans_No='" + txtReceiptNo.Text + "' and CountID='" + dt.Rows[i]["CountID"].ToString() + "' and LotNo='" + dt.Rows[i]["LotNo"].ToString() + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                    objdata.RptEmployeeMultipleDetails(query);
                }

                query = "insert into Wind_Stock_Ledger_All(Ccode,Lcode,FinYearCode,FinYearVal,Trans_No,Trans_Date,Trans_Date_Str,Trans_Type,LotNo,CountID,";
                query = query + "CountName,Add_Qty,Minus_Qty,GoDownID,GoDownName,UserID,UserName) Values('" + SessionCcode + "',";
                query = query + " '" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + txtReceiptNo.Text + "','" + Convert.ToDateTime(txtDate.Text).ToString("yyyy/MM/dd") + "','" + txtDate.Text + "','Wind Production',";
                query = query + " '" + dt.Rows[i]["LotNo"].ToString()  + "','" + dt.Rows[i]["CountID"].ToString() + "','" + dt.Rows[i]["CountName"].ToString() + "','" + dt.Rows[i]["ActProd"].ToString() + "',";
                query = query + "'0','" + ddlGodown.SelectedValue + "','" + ddlGodown.SelectedItem.Text + "','" + SessionUserID + "','" + SessionUserName + "')";
                objdata.RptEmployeeMultipleDetails(query);



            query = "delete from LotNo_Stage where YarnLotNo='" + dt.Rows[i]["LotNo"].ToString() + "' and Unit='" + ddlUnit.SelectedValue + "' and ColorName='" + dt.Rows[i]["Color"].ToString() + "'";
            objdata.RptEmployeeMultipleDetails(query);

            }
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Yarn Production Receipt Details Approved Successfully..');", true);

            Response.Redirect("Winding_Main.aspx");
        }
    }

    private void Load_OLD_data()
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        Repeater1.DataSource = dt;
        Repeater1.DataBind();

        DataTable DT_dt = (DataTable)ViewState["ItemTable"];

        if (DT_dt.Rows.Count != 0)
        {

            int k = 0;
            foreach (RepeaterItem item1 in Repeater1.Items)
            {
                DropDownList yourDropDown = (DropDownList)item1.FindControl("ddlCount");
                yourDropDown.SelectedValue = DT_dt.Rows[k]["CountID"].ToString();
                DropDownList LotDropDown = (DropDownList)item1.FindControl("ddlYarnLotNo");
                LotDropDown.SelectedValue = DT_dt.Rows[k]["LotNo"].ToString();
                DropDownList ColorDropDown = (DropDownList)item1.FindControl("ddlColor");
                ColorDropDown.SelectedValue = DT_dt.Rows[k]["Color"].ToString();
                k++;
            }
        }
            

    }

    private void Initial_Data_Referesh()
    {
        DataTable dt = new DataTable();

        dt.Columns.Add(new DataColumn("MachineCode", typeof(string)));
        dt.Columns.Add(new DataColumn("MachineName", typeof(string)));
        dt.Columns.Add(new DataColumn("CountID", typeof(string)));
        dt.Columns.Add(new DataColumn("CountName", typeof(string)));
        dt.Columns.Add(new DataColumn("Color", typeof(string)));
        dt.Columns.Add(new DataColumn("LotNo", typeof(string)));
        dt.Columns.Add(new DataColumn("Speed", typeof(string)));
        dt.Columns.Add(new DataColumn("StdProd", typeof(string)));
        dt.Columns.Add(new DataColumn("ActProd", typeof(string)));
        dt.Columns.Add(new DataColumn("StopMin", typeof(string)));
        dt.Columns.Add(new DataColumn("StopReason", typeof(string)));
        dt.Columns.Add(new DataColumn("Utilization", typeof(string)));
        dt.Columns.Add(new DataColumn("Efficiency", typeof(string)));
        dt.Columns.Add(new DataColumn("ConeWeight", typeof(string)));
        dt.Columns.Add(new DataColumn("NoofCode", typeof(string)));
        dt.Columns.Add(new DataColumn("NoofDoff", typeof(string)));
        dt.Columns.Add(new DataColumn("NetWeight", typeof(string)));
        dt.Columns.Add(new DataColumn("HardWaste", typeof(string)));



        Repeater1.DataSource = dt;
        Repeater1.DataBind();
        ViewState["ItemTable"] = Repeater1.DataSource;

        //dt = Repeater1.DataSource;
    }

    //protected void btnAddItem_Click(object sender, EventArgs e)
    //{
    //    DataTable dt = new DataTable();
    //    DataTable qry_dt = new DataTable();
    //    bool ErrFlag = false;
    //    DataRow dr = null;
    //    string query = "";

    //    //if (txtActProd.Text == "0.0" || txtActProd.Text == "0" || txtActProd.Text == "")
    //    //{
    //    //    ErrFlag = true;
    //    //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the Actual Production...');", true);
    //    //}

    //    if (!ErrFlag)
    //    {
    //        // check view state is not null  
    //        if (ViewState["ItemTable"] != null)
    //        {
    //            //get datatable from view state   
    //            dt = (DataTable)ViewState["ItemTable"];

    //            //check Item Already add or not
    //            for (int i = 0; i < dt.Rows.Count; i++)
    //            {
    //                if ((dt.Rows[i]["CountID"].ToString().ToUpper() == ddlCount.SelectedValue.ToString().ToUpper()) && (dt.Rows[i]["LotNo"].ToString().ToUpper() == ddlYarnLotNo.SelectedValue))
    //                {
    //                    ErrFlag = true;
    //                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('This Count Already Added..');", true);
    //                }

    //            }
    //            if (!ErrFlag)
    //            {
    //                dr = dt.NewRow();

    //                dr["MachineCode"] = ddlMachine.SelectedValue;
    //                dr["MachineName"] = ddlMachine.SelectedItem.Text;
    //                dr["CountID"] = ddlCount.SelectedValue;
    //                dr["CountName"] = ddlCount.SelectedItem.Text;
    //                dr["StdProd"] = txtStdProd.Text;
    //                dr["ActProd"] = txtActProd.Text;
    //                dr["StopMin"] = txtStoppage.Text;
    //                dr["StopReason"] = txtStoppageReason.Text;
    //                dr["Utilization"] = txtUtilization.Text;
    //                dr["Efficiency"] = txtEfficiency.Text;
    //                dr["ConeWeight"] = txtConeWeight.Text;

    //                dr["NoofCode"] = txtNoofCone.Text;
    //                dr["NoofDoff"] = txtDoff.Text;
    //                dr["NetWeight"] = txtNetWeight.Text;
    //                dr["HardWaste"] = txtHardWaste.Text;
    //                dt.Rows.Add(dr);
    //                ViewState["ItemTable"] = dt;
    //                Repeater1.DataSource = dt;
    //                Repeater1.DataBind();


    //                ddlCount.SelectedValue = "-Select-";

    //                ddlMachine.SelectedValue = "-Select-";

    //                txtStdProd.Text = "0";
    //                txtActProd.Text = "0";
    //                txtSpeed.Text = "0";
    //                txtStoppage.Text = "0";
    //                txtStoppageReason.Text = "0";
    //                txtUtilization.Text = "0";
    //                txtEfficiency.Text = "0";
    //                txtConeWeight.Text = "0";
    //                txtNoofCone.Text = "0";
    //                txtDoff.Text = "0";
    //                txtNetWeight.Text = "0";
    //                txtHardWaste.Text = "0";


                   
    //            }
    //        }
    //    }
    //}
    //protected void ddlMachine_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    DataTable dt = new DataTable();
    //    string Query = "select Speed,ProdTarget from MstMachine where MachineCode='" + ddlMachine.SelectedValue + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
    //    dt = objdata.RptEmployeeMultipleDetails(Query);
    //    if (dt.Rows.Count > 0)
    //    {
    //        txtSpeed.Text = dt.Rows[0]["Speed"].ToString();
    //        txtStdProd.Text = dt.Rows[0]["ProdTarget"].ToString();
    //    }
    //}
    //public void Act_Calc()
    //{
    //    string Act_Prod = "";
    //    if (txtActProd.Text != "" && txtStdProd.Text != "")
    //    {
    //        Act_Prod = ((Convert.ToDecimal(txtActProd.Text) / (Convert.ToDecimal(txtStdProd.Text))) * (100)).ToString();
    //        Act_Prod = (Math.Round(Convert.ToDecimal(Act_Prod), 2, MidpointRounding.AwayFromZero)).ToString();
    //    }

    //    txtEfficiency.Text = Act_Prod;
    //}
    //protected void txtActProd_TextChanged(object sender, EventArgs e)
    //{
    //    Act_Calc();
    //}

    protected void ddlYarnLotNo_SelectedIndexChanged(object sender, EventArgs e)
    {
        //DataTable dt = new DataTable();
        //string Query = "Select Unit,ColorName from LotNo_Stage where YarnLotNo='" + ddlYarnLotNo.SelectedValue + "' and Types='Winding'";
        //dt = objdata.RptEmployeeMultipleDetails(Query);
        //if (dt.Rows.Count > 0)
        //{
        //    ddlUnit.SelectedValue = dt.Rows[0]["Unit"].ToString().Trim();
        //    ddlColor.SelectedValue = dt.Rows[0]["ColorName"].ToString().Trim();
        //    Get_Repeater(ddlUnit.SelectedValue);
        //}
        //else
        //{
        //    Load_Color();
        //    Load_Unit();
        //}
    }

    public void Get_Repeater(string Units)
    {
        DataTable dt = new DataTable();
        //DataTable dt_Empty = new DataTable();
        //ViewState["ItemTable"] = dt_Empty;
        Initial_Data_Referesh();
        bool ErrFlag = false;
        DataRow dr = null;
        string query = "";
        DataTable dtdsupp = new DataTable();

        //ddlMachineName.Items.Clear();
        query = "select MM.MachineCode,MM.MachineName,MC.CountID,MC.CountName,MM.ProdTarget,MM.Speed from MstMachine MM inner join MstCount MC on MM.Counts=MC.CountID and MM.Lcode=MC.Lcode ";
        query = query + "where MM.Ccode='" + SessionCcode + "' And MM.Lcode='" + SessionLcode + "' and MC.Ccode='" + SessionCcode + "' And MC.Lcode='" + SessionLcode + "' And MM.DeptName<>'OE' AND MM.DeptName<>'DRAWING' AND MM.DeptName<>'CARDING' and MM.Unit='" + Units + "' order by cast(MachineCode as int) asc ";
        //query = "Select * from MstMachine ";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        
            //get datatable from view state   
            dt = (DataTable)ViewState["ItemTable"];

            for (int i = 0; i < dtdsupp.Rows.Count; i++)
            {
                dr = dt.NewRow();
                dr["MachineCode"] = dtdsupp.Rows[i]["MachineCode"].ToString();
                dr["MachineName"] = dtdsupp.Rows[i]["MachineName"].ToString();
                dr["CountID"] = "0";
                dr["CountName"] = "0";
                dr["LotNo"] = "0";
                dr["Color"] = "0";
                dr["Speed"] = dtdsupp.Rows[i]["Speed"].ToString();
                //dr["Hank"] = dtdsupp.Rows[i]["StdHank"].ToString();
                dr["StdProd"] = dtdsupp.Rows[i]["ProdTarget"].ToString();
                dr["ActProd"] = "0";
                dr["Utilization"] = "0";
                dr["Efficiency"] = "0";
                dr["NoofCode"] = "0";
                dr["NoofDoff"] = "0";
                dr["ConeWeight"] = "0";
                dr["NetWeight"] = "0";
                dr["HardWaste"] = "0";
                dr["StopMin"] = "0";
                dr["StopReason"] = "";
                dt.Rows.Add(dr);

            }

            ViewState["ItemTable"] = dt;
            Repeater1.DataSource = dt;
            Repeater1.DataBind();
    }

    protected void ddlUnit_SelectedIndexChanged(object sender, EventArgs e)
    {
        Get_Repeater(ddlUnit.SelectedValue);
    }

    protected void ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        
        if (e.Item.ItemType == ListItemType.Item ||
        e.Item.ItemType == ListItemType.AlternatingItem)
        {
            var degree_dropdown = e.Item.FindControl("ddlCount") as DropDownList;
            //PopulateDropDownList(degree_dropdown, "CountName", "CountID");
            if (degree_dropdown != null)
            {

                DataTable dtdsupp = new DataTable();
                degree_dropdown.Items.Clear();
                string query = "Select * from MstCount where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                dtdsupp = objdata.RptEmployeeMultipleDetails(query);
                degree_dropdown.DataSource = dtdsupp;
                //DataRow dr = dtdsupp.NewRow();
                //dr["EmpName"] = "-Select-";
                //dr["EmpCode"] = "-Select-";
                //dtdsupp.Rows.InsertAt(dr, 0);
                degree_dropdown.DataTextField = "CountName";
                degree_dropdown.DataValueField = "CountID";
                degree_dropdown.DataBind();
                degree_dropdown.Items.Insert(0, new ListItem("-Select-", "0"));
                degree_dropdown.Items.FindByValue("0").Selected = true;


            }

            var Lot_dropdown = e.Item.FindControl("ddlYarnLotNo") as DropDownList;
            //PopulateDropDownList(degree_dropdown, "CountName", "CountID");
            if (Lot_dropdown != null)
            {

                DataTable dtdsupp = new DataTable();
                Lot_dropdown.Items.Clear();

                string query = "Select distinct YarnLotNo from LotNo_Stage where Types='Winding' and Unit='" + ddlUnit.SelectedValue + "'";
                dtdsupp = objdata.RptEmployeeMultipleDetails(query);
                Lot_dropdown.DataSource = dtdsupp;
                Lot_dropdown.DataTextField = "YarnLotNo";
                Lot_dropdown.DataValueField = "YarnLotNo";
                Lot_dropdown.DataBind();
                Lot_dropdown.Items.Insert(0, new ListItem("-Select-", "0"));
                Lot_dropdown.Items.FindByValue("0").Selected = true;
            }

            var Color_dropdown = e.Item.FindControl("ddlColor") as DropDownList;

            if (Color_dropdown != null)
            {
                DataTable dtdsupp = new DataTable();
                Color_dropdown.Items.Clear();

                string query = "";
               // DataTable dtdsupp = new DataTable();
                //ddlColor.Items.Clear();
                query = "Select ColorName from MstColor where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                dtdsupp = objdata.RptEmployeeMultipleDetails(query);
                Color_dropdown.DataSource = dtdsupp;
                //DataRow dr = dtdsupp.NewRow();
                //dr["ColorName"] = "-Select-";
                //dr["ColorName"] = "-Select-";
                //dtdsupp.Rows.InsertAt(dr, 0);
                Color_dropdown.DataTextField = "ColorName";
                Color_dropdown.DataValueField = "ColorName";
                Color_dropdown.DataBind();
                Color_dropdown.Items.Insert(0, new ListItem("-Select-", "0"));
                Color_dropdown.Items.FindByValue("0").Selected = true;
            }

            
        }
    }

}
