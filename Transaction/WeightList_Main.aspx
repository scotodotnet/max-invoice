﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="WeightList_Main.aspx.cs" Inherits="Transaction_WeightList_Main" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<script src='<%= ResolveUrl("../assets/js/master_list_jquery.min.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/master_list_jquery-ui.min.js") %>'></script>
<link href="<%= ResolveUrl("../assets/css/master_list_jquery-ui.css") %>" rel="stylesheet" type="text/css"/>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function(sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    $('#TblWeightList').dataTable();
                }
            });
        };
   </script>
   
    <script>
        $(document).ready(function() {
            $('#TblWeightList').dataTable();
        });
    </script>
    
    <link href="<%= ResolveUrl("../assets/css/Popup.css") %>" rel="stylesheet" type="text/css"/>
    <script>
        function pageLoad(sender, args) {
            if (!args.get_isPartialLoad()) {
                //  add our handler to the document's
                //  keydown event
                $addHandler(document, "keydown", onKeyDown);
            }
        }

        function onKeyDown(e) {
            if (e && e.keyCode == Sys.UI.Key.esc) {
                // if the key pressed is the escape key, dismiss the dialog
                $find('Supp1_Close').hide();

                $find('Variety_Close').hide();
                $find('LotNo_Close').hide();

            }
        } 
 </script>
    
    
      
<script type="text/javascript">
    function SaveMsgAlert(msg) {
        swal(msg);
    }
</script>
<div class="page-breadcrumb">
    <ol class="breadcrumb container">
        <h4><li class="active">Weight List</li></h4> 
    </ol>
</div>
<div id="main-wrapper" class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-9">
			    <div class="panel panel-white">
			        <div class="panel panel-primary">
				        <div class="panel-heading clearfix">
					        <h4 class="panel-title">Weight List</h4>
				        </div>
				    </div>
				    <form class="form-horizontal">
				        <div class="panel-body">
				            <div class="col-md-12">
					            <div class="row">
					                <div class="form-group col-md-4">
				                        <asp:Button ID="btnAddNew" class="btn btn-success"  runat="server" Text="Add New" OnClick="btnAddNew_Click"/>
				                    </div>
				                </div>
				            </div>
				            <div class="form-group row"></div>
                            <div class="form-group row"></div>
				            <!-- table start -->
				
					<div class="col-md-12">
					    <div class="row">
					        <asp:Repeater ID="RptWeightList" runat="server" EnableViewState="false">
			                    <HeaderTemplate>
                                    <table id="TblWeightList" class="display table">
                                        <thead>
                                            <tr>
                                               <%-- <th>S.No</th>--%>
                                                <th>WeightListNo</th>
                                                <th>WeightListDate</th>
                                                <th>LotNo</th>
                                                <th>NoOfBale</th>
                                                <th>BaleWeight</th>
                                                <th>TareWeight</th>
                                                <th>NetWeight</th>
                                                <th>Mode</th>
                                            </tr>
                                        </thead>
                                </HeaderTemplate>
                                <ItemTemplate> 
                                    <tr>
                                        <%--<td><%# Container.ItemIndex + 1 %></td>--%>
                                        <td><%# Eval("Weight_List_No")%></td>
                                         <td><%# Eval("Weight_List_Date")%></td>
                                         <td><%# Eval("LotNo")%></td>
                                         <td><%# Eval("NoOfBale")%></td>
                                         <td><%# Eval("BaleWeight")%></td>
                                         <td><%# Eval("TareWeight")%></td>
                                          <td><%# Eval("NetWeight")%></td>
                                         <td>
                                            <asp:LinkButton ID="btnEditGrid" class="btn btn-success btn-sm fa fa-pencil"  runat="server" 
                                                Text="" OnCommand="GridEditClick" CommandArgument="Edit" CommandName='<%# Eval("Weight_List_No")%>'> 
                                            </asp:LinkButton>
                                            
                                            <asp:LinkButton ID="btnDeleteEnquiry_Grid" class="btn btn-danger btn-sm fa fa-trash-o"  runat="server" 
                                               Text="" OnCommand="GridDeleteClick" CommandArgument="Delete" CommandName='<%# Eval("Weight_List_No")%>' 
                                              CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this WeightList details?');">
                                            </asp:LinkButton>
                                        </td>
                                        
                                        
                                        
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate></table></FooterTemplate>                                
			                </asp:Repeater>
					    </div>
					</div>

					<!-- table End -->
				        </div><!-- panel body end -->
				    </form>
				</div><!-- panel white end -->
			</div><!-- col-9 end -->
			
			<!-- Dashboard start -->
			<div class="col-lg-3 col-md-6">
			    <div class="panel panel-white" style="height: 100%;">
                    <div class="panel-heading">
                        <h4 class="panel-title">Dashboard Details</h4>
                        <div class="panel-control"></div>
                    </div>
                    <div class="panel-body"></div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="panel panel-white">
                    <div class="panel-body">
                        <div class="live-tile flip ha" data-mode="flip" data-speed="750" data-delay="3000">
                        </div>
                    </div>
                </div>
                <div class="panel panel-white">
                    <div class="panel-body">
                        <div class="live-tile flip ha" data-mode="flip" data-speed="750" data-delay="3000">
                        </div>
                    </div>
                </div>
            </div>
			<!-- Dashboard End -->
			<div class="col-md-2"></div>
		</div> <!-- col 12 end -->
    </div><!-- row end -->
</div><!-- main-wrapper end -->

</asp:Content>

