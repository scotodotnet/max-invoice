﻿<%@ Page Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="Waste_Sub.aspx.cs" Inherits="Transaction_Waste_Sub" Title="" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">



<script src='<%= ResolveUrl("../assets/js/master_list_jquery.min.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/master_list_jquery-ui.min.js") %>'></script>
<link href="<%= ResolveUrl("../assets/css/master_list_jquery-ui.css") %>" rel="stylesheet" type="text/css"/>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#example').dataTable();
            }
        });
    };
</script>

<script type="text/javascript">
    function SaveMsgAlert(msg) {
        swal(msg);
    }
</script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('.date-picker').datepicker({ format: 'dd/mm/yyyy' });
                $('.js-states').select2();
            }
        });
    };
</script>


<div class="page-breadcrumb">
    <ol class="breadcrumb container">
       <h4><li class="active">Waste</li></h4> 
    </ol>
</div>
                
                        
<div id="main-wrapper" class="container">
 <div class="row">
    <div class="col-md-12">
     
    
    <div class="page-inner">
    
      <div class="col-md-12">
		<div class="panel panel-white">
		  <div class="panel panel-primary">
			<div class="panel-heading clearfix">
			  <h4 class="panel-title">Waste </h4>
			</div>
		   </div>
		<form class="form-horizontal">
		  <div class="panel-body">
			<div class="col-md-12">
		      <div class="row">
				<div class="form-group col-md-3">
				   <label for="exampleInputName">Trans No<span class="mandatory">*</span></label>
				   <asp:Label ID="txtTransNo" runat="server" class="form-control" BackColor="#F3F3F3"></asp:Label>
			    </div>
				<div class="form-group col-md-3">
				  <label for="exampleInputName">Date</label>
				  <asp:TextBox ID="txtDate" MaxLength="20" class="form-control date-picker" runat="server"></asp:TextBox>
				  <asp:RequiredFieldValidator ControlToValidate="txtDate" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                  </asp:RequiredFieldValidator>
                  <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                     TargetControlID="txtDate" ValidChars="0123456789./">
                  </cc1:FilteredTextBoxExtender>
				</div>
				    <div class="form-group col-md-3">
			      <label for="exampleInputName">Shift</label>
				 <asp:DropDownList ID="ddlShift" runat="server" 
                            class="js-states form-control" >
                       </asp:DropDownList>
                       <asp:RequiredFieldValidator ControlToValidate="ddlShift" Display="Dynamic" InitialValue="-Select-" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator4" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                       </asp:RequiredFieldValidator>
			   </div>
				
			  <div class="form-group col-md-3">
				   <label for="exampleInputName">Mixing Kg</label>
				   <asp:TextBox ID="txtMixingKg" class="form-control" runat="server"></asp:TextBox>
				   <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                     TargetControlID="txtMixingKg" ValidChars="0123456789.">
                  </cc1:FilteredTextBoxExtender>
				   <asp:RequiredFieldValidator ControlToValidate="txtMixingKg" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator8" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                   </asp:RequiredFieldValidator>
			  </div>
			 
			 </div>
			</div>
			
			<div class="col-md-12">
		     <div class="row"> 
		     <div class="form-group col-md-4">
				   <label for="exampleInputName">Remarks</label>
				   <asp:TextBox ID="txtRemarks" TextMode="MultiLine" class="form-control" runat="server"></asp:TextBox>
			 </div>
		      
		     </div>
		     </div>
			
					
			<div class="clearfix"></div>				    
			<div class="timeline-options">
			  <h4 class="panel-title">Machine Production</h4>
			</div>
		     <div class="col-md-12">
				<div class="row">
				  <div class="form-group col-md-4">
					   <label for="exampleInputName">Waste Description</label>
					   <asp:DropDownList ID="ddlWasteDesc" runat="server" class="js-states form-control" >
					   <asp:ListItem Value="-Select-">-Select-</asp:ListItem>
					   <asp:ListItem Value="Blow Room">Blow Room</asp:ListItem>
					   <asp:ListItem Value="Carding">Carding</asp:ListItem>
					   <asp:ListItem Value="Soil Waste">Soil Waste</asp:ListItem>
					   <asp:ListItem Value="Drawing">Drawing</asp:ListItem>
					   <asp:ListItem Value="Open End">Open End</asp:ListItem>
					   <asp:ListItem Value="Others">Others</asp:ListItem>
					   <asp:ListItem Value="Invisible Loss">Invisible Loss</asp:ListItem>
					   <asp:ListItem Value="Step Waste">Step Waste</asp:ListItem>
                       </asp:DropDownList>
                       <asp:RequiredFieldValidator ControlToValidate="ddlWasteDesc" Display="Dynamic" InitialValue="-Select-" ValidationGroup="Item_Validate_Field" class="form_error" ID="RequiredFieldValidator5" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                       </asp:RequiredFieldValidator>
				  </div>
					        
					      <div class="form-group col-md-2">
					        <label for="exampleInputName">OPT %</label>
					        <asp:TextBox ID="txtOPTPer" class="form-control" runat="server" Text="0"></asp:TextBox>
					        <asp:RequiredFieldValidator ControlToValidate="txtOPTPer" Display="Dynamic" ValidationGroup="Item_Validate_Field" class="form_error" ID="RequiredFieldValidator7" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                            </asp:RequiredFieldValidator>
                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                 TargetControlID="txtOPTPer" ValidChars="0123456789.">
                            </cc1:FilteredTextBoxExtender>
					       </div>   
					   
			                <div class="form-group col-md-2">
					          <label for="exampleInputName">Waste Kg</label>
					          <asp:TextBox ID="txtWasteKg" class="form-control" runat="server" Text="0"></asp:TextBox>
					          <asp:RequiredFieldValidator ControlToValidate="txtWasteKg" Display="Dynamic" ValidationGroup="Item_Validate_Field" class="form_error" ID="RequiredFieldValidator12" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                              </asp:RequiredFieldValidator>
                               <cc1:FilteredTextBoxExtender ID="FilTxtPin1" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                TargetControlID="txtWasteKg" ValidChars="0123456789.">
                               </cc1:FilteredTextBoxExtender>
					         </div>
					        <div class="form-group col-md-4">
				            <label for="exampleInputName">Reason</label>
				            <asp:TextBox ID="txtReason" TextMode="MultiLine" class="form-control" runat="server"></asp:TextBox>
			                </div>
					        </div>
					        </div>
					        
			
					        
					     <div class="col-md-12">
					    <div class="row">   
					      
					        
					        <div class="form-group col-md-1">
					            <br />
					            <asp:Button ID="btnAddItem" Width="50" Height="30" class="btn-success"  
                                    runat="server" Text="Add" ValidationGroup="Item_Validate_Field" 
                                    onclick="btnAddItem_Click" />
                                     
					        </div>
					    </div>
					</div>
				
				     
                    
                    <!-- table start -->
					<div class="col-md-12">
					    <div class="row">
					        <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
			                    <HeaderTemplate>
                                    <table id="example" class="display table">
                                        <thead>
                                            <tr>
                                                <th>S.No</th>
                                                <th>Waste Description</th>
                                                <th>OPT %</th>
                                                <th>Waste Kg</th>
                                               
                                                <th>Mode</th>
                                            </tr>
                                        </thead>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td><%# Container.ItemIndex + 1 %></td>
                                        <td><%# Eval("WasteDesc")%></td>
                                        <td><%# Eval("OPT")%></td>
                                        <td><%# Eval("WasteKg")%></td>
                                      
                                        <td>
                                            <asp:LinkButton ID="btnDeleteGrid" class="btn btn-danger btn-sm fa fa-trash-o"  runat="server" 
                                                Text="" OnCommand="GridDeleteClick" CommandArgument='Delete' CommandName='<%# Eval("WasteDesc")%>' 
                                                CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this WasteDesc details?');">
                                            </asp:LinkButton>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate></table></FooterTemplate>                                
			                </asp:Repeater>
					    </div>
					</div>
					<!-- table End -->
					
					
					
					
					
					<div class="form-group row"></div>	
                    <!-- Button start -->
                    <div class="txtcenter">
                        <asp:Button ID="btnSave" class="btn btn-success"  runat="server" Text="Save" 
                            ValidationGroup="Validate_Field" onclick="btnSave_Click"/>
                        <asp:Button ID="btnCancel" class="btn btn-danger" runat="server" Text="Cancel" 
                            onclick="btnCancel_Click" />
                        <asp:Button ID="btnBackEnquiry" class="btn btn-success" runat="server" 
                            Text="Back" onclick="btnBackEnquiry_Click" />
                        <asp:Button ID="btnApprove" class="btn btn-success" runat="server" Visible="false" 
                            Text="Approve"  />
                    </div>
                    <!-- Button end -->
                  </div>      
				</form>
			
			</div><!-- panel white end -->
		    </div>
		    
		    <div class="col-md-2"></div>
		    
            
      
 </div> <!-- col-9 end -->
 </div><!-- col 12 end -->
      
  </div><!-- row end -->
 </div>


</asp:Content>

