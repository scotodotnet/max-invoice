﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="CottonInwardDetails.aspx.cs" Inherits="Transaction_CottonInwardDetails" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<script src='<%= ResolveUrl("../assets/js/master_list_jquery.min.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/master_list_jquery-ui.min.js") %>'></script>
<link href="<%= ResolveUrl("../assets/css/master_list_jquery-ui.css") %>" rel="stylesheet" type="text/css"/>
<link href="<%= ResolveUrl("../assets/css/Popup.css") %>" rel="stylesheet" type="text/css"/>  

<script src='<%= ResolveUrl("../assets/js/master_list_jquery.min.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/master_list_jquery-ui.min.js") %>'></script>
<link href="<%= ResolveUrl("../assets/css/master_list_jquery-ui.css") %>" rel="stylesheet" type="text/css"/>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function(sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    $('#TblCottonInwards').dataTable();
                }
            });
        };
   </script>
   
    <script>
        $(document).ready(function() {
            $('#TblCottonInwards').dataTable();
        });
    </script>
    
    



<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('.date-picker').datepicker({ format: 'dd/mm/yyyy' });
                $('.js-states').select2();
            }
        });
    };
</script>


   <style type="text/css">
        .CalendarCSS
        {
            background-color:White;
            color:Black;
            border:1px solid #646464;
            font-size:xx-large;
            font-weight: bold;
            margin-bottom: 4px;
            margin-top: 2px;
            
           
        }
        </style>
       <style type="text/css">
                 .ajax__calendar_container { z-index : 1000 ;background-color: White;}
         </style>


    <style>
.add-on .input-group-btn > .btn {
    border-left-width: 0;
    left:-2px;
    -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
            
            : inset 0 1px 1px rgba(0, 0, 0, 0.075);
}
/* stop the glowing blue shadow */
.add-on .form-control:focus {
    -webkit-box-shadow: none; 
            box-shadow: none;
    border-color:#cccccc; 
}
</style>

<script src='<%= ResolveUrl("../assets/js/master_list_jquery.min.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/master_list_jquery-ui.min.js") %>'></script>
<link href="<%= ResolveUrl("../assets/css/master_list_jquery-ui.css") %>" rel="stylesheet" type="text/css"/>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function(sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    $('#TblSupplier').dataTable();
                }
            });
        };
   </script>
   
    <script>
        $(document).ready(function() {
            $('#TblSupplier').dataTable();
        });
    </script>
    
<script src='<%= ResolveUrl("../assets/js/master_list_jquery.min.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/master_list_jquery-ui.min.js") %>'></script>
<link href="<%= ResolveUrl("../assets/css/master_list_jquery-ui.css") %>" rel="stylesheet" type="text/css"/>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function(sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    $('#Transport').dataTable();
                }
            });
        };
   </script>
   
    <script>
        $(document).ready(function() {
            $('#Transport').dataTable();
        });
    </script>

 
<script src='<%= ResolveUrl("../assets/js/master_list_jquery.min.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/master_list_jquery-ui.min.js") %>'></script>
<link href="<%= ResolveUrl("../assets/css/master_list_jquery-ui.css") %>" rel="stylesheet" type="text/css"/>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function(sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    $('#VarietyTable').dataTable();
                }
            });
        };
   </script>
   
   
    <script>
        $(document).ready(function() {
            $('#VarietyTable').dataTable();
        });
    </script> 
    
    
 <script src='<%= ResolveUrl("../assets/js/master_list_jquery.min.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/master_list_jquery-ui.min.js") %>'></script>
<link href="<%= ResolveUrl("../assets/css/master_list_jquery-ui.css") %>" rel="stylesheet" type="text/css"/>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function(sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    $('#TblStation').dataTable();
                }
            });
        };
   </script>
   
   
    <script>
        $(document).ready(function() {
            $('#TblStation').dataTable();
        });
    </script>   
     
    
    <link href="<%= ResolveUrl("../assets/css/Popup.css") %>" rel="stylesheet" type="text/css"/>
    <script>
        function pageLoad(sender, args) {
            if (!args.get_isPartialLoad()) {
                //  add our handler to the document's
                //  keydown event
                $addHandler(document, "keydown", onKeyDown);
            }
        }

        function onKeyDown(e) {
            if (e && e.keyCode == Sys.UI.Key.esc) {
                // if the key pressed is the escape key, dismiss the dialog
                $find('Supp1_Close').hide();
                $find('Supp2_Close').hide();
                $find('Variety_Close').hide();
                $find('Station_Close').hide();
                $find('ViewTable_Close').hide();
            }
        } 
 </script>
    

    <script type="text/javascript">
        function SaveMsgAlert(msg) {
            swal(msg);
        }
</script>





<div class="page-breadcrumb">
    <ol class="breadcrumb container">
        <h4><li class="active">Cotton Inwards Details</li></h4> 
    </ol>
</div>
<div id="main-wrapper" class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-9">
			    <div class="panel panel-white">
			        <div class="panel panel-primary">
				        <div class="panel-heading clearfix">
					        <h4 class="panel-title">Cotton Inwards Details</h4>
				        </div>
				    </div>
				    <form class="form-horizontal">
				        <div class="panel-body">
				          <div class="col-md-12">
				             <div class="row">
				               
				             </div>
				          </div>
				            <div class="col-md-12">
					            <div class="row">
					             <div class="form-group col-md-4">
					                    <label for="exampleInputName">Ctn.Inwards.No<span class="mandatory">*</span></label>
					                    <asp:Label ID="txtCottonInwardsNo" runat="server" class="form-control" BackColor="#F3F3F3"></asp:Label>
					                </div>
					         
					               
					                <div class="form-group col-md-4">
					                    <label for="exampleInputName">Ctn.Inwards.Date<span class="mandatory">*</span></label>
					                    <div class="input-group m-b-sm" style="width: 230px;padding-left: 0px; float:left"> <span class="input-group-addon" id="Span1" style="float: left; padding: 9px; width: auto;"><i class="fa fa-calendar"></i></span>
                                            <asp:TextBox ID="txtCottonInwardsDate" MaxLength="30" class="form-control date-picker" runat="server" style="float: left;width: 86%;"></asp:TextBox>
                                            <asp:RequiredFieldValidator Display="Dynamic" ControlToValidate="txtCottonInwardsDate" ValidationGroup="Validate_Field" class="form_error" ID="validate1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                            </asp:RequiredFieldValidator>
            					        </div>
                                    </div>
                                    
                                      <div class="form-group col-md-4">
					                    <label for="exampleInputName">Color<span class="mandatory">*</span></label>
					                    <asp:DropDownList ID="ddlColor" runat="server" class="form-control">
                                            </asp:DropDownList>
					        </div>
                                 
					            </div>
					        </div>
				            <div class="col-md-12">
					            <div class="row">
					               <div class="form-group col-md-4">
                                          <label for="exampleInputName">Lot No<span class="mandatory">*</span></label>
                                        <asp:TextBox ID="txtLotNo" MaxLength="30" class="form-control" runat="server" ></asp:TextBox>
                                         
                                        <asp:RequiredFieldValidator Display="Dynamic" ControlToValidate="txtLotNo" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator2" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                        </asp:RequiredFieldValidator>
                                      <%--  <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Numbers,Custom"
                                            ValidChars=".," TargetControlID="txtLotNo" />--%>
                                        
                                        
                                    </div>
					                  <div class="form-group col-md-4">
					                    <label for="exampleInputName">Type</label>
					                    <asp:RadioButtonList ID="RdpActiveMode" class="form-control" runat="server" 
                                            RepeatColumns="2" AutoPostBack="true" onselectedindexchanged="RdpActiveMode_SelectedIndexChanged" >
                                            <asp:ListItem Value="1" Text="Spot" style="padding-right:40px" Selected="True"></asp:ListItem>
                                            <asp:ListItem Value="2" Text="FOR" ></asp:ListItem>
                                        </asp:RadioButtonList>	
					                </div>
					                <div class="form-group col-md-4">
					                    <!--<label for="exampleInputName">PurchaseOrderNo<span class="mandatory">*</span></label>  -->
                                        <asp:DropDownList ID="ddlPurchaseOrderNo" runat="server" class="js-states form-control" AutopostBack="true"
                                            onselectedindexchanged="ddlPurchaseOrderNo_SelectedIndexChanged" Enabled = "false" Visible="false">
                                        </asp:DropDownList>
                                        
                                         <label for="exampleInputName">Agent Name<span class="mandatory">*</span></label>
                                        <asp:DropDownList ID="ddlAgentName" runat="server" class="js-states form-control">
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator InitialValue="-Select-" Display="Dynamic" ControlToValidate="ddlAgentName" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator19" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                        </asp:RequiredFieldValidator>
                                    </div>
                                   
					            </div>
					        </div>
				            <div class="col-md-12">
					            <div class="row">
					                 <div class="form-group col-md-4">
                                        <%--<asp:Label ID="La" runat="server" Text="Label"></asp:Label>--%>
                                         <label for="exampleInputName">Lot Date<span class="mandatory">*</span></label>
					                    <div class="input-group m-b-sm" style="width: 230px;padding-left: 0px; float:left"> <span class="input-group-addon" id="Span2" style="float: left; padding: 9px; width: auto;"><i class="fa fa-calendar"></i></span>
                                            <asp:TextBox ID="txtLotDate" MaxLength="30" class="form-control date-picker" runat="server" style="float: left;width: 86%;"></asp:TextBox>
                                            <asp:RequiredFieldValidator Display="Dynamic" ControlToValidate="txtLotDate" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator10" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                            </asp:RequiredFieldValidator>
					                    </div>
					                </div>
					               
				                    <div class="form-group col-md-4">
					                    <label for="exampleInputName">Supplier Name</label><span class="mandatory">*</span></label>
                                        <asp:DropDownList ID="txtSupplierName" runat="server" 
                                            class="js-states form-control" AutoPostBack="True" 
                                            onselectedindexchanged="txtSupplierName_SelectedIndexChanged">
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator InitialValue="-Select-" ControlToValidate="txtSupplierName" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator20" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                        </asp:RequiredFieldValidator>
                                    </div>
                                      <div class="form-group col-md-4">
					                    <label for="exampleInputName">Invoice No<span class="mandatory">*</span></label>
                                        <asp:TextBox ID="txtInvoiceNo" MaxLength="30" class="form-control" runat="server"></asp:TextBox>
                                        <asp:RequiredFieldValidator Display="Dynamic" ControlToValidate="txtInvoiceNo" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator3" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                        </asp:RequiredFieldValidator>
                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" FilterType="Numbers, LowercaseLetters, UppercaseLetters, Custom"
                                            ValidChars=" " TargetControlID="txtInvoiceNo" />
                                    </div>
                                    
					            </div>
				            </div>
					        <div class="col-md-12">
					            <div class="row">
					    		  	    
					                <div class="form-group col-md-4">
					                    <label for="exampleInputName">Invoice Date<span class="mandatory">*</span></label>
                                        <div class="input-group m-b-sm" style="width: 230px;padding-left: 0px; float:left"> <span class="input-group-addon" id="Span3" style="float: left; padding: 9px; width: auto;"><i class="fa fa-calendar"></i></span>
                                            <asp:TextBox ID="txtInvoiceDate" MaxLength="30" class="form-control date-picker" runat="server" style="float: left;width: 86%;"></asp:TextBox>
                                            <asp:RequiredFieldValidator Display="Dynamic" ControlToValidate="txtInvoiceDate" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator4" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                            </asp:RequiredFieldValidator>
					                    </div>
					                </div>
					                <div class="form-group col-md-4">
					                    <label for="exampleInputName">Transport Name<span class="mandatory">*</span></label>
                                        <asp:DropDownList ID="txtTransportName" runat="server" class="js-states form-control">
                                        </asp:DropDownList>
					                    <asp:RequiredFieldValidator InitialValue="-Select-" Display="Dynamic" ControlToValidate="txtTransportName" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator22" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                        </asp:RequiredFieldValidator>
                                    </div>
                                      <div class="form-group col-md-4">
					                    <label for="exampleInputName">Station Name</label>
					                    <asp:DropDownList ID="txtstationName" runat="server" class="js-states form-control">
                                        </asp:DropDownList>
					                    <asp:RequiredFieldValidator InitialValue="-Select-" Display="Dynamic" ControlToValidate="txtstationName" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator21" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                        </asp:RequiredFieldValidator>
                                        <%-- <select name="txtPreparedBy" id="txtstationName" class="form-control" runat="server"></select>--%>
			                        </div>
			                        
					            </div>
					        </div>
					        <div class="col-md-12">
					            <div class="row">  
					              
					                      <div class="form-group col-md-4">
					                    <label for="exampleInputName">Lorry No<span class="mandatory">*</span></label>
                                        <asp:TextBox ID="txtLorryNo" MaxLength="30" class="form-control" runat="server"></asp:TextBox>
                                        <asp:RequiredFieldValidator Display="Dynamic" ControlToValidate="txtLorryNo" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator5" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                        </asp:RequiredFieldValidator>
                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server" FilterType="Numbers, LowercaseLetters, UppercaseLetters, Custom"
                                            ValidChars=" " TargetControlID="txtLorryNo" />
					                </div>
					        
					                <div class="form-group col-md-4">
					                    <label for="exampleInputName">Lorry Freight<span class="mandatory">*</span></label>
                                        <asp:TextBox ID="txtLorryFright" MaxLength="30" class="form-control" runat="server"></asp:TextBox>
                                        <asp:RequiredFieldValidator Display="Dynamic" ControlToValidate="txtLorryFright" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator6" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                        </asp:RequiredFieldValidator>
                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterType="Numbers,Custom"
                                            ValidChars=".," TargetControlID="txtLorryFright" />
					                </div>
					                 <div class="form-group col-md-4">
					                    <label for="exampleInputName">Party Lot.No</label>
                                        <asp:TextBox ID="txtPartyLotNo" MaxLength="30" class="form-control" runat="server" Text="0"></asp:TextBox>
                                        
                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender9" runat="server" FilterType="Numbers,Custom"
                                            ValidChars=".," TargetControlID="txtPartyLotNo" />
                                    </div>
					              <%--    <div class="form-group col-md-4">
					                    <label for="exampleInputName">Party P.R.No1</label>
                                        <asp:TextBox ID="txtpartyPRno1" MaxLength="30" class="form-control" runat="server" Text="0"></asp:TextBox>
                                        
                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterType="Numbers,Custom"
                                            ValidChars=".," TargetControlID="txtpartyPRno1" />
					                </div>
					                  <div class="form-group col-md-4">
					                    <label for="exampleInputName">Party P.R.No2</label>
                                        <asp:TextBox ID="txtpartyPRno2" MaxLength="30" class="form-control" runat="server" Text="0"></asp:TextBox>
                                        
                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Numbers,Custom"
                                            ValidChars=".," TargetControlID="txtpartyPRno2" />
					                </div>--%>
					            </div>
			                </div>
			                <div class="col-md-12">
					            <div class="row">
			                      
					              
			         
			                    <%--    <div class="form-group col-md-4">
					                    <label for="exampleInputName">Party P.R.No3</label>
                                        <asp:TextBox ID="txtpartyPRno3" MaxLength="30" class="form-control" runat="server" Text="0"></asp:TextBox>
                                       
                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" FilterType="Numbers,Custom"
                                            ValidChars=".," TargetControlID="txtpartyPRno3" />
					                </div>
					                 <div class="form-group col-md-4">
					                    <label for="exampleInputName">Party P.R.No4</label>
                                        <asp:TextBox ID="txtpartyPRno4" MaxLength="30" class="form-control" runat="server" Text="0"></asp:TextBox>
                                        
                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" FilterType="Numbers,Custom"
                                            ValidChars=".," TargetControlID="txtpartyPRno4" />
					                </div>--%>
					                
			                    </div>
			                </div>
			                <div class="col-md-12">
					            <div class="row">
					               
					               
					    
					                <div class="form-group col-md-4">
					                    <label for="exampleInputName">Press Mark No</label>
                                        <asp:TextBox ID="txtPressMarkNo" MaxLength="30" class="form-control" runat="server" Text="0"></asp:TextBox>
                                        
                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender10" runat="server" FilterType="Numbers,Custom"
                                            ValidChars=".," TargetControlID="txtPressMarkNo" />
					                </div>
					                <div class="form-group col-md-4">
					                    <label for="exampleInputName">Variety Code</label>
                                        <asp:TextBox ID="txtVarietyCode" class="form-control" runat="server" style="float: left;width: 80%;" Enabled="false"></asp:TextBox> 
                                        <asp:RequiredFieldValidator Display="Dynamic" ControlToValidate="txtVarietyCode" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator23" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                        </asp:RequiredFieldValidator>
                                        <asp:Button ID="btnVarietyCode" runat="server" Text="+" class="fa fa-plus" 
                                            style="float: left; padding: 9px;" onclick="btnVarietyCode_Click"/>
                                        <cc1:ModalPopupExtender ID="modalPop_VarietyCode"  runat="server" PopupControlID="Panel5" TargetControlID="btnVarietyCode"
                                            CancelControlID="BtnVarietyClose" BackgroundCssClass="modalBackground" BehaviorID="Variety_Close">
                                        </cc1:ModalPopupExtender>
                                        <asp:Panel ID="Panel5" runat="server" CssClass="modalPopup" style="display:none" >
                                            <div class="header">
                                                Variety Details
                                            </div>
                                            <div class="body">
				                                <div class="col-md-12 headsize">
    					                            <asp:Repeater ID="Repeater_VarietyCode" runat="server" EnableViewState="false">
					                                    <HeaderTemplate>
                                                            <table id="VarietyTable" class="display table">
                                                            <thead>
                                                                <tr>
                                                                    <th>VarietyCode</th>
                                                                    <th>VarietyName</th>
                                                                    <th>Rate Per Candy</th>
                                                                    <th>View</th>
                                                                </tr>
                                                            </thead>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <tr>
                                                                <td><%# Eval("VarietyID")%></td>
                                                                <td><%# Eval("VarietyName")%></td>
                                                                <td><%# Eval("RatePerCandy")%></td>      
                                                                <td>
                                                                    <%-- <asp:LinkButton ID="btnEdit_VarietyCode" class="btn btn-success btn-sm fa fa-pencil"  runat="server" 
                                                                    Text="" OnCommand="GridViewClick_VarietyCode" CommandArgument='<%# Eval("VarietyID")%>' CommandName='<%# Eval("VarietyName")%>'>
                                                                    </asp:LinkButton>--%>
                                                                    <asp:LinkButton ID="btnEdit_VarietyCode" class="btn btn-success btn-sm fa fa-pencil"  runat="server" 
                                                                        Text="" OnCommand="GridViewClick_VarietyCode" CommandArgument='<%# Eval("VarietyID")+","+ Eval("RatePerCandy")%>' CommandName='<%# Eval("VarietyName")%>'>
                                                                    </asp:LinkButton>
                                                                </td>
                                                            </tr>
                                                        </ItemTemplate>
                                                        <FooterTemplate></table></FooterTemplate>                                
					                                </asp:Repeater>
					                            </div>
					                        </div>
					                        <div class="footer" align="right">
					                            <asp:Button ID="BtnVarietyClose" class="btn btn-rounded" runat="server" Text="Close" />
					                        </div>
					                    </asp:Panel>
					                </div>
					                 <div class="form-group col-md-4">
					                    <label for="exampleInputName">Variety Name</label>
                                        <asp:Label ID="txtVarietyName" class="form-control" runat="server" BackColor="#F3F3F3"></asp:Label>
                                        <%--<asp:RequiredFieldValidator Display="Dynamic" ControlToValidate="txtVarietyName" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                        </asp:RequiredFieldValidator>--%>
                                    </div>
					            </div>
					        </div>
	   		    	        <div class="col-md-12">
					            <div class="row">
					                
					               
					                <div class="form-group col-md-3">
					                    <label for="exampleInputName">CottonStable(MM)<span class="mandatory">*</span></label>
                                        <asp:TextBox ID="txtCottonStable" class="form-control" runat="server" Text="1.0" ></asp:TextBox>
                                        <asp:RequiredFieldValidator Display="Dynamic" ControlToValidate="txtCottonStable" InitialValue="0" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator8" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                        </asp:RequiredFieldValidator>
                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender16" runat="server" FilterType="Numbers,Custom"
                                        ValidChars="." TargetControlID="txtCottonStable" />
					                </div>
					                <div class="form-group col-md-3">
					                    <label for="exampleInputName">Cotton Mic<span class="mandatory">*</span></label>
                                        <asp:TextBox ID="txtCotton_Mic" class="form-control" runat="server" Text="1.0" ></asp:TextBox>
                                          <asp:RequiredFieldValidator Display="Dynamic" ControlToValidate="txtCotton_Mic" InitialValue="0" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator9" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                        </asp:RequiredFieldValidator>
                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender17" runat="server" FilterType="Numbers,Custom"
                                        ValidChars="." TargetControlID="txtCotton_Mic" />
					                </div>
					                <div class="form-group col-md-2">
					                    <label for="exampleInputName">Lengh<span class="mandatory">*</span></label>
					                    <asp:TextBox ID="txtLengh" class="form-control" runat="server" Text="1.0" ></asp:TextBox>
					                    <asp:RequiredFieldValidator Display="Dynamic" ControlToValidate="txtLengh"  ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                        </asp:RequiredFieldValidator>
                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender18" runat="server" FilterType="Numbers,Custom"
                                        ValidChars="." TargetControlID="txtLengh" />
					                </div>
					                <div class="form-group col-md-2">
					                    <label for="exampleInputName">Trash<span class="mandatory">*</span></label>
					                    <asp:TextBox ID="txtMaster" class="form-control" runat="server" Text="1.0" ></asp:TextBox>
					                    <asp:RequiredFieldValidator Display="Dynamic" ControlToValidate="txtLengh"  ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator7" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                        </asp:RequiredFieldValidator>
                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender19" runat="server" FilterType="Numbers,Custom"
                                        ValidChars="." TargetControlID="txtMaster" />
					                </div>
					                <div class="form-group col-md-2">
					                    <label for="exampleInputName">Moisture</label>
					                    <asp:TextBox ID="txtOthers" class="form-control" runat="server" Text="1.0" ></asp:TextBox>
					                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender20" runat="server" FilterType="Numbers,Custom"
                                        ValidChars="." TargetControlID="txtOthers" />
					                
					                </div>
					                
					            </div>
					        </div>
					        <div class="col-md-12">
					    <div class="row">
					      <div class="form-group col-md-4">
                              
                                <label for="exampleInputName">GoDown ID<span class="mandatory">*</span></label>
                                <asp:DropDownList ID="ddlGodownID" runat="server" class="js-states form-control">
                                </asp:DropDownList>
                                 
					        </div> 
					              <div class="form-group col-md-4">
					            <label for="exampleInputName">Count Code<span class="mandatory">*</span></label>
					             <asp:DropDownList ID="txtCountCode" runat="server" class="form-control" 
                                    onselectedindexchanged="txtCountCode_SelectedIndexChanged" AutoPostBack="True">
                                 </asp:DropDownList>
					         
					        </div>
					        
				         	
					       <div class="form-group col-md-4">
					            <label for="exampleInputName">CountName<span class="mandatory">*</span></label>
                                <asp:Label ID="txtCountName" MaxLength="30" class="form-control" runat="server" BackColor="#F3F3F3"></asp:Label>
                                <%--<asp:RequiredFieldValidator ControlToValidate="txtCountName" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator10" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>--%>
					        </div>
					    </div>
					 </div>
			    	        <div class="col-md-12">
					            <div class="row">
					                <div class="form-group col-md-2">
					                    <label for="exampleInputName">No Of Bale</label>
                                        <asp:TextBox ID="txtBaleNo" class="form-control" runat="server" Text="0" ></asp:TextBox>
                                        <asp:RequiredFieldValidator Display="Dynamic" ControlToValidate="txtBaleNo" InitialValue="0" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator14" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                        </asp:RequiredFieldValidator>
                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender11" runat="server" FilterType="Numbers,Custom"
                                            ValidChars="." TargetControlID="txtBaleNo" />
                                    </div>
					                <div class="form-group col-md-2">
					                    <label for="exampleInputName">Basic Rate</label>
                                        <asp:TextBox ID="txtRatePerCandy" class="form-control" runat="server" BackColor="#F3F3F3" Text="0"></asp:TextBox>
                                        <asp:RequiredFieldValidator Display="Dynamic" ControlToValidate="txtRatePerCandy" InitialValue="0" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator15" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                        </asp:RequiredFieldValidator>
                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender12" runat="server" FilterType="Numbers,Custom"
                                            ValidChars="." TargetControlID="txtRatePerCandy" />
					                </div>
					                <div class="form-group col-md-2">
					                    <label for="exampleInputName">Net Rate</label>
                                        <asp:TextBox ID="txtCandyweight" class="form-control" runat="server" Text="0"></asp:TextBox>
                                        <asp:RequiredFieldValidator Display="Dynamic" ControlToValidate="txtCandyweight" InitialValue="0" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator16" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                        </asp:RequiredFieldValidator>
                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender13" runat="server" FilterType="Numbers,Custom"
                                            ValidChars="." TargetControlID="txtCandyweight" />
					                </div>
    					        
					                <div class="form-group col-md-2">
					                    <label for="exampleInputName">Total Weight</label>
                                        <asp:TextBox ID="txtBaleWeight" class="form-control" runat="server" Text="0" AutoPostBack="true"
                                            ontextchanged="txtBaleWeight_TextChanged"></asp:TextBox>
                                        <asp:RequiredFieldValidator Display="Dynamic" ControlToValidate="txtBaleWeight" InitialValue="0" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator17" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                        </asp:RequiredFieldValidator>
                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender14" runat="server" FilterType="Numbers,Custom"
                                            ValidChars="." TargetControlID="txtBaleWeight" />
					                </div>
					                <div class="form-group col-md-4">
					                    <label for="exampleInputName">Net Amount</label>
                                        <asp:TextBox ID="txtNetAmount" class="form-control" runat="server" Text="0"></asp:TextBox>
                                        <asp:RequiredFieldValidator Display="Dynamic" ControlToValidate="txtNetAmount" InitialValue="0" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator18" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                        </asp:RequiredFieldValidator>
                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender15" runat="server" FilterType="Numbers,Custom"
                                            ValidChars="." TargetControlID="txtNetAmount" />
					                </div>
					            </div>
					        </div>
					        
					        <div class="col-md-12">
					      <div class="row">
					      
					        
					      
					   
					        
					         <div class="form-group col-md-3">
					            <label for="exampleInputName">Tare Weight</label>
                                <asp:TextBox ID="txtTareWeight" class="form-control" runat="server" AutoPostBack="true"
                                     Font-Bold="True" Text="0" ontextchanged="txtTareWeight_TextChanged"></asp:TextBox>
                                     
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterType="Numbers,Custom" 
					             TargetControlID="txtTareWeight" ValidChars="."/>   
					        </div>
					        
					         <div class="form-group col-md-3">
					            <label for="exampleInputName">Net Weight</label>
                                <asp:TextBox ID="txtNetWeight" class="form-control" runat="server" Enabled ="false" Text="0"></asp:TextBox>
					        
					        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" FilterType="Numbers,Custom" 
					             TargetControlID="txtNetWeight" ValidChars="."/>   
					        
					        </div>
					          <div class="form-group col-md-3">
					            <label for="exampleInputName">Weigh bridge </label>
                                <asp:TextBox ID="txtBridge" class="form-control" runat="server" 
                                     Font-Bold="True" Text="0"></asp:TextBox>
                                     
                               <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" FilterType="Numbers,Custom" 
					             TargetControlID="txtBridge" ValidChars="."/>      
					        </div>
					         <div class="form-group col-md-3">
					            <label for="exampleInputName">Inform Weight </label>
                                <asp:TextBox ID="txtInformWeight" class="form-control" runat="server" 
                                     Font-Bold="True" Text="0"></asp:TextBox>
                                     
                               <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender21" runat="server" FilterType="Numbers,Custom" 
					             TargetControlID="txtInformWeight" ValidChars="."/>      
					        </div>
					      
				      
					      </div>
					  </div>
					  
					  <div class="col-md-12">
					  <div class="row">
					   
					  </div>
					  </div>
				  
				            <!-- Button start -->   
		                    <div class="txtcenter">
                                <asp:Button ID="btnSave" class="btn btn-success"  runat="server" Text="Save" 
                                    ValidationGroup="Validate_Field" onclick="btnSave_Click" />
                                <%--  <asp:Button ID="Button1" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg" runat="server" Text="View" />--%>
                                <asp:Button ID="btnCancel" class="btn btn-danger" runat="server" Text="Cancel" 
                                    onclick="btnCancel_Click" />
                                <asp:Button ID="btnBackEnquiry" class="btn btn-success" runat="server" 
                                    Text="Back" onclick="btnBackEnquiry_Click" />
                                <asp:Button ID="btnApproval" class="btn btn-success" runat="server" 
                                    Text="Approval" onclick="btnApproval_Click" />   
                            </div>
                            <!-- Button end -->					
				        </div><!-- panel body end -->
				    </form>
			    </div><!-- panel white end -->
			</div><!-- col-9 end -->
		    <!-- Dashboard start -->
		    <div class="col-lg-3 col-md-6">
                <div class="panel panel-white" style="height: 100%;">
                    <div class="panel-heading">
                        <h4 class="panel-title">Dashboard Details</h4>
                        <div class="panel-control">
                        </div>
                    </div>
                    <div class="panel-body">
                    </div>
                </div>
            </div>  
                        
            <div class="col-lg-3 col-md-6">
                <div class="panel panel-white">
                    <div class="panel-body">
                        <div class="live-tile flip ha" data-mode="flip" data-speed="750" data-delay="3000">
                        </div>
                    </div>
                </div>
                
			</div> 
		    <!-- Dashboard End -->
		    <div class="col-md-2"></div>
		    <div class="col-md-2"></div>
        </div> <!-- col 12 end -->
    </div><!-- row end -->
</div><!-- main-wrapper end -->
 


</asp:Content>
