﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;



public partial class Transaction_Weight_List : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionEnquiryNo;
    string SessionFinYearCode;
    string SessionFinYearVal;
    static string sum;
    static decimal Totalbale;
    DataTable qry_dt = new DataTable();
    DataTable dtd1 = new DataTable();
    Boolean EventFire = false;
    string SessionWeightList_No;
    Boolean OpeningStock = false;
    string Auto_Transaction_No;
    bool BaleNoAlreadyExist = false;
    static decimal BaleWeightCheck = 0;
    bool ErrFlags = false;
    string bno;
    string lno;
    string BWeight;
    decimal Curr_BaleWeight;
    string No_Of_Bale;
    string Bale_Weight;
    decimal Inwards_Baleweight;
    decimal Adjustw;
    string Trans_No;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();
        if (!IsPostBack)
        {
            Page.Title = "CMS | Weight List";
            HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Transaction"));
            li.Attributes.Add("class", "droplink active open");
            //LoadUnit();
            Godown_Name_Load();
            Initial_Data_Referesh();
            Initial_Data_RefereshGV();
            Color();
            if (Session["Weight_List_No"] == null)
            {
                SessionWeightList_No = "";
                Date_Loading();
            }
            else
            {
                SessionWeightList_No = Session["Weight_List_No"].ToString();
                txtWeightListNo.Text = SessionWeightList_No;
                btnSearch_Click(sender, e);
            }
            Count_Name_Load();
           
        }
        Load_Data_Empty_LotNo();
        Load_OLD_data();
        Load_OLD_data1();
    }
    private void Color()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlColor.Items.Clear();
        query = "Select *from MstColor where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlColor.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["ColorName"] = "-Select-";
        dr["ColorName"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlColor.DataTextField = "ColorName";
        ddlColor.DataValueField = "ColorName";
        ddlColor.DataBind();
    }
    //public void LoadUnit()
    //{
    //    string query = "";
    //    DataTable dtdsupp = new DataTable();
    //    ddlUnit.Items.Clear();
    //    query = "Select *from MstUnit where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
    //    dtdsupp = objdata.RptEmployeeMultipleDetails(query);
    //    ddlUnit.DataSource = dtdsupp;
    //    DataRow dr = dtdsupp.NewRow();
    //    dr["UnitName"] = "-Select-";
    //    dr["UnitName"] = "-Select-";
    //    dtdsupp.Rows.InsertAt(dr, 0);
    //    ddlUnit.DataTextField = "UnitName";
    //    ddlUnit.DataValueField = "UnitName";
    //    ddlUnit.DataBind();
    //}
    private void Count_Name_Load()
    {
        string query = "";
        DataTable Main_DT = new DataTable();

        txtCountCode.Items.Clear();
        query = "Select * from MstCount where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' order by CountName Asc";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtCountCode.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();
        dr["CountID"] = "-Select-";
        dr["CountName"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtCountCode.DataTextField = "CountID";
        txtCountCode.DataValueField = "CountID";
        txtCountCode.DataBind();
    }

    private void Godown_Name_Load()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();

        ddlGodownID.Items.Clear();
        query = "Select * from MstGoDown where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' order by GoDownID Asc";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlGodownID.DataSource = dtdsupp;

        DataRow dr = dtdsupp.NewRow();
        dr["GoDownID"] = "-Select-";
        dr["GoDownName"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlGodownID.DataTextField = "GoDownName";
        ddlGodownID.DataValueField = "GoDownID";
        ddlGodownID.DataBind();
    }


    public void Date_Loading()
    {
        txtWeightListDate.Text = System.DateTime.Now.ToString("dd/MM/yyyy");
    }

    protected void btnApprove_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        string query;
        DataTable DT_Check = new DataTable();

        query = "Select * from Weight_List_Main where Weight_List_No='" + txtWeightListNo.Text + "'";
        query = query + " And Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
        DT_Check = objdata.RptEmployeeMultipleDetails(query);
        if (DT_Check.Rows.Count == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('First you have to register this Weight List No...');", true);
        }
        
        query = "Select * from Weight_List_Main where WL_Status = '1' And Weight_List_No='" + txtWeightListNo.Text + "'";
        query = query + " And Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
        DT_Check = objdata.RptEmployeeMultipleDetails(query);
        if (DT_Check.Rows.Count != 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('This Weight List No Already Approved...');", true);
        }
            DataTable dtt = new DataTable();
            dtt = (DataTable)ViewState["CountTable"];
            if (!ErrFlag)
            {
            if (txtNoOfBales.Text == dtt.Rows.Count.ToString())
            {
                query = "update Weight_List_Main set WL_Status = '1' where Weight_List_No='" + txtWeightListNo.Text + "'";
                objdata.RptEmployeeMultipleDetails(query);
            }
            else
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Invoice Bale Match with Total');", true);
            }

            DataTable dt = new DataTable();
            dt = (DataTable)ViewState["CountTable"];

            BaleNoAlreadyExist = true;

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                //Auto generate Transaction For Weight List

                if (!ErrFlag)
                {
                    TransactionNoGenerate TransNO = new TransactionNoGenerate();
                    Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup(SessionCcode, SessionLcode, "Opening Stock", SessionFinYearVal);
                    if (Auto_Transaction_No == "")
                    {
                        ErrFlag = true;
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Auto Generate Transaction No Error... Contact Server Admin.The Row Number is " + i + "');", true);
                    }
                    else
                    {

                        //txtWeightListNo.Text = Auto_Transaction_No;
                        Trans_No = Auto_Transaction_No.ToString();
                    }
                }

                string Trans_Date_Format = DateTime.Now.ToString();
                string Trans_Date = DateTime.Now.ToString("dd/MM/yyyy");

                if (ErrFlag != true)
                {
                    query = "insert into Temp_Upload_Stock(Ccode,Lcode,TransNo,TransDate,LotNo,VariteyCode,VariteyName,BaleNo,";
                    query = query + "InvoiceWeight,GoDownID,GoDownName,UserID,UserName,OP_Status) values('" + SessionCcode + "',";
                    query = query + "'" + SessionLcode + "','" + Trans_No + "','" + Trans_Date + "',";
                    query = query + "'" + txtLotNo.Text + "','" + txtVarietyCode.Text + "','" + txtVarietyName.Text + "',";
                    query = query + "'" + dt.Rows[i]["BaleNo"].ToString() + "','" + dt.Rows[i]["BaleWeight"].ToString() + "','" + ddlGodownID.SelectedValue + "','" + ddlGodownID.SelectedItem.Text + "',";
                    query = query + "'" + SessionUserID + "','" + SessionUserName + "','0')";
                    objdata.RptEmployeeMultipleDetails(query);




                    //Insert Data Stock Transaction Ledger
                    query = "insert into Stock_Transaction_Ledger(Ccode,Lcode,FinYearCode,FinYearVal,Trans_No,Trans_Date,Trans_Date_Str,Trans_Type,LotNo,VariteyCode,VariteyName,BaleNo,PO_Qty,GoDownID,GoDownName,UserID,UserName,Count_Code,Count_Name)";
                    query = query + "values('" + SessionCcode + "','" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + txtWeightListNo.Text + "','" + Convert.ToDateTime(txtWeightListDate.Text).AddDays(0).ToString("MM/dd/yyyy") + "','" + txtWeightListDate.Text + "','WEIGHT LIST','" + txtLotNo.Text + "','" + txtVarietyCode.Text + "','" + txtVarietyName.Text + "',";
                    query = query + "'" + dt.Rows[i]["BaleNo"].ToString() + "','" + dt.Rows[i]["BaleWeight"].ToString() + "','" + ddlGodownID.SelectedValue + "','" + ddlGodownID.SelectedItem.Text + "','" + SessionUserID + "','" + SessionUserName + "','" + txtCountCode.Text + "','" + txtCountName.Text + "')";
                    objdata.RptEmployeeMultipleDetails(query);



                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Weight list Approve Successfully');", true);

                }
            }

            Clear_All();
        }
        Response.Redirect("WeightList_Main.aspx");
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        string query = "";
        DataTable DT_Check = new DataTable();
        string SaveMode = "Insert";
        bool ErrFlag = false;


        if (txtTareWeight.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Give Tare Weight');", true);
        }

        if (txtNetWeight.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Give Net Weight');", true);
        }

        if (txtWeightListDate.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Give Weight List Date');", true);
        }


        //Check Inwards weight equal to weightlist bale weight
        //if (txtInvoiceWeight.Text != txtTotalWeight.Text)
        //{
        //    ErrFlag = true;
        //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Total Weight and Invoice Weight must be Tally ');", true);
        //}

        //check with Count Details Add with Grid

        //DT_Check = (DataTable)ViewState["ItemTable"];
        //if (DT_Check.Rows.Count == 0)
        //{
        //    ErrFlag = true;
        //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You have to add atleast one  Weight List Details..');", true);
        //}

        DataTable dtt = new DataTable();
        dtt = (DataTable)ViewState["CountTable"];



        if (txtNoOfBales.Text != dtt.Rows.Count.ToString())
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Of Bales and Bale Count must be Tally');", true);
        }



        //User Rights Check Start
        bool Rights_Check = false;
        if (btnSave.Text == "Update")
        {
            //Rights_Check = CommonClass_Function.ModifyRights_Check(SessionCcode, SessionLcode, SessionUserID, "4", "5", "Weight List");
            //if (Rights_Check == false)
            //{
            //    ErrFlag = true;
            //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Modify Weight List Details..');", true);
            //}
        }
        else
        {
            //Rights_Check = CommonClass_Function.AddRights_Check(SessionCcode, SessionLcode, SessionUserID, "4", "5", "Weight List");
            //if (Rights_Check == false)
            //{
            //    ErrFlag = true;

            //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Adding New Weight List..');", true);
            //}
        }
        //User Rights Check End

        //Auto generate Transaction Function Call
        if (btnSave.Text != "Update")
        {
            if (!ErrFlag)
            {
                TransactionNoGenerate TransNO = new TransactionNoGenerate();
                string Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup(SessionCcode, SessionLcode, "Weight List", SessionFinYearVal);
                if (Auto_Transaction_No == "")
                {
                    ErrFlag = true;
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Auto Generate Transaction No Error... Contact Server Admin');", true);
                }
                else
                {
                    txtWeightListNo.Text = Auto_Transaction_No;
                }
            }
        }


        decimal Inweight = Convert.ToDecimal(txtInvoiceWeight.Text);
        decimal Totweight = Convert.ToDecimal(txtTotalWeight.Text);

        if (!ErrFlag)
        {
            if (Inweight != Totweight)
            {
                if (RdbAdjustment.Visible == true)
                {

                }
                else
                {
                    RdbAdjustment.Visible = true;
                    ErrFlag = true;
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Please Fill Adjustment Entry');", true);
                }
            }
        }

        if (!ErrFlag)
        {
            if (RdbAdjustment.Visible == true)
            {
                if (RdbAdjustment.SelectedItem.Text == "Add")
                {
                    decimal Adjustw = Convert.ToDecimal(txtadjustment.Text);

                    Totweight = Totweight - Adjustw;
                }
                else if (RdbAdjustment.SelectedItem.Text == "Sub")
                {
                    decimal Adjustw = Convert.ToDecimal(txtadjustment.Text);

                    Totweight = Adjustw + Totweight;
                }
            }

            if (Inweight == Totweight)
            {
                if (!ErrFlag)
                {
                    query = "Select * from Weight_List_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Weight_List_No='" + txtWeightListNo.Text + "'";
                    DT_Check = objdata.RptEmployeeMultipleDetails(query);
                    if (DT_Check.Rows.Count != 0)
                    {
                        SaveMode = "Update";
                        query = "Delete from Weight_List_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Weight_List_No='" + txtWeightListNo.Text + "'";
                        objdata.RptEmployeeMultipleDetails(query);
                        query = "Delete from Weight_List_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Weight_List_No='" + txtWeightListNo.Text + "'";
                        objdata.RptEmployeeMultipleDetails(query);
                    }

                    DataTable dt = new DataTable();
                    dt = (DataTable)ViewState["CountTable"];

                    //Insert Main Table

                    if (RdbAdjustment.Visible == true && txtadjustment.Text != "")
                    {
                        query = "Insert Into Weight_List_Main(Ccode,Lcode,FinYearCode,FinYearVal,Weight_List_No,Weight_List_Date,";
                        query = query + "InvoiceNo,InvoiceDate,VarietyCode,VarietyName,Supp_Code,Supp_Name,GoDownID,GoDownName,";
                        query = query + "LotNo,LotDate,NoOfBale,Total_InwardsBale,Total_InwardsBaleWeight,RatePerCandy,CandyWeight,TotalWeight,BaleWeight,TareWeight,NetWeight,AdjustWeight,AdjustStatus,";
                        query = query + "UserID,UserName,WL_Status,Issue_Status,CountCode,CountName,ColorName,Weighbridge,InformWeight) Values('" + SessionCcode + "',";
                        query = query + " '" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + txtWeightListNo.Text + "','" + txtWeightListDate.Text + "',";
                        query = query + " '" + txtInvoiceNo.Text + "','" + txtInvoiceDate.Text + "','" + txtVarietyCode.Text + "','" + txtVarietyName.Text + "',";
                        query = query + " '" + txtSuppCode1.Value + "','" + txtSupplier.Text + "','" + ddlGodownID.SelectedValue + "','" + ddlGodownID.SelectedItem.Text + "',";
                        query = query + " '" + txtLotNo.Text + "','" + txtLotDate.Text + "','" + dt.Rows.Count + "','" + txtNoOfBales.Text + "','" + txtInvoiceWeight.Text + "','" + txtRatePerCandy.Text + "',";
                        query = query + " '" + txtCandyweight.Text + "','" + txtTotalWeight.Text + "',";
                        query = query + " '" + txtTotalWeight.Text + "','" + txtTareWeight.Text + "','" + txtNetWeight.Text + "','" + txtadjustment.Text + "','" + RdbAdjustment.SelectedItem.Text + "','" + SessionUserID + "','" + SessionUserName + "','0','0','" + txtCountCode.Text + "','" + txtCountName.Text + "','" + ddlColor.SelectedValue + "','" + txtBridge.Text + "','" + txtInformWeight.Text + "')";
                        objdata.RptEmployeeMultipleDetails(query);
                    }
                    else
                    {
                        query = "Insert Into Weight_List_Main(Ccode,Lcode,FinYearCode,FinYearVal,Weight_List_No,Weight_List_Date,";
                        query = query + "InvoiceNo,InvoiceDate,VarietyCode,VarietyName,Supp_Code,Supp_Name,GoDownID,GoDownName,";
                        query = query + "LotNo,LotDate,NoOfBale,Total_InwardsBale,Total_InwardsBaleWeight,RatePerCandy,CandyWeight,TotalWeight,BaleWeight,TareWeight,NetWeight,";
                        query = query + "UserID,UserName,WL_Status,Issue_Status,CountCode,CountName,ColorName,Weighbridge,InformWeight) Values('" + SessionCcode + "',";
                        query = query + " '" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + txtWeightListNo.Text + "','" + txtWeightListDate.Text + "',";
                        query = query + " '" + txtInvoiceNo.Text + "','" + txtInvoiceDate.Text + "','" + txtVarietyCode.Text + "','" + txtVarietyName.Text + "',";
                        query = query + " '" + txtSuppCode1.Value + "','" + txtSupplier.Text + "','" + ddlGodownID.SelectedValue + "','" + ddlGodownID.SelectedItem.Text + "',";
                        query = query + " '" + txtLotNo.Text + "','" + txtLotDate.Text + "','" + dt.Rows.Count + "','" + txtNoOfBales.Text + "','" + txtInvoiceWeight.Text + "','" + txtRatePerCandy.Text + "',";
                        query = query + " '" + txtCandyweight.Text + "','" + txtTotalWeight.Text + "',";
                        query = query + " '" + txtTotalWeight.Text + "','" + txtTareWeight.Text + "','" + txtNetWeight.Text + "','" + SessionUserID + "','" + SessionUserName + "','0','0','" + txtCountCode.Text + "','" + txtCountName.Text + "','" + ddlColor.SelectedValue + "','" + txtBridge.Text + "','" + txtInformWeight.Text + "')";
                        objdata.RptEmployeeMultipleDetails(query);

                    }

                    BaleNoAlreadyExist = true;

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        query = "Insert Into Weight_List_Main_Sub(Ccode,Lcode,FinYearCode,FinYearVal,Weight_List_No,Weight_List_Date,LotNo,LotDate,InvoiceNo,InvoiceDate,VarietyCode,VarietyName,Supp_Code,Supp_Name,BaleNo,BaleWeight,UserID,UserName) Values('" + SessionCcode + "',";
                        query = query + " '" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + txtWeightListNo.Text + "','" + txtWeightListDate.Text + "','" + txtLotNo.Text + "',";
                        query = query + " '" + txtLotDate.Text + "','" + txtInvoiceNo.Text + "','" + txtInvoiceDate.Text + "','" + txtVarietyCode.Text + "','" + txtVarietyName.Text + "',";
                        query = query + " '" + txtSuppCode1.Value + "','" + txtSupplier.Text + "','" + dt.Rows[i]["BaleNo"].ToString() + "','" + dt.Rows[i]["BaleWeight"].ToString() + "','" + SessionUserID + "','" + SessionUserName + "')";
                        objdata.RptEmployeeMultipleDetails(query);
                    }
                    if (SaveMode == "Insert")
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Weight List Details Saved Successfully');", true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Weight List Details Updated Successfully');", true);
                    }

                    Session["Weight_List_No"] = txtWeightListNo.Text;
                    btnSave.Text = "Update";
                    Load_OLD_data1();

                    if (txtNoOfBales.Text == dt.Rows.Count.ToString())
                    {
                    query = "update CottonInwards set CI_Status='1' where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And LotNo='" + txtLotNo.Text + "'";
                    objdata.RptEmployeeMultipleDetails(query);
                    }


                    Response.Redirect("WeightList_Main.aspx");
                    //Initial_Data_Referesh();
                    //Initial_Data_RefereshGV();
                }
            }
        }
    }

    protected void GridEditCottonInwardsClick(object sender, CommandEventArgs e)
    {
        string CottonInwards_No = e.CommandName.ToString();
        Session.Remove("Ctn_Inwards_No");
        Session["Ctn_Inwards_No"] = CottonInwards_No;
        Response.Redirect("CottonInwards.aspx");

    }



    protected void btnSearch_Click(object sender, EventArgs e)
    {
        //Search Gate_Pass_Out
        Count_Name_Load();
        string query = "";
        DataTable Main_DT = new DataTable();
        query = "Select * from Weight_List_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Weight_List_No='" + txtWeightListNo.Text + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        if (Main_DT.Rows.Count != 0)
        {
            txtWeightListNo.Text = Main_DT.Rows[0]["Weight_List_No"].ToString();
            txtWeightListDate.Text = Main_DT.Rows[0]["Weight_List_Date"].ToString();
            txtLotDate.Text = Main_DT.Rows[0]["LotDate"].ToString();
            txtLotNo.Text = Main_DT.Rows[0]["LotNo"].ToString();
            txtSuppCode1.Value = Main_DT.Rows[0]["Supp_Code"].ToString();
            txtSupplier.Text = Main_DT.Rows[0]["Supp_Name"].ToString();
            txtInvoiceNo.Text = Main_DT.Rows[0]["InvoiceNo"].ToString();
            txtInvoiceDate.Text = Main_DT.Rows[0]["InvoiceDate"].ToString();
            txtVarietyCode.Text = Main_DT.Rows[0]["VarietyCode"].ToString();
            txtVarietyName.Text = Main_DT.Rows[0]["VarietyName"].ToString();
            txtNoOfBales.Text = Main_DT.Rows[0]["Total_InwardsBale"].ToString();
            txtRatePerCandy.Text = Main_DT.Rows[0]["RatePerCandy"].ToString();
            txtCandyweight.Text = Main_DT.Rows[0]["CandyWeight"].ToString();
            txtInvoiceWeight.Text = Main_DT.Rows[0]["Total_InwardsBaleWeight"].ToString();
            txtTotalBaleweight.Text = Main_DT.Rows[0]["TotalWeight"].ToString();
            txtTareWeight.Text = Main_DT.Rows[0]["TareWeight"].ToString();
            txtNetWeight.Text = Main_DT.Rows[0]["NetWeight"].ToString();
            txtTotalWeight.Text = Main_DT.Rows[0]["TotalWeight"].ToString();
            txtadjustment.Text = Main_DT.Rows[0]["AdjustWeight"].ToString();
            ddlGodownID.SelectedValue = Main_DT.Rows[0]["GoDownID"].ToString();
            txtCountName.Text = Main_DT.Rows[0]["CountName"].ToString();
            txtCountCode.Text = Main_DT.Rows[0]["CountCode"].ToString();

            ddlColor.SelectedValue = Main_DT.Rows[0]["ColorName"].ToString().Trim();
            txtBridge.Text = Main_DT.Rows[0]["Weighbridge"].ToString();
            txtInformWeight.Text = Main_DT.Rows[0]["InformWeight"].ToString();

            if (txtadjustment.Text != "")
            {
                txtadjustment.Visible = true;
              
            }

            ddlGodownID.SelectedValue = Main_DT.Rows[0]["GoDownID"].ToString();

            string ss =  Main_DT.Rows[0]["AdjustStatus"].ToString();
            if (ss != "")
            {
                RdbAdjustment.Visible = true;
            
                if (ss.ToString() == "Add")
                {
                    RdbAdjustment.SelectedIndex = 0;
                }
                else if (ss.ToString() == "Sub")
                {
                    RdbAdjustment.SelectedIndex = 1;
                }
            }
            //DataTable dt = new DataTable();
            //query = "Select LotNo,BaleNo,BaleWeight from Weight_List_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Weight_List_No='" + txtWeightListNo.Text + "'";
            //dt = objdata.RptEmployeeMultipleDetails(query);
            //ViewState["ItemTable"] = dt;
            //RptrCottonInwards.DataSource = dt;
            //RptrCottonInwards.DataBind();


            DataTable dtt = new DataTable();
            query = "Select LotNo,BaleNo,BaleWeight,InvoiceNo,VarietyName,Supp_Name from Weight_List_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Weight_List_No='" + txtWeightListNo.Text + "'";
            dtt = objdata.RptEmployeeMultipleDetails(query);
            ViewState["CountTable"] = dtt;
            RptrCottonInwards.DataSource = dtt;
            RptrCottonInwards.DataBind();

            btnSave.Text = "Update";
            }
            else
            {
               Clear_All();
            }
      }



    protected void btnStationName_Click(object sender, EventArgs e)
    {

    }


    protected void btnCancel_Click(object sender, EventArgs e)
    {

    }


    protected void btnTransportName_Click(object sender, EventArgs e)
    {

    }

    private void Load_OLD_data()
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        RptrCottonInwards.DataSource = dt;
        RptrCottonInwards.DataBind();

        
    }


    private void Load_OLD_data1()
    {
        DataTable dtd1 = new DataTable();
        dtd1 = (DataTable)ViewState["CountTable"];
        RptWeightList.DataSource = dtd1;
        RptWeightList.DataBind();
    }







    protected void btnAddVariety_Click(object sender, EventArgs e)
    {
        string query;
        DataTable dt = new DataTable();
        ErrFlags = false;



        decimal NoOfBale = 0;
        decimal BaleWeight;

        decimal Inwards_NoofBale;


        decimal Remaining_NoOfBale;
        decimal Remaining_BaleWeight;


        //Check bale no already added

        if (EventFire == false)
        {

            if (txtBaleNo.Text == "")
            {
                ErrFlags = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Fill bale number');", true);
            }

            if (txtCrossWeight.Text == "")
            {
                ErrFlags = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Fill bale Weight');", true);
            }

            //DataTable AlreadyAdded = new DataTable();

            //query = "select sum(WLM.BaleWeight)As BaleWeight,Max(WL.NoOfBale)As NoofBale from Weight_List_Main_Sub WLM INNER JOIN Weight_List_Main WL";
            //query = query + " on WL.Weight_List_No=WLM.Weight_List_No where";
            //query = query + " WLM.Lcode='" + SessionLcode + "' And WLM.FinYearCode='" + SessionFinYearCode + "'";
            //query = query + " And WL.LotNo='" + txtLotNo.Text + "' and WL.WL_Status='0'";
            //query = query + " And WL.Lcode='" + SessionLcode + "' And WL.FinYearCode='" + SessionFinYearCode + "'";
            //query = query + " And WL.Ccode='" + SessionCcode + "' and WLM.Ccode='" + SessionCcode + "'";

            dt = (DataTable)ViewState["ItemTable"];

            
            if (dt.Rows.Count == 0)
            {
                decimal BaleWeigh = Convert.ToDecimal(txtCrossWeight.Text);
                decimal InvWeigh = Convert.ToDecimal(txtInvoiceWeight.Text);
                if(BaleWeigh > InvWeigh)
                {
                    ErrFlags = true;
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check Your Bale Weight');", true); 
                }
            }
            



            DataTable dtt = new DataTable();
            dtt = (DataTable)ViewState["CountTable"];
            for (int i = 0; i < dtt.Rows.Count; i++)
            {
                Bale_Weight = (Convert.ToDecimal(Bale_Weight) + Convert.ToDecimal(dtt.Rows[i]["BaleWeight"])).ToString();
                No_Of_Bale = Convert.ToString(i);
            }



            //AlreadyAdded = objdata.RptEmployeeMultipleDetails(query);
            //if (AlreadyAdded.Rows.Count != 0)
            //{
            //    No_Of_Bale = AlreadyAdded.Rows[0]["NoofBale"].ToString();
                //Bale_Weight = AlreadyAdded.Rows[0]["BaleWeight"].ToString();

                if (No_Of_Bale == null || No_Of_Bale == "")
                {
                    NoOfBale = 0;

                }
                else
                {
                    NoOfBale = Convert.ToDecimal(No_Of_Bale.ToString());
                }


                if (Bale_Weight == "" || Bale_Weight == null || Bale_Weight == "0")
                {
                    BaleWeight = 0;
                }
                else
                {
                    BaleWeight = Convert.ToDecimal(Bale_Weight.ToString());
                    BaleWeightCheck = Convert.ToDecimal(Bale_Weight.ToString());
                }


                Inwards_NoofBale = Convert.ToDecimal(txtNoOfBales.Text);
                Inwards_Baleweight = Convert.ToDecimal(txtInvoiceWeight.Text);
                Remaining_NoOfBale = Inwards_NoofBale - NoOfBale;
                Remaining_BaleWeight = Inwards_Baleweight - BaleWeight;

                if (txtTotalBaleweight.Text != "" && txtCrossWeight.Text != "")
                {
                    decimal Baleweightadded = Convert.ToDecimal(txtTotalBaleweight.Text);
                    Curr_BaleWeight = (Convert.ToDecimal(txtTotalBaleweight.Text) + Convert.ToDecimal(txtCrossWeight.Text));
                }

                if (ErrFlags == false)
                {

                    DataRow dr = null;
                    dt = (DataTable)ViewState["ItemTable"];
                    //double noba = Convert.ToDouble(dt.Rows.Count);


                    dr = dt.NewRow();
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                         if (dt.Rows[i]["Baleno"].ToString().ToUpper() == txtBaleNo.Text)
                        {

                            ErrFlags = true;
                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('This BaleNo Already Added..');", true);
                        }
                    }



                    dtd1 = (DataTable)ViewState["CountTable"];

                    for (int i = 0; i < dtd1.Rows.Count; i++)
                    {
                        if (dtd1.Rows[i]["baleno"].ToString().ToUpper() == txtBaleNo.Text)
                        {
                            ErrFlags = true;
                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('This BaleNo Already Added..');", true);
                        }
                    }


                    decimal current_data = dt.Rows.Count;
                    if (ErrFlags == false)
                    {
                        if (Remaining_NoOfBale == current_data)
                        {
                            ErrFlags = true;
                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert(Number of Bale Completed);", true);
                            lblalert.Text = "No Of Bale Completed....";
                            lblalert.Visible = true;

                        }
                    }

                    Check_BaleWeight();

                    if (ErrFlags == false)
                    {
                        dr["Lotno"] = txtLotNo.Text;
                        dr["BaleNo"] = txtBaleNo.Text;
                        dr["BaleWeight"] = txtCrossWeight.Text;

                        dt.Rows.Add(dr);

                        decimal bweight = Convert.ToDecimal(txtCrossWeight.Text);
                        BaleWeightCheck = BaleWeightCheck + bweight;
                        txtTotalBaleweight.Text = Convert.ToString(BaleWeightCheck);

                        ViewState["ItemTable"] = dt;
                        RptrCottonInwards.DataSource = dt;
                        RptrCottonInwards.DataBind();
                        AddSum();
                        txtBaleNo.Enabled = true;
                        txtBaleNo.Text = "";
                        txtCrossWeight.Text = "";
                        lblalert.Visible = false;

                    }
                }
                else
                {
                    ErrFlags = true;
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Completed all bales for LotNo:" + txtLotNo.Text + "No Of Bales are:" + NoOfBale + "');", true);
                }
            }
        }
    //}  
            
        

    public void Check_BaleWeight()
    {
        //decimal Inwards_Baleweight = Convert.ToDecimal(txtInvoiceWeight.Text);
        //if (txtTotalWeight.Text != "")
        //{
        //    decimal Curr_BaleWeight = Convert.ToDecimal(txtTotalWeight.Text);
        //}
        //else
        //{
        //    Curr_BaleWeight = 0;

        //}
        //if (txtCrossWeight.Text != "")
        //{
        //    decimal crossweight = Convert.ToDecimal(txtCrossWeight.Text);
        //    Curr_BaleWeight = Curr_BaleWeight + crossweight;
        //}

        if (ErrFlags == false)
        {
            if (Inwards_Baleweight < Curr_BaleWeight)
            {
                if (RdbAdjustment.Visible == true)
                {
                    if (RdbAdjustment.SelectedValue != "")
                    {
                        if (RdbAdjustment.SelectedItem.Text == "Add")
                        {
                            decimal AdjusWeight = Curr_BaleWeight - Inwards_Baleweight;
                            string Adtsutstr = Convert.ToString(AdjusWeight);
                            txtadjustment.Text = Adtsutstr.ToString();
                        }
                    }
                    else
                    {
                        ErrFlags = true;
                        lblalert.Text = "Select Adjustment Yes or No....";
                        lblalert.Visible = true;
                    }
                }
                else
                {
                    lblAdjustment.Visible = true;
                    RdbAdjustment.Visible = true;
                    lblrdbadjustment.Visible = true;
                    txtadjustment.Visible = true;

                    ErrFlags = true;
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Bale Weight Must Match with Invoice weight. Otherwise User choose Adustment');", true);
                }
            }
            else
            {
                lblAdjustment.Visible = false;
                RdbAdjustment.Visible = false;
                lblrdbadjustment.Visible = false;
                txtadjustment.Visible = false;
                txtadjustment.Text = "";
                RdbAdjustment.SelectedValue = null;
            }
        }
    }



    protected void GridDeleteClickBale(object sender, CommandEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            if (dt.Rows[i]["baleno"].ToString() == e.CommandName.ToString())
            {
                dt.Rows.RemoveAt(i);
                dt.AcceptChanges();
            }
        }
        ViewState["ItemTable"] = dt;
        //btnAddVariety_Click(sender, e);
        Load_OLD_data();
        Totalsum();
        Check_BaleWeight();
      
   }

    protected void GridDeleteClick(object sender, CommandEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["CountTable"];
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            if (dt.Rows[i]["baleno"].ToString() == e.CommandName.ToString())
            {
                dt.Rows.RemoveAt(i);
                dt.AcceptChanges();
            }
        }
        ViewState["CountTable"] = dt;
        //btnAddVariety_Click(sender, e);
        Load_OLD_data1();
        Totalsum();
        txtTareWeight_TextChanged(sender,e);
      
        txtTotalBaleweight.Text = txtTotalWeight.Text;
        Check_BaleWeight();
    }


    protected void GridEditEnquiryClick(object sender, CommandEventArgs e)
    {

    }
    protected void GridDeleteEnquiryClick(object sender, CommandEventArgs e)
    {

    }

    protected void btnBackEnquiry_Click(object sender, EventArgs e)
    {
        Response.Redirect("WeightList_Main.aspx");
    }

    private void Transaction_No_Generate(string FormName)
    {

    }

    
    private void Load_Data_Empty_Supp1()
    {

    }

      private void Initial_Data_Referesh()
    {
        DataTable dtd1 = new DataTable();
        dtd1.Columns.Add(new DataColumn("Lotno", typeof(string)));
        dtd1.Columns.Add(new DataColumn("baleno", typeof(string)));
        dtd1.Columns.Add(new DataColumn("BaleWeight", typeof(string)));
        dtd1.Columns.Add(new DataColumn("VarietyCode", typeof(string)));
        dtd1.Columns.Add(new DataColumn("VarietyName", typeof(string)));
        dtd1.Columns.Add(new DataColumn("Supp_Code", typeof(string)));
        dtd1.Columns.Add(new DataColumn("TotalInwardsWeight", typeof(string)));
        dtd1.Columns.Add(new DataColumn("Supp_Name", typeof(string)));
        dtd1.Columns.Add(new DataColumn("InvoiceNo", typeof(string)));
        dtd1.Columns.Add(new DataColumn("InvoiceDate", typeof(string)));
        dtd1.Columns.Add(new DataColumn("LotDate", typeof(string)));
        dtd1.Columns.Add(new DataColumn("Ctn_Inwards_No", typeof(string)));
        dtd1.Columns.Add(new DataColumn("InvoiceWeight", typeof(string)));
        RptWeightList.DataSource = dtd1;
        RptWeightList.DataBind();
        ViewState["CountTable"] = RptWeightList.DataSource;
    }

      private void Initial_Data_RefereshGV()
      {
          DataTable dtd1 = new DataTable();
          dtd1.Columns.Add(new DataColumn("Lotno", typeof(string)));
          dtd1.Columns.Add(new DataColumn("BaleNo", typeof(string)));
          dtd1.Columns.Add(new DataColumn("BaleWeight", typeof(string)));
          RptrCottonInwards.DataSource = dtd1;
          RptrCottonInwards.DataBind();
          ViewState["ItemTable"] = RptrCottonInwards.DataSource;
      }






      protected void BtnSubmit_Click(object sender, EventArgs e)
      {
          string query = "";
          lblalert.Text = "";
          DataTable DT_Check = new DataTable();

          string SaveMode = "";
          bool ErrFlag = false;
          DataTable dtitem = new DataTable();

          dtd1 = (DataTable)ViewState["CountTable"];
          dtitem = (DataTable)ViewState["ItemTable"];


          int balecount = dtitem.Rows.Count + dtd1.Rows.Count;

          if (txtNoOfBales.Text != balecount.ToString())
          {
              EventFire = true;
              ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Of Bales and Bale Count must be Tally');", true);
          }
          else
          {
              if (EventFire == false)
              {
                  for (int i = 0; i < dtitem.Rows.Count; i++)
                  {
                      bno = dtitem.Rows[i]["BaleNo"].ToString();
                      lno = dtitem.Rows[i]["Lotno"].ToString();
                      BWeight = dtitem.Rows[i]["BaleWeight"].ToString();

                      if (ErrFlag == false)
                      {
                          DataRow dr = null;
                          dr = dtd1.NewRow();

                          dr["Lotno"] = lno.ToString();
                          dr["baleno"] = bno.ToString();
                          dr["BaleWeight"] = BWeight.ToString();


                          dr["VarietyName"] = txtVarietyName.Text;
                          dr["Supp_Name"] = txtSupplier.Text;
                          dr["InvoiceNo"] = txtInvoiceNo.Text;


                          dtd1.Rows.Add(dr);
                      }

                  }
              }

              ViewState["CountTable"] = dtd1;
              Totalsum();
              txtTareWeight_TextChanged(sender, e);
              RptWeightList.DataSource = dtd1;
              RptWeightList.DataBind();
              Initial_Data_RefereshGV();
          }
      }

      public void Totalsum()
      {
          sum = "0";
          Totalbale = 0;
          DataTable dtt = new DataTable();
          dtt = (DataTable)ViewState["CountTable"];
          for (int i = 0; i < dtt.Rows.Count; i++)
          {
              sum = (Convert.ToDecimal(sum) + Convert.ToDecimal(dtt.Rows[i]["BaleWeight"])).ToString();
              txtTotalWeight.Text = sum;
              //txtTareWeight.Text = "";
              txtNetWeight.Text = "";
          }

      }

      public void AddSum()
      {
          sum = "0";
          Totalbale = 0;
          DataTable dt = new DataTable();
          dt = (DataTable)ViewState["ItemTable"];
          for (int i = 0; i < dt.Rows.Count; i++)
          {
              decimal dd = 0;
              sum = (Convert.ToDecimal(sum) + Convert.ToDecimal(dt.Rows[i]["BaleWeight"])).ToString();
              if (txtTotalWeight.Text != "")
              {
                 dd = Convert.ToDecimal(txtTotalWeight.Text);
              }
         
              decimal dd1 = Convert.ToDecimal(sum);
              decimal dd2 = dd + dd1;
              txtTotalBaleweight.Text = Convert.ToString(dd2.ToString());
          }
          if (dt.Rows.Count == 0)
          {
              txtTotalBaleweight.Text = "";
          }

      }



    protected void BtnLotNo_Click(object sender, EventArgs e)
    {
        Mdlpopup_LotNo.Show();
    }
    private void Load_Data_Empty_LotNo()
    {
        string LotNo;
        string Baleweight;
        string query = "";
        DataTable DT = new DataTable();
        DataTable DTT = new DataTable();
        //query = "select LotNo,SUM(InvoiceWeight)AS BaleWeight from Temp_Upload_Stock where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And (OP_Status = '0') group by LotNo";

        query = "Select LotNo,BaleWeight from CottonInwards where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And CI_Status='0'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        DataTable AutoTable = new DataTable();
        AutoTable.Columns.Add("LotNo");
        AutoTable.Columns.Add("BaleWeight");

        for (int i = 0; i < DT.Rows.Count; i++)
        {
            LotNo = DT.Rows[i]["LotNo"].ToString();
            Baleweight = DT.Rows[i]["BaleWeight"].ToString();

            query = "select LotNo from Weight_List_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And LotNo='"+ LotNo.ToString() +"'";
            DTT = objdata.RptEmployeeMultipleDetails(query);

            if (DTT.Rows.Count == 0)
            {
                AutoTable.NewRow();
                AutoTable.Rows.Add();
                AutoTable.Rows[AutoTable.Rows.Count - 1]["LotNo"] = LotNo;
                AutoTable.Rows[AutoTable.Rows.Count - 1]["BaleWeight"] = Baleweight;
            }
        }

        RepeaterLotNo.DataSource = AutoTable;
        RepeaterLotNo.DataBind();
        RepeaterLotNo.Visible = true;
    }



   
    protected void GridViewClick(object sender, CommandEventArgs e)
    {
        txtSuppCode1.Value = Convert.ToString(e.CommandArgument);
        txtSupplier.Text = Convert.ToString(e.CommandArgument);
    }

   

  


    protected void GridViewClick_LotNo(object sender, CommandEventArgs e)
    {
        txtLotNo.Text = Convert.ToString(e.CommandArgument);
        //txtLotDate.Text = Convert.ToString(e.CommandName);

        btnSearch_LotNo_Click(sender, e);
    }



    protected void btnSearch_LotNo_Click(object sender, EventArgs e)
    {
        //Search Gate_Pass_Out
        string query = "";
        DataTable Main_DT = new DataTable();
        bool ErrFlag;

        query = "Select LotDate,InvoiceNo,InvoiceDate,VarietyCode,VarietyName,Supp_Code,Supp_Name,NoOfBale,RatePerCandy,CandyWeight,BaleWeight from CottonInwards where Ccode='" + SessionCcode + "'";
        query = query + "And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
        query = query + "And LotNo='" + txtLotNo.Text + "' and CI_Status='0' and (D_Status is null or D_Status='0')";

        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        if (Main_DT.Rows.Count > 0)
        {
            txtInvoiceWeight.Text = Main_DT.Rows[0]["BaleWeight"].ToString();
        }

        DataTable dtd = new DataTable();

        query = "select sum(BaleWeight)As BaleWeight from Weight_List_Main where";
        query = query + " Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
        query = query + " And LotNo='" + txtLotNo.Text + "' and WL_Status='0'";

        dtd = objdata.RptEmployeeMultipleDetails(query);




        if (dtd.Rows.Count > 0)
        {
            string bale_weight = dtd.Rows[0]["BaleWeight"].ToString();
            if (bale_weight != "")
            {
                double Sum_Bale = Convert.ToDouble(bale_weight);
                double inv_Bale = Convert.ToDouble(txtInvoiceWeight.Text);
                if (Sum_Bale > inv_Bale)
                {
                    ErrFlag = true;
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('sorry! Bale Weight Already Added for this lot number');", true);
                }
            }

            DataTable dtdbaleno = new DataTable();

            //query = "select Max(NoOfBale) as baleno from Weight_List_Main_Sub where ";
            //query = query + " Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
            //query = query + " And LotNo='" + txtLotNo.Text + "'";

            //dtdbaleno = objdata.RptEmployeeMultipleDetails(query);
            //if (dtdbaleno.Rows.Count > 0)
            //{
            //    string balenoAuto = dtdbaleno.Rows[0]["baleno"].ToString();
            //    if (balenoAuto.ToString() != "")
            //    {
            //        decimal baleno = Convert.ToDecimal(balenoAuto.ToString());
            //        baleno = baleno + 1;
            //        string bno = Convert.ToString(baleno);
            //        txtBaleNo.Text = bno.ToString();
            //        txtBaleNo.Enabled = false;
            //    }
            //}
            if (Main_DT.Rows.Count > 0)
            {
                txtLotDate.Text = Main_DT.Rows[0]["LotDate"].ToString();
                txtSupplier.Text = Main_DT.Rows[0]["Supp_Name"].ToString();
                txtSuppCode1.Value = Main_DT.Rows[0]["Supp_Code"].ToString();
                txtInvoiceNo.Text = Main_DT.Rows[0]["InvoiceNo"].ToString();
                txtInvoiceDate.Text = Main_DT.Rows[0]["InvoiceDate"].ToString();
                txtVarietyCode.Text = Main_DT.Rows[0]["VarietyCode"].ToString();
                txtVarietyName.Text = Main_DT.Rows[0]["VarietyName"].ToString();
                txtNoOfBales.Text = Main_DT.Rows[0]["NoOfBale"].ToString();
                txtRatePerCandy.Text = Main_DT.Rows[0]["RatePerCandy"].ToString();
                txtCandyweight.Text = Main_DT.Rows[0]["CandyWeight"].ToString();
                txtInvoiceWeight.Text = Main_DT.Rows[0]["BaleWeight"].ToString();
            }
        }
    }
                
            
        
       
    
    protected void txtTareWeight_TextChanged(object sender, EventArgs e)
    {
        decimal tarweight;

        if (txtTareWeight.Text != "")
        {
            tarweight = Convert.ToDecimal(txtTareWeight.Text);
        }
        else
        {
            tarweight = 0;
        }

        decimal tweight = Convert.ToDecimal(txtTotalWeight.Text);
        txtNetWeight.Text = Convert.ToString(tweight - tarweight);

            Load_OLD_data();
            Load_OLD_data1();

        
        //EventFire = true;
        //BtnSubmit_Click(sender,e);
        //btnAddVariety_Click(sender,e);
    }





    protected void GridDeleteCottonInwardsClick(object sender, CommandEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            if (dt.Rows[i]["BaleNo"].ToString() == e.CommandName.ToString())
            {
                dt.Rows.RemoveAt(i);
                dt.AcceptChanges();
            }
        }
        ViewState["ItemTable"] = dt;
        Load_OLD_data();
        Totalsum();
        AddSum();
    }





    public void Clear_All()
    {
        BaleWeightCheck = 0;
        txtBaleNo.Text = "";
        txtCandyweight.Text = "";
        txtCrossWeight.Text = "";
        txthiddenlotdate.Value = "";
        txtInvoiceDate.Text = "";
        txtInvoiceNo.Text = "";
        txtInvoiceWeight.Text = "";
        txtLotDate.Text = "";
        txtLotNo.Text = "";
        txtNetWeight.Text = "";
        txtNoOfBales.Text = "";
        txtRatePerCandy.Text = "";
        txtSuppCode1.Value = "";
        txtSupplier.Text = "";
        txtTareWeight.Text = "";
        txtTotalBaleweight.Text = "";
        txtTotalWeight.Text = "";
        txtVarietyCode.Text = "";
        txtVarietyName.Text = "";
        txtWeightListDate.Text = "";
        txtWeightListNo.Text = "";
        txtBaleNo.Enabled = true;
        Initial_Data_Referesh();
        Initial_Data_RefereshGV();
        btnSave.Text = "Save";
        txtadjustment.Text = "";
        ddlGodownID.SelectedIndex = 0;
        RdbAdjustment.Visible = false;
    }

    protected void txtBaleNo_TextChanged(object sender, EventArgs e)
    {
        
        
    }
    protected void RdbAdjustment_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (RdbAdjustment.SelectedItem.Text == "Sub")
        {
            decimal Inweight = Convert.ToDecimal(txtInvoiceWeight.Text);
            decimal Totweight = Convert.ToDecimal(txtTotalWeight.Text);
            decimal Adjustw = Inweight - Totweight;
            txtadjustment.Text = Convert.ToString(Adjustw);
        }
    }
    protected void txtCountCode_SelectedIndexChanged(object sender, EventArgs e)
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "select CountName from MstCount";
        query = query + " where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        query = query + " and CountID='" + txtCountCode.Text + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count > 0)
        {
            txtCountName.Text = DT.Rows[0]["CountName"].ToString();
        }
    }
}
