﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;


public partial class Transaction_WeightList : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionEnquiryNo;
    string SessionFinYearCode;
    string SessionFinYearVal;
    static string sum;
    static decimal Totalbale;
    DataTable qry_dt = new DataTable();
    DataTable dtd1 = new DataTable();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();
        if (!IsPostBack)
        {
            //Initial_Data_Referesh();
            Page.Title = "CMS | Weight List";
            HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Transaction"));
            li.Attributes.Add("class", "droplink active open");


        }

        Load_Data_Empty_Supp();
        Load_Data_Empty_VarietyCode();
        Load_Data_Empty_LotNo();


    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        //string query = "";
        //DataTable DT_Check = new DataTable();
        //string SaveMode = "Insert";
        //bool ErrFlag = false;

        ////check with Count Details Add with Grid

        //if (DT_Check.Rows.Count == 0)
        //{
        //    ErrFlag = true;
        //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You have to add atleast one Count Details..');", true);
        //}

        ////User Rights Check Start
        //bool Rights_Check = false;
        //if (btnSave.Text == "Update")
        //{
        //    Rights_Check = CommonClass_Function.ModifyRights_Check(SessionCcode, SessionLcode, SessionUserID, "8", "4", "Gate Pass In");
        //    if (Rights_Check == false)
        //    {
        //        ErrFlag = true;
        //        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Modify Gate Pass In Details..');", true);
        //    }
        //}
        //else
        //{
        //    Rights_Check = CommonClass_Function.AddRights_Check(SessionCcode, SessionLcode, SessionUserID, "8", "4", "Gate Pass In");
        //    if (Rights_Check == false)
        //    {
        //        ErrFlag = true;
        //        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Adding New Gate Pass In..');", true);
        //    }
        //}
        ////User Rights Check End

        ////Auto generate Transaction Function Call
        //if (btnSave.Text != "Update")
        //{
        //    if (!ErrFlag)
        //    {
        //        TransactionNoGenerate TransNO = new TransactionNoGenerate();
        //        string Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup(SessionCcode, SessionLcode, "Gate Pass In", SessionFinYearVal);
        //        if (Auto_Transaction_No == "")
        //        {
        //            ErrFlag = true;
        //            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Auto Generate Transaction No Error... Contact Server Admin');", true);
        //        }
        //        else
        //        {
        //            txtTransNo.Text = Auto_Transaction_No;
        //        }
        //    }
        //}

        //if (!ErrFlag)
        //{


        //    query = "Select * from GatePass_IN_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And GP_IN_No='" + txtTransNo.Text + "'";
        //    DT_Check = objdata.RptEmployeeMultipleDetails(query);
        //    if (DT_Check.Rows.Count != 0)
        //    {
        //        SaveMode = "Update";
        //        query = "Delete from GatePass_IN_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And GP_IN_No='" + txtTransNo.Text + "'";
        //        objdata.RptEmployeeMultipleDetails(query);
        //        query = "Delete from GatePass_IN_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And GP_IN_No='" + txtTransNo.Text + "'";
        //        objdata.RptEmployeeMultipleDetails(query);
        //    }

        //    //Insert Main Table
        //    query = "Insert Into GatePass_IN_Main(Ccode,Lcode,FinYearCode,FinYearVal,GP_IN_No,GP_IN_Date,Sale_Type,Type_Cust_Supp,CustomerID,CustomerName,Supp_Code,Supp_Name,Note,TotalAmt,UserID,UserName) Values('" + SessionCcode + "',";
        //    query = query + " '" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + txtTransNo.Text + "','" + txtDate.Text + "','" + RdpSalesType.SelectedValue + "','" + RdpType.SelectedValue + "','" + txtCustomerID.Value + "','" + txtCustomerName.Text + "',";
        //    query = query + " '" + txtSupplierID.Value + "','" + txtSupplierName.Text + "',";
        //    query = query + " '" + txtNote.Text + "','" + txtTotAmt.Text + "','" + SessionUserID + "','" + SessionUserName + "')";
        //    objdata.RptEmployeeMultipleDetails(query);

        //    DataTable dt = new DataTable();
        //    dt = (DataTable)ViewState["CountTable"];
        //    for (int i = 0; i < dt.Rows.Count; i++)
        //    {
        //        query = "Insert Into GatePass_IN_Main_Sub(Ccode,Lcode,FinYearCode,FinYearVal,GP_IN_No,GP_IN_Date,ItemID,ItemDesc,Qty,Rate,LineTotal,UserID,UserName) Values('" + SessionCcode + "',";
        //        query = query + " '" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + txtTransNo.Text + "','" + txtDate.Text + "',";
        //        query = query + " '" + dt.Rows[i]["ItemID"].ToString() + "','" + dt.Rows[i]["ItemDesc"].ToString() + "','" + dt.Rows[i]["Qty"].ToString() + "','" + dt.Rows[i]["Rate"].ToString() + "','" + dt.Rows[i]["LineTotal"].ToString() + "','" + SessionUserID + "','" + SessionUserName + "')";
        //        objdata.RptEmployeeMultipleDetails(query);
        //    }
        //    if (SaveMode == "Insert")
        //    {
        //        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Gate Pass In Details Saved Successfully');", true);
        //    }
        //    else
        //    {
        //        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Gate Pass In Details Updated Successfully');", true);
        //    }

        //    Session["Gate_Pass_In_No"] = txtTransNo.Text;
        //    btnSave.Text = "Update";

        //}


    }

    protected void btnStationName_Click(object sender, EventArgs e)
    {

    }


    protected void btnCancel_Click(object sender, EventArgs e)
    {

    }


    protected void btnTransportName_Click(object sender, EventArgs e)
    {

    }



    protected void btnAddVariety_Click(object sender, EventArgs e)
    {

    }

    protected void GridDeleteClick(object sender, CommandEventArgs e)
    {

    }

    private void Load_OLD_data()
    {

    }




    protected void GridEditEnquiryClick(object sender, CommandEventArgs e)
    {

    }
    protected void GridDeleteEnquiryClick(object sender, CommandEventArgs e)
    {

    }

    protected void btnBackEnquiry_Click(object sender, EventArgs e)
    {
        Response.Redirect("WeightList_Main.aspx");
    }

    private void Transaction_No_Generate(string FormName)
    {

    }


    private void Load_Data_Empty_Supp1()
    {

    }


    protected void BtnSubmit_Click(object sender, EventArgs e)
    {
        string query = "";
        DataTable DT_Check = new DataTable();

        string SaveMode = "";
        bool ErrFlag = false;

        //Check User Name

        dtd1.Columns.Add(new DataColumn("Lotno", typeof(string)));
        dtd1.Columns.Add(new DataColumn("baleno", typeof(string)));
        dtd1.Columns.Add(new DataColumn("qun", typeof(string)));
        dtd1.Columns.Add(new DataColumn("VarietyCode", typeof(string)));
        dtd1.Columns.Add(new DataColumn("VarietyName", typeof(string)));
        dtd1.Columns.Add(new DataColumn("Supp_Name", typeof(string)));
        dtd1.Columns.Add(new DataColumn("InvoiceNo", typeof(string)));

        foreach (GridViewRow gvsal in GVModule.Rows)
        {
            SaveMode = "Insert";
            CheckBox ChkAdd = (CheckBox)gvsal.FindControl("ChkboxLotNo");
            if (ChkAdd.Checked == true)
            {
                Label Lotno = (Label)gvsal.FindControl("Lotno");
                Label baleno = (Label)gvsal.FindControl("Baleno");
                Label qun = (Label)gvsal.FindControl("Qty");
                DataRow dr = null;
                dr = dtd1.NewRow();
                dr["Lotno"] = Lotno.Text;
                dr["baleno"] = baleno.Text;
                dr["qun"] = qun.Text;


                query = "Select LotDate,InvoiceNo,InvoiceDate,VarietyCode,VarietyName,Supp_Code,Supp_Name from CottonInwards where Ccode='" + SessionCcode + "'";
                query = query + "And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
                query = query + "And LotNo='" + txtLotNo.Text + "' and CI_Status='0'";
                query = query + "And BaleNo='" + baleno.Text + "' and BaleWeight='" + qun.Text + "'";

                qry_dt = objdata.RptEmployeeMultipleDetails(query);

                if (qry_dt.Rows.Count > 0)
                {
                    dr["VarietyCode"] = qry_dt.Rows[0]["VarietyCode"].ToString();
                    dr["VarietyName"] = qry_dt.Rows[0]["VarietyName"].ToString();
                    dr["Supp_Name"] = qry_dt.Rows[0]["Supp_Name"].ToString();
                    dr["InvoiceNo"] = qry_dt.Rows[0]["InvoiceNo"].ToString();
                }

                dtd1.Rows.Add(dr);
            }
        }

        Totalsum();
        RptWeightList.DataSource = dtd1;
        RptWeightList.DataBind();
    }






    public void Totalsum()
    {
        sum = "0";
        Totalbale = 0;
        DataTable dt = new DataTable();

        //dt = (DataTable)ViewState["ItemTable"];
        for (int i = 0; i < dtd1.Rows.Count; i++)
        {
            sum = (Convert.ToDecimal(sum) + Convert.ToDecimal(dtd1.Rows[i]["qun"])).ToString();
            txtTotalWeight.Text = sum;
            Totalbale = Convert.ToDecimal(Totalbale) + 1;
            txtNoOfBales.Text = Convert.ToString(Totalbale);
            txtInvoiceWeight.Text = sum;
        }
        //txtNetAmt.Text = ((Convert.ToDecimal(txtTotalWeight.Text) + Convert.ToDecimal(txtTaxAmt.Text) + Convert.ToDecimal(txtOtherCharge.Text)) - (Convert.ToDecimal(txtDiscount.Text))).ToString();
    }


    protected void BtnLotNo_Click(object sender, EventArgs e)
    {
        Mdlpopup_LotNo.Show();
    }
    private void Load_Data_Empty_LotNo()
    {
        string query = "";
        DataTable DT = new DataTable();
        //query = "select LotNo,SUM(InvoiceWeight)AS BaleWeight from Temp_Upload_Stock where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And (OP_Status = '0') group by LotNo";

        query = "Select LotNo,sum(BaleWeight) As BaleWeight from CottonInwards where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And CI_Status='0' group by LotNo";
        DT = objdata.RptEmployeeMultipleDetails(query);
        RepeaterLotNo.DataSource = DT;
        RepeaterLotNo.DataBind();
        RepeaterLotNo.Visible = true;
    }



    protected void btnSupp_Click(object sender, EventArgs e)
    {
        modalPop_Supp1.Show();
    }
    private void Load_Data_Empty_Supp()
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select SuppCode,SuppName from MstSupplier where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        Repeater_Supplier1.DataSource = DT;
        Repeater_Supplier1.DataBind();
        Repeater_Supplier1.Visible = true;
    }
    protected void GridViewClick(object sender, CommandEventArgs e)
    {
        txtSuppCode1.Value = Convert.ToString(e.CommandArgument);
        txtSupplier.Text = Convert.ToString(e.CommandArgument);
    }

    protected void btnVarietyCode_Click(object sender, EventArgs e)
    {
        modalPop_VarietyCode.Show();
    }
    private void Load_Data_Empty_VarietyCode()
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select VarietyID,VarietyName from MstVariety where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        Repeater_VarietyCode.DataSource = DT;
        Repeater_VarietyCode.DataBind();
        Repeater_VarietyCode.Visible = true;
    }

    protected void GridViewClick_VarietyCode(object sender, CommandEventArgs e)
    {
        txtVarietyCode.Text = Convert.ToString(e.CommandArgument);
        txtVarietyName.Text = Convert.ToString(e.CommandName);
    }


    protected void GridViewClick_LotNo(object sender, CommandEventArgs e)
    {
        txtLotNo.Text = Convert.ToString(e.CommandArgument);
        //txtLotDate.Text = Convert.ToString(e.CommandName);

        btnSearch_LotNo_Click(sender, e);
    }

    private void Initial_Data_Referesh()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add(new DataColumn("SNo", typeof(string)));
        dt.Columns.Add(new DataColumn("BaleNo", typeof(string)));
        dt.Columns.Add(new DataColumn("CrossWeight", typeof(string)));


        //RptWeightList.DataSource = dt;
        //RptWeightList.DataBind();
        //ViewState["CountTable"] = RptWeightList.DataSource;
    }

    protected void btnSearch_LotNo_Click(object sender, EventArgs e)
    {
        //Search Gate_Pass_Out
        string query = "";
        DataTable Main_DT = new DataTable();
        query = "Select LotNo,BaleNo,BaleWeight from CottonInwards where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And LotNo='" + txtLotNo.Text + "' and CI_Status='0'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        if (Main_DT.Rows.Count != 0)
        {
            GVModule.DataSource = Main_DT;
            GVModule.DataBind();
        }
        else
        {

        }
    }
    protected void txtTareWeight_TextChanged(object sender, EventArgs e)
    {
        decimal tweight = Convert.ToDecimal(txtInvoiceWeight.Text);
        decimal tarweight = Convert.ToDecimal(txtTareWeight.Text);
        txtNetWeight.Text = Convert.ToString(tweight - tarweight);

        BtnSubmit_Click(sender, e);
    }
}
