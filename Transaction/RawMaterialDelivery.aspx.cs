﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class Transaction_RawMaterialDelivery : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionEnquiryNo;
    string SessionFinYearCode;
    string SessionFinYearVal;
    string SessionTrans_No;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "ERP Module :: Raw Material";
            HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Transaction"));
            li.Attributes.Add("class", "droplink active open");
            Initial_Data_Referesh();

            Load_Unit();
            Load_FromGoDown();
            Load_ToGoDown();
            Load_LotNo();
            Load_Variety();

            if (Session["Trans_No"] == null)
            {
                SessionTrans_No = "";
            }
            else
            {
                SessionTrans_No = Session["Trans_No"].ToString();
                txtTransNo.Text = SessionTrans_No;
                btnSearch_Click(sender, e);
            }

        }
        Load_OLD_data();

    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        //Search Gate_Pass_Out
        string query = "";
        DataTable Main_DT = new DataTable();
        query = "Select * from RawMaterialDelivery_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Trans_No='" + txtTransNo.Text + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        if (Main_DT.Rows.Count != 0)
        {
            txtTransNo.Text = Main_DT.Rows[0]["Trans_No"].ToString();
            txtDate.Text = Main_DT.Rows[0]["Trans_Date"].ToString();
            RdpIssueType.SelectedValue = Main_DT.Rows[0]["Trans_Type"].ToString();
            txtUnit.SelectedValue = Main_DT.Rows[0]["ToUnit"].ToString();
            txtFromGoDown.SelectedValue = Main_DT.Rows[0]["FromGoDownID"].ToString();
            txtToGoDown.SelectedValue = Main_DT.Rows[0]["ToGoDownID"].ToString();

            txtRemarks.Text = Main_DT.Rows[0]["Remarks"].ToString();
            RdpIssueType_SelectedIndexChanged(sender, e);

            //Unplanned_Receipt_Main_Sub Table Load
            DataTable dt = new DataTable();
            query = "Select *from RawMaterialDelivery_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Trans_No='" + txtTransNo.Text + "'";
            dt = objdata.RptEmployeeMultipleDetails(query);
            ViewState["ItemTable"] = dt;
            Repeater1.DataSource = dt;
            Repeater1.DataBind();

            Totalsum();

            btnSave.Text = "Update";
        }
        else
        {

        }
    }


    private void Load_OLD_data()
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        Repeater1.DataSource = dt;
        Repeater1.DataBind();
    }

    private void Initial_Data_Referesh()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add(new DataColumn("LotNo", typeof(string)));
        dt.Columns.Add(new DataColumn("VarietyID", typeof(string)));
        dt.Columns.Add(new DataColumn("VarietyName", typeof(string)));
        dt.Columns.Add(new DataColumn("NoOfBale", typeof(string)));
        dt.Columns.Add(new DataColumn("AvlBale", typeof(string)));
        dt.Columns.Add(new DataColumn("BaleWeight", typeof(string)));
        dt.Columns.Add(new DataColumn("AvlBaleWt", typeof(string)));
        dt.Columns.Add(new DataColumn("LineTotal", typeof(string)));
      
        Repeater1.DataSource = dt;
        Repeater1.DataBind();
        ViewState["ItemTable"] = Repeater1.DataSource;

        //dt = Repeater1.DataSource;
    }


    private void Load_Unit()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        txtUnit.Items.Clear();
        query = "Select *from MstLocation where Ccode='" + SessionCcode + "' And Lcode!='" + SessionLcode + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        txtUnit.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["Lcode"] = "-Select-";
        dr["Lcode"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        txtUnit.DataTextField = "Lcode";
        txtUnit.DataValueField = "Lcode";
        txtUnit.DataBind();
    }

    private void Load_FromGoDown()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        txtFromGoDown.Items.Clear();
        query = "Select *from MstGoDown where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        txtFromGoDown.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["GoDownName"] = "-Select-";
        dr["GoDownID"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        txtFromGoDown.DataTextField = "GoDownName";
        txtFromGoDown.DataValueField = "GoDownID";
        txtFromGoDown.DataBind();
    }

    private void Load_ToGoDown()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        txtToGoDown.Items.Clear();
        query = "Select *from MstGoDown where Ccode='" + SessionCcode + "' ";
        if(RdpIssueType.SelectedValue=="3")
        {
            query =query +"And Lcode='" + txtUnit.SelectedValue + "'";
        }
        else
        {
            query = query + "And Lcode='" + SessionLcode + "'";
        }
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        txtToGoDown.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["GoDownName"] = "-Select-";
        dr["GoDownID"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        txtToGoDown.DataTextField = "GoDownName";
        txtToGoDown.DataValueField = "GoDownID";
        txtToGoDown.DataBind();
    }

    private void Load_LotNo()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlLotNo.Items.Clear();
        query = "Select LotNo from Cotton_Stock_Ledger_All where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        query = query + " And GoDownID='" + txtFromGoDown.SelectedValue + "'";
        query = query + "  group by LotNo having ((Sum(Add_Bale)-Sum(Minus_Bale)) >0 )";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlLotNo.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["LotNo"] = "-Select-";
        dr["LotNo"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlLotNo.DataTextField = "LotNo";
        ddlLotNo.DataValueField = "LotNo";
        ddlLotNo.DataBind();
    }

    private void Load_Variety()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlVarietyName.Items.Clear();
        query = "Select Distinct ItemCode,ItemName from Cotton_Stock_Ledger_All where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        query = query + " And LotNo='" + ddlLotNo.SelectedValue + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlVarietyName.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["ItemName"] = "-Select-";
        dr["ItemCode"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlVarietyName.DataTextField = "ItemName";
        ddlVarietyName.DataValueField = "ItemCode";
        ddlVarietyName.DataBind();
    }

    protected void GridDeleteClick(object sender, CommandEventArgs e)
    {

        DataTable qry_dt = new DataTable();
        bool ErrFlag = false;
        string qry = "";


        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            if ((dt.Rows[i]["LotNo"].ToString() == e.CommandName.ToString()) && (dt.Rows[i]["VarietyID"].ToString() == e.CommandArgument.ToString()))
            {
                dt.Rows.RemoveAt(i);
                dt.AcceptChanges();
            }
        }
        ViewState["ItemTable"] = dt;
        Load_OLD_data();
        Totalsum();

       
    }

    protected void ddlLotNo_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_Variety();
    }
    protected void RdpIssueType_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (RdpIssueType.SelectedValue == "1")
        {
            txtToGoDown.Enabled = false;
            txtUnit.Enabled = false;
        }
        else if (RdpIssueType.SelectedValue == "3")
        {
            txtToGoDown.Enabled = true;
            txtUnit.Enabled = true;
        }
        else if (RdpIssueType.SelectedValue == "2")
        {
            txtToGoDown.Enabled = true;
            txtUnit.Enabled = false;
        }
        Load_ToGoDown();
    }
    protected void txtFromGoDown_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_LotNo();
    }
    protected void btnAddItem_Click(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        DataTable qry_dt = new DataTable();
        bool ErrFlag = false;
        DataRow dr = null;
        string query = "";

        if (txtNoOfBale.Text == "0.0" || txtNoOfBale.Text == "0" || txtNoOfBale.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the No Of Bale...');", true);
        }

        if (txtBaleWt.Text == "0.0" || txtBaleWt.Text == "0" || txtBaleWt.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the Bale Weight...');", true);
        }

        if (Convert.ToDecimal(txtAvlBale.Text) < Convert.ToDecimal(txtNoOfBale.Text))
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check with No Of Bale. It exceeds Stock..');", true);
        }
        if (Convert.ToDecimal(txtAvlBaleWt.Text) < Convert.ToDecimal(txtBaleWt.Text))
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check with Bale Weight. It exceeds Stock..');", true);
        }

        if (!ErrFlag)
        {
            // check view state is not null  
            if (ViewState["ItemTable"] != null)
            {
                //get datatable from view state   
                dt = (DataTable)ViewState["ItemTable"];

                //check Item Already add or not
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if ((dt.Rows[i]["LotNo"].ToString().ToUpper() == ddlLotNo.SelectedValue.ToString().ToUpper()) && (dt.Rows[i]["VarietyID"].ToString().ToUpper() == ddlVarietyName.SelectedValue.ToString().ToUpper()))
                    {
                        ErrFlag = true;
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('This Variety Already Added..');", true);
                    }
                }

                string Old_BaleValue = "";
                Old_BaleValue = ((Convert.ToDecimal(txtBaleValue.Value)) / Convert.ToDecimal(txtAvlBale.Text)).ToString();
                Old_BaleValue = (Math.Round(Convert.ToDecimal(Old_BaleValue), 0, MidpointRounding.AwayFromZero)).ToString();

                string New_BaleValue = "";
                New_BaleValue = ((Convert.ToDecimal(txtNoOfBale.Text)) * Convert.ToDecimal(Old_BaleValue)).ToString();
                New_BaleValue = (Math.Round(Convert.ToDecimal(New_BaleValue), 0, MidpointRounding.AwayFromZero)).ToString();


                if (!ErrFlag)
                {
                    dr = dt.NewRow();
                    dr["LotNo"] = ddlLotNo.SelectedValue;
                    dr["VarietyID"] = ddlVarietyName.SelectedValue;
                    dr["VarietyName"] = ddlVarietyName.SelectedItem.Text;
                    dr["NoOfBale"] = txtNoOfBale.Text;
                    dr["AvlBale"] = txtAvlBale.Text;
                    dr["BaleWeight"] = txtBaleWt.Text;
                    dr["AvlBaleWt"] = txtAvlBaleWt.Text;
                    dr["LineTotal"] = New_BaleValue;

                    dt.Rows.Add(dr);
                    ViewState["ItemTable"] = dt;
                    Repeater1.DataSource = dt;
                    Repeater1.DataBind();

                    Totalsum();


                    ddlLotNo.SelectedValue = "-Select-"; ddlVarietyName.SelectedValue = "-Select-";
                    txtNoOfBale.Text = "0"; txtAvlBale.Text = "0";
                    txtBaleWt.Text = "0"; txtAvlBaleWt.Text = "0";
                    
                }
            }
            else
            {
                string Old_BaleValue = "";
                Old_BaleValue = ((Convert.ToDecimal(txtBaleValue.Value)) / Convert.ToDecimal(txtAvlBale)).ToString();
                Old_BaleValue = (Math.Round(Convert.ToDecimal(Old_BaleValue), 0, MidpointRounding.AwayFromZero)).ToString();

                string New_BaleValue = "";
                New_BaleValue = ((Convert.ToDecimal(txtNoOfBale.Text)) * Convert.ToDecimal(Old_BaleValue)).ToString();
                New_BaleValue = (Math.Round(Convert.ToDecimal(New_BaleValue), 0, MidpointRounding.AwayFromZero)).ToString();


                dr = dt.NewRow();
                dr["LotNo"] = ddlLotNo.SelectedValue;
                dr["VarietyID"] = ddlVarietyName.SelectedValue;
                dr["VarietyName"] = ddlVarietyName.SelectedItem.Text;
                dr["NoOfBale"] = txtNoOfBale.Text;
                dr["AvlBale"] = txtAvlBale.Text;
                dr["BaleWeight"] = txtBaleWt.Text;
                dr["AvlBaleWt"] = txtAvlBaleWt.Text;
                dr["LineTotal"] = New_BaleValue;

                dt.Rows.Add(dr);
                ViewState["ItemTable"] = dt;
                Repeater1.DataSource = dt;
                Repeater1.DataBind();

                Totalsum();


                ddlLotNo.SelectedValue = "-Select-"; ddlVarietyName.SelectedValue = "-Select-";
                txtNoOfBale.Text = "0"; txtAvlBale.Text = "0";
                txtBaleWt.Text = "0"; txtAvlBaleWt.Text = "0";
            }
        }
    }

    public void Totalsum()
    {
        string NoofBale_sum = "0";
        string BaleWt_sum = "0";

        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        if (dt.Rows.Count != 0)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                NoofBale_sum = (Convert.ToDecimal(NoofBale_sum) + Convert.ToDecimal(dt.Rows[i]["NoOfBale"])).ToString();
                txtTotBale.Text = NoofBale_sum;

                BaleWt_sum = (Convert.ToDecimal(BaleWt_sum) + Convert.ToDecimal(dt.Rows[i]["BaleWeight"])).ToString();
                txtTotWt.Text = BaleWt_sum;

            }
        }
        else
        {
            txtTotBale.Text = "0";
            txtTotWt.Text = "0";
        }
      }

    protected void ddlVarietyName_SelectedIndexChanged(object sender, EventArgs e)
    {
        string query = "";
        DataTable dtdsupp = new DataTable();

        query = "Select (Sum(Add_Bale)-Sum(Minus_Bale)) as NoOfBale,(Sum(Add_Qty)-Sum(Minus_Qty)) as BaleWt,(Sum(Add_Value)-Sum(Minus_Value)) as BaleValue from Cotton_Stock_Ledger_All where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        query = query + " And GoDownID='" + txtFromGoDown.SelectedValue + "'";
        query = query + " And LotNo='" + ddlLotNo.SelectedValue + "'";
        query = query + " And ItemCode='" + ddlVarietyName.SelectedValue + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        if (dtdsupp.Rows.Count != 0)
        {
            txtAvlBale.Text = dtdsupp.Rows[0]["NoOfBale"].ToString();
            txtAvlBaleWt.Text = dtdsupp.Rows[0]["BaleWt"].ToString();
            txtBaleValue.Value = dtdsupp.Rows[0]["BaleValue"].ToString();
        }
        else
        {
            txtAvlBale.Text = "0";
            txtAvlBaleWt.Text = "0";
            txtBaleValue.Value = "0";
        }

    }

    private void Clear_All_Field()
    {
        txtTransNo.Text = ""; txtDate.Text = ""; RdpIssueType.SelectedValue = "1";
        txtUnit.SelectedValue = "-Select-";
        txtFromGoDown.SelectedValue = "-Select-";
        txtToGoDown.SelectedValue = "-Select-"; txtRemarks.Text = "";
        txtTotBale.Text = "0";
        txtTotWt.Text = "0";

        ddlLotNo.SelectedValue = "-Select-"; ddlVarietyName.SelectedValue = "-Select-";
        txtNoOfBale.Text = "0"; txtAvlBale.Text = "0";
        txtBaleWt.Text = "0"; txtAvlBaleWt.Text = "0";

      
        btnSave.Text = "Save";
        Initial_Data_Referesh();
        Session.Remove("Trans_No");

       
       txtToGoDown.Enabled = false;
       txtUnit.Enabled = false;
       
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        string query = "";
        DataTable DT_Check = new DataTable();
        DataTable qry_dt = new DataTable();
        string SaveMode = "Insert";
        bool ErrFlag = false;

        //check with Item Details Add with Grid
        DT_Check = (DataTable)ViewState["ItemTable"];
        if (DT_Check.Rows.Count == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You have to add atleast one Item Details..');", true);
        }

        if (RdpIssueType.SelectedValue == "3")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select the Unit..');", true);
        }

        if (RdpIssueType.SelectedValue!="1")
        {
            if (txtToGoDown.SelectedValue == "-Select-")
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select the To Godown..');", true);
            }
        }

        //Check Supplier Name
        //query = "Select * from MstSupplier where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And SuppCode='" + txtSupplierName.SelectedValue + "'";
        //qry_dt = objdata.RptEmployeeMultipleDetails(query);
        //if (qry_dt.Rows.Count == 0)
        //{
        //    ErrFlag = true;
        //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check with Supplier Name...');", true);
        //}


        //User Rights Check Start
        bool Rights_Check = false;
        //if (btnSave.Text == "Update")
        //{
        //    Rights_Check = CommonClass_Function.ModifyRights_Check(SessionCcode, SessionLcode, SessionUserID, "7", "2", "Unplanned Receipt");
        //    if (Rights_Check == false)
        //    {
        //        ErrFlag = true;
        //        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Modify Unplanned Receipt Details..');", true);
        //    }
        //}
        //else
        //{
        //    Rights_Check = CommonClass_Function.AddRights_Check(SessionCcode, SessionLcode, SessionUserID, "7", "2", "Unplanned Receipt");
        //    if (Rights_Check == false)
        //    {
        //        ErrFlag = true;
        //        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Adding New Unplanned Receipt..');", true);
        //    }
        //}
        //User Rights Check End


        //Auto generate Transaction Function Call
        if (btnSave.Text != "Update")
        {
            if (!ErrFlag)
            {
                TransactionNoGenerate TransNO = new TransactionNoGenerate();
                string Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup(SessionCcode, SessionLcode, "Raw Material Delivery", SessionFinYearVal);
                if (Auto_Transaction_No == "")
                {
                    ErrFlag = true;
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Auto Generate Transaction No Error... Contact Server Admin');", true);
                }
                else
                {
                    txtTransNo.Text = Auto_Transaction_No;
                }
            }
        }

        //Check With Already Approved Start
        DataTable DT_Check_Apprv = new DataTable();
        query = "Select * from RawMaterialDelivery_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Trans_No='" + txtTransNo.Text + "' And Status='1'";
        DT_Check_Apprv = objdata.RptEmployeeMultipleDetails(query);
        if (DT_Check_Apprv.Rows.Count != 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Do not Update Raw Material Delivery Details Already Approved..');", true);
        }


        if (!ErrFlag)
        {
            query = "Select * from RawMaterialDelivery_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Trans_No='" + txtTransNo.Text + "'";
            DT_Check = objdata.RptEmployeeMultipleDetails(query);
            if (DT_Check.Rows.Count != 0)
            {
                SaveMode = "Update";
                query = "Delete from RawMaterialDelivery_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Trans_No='" + txtTransNo.Text + "'";
                objdata.RptEmployeeMultipleDetails(query);
                query = "Delete from RawMaterialDelivery_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Trans_No='" + txtTransNo.Text + "'";
                objdata.RptEmployeeMultipleDetails(query);
            }

            //Response.Write(strValue);
            //Insert Main Table
            query = "Insert Into RawMaterialDelivery_Main(Ccode,Lcode,FinYearCode,FinYearVal,Trans_No,Trans_Date,Trans_Type,ToUnit,FromGoDownID,FromGoDownName,ToGoDownID,ToGoDownName,Remarks,TotNoOfBale,TotBaleWeight,UserID,UserName) Values('" + SessionCcode + "',";
            query = query + " '" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + txtTransNo.Text + "','" + txtDate.Text + "','" + RdpIssueType.SelectedValue + "','" + txtUnit.SelectedValue + "',";
            query = query + " '" + txtFromGoDown.SelectedValue + "','" + txtFromGoDown.SelectedItem.Text + "','" + txtToGoDown.SelectedValue + "',";
            query = query + " '" + txtToGoDown.SelectedItem.Text + "','" + txtRemarks.Text + "','" + txtTotBale.Text + "','" + txtTotWt.Text + "','" + SessionUserID + "','" + SessionUserName + "')";
            objdata.RptEmployeeMultipleDetails(query);

            DataTable dt = new DataTable();
            dt = (DataTable)ViewState["ItemTable"];
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                query = "Insert Into RawMaterialDelivery_Main_Sub(Ccode,Lcode,FinYearCode,FinYearVal,Trans_No,Trans_Date,LotNo,VarietyID,VarietyName,NoOfBale,AvlBale,BaleWeight,AvlBaleWt,LineTotal,";
                query = query + "UserID,UserName) Values('" + SessionCcode + "',";
                query = query + " '" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + txtTransNo.Text + "','" + txtDate.Text + "','" + dt.Rows[i]["LotNo"].ToString() + "','" + dt.Rows[i]["VarietyID"].ToString() + "',";
                query = query + " '" + dt.Rows[i]["VarietyName"].ToString() + "','" + dt.Rows[i]["NoOfBale"].ToString() + "','" + dt.Rows[i]["AvlBale"].ToString() + "','" + dt.Rows[i]["BaleWeight"].ToString() + "','" + dt.Rows[i]["AvlBaleWt"].ToString() + "',";
                query = query + " '" + dt.Rows[i]["LineTotal"].ToString() + "','" + SessionUserID + "','" + SessionUserName + "')";
                objdata.RptEmployeeMultipleDetails(query);
            }



            if (SaveMode == "Insert")
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Raw Material Delivery Details Saved Successfully');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Raw Material Delivery Details Updated Successfully');", true);
            }
            //Clear_All_Field();
            Session["Trans_No"] = txtTransNo.Text;
            btnSave.Text = "Update";
            //Load_Data_Enquiry_Grid();
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }

    protected void btnBackEnquiry_Click(object sender, EventArgs e)
    {
        Session.Remove("Trans_No");
        Response.Redirect("RawMaterialDelivery_Main.aspx");
    }

    protected void btnApprove_Click(object sender, EventArgs e)
    {
        string query = "";
        DataTable DT_Check = new DataTable();
        bool ErrFlag = false;

        //User Rights Check Start
        bool Rights_Check = false;
        //Rights_Check = CommonClass_Function.ApproveRights_Check(SessionCcode, SessionLcode, SessionUserID, "7", "2", "Unplanned Receipt");
        //if (Rights_Check == false)
        //{
        //    ErrFlag = true;
        //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Approve Unplanned Receipt...');", true);
        //}
        //User Rights Check End

        query = "Select * from RawMaterialDelivery_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Trans_No='" + txtTransNo.Text + "'";
        DT_Check = objdata.RptEmployeeMultipleDetails(query);
        if (DT_Check.Rows.Count == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('First You have to Register this Raw Material Delivery Details..');", true);
        }

        query = "Select * from RawMaterialDelivery_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Trans_No='" + txtTransNo.Text + "' And Status='1'";
        DT_Check = objdata.RptEmployeeMultipleDetails(query);
        if (DT_Check.Rows.Count != 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Raw Material Delivery Details Already Approved..');", true);
        }

        if (!ErrFlag)
        {
            query = "Update RawMaterialDelivery_Main set Status='1' where Ccode='" + SessionCcode + "'";
            query = query + "  And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
            query = query + "  And Trans_No='" + txtTransNo.Text + "'";
            objdata.RptEmployeeMultipleDetails(query);

            Stock_Add();

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Raw Material Delivery Details Approved Successfully..');", true);
        }
    }

    private void Stock_Add()
    {
        string query = "";
        DataTable dt_check = new DataTable();
        DataTable qry_dt = new DataTable();


        DateTime transDate = Convert.ToDateTime(txtDate.Text);
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];

        for (int i = 0; i < dt.Rows.Count; i++)
        {
        //Check Cotton_Stock_Ledger_All
        query = "Select * from Cotton_Stock_Ledger_All where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        query = query + " And FinYearCode='" + SessionFinYearCode + "' And Trans_No='" + txtTransNo.Text + "'";
        query = query + " And ItemCode='" + dt.Rows[i]["VarietyID"].ToString() + "'";
        dt_check = objdata.RptEmployeeMultipleDetails(query);
        if (dt_check.Rows.Count != 0)
        {
            query = "Delete from Cotton_Stock_Ledger_All where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
            query = query + " And FinYearCode='" + SessionFinYearCode + "' And Trans_No='" + txtTransNo.Text + "'";
            query = query + " And ItemCode='" + dt.Rows[i]["VarietyID"].ToString() + "'";
            objdata.RptEmployeeMultipleDetails(query);
        }
       
            if (RdpIssueType.SelectedValue == "1")
            {
                //Insert Into Cotton_Stock_Ledger_All
                query = "Insert Into Cotton_Stock_Ledger_All(Ccode,Lcode,FinYearCode,FinYearVal,Trans_No,Trans_Date,Trans_Date_Str,";
                query = query + " Trans_Type,LotNo,ItemCode,ItemName,Add_Qty,Add_Value,Add_Bale,Minus_Qty,Minus_Value,Minus_Bale,GoDownID,GoDownName,";
                query = query + " UserID,UserName) Values('" + SessionCcode + "','" + SessionLcode + "',";
                query = query + " '" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + txtTransNo.Text + "','" + transDate.ToString("yyyy/MM/dd") + "','" + txtDate.Text + "',";
                query = query + " 'RAW MATERIAL DELIVERY','" + dt.Rows[i]["LotNo"].ToString() + "','" + dt.Rows[i]["VarietyID"].ToString() + "','" + dt.Rows[i]["VarietyName"].ToString() + "','0.0',";
                query = query + " '0.0','0.0','" + dt.Rows[i]["BaleWeight"].ToString() + "','" + dt.Rows[i]["LineTotal"].ToString() + "','" + dt.Rows[i]["NoOfBale"].ToString() + "',";
                query = query + "'" + txtFromGoDown.SelectedValue + "','" + txtFromGoDown.SelectedItem.Text + "',";
                query = query + " '" + SessionUserID + "','" + SessionUserName + "')";
                objdata.RptEmployeeMultipleDetails(query);
            }
            else if (RdpIssueType.SelectedValue == "2")
            {
                //Insert Into Cotton_Stock_Ledger_All
                query = "Insert Into Cotton_Stock_Ledger_All(Ccode,Lcode,FinYearCode,FinYearVal,Trans_No,Trans_Date,Trans_Date_Str,";
                query = query + " Trans_Type,LotNo,ItemCode,ItemName,Add_Qty,Add_Value,Add_Bale,Minus_Qty,Minus_Value,Minus_Bale,GoDownID,GoDownName,";
                query = query + " UserID,UserName) Values('" + SessionCcode + "','" + SessionLcode + "',";
                query = query + " '" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + txtTransNo.Text + "','" + transDate.ToString("yyyy/MM/dd") + "','" + txtDate.Text + "',";
                query = query + " 'RAW MATERIAL DELIVERY','" + dt.Rows[i]["LotNo"].ToString() + "','" + dt.Rows[i]["VarietyID"].ToString() + "','" + dt.Rows[i]["VarietyName"].ToString() + "','0.0',";
                query = query + " '0.0','0.0','" + dt.Rows[i]["BaleWeight"].ToString() + "','" + dt.Rows[i]["LineTotal"].ToString() + "','" + dt.Rows[i]["NoOfBale"].ToString() + "',";
                query = query + "'" + txtFromGoDown.SelectedValue + "','" + txtFromGoDown.SelectedItem.Text + "',";
                query = query + " '" + SessionUserID + "','" + SessionUserName + "')";
                objdata.RptEmployeeMultipleDetails(query);

                //Insert Into Cotton_Stock_Ledger_All
                query = "Insert Into Cotton_Stock_Ledger_All(Ccode,Lcode,FinYearCode,FinYearVal,Trans_No,Trans_Date,Trans_Date_Str,";
                query = query + " Trans_Type,LotNo,ItemCode,ItemName,Add_Qty,Add_Value,Add_Bale,Minus_Qty,Minus_Value,Minus_Bale,GoDownID,GoDownName,";
                query = query + " UserID,UserName) Values('" + SessionCcode + "','" + SessionLcode + "',";
                query = query + " '" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + txtTransNo.Text + "','" + transDate.ToString("yyyy/MM/dd") + "','" + txtDate.Text + "',";
                query = query + " 'RAW MATERIAL DELIVERY','" + dt.Rows[i]["LotNo"].ToString() + "','" + dt.Rows[i]["VarietyID"].ToString() + "','" + dt.Rows[i]["VarietyName"].ToString() + "',";
                query = query + " '" + dt.Rows[i]["BaleWeight"].ToString() + "','" + dt.Rows[i]["LineTotal"].ToString() + "','" + dt.Rows[i]["NoOfBale"].ToString() + "','0.0','0.0','0.0',";
                query = query + "'" + txtToGoDown.SelectedValue + "','" + txtToGoDown.SelectedItem.Text + "',";
                query = query + " '" + SessionUserID + "','" + SessionUserName + "')";
                objdata.RptEmployeeMultipleDetails(query);
            }
            else if (RdpIssueType.SelectedValue == "3")
            {
                //Insert Into Cotton_Stock_Ledger_All
                query = "Insert Into Cotton_Stock_Ledger_All(Ccode,Lcode,FinYearCode,FinYearVal,Trans_No,Trans_Date,Trans_Date_Str,";
                query = query + " Trans_Type,LotNo,ItemCode,ItemName,Add_Qty,Add_Value,Add_Bale,Minus_Qty,Minus_Value,Minus_Bale,GoDownID,GoDownName,";
                query = query + " UserID,UserName) Values('" + SessionCcode + "','" + SessionLcode + "',";
                query = query + " '" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + txtTransNo.Text + "','" + transDate.ToString("yyyy/MM/dd") + "','" + txtDate.Text + "',";
                query = query + " 'RAW MATERIAL DELIVERY','" + dt.Rows[i]["LotNo"].ToString() + "','" + dt.Rows[i]["VarietyID"].ToString() + "','" + dt.Rows[i]["VarietyName"].ToString() + "','0.0',";
                query = query + " '0.0','0.0','" + dt.Rows[i]["BaleWeight"].ToString() + "','" + dt.Rows[i]["LineTotal"].ToString() + "','" + dt.Rows[i]["NoOfBale"].ToString() + "',";
                query = query + "'" + txtFromGoDown.SelectedValue + "','" + txtFromGoDown.SelectedItem.Text + "',";
                query = query + " '" + SessionUserID + "','" + SessionUserName + "')";
                objdata.RptEmployeeMultipleDetails(query);

                //Insert Into Cotton_Stock_Ledger_All
                query = "Insert Into Cotton_Stock_Ledger_All(Ccode,Lcode,FinYearCode,FinYearVal,Trans_No,Trans_Date,Trans_Date_Str,";
                query = query + " Trans_Type,LotNo,ItemCode,ItemName,Add_Qty,Add_Value,Add_Bale,Minus_Qty,Minus_Value,Minus_Bale,GoDownID,GoDownName,";
                query = query + " UserID,UserName) Values('" + SessionCcode + "','" + txtUnit.SelectedValue + "',";
                query = query + " '" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + txtTransNo.Text + "','" + transDate + "','" + txtDate.Text + "',";
                query = query + " 'RAW MATERIAL DELIVERY','" + dt.Rows[i]["LotNo"].ToString() + "','" + dt.Rows[i]["VarietyID"].ToString() + "','" + dt.Rows[i]["VarietyName"].ToString() + "',";
                query = query + " '" + dt.Rows[i]["BaleWeight"].ToString() + "','" + dt.Rows[i]["LineTotal"].ToString() + "','" + dt.Rows[i]["NoOfBale"].ToString() + "','0.0','0.0','0.0',";
                query = query + "'" + txtToGoDown.SelectedValue + "','" + txtToGoDown.SelectedItem.Text + "',";
                query = query + " '" + SessionUserID + "','" + SessionUserName + "')";
                objdata.RptEmployeeMultipleDetails(query);
            }
            
        }
    }
}
