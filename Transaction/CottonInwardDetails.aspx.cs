﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class Transaction_CottonInwardDetails : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionStdPOOrderNo;
    string SessionStdPOOrderNot;
    string SessionFinYearCode;
    string SessionFinYearVal;
    static string sum;
    string SessionSaleOrderNo;
    string Session_Ctn_Inwards_No;
    Boolean ErrFlag = false;
    string PurchaseOrderNo;
    string AgentName;
    string SupplierName;
    string Auto_Transaction_No;
    string Trans_No;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();
        if (!IsPostBack)
        {
            Page.Title = "CMS Module :: Cotton Inwards Details";
            HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Transaction"));
            li.Attributes.Add("class", "droplink active open");

            Agent_Name_Load();
            Purchase_OrderNo_Load();
            Station_Name_Load();
            Supplier_Name_Load();
            Date_Loading();
            Transport_Name_Load();
            Color();
            Count_Name_Load();
            Godown_Name_Load();
            //LoadUnit();
            //GatePassIN();

            if (Session["Ctn_Inwards_No"] == null)
            {
                Session_Ctn_Inwards_No = "";
            }
            else
            {
                Session_Ctn_Inwards_No = Session["Ctn_Inwards_No"].ToString();
                txtCottonInwardsNo.Text = Session_Ctn_Inwards_No;
                btnSearch_Click(sender, e);
            }
         
        }
        Load_Data_View();
        Load_Data_Empty_VarietyCode();

       
    }
    private void Count_Name_Load()
    {
        string query = "";
        DataTable Main_DT = new DataTable();

        txtCountCode.Items.Clear();
        query = "Select * from MstCount where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' order by CountName Asc";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtCountCode.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();
        dr["CountID"] = "-Select-";
        dr["CountName"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtCountCode.DataTextField = "CountID";
        txtCountCode.DataValueField = "CountID";
        txtCountCode.DataBind();
    }

    private void Godown_Name_Load()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();

        ddlGodownID.Items.Clear();
        query = "Select * from MstGoDown where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' order by GoDownID Asc";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlGodownID.DataSource = dtdsupp;

        DataRow dr = dtdsupp.NewRow();
        dr["GoDownID"] = "-Select-";
        dr["GoDownName"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlGodownID.DataTextField = "GoDownName";
        ddlGodownID.DataValueField = "GoDownID";
        ddlGodownID.DataBind();
    }
    private void Color()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlColor.Items.Clear();
        query = "Select *from MstColor where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlColor.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["ColorName"] = "-Select-";
        dr["ColorName"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlColor.DataTextField = "ColorName";
        ddlColor.DataValueField = "ColorName";
        ddlColor.DataBind();
    }
    //public void LoadUnit()
    //{
    //    string query = "";
    //    DataTable dtdsupp = new DataTable();
    //    ddlUnit.Items.Clear();
    //    query = "Select *from MstUnit where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
    //    dtdsupp = objdata.RptEmployeeMultipleDetails(query);
    //    ddlUnit.DataSource = dtdsupp;
    //    DataRow dr = dtdsupp.NewRow();
    //    dr["UnitName"] = "-Select-";
    //    dr["UnitName"] = "-Select-";
    //    dtdsupp.Rows.InsertAt(dr, 0);
    //    ddlUnit.DataTextField = "UnitName";
    //    ddlUnit.DataValueField = "UnitName";
    //    ddlUnit.DataBind();
    //}

    private void Station_Name_Load()
    {
        string query = "";
        DataTable Main_DT = new DataTable();
        txtstationName.Items.Clear();
        query = "Select * from MstStation where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' order by StationName Asc";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtstationName.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();
        dr["StationID"] = "-Select-";
        dr["StationName"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtstationName.DataTextField = "StationName";
        txtstationName.DataValueField = "StationID";
        txtstationName.DataBind();
    }
   
    public void Load_Data_View()
    {
        string query = "";
        DataTable Main_DT = new DataTable();
        query = "select LotNo,LotDate,Total_InwardsBaleWeight,BaleWeight,AdjustWeight,AdjustStatus from Weight_List_Main";
        query = query + " where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' and";
        if (txtSupplierName.SelectedIndex != 0)
        {
            query = query + " Supp_Name='" + txtSupplierName.SelectedItem.Text + "' and";
        }
        query = query + " FinYearCode='" + SessionFinYearCode + "' order by LotNo Asc";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        if (Main_DT.Rows.Count > 0)
        {
            //RptrCottonInwards.DataSource = Main_DT;
            //RptrCottonInwards.DataBind();
            //RptrCottonInwards.Visible = true;
        }
    }
    private void Supplier_Name_Load()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        txtSupplierName.Items.Clear();
        query = "Select * from MstSupplier where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' order by SuppName Asc";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        txtSupplierName.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["SuppCode"] = "-Select-";
        dr["SuppName"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        txtSupplierName.DataTextField = "SuppName";
        txtSupplierName.DataValueField = "SuppCode";
        txtSupplierName.DataBind();
    }
    public void Date_Loading()
    {
        txtCottonInwardsDate.Text = System.DateTime.Now.ToString("dd/MM/yyyy");
        txtLotDate.Text = System.DateTime.Now.ToString("dd/MM/yyyy");
        txtInvoiceDate.Text = System.DateTime.Now.ToString("dd/MM/yyyy");
    }
    private void Transport_Name_Load()
    {
        string query = "";
        DataTable dtdtrans = new DataTable();

        txtTransportName.Items.Clear();
        query = "Select * from MstTransporter where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' order by TransPortName Asc";
        dtdtrans = objdata.RptEmployeeMultipleDetails(query);
        txtTransportName.DataSource = dtdtrans;
        DataRow dr = dtdtrans.NewRow();
        dr["TransPortCode"] = "-Select-";
        dr["TransPortName"] = "-Select-";
        dtdtrans.Rows.InsertAt(dr, 0);
        txtTransportName.DataTextField = "TransPortName";
        txtTransportName.DataValueField = "TransPortCode";
        txtTransportName.DataBind();
    }
    private void Purchase_OrderNo_Load()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlPurchaseOrderNo.Items.Clear();
        //query = "Select * from Customer_Purchase_Order_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' and SO_Status='1' order by Cus_Pur_Order_No Asc";
        query = "Select PM.Cus_Pur_Order_No from Customer_Purchase_Order_Main PM Left join CottonInwards CI on PM.Cus_Pur_Order_No = CI.Purchase_Order_No where PM.Ccode='" + SessionCcode + "' And PM.Lcode='" + SessionLcode + "' and PM.SO_Status='1' AND CI.Purchase_Order_No IS NULL order by PM.Cus_Pur_Order_No Asc";

        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlPurchaseOrderNo.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["Cus_Pur_Order_No"] = "-Select-";
        dr["Cus_Pur_Order_No"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlPurchaseOrderNo.DataTextField = "Cus_Pur_Order_No";
        ddlPurchaseOrderNo.DataValueField = "Cus_Pur_Order_No";
        ddlPurchaseOrderNo.DataBind();
    }

    protected void RdpActiveMode_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (RdpActiveMode.SelectedValue == "20")
        {
            ddlPurchaseOrderNo.Enabled = true;
        }
        else if (RdpActiveMode.SelectedValue == "1")
        {
            ddlPurchaseOrderNo.Enabled = false;

            ddlAgentName.SelectedIndex = 0;
            ddlPurchaseOrderNo.SelectedIndex = 0;
            txtSupplierName.SelectedIndex = 0;
            txtVarietyName.Text = "";
            txtVarietyCode.Text = "";
            txtNetAmount.Text = "";
            txtBaleWeight.Text = "";
        }
        else
        {
            ddlPurchaseOrderNo.Enabled = false;

            ddlAgentName.SelectedIndex = 0;
            ddlPurchaseOrderNo.SelectedIndex = 0;
            txtSupplierName.SelectedIndex = 0;
            txtVarietyName.Text = "";
            txtVarietyCode.Text = "";
            txtNetAmount.Text = "";
            txtBaleWeight.Text = "";
        }
    }

    protected void ddlPurchaseOrderNo_SelectedIndexChanged(object sender, EventArgs e)
    {
        string query = "";
        DataTable Main_DT = new DataTable();

        query = "select CM.AgentID,CM.SupplierID,CS.VarietyName,CS.VarietyID,CM.NetAmount,CS.NoOfBale,CS.Rate";
        query = query + " from Customer_Purchase_Order_Main CM inner join Customer_Purchase_Order_Main_Sub CS";
        query = query + " on CM.Cus_Pur_Order_No = CS.Cus_Pur_Order_No";
        query = query + " where CM.Ccode='" + SessionCcode + "' And CM.Lcode='" + SessionLcode + "' And";
        query = query + " CM.FinYearCode='" + SessionFinYearCode + "'";
        query = query + " And CM.Cus_Pur_Order_No='" + ddlPurchaseOrderNo.SelectedItem.Text + "'";
        query = query + " And CS.Ccode='" + SessionCcode + "' And CS.Lcode='" + SessionLcode + "' And";
        query = query + " CS.FinYearCode='" + SessionFinYearCode + "'";
        query = query + " And CS.Cus_Pur_Order_No='" + ddlPurchaseOrderNo.SelectedItem.Text + "'";

        
        
        //query = "Select * from Customer_Purchase_Order_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Cus_Pur_Order_No='" + ddlPurchaseOrderNo.SelectedItem.Text + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        if (Main_DT.Rows.Count != 0)
        {
            ddlAgentName.SelectedValue = Main_DT.Rows[0]["AgentID"].ToString();
            txtSupplierName.SelectedValue = Main_DT.Rows[0]["SupplierID"].ToString();
            txtVarietyName.Text = Main_DT.Rows[0]["VarietyName"].ToString();
            txtVarietyCode.Text = Main_DT.Rows[0]["VarietyID"].ToString();
            txtNetAmount.Text = Main_DT.Rows[0]["NetAmount"].ToString();
            //txtBaleWeight.Text = Main_DT.Rows[0]["Qty"].ToString();
            txtRatePerCandy.Text = Main_DT.Rows[0]["Rate"].ToString();
            //ddlAgentName.Enabled = false;
            //txtSupplierName.Enabled = false;
            //txtVarietyName.Enabled = false;
            //txtVarietyCode.Enabled = false;
            //txtNetAmount.Enabled = false;
            //txtBaleWeight.Enabled = false;
        }
    }

    private void Agent_Name_Load()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlAgentName.Items.Clear();
        query = "Select * from MstAgent where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' order by AgentName Asc";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlAgentName.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["AgentID"] = "-Select-";
        dr["AgentName"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlAgentName.DataTextField = "AgentName";
        ddlAgentName.DataValueField = "AgentID";
        ddlAgentName.DataBind();
    }

    

    protected void GridViewClick_VarietyCode(object sender, CommandEventArgs e)
    {
        //txtVarietyCode.Text = Convert.ToString(e.CommandArgument);
        //txtVarietyName.Text = Convert.ToString(e.CommandName);

        txtVarietyName.Text = Convert.ToString(e.CommandName);
        string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
        string Vcode = commandArgs[0];
        string Rate = commandArgs[1];
        txtVarietyCode.Text = Vcode;
        txtRatePerCandy.Text = Rate;

        txtBaleNo.Focus();

    }


    protected void btnSave_Click(object sender, EventArgs e)
    {
        string query = "";
        DataTable dt_LotNo = new DataTable();
        DataTable DT_Check = new DataTable();
        string SaveMode = "Insert";
        bool ErrFlag = false;

        //User Rights Check Start

        bool Rights_Check = false;
        if (btnSave.Text == "Update")
        {
            //Rights_Check = CommonClass_Function.ModifyRights_Check(SessionCcode, SessionLcode, SessionUserID, "4", "5", "Cotton Inwards");
            //if (Rights_Check == false)
            //{
            //    ErrFlag = true;
            //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Modify Cotton Inwards Details..');", true);
            //}
        }
        else
        {
            //Rights_Check = CommonClass_Function.AddRights_Check(SessionCcode, SessionLcode, SessionUserID, "4", "5", "Cotton Inwards");
            //if (Rights_Check == false)
            //{
            //    ErrFlag = true;
            //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Adding New Cotton Inwards..');", true);
            //}
        }
        //User Rights Check End

        //Auto generate Transaction Function Call
        if (btnSave.Text != "Update")
        {
            if (!ErrFlag)
            {
                TransactionNoGenerate TransNO = new TransactionNoGenerate();
                string Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup(SessionCcode, SessionLcode, "Cotton Inwards", SessionFinYearVal);
                if (Auto_Transaction_No == "")
                {
                    ErrFlag = true;
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Auto Generate Transaction No Error... Contact Server Admin');", true);
                }
                else
                {
                    txtCottonInwardsNo.Text = Auto_Transaction_No;
                }
            }
        }

        if (RdpActiveMode.SelectedItem.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select PO/Agent');", true);
        }



        if (!ErrFlag)
        {
            query = "Select * from CottonInwards where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Ctn_Inwards_No='" + txtCottonInwardsNo.Text + "'";
            DT_Check = objdata.RptEmployeeMultipleDetails(query);
            if (DT_Check.Rows.Count != 0)
            {
                SaveMode = "Update";
                query = "Delete from CottonInwards where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Ctn_Inwards_No='" + txtCottonInwardsNo.Text + "'";
                objdata.RptEmployeeMultipleDetails(query);
                //query = "Delete from Payment_Status where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Trans_No='" + txtCottonInwardsNo.Text + "'";
                //objdata.RptEmployeeMultipleDetails(query);
            }


            if (ddlPurchaseOrderNo.SelectedItem.Text == "-Select-")
            {
                PurchaseOrderNo = "";
            }
            else
            {
                PurchaseOrderNo = ddlPurchaseOrderNo.SelectedItem.Text;
            }

            if (ddlAgentName.SelectedItem.Text == "-Select-")
            {
                AgentName = "";
            }
            else
            {
                AgentName = ddlAgentName.SelectedItem.Text;
            }


            if (txtSupplierName.SelectedItem.Text == "-Select-")
            {
                SupplierName = "";
            }
            else
            {
                SupplierName = txtSupplierName.SelectedItem.Text;
            }

            query = "Select * from CottonInwards where LotNo='" + txtLotNo.Text + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
            dt_LotNo = objdata.RptEmployeeMultipleDetails(query);
            if (dt_LotNo.Rows.Count > 0)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Lotno Already Available...,');", true);
            }
            else
            {
                //Insert Main Table
                query = "Insert Into CottonInwards(Ccode,Lcode,FinYearCode,FinYearVal,Ctn_Inwards_No,";
                query = query + "Ctn_Inwards_Date,GatePassIN,LotNo,LotDate,Type,Purchase_Order_No,AgentID,";
                query = query + "AgentName,Supp_Code,Supp_Name,InvoiceNo,InvoiceDate,TransportCode,TransportName,";
                query = query + "StationCode,StationName,LorryNo,LorryFright,PartyLotNo,";
                query = query + "PressMarkNo,VarietyCode,VarietyName,NoOfBale,RatePerCandy,CandyWeight,BaleWeight,";
                query = query + "NetAmount,Cotton_Stable,Cotton_Mic,Cotton_Lengh,Cotton_Master,Cottin_Others,UserID,UserName,CI_Status,D_Status,ColorName,GoDownID,GoDownName,CountCode,CountName,TareWeight,NetWeight,Weighbridge,InformWeight)";
                query = query + "Values('" + SessionCcode + "','" + SessionLcode + "','" + SessionFinYearCode + "',";
                query = query + "'" + SessionFinYearVal + "','" + txtCottonInwardsNo.Text + "',";
                query = query + "'" + txtCottonInwardsDate.Text + "','',";
                query = query + "'" + txtLotNo.Text + "','" + txtLotDate.Text + "',";
                query = query + "'" + RdpActiveMode.SelectedValue + "','" + PurchaseOrderNo + "',";
                query = query + "'" + ddlAgentName.SelectedValue + "','" + AgentName + "',";
                query = query + "'" + txtSupplierName.SelectedValue + "','" + SupplierName + "',";
                query = query + "'" + txtInvoiceNo.Text + "','" + txtInvoiceDate.Text + "',";
                query = query + "'" + txtTransportName.SelectedValue + "','" + txtTransportName.SelectedItem.Text + "',";
                query = query + "'" + txtstationName.SelectedValue + "','" + txtstationName.SelectedItem.Text + "',";
                query = query + "'" + txtLorryNo.Text + "','" + txtLorryFright.Text + "',";
                //query = query + "'" + txtpartyPRno2.Text + "','" + txtpartyPRno3.Text + "','" + txtpartyPRno4.Text + "',";
                query = query + "'" + txtPartyLotNo.Text + "','" + txtPressMarkNo.Text + "','" + txtVarietyCode.Text + "',";
                query = query + "'" + txtVarietyName.Text + "','" + txtBaleNo.Text + "','" + txtRatePerCandy.Text + "',";
                query = query + "'" + txtCandyweight.Text + "','" + txtBaleWeight.Text + "','" + txtNetAmount.Text + "',";
                query = query + "'" + txtCottonStable.Text + "','" + txtCotton_Mic.Text + "','" + txtLengh.Text + "','" + txtMaster.Text + "','" + txtOthers.Text + "',";
                query = query + "'" + SessionUserID + "','" + SessionUserName + "','0','0','" + ddlColor.SelectedValue + "','" + ddlGodownID.SelectedValue + "','" + ddlGodownID.SelectedItem.Text + "',";
                query = query + "'" + txtCountCode.SelectedValue + "','" + txtCountName.Text + "','" + txtTareWeight.Text + "','" + txtNetWeight.Text + "','" + txtBridge.Text + "','" + txtInformWeight.Text + "')";


                objdata.RptEmployeeMultipleDetails(query);


                //query = "Insert into Payment_Status(Ccode,Lcode,FinYearCode,FinYearVal,Trans_No,Date,Type,Supplier_ID,Supplier_Name,Bill_Amt,Paid_Amt,Advance_Amt)";
                //query = query + "  values('" + SessionCcode + "','" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + txtCottonInwardsNo.Text + "','" + txtCottonInwardsDate.Text + "',";
                //query = query + " 'INWARDS','" + txtSupplierName.SelectedValue + "','" + txtSupplierName.SelectedItem.Text + "','" + txtNetAmount.Text + "','0','0' )";
                //objdata.RptEmployeeMultipleDetails(query);




                //Update GatePass_ID status
                //query = "Update GatePass_IN_Main set GP_IN_Status='2' where GP_IN_No='" + ddlGateIN.SelectedItem.Text + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                //objdata.RptEmployeeMultipleDetails(query);


                if (SaveMode == "Insert")
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Cotton Inwards Details Saved Successfully');", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Cotton Inwards Details Updated Successfully');", true);
                }

                Session["Ctn_Inwards_No"] = txtCottonInwardsNo.Text;
                btnSave.Text = "Update";
                Clear_All_Field();
                Response.Redirect("CottonInwards_Main.aspx");
            }
                

        


        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Clear_All_Field();

    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        //Search Gate_Pass_Out
        txtCottonInwardsDate.Enabled = false;
        txtLotNo.Enabled = false;
        RdpActiveMode.Enabled = false;
        ddlPurchaseOrderNo.Enabled = false;
        ddlAgentName.Enabled = false;
        txtLotDate.Enabled = false;
        txtSupplierName.Enabled = false;
        txtInvoiceNo.Enabled = false;
        txtInvoiceDate.Enabled = false;
        txtTransportName.Enabled = false;
        txtstationName.Enabled = false;
        txtLorryNo.Enabled = false;
        txtLorryFright.Enabled = false;
        //txtpartyPRno1.Enabled = false;
        //txtpartyPRno2.Enabled = false;
        //txtpartyPRno3.Enabled = false;
        //txtpartyPRno4.Enabled = false;
        txtPartyLotNo.Enabled = false;
        txtPressMarkNo.Enabled = false;

        txtVarietyCode.Enabled = false;
        txtBaleNo.Enabled = false;
        txtRatePerCandy.Enabled = false;
        txtCandyweight.Enabled = false;
        txtBaleWeight.Enabled = false;
        txtNetAmount.Enabled = false;
        

        string query = "";
        DataTable Main_DT = new DataTable();
        query = "Select * from CottonInwards where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Ctn_Inwards_No='" + txtCottonInwardsNo.Text + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        if (Main_DT.Rows.Count != 0)
        {
            txtCottonInwardsNo.Text = Main_DT.Rows[0]["Ctn_Inwards_No"].ToString();
            txtCottonInwardsDate.Text = Main_DT.Rows[0]["Ctn_Inwards_Date"].ToString();
            txtLotNo.Text = Main_DT.Rows[0]["LotNo"].ToString();
            txtLotDate.Text = Main_DT.Rows[0]["LotDate"].ToString();
            //txtSupplierName.SelectedValue = Main_DT.Rows[0]["Supp_Code"].ToString();
            //txtSupplierName.SelectedItem.Text = Main_DT.Rows[0]["Supp_Name"].ToString();
            txtInvoiceNo.Text = Main_DT.Rows[0]["InvoiceNo"].ToString();
            txtInvoiceDate.Text = Main_DT.Rows[0]["InvoiceDate"].ToString();
            txtTransportName.SelectedValue = Main_DT.Rows[0]["TransportCode"].ToString();
            //txtTransportName.SelectedItem.Text = Main_DT.Rows[0]["TransportName"].ToString();
            RdpActiveMode.SelectedValue = Main_DT.Rows[0]["Type"].ToString();

            PurchaseOrderNo = Main_DT.Rows[0]["Purchase_Order_No"].ToString();
            if (PurchaseOrderNo.ToString() == "")
            {
                ddlPurchaseOrderNo.SelectedValue = "-Select-";
            }
            else
            {
                ddlPurchaseOrderNo.SelectedItem.Text = Main_DT.Rows[0]["Purchase_Order_No"].ToString();
            }

            AgentName = Main_DT.Rows[0]["AgentID"].ToString();
            if (AgentName.ToString() == "")
            {
                ddlAgentName.SelectedIndex = 0;
            }
            else
            {
                ddlAgentName.SelectedValue = Main_DT.Rows[0]["AgentID"].ToString();
            }

            SupplierName = Main_DT.Rows[0]["Supp_Code"].ToString();
            if (SupplierName.ToString() == "")
            {
                txtSupplierName.SelectedIndex = 0;
            }
            else
            {
                txtSupplierName.SelectedValue = Main_DT.Rows[0]["Supp_Code"].ToString();
            }


            //ddlPurchaseOrderNo.SelectedValue = Main_DT.Rows[0]["Purchase_Order_No"].ToString();
            //ddlAgentName.SelectedValue = Main_DT.Rows[0]["AgentID"].ToString();
            txtstationName.SelectedValue = Main_DT.Rows[0]["StationCode"].ToString();
            //txtstationName.SelectedItem.Text = Main_DT.Rows[0]["StationName"].ToString();
            txtLorryNo.Text = Main_DT.Rows[0]["LorryNo"].ToString();
            txtLorryFright.Text = Main_DT.Rows[0]["LorryFright"].ToString();
            //txtpartyPRno1.Text = Main_DT.Rows[0]["PRNo1"].ToString();
            //txtpartyPRno2.Text = Main_DT.Rows[0]["PRNo2"].ToString();
            //txtpartyPRno3.Text = Main_DT.Rows[0]["PRNo3"].ToString();
            //txtpartyPRno4.Text = Main_DT.Rows[0]["PRNo4"].ToString();
            txtPartyLotNo.Text = Main_DT.Rows[0]["PartyLotNo"].ToString();
            txtPressMarkNo.Text = Main_DT.Rows[0]["PressMarkNo"].ToString();
            txtVarietyCode.Text = Main_DT.Rows[0]["VarietyCode"].ToString();
            txtVarietyName.Text = Main_DT.Rows[0]["VarietyName"].ToString(); ;
            txtBaleNo.Text = Main_DT.Rows[0]["NoOfBale"].ToString();
            txtRatePerCandy.Text = Main_DT.Rows[0]["RatePerCandy"].ToString();
            txtCandyweight.Text = Main_DT.Rows[0]["CandyWeight"].ToString();
            txtBaleWeight.Text = Main_DT.Rows[0]["BaleWeight"].ToString();
            txtNetAmount.Text = Main_DT.Rows[0]["NetAmount"].ToString();
            txtCottonStable.Text = Main_DT.Rows[0]["Cotton_Stable"].ToString();
            ddlColor.SelectedValue = Main_DT.Rows[0]["ColorName"].ToString();

            ddlGodownID.SelectedValue = Main_DT.Rows[0]["GoDownID"].ToString();
            txtCountName.Text = Main_DT.Rows[0]["CountName"].ToString();
            txtCountCode.Text = Main_DT.Rows[0]["CountCode"].ToString();
            txtBridge.Text = Main_DT.Rows[0]["Weighbridge"].ToString();
            txtInformWeight.Text = Main_DT.Rows[0]["InformWeight"].ToString();
            txtTareWeight.Text = Main_DT.Rows[0]["TareWeight"].ToString();
            txtNetWeight.Text = Main_DT.Rows[0]["NetWeight"].ToString();



            //ddlUnit.SelectedValue = Main_DT.Rows[0]["unit"].ToString();

            btnSave.Text = "Update";
        }
        else
        {
            Clear_All_Field();
        }
    }

    protected void btnBackEnquiry_Click(object sender, EventArgs e)
    {
        Response.Redirect("CottonInwards_Main.aspx");
    }

    private void Clear_All_Field()
    {
        if (RdpActiveMode.SelectedValue == "1")
        {
            ddlPurchaseOrderNo.Enabled = false;
          
        }
        txtCottonInwardsNo.Text = ""; txtCottonInwardsDate.Text = "";
        txtLotNo.Text = ""; txtLotDate.Text = ""; txtSupplierName.SelectedItem.Text = ""; txtInvoiceNo.Text = "";
        txtInvoiceDate.Text = ""; txtTransportName.SelectedItem.Text = "";
        Station_Name_Load();
        Supplier_Name_Load();
        Agent_Name_Load();
        Purchase_OrderNo_Load();
        Station_Name_Load();
        Transport_Name_Load();
        Date_Loading();
        txtLorryNo.Text = ""; txtLorryFright.Text = ""; 
        txtPartyLotNo.Text = ""; txtPressMarkNo.Text = ""; txtVarietyCode.Text = "";
        txtVarietyName.Text = ""; txtBaleNo.Text = ""; txtRatePerCandy.Text = ""; txtCandyweight.Text = ""; txtBaleWeight.Text = "";
        txtCottonStable.Text = "0";
        txtCotton_Mic.Text = "0";
        btnSave.Text = "Save";
        Session.Remove("Ctn_Inwards_No");
        Load_Data_Empty_VarietyCode();

        txtNetAmount.Text = "";
    }

    protected void btnVarietyCode_Click(object sender, EventArgs e)
    {
        modalPop_VarietyCode.Show();
    }
    private void Load_Data_Empty_VarietyCode()
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select VarietyID,VarietyName,RatePerCandy from MstVariety where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        Repeater_VarietyCode.DataSource = DT;
        Repeater_VarietyCode.DataBind();
        Repeater_VarietyCode.Visible = true;
    }


    protected void txtSupplierName_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_Data_View();
    }
   
    protected void txtCountCode_SelectedIndexChanged(object sender, EventArgs e)
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "select CountName from MstCount";
        query = query + " where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        query = query + " and CountID='" + txtCountCode.Text + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count > 0)
        {
            txtCountName.Text = DT.Rows[0]["CountName"].ToString();
        }
    }
    protected void txtTareWeight_TextChanged(object sender, EventArgs e)
    {
        txtNetWeight.Text = (Convert.ToDecimal(txtBaleWeight.Text) - Convert.ToDecimal(txtTareWeight.Text)).ToString();
    }
    protected void btnApproval_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        string query;
        DataTable DT_Check = new DataTable();

        query = "Select * from CottonInwards where Ctn_Inwards_No='" + txtCottonInwardsNo.Text + "'";
        query = query + " And Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
        DT_Check = objdata.RptEmployeeMultipleDetails(query);
        if (DT_Check.Rows.Count == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('First you have to register this Weight List No...');", true);
        }

        query = "Select * from CottonInwards where CI_Status = '1' And Ctn_Inwards_No='" + txtCottonInwardsNo.Text + "'";
        query = query + " And Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
        DT_Check = objdata.RptEmployeeMultipleDetails(query);
        if (DT_Check.Rows.Count != 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('This Cotton Inward No Already Approved...');", true);
        }

        query = "update CottonInwards set CI_Status = '1' where Ctn_Inwards_No='" + txtCottonInwardsNo.Text + "'";
        objdata.RptEmployeeMultipleDetails(query);

        TransactionNoGenerate TransNO = new TransactionNoGenerate();
        Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup(SessionCcode, SessionLcode, "Opening Stock", SessionFinYearVal);
        if (Auto_Transaction_No == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Auto Generate Transaction No Error... Contact Server Admin.');", true);
        }
        else
        {

            //txtWeightListNo.Text = Auto_Transaction_No;
            Trans_No = Auto_Transaction_No.ToString();
        }

        string Trans_Date_Format = DateTime.Now.ToString();
        string Trans_Date = DateTime.Now.ToString("dd/MM/yyyy");


        query = "insert into Temp_Upload_Stock(Ccode,Lcode,TransNo,TransDate,LotNo,VariteyCode,VariteyName,BaleNo,";
        query = query + "InvoiceWeight,GoDownID,GoDownName,UserID,UserName,OP_Status) values('" + SessionCcode + "',";
        query = query + "'" + SessionLcode + "','" + Trans_No + "','" + Trans_Date + "',";
        query = query + "'" + txtLotNo.Text + "','" + txtVarietyCode.Text + "','" + txtVarietyName.Text + "',";
        query = query + "'" + txtBaleNo.Text + "','" + txtBaleWeight.Text + "','" + ddlGodownID.SelectedValue + "','" + ddlGodownID.SelectedItem.Text + "',";
        query = query + "'" + SessionUserID + "','" + SessionUserName + "','0')";
        objdata.RptEmployeeMultipleDetails(query);


        //Insert Data Stock Transaction Ledger
        query = "insert into Stock_Transaction_Ledger(Ccode,Lcode,FinYearCode,FinYearVal,Trans_No,Trans_Date,Trans_Date_Str,Trans_Type,LotNo,VariteyCode,VariteyName,BaleNo,PO_Qty,GoDownID,GoDownName,UserID,UserName,Count_Code,Count_Name)";
        query = query + "values('" + SessionCcode + "','" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + txtCottonInwardsNo.Text + "','" + Convert.ToDateTime(txtCottonInwardsDate.Text).AddDays(0).ToString("MM/dd/yyyy") + "','" + txtCottonInwardsDate.Text + "','WEIGHT LIST','" + txtLotNo.Text + "','" + txtVarietyCode.Text + "','" + txtVarietyName.Text + "',";
        query = query + "'" + txtBaleNo.Text + "','" + txtBaleWeight.Text + "','" + ddlGodownID.SelectedValue + "','" + ddlGodownID.SelectedItem.Text + "','" + SessionUserID + "','" + SessionUserName + "','" + txtCountCode.Text + "','" + txtCountName.Text + "')";
        objdata.RptEmployeeMultipleDetails(query);


        Response.Redirect("CottonInwards_Main.aspx");
    }
    protected void txtBaleWeight_TextChanged(object sender, EventArgs e)
    {
        SumCal();
    }
    public void SumCal()
    {
        if (txtBaleWeight.Text == "")
        {
            txtBaleWeight.Text = "0";
        }
        if (txtCandyweight.Text == "")
        {
            txtCandyweight.Text = "0";
        }
        txtNetAmount.Text = (Convert.ToDecimal(txtCandyweight.Text) * Convert.ToDecimal(txtBaleWeight.Text)).ToString();
    }
}
