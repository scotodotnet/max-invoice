﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;


public partial class Transaction_RawMaterial_Sub : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionEnquiryNo;
    string SessionFinYearCode;
    string SessionFinYearVal;
    string SessionTrans_No;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "ERP Module :: Raw Material";
            HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Transaction"));
            li.Attributes.Add("class", "droplink active open");

            Load_Supplier();
            Load_Station();
            Load_GoDown();
            Load_Variety();

            if (Session["Trans_No"] == null)
            {
                SessionTrans_No = "";
            }
            else
            {
                SessionTrans_No = Session["Trans_No"].ToString();
                txtTransNo.Text = SessionTrans_No;
                btnSearch_Click(sender, e);
            }

        }

    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        //Search Gate_Pass_Out
        string query = "";
        DataTable Main_DT = new DataTable();
        query = "Select * from RawMaterial where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Trans_No='" + txtTransNo.Text + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        if (Main_DT.Rows.Count != 0)
        {
            txtTransNo.Text = Main_DT.Rows[0]["Trans_No"].ToString();
            txtTransDate.Text = Main_DT.Rows[0]["Trans_Date"].ToString();
            txtLotNo.Text = Main_DT.Rows[0]["LotNo"].ToString();
            txtSupplierName.SelectedValue = Main_DT.Rows[0]["SuppCode"].ToString();
            txtstationName.SelectedValue = Main_DT.Rows[0]["StationID"].ToString();
            ddlGoDown.SelectedValue = Main_DT.Rows[0]["GoDownID"].ToString();
            ddlVarietyName.SelectedValue = Main_DT.Rows[0]["VarietyID"].ToString();
            txtTrash.Text = Main_DT.Rows[0]["Trash"].ToString();
            txtOthers.Text = Main_DT.Rows[0]["Moisture"].ToString();
            txtBaleNo.Text = Main_DT.Rows[0]["NoOfBale"].ToString();
            txtCandyweight.Text = Main_DT.Rows[0]["CandyWeight"].ToString();
            txtBaleWeight.Text = Main_DT.Rows[0]["BaleWeight"].ToString();
            txtNetAmount.Text = Main_DT.Rows[0]["NetAmount"].ToString();

          
            btnSave.Text = "Update";
        }
        else
        {

        }
    }


    private void Load_Supplier()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        txtSupplierName.Items.Clear();
        query = "Select *from MstSupplier where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        txtSupplierName.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["SuppName"] = "-Select-";
        dr["SuppCode"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        txtSupplierName.DataTextField = "SuppName";
        txtSupplierName.DataValueField = "SuppCode";
        txtSupplierName.DataBind();
    }

    private void Load_Station()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        txtstationName.Items.Clear();
        query = "Select *from MstStation where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        txtstationName.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["StationName"] = "-Select-";
        dr["StationID"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        txtstationName.DataTextField = "StationName";
        txtstationName.DataValueField = "StationID";
        txtstationName.DataBind();
    }

    private void Load_GoDown()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlGoDown.Items.Clear();
        query = "Select *from MstGoDown where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlGoDown.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["GoDownName"] = "-Select-";
        dr["GoDownID"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlGoDown.DataTextField = "GoDownName";
        ddlGoDown.DataValueField = "GoDownID";
        ddlGoDown.DataBind();
    }

    private void Load_Variety()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlVarietyName.Items.Clear();
        query = "Select *from MstVariety where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlVarietyName.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["VarietyName"] = "-Select-";
        dr["VarietyID"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlVarietyName.DataTextField = "VarietyName";
        ddlVarietyName.DataValueField = "VarietyID";
        ddlVarietyName.DataBind();
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        string query = "";
        DataTable DT_Check = new DataTable();
        string SaveMode = "Insert";
        bool ErrFlag = false;

        


        //User Rights Check Start
        bool Rights_Check = false;
        //if (btnSave.Text == "Update")
        //{
        //    Rights_Check = CommonClass_Function.ModifyRights_Check(SessionCcode, SessionLcode, SessionUserID, "4", "5", "Gate Pass In");
        //    if (Rights_Check == false)
        //    {
        //        ErrFlag = true;
        //        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Modify Gate Pass In Details..');", true);
        //    }
        //}
        //else
        //{
        //    Rights_Check = CommonClass_Function.AddRights_Check(SessionCcode, SessionLcode, SessionUserID, "4", "5", "Gate Pass In");
        //    if (Rights_Check == false)
        //    {
        //        ErrFlag = true;
        //        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Adding New Gate Pass In..');", true);
        //    }
        //}
        //User Rights Check End

        //Auto generate Transaction Function Call
        if (btnSave.Text != "Update")
        {
            if (!ErrFlag)
            {
                TransactionNoGenerate TransNO = new TransactionNoGenerate();
                string Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup(SessionCcode, SessionLcode, "Raw Material", SessionFinYearVal);
                if (Auto_Transaction_No == "")
                {
                    ErrFlag = true;
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Auto Generate Transaction No Error... Contact Server Admin');", true);
                }
                else
                {
                    txtTransNo.Text = Auto_Transaction_No;
                }
            }
        }

        if (!ErrFlag)
        {


            query = "Select * from RawMaterial where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Trans_No='" + txtTransNo.Text + "'";
            DT_Check = objdata.RptEmployeeMultipleDetails(query);
            if (DT_Check.Rows.Count != 0)
            {
                SaveMode = "Update";
                query = "Delete from RawMaterial where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Trans_No='" + txtTransNo.Text + "'";
                objdata.RptEmployeeMultipleDetails(query);
                
            }

            //Insert Main Table
            query = "Insert Into RawMaterial(Ccode,Lcode,FinYearCode,FinYearVal,Trans_No,Trans_Date,SuppCode,SuppName,StationID,StationName,GoDownID,GoDownName,";
            query = query + "LotNo,VarietyID,VarietyName,Trash,Moisture,NoOfBale,CandyWeight,BaleWeight,NetAmount,UserID,UserName) Values('" + SessionCcode + "',";
            query = query + " '" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + txtTransNo.Text + "','" + txtTransDate.Text + "',";
            query = query + " '" + txtSupplierName.SelectedValue + "','" + txtSupplierName.SelectedItem.Text + "','" + txtstationName.SelectedValue + "','" + txtstationName.SelectedItem.Text + "',";
            query = query + " '" + ddlGoDown.SelectedValue + "','" + ddlGoDown.SelectedItem.Text + "','" + txtLotNo.Text + "',";
            query = query + "'" + ddlVarietyName.SelectedValue + "','" + ddlVarietyName.SelectedItem.Text + "','" + txtTrash.Text + "',";
            query = query + "'" + txtOthers.Text + "','" + txtBaleNo.Text + "','" + txtCandyweight.Text + "','" + txtBaleWeight.Text + "',";
            query = query + " '" + txtNetAmount.Text + "','" + SessionUserID + "','" + SessionUserName + "')";
            objdata.RptEmployeeMultipleDetails(query);

            
            if (SaveMode == "Insert")
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Raw Material Details Saved Successfully');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Raw Material Details Updated Successfully');", true);
            }

           
            
            Session["Trans_No"] = txtTransNo.Text;
            txtTransNo.Text = Session["Trans_No"].ToString();
            btnSearch_Click(sender, e);
            btnSave.Text = "Update";
            Clear_All_Field();
            //Response.Redirect("Gate_Pass_In_Main.aspx");

        }
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }
    protected void btnBackEnquiry_Click(object sender, EventArgs e)
    {
        Session.Remove("Trans_No");
        Response.Redirect("RawMaterial_Main.aspx");
    }
    protected void btnApprove_Click(object sender, EventArgs e)
    {
        string query = "";
        DataTable DT_Check = new DataTable();
        bool ErrFlag = false;

        //User Rights Check Start
        bool Rights_Check = false;
        //Rights_Check = CommonClass_Function.ApproveRights_Check(SessionCcode, SessionLcode, SessionUserID, "7", "2", "Unplanned Receipt");
        //if (Rights_Check == false)
        //{
        //    ErrFlag = true;
        //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Approve Unplanned Receipt...');", true);
        //}
        //User Rights Check End

        query = "Select * from RawMaterial where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Trans_No='" + txtTransNo.Text + "'";
        DT_Check = objdata.RptEmployeeMultipleDetails(query);
        if (DT_Check.Rows.Count == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('First You have to Register this Raw Material Receipt Details..');", true);
        }

        query = "Select * from RawMaterial where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Trans_No='" + txtTransNo.Text + "' And Status='1'";
        DT_Check = objdata.RptEmployeeMultipleDetails(query);
        if (DT_Check.Rows.Count != 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Raw Material Receipt Details Already Approved..');", true);
        }

        if (!ErrFlag)
        {
            query = "Update RawMaterial set Status='1' where Ccode='" + SessionCcode + "'";
            query = query + "  And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
            query = query + "  And Trans_No='" + txtTransNo.Text + "'";
            objdata.RptEmployeeMultipleDetails(query);

            Stock_Add();

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Raw Material Receipt Details Approved Successfully..');", true);
        }
    }

    private void Clear_All_Field()
    {
        txtTransNo.Text = "";
        txtTransDate.Text = "";
        txtSupplierName.SelectedValue = "-Select-";
        txtstationName.SelectedValue = "-Select-";
        ddlGoDown.SelectedValue = "-Select-";
        txtLotNo.Text = "";
        ddlVarietyName.SelectedValue = "-Select-";

        txtTrash.Text = "0";
        txtOthers.Text = "0";
        txtBaleNo.Text = "0";
        txtCandyweight.Text = "0";
        txtBaleWeight.Text = "0";
        txtNetAmount.Text = "0";
        btnSave.Text = "Save";
        Session.Remove("Trans_No");
    }

    private void Stock_Add()
    {
        string query = "";
        DataTable dt_check = new DataTable();
        DataTable qry_dt = new DataTable();


        DateTime transDate = Convert.ToDateTime(txtTransDate.Text);

        //Check Cotton_Stock_Ledger_All
        query = "Select * from Cotton_Stock_Ledger_All where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        query = query + " And FinYearCode='" + SessionFinYearCode + "' And Trans_No='" + txtTransNo.Text + "'";
        query = query + " And ItemCode='" + ddlVarietyName.SelectedValue + "'";
        dt_check = objdata.RptEmployeeMultipleDetails(query);
        if (dt_check.Rows.Count != 0)
        {
            query = "Delete from Cotton_Stock_Ledger_All where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
            query = query + " And FinYearCode='" + SessionFinYearCode + "' And Trans_No='" + txtTransNo.Text + "'";
            query = query + " And ItemCode='" + ddlVarietyName.SelectedValue + "'";
            objdata.RptEmployeeMultipleDetails(query);
        }

        //Insert Into Cotton_Stock_Ledger_All
        query = "Insert Into Cotton_Stock_Ledger_All(Ccode,Lcode,FinYearCode,FinYearVal,Trans_No,Trans_Date,Trans_Date_Str,";
        query = query + " Trans_Type,LotNo,ItemCode,ItemName,Add_Qty,Add_Value,Add_Bale,Minus_Qty,Minus_Value,Minus_Bale,GoDownID,GoDownName,";
        query = query + " SuppCode,SuppName,UserID,UserName) Values('" + SessionCcode + "','" + SessionLcode + "',";
        query = query + " '" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + txtTransNo.Text + "','" + transDate.ToString("yyyy/MM/dd") + "','" + txtTransDate.Text + "',";
        query = query + " 'RAW MATERIAL RECEIPT','" + txtLotNo.Text + "','" + ddlVarietyName.SelectedValue + "','" + ddlVarietyName.SelectedItem.Text + "','" + txtBaleWeight.Text + "',";
        query = query + " '" + txtNetAmount.Text + "','" + txtBaleNo.Text + "','0.0','0.0','0.0','" + ddlGoDown.SelectedValue + "','" + ddlGoDown.SelectedItem.Text + "',";
        query = query + " '" + txtSupplierName.SelectedValue + "','" + txtSupplierName.SelectedItem.Text + "',";
        query = query + " '" + SessionUserID + "','" + SessionUserName + "')";
        objdata.RptEmployeeMultipleDetails(query);

    }
}
