﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="WeightList.aspx.cs" Inherits="Transaction_WeightList" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<script src='<%= ResolveUrl("../assets/js/master_list_jquery.min.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/master_list_jquery-ui.min.js") %>'></script>
<link href="<%= ResolveUrl("../assets/css/master_list_jquery-ui.css") %>" rel="stylesheet" type="text/css"/>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function(sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    $('#TblSupplier').dataTable();
                }
            });
        };
   </script>
   
    <script>
        $(document).ready(function() {
            $('#TblSupplier').dataTable();
        });
    </script>
    
    
  
<script src='<%= ResolveUrl("../assets/js/master_list_jquery.min.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/master_list_jquery-ui.min.js") %>'></script>
<link href="<%= ResolveUrl("../assets/css/master_list_jquery-ui.css") %>" rel="stylesheet" type="text/css"/>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function(sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    $('#TblWeightList').dataTable();
                }
            });
        };
   </script>
   
    <script>
        $(document).ready(function() {
        $('#TblWeightList').dataTable();
        });
    </script>  
    
    
 <script src='<%= ResolveUrl("../assets/js/master_list_jquery.min.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/master_list_jquery-ui.min.js") %>'></script>
<link href="<%= ResolveUrl("../assets/css/master_list_jquery-ui.css") %>" rel="stylesheet" type="text/css"/>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function(sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    $('#VarietyTable').dataTable();
                }
            });
        };
   </script>
   
   
    <script>
        $(document).ready(function() {
            $('#VarietyTable').dataTable();
        });
    </script> 
    
     <script src='<%= ResolveUrl("../assets/js/master_list_jquery.min.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/master_list_jquery-ui.min.js") %>'></script>
<link href="<%= ResolveUrl("../assets/css/master_list_jquery-ui.css") %>" rel="stylesheet" type="text/css"/>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function(sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    $('#TblLotNo').dataTable();
                }
            });
        };
   </script>
   
   
    <script>
        $(document).ready(function() {
        $('#TblLotNo').dataTable();
        });
    </script> 
    
    
<script type="text/javascript">
    function SaveMsgAlert(msg) {
        swal(msg);
    }
</script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('.date-picker').datepicker({ format: 'dd/mm/yyyy' });
                $('.js-states').select2();
            }
        });
    };
</script>


<link href="<%= ResolveUrl("../assets/css/Popup.css") %>" rel="stylesheet" type="text/css"/>
    <script>
        function pageLoad(sender, args) {
            if (!args.get_isPartialLoad()) {
                //  add our handler to the document's
                //  keydown event
                $addHandler(document, "keydown", onKeyDown);
            }
        }

        function onKeyDown(e) {
            if (e && e.keyCode == Sys.UI.Key.esc) {
                // if the key pressed is the escape key, dismiss the dialog
                $find('Supp1_Close').hide();
                
                $find('Variety_Close').hide();
                $find('LotNo_Close').hide();

            }
        } 
 </script>


    <div class="page-breadcrumb">
        <ol class="breadcrumb container">
         <h4><li class="active">Weight List</li></h4> 
       </ol>
   </div>
   
   <div id="main-wrapper" class="container">
 <div class="row">
    <div class="col-md-12">
        <div class="col-md-9">
			<div class="panel panel-white">
			<div class="panel panel-primary">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">Weight List</h4>
				</div>
				</div>
				<form class="form-horizontal">
				
				<div class="panel-body">
				
			       	 <div class="col-md-12">
					    <div class="row">
					    
					         <div class="form-group col-md-4">
					            <label for="exampleInputName">Weight.List.No<span class="mandatory">*</span></label>
					            <asp:Label ID="txtWeightListNo" runat="server" class="form-control"></asp:Label>
					        </div>
					        
					         <div class="form-group col-md-4">
					            <label for="exampleInputName">Weight.List.Date<span class="mandatory">*</span></label>
                                <asp:TextBox ID="txtWeightListDate" MaxLength="30" class="form-control date-picker" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ControlToValidate="txtWeightListDate" ValidationGroup="Validate_Field" class="form_error" ID="validate1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
					        </div>

					         <div class="form-group col-md-4">
					            <label for="exampleInputName">Lot No<span class="mandatory">*</span></label>
                                <asp:TextBox ID="txtLotNo" MaxLength="30" class="form-control" runat="server" style="float: left;width: 84%;"></asp:TextBox>
                                 <asp:Button ID="BtnLotNo" runat="server" Text="+" class="fa fa-plus" 
                                    style="float: left; padding: 9px;" onclick="BtnLotNo_Click"/>
                                    
                               <cc1:ModalPopupExtender ID="Mdlpopup_LotNo"  runat="server" PopupControlID="PanelLotNo" TargetControlID="BtnLotNo"
                                     CancelControlID="BtnClose" BackgroundCssClass="modalBackground" BehaviorID="LotNo_Close">
                               </cc1:ModalPopupExtender>
                               <asp:Panel ID="PanelLotNo" runat="server" CssClass="modalPopup" style="display:none" >
                               <div class="header">
                                     Lot Details
                               </div>
                              <div class="body">
				                 <div class="col-md-12 headsize">
					
					                <asp:Repeater ID="RepeaterLotNo" runat="server" EnableViewState="false">
					                <HeaderTemplate>
                                       <table id="TblLotNo" class="display table">
                                         <thead >
                                           <tr>
                                             <th>LotNo</th>
                                             <th>BaleWeight</th>
                                             <th>Mode</th>
                                         </tr>
                                       </thead>
                                   </HeaderTemplate>
                                   <ItemTemplate>
                                     <tr>
                                       <td><%# Eval("LotNo")%></td>
                                      <%-- <td><%# Eval("LotDate")%></td>
                                       <td><%# Eval("BaleNo")%></td>--%>
                                       <td><%# Eval("BaleWeight")%></td>
                                       <td>
                                         <asp:LinkButton ID="btnEditEnquiry_Grid" class="btn btn-success btn-sm fa fa-pencil"  runat="server" 
                                         Text="" OnCommand="GridViewClick_LotNo" CommandArgument='<%# Eval("LotNo")%>' CommandName='<%# Eval("LotNo")%>'>
                                         </asp:LinkButton>
                                      </td>
                                   </tr>
                                  </ItemTemplate>
                                 <FooterTemplate></table></FooterTemplate>                                
					            </asp:Repeater>
					         </div>
					     </div>
					     
					     <div class="footer" align="right">
					     <asp:Button ID="BtnClose" class="btn btn-rounded" runat="server" Text="Close"/>
					     </div>
					     </asp:Panel>
                                <asp:HiddenField ID="txthiddenlotdate" Value="0" runat="server" />
                                
                                
                                
                                <asp:RequiredFieldValidator ControlToValidate="txtLotNo" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator2" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
					        </div>
					     
					     </div>
					</div>
					
				     <div class="col-md-12">
					<div class="row">
					
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">Lot Date<span class="mandatory">*</span></label>
                                <asp:TextBox ID="txtLotDate" MaxLength="30" class="form-control date-picker" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ControlToValidate="txtLotDate" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator10" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
					        </div>

					        <div class="form-group col-md-4">
					            <label for="exampleInputName">Supplier Name</label><span class="mandatory">*</span></label>
                                 <asp:TextBox ID="txtSupplier" class="form-control" runat="server" style="float: left;width: 80%;"></asp:TextBox>
                                   <asp:Button ID="btnSupp" runat="server" Text="+" class="fa fa-plus" 
                                    style="float: left; padding: 9px;" onclick="btnSupp_Click"/>
                                   
                                   
                                   
                                   
                       <cc1:ModalPopupExtender ID="modalPop_Supp1"  runat="server" PopupControlID="Panlsupplier" TargetControlID="btnSupp"
                       CancelControlID="BtnClear" BackgroundCssClass="modalBackground" BehaviorID="Supp1_Close">
                       </cc1:ModalPopupExtender>
                       <asp:Panel ID="Panlsupplier" runat="server" CssClass="modalPopup" style="display:none" >
                          <div class="header">
                            Supplier Details
                          </div>
                          <div class="body">
				           <div class="col-md-12 headsize">
					
					        <asp:Repeater ID="Repeater_Supplier1" runat="server" EnableViewState="false">
					         <HeaderTemplate>
                             <table id="TblSupplier" class="display table">
                             <thead >
                                    <tr>
                                        <th>SupCode</th>
                                        <th>SupName</th>
                                        <th>Mode</th>
                                   </tr>
                            </thead>
                           </HeaderTemplate>
                           <ItemTemplate>
                                    <tr>
                                       <td><%# Eval("SuppCode")%></td>
                                       <td><%# Eval("SuppName")%></td>
                                       <td>
                                         <asp:LinkButton ID="btnEditEnquiry_Grid" class="btn btn-success btn-sm fa fa-pencil"  runat="server" 
                                         Text="" OnCommand="GridViewClick" CommandArgument='<%# Eval("SuppCode")%>' CommandName='<%# Eval("SuppName")%>'>
                                         </asp:LinkButton>
                                      </td>
                                   </tr>
                          </ItemTemplate>
                         <FooterTemplate></table></FooterTemplate>                                
					     </asp:Repeater>
					     </div>
					     </div>
					     <div class="footer" align="right">
					     <asp:Button ID="BtnClear" class="btn btn-rounded" runat="server" Text="Close"/>
					     </div>
					     </asp:Panel>
                                <asp:HiddenField ID="txtSuppCode1" Value="0" runat="server" />
                                <asp:RequiredFieldValidator ControlToValidate="txtSupplier" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
					        </div>
					        
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">Invoice No<span class="mandatory">*</span></label>
                                <asp:TextBox ID="txtInvoiceNo" MaxLength="30" class="form-control" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ControlToValidate="txtInvoiceNo" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator4" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
					        </div>
					</div>
				  </div>
					 
			         <div class="col-md-12">
					    <div class="row">
					    
					         <div class="form-group col-md-4">
					            <label for="exampleInputName">Invoice Date<span class="mandatory">*</span></label>
                                <asp:TextBox ID="txtInvoiceDate" MaxLength="30" class="form-control date-picker" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ControlToValidate="txtInvoiceDate" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator5" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
					        </div>
					    
					         <div class="form-group col-md-4">
					            <label for="exampleInputName">Variety Code</label>
                                <asp:TextBox ID="txtVarietyCode" class="form-control" runat="server" style="float: left;width: 80%;"></asp:TextBox> 
                                <asp:Button ID="btnVarietyCode" runat="server" Text="+" class="fa fa-plus" 
                                    style="float: left; padding: 9px;" onclick="btnVarietyCode_Click"/>
                                
                                 <cc1:ModalPopupExtender ID="modalPop_VarietyCode"  runat="server" PopupControlID="Panel5" TargetControlID="btnVarietyCode"
                       CancelControlID="BtnVarietyClose" BackgroundCssClass="modalBackground" BehaviorID="Variety_Close">
                    </cc1:ModalPopupExtender>
                                 <asp:Panel ID="Panel5" runat="server" CssClass="modalPopup" style="display:none" >
                    <div class="header">
                                            Variety Details
                                          </div>
                                          <div class="body">
				     <div class="col-md-12 headsize">
					
					   <asp:Repeater ID="Repeater_VarietyCode" runat="server" EnableViewState="false">
					    <HeaderTemplate>
                         <table id="VarietyTable" class="display table">
                         <thead >
                         <tr>
                         <th>VarietyCode</th>
                         <th>VarietyName</th>
                         <th>View</th>
                         </tr>
                         </thead>
                         </HeaderTemplate>
                         <ItemTemplate>
                          <tr>
                          <td><%# Eval("VarietyID")%></td>
                          <td><%# Eval("VarietyName")%></td>
                                                                           
                          <td>
                          <asp:LinkButton ID="btnEdit_VarietyCode" class="btn btn-success btn-sm fa fa-pencil"  runat="server" 
                            Text="" OnCommand="GridViewClick_VarietyCode" CommandArgument='<%# Eval("VarietyID")%>' CommandName='<%# Eval("VarietyName")%>'>
                          </asp:LinkButton>
                          </td>
                          </tr>
                          </ItemTemplate>
                         <FooterTemplate></table></FooterTemplate>                                
					     </asp:Repeater>
					     </div>
					     </div>
					     <div class="footer" align="right">
					     <asp:Button ID="BtnVarietyClose" class="btn btn-rounded" runat="server" Text="Close" />
					     </div>
					     </asp:Panel>
                                
                                
                                                               
					        </div>
					        
					         <div class="form-group col-md-4">
					            <label for="exampleInputName">Variety Name</label>
                                <asp:TextBox ID="txtVarietyName" class="form-control" runat="server"></asp:TextBox>
					        </div>
					        
					        </div>
					        </div>
					        
					  <div class="col-md-12">
					    <div class="row">  
					    
					        
					   
					         <div class="form-group col-md-2">
					            <label for="exampleInputName">No Of Bale</label>
                                <asp:TextBox ID="txtNoOfBales" class="form-control" runat="server" style="float: left;width: 80%;"></asp:TextBox>
					        </div>
					        
					         <div class="form-group col-md-2">
					            <label for="exampleInputName">Rate Per Candy</label>
                                <asp:TextBox ID="txtRatePerCandy" class="form-control" runat="server"></asp:TextBox>
					        </div>
					        
					         <div class="form-group col-md-2">
					            <label for="exampleInputName">Candy Weight</label>
                                <asp:TextBox ID="txtCandyweight" class="form-control" runat="server"></asp:TextBox>
					        </div>
					        
					        
					        </div>
					 </div>
		             <div class="col-md-12">
					    <div class="row">
					         <div class="form-group col-md-4">
                                <asp:Label ID="lblOthers" runat="server" Text="Details" Font-Size="Large" Font-Bold="True"></asp:Label>
					            
					        </div>
					    </div>
					 </div>
					
					        
				     <div class="col-md-12">
					    <div class="row">
					       
					        <div class="form-group col-md-3">
					            <label for="exampleInputName">Bale No</label>
                                <asp:TextBox ID="txtBaleNo" class="form-control" runat="server" style="float: left;width: 80%;"></asp:TextBox>
					              <asp:Button ID="btnBaleNo" runat="server" Text="+" class="fa fa-plus" 
                                    style="float: left; padding: 9px;" onclick="btnVarietyCode_Click"/>
                                
                                 
					       
					        </div>
					        
					        <div class="form-group col-md-3">
					            <label for="exampleInputName">Cross Weight</label>
                                <asp:TextBox ID="txtCrossWeight" class="form-control" runat="server"></asp:TextBox>
					        </div>
					        
					        <div class="form-group col-md-2">     
					            <br />
					            <asp:Button ID="btnAddVariety" class="btn btn-success"  runat="server" Text="Add" OnClick="btnAddVariety_Click"/>
					        </div> 
					        
					        </div>
					      </div>
					     
					   
					   
				   <div class="form-group row"></div>
                    <div class="form-group row"></div>
                    
					<!-- table start -->
				
					<div class="col-md-12">
					    <div class="row">
					        <asp:Repeater ID="RptWeightList" runat="server" EnableViewState="false">
			                    <HeaderTemplate>
                                    <table id="TblWeightList" class="display table">
                                        <thead>
                                            <tr>
                                                <th>S.No</th>
                                                <th>Lot.No</th>
                                              <%--<th>VCode</th>--%>
                                                <th>BaleNo</th>
                                                <th>BaleWeight</th>
                                                <th>Inv.No</th>
                                                <th>VName</th>
                                                <th>SName</th>
                                                <th>Mode</th>
                                            </tr>
                                        </thead>
                                </HeaderTemplate>
                                <ItemTemplate> 
                                    <tr>
                                        <td><%# Container.ItemIndex + 1 %></td>
                                        <td><%# Eval("Lotno")%></td>
                                         <td><%# Eval("baleno")%></td>
                                         <td><%# Eval("qun")%></td>
                                         <td><%# Eval("InvoiceNo")%></td>
                                         <td><%# Eval("VarietyName")%></td>
                                         <td><%# Eval("Supp_Name")%></td>
                                         <td>
                                            <asp:LinkButton ID="btnDeleteGrid" class="btn btn-success btn-sm fa fa-pencil"  runat="server" 
                                                Text="" OnCommand="GridDeleteClick" CommandArgument="Edit" CommandName='<%# Eval("Lotno")%>'> 
                                            </asp:LinkButton>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate></table></FooterTemplate>                                
			                </asp:Repeater>
					    </div>
					</div>

					<!-- table End -->
					<div class="form-group row">
					</div>
					   
					   
					   
					      
				     <div class="col-md-12">
					      <div class="row">
					      
					         <div class="form-group col-md-3">
					            <label for="exampleInputName">Total Weight</label>
                                <asp:TextBox ID="txtTotalWeight" class="form-control" runat="server"></asp:TextBox>
					        </div>
					      
					         <div class="form-group col-md-3">
					            <label for="exampleInputName">Invoice Weight</label>
                                <asp:TextBox ID="txtInvoiceWeight" class="form-control" runat="server"></asp:TextBox>
					        </div>
					        
					         <div class="form-group col-md-3">
					            <label for="exampleInputName">Tare Weight</label>
                                <asp:TextBox ID="txtTareWeight" class="form-control" runat="server" AutoPostBack="true"
                                     ontextchanged="txtTareWeight_TextChanged"></asp:TextBox>
					        </div>
					        
					         <div class="form-group col-md-3">
					            <label for="exampleInputName">Net Weight</label>
                                <asp:TextBox ID="txtNetWeight" class="form-control" runat="server"></asp:TextBox>
					        </div>
					      
				      
					      </div>
					  </div>
					      
					      
					       
					   
					
					
					<div class="form-group row"></div>
                    <div class="form-group row"></div>
                    
					
					<div class="form-group row">
					</div>
					<!-- Button start -->   
		            <div class="txtcenter">
                      <%--  <asp:Button ID="btnSave" class="btn btn-success"  runat="server" Text="Save" ValidationGroup="Validate_Field" OnClick="btnSave_Click"/>--%>
                        <asp:Button ID="btnSave" class="btn btn-success"  runat="server" Text="Save" OnClick="btnSave_Click"/>
                        <asp:Button ID="btnCancel" class="btn btn-danger" runat="server" Text="Cancel" OnClick="btnCancel_Click"/>
                        <asp:Button ID="btnBackEnquiry" class="btn btn-success" runat="server" Text="Back" OnClick="btnBackEnquiry_Click"/>
                    </div>
                    <!-- Button end -->					
				</div><!-- panel body end -->
				</form>
				
					
			</div><!-- panel white end -->
			</div><!-- col-9 end -->
		    <!-- Dashboard start -->
		                <div class="col-lg-3 col-md-6">
                            <div class="panel panel-white" style="height: 100%;">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Dashboard Details</h4>
                                    <div class="panel-control">
                                     </div>
                                </div>
                                <div class="panel-body">
                               </div>
                            </div>
                        </div>  
                        
                         <div class="col-lg-3 col-md-6">
                            <div class="panel panel-white" style="height: 100%;">
                            <div align="center">
                             <asp:Label ID="lbllabel" runat="server" Text="Add Items" Font-Bold="True" 
                                    Font-Size="Large"></asp:Label>
                             </div>
                             
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate> 
                
                                 <asp:Panel ID="GVPanel" runat="server" ScrollBars="None" Visible="true">
				                <asp:GridView id="GVModule" runat="server" AutoGenerateColumns="false" 
				                ClientIDMode="Static" class="gvv display table">
				                    <Columns>
				                      <asp:TemplateField  HeaderText="Lotno">
				                            <ItemTemplate>
				                                <asp:Label id="Lotno" runat="server" Text='<%# Eval("Lotno") %>'/>
				                            </ItemTemplate>
				                        </asp:TemplateField>
				                        <asp:TemplateField  HeaderText="BaleNo">
				                            <ItemTemplate>
				                                <asp:Label id="BaleNo" runat="server" Text='<%# Eval("BaleNo") %>'/>
				                            </ItemTemplate>
				                        </asp:TemplateField>
				                         <asp:TemplateField  HeaderText="Qty">
				                            <ItemTemplate>
				                                <asp:Label id="Qty" runat="server" Text='<%# Eval("BaleWeight") %>'/>
				                            </ItemTemplate>
				                        </asp:TemplateField>
				                        <asp:TemplateField  HeaderText="Add">
				                            <ItemTemplate>
				                                
				                                <asp:CheckBox id="ChkboxLotNo" runat="server"/>
				                            </ItemTemplate>
				                        </asp:TemplateField>
				                    
                                     
				                      
				                   
				                                  
				                    </Columns>
				                </asp:GridView>
				            </asp:Panel>
				            
				              </ContentTemplate>
				           </asp:UpdatePanel>
 
   
			                <div align="center">
                                <asp:Button ID="BtnSubmit" class="btn btn-success" runat="server" Text="Submit" OnClick="BtnSubmit_Click" />
					        </div>
					    </div>
                        </div> 
		    <!-- Dashboard End -->
		    <div class="col-md-2"></div>
		    <div class="col-md-2"></div>
            
      
     
  </div> <!-- col 12 end -->
 </div><!-- row end -->
  </div><!-- main-wrapper end -->
   
   
   
  
</asp:Content>


