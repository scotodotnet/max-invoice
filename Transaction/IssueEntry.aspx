﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="IssueEntry.aspx.cs" Inherits="Transaction_IssueEntry" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<%--<script src='<%= ResolveUrl("../assets/js/master_list_jquery.min.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/master_list_jquery-ui.min.js") %>'></script>
<link href="<%= ResolveUrl("../assets/css/master_list_jquery-ui.css") %>" rel="stylesheet" type="text/css"/>
<link href="<%= ResolveUrl("../assets/css/Popup.css") %>" rel="stylesheet" type="text/css"/>  

--%>
    <style>
.add-on .input-group-btn > .btn {
    border-left-width: 0;
    left:-2px;
    -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
}
/* stop the glowing blue shadow */
.add-on .form-control:focus {
    -webkit-box-shadow: none; 
            box-shadow: none;
    border-color:#cccccc; 
}
</style>

    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function(sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    $('#TableIssueEntry').dataTable();
                }
            });
        };
   </script>

<script type="text/javascript">
    function SaveMsgAlert(msg) {
        swal(msg);
    }
</script>


 <script src='<%= ResolveUrl("../assets/js/master_list_jquery.min.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/master_list_jquery-ui.min.js") %>'></script>
<link href="<%= ResolveUrl("../assets/css/master_list_jquery-ui.css") %>" rel="stylesheet" type="text/css"/>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function(sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    $('#TblWeightList').dataTable();
                }
            });
        };
   </script>
   
    <script>
        $(document).ready(function() {
            $('#TblWeightList').dataTable();
        });
    </script>  
    
    

   <script type="text/javascript">
       //On UpdatePanel Refresh
       var prm = Sys.WebForms.PageRequestManager.getInstance();
       if (prm != null) {
           prm.add_endRequest(function(sender, e) {
               if (sender._postBackSettings.panelsToUpdate != null) {
                   $('.date-picker').datepicker({ format: 'dd/mm/yyyy' });
                   $('.js-states').select2();
               }
           });
       };
</script>



<script src='<%= ResolveUrl("../assets/js/master_list_jquery.min.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/master_list_jquery-ui.min.js") %>'></script>
<link href="<%= ResolveUrl("../assets/css/master_list_jquery-ui.css") %>" rel="stylesheet" type="text/css"/>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function(sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    $('#TblLotNo').dataTable();
                }
            });
        };
   </script>
   
   
    <script>
        $(document).ready(function() {
            $('#TblLotNo').dataTable();
        });
    </script> 
    
   <link href="<%= ResolveUrl("../assets/css/Popup.css") %>" rel="stylesheet" type="text/css"/>
    <script>
        function pageLoad(sender, args) {
            if (!args.get_isPartialLoad()) {
                //  add our handler to the document's
                //  keydown event
                $addHandler(document, "keydown", onKeyDown);
            }
        }

        function onKeyDown(e) {
            if (e && e.keyCode == Sys.UI.Key.esc) {
                // if the key pressed is the escape key, dismiss the dialog
                $find('Supp1_Close').hide();

                $find('Variety_Close').hide();
                $find('LotNo_Close').hide();

            }
        } 
 </script>



<div class="page-breadcrumb">
                    <ol class="breadcrumb container">
                   <h4><li class="active">Issue Entry</li></h4> 
                    </ol>
                </div>
<div id="main-wrapper" class="container">
 <div class="row">
    <div class="col-md-12">
        <div class="col-md-12">
			<div class="panel panel-white">
			<div class="panel panel-primary">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">Issue Entry</h4>
				</div>
				</div>
				<form class="form-horizontal">
				
				<div class="panel-body">
				
				    <div align="right">
					       <div class="row">
				            <asp:Button ID="btnBack" class="btn btn-primary btn-rounded btn-sm"  runat="server" Text="Back" OnClick="btnBackEnquiry_Click"/>
				           </div>
				      </div>
				
					<div class="col-md-12">
					    <div class="row">
					    
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">Issue.Entry.No<span class="mandatory">*</span></label>
					            <asp:Label ID="txtIssueEntryNo" runat="server" class="form-control" BackColor="#F3F3F3"></asp:Label>
					        </div>
					        
					        <div class="form-group col-md-4">
					          <label for="exampleInputName">Unit<span class="mandatory">*</span></label>
                                <asp:DropDownList ID="ddlUnit" class="form-control" runat="server">
                                </asp:DropDownList>
					        </div>
					        
					        <div class="form-group col-md-3">
					            <label for="exampleInputName">Issue.Entry.Date<span class="mandatory">*</span></label>
                                 <div class="input-group m-b-sm" style="width: 230px;padding-left: 0px; float:left"> <span class="input-group-addon" id="Span3" style="float: left; padding: 9px; width: auto;"><i class="fa fa-calendar"></i></span>
                                 <asp:TextBox ID="txtIssueEntryDate" MaxLength="30" class="form-control date-picker" runat="server" style="float: left;width: 66%;"></asp:TextBox>
                                <asp:RequiredFieldValidator ControlToValidate="txtIssueEntryDate" ValidationGroup="Validate_Field" class="form_error" ID="validate1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
					        </div>
					        </div>
					        
                            
					        
					        </div>
					 </div>
					        
				    <div class="col-md-12">
					    <div class="row">      
					    <div class="form-group col-md-3">
					            <label for="exampleInputName">Yarn Lot No<span class="mandatory">*</span></label>
                                <asp:TextBox ID="txtLotNo" class="form-control" runat="server"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" FilterType="Numbers" TargetControlID="txtLotNo"/>
					        </div> 
					        
					  
					        	    <div class="form-group col-md-3">
					            
                                <label for="exampleInputName">Color<span class="mandatory">*</span></label>
					            <asp:DropDownList ID="ddlColor" runat="server" class="form-control">
                                </asp:DropDownList>
					 </div>
					</div>
				  </div>
				  <div class="clearfix"></div>				    
			        <div class="timeline-options">
			          <h4 class="panel-title">Item Issues</h4>
			        </div>
				  
				  <div class="col-md-12">
				     <div class="row">
				     <div class="form-group col-md-3">
					            <label for="exampleInputName">Lot No<span class="mandatory">*</span></label>
                         <asp:DropDownList ID="ddlLotNo" runat="server" class="form-control" AutoPostBack="true"
                                    onselectedindexchanged="ddlLotNo_SelectedIndexChanged">
                         </asp:DropDownList>
                        
					 </div>
				
					   <div class="form-group col-md-2">
					            <label for="exampleInputName">%<span class="mandatory">*</span></label>
                                <asp:TextBox ID="txtPercent" runat="server" class="form-control" Text="0" 
                                    AutoPostBack="true" ontextchanged="txtPercent_TextChanged"></asp:TextBox>
                                    
					 </div>
					   <div class="form-group col-md-3">
					            <label for="exampleInputName">Bale Weight<span class="mandatory">*</span></label>
                                <asp:TextBox ID="txtTotalWeight" runat="server" class="form-control"></asp:TextBox>
					 </div>
					  <div class="form-group col-md-3">
					            <label for="exampleInputName">No of Bale<span class="mandatory">*</span></label>
                                <asp:TextBox ID="txtNoofBale" runat="server" class="form-control" Text="0" Enabled="false"></asp:TextBox>
					 </div>
					
				     </div>
				     <div class="row">
				       <div class="form-group col-md-3">
					            <label for="exampleInputName">Color Name</label>
                                <asp:TextBox ID="txtTempColor" runat="server" class="form-control" Text="0" Enabled="false"></asp:TextBox>
					 </div>
					   <div class="form-group col-md-3">
					            <label for="exampleInputName">Rate</label>
                                <asp:TextBox ID="txtTempRate" runat="server" class="form-control" Text="0" Enabled="false"></asp:TextBox>
					 </div>
				      <div class="form-group col-md-1">
					            <br />
					            <asp:Button ID="btnAddItem" Width="50" Height="30" class="btn-success"  
                                    runat="server" Text="Add" ValidationGroup="Item_Validate_Field" onclick="btnAddItem_Click" 
                                     />
                                     
					        </div>
				     </div>
				  </div>
				  
				  <div class="col-md-12">
				    <div class="row">
				    
				     
				    
					  
					
					 
				    </div>
				  </div>
		
		                <%--<div class="col-md-12">
					    <div class="row">
					        <div class="form-group col-md-4">
                                <asp:Label ID="lblOthers" runat="server" Text="Details" Font-Size="Large" Font-Bold="True"></asp:Label>
					            
					        </div>
					    </div>
					 </div>
					
				
			        	<div class="col-md-12">
					    <div class="row">
					        
					        <div class="form-group col-md-2">
					            <label for="exampleInputName">SI.No</label>
                                <asp:TextBox ID="txtSNo" class="form-control" runat="server"></asp:TextBox>
					        </div>
					        
					        
					        
					         <div class="form-group col-md-3">
					            <label for="exampleInputName">Bale No</label>
                                <asp:TextBox ID="txtBaleNo" class="form-control" runat="server" style="float: left;width: 80%;"></asp:TextBox>
					              <asp:Button ID="btnBaleNo" runat="server" Text="+" class="fa fa-plus" 
                                    style="float: left; padding: 9px;"/>
                                
                                 <cc1:ModalPopupExtender ID="ModalPopupExtender1"  runat="server" PopupControlID="PanelBaleNo" TargetControlID="btnBaleNo"
                                 CancelControlID="BtnVariteyClose" BackgroundCssClass="modalBackground" BehaviorID="Varitey_Close">
                                </cc1:ModalPopupExtender>
                                <asp:Panel ID="PanelBaleNo" runat="server" CssClass="modalPopup" style="display:none" >
                                <div class="header">
                                    Varitey Details
                                 </div>
                                 <div class="body">
				                 <div class="col-md-12 headsize">
					
					             <asp:Repeater ID="Repeater2" runat="server" EnableViewState="false">
					             <HeaderTemplate>
                                 <table id="VariteyTable" class="display table">
                                 <thead >
                                 <tr>
                                     <th>VariteyCode</th>
                                     <th>VariteyName</th>
                                     <th>View</th>
                                 </tr>
                                </thead>
                               </HeaderTemplate>
                               <ItemTemplate>
                               <tr>
                                  <td><%# Eval("VariteyCode")%></td>
                                  <td><%# Eval("VariteyName")%></td>
                                                                           
                                <td>
                                   <asp:LinkButton ID="btnEdit_VariteyCode" class="btn btn-success btn-sm fa fa-pencil"  runat="server" 
                                    Text="" OnCommand="GridViewClick_VariteyCode" CommandArgument='<%# Eval("VariteyCode")%>' CommandName='<%# Eval("VariteyName")%>'>
                                   </asp:LinkButton>
                                 </td>
                                 </tr>
                                 </ItemTemplate>
                                  <FooterTemplate></table></FooterTemplate>                                
					             </asp:Repeater>
					             </div>
					            </div>
					             <div class="footer" align="right">
					             <asp:Button ID="Button2" class="btn btn-rounded" runat="server" Text="Close" />
					            </div>
					            </asp:Panel>
					       
					        </div>
					        
					         <div class="form-group col-md-2">
					            <label for="exampleInputName">Qty</label>
                                <asp:TextBox ID="txtQty" class="form-control" runat="server"></asp:TextBox>
					        </div>
					        
					         <div class="form-group col-md-2">
					              <br />
					             <asp:Button ID="btnAddVaritey" class="btn btn-success"  runat="server" Text="Add" OnClick="btnAddVaritey_Click"/>

					        </div>
					        
					        </div>
					 </div>--%>
					        
					
					<div class="form-group row"></div>
                    <div class="form-group row"></div>
                    
					<!-- table start -->
					<div class="col-md-12">
					    <div class="row">
					        <asp:Repeater ID="RptIssueEntry" runat="server" EnableViewState="false">
			                    <HeaderTemplate>
                                    <table id="TblWeightList" class="display table">
                                        <thead>
                                            <tr>
                                                <th>S.No</th>
                                                <th>LNo</th>
                                                <th>Inv.No</th>
                                                <th>LDate</th>
                                                <th>BaleNo</th>
                                                <th>BWeight</th>
                                                <th>Color</th>
                                                <th>VName</th>
                                                <th>SName</th>
                                                <th>Mode</th>
                                             
                                            </tr>
                                        </thead>
                                </HeaderTemplate>
                                <ItemTemplate> 
                                    <tr>
                                         <td><%# Container.ItemIndex + 1 %></td>
                                          <td><%# Eval("Lotno")%></td>
                                          <td><%# Eval("InvoiceNo")%></td>
                                          <td><%# Eval("LotDate")%></td>
                                          <td><%# Eval("baleno")%></td>
                                          <td><%# Eval("BaleWeight")%></td>
                                          <td><%# Eval("ColorName")%></td>
                                          <td><%# Eval("VarietyName")%><%# Eval("VarietyCode")%></td>
                                          <td><%# Eval("Supp_Name")%><%# Eval("Supp_Code")%></td>
                                          <td>
                                                   <%--  <asp:LinkButton ID="btnDeleteEnquiry_Grid" class="btn btn-danger btn-sm fa fa-trash-o"  runat="server" 
                                                        Text="" OnCommand="GridDeleteCottonInwardsClick" CommandArgument="Delete" CommandName='<%# Eval("BaleNo")%>' 
                                                        CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this Cotton Inwards details?');">
                                                    </asp:LinkButton>--%>
                                                    
                                         <%-- <asp:LinkButton ID="btnEdit_VarietyCode" class="btn btn-danger btn-sm fa fa-trash-o"  runat="server" 
                                           Text="" OnCommand="GridViewClick_VarietyCode" CommandArgument='<%# Eval("Lotno")%>' 
                                           CommandName='<%# Eval("InvoiceNo")%>'>
                                          </asp:LinkButton>  --%>    
                                                    
                                           <asp:LinkButton ID="btnDeleteGrid" class="btn btn-danger btn-sm fa fa-trash-o"  runat="server" 
                                                Text="" OnCommand="GridDeleteClick" CommandArgument="Edit" CommandName='<%# Eval("baleno")%>'> 
                                            </asp:LinkButton>          
                                                    
                                        </td>
                                        
                                        
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate></table></FooterTemplate>                                
			                </asp:Repeater>
					    </div>
					</div>
					<!-- table End -->
					
									
					
					
					
					
					
					
					
					<div class="form-group row">
					<div class="form-group col-md-3"></div>
					<div class="form-group col-md-3"></div>
					<div class="form-group col-md-3"></div>
					   <div class="form-group col-md-3">
					            <label for="exampleInputName">Total Weight</label>
                                <asp:TextBox ID="txtNetWeight" runat="server" class="form-control" Enabled="false"></asp:TextBox>
					 </div>
					</div>
					<!-- Button start -->   
		            <div class="txtcenter">
                        <asp:Button ID="btnSave" class="btn btn-success"  runat="server" Text="Save" ValidationGroup="Validate_Field" OnClick="btnSave_Click"/>
                        <asp:Button ID="btnCancel" class="btn btn-danger" runat="server" Text="Cancel" OnClick="btnCancel_Click"/>
                        <asp:Button ID="btnBackEnquiry" class="btn btn-success" runat="server" Text="Back" OnClick="btnBackEnquiry_Click"/>
                         <asp:Button ID="btnApprove" class="btn btn-success" runat="server" Text="Approve" OnClick="btnApprove_Click"/>
                    </div>
                    <!-- Button end -->					
				</div><!-- panel body end -->
				</form>
			</div><!-- panel white end -->
			</div><!-- col-9 end -->
		 
		    <div class="col-md-2"></div>
		    <div class="col-md-2"></div>
            
      
     
  </div> <!-- col 12 end -->
 </div><!-- row end -->
  </div><!-- main-wrapper end -->
 
  
 
</asp:Content>
