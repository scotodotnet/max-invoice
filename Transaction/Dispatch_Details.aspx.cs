﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
public partial class Transaction_Dispatch_Details : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionUnplanReceiptNo;
    string SessionFinYearCode;
    string SessionFinYearVal;
    static string sum;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "ERP Module :: Yarn Production";
            HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Transaction"));
            li.Attributes.Add("class", "droplink active open");
            Initial_Data_Referesh();
            Load_Customer();
            Load_GoDown();
            Load_Count();
            Load_Color();
            if (Session["Trans_No"] == null)
            {
                SessionUnplanReceiptNo = "";
            }
            else
            {
                SessionUnplanReceiptNo = Session["Trans_No"].ToString();
                txtReceiptNo.Text = SessionUnplanReceiptNo;
                btnSearch_Click(sender, e);
            }
        }
        Load_OLD_data();
    }
 
    private void Load_GoDown()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlGodown.Items.Clear();
        query = "Select * from MstGoDown where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlGodown.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["GoDownName"] = "-Select-";
        dr["GoDownID"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlGodown.DataTextField = "GoDownName";
        ddlGodown.DataValueField = "GoDownID";
        ddlGodown.DataBind();
    }
    private void Load_Count()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlCount.Items.Clear();
        query = "Select * from MstCount where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlCount.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["CountName"] = "-Select-";
        dr["CountID"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlCount.DataTextField = "CountName";
        ddlCount.DataValueField = "CountID";
        ddlCount.DataBind();
    }

    private void Load_Customer()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlCustomer.Items.Clear();
        query = "Select * from MstCustomer where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlCustomer.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["CustomerName"] = "-Select-";
        dr["CustomerID"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlCustomer.DataTextField = "CustomerName";
        ddlCustomer.DataValueField = "CustomerID";
        ddlCustomer.DataBind();
    }

    private void Load_Color()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlColor.Items.Clear();
        query = "Select * from MstColor where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlColor.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["ColorName"] = "-Select-";
        dr["ColorName"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlColor.DataTextField = "ColorName";
        ddlColor.DataValueField = "ColorName";
        ddlColor.DataBind();
    }

 
   


    protected void btnSearch_Click(object sender, EventArgs e)
    {
        //Search Purchase Enquiry
        string query = "";
        DataTable Main_DT = new DataTable();
        query = "Select * from Dispatch_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Trans_No='" + txtReceiptNo.Text + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        if (Main_DT.Rows.Count != 0)
        {
            txtDate.Text = Main_DT.Rows[0]["Trans_Date"].ToString();
            ddlGodown.SelectedValue = Main_DT.Rows[0]["GoDownID"].ToString();
            ddlCustomer.SelectedValue = Main_DT.Rows[0]["CustomerID"].ToString();
            txtAddress.Text = Main_DT.Rows[0]["Address1"].ToString();
            txtMobile.Text = Main_DT.Rows[0]["MobileNo"].ToString();
            txtRemarks.Text = Main_DT.Rows[0]["Remarks"].ToString();
            //txtSupplierName.SelectedValue = Main_DT.Rows[0]["PrepaidBy"].ToString().Trim();
            //ddlUnit.SelectedValue = Main_DT.Rows[0]["Unit"].ToString().Trim();

            txtVehicleNo.Text = Main_DT.Rows[0]["VehicleNo"].ToString();
            txtFrom.Text = Main_DT.Rows[0]["FromPlace"].ToString();
            txtTo.Text = Main_DT.Rows[0]["ToPlace"].ToString();
            //txtRemarks.Text = Main_DT.Rows[0]["Reason"].ToString();
            txtNote.Text = Main_DT.Rows[0]["Note"].ToString();
          


            //txtRouter.Text = Main_DT.Rows[0]["RouterWaste"].ToString();

            // txtStopPage.Text = Main_DT.Rows[0]["StoppageMins"].ToString();
            //txtRemarks.Text = Main_DT.Rows[0]["StoppageReason"].ToString();

            //Unplanned_Receipt_Main_Sub Table Load
            DataTable dt = new DataTable();
            query = "Select * from Dispatch_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Trans_No='" + txtReceiptNo.Text + "'";
            dt = objdata.RptEmployeeMultipleDetails(query);
            ViewState["ItemTable"] = dt;
            Repeater1.DataSource = dt;
            Repeater1.DataBind();

            btnSave.Text = "Update";
        }
    }
    protected void GridDeleteClick(object sender, CommandEventArgs e)
    {
        DataTable qry_dt = new DataTable();
        bool ErrFlag = false;
        string qry = "";


        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            if ((dt.Rows[i]["CountName"].ToString().ToUpper() == ddlCount.SelectedItem.Text.ToString().ToUpper()) && (dt.Rows[i]["LotNo"].ToString().ToUpper() == txtLotNo.ToString().ToUpper()))
            {
                dt.Rows.RemoveAt(i);
                dt.AcceptChanges();
            }
        }
        ViewState["ItemTable"] = dt;
        Load_OLD_data();

    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        string query = "";
        DataTable DT_Check = new DataTable();
        DataTable qry_dt = new DataTable();
        string SaveMode = "Insert";
        bool ErrFlag = false;

        //check with Item Details Add with Grid
        DT_Check = (DataTable)ViewState["ItemTable"];
        if (DT_Check.Rows.Count == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You have to add atleast one Item Details..');", true);
        }

        //User Rights Check Start
        bool Rights_Check = false;
        //if (btnSave.Text == "Update")
        //{
        //    Rights_Check = CommonClass_Function.ModifyRights_Check(SessionCcode, SessionLcode, SessionUserID, "7", "2", "Unplanned Receipt");
        //    if (Rights_Check == false)
        //    {
        //        ErrFlag = true;
        //        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Modify Unplanned Receipt Details..');", true);
        //    }
        //}
        //else
        //{
        //    Rights_Check = CommonClass_Function.AddRights_Check(SessionCcode, SessionLcode, SessionUserID, "7", "2", "Unplanned Receipt");
        //    if (Rights_Check == false)
        //    {
        //        ErrFlag = true;
        //        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Adding New Unplanned Receipt..');", true);
        //    }
        //}
        //User Rights Check End

        //Auto generate Transaction Function Call
        if (btnSave.Text != "Update")
        {
            if (!ErrFlag)
            {
                TransactionNoGenerate TransNO = new TransactionNoGenerate();
                string Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup(SessionCcode, SessionLcode, "Dispatch", SessionFinYearVal);
                if (Auto_Transaction_No == "")
                {
                    ErrFlag = true;
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Auto Generate Transaction No Error... Contact Server Admin');", true);
                }
                else
                {
                    txtReceiptNo.Text = Auto_Transaction_No;
                }
            }
        }

        //Check With Already Approved Start
        DataTable DT_Check_Apprv = new DataTable();
        query = "Select * from Dispatch_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Trans_No='" + txtReceiptNo.Text + "' And Status='1'";
        DT_Check_Apprv = objdata.RptEmployeeMultipleDetails(query);
        if (DT_Check_Apprv.Rows.Count != 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Do not Update Unplanned Receipt Details Already Approved..');", true);
        }

        if (!ErrFlag)
        {
            query = "Select * from Dispatch_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Trans_No='" + txtReceiptNo.Text + "'";
            DT_Check = objdata.RptEmployeeMultipleDetails(query);
            if (DT_Check.Rows.Count != 0)
            {
                SaveMode = "Update";
                query = "Delete from Dispatch_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Trans_No='" + txtReceiptNo.Text + "'";
                objdata.RptEmployeeMultipleDetails(query);
                query = "Delete from Dispatch_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Trans_No='" + txtReceiptNo.Text + "'";
                objdata.RptEmployeeMultipleDetails(query);
            }

            //Insert Main Table
            query = "Insert Into Dispatch_Main(Ccode,Lcode,FinYearCode,FinYearVal,Trans_No,Trans_Date,CustomerID,CustomerName,Address1,MobileNo,VehicleNo,FromPlace,ToPlace,GoDownID,GoDownName,Remarks,Note,UserID,UserName,Status) Values('" + SessionCcode + "',";
            query = query + " '" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + txtReceiptNo.Text + "','" + txtDate.Text + "','" + ddlCustomer.SelectedValue + "','" + ddlCustomer.SelectedItem.Text + "','" + txtAddress.Text + "',";
            query = query + "'" + txtMobile.Text + "','" + txtVehicleNo.Text + "','" + txtFrom.Text + "','" + txtTo.Text + "',";
            query = query + " '" + ddlGodown.SelectedValue + "','" + ddlGodown.SelectedItem.Text + "','" + txtRemarks.Text + "','" + txtNote.Text + "','" + SessionUserID + "','" + SessionUserName + "','0')";
            objdata.RptEmployeeMultipleDetails(query);

            DataTable dt = new DataTable();
            dt = (DataTable)ViewState["ItemTable"];
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                query = "Insert Into Dispatch_Sub(Ccode,Lcode,FinYearCode,FinYearVal,Trans_No,Trans_Date,CountID,CountName,LotNo,Color,ShadeNo,NoofBag,";
                query = query + "BagKG,Qty,Rate,Amount,UserID,UserName) Values('" + SessionCcode + "',";
                query = query + " '" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + txtReceiptNo.Text + "','" + txtDate.Text + "',";
                query = query + " '" + dt.Rows[i]["CountID"].ToString() + "','" + dt.Rows[i]["CountName"].ToString() + "','" + dt.Rows[i]["LotNo"].ToString() + "','" + dt.Rows[i]["Color"].ToString() + "','" + dt.Rows[i]["ShadeNo"].ToString() + "','" + dt.Rows[i]["NoofBag"].ToString() + "',";
                query = query + " '" + dt.Rows[i]["BagKG"].ToString() + "','" + dt.Rows[i]["Qty"].ToString() + "','" + dt.Rows[i]["Rate"].ToString() + "','" + dt.Rows[i]["Amount"].ToString() + "','" + SessionUserID + "','" + SessionUserName + "')";
                objdata.RptEmployeeMultipleDetails(query);
            }

            if (SaveMode == "Insert")
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Dispatch Details Saved Successfully');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Dispatch Details Updated Successfully');", true);
            }
            //Clear_All_Field();
            Session["Trans_No"] = txtReceiptNo.Text;
            btnSave.Text = "Update";
            //Load_Data_Enquiry_Grid();


        }

    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {

    }


    protected void btnBackEnquiry_Click(object sender, EventArgs e)
    {
        Session.Remove("Trans_No");
        Response.Redirect("Packing_Main.aspx");
    }

    protected void btnApprove_Click(object sender, EventArgs e)
    {
        string query = "";
        DataTable DT_Check = new DataTable();
        bool ErrFlag = false;

        //User Rights Check Start
        bool Rights_Check = false;
        //Rights_Check = CommonClass_Function.ApproveRights_Check(SessionCcode, SessionLcode, SessionUserID, "7", "2", "Unplanned Receipt");
        //if (Rights_Check == false)
        //{
        //    ErrFlag = true;
        //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Approve Unplanned Receipt...');", true);
        //}
        //User Rights Check End

        query = "Select * from Dispatch_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Trans_No='" + txtReceiptNo.Text + "'";
        DT_Check = objdata.RptEmployeeMultipleDetails(query);
        if (DT_Check.Rows.Count == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('First You have to Register this Service Receipt Details..');", true);
        }

        query = "Select * from Dispatch_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Trans_No='" + txtReceiptNo.Text + "' And Status='1'";
        DT_Check = objdata.RptEmployeeMultipleDetails(query);
        if (DT_Check.Rows.Count != 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Service Receipt Details Already Approved..');", true);
        }
        if (!ErrFlag)
        {
            query = "Update Dispatch_Main set Status='1' where Ccode='" + SessionCcode + "'";
            query = query + "  And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
            query = query + "  And Trans_No='" + txtReceiptNo.Text + "'";
            objdata.RptEmployeeMultipleDetails(query);

            //Stock_Add();
            DataTable dt = new DataTable();
            DataTable dt_Find = new DataTable();
            dt = (DataTable)ViewState["ItemTable"];
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                query = "select * from Yarn_Stock_Ledger_All where Trans_No='" + txtReceiptNo.Text + "' and CountID='" + dt.Rows[i]["CountID"].ToString() + "' and LotNo='" + dt.Rows[i]["LotNo"].ToString() + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                dt_Find = objdata.RptEmployeeMultipleDetails(query);
                if (dt_Find.Rows.Count > 0)
                {
                    query = "delete from Yarn_Stock_Ledger_All where Trans_No='" + txtReceiptNo.Text + "' and CountID='" + dt.Rows[i]["CountID"].ToString() + "' and LotNo='" + dt.Rows[i]["LotNo"].ToString() + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                    objdata.RptEmployeeMultipleDetails(query);
                }

                query = "insert into Yarn_Stock_Ledger_All(Ccode,Lcode,FinYearCode,FinYearVal,Trans_No,Trans_Date,Trans_Date_Str,Trans_Type,LotNo,CountID,";
                query = query + "CountName,Color,ShadeNo,Add_Qty,Add_Bag,Minus_Qty,Minus_Bag,GoDownID,GoDownName,UserID,UserName) Values('" + SessionCcode + "',";
                query = query + " '" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + txtReceiptNo.Text + "','" + Convert.ToDateTime(txtDate.Text).ToString("yyyy/MM/dd") + "','" + txtDate.Text + "','Dispatch',";
                query = query + " '" + dt.Rows[i]["LotNo"].ToString() + "','" + dt.Rows[i]["CountID"].ToString() + "','" + dt.Rows[i]["CountName"].ToString() + "','" + dt.Rows[i]["Color"].ToString() + "','" + dt.Rows[i]["ShadeNo"].ToString() + "',";
                query = query + " '0','0','" + dt.Rows[i]["Qty"].ToString() + "','" + dt.Rows[i]["NoofBag"].ToString() + "',";
                query = query + " '" + ddlGodown.SelectedValue + "','" + ddlGodown.SelectedItem.Text + "','" + SessionUserID + "','" + SessionUserName + "')";
                objdata.RptEmployeeMultipleDetails(query);

            }


            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Dispatch Receipt Details Approved Successfully..');", true);
        }
        Response.Redirect("Dispatch_Main.aspx");

    }
    private void Load_OLD_data()
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        Repeater1.DataSource = dt;
        Repeater1.DataBind();
    }

    private void Initial_Data_Referesh()
    {
        DataTable dt = new DataTable();

        dt.Columns.Add(new DataColumn("CountID", typeof(string)));
        dt.Columns.Add(new DataColumn("CountName", typeof(string)));
        dt.Columns.Add(new DataColumn("LotNo", typeof(string)));
        dt.Columns.Add(new DataColumn("Color", typeof(string)));
        dt.Columns.Add(new DataColumn("ShadeNo", typeof(string)));
        dt.Columns.Add(new DataColumn("NoofBag", typeof(string)));
        dt.Columns.Add(new DataColumn("BagKG", typeof(string)));
        dt.Columns.Add(new DataColumn("Qty", typeof(string)));
        dt.Columns.Add(new DataColumn("Rate", typeof(string)));
        dt.Columns.Add(new DataColumn("Amount", typeof(string)));



        Repeater1.DataSource = dt;
        Repeater1.DataBind();
        ViewState["ItemTable"] = Repeater1.DataSource;

        //dt = Repeater1.DataSource;
    }
    protected void btnAddItem_Click(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        DataTable qry_dt = new DataTable();
        bool ErrFlag = false;
        DataRow dr = null;
        string query = "";

        //if (txtActProd.Text == "0.0" || txtActProd.Text == "0" || txtActProd.Text == "")
        //{
        //    ErrFlag = true;
        //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the Actual Production...');", true);
        //}

        if (!ErrFlag)
        {
            // check view state is not null  
            if (ViewState["ItemTable"] != null)
            {
                //get datatable from view state   
                dt = (DataTable)ViewState["ItemTable"];

                //check Item Already add or not
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if ((dt.Rows[i]["CountID"].ToString().ToUpper() == ddlCount.SelectedValue.ToString().ToUpper()) && (dt.Rows[i]["LotNo"].ToString().ToUpper() == txtLotNo.Text.ToString().ToUpper()))
                    {
                        ErrFlag = true;
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('This Count Already Added..');", true);
                    }

                }
                if (!ErrFlag)
                {
                    dr = dt.NewRow();

                    dr["CountID"] = ddlCount.SelectedValue;
                    dr["CountName"] = ddlCount.SelectedItem.Text;
                    dr["LotNo"] = txtLotNo.Text;
                    dr["Color"] = ddlColor.SelectedValue;
                    dr["ShadeNo"] = ddlShade.SelectedValue;
                    dr["NoofBag"] = txtBags.Text;
                    dr["BagKG"] = txtBagKG.Text;
                    dr["Qty"] = txtQty.Text;
                    dr["Rate"] = txtRate.Text;
                    //dr["Rate"] = txtRate.Text;
                    dr["Amount"] = (Convert.ToDecimal(txtQty.Text) * Convert.ToDecimal(txtRate.Text));

                    dt.Rows.Add(dr);
                    //ViewState["ItemTable"] = dt;
                    Repeater1.DataSource = dt;
                    Repeater1.DataBind();


                    ddlCount.SelectedValue = "-Select-";

                    txtLotNo.Text = "0";
                    txtQty.Text = "0";
                    txtBags.Text = "0";
                    txtRate.Text = "0";
                    //txtRate.Text = "0";
                    //txtBagKG.Text = "50";
                }
            }
        }
    }

    protected void ddlCount_SelectedIndexChanged(object sender, EventArgs e)
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlShade.Items.Clear();
        query = "Select ShadeNo from MstCount where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' and CountID='" + ddlCount.SelectedValue + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlShade.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["ShadeNo"] = "-Select-";
        dr["ShadeNo"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlShade.DataTextField = "ShadeNo";
        ddlShade.DataValueField = "ShadeNo";
        ddlShade.DataBind();
    }
    public void Add_Qty()
    {
        string NoofBag = "";
        string BagKG = "";


        if (txtBags.Text == "")
        {
            NoofBag = "0";
        }
        else
        {
            NoofBag = txtBags.Text;
        }
        if (txtBagKG.Text == "0")
        {
            BagKG = "0";
        }
        else
        {
            BagKG = txtBagKG.Text;
        }
        txtQty.Text = (Convert.ToDecimal(NoofBag) * Convert.ToDecimal(BagKG)).ToString();
    }
    protected void txtBags_TextChanged(object sender, EventArgs e)
    {
        Add_Qty();
    }
    protected void txtBagKG_TextChanged(object sender, EventArgs e)
    {
        Add_Qty();
    }


    protected void ddlCustomer_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        string Query = "select CustomerID,CustomerName,Address1,MobileNo from MstCustomer where CustomerID='" + ddlCustomer.SelectedValue + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
        dt = objdata.RptEmployeeMultipleDetails(Query);
        if (dt.Rows.Count > 0)
        {
            txtAddress.Text = dt.Rows[0]["Address1"].ToString();
            txtMobile.Text = dt.Rows[0]["MobileNo"].ToString();

        }
        else
        {
            txtAddress.Text = "";
            txtMobile.Text = "";
        }
    }
}
