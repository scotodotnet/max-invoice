﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;


public partial class Transaction_Waste_Sub : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionEnquiryNo;
    string SessionFinYearCode;
    string SessionFinYearVal;
    string SessionTrans_No;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "ERP Module :: Yarn Delivery";
            HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Transaction"));
            li.Attributes.Add("class", "droplink active open");
            Initial_Data_Referesh();

            Load_Shift();

            if (Session["Trans_No"] == null)
            {
                SessionTrans_No = "";
            }
            else
            {
                SessionTrans_No = Session["Trans_No"].ToString();
                txtTransNo.Text = SessionTrans_No;
                btnSearch_Click(sender, e);
            }

        }
        Load_OLD_data();
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        //Search Gate_Pass_Out
        string query = "";
        DataTable Main_DT = new DataTable();
        query = "Select * from Waste_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Trans_No='" + txtTransNo.Text + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        if (Main_DT.Rows.Count != 0)
        {
            txtTransNo.Text = Main_DT.Rows[0]["Trans_No"].ToString();
            txtDate.Text = Main_DT.Rows[0]["Trans_Date"].ToString();
            ddlShift.SelectedValue = Main_DT.Rows[0]["Shift"].ToString();
            txtMixingKg.Text = Main_DT.Rows[0]["MixingKg"].ToString();
            txtRemarks.Text = Main_DT.Rows[0]["Remarks"].ToString();

          
            //Unplanned_Receipt_Main_Sub Table Load
            DataTable dt = new DataTable();
            query = "Select *from Waste_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Trans_No='" + txtTransNo.Text + "'";
            dt = objdata.RptEmployeeMultipleDetails(query);
            ViewState["ItemTable"] = dt;
            Repeater1.DataSource = dt;
            Repeater1.DataBind();

            //Totalsum();

            btnSave.Text = "Update";
        }
        else
        {
            Clear_All_Field();
        }
    }

    private void Load_OLD_data()
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        Repeater1.DataSource = dt;
        Repeater1.DataBind();
    }
    private void Initial_Data_Referesh()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add(new DataColumn("WasteDesc", typeof(string)));
        dt.Columns.Add(new DataColumn("OPT", typeof(string)));
        dt.Columns.Add(new DataColumn("WasteKg", typeof(string)));
        dt.Columns.Add(new DataColumn("Reason", typeof(string)));
      
        Repeater1.DataSource = dt;
        Repeater1.DataBind();
        ViewState["ItemTable"] = Repeater1.DataSource;

        //dt = Repeater1.DataSource;
    }
    private void Load_Shift()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlShift.Items.Clear();
        query = "Select ShiftName from MstShift where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlShift.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["ShiftName"] = "-Select-";
        dr["ShiftName"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlShift.DataTextField = "ShiftName";
        ddlShift.DataValueField = "ShiftName";
        ddlShift.DataBind();
    }

    protected void GridDeleteClick(object sender, CommandEventArgs e)
    {
        DataTable qry_dt = new DataTable();
        bool ErrFlag = false;
        string qry = "";


        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            if ((dt.Rows[i]["WasteDesc"].ToString() == e.CommandName.ToString()))
            {
                dt.Rows.RemoveAt(i);
                dt.AcceptChanges();
            }
        }
        ViewState["ItemTable"] = dt;
        Load_OLD_data();
        //Totalsum();
    }
    protected void btnAddItem_Click(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        DataTable qry_dt = new DataTable();
        bool ErrFlag = false;
        DataRow dr = null;
        string query = "";

        if (txtOPTPer.Text == "0.0" || txtOPTPer.Text == "0" || txtOPTPer.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the OPT %...');", true);
        }
        if (txtWasteKg.Text == "0.0" || txtWasteKg.Text == "0" || txtWasteKg.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the Waste Kg...');", true);
        }

        if (!ErrFlag)
        {
            // check view state is not null  
            if (ViewState["ItemTable"] != null)
            {

                //get datatable from view state   
                dt = (DataTable)ViewState["ItemTable"];

                //check Item Already add or not
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (dt.Rows[i]["WasteDesc"].ToString().ToUpper() == ddlWasteDesc.SelectedValue.ToString().ToUpper())
                    {
                        ErrFlag = true;
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('This Waste Already Added..');", true);
                    }
                }
                if (!ErrFlag)
                {
                    dr = dt.NewRow();
                    dr["WasteDesc"] = ddlWasteDesc.SelectedValue;
                    dr["OPT"] = txtOPTPer.Text;
                    dr["WasteKg"] = txtWasteKg.Text;
                    dr["Reason"] = txtReason.Text;
                    
                  
                    dt.Rows.Add(dr);
                    ViewState["ItemTable"] = dt;
                    Repeater1.DataSource = dt;
                    Repeater1.DataBind();

                    //Totalsum();


                    ddlWasteDesc.SelectedValue = "-Select-";
                    txtOPTPer.Text = "0"; txtWasteKg.Text = "0";
                    txtReason.Text="";


                }
            }
            else
            {
                dr = dt.NewRow();
                dr["WasteDesc"] = ddlWasteDesc.SelectedValue;
                dr["OPT"] = txtOPTPer.Text;
                dr["WasteKg"] = txtWasteKg.Text;
                dr["Reason"] = txtReason.Text;


                dt.Rows.Add(dr);
                ViewState["ItemTable"] = dt;
                Repeater1.DataSource = dt;
                Repeater1.DataBind();

                //Totalsum();


                ddlWasteDesc.SelectedValue = "-Select-";
                txtOPTPer.Text = "0"; txtWasteKg.Text = "0";
                txtReason.Text = "";
            }
        }
    }


    protected void btnSave_Click(object sender, EventArgs e)
    {
        string query = "";
        DataTable DT_Check = new DataTable();
        DataTable qry_dt = new DataTable();
        string SaveMode = "Insert";
        bool ErrFlag = false;

        //check with Item Details Add with Grid
        DT_Check = (DataTable)ViewState["ItemTable"];
        if (DT_Check.Rows.Count == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You have to add atleast one Waste Details..');", true);
        }

        //User Rights Check Start
        bool Rights_Check = false;
        //if (btnSave.Text == "Update")
        //{
        //    Rights_Check = CommonClass_Function.ModifyRights_Check(SessionCcode, SessionLcode, SessionUserID, "7", "2", "Spares Receipt");
        //    if (Rights_Check == false)
        //    {
        //        ErrFlag = true;
        //        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Modify Spares Receipt Details..');", true);
        //    }
        //}
        //else
        //{
        //    Rights_Check = CommonClass_Function.AddRights_Check(SessionCcode, SessionLcode, SessionUserID, "7", "2", "Spares Receipt");
        //    if (Rights_Check == false)
        //    {
        //        ErrFlag = true;
        //        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Adding New Spares Receipt..');", true);
        //    }
        //}
        //User Rights Check End


        //Auto generate Transaction Function Call
        if (btnSave.Text != "Update")
        {
            if (!ErrFlag)
            {
                TransactionNoGenerate TransNO = new TransactionNoGenerate();
                string Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup(SessionCcode, SessionLcode, "Waste", SessionFinYearVal);
                if (Auto_Transaction_No == "")
                {
                    ErrFlag = true;
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Auto Generate Transaction No Error... Contact Server Admin');", true);
                }
                else
                {
                    txtTransNo.Text = Auto_Transaction_No;
                }
            }
        }




        //Check With Already Approved Start
        DataTable DT_Check_Apprv = new DataTable();
        query = "Select * from Waste_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Trans_No='" + txtTransNo.Text + "' And Status='1'";
        DT_Check_Apprv = objdata.RptEmployeeMultipleDetails(query);
        if (DT_Check_Apprv.Rows.Count != 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Do not Waste Details Already Approved..');", true);
        }




        if (!ErrFlag)
        {
            query = "Select * from Waste_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Trans_No='" + txtTransNo.Text + "'";
            DT_Check = objdata.RptEmployeeMultipleDetails(query);
            if (DT_Check.Rows.Count != 0)
            {
                SaveMode = "Update";
                query = "Delete from Waste_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Trans_No='" + txtTransNo.Text + "'";
                objdata.RptEmployeeMultipleDetails(query);
                query = "Delete from Waste_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Trans_No='" + txtTransNo.Text + "'";
                objdata.RptEmployeeMultipleDetails(query);
            }

            //Response.Write(strValue);
            //Insert Main Table
            query = "Insert Into Waste_Main(Ccode,Lcode,FinYearCode,FinYearVal,Trans_No,Trans_Date,Shift,MixingKg,Remarks,";
            query = query + "UserID,UserName) Values('" + SessionCcode + "','" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + txtTransNo.Text + "','" + txtDate.Text + "',";
            query = query + " '" + ddlShift.SelectedValue + "','" + txtMixingKg.Text + "',";
            query = query + "'" + txtRemarks.Text + "','" + SessionUserID + "','" + SessionUserName + "')";
            objdata.RptEmployeeMultipleDetails(query);

            DataTable dt = new DataTable();
            dt = (DataTable)ViewState["ItemTable"];
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                query = "Insert Into Waste_Main_Sub(Ccode,Lcode,FinYearCode,FinYearVal,Trans_No,Trans_Date,WasteDesc,OPT,";
                query = query + "WasteKg,Reason,UserID,UserName) Values('" + SessionCcode + "',";
                query = query + " '" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + txtTransNo.Text + "','" + txtDate.Text + "',";
                query = query + " '" + dt.Rows[i]["WasteDesc"].ToString() + "','" + dt.Rows[i]["OPT"].ToString() + "',";
                query = query + "'" + dt.Rows[i]["WasteKg"].ToString() + "','" + dt.Rows[i]["Reason"].ToString() + "','" + SessionUserID + "','" + SessionUserName + "')";
                objdata.RptEmployeeMultipleDetails(query);
            }



            if (SaveMode == "Insert")
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Waste Details Saved Successfully');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Waste Details Updated Successfully');", true);
            }
            //Clear_All_Field();
            Session["Trans_No"] = txtTransNo.Text;
            btnSave.Text = "Update";
            //Load_Data_Enquiry_Grid();
        }
    }

    private void Clear_All_Field()
    {
       
        txtTransNo.Text = ""; txtDate.Text = ""; ddlShift.SelectedValue = "-Select-";

        txtRemarks.Text = "";
        txtMixingKg.Text = "";

        ddlWasteDesc.SelectedValue = "-Select-"; 
        txtOPTPer.Text = "0"; txtWasteKg.Text = "0";
        txtReason.Text = "";

        btnSave.Text = "Save";
        Initial_Data_Referesh();
        Session.Remove("Trans_No");
        //Load_Data_Enquiry_Grid();
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }
    protected void btnBackEnquiry_Click(object sender, EventArgs e)
    {
        Session.Remove("Trans_No");
        Response.Redirect("Waste_Main.aspx");
    }
}
