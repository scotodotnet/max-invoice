﻿<%@ Page Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="Dispatch_Details.aspx.cs" Inherits="Transaction_Dispatch_Details" Title="" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<script src='<%= ResolveUrl("../assets/js/master_list_jquery.min.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/master_list_jquery-ui.min.js") %>'></script>
<link href="<%= ResolveUrl("../assets/css/master_list_jquery-ui.css") %>" rel="stylesheet" type="text/css"/>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#example').dataTable();
            }
        });
    };
</script>

<script type="text/javascript">
    function SaveMsgAlert(msg) {
        swal(msg);
    }
</script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('.date-picker').datepicker({ format: 'dd/mm/yyyy' });
                $('.js-states').select2();
            }
        });
    };
</script>


<div class="page-breadcrumb">
    <ol class="breadcrumb container">
       <h4><li class="active">Dispatch</li></h4> 
    </ol>
</div>
                
                        
<div id="main-wrapper" class="container">
 <div class="row">
    <div class="col-md-12">
     
    
    <div class="page-inner">
    
      <div class="col-md-12">
		<div class="panel panel-white">
		  <div class="panel panel-primary">
			<div class="panel-heading clearfix">
			  <h4 class="panel-title">Dispatch </h4>
			</div>
		   </div>
		<form class="form-horizontal">
		  <div class="panel-body">
			<div class="col-md-12">
		      <div class="row">
				<div class="form-group col-md-4">
				   <label for="exampleInputName">Trans No<span class="mandatory">*</span></label>
				   <asp:Label ID="txtReceiptNo" runat="server" class="form-control" BackColor="#F3F3F3"></asp:Label>
			    </div>
				<div class="form-group col-md-4">
				  <label for="exampleInputName">Date</label>
				  <asp:TextBox ID="txtDate" MaxLength="20" class="form-control date-picker" runat="server"></asp:TextBox>
				  <asp:RequiredFieldValidator ControlToValidate="txtDate" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                  </asp:RequiredFieldValidator>
                  <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                     TargetControlID="txtDate" ValidChars="0123456789./">
                  </cc1:FilteredTextBoxExtender>
				</div>
				 <div class="form-group col-md-4">
			          <label for="exampleInputName">Customer Name</label>
				     <asp:DropDownList ID="ddlCustomer" runat="server" AutoPostBack="true"
                                class="js-states form-control" 
                          onselectedindexchanged="ddlCustomer_SelectedIndexChanged" >
                           </asp:DropDownList>
                           <asp:RequiredFieldValidator ControlToValidate="ddlCustomer" Display="Dynamic" InitialValue="-Select-" ValidationGroup="Item_Validate_Field" class="form_error" ID="RequiredFieldValidator3" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                           </asp:RequiredFieldValidator>
			       </div>
				   
			
			
			 
			 </div>
			</div>
					        
			<div class="col-md-12">
		     <div class="row">  
					         <div class="form-group col-md-3">
			                  <label for="exampleInputName">Address</label>
                                <asp:TextBox ID="txtAddress" runat="server" class="form-control" TextMode="MultiLine"></asp:TextBox>
                                   <asp:RequiredFieldValidator ControlToValidate="txtAddress" Display="Dynamic" InitialValue="-Select-" ValidationGroup="Item_Validate_Field" class="form_error" ID="RequiredFieldValidator4" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                   </asp:RequiredFieldValidator>
			               </div>
				
			                <div class="form-group col-md-3">
			                    <label for="exampleInputName">Mobile No</label>
					                <asp:TextBox ID="txtMobile" runat="server" class="form-control" ></asp:TextBox> 
                                    <asp:RequiredFieldValidator ControlToValidate="txtMobile" Display="Dynamic" InitialValue="-Select-" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator6" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                    </asp:RequiredFieldValidator>
			                   </div>
			               <div class="form-group col-md-3">
			                   <label for="exampleInputName">Vehicle No</label>
					            <asp:TextBox ID="txtVehicleNo" runat="server" class="form-control"></asp:TextBox> 
                                <asp:RequiredFieldValidator ControlToValidate="txtVehicleNo" Display="Dynamic" InitialValue="-Select-" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator16" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
			               </div>
			  
			  
			   
			             <div class="form-group col-md-3">
				           <label for="exampleInputName">Reason</label>
				           <asp:TextBox ID="txtRemarks" TextMode="MultiLine" class="form-control" runat="server"></asp:TextBox>
			            </div>
					        
			  </div>
			</div>
			
			<div class="col-md-12">
			<div class="row">  
			
			 <div class="form-group col-md-3">
			          <label for="exampleInputName">GoDown</label>
				     <asp:DropDownList ID="ddlGodown" runat="server" 
                                class="js-states form-control" >
                           </asp:DropDownList>
                           <asp:RequiredFieldValidator ControlToValidate="ddlCustomer" Display="Dynamic" InitialValue="-Select-" ValidationGroup="Item_Validate_Field" class="form_error" ID="RequiredFieldValidator10" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                           </asp:RequiredFieldValidator>
			       </div>
			
			   <div class="form-group col-md-3">
			                   <label for="exampleInputName">Delivery From</label>
					            <asp:TextBox ID="txtFrom" runat="server" class="form-control"></asp:TextBox> 
                                <asp:RequiredFieldValidator ControlToValidate="txtFrom" Display="Dynamic" InitialValue="-Select-" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator11" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
			               </div>
			               
			               
			                  <div class="form-group col-md-3">
			                   <label for="exampleInputName">Delivery From</label>
					            <asp:TextBox ID="txtTo" runat="server" class="form-control"></asp:TextBox> 
                                <asp:RequiredFieldValidator ControlToValidate="txtTo" Display="Dynamic" InitialValue="-Select-" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator14" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
			               </div>
			
			      <div class="form-group col-md-3">
				           <label for="exampleInputName">Note</label>
				           <asp:TextBox ID="txtNote" TextMode="MultiLine" class="form-control" runat="server"></asp:TextBox>
			            </div>
			            
			    
			</div>
			</div>
		
			
					
			<div class="clearfix"></div>				    
			<div class="timeline-options">
			  <h4 class="panel-title">Count Details</h4>
			</div>
		     <div class="col-md-12">
				<div class="row">
			
					        
					          <div class="form-group col-md-4">
					               <label for="exampleInputName">Count</label>
					               <asp:DropDownList ID="ddlCount" runat="server" 
                                        class="js-states form-control" AutoPostBack="true"
                                       onselectedindexchanged="ddlCount_SelectedIndexChanged" >
                                   </asp:DropDownList>
                                   <asp:RequiredFieldValidator ControlToValidate="ddlCount" Display="Dynamic" InitialValue="-Select-" ValidationGroup="Item_Validate_Field" class="form_error" ID="RequiredFieldValidator2" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                   </asp:RequiredFieldValidator>
				              </div>
				              
				                <div class="form-group col-md-2">
					                    <label for="exampleInputName">Lot No</label>
					                  <asp:TextBox ID="txtLotNo" class="form-control" runat="server" Text="0"></asp:TextBox>
					                    <asp:RequiredFieldValidator ControlToValidate="txtLotNo" Display="Dynamic" ValidationGroup="Item_Validate_Field" class="form_error" ID="RequiredFieldValidator13" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                        </asp:RequiredFieldValidator>
                                   
					                </div>
					                
					                <div class="form-group col-md-3">
					                   <label for="exampleInputName">Color</label>
					                   <asp:DropDownList ID="ddlColor" runat="server" 
                                            class="js-states form-control" >
                                       </asp:DropDownList>
                                       <asp:RequiredFieldValidator ControlToValidate="ddlColor" Display="Dynamic" InitialValue="-Select-" ValidationGroup="Item_Validate_Field" class="form_error" ID="RequiredFieldValidator8" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                       </asp:RequiredFieldValidator>
				                  </div>
				                  
				                  <div class="form-group col-md-3">
					                   <label for="exampleInputName">Shade No</label>
					                   <asp:DropDownList ID="ddlShade" runat="server" 
                                            class="js-states form-control"  >
                                       </asp:DropDownList>
                                       <asp:RequiredFieldValidator ControlToValidate="ddlShade" Display="Dynamic" InitialValue="-Select-" ValidationGroup="Item_Validate_Field" class="form_error" ID="RequiredFieldValidator9" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                       </asp:RequiredFieldValidator>
				                  </div>
					                
					                  
					                   
					              
					      
					      
					       
					        </div>
					        </div>
					        
					 <div class="col-md-12">
					  <div class="row">
					  
					     <div class="form-group col-md-2">
					                        <label for="exampleInputName">No of Bag</label>
					                        <asp:TextBox ID="txtBags" class="form-control" runat="server" Text="0" AutoPostBack="true"
                                                ontextchanged="txtBags_TextChanged"></asp:TextBox>
					                        <asp:RequiredFieldValidator ControlToValidate="txtBags" Display="Dynamic" ValidationGroup="Item_Validate_Field" class="form_error" ID="RequiredFieldValidator12" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                            </asp:RequiredFieldValidator>
                                          
					                    </div>
					                      <div class="form-group col-md-2">
					                        <label for="exampleInputName">Bag KG</label>
					                        <asp:TextBox ID="txtBagKG" class="form-control" runat="server" Text="50" AutoPostBack="true" 
                                                  ontextchanged="txtBagKG_TextChanged"></asp:TextBox>
					                        <asp:RequiredFieldValidator ControlToValidate="txtBags" Display="Dynamic" ValidationGroup="Item_Validate_Field" class="form_error" ID="RequiredFieldValidator7" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                            </asp:RequiredFieldValidator>
                                          
					                    </div>
					                    <div class="form-group col-md-2">
					                    <label for="exampleInputName">Quantity</label>
					                  <asp:TextBox ID="txtQty" class="form-control" Enabled="false" runat="server" Text="0"></asp:TextBox>
					                    <asp:RequiredFieldValidator ControlToValidate="txtQty" Display="Dynamic" ValidationGroup="Item_Validate_Field" class="form_error" ID="RequiredFieldValidator20" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                        </asp:RequiredFieldValidator>
                                       
					                </div>
					                
					                 <div class="form-group col-md-2">
					                        <label for="exampleInputName">Rate</label>
					                        <asp:TextBox ID="txtRate" class="form-control" runat="server" Text="0"
                                                  ></asp:TextBox>
					                        <asp:RequiredFieldValidator ControlToValidate="txtRate" Display="Dynamic" ValidationGroup="Item_Validate_Field" class="form_error" ID="RequiredFieldValidator5" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                            </asp:RequiredFieldValidator>
                                          
					                    </div>
					      
					                      <div class="form-group col-md-2">
					                        <br />
					                        <asp:Button ID="btnAddItem" Width="50" Height="30" class="btn-success"  
                                                runat="server" Text="Add" ValidationGroup="Item_Validate_Field" 
                                                onclick="btnAddItem_Click" />
                                                 
					                    </div>
					  </div>
					 </div>       
					  
                    <!-- table start -->
					<div class="col-md-12">
					    <div class="row">
					        <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
			                    <HeaderTemplate>
                                    <table id="example" class="display table">
                                        <thead>
                                            <tr>
                                                <th>S.No</th>
                                                <th>Count</th>
                                                <th>LotNo</th>
                                                <th>Color</th>
                                                <th>ShadeNo</th>
                                                <th>No of Bag</th>
                                                <th>Bag KG</th>
                                                <th>Qty</th>
                                                <th>Rate</th>
                                                <th>Amout</th>
                                                <th>Mode</th>
                                            </tr>
                                        </thead>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td><%# Container.ItemIndex + 1 %></td>
                                        
                                        <td><%# Eval("CountName")%></td>
                                        <td><%# Eval("LotNo")%></td>
                                        <td><%# Eval("Color")%></td>
                                        <td><%# Eval("ShadeNo")%></td>
                                        <td><%# Eval("NoofBag")%></td>
                                        <td><%# Eval("BagKG")%></td>
                                        <td><%# Eval("Qty")%></td>
                                        <td><%# Eval("Rate")%></td>
                                        <td><%# Eval("Amount")%></td>
                                        
                                        <td>
                                            <asp:LinkButton ID="btnDeleteGrid" class="btn btn-danger btn-sm fa fa-trash-o"  runat="server" 
                                                Text="" OnCommand="GridDeleteClick" CommandArgument='<%# Eval("LotNo")%>' CommandName='<%# Eval("CountName")%>' 
                                                CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this Cheese details?');">
                                            </asp:LinkButton>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate></table></FooterTemplate>                                
			                </asp:Repeater>
					    </div>
					</div>
					<!-- table End -->
					
					
					
					
					
					<div class="form-group row"></div>	
                    <!-- Button start -->
                    <div class="txtcenter">
                        <asp:Button ID="btnSave" class="btn btn-success"  runat="server" Text="Save" 
                            ValidationGroup="Validate_Field" onclick="btnSave_Click"/>
                        <asp:Button ID="btnCancel" class="btn btn-danger" runat="server" Text="Cancel" onclick="btnCancel_Click"/>
                        <asp:Button ID="btnBackEnquiry" class="btn btn-success" runat="server" 
                            Text="Back" onclick="btnBackEnquiry_Click" />
                        <asp:Button ID="btnApprove" class="btn btn-success" runat="server" 
                            Text="Approve" onclick="btnApprove_Click" />
                    </div>
                    <!-- Button end -->
                  </div>      
				</form>
			
			</div><!-- panel white end -->
		    </div>
		    
		    <div class="col-md-2"></div>
		    
            
      
 </div> <!-- col-9 end -->
 </div><!-- col 12 end -->
      
  </div><!-- row end -->
 </div>
</asp:Content>

