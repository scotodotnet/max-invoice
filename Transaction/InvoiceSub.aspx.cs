﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
public partial class Transaction_InvoiceSub : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionEnquiryNo;
    string SessionFinYearCode;
    string SessionFinYearVal;
    DataTable qry_dt = new DataTable();
    DataTable dtd1 = new DataTable();
    bool EventFire = false;
    static string sum;
    static decimal Totalbale;
    string Auto_Transaction_No;
    string SessionIssue_Entry_No;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();
        if (!IsPostBack)
        {
            Initial_Data_Referesh();
            Load_Supplier();
            Load_CosistItem();
            Load_Item();
            Tax_Load();
            txtTaxName_SelectedIndexChanged(sender, e);
            Page.Title = "ERP Module :: Invoice Entry";
            HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Master"));
            li.Attributes.Add("class", "droplink active open");

            if (Session["Invoice_Code"] == null)
            {
                SessionIssue_Entry_No = "";
            }
            else
            {
                SessionIssue_Entry_No = Session["Invoice_Code"].ToString();
                txtInvoiceNo.Text = SessionIssue_Entry_No;
                btnSearch_Click(sender, e);
            }
        }

        Load_OLD_data();
    }

    private void Load_CosistItem()
    {
        string SSQL = "";
        SSQL = "Select * from ConsistItemMaster Where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
        DataTable conitem = new DataTable();
        conitem = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlConsistItemName.DataSource = conitem;
        ddlConsistItemName.DataTextField = "ItemName";
        ddlConsistItemName.DataValueField = "ItemCode";
        ddlConsistItemName.DataBind();
        ddlConsistItemName.Items.Insert(0, new ListItem("-Select-", "-Select-"));
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    { 
    //Search Sales_Order
        string query = "";
        DataTable Main_DT = new DataTable();
        query = "Select * from Invoice_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Invoice_Entry_No='" + txtInvoiceNo.Text + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        if (Main_DT.Rows.Count != 0)
        {
            txtDate.Text = Main_DT.Rows[0]["Invoice_Entry_Date"].ToString();
            ddlSupplier.SelectedValue = Main_DT.Rows[0]["SupplierID"].ToString();
            ddlSupplier.SelectedItem.Text = Main_DT.Rows[0]["SupplierName"].ToString();
            txtOrderNo.Text = Main_DT.Rows[0]["OrderNum"].ToString();
            txtOrderDate.Text = Main_DT.Rows[0]["OrderDate"].ToString();
            txtDespatchThrough.Text = Main_DT.Rows[0]["Despatch"].ToString();

            txtPackAmt.Text= Main_DT.Rows[0]["PackingAmt"].ToString();
            txtService.Text = Main_DT.Rows[0]["ServiceAmt"].ToString();
            txtCourierAmt.Text = Main_DT.Rows[0]["CourrierAmt"].ToString();

            txtLRNo.Text = Main_DT.Rows[0]["LRno"].ToString();
            txtDocument.Text = Main_DT.Rows[0]["Documents"].ToString();
            ddlPayemnt.SelectedItem.Text = Main_DT.Rows[0]["Payment"].ToString();
            txtEwayBill.Text = Main_DT.Rows[0]["Ewaybill"].ToString();

            txtSubTotal.Text = Main_DT.Rows[0]["SubTotal"].ToString();
            txtNetAmt.Text = Main_DT.Rows[0]["NetAmount"].ToString();
            txtDiscountPercent.Text = Main_DT.Rows[0]["DiscountPer"].ToString();
            txtDiscountAmt.Text = Main_DT.Rows[0]["DiscountAmt"].ToString();

            txtTaxName.SelectedValue = Main_DT.Rows[0]["GSTID"].ToString();
            txtTaxName.SelectedItem.Text = Main_DT.Rows[0]["GSTDescription"].ToString();

            txtCGST.Text = Main_DT.Rows[0]["CGSTPercentage"].ToString();
            txtCGSTAmt.Text = Main_DT.Rows[0]["CGSTAmount"].ToString();
            txtSGST.Text = Main_DT.Rows[0]["SGSTPercentage"].ToString();
            txtSGSTAmt.Text = Main_DT.Rows[0]["SGSTAmount"].ToString();
            txtIGST.Text = Main_DT.Rows[0]["IGSTPercentage"].ToString();
            txtIGSTAmt.Text = Main_DT.Rows[0]["IGSTAmount"].ToString();
            txtTotalGST.Text = Main_DT.Rows[0]["NetAmount"].ToString();


            //Customer_Purchase_Order_Main_Sub Table Load
            DataTable dt = new DataTable();
            query = "Select ItemCode,ItemName,HsnCode,isnull(Price,'0') as Price,isnull(Qty,0) as Qty,isnull(QtyType,0) as QtyType,isnull(Amount,0) as Amount from Invoice_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Invoice_Entry_No='" + txtInvoiceNo.Text + "'";
            dt = objdata.RptEmployeeMultipleDetails(query);
            ViewState["CountTable"] = dt;
            RptInvioceEntry.DataSource = dt;
            RptInvioceEntry.DataBind();
        }
    }
    private void Tax_Load()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        txtTaxName.Items.Clear();
        query = "Select * from MstGst  order by GstType Asc";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        txtTaxName.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["GstType"] = "-Select-";
        dr["GstType"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        txtTaxName.DataTextField = "GstType";
        txtTaxName.DataValueField = "GstType";

        txtTaxName.DataBind();
    }
    private void Load_OLD_data()
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["CountTable"];
        RptInvioceEntry.DataSource = dt;
        RptInvioceEntry.DataBind();
    }
    private void Load_Supplier()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlSupplier.Items.Clear();
        query = "Select SuppCode,SuppName from MstSupplier where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlSupplier.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["SuppCode"] = "-Select-";
        dr["SuppName"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlSupplier.DataTextField = "SuppName";
        ddlSupplier.DataValueField = "SuppCode";

        ddlSupplier.DataBind();
    }
    private void Load_Item()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlItemName.Items.Clear();
        query = "Select ItemCode,ItemName from ItemMaster where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlItemName.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["ItemCode"] = "-Select-";
        dr["ItemName"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlItemName.DataTextField = "ItemName";
        ddlItemName.DataValueField = "ItemCode";

        ddlItemName.DataBind();
    }
    protected void ddlItemName_SelectedIndexChanged(object sender, EventArgs e)
    {
        string query = "";

        txtPrice.Text = "";
        txtHSN.Text = "";
        DataTable dt = new DataTable();
        //ddlItemName.Items.Clear();
        query = "Select HSNcode,Amount,ItemQty from ItemMaster where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' and ItemCode='" + ddlItemName.SelectedValue + "'";
        dt = objdata.RptEmployeeMultipleDetails(query);
        if (dt.Rows.Count > 0)
        {
            txtHSN.Text = dt.Rows[0]["HSNcode"].ToString();
            txtPrice.Text = dt.Rows[0]["Amount"].ToString();
            txtQtytyp.Text = dt.Rows[0]["ItemQty"].ToString();
        }
    }
    protected void txtQty_TextChanged(object sender, EventArgs e)
    {
        if (txtPrice.Text == "") { txtPrice.Text = "0"; }
        txtAmount.Text = (Convert.ToDecimal(txtQty.Text) * Convert.ToDecimal(txtPrice.Text)).ToString();

    }
    private void Initial_Data_Referesh()
    {
        DataTable dtd1 = new DataTable();
        dtd1.Columns.Add(new DataColumn("ItemCode", typeof(string)));
        dtd1.Columns.Add(new DataColumn("ItemName", typeof(string)));
        dtd1.Columns.Add(new DataColumn("HSNCode", typeof(string)));
        dtd1.Columns.Add(new DataColumn("Price", typeof(string)));
        dtd1.Columns.Add(new DataColumn("Qty", typeof(string)));
        dtd1.Columns.Add(new DataColumn("QtyType", typeof(string)));
        dtd1.Columns.Add(new DataColumn("Amount", typeof(string)));

        RptInvioceEntry.DataSource = dtd1;
        RptInvioceEntry.DataBind();
        ViewState["CountTable"] = RptInvioceEntry.DataSource;


    }
    protected void GridDeleteClick(object sender, CommandEventArgs e)
    {
       
    }
    protected void txtTaxName_SelectedIndexChanged(object sender, EventArgs e)
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select * from MstGst where  GstType='" + txtTaxName.SelectedValue + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            txtCGST.Text = DT.Rows[0]["Cgst"].ToString();
            txtSGST.Text = DT.Rows[0]["Sgst"].ToString();
            txtIGST.Text = DT.Rows[0]["Igst"].ToString();
        }
        Total_Calculate();
    }
    protected void btnApproval_Click(object sender, EventArgs e)
    {
        string query = "";
        //User Rights Check Start
        bool ErrFlag = false;
        bool Rights_Check = false;

        DataTable DT = new DataTable();
        query = "Select * from Invoice_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Invoice_Entry_No='" + txtInvoiceNo.Text + "' And SO_Status = '1'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Invoice Details Already Approved..');", true);
        }
        if (!ErrFlag)
        {
            DataTable dt = new DataTable();
            query = "Select * from Invoice_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Invoice_Entry_No='" + txtInvoiceNo.Text + "'";
            dt = objdata.RptEmployeeMultipleDetails(query);
            if (dt.Rows.Count > 0)
            {
                query = "update Invoice_Main set SO_Status='1' where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Invoice_Entry_No='" + txtInvoiceNo.Text + "'";
                objdata.RptEmployeeMultipleDetails(query);
                
            }
            Response.Redirect("InvoiceMain.aspx");

        }


    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        DataTable qry_dt = new DataTable();
        bool ErrFlag = false;
        string SaveMode = "Insert";
        DataRow dr = null;
        string query = "";
        string NetAmount;

        if ((ddlSupplier.SelectedValue == "0") || (ddlSupplier.SelectedValue == "-Select-"))
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select the Supplier Name...');", true);
        }
        else if (txtInvoiceNo.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the Invoice No..');", true);
        }
        else if (txtDate.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the Date..');", true);
        }
        if (!ErrFlag)
        {
            query = "";
            query = query + "Select * from Invoice_Main where Invoice_Entry_No='" + txtInvoiceNo.Text + "' and Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
            qry_dt = objdata.RptEmployeeMultipleDetails(query);
            if (qry_dt.Rows.Count > 0)
            {
                SaveMode = "Update";
                query = query + "Delete from Invoice_Main where Invoice_Entry_No='" + txtInvoiceNo.Text + "' and Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
                objdata.RptEmployeeMultipleDetails(query);

                query = query + "Delete from Invoice_Sub where Invoice_Entry_No='" + txtInvoiceNo.Text + "' and Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
                objdata.RptEmployeeMultipleDetails(query);
            }

            NetAmount = (Math.Round(Convert.ToDecimal(txtNetAmt.Text), 0, MidpointRounding.AwayFromZero)).ToString();

            //Insert Data to Table
            query = "insert into Invoice_Main(Ccode,Lcode,FinYearCode,FinYearVal,Invoice_Entry_No,Invoice_Entry_Date,";
            query = query + "SupplierID,SupplierName,OrderNum,OrderDate,Despatch,LRno,Documents,Payment,Ewaybill,";
            query = query + "SubTotal,PackingAmt,ServiceAmt,CourrierAmt,DiscountPer,DiscountAmt,NetAmount,GSTID,GSTDescription,CGSTPercentage,CGSTAmount,SGSTPercentage,SGSTAmount,";
            query = query + "IGSTPercentage,IGSTAmount,TotalGST,UserID,UserName,SO_Status) values('" + SessionCcode + "','" + SessionLcode + "',";
            query = query + "'" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + txtInvoiceNo.Text + "','" + txtDate.Text + "',";
            query = query + "'" + ddlSupplier.SelectedValue + "','" + ddlSupplier.SelectedItem.Text + "','" + txtOrderNo.Text + "','" + txtOrderDate.Text + "',";
            query = query + "'" + txtDespatchThrough.Text + "','" + txtLRNo.Text + "','" + txtDocument.Text + "','" + ddlPayemnt.SelectedItem.Text + "',";
            query = query + "'" + txtEwayBill.Text + "','" + txtSubTotal.Text + "','" + txtPackAmt.Text + "','" + txtService.Text + "','" + txtCourierAmt.Text + "','" + txtDiscountPercent.Text + "','" + txtDiscountAmt.Text + "','" + NetAmount + "','" + txtTaxName.SelectedValue + "',";
            query = query + "'" + txtTaxName.SelectedItem.Text + "','" + txtCGST.Text + "','" + txtCGSTAmt.Text + "','" + txtSGST.Text + "',";
            query = query + "'" + txtSGSTAmt.Text + "','" + txtIGST.Text + "','" + txtIGSTAmt.Text + "','" + txtTotalGST.Text + "','" + SessionUserID + "','" + SessionUserName + "','0')";
            objdata.RptEmployeeMultipleDetails(query);

            //DataTable dt = new DataTable();
            dt = (DataTable)ViewState["CountTable"];

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                query = "insert into Invoice_Sub(Ccode,Lcode,FinYearCode,FinYearVal,Invoice_Entry_No,Invoice_Entry_Date,";
                query = query + "ItemCode,ItemName,HsnCode,Price,Qty,QtyType,Amount,UserID,UserName) Values('" + SessionCcode + "','" + SessionLcode + "',";
                query = query + "'" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + txtInvoiceNo.Text + "','" + txtDate.Text + "',";
                query = query + "'" + dt.Rows[i]["ItemCode"].ToString() + "','" + dt.Rows[i]["ItemName"].ToString() + "','" + dt.Rows[i]["HSNCode"].ToString() + "',";
                query = query + "'" + dt.Rows[i]["Price"].ToString() + "','" + dt.Rows[i]["Qty"].ToString() + "','"+dt.Rows[i]["QtyType"].ToString() +"','" + dt.Rows[i]["Amount"].ToString() + "',";
                query = query + "'" + SessionUserID + "','" + SessionUserName + "')";
                objdata.RptEmployeeMultipleDetails(query);

            }
            Response.Redirect("InvoiceMain.aspx");

            //Clear();

            if (SaveMode == "Insert")
            {
                //Update_Trans_ID();
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Invoice Order Details Saved Successfully');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Invoice Order Details Updated Successfully');", true);
            }



        }
    }
    protected void txtDiscountPercent_TextChanged(object sender, EventArgs e)
    {
        discount();
        
    }
    protected void discount()
    {
        txtSubTotal.Text = (Convert.ToDecimal(hidden_fld.Value) - Convert.ToDecimal((Convert.ToDecimal(txtDiscountPercent.Text) / 100) * Convert.ToDecimal(hidden_fld.Value))).ToString();
        txtSubTotal.Text = Math.Round(Convert.ToDecimal(txtSubTotal.Text), 2, MidpointRounding.AwayFromZero).ToString();
        txtDiscountAmt.Text = Convert.ToDecimal((Convert.ToDecimal(txtDiscountPercent.Text) / 100) * Convert.ToDecimal(hidden_fld.Value)).ToString();
        txtDiscountAmt.Text = Math.Round(Convert.ToDecimal(txtDiscountAmt.Text), 2, MidpointRounding.AwayFromZero).ToString();
        Total_Calculate();
    }
    public void Clear()
    {
        txtInvoiceNo.Text = "";
        txtDate.Text = "";
        Load_Supplier();
        txtOrderNo.Text = "";
        txtOrderDate.Text = "";
        txtDespatchThrough.Text = "";
        txtLRNo.Text = "";
        txtDocument.Text = "";
        ddlPayemnt.SelectedValue = "0";
        txtEwayBill.Text = "";
        txtSubTotal.Text = "";
        txtNetAmt.Text = "";
        //txtTaxName.SelectedValue = "0";
        Tax_Load();
        txtCGST.Text = "0";
        txtCGSTAmt.Text = "0";
        txtDiscountAmt.Text = "";
        txtDiscountPercent.Text = "";
        txtSGST.Text = "0";
        txtSGSTAmt.Text = "0";

        txtIGST.Text = "0";
        txtIGSTAmt.Text = "0";
    }
    protected void btnAddItem_Click(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        DataTable qry_dt = new DataTable();
        bool ErrFlag = false;
        DataRow dr = null;
        string query = "";

        if ((ddlItemName.SelectedValue == "0") || (ddlItemName.SelectedValue == "-Select-"))
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select the Item Name...');", true);
        }
        else if (txtPrice.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the Price ...');", true);
        }
        else if (txtQty.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the Quantity ...');", true);
        }
        if (!ErrFlag)
        { 
         // check view state is not null  
            if (ViewState["CountTable"] != null)
            {
                //get datatable from view state   
                dt = (DataTable)ViewState["CountTable"];

                if (dt.Rows.Count <= 8)
                {
                    //check Item Already add or not
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        if (dt.Rows[i]["ItemCode"].ToString().ToUpper() == ddlItemName.SelectedValue)
                        {
                            ErrFlag = true;
                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('This Item Already Added..');", true);
                        }
                    }

                    if (!ErrFlag)
                    {
                        dr = dt.NewRow();
                        dr["ItemCode"] = ddlItemName.SelectedValue;
                        dr["ItemName"] = ddlItemName.SelectedItem.Text;
                        dr["HSNCode"] = txtHSN.Text;
                        dr["Price"] = txtPrice.Text;
                        dr["Qty"] = txtQty.Text;
                        dr["QtyType"] = txtQtytyp.Text;
                        dr["Amount"] = txtAmount.Text;

                        dt.Rows.Add(dr);
                        ViewState["CountTable"] = dt;
                        RptInvioceEntry.DataSource = dt;
                        RptInvioceEntry.DataBind();
                        Totalsum();


                        Load_Item();
                        txtHSN.Text = "";
                        txtPrice.Text = "";
                        txtQty.Text = "";
                        txtAmount.Text = "";
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You have to reach the Maximum item in this Invoince');", true);
                }
            }
            else
            {
                dr = dt.NewRow();
                dr["ItemCode"] = ddlItemName.SelectedValue;
                dr["ItemName"] = ddlItemName.SelectedItem.Text;
                dr["HSNCode"] = txtHSN.Text;
                dr["Price"] = txtPrice.Text;
                dr["Qty"] = txtQty.Text;
                dr["QtyType"] = txtQtytyp.Text;
                dr["Amount"] = txtAmount.Text;

                dt.Rows.Add(dr);
                ViewState["CountTable"] = dt;
                RptInvioceEntry.DataSource = dt;
                RptInvioceEntry.DataBind();
                Totalsum();


                Load_Item();
                txtHSN.Text = "";
                txtPrice.Text = "";
                txtQty.Text = "";
                txtAmount.Text = "";
            }
        }
    }
    public void Totalsum()
    {
        sum = "0";

        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["CountTable"];
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            try
            {
                sum = (Convert.ToDecimal(sum) + Convert.ToDecimal(dt.Rows[i]["Amount"])).ToString();
            }
            catch (Exception ex)
            {

            }
        }

        txtSubTotal.Text = (Convert.ToDecimal(sum) + Convert.ToDecimal(txtPackAmt.Text) + Convert.ToDecimal(txtCourierAmt.Text) + Convert.ToDecimal(txtService.Text)).ToString();
        hidden_fld.Value= (Convert.ToDecimal(sum) + Convert.ToDecimal(txtPackAmt.Text) + Convert.ToDecimal(txtCourierAmt.Text) + Convert.ToDecimal(txtService.Text)).ToString();
        //Total_Calculate();
        discount();
    }
    private void Total_Calculate()
    {
        string Item_Total = "0";
        string CGST_Per = "0";
        string SGST_Per = "0";
        string IGST_Per = "0";
        string CGST_Amt = "0";
        string SGST_Amt = "0";
        string IGST_Amt = "0";
        string All_GST_Amt = "0";
        string Discount_Percent = "0";
        string Packing_Amt = "0";
        string Service_Amt = "0";
        string Courier_Amt = "0";
        string Discount_Amt = "0";
        string Final_Amount = "0";

        Item_Total = txtSubTotal.Text.ToString();


        if (Convert.ToDecimal(Item_Total) != 0)
        {
            if (Convert.ToDecimal(txtDiscountPercent.Text.ToString()) != 0) { Discount_Percent = txtDiscountPercent.Text.ToString(); }
            if (Convert.ToDecimal(txtCGST.Text.ToString()) != 0) { CGST_Per = txtCGST.Text.ToString(); }
            if (Convert.ToDecimal(txtSGST.Text.ToString()) != 0) { SGST_Per = txtSGST.Text.ToString(); }
            if (Convert.ToDecimal(txtIGST.Text.ToString()) != 0) { IGST_Per = txtIGST.Text.ToString(); }

            ////Discount Percent Wise Calculate
            //if (Convert.ToDecimal(Discount_Percent) != 0)
            //{
            //    Discount_Amt = (Convert.ToDecimal(Item_Total) * Convert.ToDecimal(Discount_Percent)).ToString();
            //    Discount_Amt = (Convert.ToDecimal(Discount_Amt) / Convert.ToDecimal(100)).ToString();
            //    Discount_Amt = (Math.Round(Convert.ToDecimal(Discount_Amt), 2, MidpointRounding.AwayFromZero)).ToString();
            //    //txtSubTotal.Text = (Convert.ToDecimal(hidden_fld.Value) - Convert.ToDecimal(Discount_Amt)).ToString();
            //}
            //else
            //{
            //    Discount_Amt = "0.00";
            //}

            //Tax Percentage Calculate
            if ((Convert.ToDecimal(CGST_Per) != 0) || (Convert.ToDecimal(SGST_Per) != 0) || (Convert.ToDecimal(IGST_Per) != 0))
            {
                string Item_Discount_Amt = "0.00";
                //Item_Discount_Amt = (Convert.ToDecimal(Item_Total) - Convert.ToDecimal(Discount_Amt)).ToString();
                Item_Discount_Amt = (Convert.ToDecimal(Item_Total)).ToString();

                //CGST Amount Calc
                CGST_Amt = (Convert.ToDecimal(Item_Discount_Amt) * Convert.ToDecimal(CGST_Per)).ToString();
                CGST_Amt = (Convert.ToDecimal(CGST_Amt) / Convert.ToDecimal(100)).ToString();
                CGST_Amt = (Math.Round(Convert.ToDecimal(CGST_Amt), 2, MidpointRounding.AwayFromZero)).ToString();

                //SGST Amount Calc
                SGST_Amt = (Convert.ToDecimal(Item_Discount_Amt) * Convert.ToDecimal(SGST_Per)).ToString();
                SGST_Amt = (Convert.ToDecimal(SGST_Amt) / Convert.ToDecimal(100)).ToString();
                SGST_Amt = (Math.Round(Convert.ToDecimal(SGST_Amt), 2, MidpointRounding.AwayFromZero)).ToString();


                //IGST Amount Calc
                IGST_Amt = (Convert.ToDecimal(Item_Discount_Amt) * Convert.ToDecimal(IGST_Per)).ToString();
                IGST_Amt = (Convert.ToDecimal(IGST_Amt) / Convert.ToDecimal(100)).ToString();
                IGST_Amt = (Math.Round(Convert.ToDecimal(IGST_Amt), 2, MidpointRounding.AwayFromZero)).ToString();

                All_GST_Amt = (Convert.ToDecimal(CGST_Amt) + Convert.ToDecimal(SGST_Amt) + Convert.ToDecimal(IGST_Amt)).ToString();
            }
            else
            {
                CGST_Amt = "0.00";
                SGST_Amt = "0.00";
                IGST_Amt = "0.00";
            }

            //Final Amt
            Final_Amount = (Convert.ToDecimal(Item_Total) - Convert.ToDecimal(Discount_Amt)).ToString();
            Final_Amount = (Convert.ToDecimal(Final_Amount) + Convert.ToDecimal(CGST_Amt) + Convert.ToDecimal(SGST_Amt) + Convert.ToDecimal(IGST_Amt)).ToString();
            Final_Amount = (Math.Round(Convert.ToDecimal(Final_Amount), 2, MidpointRounding.AwayFromZero)).ToString();

           // txtDiscountAmt.Text = Discount_Amt;
            txtCGSTAmt.Text = CGST_Amt;
            txtSGSTAmt.Text = SGST_Amt;
            txtIGSTAmt.Text = IGST_Amt;
            txtTotalGST.Text = All_GST_Amt;

            txtNetAmt.Text = Final_Amount;


        }
    }

    protected void RptInvioceEntry_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["CountTable"];
        if (e.CommandArgument.ToString() == "Delete")
        {
            for(int i = 0; i < dt.Rows.Count; i++)
            {
                string itemname = dt.Rows[e.Item.ItemIndex]["ItemName"].ToString().Trim().ToUpper();
                if (dt.Rows[e.Item.ItemIndex]["ItemName"].ToString().ToUpper()== "Consist Of :".ToString().ToUpper())
                {
                    for (int j = e.Item.ItemIndex;j<dt.Rows.Count; j++)
                    {
                        if (dt.Rows[e.Item.ItemIndex]["ItemName"].ToString().ToUpper() == "Consist Of :".ToString().ToUpper())
                        {
                            dt.Rows[j].Delete();
                            dt.AcceptChanges();
                        }
                        else 
                        if (Convert.ToDecimal(dt.Rows[j]["ItemCode"]) >= 1000)
                        {
                            dt.Rows[j].Delete();
                        }
                    }
                }
                else
                {
                    dt.Rows[e.Item.ItemIndex].Delete();
                }
                dt.AcceptChanges();
                ViewState["CountTable"] = dt;
                RptInvioceEntry.DataSource = dt;
                RptInvioceEntry.DataBind();

                break;
            }
            //ViewState["CountTable"]=
            Totalsum();
        }
    }

    protected void txtPackAmt_TextChanged(object sender, EventArgs e)
    {
        Totalsum();
    }

    protected void txtCourierAmt_TextChanged(object sender, EventArgs e)
    {
        Totalsum();
    }

    protected void txtService_TextChanged(object sender, EventArgs e)
    {
        Totalsum();
    }

    protected void btnAddConsistitem_Click(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["CountTable"];
        DataRow dr = null;
        bool cocsiststringpresent = false;
        bool ErrFlag = false;
        if (dt.Rows.Count <= 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Please Add atleast one Main Item for this invoice');", true);
        }
        else if(dt.Rows.Count<=8)
        {
            for(int i = 0; i < dt.Rows.Count; i++)
            {
                if (dt.Rows[i]["ItemName"].ToString().ToUpper().Trim() == ddlConsistItemName.SelectedItem.Text.ToString().ToUpper().Trim())
                {
                    ErrFlag = true;
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('This Item Already Added..');", true);
                }
                if(dt.Rows[i]["ItemName"].ToString().ToUpper().Trim()==("Consist of :").ToString().ToUpper().Trim())
                {
                    cocsiststringpresent = true;
                }
            }
            if (!ErrFlag)
            {
                if (!cocsiststringpresent)
                {
                    dr = dt.NewRow();
                    dr["ItemCode"] = "Consist Of :";
                    dr["ItemName"] = "Consist Of :";
                    dt.Rows.Add(dr);
                    dr = null;
                }
                dr = dt.NewRow();
                dr["ItemCode"] = ddlConsistItemName.SelectedValue;
                dr["ItemName"] = ddlConsistItemName.SelectedItem.Text;
                dt.Rows.Add(dr);
                ViewState["CountTable"] = dt;
                RptInvioceEntry.DataSource = dt;
                RptInvioceEntry.DataBind();
            }
            ddlConsistItemName.ClearSelection();
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You have to reach the Maximum item in this Invoince');", true);
        }
    }
}
