﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Data.SqlClient;
public partial class Transaction_OE_Details : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionUnplanReceiptNo;
    string SessionFinYearCode;
    string SessionFinYearVal;
    static string sum;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "ERP Module :: OE";
            HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Transaction"));
            li.Attributes.Add("class", "droplink active open");
            Initial_Data_Referesh();
            Load_Supplier();
            Load_Shift();
            LoadUnit();
            Get_Repeater();
            //Load_Machine();
            //Load_Count();
            
            Load_YarnLot();
            //Color();
            if (Session["Trans_No"] == null)
            {
                SessionUnplanReceiptNo = "";
                
            }
            else
            {
                SessionUnplanReceiptNo = Session["Trans_No"].ToString();
                txtReceiptNo.Text = SessionUnplanReceiptNo;
                btnSearch_Click(sender, e);
                btnSave.Text = "Update";
            }
        }
        Load_OLD_data();
    }

    //private void Load_Count()
    //{
    //    string query = "";
    //    DataTable dtdsupp = new DataTable();
    //    ddlCount.Items.Clear();
    //    query = "Select * from MstEmployee where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
    //    dtdsupp = objdata.RptEmployeeMultipleDetails(query);
    //    txtSupplierName.DataSource = dtdsupp;
    //    DataRow dr = dtdsupp.NewRow();
    //    dr["EmpName"] = "-Select-";
    //    dr["EmpCode"] = "-Select-";
    //    dtdsupp.Rows.InsertAt(dr, 0);
    //    ddlCount.DataTextField = "EmpName";
    //    ddlCount.DataValueField = "EmpCode";
    //    ddlCount.DataBind();
    //}

    private void Load_YarnLot()
    {
        //string query = "";
        //DataTable dtdsupp = new DataTable();
        //ddlYarnLotNo.Items.Clear();
        //query = "Select * from LotNo_Stage where Types='OE' order by CAST(YarnLotNo as int) asc";
        //dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        //ddlYarnLotNo.DataSource = dtdsupp;
        //DataRow dr = dtdsupp.NewRow();
        //dr["YarnLotNo"] = "-Select-";
        //dr["YarnLotNo"] = "-Select-";
        //dtdsupp.Rows.InsertAt(dr, 0);
        //ddlYarnLotNo.DataTextField = "YarnLotNo";
        //ddlYarnLotNo.DataValueField = "YarnLotNo";
        //ddlYarnLotNo.DataBind();

    }



    public void LoadUnit()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlUnit.Items.Clear();
        query = "Select *from MstUnit where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlUnit.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["UnitName"] = "-Select-";
        dr["UnitName"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlUnit.DataTextField = "UnitName";
        ddlUnit.DataValueField = "UnitName";
        ddlUnit.DataBind();
    }
    private void Load_Supplier()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        txtSupplierName.Items.Clear();
        query = "Select * from MstEmployee where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        txtSupplierName.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["EmpName"] = "-Select-";
        dr["EmpCode"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        txtSupplierName.DataTextField = "EmpName";
        txtSupplierName.DataValueField = "EmpCode";
        txtSupplierName.DataBind();
    }
    //private void Load_Count()
    //{
    //    string query = "";
    //    DataTable dtdsupp = new DataTable();
    //    ddlCount.Items.Clear();
    //    query = "Select * from MstCount where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
    //    dtdsupp = objdata.RptEmployeeMultipleDetails(query);
    //    ddlCount.DataSource = dtdsupp;
    //    DataRow dr = dtdsupp.NewRow();
    //    dr["CountName"] = "-Select-";
    //    dr["CountID"] = "-Select-";
    //    dtdsupp.Rows.InsertAt(dr, 0);
    //    ddlCount.DataTextField = "CountName";
    //    ddlCount.DataValueField = "CountID";
    //    ddlCount.DataBind();
    //}
    //private void Load_Machine()
    //{
    //    string query = "";
    //    DataTable dtdsupp = new DataTable();
    //    ddlMachineName.Items.Clear();
    //    query = "Select * from MstMachine where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And DeptName='OE'";
    //    dtdsupp = objdata.RptEmployeeMultipleDetails(query);
    //    ddlMachineName.DataSource = dtdsupp;
    //    DataRow dr = dtdsupp.NewRow();
    //    dr["MachineName"] = "-Select-";
    //    dr["MachineCode"] = "-Select-";
    //    dtdsupp.Rows.InsertAt(dr, 0);
    //    ddlMachineName.DataTextField = "MachineName";
    //    ddlMachineName.DataValueField = "MachineCode";
    //    ddlMachineName.DataBind();
    //}
    private void Load_Shift()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlShift.Items.Clear();
        query = "Select ShiftName from MstShift where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlShift.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["ShiftName"] = "-Select-";
        dr["ShiftName"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlShift.DataTextField = "ShiftName";
        ddlShift.DataValueField = "ShiftName";
        ddlShift.DataBind();
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        //Search Purchase Enquiry
        string query = "";
        DataTable Main_DT = new DataTable();
        query = "Select * from OE_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Trans_No='" + txtReceiptNo.Text + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        if (Main_DT.Rows.Count != 0)
        {
            txtDate.Text = Main_DT.Rows[0]["Trans_Date"].ToString();
            txtSupplierName.SelectedValue = Main_DT.Rows[0]["SupervisorName"].ToString().Trim();
            ddlShift.Text = Main_DT.Rows[0]["ShiftName"].ToString();
            //ddlYarnLotNo.SelectedValue = Main_DT.Rows[0]["LotNo"].ToString().Trim();
            //txtSilver.Text = Main_DT.Rows[0]["SliverWaste"].ToString();

            //ddlColor.SelectedValue = Main_DT.Rows[0]["Color"].ToString().Trim();

            ddlUnit.SelectedValue = Main_DT.Rows[0]["Unit"].ToString().Trim();
            ddlUnit_SelectedIndexChanged(sender, e);
            
           // txtRouter.Text = Main_DT.Rows[0]["RouterWaste"].ToString();

           // txtStopPage.Text = Main_DT.Rows[0]["StoppageMins"].ToString();
           // txtRemarks.Text = Main_DT.Rows[0]["StoppageReason"].ToString();

            //Unplanned_Receipt_Main_Sub Table Load
            DataTable dt = new DataTable();
            query = "Select *from OE_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Trans_No='" + txtReceiptNo.Text + "'";
            dt = objdata.RptEmployeeMultipleDetails(query);

            DataTable Temp = (DataTable)ViewState["ItemTable"];

            for (int i = 0; i < Temp.Rows.Count; i++)
            {
                for (int j = 0; j < dt.Rows.Count; j++)
                {
                    if ((Temp.Rows[i]["MachineName"].ToString() == dt.Rows[j]["MachineName"].ToString()) && (Temp.Rows[i]["Unit"].ToString() == dt.Rows[j]["Unit"].ToString()) && (Temp.Rows[i]["Side"].ToString() == dt.Rows[j]["Side"].ToString()))
                    {
                        Temp.Rows[i]["MachineCode"] = dt.Rows[j]["MachineCode"].ToString();
                        Temp.Rows[i]["MachineName"] = dt.Rows[j]["MachineName"].ToString();
                        Temp.Rows[i]["CountID"] = dt.Rows[j]["CountID"].ToString();
                        Temp.Rows[i]["CountName"] = dt.Rows[j]["CountName"].ToString();
                        Temp.Rows[i]["StdProd"] = dt.Rows[j]["StdProd"].ToString();
                        Temp.Rows[i]["ActProd"] = dt.Rows[j]["ActProd"].ToString();
                        Temp.Rows[i]["SliverWaste"] = dt.Rows[j]["SliverWaste"].ToString();
                        Temp.Rows[i]["WorkRoter"] = dt.Rows[j]["WorkRoter"].ToString();
                        Temp.Rows[i]["NoofRoter"] = dt.Rows[j]["NoofRoter"].ToString();
                        Temp.Rows[i]["StopRoter"] = dt.Rows[j]["StopRoter"].ToString();
                        Temp.Rows[i]["RouterWaste"] = dt.Rows[j]["RouterWaste"].ToString();
                        Temp.Rows[i]["StoppageMins"] = dt.Rows[j]["StoppageMins"].ToString();

                        Temp.Rows[i]["StoppageReason"] = dt.Rows[j]["StoppageReason"].ToString();
                        // Temp.Rows[i]["Delivery"] = dt.Rows[i]["Delivery"].ToString();
                        Temp.Rows[i]["Utility"] = dt.Rows[j]["Utility"].ToString();
                        Temp.Rows[i]["Efficiency"] = dt.Rows[j]["Efficiency"].ToString();

                        Temp.Rows[i]["LotNo"] = dt.Rows[j]["LotNo"].ToString();
                        Temp.Rows[i]["Color"] = dt.Rows[j]["Color"].ToString();

                        Temp.Rows[i]["Unit"] = dt.Rows[j]["Unit"].ToString();
                        Temp.Rows[i]["Side"] = dt.Rows[j]["Side"].ToString();
                    }

                }
            }
            //Temp.Rows.Clear();
            //for (int i = 0; i < dt.Rows.Count; i++)
            //{
            //    Temp.NewRow();
            //    Temp.Rows.Add();
            //    Temp.Rows[Temp.Rows.Count - 1]["MachineCode"] = dt.Rows[i]["MachineCode"].ToString();
            //    Temp.Rows[Temp.Rows.Count - 1]["MachineName"] = dt.Rows[i]["MachineName"].ToString();
            //    Temp.Rows[Temp.Rows.Count - 1]["CountID"] = dt.Rows[i]["CountID"].ToString();
            //    Temp.Rows[Temp.Rows.Count - 1]["CountName"] = dt.Rows[i]["CountName"].ToString();
            //    Temp.Rows[Temp.Rows.Count - 1]["StdProd"] = dt.Rows[i]["StdProd"].ToString();
            //    Temp.Rows[Temp.Rows.Count - 1]["ActProd"] = dt.Rows[i]["ActProd"].ToString();
            //    Temp.Rows[Temp.Rows.Count - 1]["SliverWaste"] = dt.Rows[i]["SliverWaste"].ToString();
            //    Temp.Rows[Temp.Rows.Count - 1]["WorkRoter"] = dt.Rows[i]["WorkRoter"].ToString();
            //    Temp.Rows[Temp.Rows.Count - 1]["NoofRoter"] = dt.Rows[i]["NoofRoter"].ToString();
            //    Temp.Rows[Temp.Rows.Count - 1]["StopRoter"] = dt.Rows[i]["StopRoter"].ToString();
            //    Temp.Rows[Temp.Rows.Count - 1]["RouterWaste"] = dt.Rows[i]["RouterWaste"].ToString();
            //    Temp.Rows[Temp.Rows.Count - 1]["StoppageMins"] = dt.Rows[i]["StoppageMins"].ToString();

            //    Temp.Rows[Temp.Rows.Count - 1]["StoppageReason"] = dt.Rows[i]["StoppageReason"].ToString();
            //    // Temp.Rows[Temp.Rows.Count - 1]["Delivery"] = dt.Rows[i]["Delivery"].ToString();
            //    Temp.Rows[Temp.Rows.Count - 1]["Utility"] = dt.Rows[i]["Utility"].ToString();
            //    Temp.Rows[Temp.Rows.Count - 1]["Efficiency"] = dt.Rows[i]["Efficiency"].ToString();

            //    Temp.Rows[Temp.Rows.Count - 1]["LotNo"] = dt.Rows[i]["LotNo"].ToString();
            //    Temp.Rows[Temp.Rows.Count - 1]["Color"] = dt.Rows[i]["Color"].ToString();

            //    Temp.Rows[Temp.Rows.Count - 1]["Unit"] = dt.Rows[i]["Unit"].ToString();
            //    Temp.Rows[Temp.Rows.Count - 1]["Side"] = dt.Rows[i]["Side"].ToString();

            //}


            //ViewState["ItemTable"] = Temp;
            //Repeater1.DataSource = Temp;
            //Repeater1.DataBind();
            
           
            //Temp = (DataTable)ViewState["ItemTable"];
            //if (Temp.Rows.Count != 0)
            //{
            //    int k = 0;
            //    foreach (RepeaterItem itemEquipment in Repeater1.Items)
            //    {
            //        DropDownList yourDropDown = (DropDownList)itemEquipment.FindControl("ddlCount");

            //        //to get the selected value of your dropdownlist
            //        yourDropDown.SelectedValue = Temp.Rows[k]["CountID"].ToString();


            //        //DropDownList LotDropDown = (DropDownList)itemEquipment.FindControl("ddlYarnLotNo");

            //        ////to get the selected value of your dropdownlist
            //        //LotDropDown.SelectedValue = Temp.Rows[k]["LotNo"].ToString();

            //        DropDownList ColorDropDown = (DropDownList)itemEquipment.FindControl("ddlColor");

            //        //to get the selected value of your dropdownlist
            //        ColorDropDown.SelectedValue = Temp.Rows[k]["Color"].ToString();
            //        k++;
            //    }
            //}
        }
    }
    protected void GridDeleteClick(object sender, CommandEventArgs e)
    {
        DataTable qry_dt = new DataTable();
        bool ErrFlag = false;
        string qry = "";


        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            if ((dt.Rows[i]["MachineCode"].ToString() == e.CommandName.ToString()) && (dt.Rows[i]["SideArea"].ToString() == e.CommandArgument.ToString()))
            {
                dt.Rows.RemoveAt(i);
                dt.AcceptChanges();
            }
        }
        ViewState["ItemTable"] = dt;
        Load_OLD_data();
       
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        string query = "";
        DataTable DT_Check = new DataTable();
        DataTable qry_dt = new DataTable();
        string SaveMode = "Insert";
        bool ErrFlag = false;


        DataRow dr = null;

        if (ViewState["ItemTable"] != null)
        {
            DataTable dt = (DataTable)ViewState["ItemTable"];

            int i = 0;

            dt.Rows.Clear();

            int count = 0;

            foreach (RepeaterItem item in Repeater1.Items)
            {
                Label txtMachineCode = (Label)item.FindControl("txtMachineCode");
                Label txtMachineName = (Label)item.FindControl("txtMachineName");
                //DropDownList CountDropDown = (DropDownList)item.FindControl("ddlCount");
               
                //string txtCountID = CountDropDown.SelectedValue;
                //txtCountID.Text = value.ToString();
                Label txtCount = (Label)item.FindControl("txtCount"); ;
                TextBox txtStdProd = (TextBox)item.FindControl("txtStdProd");
                Label txtSide = (Label)item.FindControl("txtSide");
                Label txtUnit = (Label)item.FindControl("txtUnit");


                //DropDownList LotDropDown = (DropDownList)item.FindControl("ddlYarnLotNo");
                //string LotNo = LotDropDown.SelectedValue;

                DropDownList ColorDropDown = (DropDownList)item.FindControl("ddlColor");
                string Color = ColorDropDown.SelectedValue;

                //Label txtSide = (Label)item.FindControl("txtSide");
                TextBox txtNoofRoter = (TextBox)item.FindControl("txtNoofRoter");
                TextBox txtWorkRoter = (TextBox)item.FindControl("txtWorkRoter");

                TextBox txtActProd = (TextBox)item.FindControl("txtActProd");
                TextBox txtLotNo = (TextBox)item.FindControl("txtLotNo");


                TextBox txtStopRoter = (TextBox)item.FindControl("txtStopRoter");
                TextBox txtUtilization = (TextBox)item.FindControl("txtUtilization");
                TextBox txtEfficency = (TextBox)item.FindControl("txtEfficency");


                TextBox txtSilver = (TextBox)item.FindControl("txtSilver");
                TextBox txtRouterWaste = (TextBox)item.FindControl("txtRouterWaste");
                //TextBox txtDelivery = (TextBox)item.FindControl("txtDelivery");

                TextBox txtStoppageMin = (TextBox)item.FindControl("txtStoppageMin");
                //TextBox txtReason = (TextBox)item.FindControl("txtReason");
                DropDownList txtReasonDropDown = (DropDownList)item.FindControl("txtReason");
                string txtReason = txtReasonDropDown.SelectedValue;


                if (count == 0)
                {
                    if (txtActProd.Text == "" && txtStoppageMin.Text == "" && txtEfficency.Text == "" && txtReason == "")
                    {
                        ErrFlag = true;
                        break;

                    }
                    else
                    {
                        dr = dt.NewRow();
                        dr["MachineCode"] = txtMachineCode.Text;
                        dr["MachineName"] = txtMachineName.Text;
                        dr["CountID"] = txtCount.Text;
                        dr["CountName"] = txtCount.Text;
                        dr["Unit"] = txtUnit.Text;
                        dr["Side"] = txtSide.Text;
                        dr["StdProd"] = txtStdProd.Text;
                        dr["ActProd"] = txtActProd.Text;
                        dr["SliverWaste"] = txtSilver.Text;
                        dr["WorkRoter"] = txtWorkRoter.Text;
                        dr["NoofRoter"] = txtNoofRoter.Text;
                        dr["StopRoter"] = txtStopRoter.Text;
                        dr["RouterWaste"] = txtRouterWaste.Text;
                        dr["StoppageMins"] = txtStoppageMin.Text;
                        dr["StoppageReason"] = txtReason ;
                        //dr["Delivery"] = txtDelivery.Text;
                        dr["Utility"] = txtUtilization.Text;
                        dr["Efficiency"] = txtEfficency.Text;
                        dr["LotNo"] = txtLotNo.Text;
                        dr["Color"] = Color;
                        dt.Rows.Add(dr);
                        ViewState["ItemTable"] = dt;


                        txtMachineCode.Text = "";
                        txtMachineName.Text = "";
                        txtCount.Text = ""; txtUnit.Text = "";
                        txtSide.Text = ""; txtUnit.Text = "";
                        txtStdProd.Text = "";
                        txtActProd.Text = "";
                        txtSilver.Text = "";
                        txtWorkRoter.Text = "";
                        txtNoofRoter.Text = "";
                        txtStopRoter.Text = "";
                        txtRouterWaste.Text = "";
                        //txtDelivery.Text = "";
                        txtStoppageMin.Text = "";
                        txtReason = "";
                        txtUtilization.Text = "";
                        txtEfficency.Text = "";
                        Color = "";

                    }
                }
                else
                {
                    dr = dt.NewRow();
                    dr["MachineCode"] = txtMachineCode.Text;
                    dr["MachineName"] = txtMachineName.Text;
                    dr["CountID"] = txtCount.Text;
                    dr["CountName"] = txtCount.Text;
                    dr["Unit"] = txtUnit.Text;
                    dr["Side"] = txtSide.Text;
                    dr["StdProd"] = txtStdProd.Text;
                    dr["ActProd"] = txtActProd.Text;
                    dr["SliverWaste"] = txtSilver.Text;
                    dr["WorkRoter"] = txtWorkRoter.Text;
                    dr["NoofRoter"] = txtNoofRoter.Text;
                    dr["StopRoter"] = txtStopRoter.Text;
                    dr["RouterWaste"] = txtRouterWaste.Text;
                    dr["StoppageMins"] = txtStoppageMin.Text;
                    dr["StoppageReason"] = txtReason;
                    //dr["Delivery"] = txtDelivery.Text;
                    dr["Utility"] = txtUtilization.Text;
                    dr["Efficiency"] = txtEfficency.Text;
                    dr["LotNo"] = txtLotNo.Text;
                    dr["Color"] = Color;
                    dt.Rows.Add(dr);
                    ViewState["ItemTable"] = dt;


                    txtMachineCode.Text = "";
                    txtMachineName.Text = "";
                    txtCount.Text = ""; txtLotNo.Text = ""; txtSide.Text = ""; txtUnit.Text = "";
                    txtStdProd.Text = "";
                    txtActProd.Text = "";
                    txtSilver.Text = "";
                    txtWorkRoter.Text = "";
                    txtNoofRoter.Text = "";
                    txtStopRoter.Text = "";
                    txtRouterWaste.Text = "";
                    //txtDelivery.Text = "";
                    txtStoppageMin.Text = "";
                    txtReason = "";
                    txtUtilization.Text = "";
                    txtEfficency.Text = "";
                    Color = "";
                }
                count = count + 1;
            }
        }
        //check with Item Details Add with Grid
        DT_Check = (DataTable)ViewState["ItemTable"];
        if (DT_Check.Rows.Count == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You have to add atleast one Item Details..');", true);
        }

        
        //if (ddlColor.SelectedValue == "0" || ddlColor.SelectedItem.Text == "-Select-")
        //{
        //    ErrFlag = true;
        //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select Any One Color First..');", true);
        //}
        if (ddlShift.SelectedValue == "0" || ddlShift.SelectedItem.Text == "-Select-")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select Any One Shift First..');", true);
        }
        //if (ddlYarnLotNo.SelectedValue == "0" || ddlYarnLotNo.SelectedItem.Text == "-Select-")
        //{
        //    ErrFlag = true;
        //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select Any One Lot No First..');", true);
        //}


        if (txtDate.Text.Length < 10 || txtDate.Text.Length > 10)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the Date properly..');", true);
        }


        //User Rights Check Start
        bool Rights_Check = false;
        //if (btnSave.Text == "Update")
        //{
        //    Rights_Check = CommonClass_Function.ModifyRights_Check(SessionCcode, SessionLcode, SessionUserID, "7", "2", "Unplanned Receipt");
        //    if (Rights_Check == false)
        //    {
        //        ErrFlag = true;
        //        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Modify Unplanned Receipt Details..');", true);
        //    }
        //}
        //else
        //{
        //    Rights_Check = CommonClass_Function.AddRights_Check(SessionCcode, SessionLcode, SessionUserID, "7", "2", "Unplanned Receipt");
        //    if (Rights_Check == false)
        //    {
        //        ErrFlag = true;
        //        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Adding New Unplanned Receipt..');", true);
        //    }
        //}
        //User Rights Check End

        //Auto generate Transaction Function Call
        if (btnSave.Text != "Update")
        {
            if (!ErrFlag)
            {
                TransactionNoGenerate TransNO = new TransactionNoGenerate();
                string Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup(SessionCcode, SessionLcode, "OE Production", SessionFinYearVal);
                if (Auto_Transaction_No == "")
                {
                    ErrFlag = true;
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Auto Generate Transaction No Error... Contact Server Admin');", true);
                }
                else
                {
                    txtReceiptNo.Text = Auto_Transaction_No;
                }
            }
        }

        //Check With Already Approved Start
        DataTable DT_Check_Apprv = new DataTable();
        query = "Select * from OE_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Trans_No='" + txtReceiptNo.Text + "' And Status='1'";
        DT_Check_Apprv = objdata.RptEmployeeMultipleDetails(query);
        if (DT_Check_Apprv.Rows.Count != 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Do not Update Unplanned Receipt Details Already Approved..');", true);
        }

        if (!ErrFlag)
        {
            query = "Select * from OE_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Trans_No='" + txtReceiptNo.Text + "'";
            DT_Check = objdata.RptEmployeeMultipleDetails(query);
            if (DT_Check.Rows.Count != 0)
            {
                SaveMode = "Update";
                query = "Delete from OE_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Trans_No='" + txtReceiptNo.Text + "'";
                objdata.RptEmployeeMultipleDetails(query);
                query = "Delete from OE_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Trans_No='" + txtReceiptNo.Text + "'";
                objdata.RptEmployeeMultipleDetails(query);
            }

            //Insert Main Table
            query = "Insert Into OE_Main(Ccode,Lcode,FinYearCode,FinYearVal,Trans_No,Trans_Date,ShiftName,SupervisorName,UserID,UserName,Status,Unit,Color) Values('" + SessionCcode + "',";
            query = query + " '" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + txtReceiptNo.Text + "','" + txtDate.Text + "','" + ddlShift.SelectedItem.Text + "','" + txtSupplierName.SelectedValue + "',";
            query = query + " '" + SessionUserID + "','" + SessionUserName + "','0','" + ddlUnit.SelectedItem.Text + "','0')";
            objdata.RptEmployeeMultipleDetails(query);

            DataTable dt = new DataTable();
            dt = (DataTable)ViewState["ItemTable"];
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (dt.Rows[i]["ActProd"].ToString() != "")
                {
                    query = "Insert Into OE_Sub(Ccode,Lcode,FinYearCode,FinYearVal,Trans_No,Trans_Date,MachineCode,MachineName,CountID,CountName,StdProd,ActProd,Utility,Efficiency,";
                    query = query + "SliverWaste,WorkRoter,NoofRoter,StopRoter,RouterWaste,StoppageMins,StoppageReason,UserID,UserName,LotNo,Color,Unit,Side) Values('" + SessionCcode + "',";
                    query = query + " '" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + txtReceiptNo.Text + "','" + txtDate.Text + "','" + dt.Rows[i]["MachineCode"].ToString() + "','" + dt.Rows[i]["MachineName"].ToString() + "',";
                    query = query + " '" + dt.Rows[i]["CountID"].ToString() + "','" + dt.Rows[i]["CountName"].ToString() + "','" + dt.Rows[i]["StdProd"].ToString() + "','" + dt.Rows[i]["ActProd"].ToString() + "','" + dt.Rows[i]["Utility"].ToString() + "','" + dt.Rows[i]["Efficiency"].ToString() + "',";
                    query = query + " '" + dt.Rows[i]["SliverWaste"].ToString() + "','" + dt.Rows[i]["WorkRoter"].ToString() + "','" + dt.Rows[i]["NoofRoter"].ToString() + "','" + dt.Rows[i]["StopRoter"].ToString() + "',";
                    query = query + " '" + dt.Rows[i]["RouterWaste"].ToString() + "','" + dt.Rows[i]["StoppageMins"].ToString() + "','" + dt.Rows[i]["StoppageReason"].ToString() + "','" + SessionUserID + "','" + SessionUserName + "','" + dt.Rows[i]["LotNo"].ToString() + "','" + dt.Rows[i]["Color"].ToString() + "','" + dt.Rows[i]["Unit"].ToString() + "','" + dt.Rows[i]["Side"].ToString() + "')";
                    objdata.RptEmployeeMultipleDetails(query);
                }
              
            }

            if (SaveMode == "Insert")
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('OE Details Saved Successfully');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('OE Details Updated Successfully');", true);
            }
            //Clear_All_Field();
            if (!ErrFlag)
            {
                Session["Trans_No"] = txtReceiptNo.Text;
                btnSave.Text = "Update";
                Response.Redirect("OE_Main.aspx");
                //Load_Data_Enquiry_Grid();
            }


        }

    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        txtReceiptNo.Text = ""; txtDate.Text = "";
        ddlShift.SelectedValue = "-Select-";
        txtSupplierName.SelectedValue = "-Select-";
        ddlUnit.SelectedValue = "-Select-";
        Initial_Data_Referesh();

        btnSave.Text = "Save";
        Session.Remove("Trans_No");
    }


    protected void btnBackEnquiry_Click(object sender, EventArgs e)
    {
        Session.Remove("Trans_No");
        Response.Redirect("OE_Main.aspx");
    }

    protected void btnApprove_Click(object sender, EventArgs e)
    {
        string query = "";
        DataTable DT_Check = new DataTable();
        bool ErrFlag = false;

        //User Rights Check Start
        bool Rights_Check = false;
        //Rights_Check = CommonClass_Function.ApproveRights_Check(SessionCcode, SessionLcode, SessionUserID, "7", "2", "Unplanned Receipt");
        //if (Rights_Check == false)
        //{
        //    ErrFlag = true;
        //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Approve Unplanned Receipt...');", true);
        //}
        //User Rights Check End

        query = "Select * from OE_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Trans_No='" + txtReceiptNo.Text + "'";
        DT_Check = objdata.RptEmployeeMultipleDetails(query);
        if (DT_Check.Rows.Count == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('First You have to Register this Service Receipt Details..');", true);
        }

        query = "Select * from OE_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Trans_No='" + txtReceiptNo.Text + "' And Status='1'";
        DT_Check = objdata.RptEmployeeMultipleDetails(query);
        if (DT_Check.Rows.Count != 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Service Receipt Details Already Approved..');", true);
        }

        DataTable DT_Sub = new DataTable();
       
        if (!ErrFlag)
        {
            query = "Update OE_Main set Status='1' where Ccode='" + SessionCcode + "'";
            query = query + "  And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
            query = query + "  And Trans_No='" + txtReceiptNo.Text + "'";
            objdata.RptEmployeeMultipleDetails(query);

            //query = "Select * from OE_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Trans_No='" + txtReceiptNo.Text + "'";
            //DT_Sub = objdata.RptEmployeeMultipleDetails(query);
            //for (int k = 0; k < DT_Sub.Rows.Count; k++)
            //{
            //    if (DT_Sub.Rows[k]["LotNo"].ToString() != "0")
            //    {
            //        query = "Update LotNo_Stage set Types='Winding' where YarnLotNo='" + DT_Sub.Rows[k]["LotNo"].ToString() + "' and Unit='" + ddlUnit.SelectedValue + "' and ColorName='" + DT_Sub.Rows[k]["Color"].ToString() + "'";
            //        objdata.RptEmployeeMultipleDetails(query);
            //    }
            //}
            //Stock_Add();

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('OE Receipt Details Approved Successfully..');", true);
            Response.Redirect("OE_Main.aspx");
        }

    }

    private void Load_OLD_data()
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        Repeater1.DataSource = dt;
        Repeater1.DataBind();

        DataTable Temp = (DataTable)ViewState["ItemTable"];
        if (Temp.Rows.Count != 0)
        {
            int k = 0;
            foreach (RepeaterItem itemEquipment in Repeater1.Items)
            {
                //DropDownList yourDropDown = (DropDownList)itemEquipment.FindControl("ddlCount");

                ////to get the selected value of your dropdownlist
                //yourDropDown.SelectedValue = Temp.Rows[k]["CountID"].ToString();


                //DropDownList LotDropDown = (DropDownList)itemEquipment.FindControl("ddlYarnLotNo");

                ////to get the selected value of your dropdownlist
                //LotDropDown.SelectedValue = Temp.Rows[k]["LotNo"].ToString();

                DropDownList ColorDropDown = (DropDownList)itemEquipment.FindControl("ddlColor");

                //to get the selected value of your dropdownlist
                ColorDropDown.SelectedValue = Temp.Rows[k]["Color"].ToString();



                DropDownList ReasonDropDown = (DropDownList)itemEquipment.FindControl("txtReason");

                //to get the selected value of your dropdownlist
                ReasonDropDown.SelectedValue = Temp.Rows[k]["StoppageReason"].ToString();
                k++;
            }
        }
    }

    private void Initial_Data_Referesh()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add(new DataColumn("Unit", typeof(string)));
        dt.Columns.Add(new DataColumn("MachineCode", typeof(string)));
        dt.Columns.Add(new DataColumn("MachineName", typeof(string)));
        dt.Columns.Add(new DataColumn("StdProd", typeof(string)));
        dt.Columns.Add(new DataColumn("CountID", typeof(string)));
        dt.Columns.Add(new DataColumn("CountName", typeof(string)));
        dt.Columns.Add(new DataColumn("Side", typeof(string)));
        dt.Columns.Add(new DataColumn("LotNo", typeof(string)));
        dt.Columns.Add(new DataColumn("Color", typeof(string)));
        dt.Columns.Add(new DataColumn("ActProd", typeof(string)));
        dt.Columns.Add(new DataColumn("SliverWaste", typeof(string)));
        dt.Columns.Add(new DataColumn("WorkRoter", typeof(string)));
        dt.Columns.Add(new DataColumn("NoofRoter", typeof(string)));
        dt.Columns.Add(new DataColumn("StopRoter", typeof(string)));
        dt.Columns.Add(new DataColumn("RouterWaste", typeof(string)));
        dt.Columns.Add(new DataColumn("StoppageMins", typeof(string)));
        dt.Columns.Add(new DataColumn("StoppageReason", typeof(string)));
        //dt.Columns.Add(new DataColumn("Delivery", typeof(string)));
        dt.Columns.Add(new DataColumn("Utility", typeof(string)));
        dt.Columns.Add(new DataColumn("Efficiency", typeof(string)));


        Repeater1.DataSource = dt;
        Repeater1.DataBind();
        ViewState["ItemTable"] = Repeater1.DataSource;

        //dt = Repeater1.DataSource;
    }
    
    //protected void ddlMachineName_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    DataTable dt = new DataTable();
    //    string Query = "select ProdTarget,WorkRoter from MstMachine where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and MachineCode='" + ddlMachineName.SelectedValue + "'";
    //    dt = objdata.RptEmployeeMultipleDetails(Query);
    //    if (dt.Rows.Count > 0)
    //    {
    //        txtStdProd.Text = dt.Rows[0]["ProdTarget"].ToString();
    //        txtNoofRoter.Text = dt.Rows[0]["WorkRoter"].ToString();
    //    }
    //    else
    //    {
    //        txtStdProd.Text = "0";
    //        txtNoofRoter.Text = "0";
    //    }
    //}
    //public void Act_Calc()
    //{
    //    string Act_Prod = "";
    //    if (txtActProd.Text != "" && txtStdProd.Text != "")
    //    {
    //        Act_Prod = ((Convert.ToDecimal(txtActProd.Text) / (Convert.ToDecimal(txtStdProd.Text))) * (100)).ToString();
    //        Act_Prod = (Math.Round(Convert.ToDecimal(Act_Prod), 2, MidpointRounding.AwayFromZero)).ToString();
    //    }

    //    txtEfficiency.Text = Act_Prod;
    //}

    protected void txtActProd_TextChanged(object sender, EventArgs e)
    {
        //Act_Calc();
    }

    protected void ddlYarnLotNo_SelectedIndexChanged(object sender, EventArgs e)
    {
        //DataTable dt = new DataTable();
        //string Query = "Select Unit,ColorName from LotNo_Stage where YarnLotNo='" + ddlYarnLotNo.SelectedValue + "' and Types='OE'";
        //dt = objdata.RptEmployeeMultipleDetails(Query);
        //if (dt.Rows.Count > 0)
        //{
        //    ddlUnit.SelectedValue = dt.Rows[0]["Unit"].ToString().Trim();
        //    ddlColor.SelectedValue = dt.Rows[0]["ColorName"].ToString().Trim();
        //    Get_Repeater(ddlUnit.SelectedValue);
        //}
        //else
        //{
        //    Color();
        //    LoadUnit();
        //}
    }

    protected void ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        //if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        //{
        //    DropDownList country = e.Item.FindControl("ddlCount") as DropDownList;
        //    PopulateDropDownList(country, "CountName", "CountID");
        //    country.Items.Insert(0, new ListItem("-Select-", "0"));
        //    country.Items.FindByValue("0").Selected = true;

        //    //string degreeCode = (string)((DataRowView)e.Item.DataItem)["CountID"];
        //    //if (country.Items.FindByValue(degreeCode) != null)
        //    //{
        //    //    country.SelectedValue = degreeCode;
        //    //}
        //}


        if (e.Item.ItemType == ListItemType.Item ||
        e.Item.ItemType == ListItemType.AlternatingItem)
        {
            //var degree_dropdown = e.Item.FindControl("ddlCount") as DropDownList;
            ////PopulateDropDownList(degree_dropdown, "CountName", "CountID");
            //if (degree_dropdown != null)
            //{

            //    DataTable dtdsupp = new DataTable();
            //    degree_dropdown.Items.Clear();
            //    string query = "Select * from MstCount where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
            //    dtdsupp = objdata.RptEmployeeMultipleDetails(query);
            //    degree_dropdown.DataSource = dtdsupp;
            //    //DataRow dr = dtdsupp.NewRow();
            //    //dr["EmpName"] = "-Select-";
            //    //dr["EmpCode"] = "-Select-";
            //    //dtdsupp.Rows.InsertAt(dr, 0);
            //    degree_dropdown.DataTextField = "CountName";
            //    degree_dropdown.DataValueField = "CountID";
            //    degree_dropdown.DataBind();
            //    degree_dropdown.Items.Insert(0, new ListItem("-Select-", "0"));
            //    degree_dropdown.Items.FindByValue("0").Selected = true;

                
            //}

            //var Lot_dropdown = e.Item.FindControl("ddlYarnLotNo") as DropDownList;
            ////PopulateDropDownList(degree_dropdown, "CountName", "CountID");
            //if (Lot_dropdown != null)
            //{

            //    DataTable dtdsupp = new DataTable();
            //    Lot_dropdown.Items.Clear();

            //    string query = "Select  Distinct YarnLotNo from LotNo_Stage where Types='OE' and Unit='" + ddlUnit.SelectedValue + "'";
            //    dtdsupp = objdata.RptEmployeeMultipleDetails(query);
            //    Lot_dropdown.DataSource = dtdsupp;
            //    //DataRow dr = dtdsupp.NewRow();
            //    //dr["EmpName"] = "-Select-";
            //    //dr["EmpCode"] = "-Select-";
            //    //dtdsupp.Rows.InsertAt(dr, 0);
            //    Lot_dropdown.DataTextField = "YarnLotNo";
            //    Lot_dropdown.DataValueField = "YarnLotNo";
            //    Lot_dropdown.DataBind();
            //    Lot_dropdown.Items.Insert(0, new ListItem("-Select-", "0"));
            //    Lot_dropdown.Items.FindByValue("0").Selected = true;


            //}
            var Color_dropdown = e.Item.FindControl("ddlColor") as DropDownList;
            if (Color_dropdown != null)
            {
                DataTable dtdsupp = new DataTable();
                Color_dropdown.Items.Clear();

                string query = "Select *from MstColor where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                dtdsupp = objdata.RptEmployeeMultipleDetails(query);
                Color_dropdown.DataSource = dtdsupp;
                //DataRow dr = dtdsupp.NewRow();
                //dr["EmpName"] = "-Select-";
                //dr["EmpCode"] = "-Select-";
                //dtdsupp.Rows.InsertAt(dr, 0);
                Color_dropdown.DataTextField = "ColorName";
                Color_dropdown.DataValueField = "ColorName";
                Color_dropdown.DataBind();
                Color_dropdown.Items.Insert(0, new ListItem("-Select-", "0"));
                Color_dropdown.Items.FindByValue("0").Selected = true;
            }

            var Reason_dropdown = e.Item.FindControl("txtReason") as DropDownList;
            if (Reason_dropdown != null)
            {
                DataTable dtdsupp = new DataTable();
                Reason_dropdown.Items.Clear();

                string query = "Select *from StoppageMst where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                dtdsupp = objdata.RptEmployeeMultipleDetails(query);
                Reason_dropdown.DataSource = dtdsupp;
                //DataRow dr = dtdsupp.NewRow();
                //dr["EmpName"] = "-Select-";
                //dr["EmpCode"] = "-Select-";
                //dtdsupp.Rows.InsertAt(dr, 0);
                Reason_dropdown.DataTextField = "StopppageResone";
                Reason_dropdown.DataValueField = "StopppageResone";
                Reason_dropdown.DataBind();
                Reason_dropdown.Items.Insert(0, new ListItem("-Select-", "0"));
                Reason_dropdown.Items.FindByValue("0").Selected = true;
            }
           
        }
    }

    public void Get_Repeater()
    {
        
        DataTable dt = new DataTable();
        //DataTable dt_Empty = new DataTable();
        //ViewState["ItemTable"] = dt_Empty;
        Initial_Data_Referesh();
        bool ErrFlag = false;
        DataRow dr = null;
        string query = "";
        DataTable dtdsupp = new DataTable();

        //ddlMachineName.Items.Clear();
        query = "select Unit,Side,WorkRoter,MachineCode,MachineName,ProdTarget,Counts from MstMachine  ";
        query = query + "where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        query = query + " and Unit='" + ddlUnit.SelectedItem.Text + "'";
        query = query + " and DeptName='OE' order by Unit asc ";
        //query = "Select * from MstMachine ";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);


        //get datatable from view state   
        dt = (DataTable)ViewState["ItemTable"];

        for (int i = 0; i < dtdsupp.Rows.Count; i++)
        {
            dr = dt.NewRow();
            dr["Unit"] = dtdsupp.Rows[i]["Unit"].ToString();
            dr["MachineCode"] = dtdsupp.Rows[i]["MachineCode"].ToString();
            dr["MachineName"] = dtdsupp.Rows[i]["MachineName"].ToString();
            dr["CountID"] = dtdsupp.Rows[i]["Counts"].ToString();
            dr["CountName"] = dtdsupp.Rows[i]["Counts"].ToString();
            dr["Side"] = dtdsupp.Rows[i]["Side"].ToString();
            dr["LotNo"] = "";
            dr["Color"] = "0";
            //dr["Hank"] = dtdsupp.Rows[i]["StdHank"].ToString();
            dr["StdProd"] = dtdsupp.Rows[i]["ProdTarget"].ToString();
            dr["ActProd"] = "";
            dr["SliverWaste"] = "";
            dr["WorkRoter"] = "";
            dr["NoofRoter"] = dtdsupp.Rows[i]["WorkRoter"].ToString();
            dr["StopRoter"] = "";
            dr["RouterWaste"] = "";
            dr["StoppageMins"] = "";
            dr["StoppageReason"] = "";
            //dr["Delivery"] = "";
            dr["Utility"] = "";
            dr["Efficiency"] = "";

            dt.Rows.Add(dr);

        }

        ViewState["ItemTable"] = dt;
        Repeater1.DataSource = dt;
        Repeater1.DataBind();
    }

    //protected void txtWorkRoter_TextChanged(object sender, EventArgs e)
    //{
    //    txtStopRoter.Text = (Convert.ToDecimal(txtNoofRoter.Text) - Convert.ToDecimal(txtWorkRoter.Text)).ToString();

    //}


    protected void ddlUnit_SelectedIndexChanged(object sender, EventArgs e)
    {
        Get_Repeater();
    }
}
