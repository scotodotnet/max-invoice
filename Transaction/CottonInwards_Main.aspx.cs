﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class Transaction_CottonInwards_Main : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionFinYearCode;
    string SessionFinYearVal;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        if (!IsPostBack)
        {
            Page.Title = "CMS | Cotton Inwards";
            HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Transaction"));
            li.Attributes.Add("class", "droplink active open");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();

        Load_Data_Cotton_Inwards_Grid();

    }

    protected void btnAddNew_Click(object sender, EventArgs e)
    {
        Session.Remove("Ctn_Inwards_No");
        Response.Redirect("CottonInwardDetails.aspx");
    }

    protected void GridEditCottonInwardsClick(object sender, CommandEventArgs e)
    {
        string query = "";
        bool ErrFlag = false;

        DataTable dtdpurchase = new DataTable();
        query = "select * from CottonInwards where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Ctn_Inwards_No='" + e.CommandName.ToString() + "'";
        dtdpurchase = objdata.RptEmployeeMultipleDetails(query);
        string status = dtdpurchase.Rows[0]["CI_Status"].ToString().Trim();

        if (status == "" || status == "0")
        {
            string CottonInwards_No = e.CommandName.ToString();
            Session.Remove("Ctn_Inwards_No");
            Session["Ctn_Inwards_No"] = CottonInwards_No;
            Response.Redirect("CottonInwardDetails.aspx");

        }
        else
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Sorry Cotton Inward Approved..');", true);
        }


      

    }
    protected void GridDeleteCottonInwardsClick(object sender, CommandEventArgs e)
    {
        string query = "";

        //User Rights Check Start
        bool ErrFlag = false;
        bool Rights_Check = false;

        //Rights_Check = CommonClass_Function.DeleteRights_Check(SessionCcode, SessionLcode, SessionUserID, "4", "5", "Cotton Inwards");
        //if (Rights_Check == false)
        //{
        //    ErrFlag = true;
        //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Delete New Cotton Inwards..');", true);
        //}
        //User Rights Check End

        DataTable DT_Check = new DataTable();
        query = "Select * from CottonInwards where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Ctn_Inwards_No ='" + e.CommandName.ToString() + "' And CI_Status='1'";
        DT_Check = objdata.RptEmployeeMultipleDetails(query);
        if (DT_Check.Rows.Count != 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Do not Delete Cotton Inward Details Already Approved..');", true);
        }

        if (!ErrFlag)
        {

            DataTable DT = new DataTable();
            query = "Select * from CottonInwards where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And Ctn_Inwards_No='" + e.CommandName.ToString() + "'";
            DT = objdata.RptEmployeeMultipleDetails(query);
            if (DT.Rows.Count != 0)
            {

               // query = "update CottonInwards set D_Status='D' where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And Ctn_Inwards_No='" + e.CommandName.ToString() + "'";
                query = "Delete CottonInwards where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And Ctn_Inwards_No='" + e.CommandName.ToString() + "'";
                objdata.RptEmployeeMultipleDetails(query);
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Cotton Inwards Details Deleted Successfully');", true);
                Load_Data_Cotton_Inwards_Grid();

            }
        }
    }

    private void Load_Data_Cotton_Inwards_Grid()
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select Ctn_Inwards_No,Ctn_Inwards_Date,LotNo,BaleNo,VarietyCode,VarietyName,BaleWeight,NoOfBale from CottonInwards where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'  and D_Status !='D'";

        //query = "Select Ctn_Inwards_No,Ctn_Inwards_Date,LotNo,BaleNo,VarietyCode,VarietyName,BaleWeight,NoOfBale from CottonInwards where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' and D_Status !='D'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        RptrCottonInwards.DataSource = DT;
        RptrCottonInwards.DataBind();
    }
}
