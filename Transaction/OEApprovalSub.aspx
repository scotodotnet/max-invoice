﻿<%@ Page Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="OEApprovalSub.aspx.cs" Inherits="Transaction_OEApprovalSub" Title="" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<script src='<%= ResolveUrl("../assets/js/master_list_jquery.min.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/master_list_jquery-ui.min.js") %>'></script>
<link href="<%= ResolveUrl("../assets/css/master_list_jquery-ui.css") %>" rel="stylesheet" type="text/css"/>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#example').dataTable();
            }
        });
    };
</script>

<script type="text/javascript">
    function SaveMsgAlert(msg) {
        swal(msg);
    }
</script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('.date-picker').datepicker({ format: 'dd/mm/yyyy' });
                $('.js-states').select2();
            }
        });
    };
</script>
<script type="text/javascript">
    $(function() {
        calculateSum();
        $(".cls").change(function() {
            calculateSum();
            //calculate();           
        });
    });
</script>
<script type="text/javascript">
    function calculateSum() {
        var sum = 0;
        //iterate through each textboxes and add the values
        $(".txtAct").each(function() {
            //add only if the value is number
            if (!isNaN(this.value) && this.value.length != 0) {
                sum += parseFloat(this.value);
            }
            //            
        });

        $("#<%=txtGrandTot.ClientID %>").val(sum.toFixed(2));


    }
</script>

<script type="text/javascript">
    $(function() {

        //$(document).bind("keydown", '.cls', function(e) {
        $('.cls').bind("keydown", function(e) {

            var n = $(".cls").length; console.log(n);
            if (e.which == 13) { //Enter key
                //alert("ji");
                // alert($(this).attr('class'));
                var class1 = $(this).attr('class');
                var split_cls = class1.split(" ");
                if (split_cls[1] == 'txt') {
                    //alert("in");
                    var Std_Production = $(this).closest("tr").find(".rot").val(); 
                    var Act_Production = $(this).val();
                    var efficiency = parseFloat((parseFloat(Std_Production) / parseFloat(480)) * parseFloat(Act_Production)).toFixed(2);
                    var WorkRoute = parseFloat(parseFloat(Std_Production) - parseFloat(efficiency)).toFixed(2);
                    var Utility = parseFloat((parseFloat(WorkRoute) / parseFloat(Std_Production)) * parseFloat(100)).toFixed(2);
                    $(this).closest("tr").find(".tot").val(efficiency);
                    $(this).closest("tr").find(".wor").val(WorkRoute);
                    $(this).closest("tr").find(".uti").val(Utility);
                }

                if (split_cls[1] == 'txt2') {
                    //alert("in");
                    var Std_Production = $(this).closest("tr").find(".txt1").text();
                    var Act_Production = $(this).closest("tr").find(".txt").val();
                    var stoppage = $(this).val();
                    //(Actprod/(Target-(target/660*stoppage)))*100
                    var prod = parseFloat(Act_Production);
                    if (stoppage != '' && prod <= Std_Production) {
                        //var efficiency = parseFloat(((parseFloat(Act_Production) + parseFloat(stoppage)) / parseFloat(Std_Production)) * 100).toFixed(2);
                        var efficiency = (parseFloat(Act_Production) / (parseFloat(Std_Production) - ((parseFloat(Std_Production) * parseFloat(stoppage)) / (720))) * 100).toFixed(2);
                        $(this).closest("tr").find(".tot").val(efficiency);
                    } else {
                        var efficiency = parseFloat(((parseFloat(Act_Production)) / parseFloat(Std_Production)) * 100).toFixed(2);
                        $(this).closest("tr").find(".tot").val(efficiency);
                    }
                    //                    if (prod >= Std_Production) {
                    //                        alert('The value is greater than Target Production');
                    //                        return false;
                    //                    }
                }

                e.preventDefault(); //to skip default behavior of the enter key
                var nextIndex = $('.cls').index(this) + 1; console.log($('.cls').index(this));
                if (nextIndex < n)
                    $('.cls')[nextIndex].focus();
                else {
                    $('.cls')[nextIndex - 1].blur();
                    $("#<%=btnSave.ClientID %>").focus();

                }
            }
        });
    });   
</script>
<script type="text/javascript">
    $(function() {
    $('#WasteReceipt').Scrollable({
            ScrollHeight: 100
        });
    });
</script>


<div class="page-breadcrumb">
    <ol class="breadcrumb container">
       <h4><li class="active">OE Approval</li></h4> 
    </ol>
</div>
                
                        
<div id="main-wrapper" class="container">
 <div class="row">
    <div class="col-md-12">
     
    
    <div class="page-inner">
    
      <div class="col-md-12">
		<div class="panel panel-white">
		  <div class="panel panel-primary">
			<div class="panel-heading clearfix">
			  <h4 class="panel-title">OE Approval</h4>
			</div>
		   </div>
		<form class="form-horizontal">
		  <div class="panel-body">
			<div class="col-md-12">
		      <div class="row">
				<div class="form-group col-md-3">
				   <label for="exampleInputName">Trans No<span class="mandatory">*</span></label>
				   <asp:Label ID="txtReceiptNo" runat="server" class="form-control" BackColor="#F3F3F3"></asp:Label>
			    </div>
				<div class="form-group col-md-3">
				  <label for="exampleInputName">Date</label>
				  <asp:TextBox ID="txtDate" MaxLength="20" class="form-control date-picker" runat="server"></asp:TextBox>
				  <asp:RequiredFieldValidator ControlToValidate="txtDate" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                  </asp:RequiredFieldValidator>
                  <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                     TargetControlID="txtDate" ValidChars="0123456789./">
                  </cc1:FilteredTextBoxExtender>
				</div>
				<div class="form-group col-md-3">
				<label for="exampleInputName">Shift</label>
				     <asp:DropDownList ID="ddlShift" runat="server" class="js-states form-control" >
                     </asp:DropDownList>
                     <asp:RequiredFieldValidator ControlToValidate="ddlShift" Display="Dynamic" InitialValue="-Select-" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator4" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                     </asp:RequiredFieldValidator>
					           <%--   <label for="exampleInputName">Unit<span class="mandatory">*</span></label>
                                    <asp:DropDownList ID="ddlUnit" class="form-control" runat="server" 
                                      >
                                    </asp:DropDownList>
                                    
                                      <asp:RequiredFieldValidator ControlToValidate="ddlUnit" Display="Dynamic" InitialValue="-Select-" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator5" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                           </asp:RequiredFieldValidator>--%>
					</div>
					<div class="form-group col-md-3">
			        <label for="exampleInputName">Prepared By</label>
					<asp:DropDownList ID="txtSupplierName" runat="server" class="js-states form-control">
                    </asp:DropDownList>  
                    <asp:RequiredFieldValidator ControlToValidate="txtSupplierName" Display="Dynamic" InitialValue="-Select-" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator6" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                    </asp:RequiredFieldValidator>
			      </div>
				<%--<div class="form-group col-md-4">
					<label for="exampleInputName">Yarn LotNo</label>
					  <asp:DropDownList ID="ddlYarnLotNo" runat="server" class="form-control">
                    </asp:DropDownList>
					
				   <asp:RequiredFieldValidator ControlToValidate="ddlYarnLotNo" Display="Dynamic" InitialValue="-Select-" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator3" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                   </asp:RequiredFieldValidator>
			    </div>--%>
			 </div>
			</div>
			
			<div class="col-md-12">
			    <div class="row"> 
			      <%--<div class="form-group col-md-4">
					<label for="exampleInputName">Color</label>
					<asp:DropDownList ID="ddlColor" runat="server" class="js-states form-control" >
 			        </asp:DropDownList>
				    <asp:RequiredFieldValidator ControlToValidate="ddlColor" Display="Dynamic" InitialValue="-Select-" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator2" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                    </asp:RequiredFieldValidator>
			      </div>--%>
			      <div class="form-group col-md-4">
			         
			      </div>
			      
			    </div>
			</div>
					        
			<div class="col-md-12">
		     <div class="row">  
		     	<div class="form-group col-md-3">
				<label for="exampleInputName">Unit</label>
				     <asp:DropDownList ID="ddlUnit" runat="server" class="js-states form-control" 
                         >
                     </asp:DropDownList>
                     <asp:RequiredFieldValidator ControlToValidate="ddlUnit" Display="Dynamic" InitialValue="-Select-" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator3" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                     </asp:RequiredFieldValidator>
					           <%--   <label for="exampleInputName">Unit<span class="mandatory">*</span></label>
                                    <asp:DropDownList ID="ddlUnit" class="form-control" runat="server" 
                                      >
                                    </asp:DropDownList>
                                    
                                      <asp:RequiredFieldValidator ControlToValidate="ddlUnit" Display="Dynamic" InitialValue="-Select-" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator5" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                           </asp:RequiredFieldValidator>--%>
					</div>
			  </div>
			</div>
			
					
			<div class="clearfix"></div>				    
			<div class="timeline-options">
			  <h4 class="panel-title">Machine Production</h4>
			</div>
                    
                    <!-- table start -->
					<div class="col-md-12">
					    <div class="row">
					    <asp:Panel ScrollBars="Vertical" ID="Panel" runat="server">
					    <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false" OnItemDataBound="ItemDataBound">
			                    <HeaderTemplate>
                                    <table id="WasteReceipt" class="display table" style="overflow-x:scroll;width:100%;">
                                        <thead>
                                            <tr>
                                                <th>Unit</th>
                                                <th>Machine Name</th>
                                                
                                                <th>Side</th> 
                                                <th>Count</th>
                                                <th>Yarn LotNo</th>
                                                <th>Color</th>
                                                <th>Std Production</th> 
                                                <th>No of Roter</th>
                                                <th>Stoppage Minutes</th> 
                                                <th>Work Roter</th>
                                               
                                                <th>Stop Roter</th>
                                                <th>Utilization</th>
                                                 <th>Act Production</th>
                                                <th>Efficiency</th>
                                                <th>Sliver Waste</th>
                                               <%-- <th>Other Waste</th>--%>
                                                <th>Fan Waste</th>
                                                <%--<th>Delivery</th>--%>
                                                
                                                <th>Reason</th>
                                            </tr>
                                        </thead>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                    <td>
                                    <asp:Label ID="txtUnit" runat="server" name="std" class="" Text='<%# DataBinder.Eval(Container.DataItem, "Unit")%>'></asp:Label>
                                    </td>
                                         <td>
                                             <asp:Label ID="txtMachineCode" runat="server" name="std" class="" Text='<%# DataBinder.Eval(Container.DataItem, "MachineCode")%>' Visible="false"></asp:Label>
                                             <asp:Label ID="txtMachineName" runat="server" name="std" class="" Text='<%# DataBinder.Eval(Container.DataItem, "MachineName")%>' ></asp:Label>
                                         </td>
                                    
                                         <td>
                                         <asp:Label ID="txtSide" runat="server" name="std" class="cls" Text='<%# DataBinder.Eval(Container.DataItem, "Side")%>' ></asp:Label>
                                         </td>
                                         <td>
                                         <%--<asp:Label ID="txtCountID" runat="server" name="std" class="" Text='<%# DataBinder.Eval(Container.DataItem, "CountID")%>' Visible="false"></asp:Label>
                                          <asp:Label ID="txtCount" runat="server" name="std" class="" Text='<%# DataBinder.Eval(Container.DataItem, "CountName")%>' ></asp:Label>--%>
                                          <asp:DropDownList ID="ddlCount" runat="server" CssClass="js-states" >
                                          </asp:DropDownList>
                                         </td>
                                       <td>
                                            <asp:TextBox ID="txtLotNo" runat="server"  Class="cls" Text='<%# DataBinder.Eval(Container.DataItem, "LotNo")%>' TextMode="SingleLine"   Width="60"></asp:TextBox> 
                                       </td>
                                        <td>
                                       <asp:DropDownList ID="ddlColor" runat="server" CssClass="js-states">
                                        </asp:DropDownList>
                                       </td>
                                            <td>
                                           
                                           <asp:TextBox ID="txtStdProd" runat="server"  Class="cls" Text='<%# DataBinder.Eval(Container.DataItem, "StdProd")%>' TextMode="SingleLine"   Width="60"></asp:TextBox> 
                                             
                                         </td>
                                         <td>
                                         <asp:TextBox ID="txtNoofRoter"  runat="server" name="NOR" Enabled="false" Class="cls rot" Text='<%# DataBinder.Eval(Container.DataItem, "NoofRoter")%>' Width="60"></asp:TextBox>
                                         <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                              TargetControlID="txtNoofRoter" ValidChars="0123456789.">
                                         </cc1:FilteredTextBoxExtender>
                                         </td>
                                          <td>
                                             <asp:TextBox ID="txtStoppageMin" runat="server" Class="cls txt" Text='<%# DataBinder.Eval(Container.DataItem, "StoppageMins")%>' Width="60"></asp:TextBox>
                                             <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                  TargetControlID="txtStoppageMin" ValidChars="0123456789.">
                                             </cc1:FilteredTextBoxExtender>  
                                        </td> 
                                         <td>
                                         <asp:TextBox ID="txtWorkRoter"  runat="server" name="WOR" Class="cls wor" Text='<%# DataBinder.Eval(Container.DataItem, "WorkRoter")%>' Width="60"></asp:TextBox>
                                          <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                              TargetControlID="txtWorkRoter" ValidChars="0123456789.">
                                          </cc1:FilteredTextBoxExtender>
                                         </td>
                                     
                                        <td>
                                          <asp:TextBox ID="txtStopRoter"  runat="server" name="SPR" Class="cls tot" Text='<%# DataBinder.Eval(Container.DataItem, "StopRoter")%>' Width="60"></asp:TextBox>
                                          <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender9" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                              TargetControlID="txtStopRoter" ValidChars="0123456789.">
                                          </cc1:FilteredTextBoxExtender>
                                        </td> 
                                        <td>
                                          <asp:TextBox ID="txtUtilization"  runat="server" name="UTL" Class="cls uti" Text='<%# DataBinder.Eval(Container.DataItem, "Utility")%>' Width="60"></asp:TextBox>
                                          <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                              TargetControlID="txtUtilization" ValidChars="0123456789.">
                                          </cc1:FilteredTextBoxExtender>
                                        </td>   
                                           <td>
                                         <asp:TextBox ID="txtActProd"  runat="server" name="ACT" Class="cls txtAct" Text='<%# DataBinder.Eval(Container.DataItem, "ActProd")%>' Width="60"></asp:TextBox>
                                         <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                              TargetControlID="txtActProd" ValidChars="0123456789.">
                                         </cc1:FilteredTextBoxExtender>
                                        </td> 
                                        <td>
                                          <asp:TextBox ID="txtEfficency" runat="server" name="EFF" Class="cls" Text='<%# DataBinder.Eval(Container.DataItem, "Efficiency")%>' Width="60"></asp:TextBox>
                                          <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                  TargetControlID="txtEfficency" ValidChars="0123456789.">
                                          </cc1:FilteredTextBoxExtender>
                                        </td> 
                                       <td>
                                         <asp:TextBox ID="txtSilver" runat="server" name="SIV" Class="cls" Text='<%# DataBinder.Eval(Container.DataItem, "SliverWaste")%>' Width="60"></asp:TextBox>
                                         <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                  TargetControlID="txtSilver" ValidChars="0123456789.">
                                         </cc1:FilteredTextBoxExtender>
                                       </td>
                                       <%--  <td>
                                             <asp:TextBox ID="txtFS" runat="server" name="Eff" Class="cls tot" Text='<%# DataBinder.Eval(Container.DataItem, "FS")%>' Width="60"></asp:TextBox>
                                             <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                  TargetControlID="txtFS" ValidChars="0123456789.">
                                             </cc1:FilteredTextBoxExtender>
                                        </td>--%>
                                        <td>
                                             <asp:TextBox ID="txtRouterWaste" runat="server" name="RWS" Class="cls" Text='<%# DataBinder.Eval(Container.DataItem, "RouterWaste")%>' Width="60"></asp:TextBox>
                                             <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender10" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                  TargetControlID="txtRouterWaste" ValidChars="0123456789.">
                                             </cc1:FilteredTextBoxExtender>
                                        </td>
                                     <%--    <td>
                                             <asp:TextBox ID="txtDelivery" runat="server" name="DEL" Class="cls" Text='<%# DataBinder.Eval(Container.DataItem, "Delivery")%>' Width="60"></asp:TextBox>
                                             <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender11" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                  TargetControlID="txtDelivery" ValidChars="0123456789.">
                                             </cc1:FilteredTextBoxExtender>
                                        </td>--%>
                                       
                                        <td>
                                            <asp:TextBox ID="txtReason" runat="server"  Class="cls" Text='<%# DataBinder.Eval(Container.DataItem, "StoppageReason")%>' TextMode="MultiLine" Height="30"  Width="150"></asp:TextBox> 
                                        </td>                                    
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate></table></FooterTemplate>                                
			                </asp:Repeater>
					    </asp:Panel>
					        
					    </div>
					</div>
					<!-- table End -->
					<div class="col-md-2"></div>
						<div class="col-md-12">
					    <div class="row">
					    <div class="form-group col-md-10"></div>
					         <div class="form-group col-md-2">
				                           <label for="exampleInputName">Total Production</label>
				                           <asp:TextBox ID="txtGrandTot" class="form-control" runat="server"></asp:TextBox>
				                           <asp:RequiredFieldValidator ControlToValidate="txtGrandTot" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator2" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                           </asp:RequiredFieldValidator>
			                  </div>
					    </div>				
					</div> 
					
					
					
					
					<div class="form-group row"></div>	
                    <!-- Button start -->
                    <div class="txtcenter">
                        <asp:Button ID="btnSave" Visible="false" class="btn btn-success"  runat="server" Text="Save" 
                            ValidationGroup="Validate_Field" onclick="btnSave_Click"/>
                            <asp:Button ID="btnApprove" class="btn btn-success" runat="server"
                            Text="Approve" onclick="btnApprove_Click" />
                        <asp:Button ID="btnCancel" class="btn btn-danger" runat="server" Text="Cancel" onclick="btnCancel_Click"/>
                        <asp:Button ID="btnBackEnquiry" class="btn btn-success" runat="server" 
                            Text="Back" onclick="btnBackEnquiry_Click" />
                        
                    </div>
                    <!-- Button end -->
                  </div>      
				</form>
			
			</div><!-- panel white end -->
		    </div>
		    
		    <div class="col-md-2"></div>
		    
            
      
 </div> <!-- col-9 end -->
 </div><!-- col 12 end -->
      
  </div><!-- row end -->
 </div>
</asp:Content>

