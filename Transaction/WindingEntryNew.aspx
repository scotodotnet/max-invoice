﻿<%@ Page Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="WindingEntryNew.aspx.cs" Inherits="Transaction_WindingEntryNew" Title="" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<script src='<%= ResolveUrl("../assets/js/master_list_jquery.min.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/master_list_jquery-ui.min.js") %>'></script>
<link href="<%= ResolveUrl("../assets/css/master_list_jquery-ui.css") %>" rel="stylesheet" type="text/css"/>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#example').dataTable();
            }
        });
    };
</script>

<script type="text/javascript">
    function SaveMsgAlert(msg) {
        swal(msg);
    }
</script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('.date-picker').datepicker({ format: 'dd/mm/yyyy' });
                $('.js-states').select2();
            }
        });
    };
</script>
<script type="text/javascript">
    $(function() {
        $('#WasteReceipt').Scrollable({
            ScrollHeight: 100
        });
    });
</script>

<div class="page-breadcrumb">
    <ol class="breadcrumb container">
       <h4><li class="active">Winding</li></h4> 
    </ol>
</div>
                
                        
<div id="main-wrapper" class="container">
 <div class="row">
    <div class="col-md-12">
     
    
    <div class="page-inner">
    
      <div class="col-md-12">
		<div class="panel panel-white">
		  <div class="panel panel-primary">
			<div class="panel-heading clearfix">
			  <h4 class="panel-title">Winding </h4>
			</div>
		   </div>
		<form class="form-horizontal">
		  <div class="panel-body">
		  
		  <asp:UpdatePanel ID="PanelWind" runat="server">
		     <ContentTemplate>
		       	<div class="col-md-12">
		      <div class="row">
				<div class="form-group col-md-4">
				   <label for="exampleInputName">Trans No<span class="mandatory">*</span></label>
				   <asp:Label ID="txtReceiptNo" runat="server" class="form-control" BackColor="#F3F3F3"></asp:Label>
			    </div>
			     
				<div class="form-group col-md-4">
				  <label for="exampleInputName">Date</label>
				  <asp:TextBox ID="txtDate" MaxLength="20" class="form-control date-picker" runat="server"></asp:TextBox>
				  <asp:RequiredFieldValidator ControlToValidate="txtDate" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                  </asp:RequiredFieldValidator>
                  <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                     TargetControlID="txtDate" ValidChars="0123456789./">
                  </cc1:FilteredTextBoxExtender>
				</div>
					        
					          <div class="form-group col-md-4">
			                          <label for="exampleInputName">Shift</label>
				                     <asp:DropDownList ID="ddlShift" runat="server" 
                                                class="js-states form-control" >
                                           </asp:DropDownList>
                                           <asp:RequiredFieldValidator ControlToValidate="ddlShift" Display="Dynamic" InitialValue="-Select-" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator4" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                           </asp:RequiredFieldValidator>
			                       </div>
				 
			   
			          
			 </div>
			</div>
					        
			<div class="col-md-12">
		     <div class="row">  
					         
					           
				                 
				
			<div class="form-group col-md-4">
			    <label for="exampleInputName">Prepared By</label>
					<asp:DropDownList ID="txtSupplierName" runat="server" class="js-states form-control">
                    </asp:DropDownList>  
                    <asp:RequiredFieldValidator ControlToValidate="txtSupplierName" Display="Dynamic" InitialValue="-Select-" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator6" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                    </asp:RequiredFieldValidator>
			   </div>
			   <div class="form-group col-md-4">
			       <label for="exampleInputName">GoDown</label>
					<asp:DropDownList ID="ddlGodown" runat="server" class="js-states form-control">
                    </asp:DropDownList>  
                    <asp:RequiredFieldValidator ControlToValidate="ddlGodown" Display="Dynamic" InitialValue="-Select-" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator16" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                    </asp:RequiredFieldValidator>
			   </div>
			  </div>
			</div>
			
			
		
			
					
			<div class="clearfix"></div>				    
			<div class="timeline-options">
			  <h4 class="panel-title">Count Details</h4>
			</div>
			
			<div class="col-md-12">
			  <div class="row">  
			  <div class="form-group col-md-3">
			       <label for="exampleInputName">Machine Name</label>
					<asp:DropDownList ID="ddlMachine" runat="server" 
                       class="js-states form-control" 
                       onselectedindexchanged="ddlMachine_SelectedIndexChanged" AutoPostBack="true">
                    </asp:DropDownList>  
               
                    <asp:HiddenField ID="txtMachineID" Value="" runat="server" />
			   </div>
			   <div class="form-group col-md-2">
			       <label for="exampleInputName">Side</label>
					<asp:DropDownList ID="ddlSide" runat="server" class="js-states form-control">
                    </asp:DropDownList>  
                  
			   </div>
			  
			    <div class="form-group col-md-3">
			       <label for="exampleInputName">Count</label>
					<asp:DropDownList ID="ddlCount" runat="server" class="js-states form-control">
                    </asp:DropDownList>  
                 
			   </div>
			    <div class="form-group col-md-2">
			       <label for="exampleInputName">Unit</label>
					<asp:DropDownList ID="ddlUnit" runat="server" class="js-states form-control">
                    </asp:DropDownList>  
               
			   </div>
			   			  
			   <div class="form-group col-md-2">
				  <label for="exampleInputName">LotNo</label>
				  <asp:TextBox ID="txtLotNo" MaxLength="20" class="form-control" runat="server"></asp:TextBox>
				 
				</div>
			   
			  </div>
			</div>
			
			<div class="col-md-12">
			   <div class="row">  
			    <div class="form-group col-md-2">
				  <label for="exampleInputName">Drum From</label>
				  <asp:TextBox ID="txtDrumFrom" MaxLength="20" class="form-control" runat="server"></asp:TextBox>
				
				</div>
				 <div class="form-group col-md-2">
				  <label for="exampleInputName">Drum To</label>
				  <asp:TextBox ID="txtDrumTo" MaxLength="20" class="form-control" runat="server"></asp:TextBox>
				
				</div>
				 <div class="form-group col-md-2">
				              <label for="exampleInputName">Main Count</label>
				              <asp:TextBox ID="txtMainCount" MaxLength="20" class="form-control" runat="server"></asp:TextBox>
				           
				            </div>
			
				 <div class="form-group col-md-2">
				  <label for="exampleInputName">Drum Speed</label>
				  <asp:TextBox ID="txtDrumSpeed" MaxLength="20" class="form-control" runat="server"></asp:TextBox>
				 
				</div>
				 <div class="form-group col-md-2">
				  <label for="exampleInputName">Doff Cone</label>
				  <asp:TextBox ID="txtDoffCone" MaxLength="20" class="form-control" runat="server"></asp:TextBox>
			
				</div>
				 <div class="form-group col-md-2">
				  <label for="exampleInputName">Cone Weight</label>
				  <asp:TextBox ID="txtConeWeight" MaxLength="20" class="form-control" runat="server"></asp:TextBox>
				
				</div>
				
			   </div>
			</div>
			
			<div class="col-md-12">
			     <div class="row">  
			     	 <div class="form-group col-md-3">
				          <label for="exampleInputName">Utilization</label>
				          <asp:TextBox ID="txtUtilization" MaxLength="20" class="form-control" runat="server"></asp:TextBox>
				    
				        </div>
				        
				        	 <div class="form-group col-md-3">
				              <label for="exampleInputName">Hard Waste</label>
				              <asp:TextBox ID="txtHardWaste" MaxLength="20" class="form-control" runat="server"></asp:TextBox>
				             
				            </div>
				             <div class="form-group col-md-3">
				              <label for="exampleInputName">Remarks</label>
				              <asp:TextBox ID="txtRemarks" MaxLength="20" class="form-control" runat="server"></asp:TextBox>
                                 <asp:HiddenField ID="txtAllotDrum" runat="server" />
                                 <asp:HiddenField ID="txtProduction" runat="server" />
                                 <asp:HiddenField ID="txtEfficiency" runat="server" />
				           
				            </div>
				            <div class="form-group col-md-1">
				            <br />
                                <asp:LinkButton ID="lbtnAdd" runat="server" class="btn btn-Warning" 
                                    BackColor="#0066FF" BorderColor="#0066FF" Font-Bold="True" 
                                    ForeColor="White" onclick="lbtnAdd_Click">Add</asp:LinkButton>
				            </div>
			     </div>
			</div>
			   <!-- table start -->
					<div class="col-md-12">
					    <div class="row">
					    <asp:Panel ScrollBars="Vertical" ID="Panel" runat="server">
					    <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
			                    <HeaderTemplate>
                                    <table id="example" class="display table">
                                        <thead>
                                            <tr>
                                                <th>S. No</th>
                                                <th>Machine Name</th>
                                                <th>Count</th>
                                                <th>Side</th>
                                                <th>Unit</th>
                                                <th>LotNo</th>
                                                <th>DrumFrom</th>
                                                <th>DrumTo</th>
                                                <th>DrumAllot</th>
                                                <th>Main Count</th>
                                                <th>Drum Speed</th>
                                                <th>Doff Cone</th>
                                                <th>Cone Weight</th>
                                                <th>Production</th>
                                                <th>Utilization</th>
                                                <th>Efficiency</th>
                                                <th>Hard Waste</th>
                                                <th>Remarks</th>
                                                <th>Mode</th>
                                            </tr>
                                        </thead>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td><%# Container.ItemIndex + 1 %></td>
                                      
                                     <%--   <td visible="false"><%# Eval("MachineCode")%></td>--%>
                                        <td><%# Eval("MachineName")%></td>
                                        <td><%# Eval("CountName")%></td>
                                        <td><%# Eval("Side")%></td>
                                        <td><%# Eval("Unit")%></td>
                                        <td><%# Eval("LotNo")%></td>
                                        <td><%# Eval("DrumFrom")%></td>
                                        <td><%# Eval("DrumTo")%></td>
                                        <td><%# Eval("DrumAllot")%></td>
                                        <td><%# Eval("MainCount")%></td>
                                        <td><%# Eval("DrumSpeed")%></td>
                                        <td><%# Eval("DoffCone")%></td>
                                        <td><%# Eval("ConeWeight")%></td>
                                        <td><%# Eval("Production")%></td>
                                        <td><%# Eval("Utilization")%></td>
                                        <td><%# Eval("Efficiency")%></td>
                                        <td><%# Eval("HardWaste")%></td>
                                        <td><%# Eval("Remarks")%></td>
                                        <td>
                                            <asp:LinkButton ID="btnDeleteGrid" class="btn btn-danger btn-sm fa fa-trash-o"  runat="server" 
                                                Text="" OnCommand="GridDeleteClick" CommandArgument="Delete" CommandName='<%# Eval("MachineCode")%>' 
                                                CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this Item details?');">
                                            </asp:LinkButton>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate></table></FooterTemplate>                                
			                </asp:Repeater>
					    </asp:Panel>
					   
					        
					    </div>
					</div>
					<!-- table End -->
					  
					
					<div class="form-group row"></div>	
                    <!-- Button start -->
                    <div class="txtcenter">
                        <asp:Button ID="btnSave" class="btn btn-success"  runat="server" Text="Save" 
                            ValidationGroup="Validate_Field" onclick="btnSave_Click" />
                        <asp:Button ID="btnCancel" class="btn btn-danger" runat="server" Text="Cancel" 
                            onclick="btnCancel_Click"/>
                        <asp:Button ID="btnBackEnquiry" class="btn btn-success" runat="server" 
                            Text="Back" onclick="btnBackEnquiry_Click" />
                        <asp:Button ID="btnApprove" class="btn btn-success" runat="server" Visible="false"
                            Text="Approve"  />
                    </div>
                    <!-- Button end -->
		     </ContentTemplate>
		  </asp:UpdatePanel>
		
                  </div>      
				</form>
			
			</div><!-- panel white end -->
		    </div>
		    
		    <div class="col-md-2"></div>
		    
            
      
 </div> <!-- col-9 end -->
 </div><!-- col 12 end -->
      
  </div><!-- row end -->
 </div>
</asp:Content>

