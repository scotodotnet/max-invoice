﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;


public partial class Transaction_EB_Conception : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionEnquiryNo;
    string SessionFinYearCode;
    string SessionFinYearVal;
    string SessionTrans_No;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "ERP Module :: EB Consumption";
            HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Transaction"));
            li.Attributes.Add("class", "droplink active open");
            Initial_Data_Referesh();

            Load_Shift();
            Load_MachineName();
            Load_Rate();

            if (Session["Trans_No"] == null)
            {
                SessionTrans_No = "";
            }
            else
            {
                SessionTrans_No = Session["Trans_No"].ToString();
                txtTransNo.Text = SessionTrans_No;
                btnSearch_Click(sender, e);
            }

        }
        Load_OLD_data();
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        //Search Gate_Pass_Out
        string query = "";
        DataTable Main_DT = new DataTable();
        query = "Select * from EB_Consumption_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Trans_No='" + txtTransNo.Text + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        if (Main_DT.Rows.Count != 0)
        {
            txtTransNo.Text = Main_DT.Rows[0]["Trans_No"].ToString();
            txtDate.Text = Main_DT.Rows[0]["Trans_Date"].ToString();
            txtShift.SelectedValue = Main_DT.Rows[0]["Shift"].ToString();
            txtPreparedBy.Text = Main_DT.Rows[0]["PreparedBy"].ToString();
            txtOtherConsume.Text = Main_DT.Rows[0]["OtherConsume"].ToString();
            txtRate.Text = Main_DT.Rows[0]["Rate"].ToString();
            txtValue.Text = Main_DT.Rows[0]["Value"].ToString();
            txtRemarks.Text = Main_DT.Rows[0]["Remarks"].ToString();
            
            //Unplanned_Receipt_Main_Sub Table Load
            DataTable dt = new DataTable();
            query = "Select *from EB_Consumption_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Trans_No='" + txtTransNo.Text + "'";
            dt = objdata.RptEmployeeMultipleDetails(query);
            ViewState["ItemTable"] = dt;
            Repeater1.DataSource = dt;
            Repeater1.DataBind();

            Totalsum();

            btnSave.Text = "Update";
        }
        else
        {

        }
    }

    private void Load_Rate()
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select * from MstKgConvEBRate where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        if (DT.Rows.Count != 0)
        {
            txtRate.Text = DT.Rows[0]["EBRate"].ToString();
        }
        else
        {
            txtRate.Text = "0";
        }
    }
    private void Load_MachineName()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlMachineName.Items.Clear();
        query = "Select *from MstMachine where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlMachineName.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["MachineName"] = "-Select-";
        dr["MachineCode"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlMachineName.DataTextField = "MachineName";
        ddlMachineName.DataValueField = "MachineCode";
        ddlMachineName.DataBind();
    }

    private void Load_Shift()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        txtShift.Items.Clear();
        query = "Select *from MstShift where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        txtShift.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["ShiftName"] = "-Select-";
        dr["ShiftName"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        txtShift.DataTextField = "ShiftName";
        txtShift.DataValueField = "ShiftName";
        txtShift.DataBind();
    }

    private void Load_OLD_data()
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        Repeater1.DataSource = dt;
        Repeater1.DataBind();
    }

    private void Initial_Data_Referesh()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add(new DataColumn("MachineCode", typeof(string)));
        dt.Columns.Add(new DataColumn("MachineName", typeof(string)));
        dt.Columns.Add(new DataColumn("TargetUnit", typeof(string)));
        dt.Columns.Add(new DataColumn("ActUnit", typeof(string)));
        dt.Columns.Add(new DataColumn("LineTotal", typeof(string)));
       
        Repeater1.DataSource = dt;
        Repeater1.DataBind();
        ViewState["ItemTable"] = Repeater1.DataSource;

        //dt = Repeater1.DataSource;
    }

    protected void GridDeleteClick(object sender, CommandEventArgs e)
    {
        DataTable qry_dt = new DataTable();
        bool ErrFlag = false;
        string qry = "";


        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            if (dt.Rows[i]["MachineCode"].ToString() == e.CommandName.ToString())
            {
                dt.Rows.RemoveAt(i);
                dt.AcceptChanges();
            }
        }
        ViewState["ItemTable"] = dt;
        Load_OLD_data();
        Totalsum();
      
    }

    protected void ddlMachineName_SelectedIndexChanged(object sender, EventArgs e)
    {
        string query = "";
        DataTable DT = new DataTable();

        query = "Select *from MstMachine where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        query = query + " And MachineCode='" + ddlMachineName.SelectedValue + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        if (DT.Rows.Count != 0)
        {
            txtTargetUnit.Text = DT.Rows[0]["EBUnitTarget"].ToString();
        }
        else
        {
            txtTargetUnit.Text = "0";
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        string query = "";
        DataTable DT_Check = new DataTable();
        DataTable qry_dt = new DataTable();
        string SaveMode = "Insert";
        bool ErrFlag = false;

        //check with Item Details Add with Grid
        DT_Check = (DataTable)ViewState["ItemTable"];
        if (DT_Check.Rows.Count == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You have to add atleast one Item Details..');", true);
        }


        //User Rights Check Start
        bool Rights_Check = false;
        //if (btnSave.Text == "Update")
        //{
        //    Rights_Check = CommonClass_Function.ModifyRights_Check(SessionCcode, SessionLcode, SessionUserID, "7", "2", "Unplanned Receipt");
        //    if (Rights_Check == false)
        //    {
        //        ErrFlag = true;
        //        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Modify Unplanned Receipt Details..');", true);
        //    }
        //}
        //else
        //{
        //    Rights_Check = CommonClass_Function.AddRights_Check(SessionCcode, SessionLcode, SessionUserID, "7", "2", "Unplanned Receipt");
        //    if (Rights_Check == false)
        //    {
        //        ErrFlag = true;
        //        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Adding New Unplanned Receipt..');", true);
        //    }
        //}
        //User Rights Check End


        //Auto generate Transaction Function Call
        if (btnSave.Text != "Update")
        {
            if (!ErrFlag)
            {
                TransactionNoGenerate TransNO = new TransactionNoGenerate();
                string Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup(SessionCcode, SessionLcode, "EB Consumption", SessionFinYearVal);
                if (Auto_Transaction_No == "")
                {
                    ErrFlag = true;
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Auto Generate Transaction No Error... Contact Server Admin');", true);
                }
                else
                {
                    txtTransNo.Text = Auto_Transaction_No;
                }
            }
        }




        //Check With Already Approved Start
        DataTable DT_Check_Apprv = new DataTable();
        query = "Select * from EB_Consumption_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Trans_No='" + txtTransNo.Text + "' And Status='1'";
        DT_Check_Apprv = objdata.RptEmployeeMultipleDetails(query);
        if (DT_Check_Apprv.Rows.Count != 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Do not Update EB Consumption Details Already Approved..');", true);
        }




        if (!ErrFlag)
        {
            query = "Select * from EB_Consumption_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Trans_No='" + txtTransNo.Text + "'";
            DT_Check = objdata.RptEmployeeMultipleDetails(query);
            if (DT_Check.Rows.Count != 0)
            {
                SaveMode = "Update";
                query = "Delete from EB_Consumption_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Trans_No='" + txtTransNo.Text + "'";
                objdata.RptEmployeeMultipleDetails(query);
                query = "Delete from EB_Consumption_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Trans_No='" + txtTransNo.Text + "'";
                objdata.RptEmployeeMultipleDetails(query);
            }

            //Response.Write(strValue);
            //Insert Main Table
            query = "Insert Into EB_Consumption_Main(Ccode,Lcode,FinYearCode,FinYearVal,Trans_No,Trans_Date,Shift,PreparedBy,OtherConsume,Rate,Value,Remarks,TotalAmt,UserID,UserName) Values('" + SessionCcode + "',";
            query = query + " '" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + txtTransNo.Text + "','" + txtDate.Text + "','" + txtShift.SelectedValue + "','" + txtPreparedBy.Text + "',";
            query = query + " '" + txtOtherConsume.Text + "','" + txtRate.Text + "','" + txtValue.Text + "',";
            query = query + " '" + txtRemarks.Text + "','" + txtTotAmt.Text + "','" + SessionUserID + "','" + SessionUserName + "')";
            objdata.RptEmployeeMultipleDetails(query);

            DataTable dt = new DataTable();
            dt = (DataTable)ViewState["ItemTable"];
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                query = "Insert Into EB_Consumption_Main_Sub(Ccode,Lcode,FinYearCode,FinYearVal,Trans_No,Trans_Date,MachineCode,MachineName,TargetUnit,";
                query = query + "ActUnit,LineTotal,UserID,UserName) Values('" + SessionCcode + "',";
                query = query + " '" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + txtTransNo.Text + "','" + txtDate.Text + "','" + dt.Rows[i]["MachineCode"].ToString() + "','" + dt.Rows[i]["MachineName"].ToString() + "',";
                query = query + " '" + dt.Rows[i]["TargetUnit"].ToString() + "','" + dt.Rows[i]["ActUnit"].ToString() + "',";
                query = query + "'" + dt.Rows[i]["LineTotal"].ToString() + "',";
                query = query + " '" + SessionUserID + "','" + SessionUserName + "')";
                objdata.RptEmployeeMultipleDetails(query);
            }



            if (SaveMode == "Insert")
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('EB Consumption Details Saved Successfully');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('EB Consumption Details Updated Successfully');", true);
            }
            //Clear_All_Field();
            Session["Trans_No"] = txtTransNo.Text;
            btnSave.Text = "Update";
            //Load_Data_Enquiry_Grid();
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }

    protected void btnBackEnquiry_Click(object sender, EventArgs e)
    {
        Session.Remove("Trans_No");
        Response.Redirect("EB_Conception_Main.aspx");
    }

    private void Clear_All_Field()
    {
        txtTransNo.Text = ""; txtDate.Text = ""; txtShift.SelectedValue = "-Select-";
        txtPreparedBy.Text = ""; txtRate.Text = "0"; txtValue.Text = ""; txtRemarks.Text = "";
        txtOtherConsume.Text = "";
        ddlMachineName.SelectedValue = "-Select-"; txtTargetUnit.Text = "0";
        txtActUnit.Text = "";
      
        txtItemTotal.Text = "0.0";

        txtTotAmt.Text = "0.0";
     
        btnSave.Text = "Save";
        Initial_Data_Referesh();
        Session.Remove("Trans_No");
        Load_Rate();
        //Load_Data_Enquiry_Grid();
    }

    protected void btnAddItem_Click(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        DataTable qry_dt = new DataTable();
        bool ErrFlag = false;
        DataRow dr = null;
        string query = "";

        if (txtActUnit.Text == "0.0" || txtActUnit.Text == "0" || txtActUnit.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the Actual Unit...');", true);
        }


        if (!ErrFlag)
        {

            string Line_Total = "";
            Line_Total = ((Convert.ToDecimal(txtActUnit.Text)) * Convert.ToDecimal(txtRate.Text)).ToString();
            Line_Total = (Math.Round(Convert.ToDecimal(Line_Total), 0, MidpointRounding.AwayFromZero)).ToString();


            //string ValuationType = qry_dt.Rows[0]["ValuationType"].ToString();
            // check view state is not null  
            if (ViewState["ItemTable"] != null)
            {
                //get datatable from view state   
                dt = (DataTable)ViewState["ItemTable"];

                //check Item Already add or not
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (dt.Rows[i]["MachineCode"].ToString().ToUpper() == ddlMachineName.SelectedValue.ToString().ToUpper())
                    {
                        ErrFlag = true;
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('This Machine Already Added..');", true);
                    }
                }
                if (!ErrFlag)
                {
                    dr = dt.NewRow();
                    dr["MachineCode"] = ddlMachineName.SelectedValue;
                    dr["MachineName"] = ddlMachineName.SelectedItem.Text;
                    dr["TargetUnit"] = txtTargetUnit.Text;
                    dr["ActUnit"] = txtActUnit.Text;
                    dr["LineTotal"] = txtItemTotal.Text;
                  
                    dt.Rows.Add(dr);
                    ViewState["ItemTable"] = dt;
                    Repeater1.DataSource = dt;
                    Repeater1.DataBind();

                    Totalsum();

                    ddlMachineName.SelectedValue = "-Select-";
                    txtTargetUnit.Text = "0"; txtActUnit.Text = "0"; txtItemTotal.Text = "0.00";
                  
                }
            }
            else
            {
                dr = dt.NewRow();
                dr["MachineCode"] = ddlMachineName.SelectedValue;
                dr["MachineName"] = ddlMachineName.SelectedItem.Text;
                dr["TargetUnit"] = txtTargetUnit.Text;
                dr["ActUnit"] = txtActUnit.Text;
                dr["LineTotal"] = txtItemTotal.Text;

                dt.Rows.Add(dr);
                ViewState["ItemTable"] = dt;
                Repeater1.DataSource = dt;
                Repeater1.DataBind();

                Totalsum();

                ddlMachineName.SelectedValue = "-Select-";
                txtTargetUnit.Text = "0"; txtActUnit.Text = "0"; txtItemTotal.Text = "0.00";
            }
        }
    }
    protected void txtActUnit_TextChanged(object sender, EventArgs e)
    {
        if (txtActUnit.Text != "")
        {
            string Line_Total = "";
            Line_Total = ((Convert.ToDecimal(txtActUnit.Text)) * Convert.ToDecimal(txtRate.Text)).ToString();
            Line_Total = (Math.Round(Convert.ToDecimal(Line_Total), 0, MidpointRounding.AwayFromZero)).ToString();
            txtItemTotal.Text = Line_Total;
        }
    }
    public void Totalsum()
    {
        string sum = "0";

        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        if (dt.Rows.Count != 0)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                sum = (Convert.ToDecimal(sum) + Convert.ToDecimal(dt.Rows[i]["LineTotal"])).ToString();
                txtTotAmt.Text = sum;

            }
        }
        else
        {
            txtTotAmt.Text = "0";
        }
    }
    protected void txtOtherConsume_TextChanged(object sender, EventArgs e)
    {
        if (txtOtherConsume.Text != "")
        {
            string Value_Total = "";
            Value_Total = ((Convert.ToDecimal(txtOtherConsume.Text)) * Convert.ToDecimal(txtRate.Text)).ToString();
            Value_Total = (Math.Round(Convert.ToDecimal(Value_Total), 0, MidpointRounding.AwayFromZero)).ToString();
            txtValue.Text = Value_Total;
        }
    }
}
