<%@ Page Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="RawMaterial_Sub.aspx.cs" Inherits="Transaction_RawMaterial_Sub" Title="" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<script type="text/javascript">
    function SaveMsgAlert(msg) {
        swal(msg);
    }
</script>
<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#example').dataTable();
                $('.js-states').select2();      
            }
        });
    };
</script>


<div class="page-breadcrumb">
    <ol class="breadcrumb container">
        <h4><li class="active">Raw Material</li></h4> 
    </ol>
</div>
<div id="main-wrapper" class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-9">
			    <div class="panel panel-white">
			        <div class="panel panel-primary">
				        <div class="panel-heading clearfix">
					        <h4 class="panel-title">Raw Material</h4>
				        </div>
				    </div>
				    <form class="form-horizontal">
				        <div class="panel-body">
				            <div class="col-md-12">
					            <div class="row">
					                <div class="form-group col-md-4">
					                    <label for="exampleInputName">Trans No<span class="mandatory">*</span></label>
					                    <asp:Label ID="txtTransNo" runat="server" class="form-control" BackColor="#F3F3F3"></asp:Label>
					                </div>
					                <div class="form-group col-md-4">
					                    <label for="exampleInputName">Trans Date<span class="mandatory">*</span></label>
					                    <div class="input-group m-b-sm" style="width: 230px;padding-left: 0px; float:left"> <span class="input-group-addon" id="Span1" style="float: left; padding: 9px; width: auto;"><i class="fa fa-calendar"></i></span>
                                            <asp:TextBox ID="txtTransDate" MaxLength="30" class="form-control date-picker" runat="server" style="float: left;width: 86%;"></asp:TextBox>
                                            <asp:RequiredFieldValidator Display="Dynamic" ControlToValidate="txtTransDate" ValidationGroup="Validate_Field" class="form_error" ID="validate1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                            </asp:RequiredFieldValidator>
            					        </div>
                                    </div>
                                     <div class="form-group col-md-4">
					                    <label for="exampleInputName">Supplier Name</label><span class="mandatory">*</span></label>
                                        <asp:DropDownList ID="txtSupplierName" runat="server" 
                                            class="js-states form-control">
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator InitialValue="-Select-" Display="Dynamic" ControlToValidate="txtSupplierName" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator20" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                        </asp:RequiredFieldValidator>
                                    </div>
                                    
					            </div>
					        </div>
				            <div class="col-md-12">
					            <div class="row">
					                 <div class="form-group col-md-4">
					                    <label for="exampleInputName">Station Name</label>
					                    <asp:DropDownList ID="txtstationName" runat="server" class="js-states form-control">
                                        </asp:DropDownList>
					                    <asp:RequiredFieldValidator InitialValue="-Select-" Display="Dynamic" ControlToValidate="txtstationName" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator21" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                        </asp:RequiredFieldValidator>
                                      </div>
                                      
                                      <div class="form-group col-md-4">
					                    <label for="exampleInputName">GoDown Name</label>
					                    <asp:DropDownList ID="ddlGoDown" runat="server" class="js-states form-control">
                                        </asp:DropDownList>
					                    <asp:RequiredFieldValidator InitialValue="-Select-" Display="Dynamic" ControlToValidate="ddlGoDown" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator11" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                        </asp:RequiredFieldValidator>
                                      </div>
                                      
                                      <div class="form-group col-md-4">
                                          <label for="exampleInputName">Lot No<span class="mandatory">*</span></label>
                                        <asp:TextBox ID="txtLotNo" MaxLength="30" class="form-control" runat="server"></asp:TextBox>
                                        <asp:RequiredFieldValidator Display="Dynamic" ControlToValidate="txtLotNo" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator2" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                        </asp:RequiredFieldValidator>
                                        
                                    </div>
                                      
					         
					            </div>
					        </div>
				           
					        <div class="col-md-12">
					            <div class="row">
					    		  	    
					                <div class="form-group col-md-4">
					                    <label for="exampleInputName">Variety Name<span class="mandatory">*</span></label>
                                        <asp:DropDownList ID="ddlVarietyName" runat="server" class="js-states form-control">
                                        </asp:DropDownList>
					                    <asp:RequiredFieldValidator InitialValue="-Select-" Display="Dynamic" ControlToValidate="ddlVarietyName" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator22" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                        </asp:RequiredFieldValidator>
                                    </div>
                                       <div class="form-group col-md-2">
					                    <label for="exampleInputName">Trash<span class="mandatory">*</span></label>
					                    <asp:TextBox ID="txtTrash" class="form-control" runat="server" Text="0" ></asp:TextBox>
					                    <asp:RequiredFieldValidator Display="Dynamic" ControlToValidate="txtTrash"  ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator7" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                        </asp:RequiredFieldValidator>
                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender19" runat="server" FilterType="Numbers,Custom"
                                        ValidChars="." TargetControlID="txtTrash" />
					                </div>
					                <div class="form-group col-md-2">
					                    <label for="exampleInputName">Moisture</label>
					                    <asp:TextBox ID="txtOthers" class="form-control" runat="server" Text="0" ></asp:TextBox>
					                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender20" runat="server" FilterType="Numbers,Custom"
                                        ValidChars="." TargetControlID="txtOthers" />
					                
					                </div>
			                          <div class="form-group col-md-2">
					                    <label for="exampleInputName">No Of Bale</label>
                                        <asp:TextBox ID="txtBaleNo" class="form-control" runat="server" Text="0" ></asp:TextBox>
                                        <asp:RequiredFieldValidator Display="Dynamic" ControlToValidate="txtBaleNo" InitialValue="0" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator14" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                        </asp:RequiredFieldValidator>
                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender11" runat="server" FilterType="Numbers,Custom"
                                            ValidChars="." TargetControlID="txtBaleNo" />
                                    </div>
                                         <div class="form-group col-md-2">
					                    <label for="exampleInputName">Candy Weight</label>
                                        <asp:TextBox ID="txtCandyweight" class="form-control" runat="server" Text="0"></asp:TextBox>
                                        <asp:RequiredFieldValidator Display="Dynamic" ControlToValidate="txtCandyweight" InitialValue="0" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator16" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                        </asp:RequiredFieldValidator>
                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender13" runat="server" FilterType="Numbers,Custom"
                                            ValidChars="." TargetControlID="txtCandyweight" />
					                </div>
					            </div>
					        </div>
					     
			            
	   		    	      
			    	        <div class="col-md-12">
					            <div class="row">
					               
					               
					                <div class="form-group col-md-2">
					                    <label for="exampleInputName">Bale Weight</label>
                                        <asp:TextBox ID="txtBaleWeight" class="form-control" runat="server" Text="0"></asp:TextBox>
                                        <asp:RequiredFieldValidator Display="Dynamic" ControlToValidate="txtBaleWeight" InitialValue="0" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator17" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                        </asp:RequiredFieldValidator>
                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender14" runat="server" FilterType="Numbers,Custom"
                                            ValidChars="." TargetControlID="txtBaleWeight" />
					                </div>
					                <div class="form-group col-md-4">
					                    <label for="exampleInputName">Net Amount</label>
                                        <asp:TextBox ID="txtNetAmount" class="form-control" runat="server" Text="0"></asp:TextBox>
                                        <asp:RequiredFieldValidator Display="Dynamic" ControlToValidate="txtNetAmount" InitialValue="0" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator18" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                        </asp:RequiredFieldValidator>
                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender15" runat="server" FilterType="Numbers,Custom"
                                            ValidChars="." TargetControlID="txtNetAmount" />
					                </div>
					            </div>
					        </div>
				  
				            <!-- Button start -->   
		                    <div class="txtcenter">
                                <asp:Button ID="btnSave" class="btn btn-success"  runat="server" Text="Save" 
                                    ValidationGroup="Validate_Field" onclick="btnSave_Click"  />
                                <%--  <asp:Button ID="Button1" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg" runat="server" Text="View" />--%>
                                <asp:Button ID="btnCancel" class="btn btn-danger" runat="server" Text="Cancel" 
                                    onclick="btnCancel_Click" />
                                <asp:Button ID="btnBackEnquiry" class="btn btn-success" runat="server" 
                                    Text="Back" onclick="btnBackEnquiry_Click"  />
                                <asp:Button ID="btnApprove" class="btn btn-success" runat="server" 
                                    Text="Approve" onclick="btnApprove_Click" />   
                            </div>
                            <!-- Button end -->					
				        </div><!-- panel body end -->
				    </form>
			    </div><!-- panel white end -->
			</div><!-- col-9 end -->
		    <!-- Dashboard start -->
		    <div class="col-lg-3 col-md-6">
                <div class="panel panel-white" style="height: 100%;">
                    <div class="panel-heading">
                        <h4 class="panel-title">Dashboard Details</h4>
                        <div class="panel-control">
                        </div>
                    </div>
                    <div class="panel-body">
                    </div>
                </div>
            </div>  
                        
            <div class="col-lg-3 col-md-6">
                <div class="panel panel-white">
                    <div class="panel-body">
                        <div class="live-tile flip ha" data-mode="flip" data-speed="750" data-delay="3000">
                        </div>
                    </div>
                </div>
               
            
			</div> 
		    <!-- Dashboard End -->
		    <div class="col-md-2"></div>
		    <div class="col-md-2"></div>
        </div> <!-- col 12 end -->
    </div><!-- row end -->
</div><!-- main-wrapper end -->
 



</asp:Content>

