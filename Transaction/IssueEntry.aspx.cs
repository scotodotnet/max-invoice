﻿
using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;


public partial class Transaction_IssueEntry : System.Web.UI.Page
{

    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionEnquiryNo;
    string SessionFinYearCode;
    string SessionFinYearVal;
    DataTable qry_dt = new DataTable();
    DataTable dtd1 = new DataTable();
    bool EventFire = false;
    static string sum;
    static decimal Totalbale;
    string Auto_Transaction_No;
    string SessionIssue_Entry_No;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();
        if (!IsPostBack)
        {
            Initial_Data_Referesh();
            //Initial_Data_RefereshGV();
            LoadUnit();
            Color();
            Load_LotNo();
            Page.Title = "CMS | Issue Entry";
            HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Transaction"));
            li.Attributes.Add("class", "droplink active open");

          
            if (Session["Issue_Entry_No"] == null)
            {
                SessionIssue_Entry_No = "";
                txtIssueEntryDate.Text = System.DateTime.Now.ToString("dd/MM/yyyy");
            }
            else
            {
                SessionIssue_Entry_No = Session["Issue_Entry_No"].ToString();
             
                txtIssueEntryNo.Text = SessionIssue_Entry_No;  
                btnSearch_Click(sender, e);
           
            }
            //Count_Name_Load();
        }

        Load_Data_Empty_LotNo();
       

        Load_OLD_data1();
        //Load_OLD_data();
    }
    private void Color()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlColor.Items.Clear();
        query = "Select *from MstColor where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlColor.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["ColorName"] = "-Select-";
        dr["ColorName"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlColor.DataTextField = "ColorName";
        ddlColor.DataValueField = "ColorName";
        ddlColor.DataBind();
    }
    private void Load_LotNo()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlLotNo.Items.Clear();
        query = "select LotNo  from Stock_Transaction_Ledger where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' ";
        query = query + "group by LotNo having (sum(isnull(PO_Qty , '0'))  - sum(isnull(Issue_Qty , '0')) ) > 0";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlLotNo.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["LotNo"] = "-Select-";
        dr["LotNo"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlLotNo.DataTextField = "LotNo";
        ddlLotNo.DataValueField = "LotNo";
        ddlLotNo    .DataBind();
    }
    public void LoadUnit()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlUnit.Items.Clear();
        query = "Select *from MstUnit where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlUnit.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["UnitName"] = "-Select-";
        dr["UnitName"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlUnit.DataTextField = "UnitName";
        ddlUnit.DataValueField = "UnitName";
        ddlUnit.DataBind();
    }


    //private void Count_Name_Load()
    //{
    //    string query = "";
    //    DataTable Main_DT = new DataTable();

    //    txtCountCode.Items.Clear();
    //    query = "Select * from MstCount where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' order by CountName Asc";
    //    Main_DT = objdata.RptEmployeeMultipleDetails(query);
    //    txtCountCode.DataSource = Main_DT;
    //    DataRow dr = Main_DT.NewRow();
    //    dr["CountID"] = "-Select-";
    //    dr["CountName"] = "-Select-";
    //    Main_DT.Rows.InsertAt(dr, 0);
    //    txtCountCode.DataTextField = "CountID";
    //    txtCountCode.DataValueField = "CountID";
    //    txtCountCode.DataBind();
    //}

    protected void btnSave_Click(object sender, EventArgs e)
    {
        string query = "";
        DataTable DT_Check = new DataTable();
        string SaveMode = "Insert";
        bool ErrFlag = false;

        //check with Count Details Add with Grid

        if ((ddlUnit.SelectedValue == "") || (ddlUnit.SelectedValue == "-Select-"))
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select any one UNIT..');", true);
        }
        if ((ddlColor.SelectedValue == "") || (ddlColor.SelectedValue == "-Select-"))
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select any one Color..');", true);
        }
        if (txtLotNo.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the Yarn LotNo..');", true);
        }

        DT_Check = (DataTable)ViewState["CountTable"];
        if (DT_Check.Rows.Count == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You have to add atleast one  Issue Entry Details..');", true);
        }

        //User Rights Check Start
        bool Rights_Check = false;
        if (btnSave.Text == "Update")
        {
            //Rights_Check = CommonClass_Function.ModifyRights_Check(SessionCcode, SessionLcode, SessionUserID, "4", "5", "Issue Entry");
            //if (Rights_Check == false)
            //{
            //    ErrFlag = true;
            //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Modify Issue Entry Details..');", true);
            //}
        }
        else
        {
            //Rights_Check = CommonClass_Function.AddRights_Check(SessionCcode, SessionLcode, SessionUserID, "4", "5", "Issue Entry");
            //if (Rights_Check == false)
            //{
            //    ErrFlag = true;

            //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Adding New Issue Entry..');", true);
            //}
        }
        //User Rights Check End

        //Auto generate Transaction Function Call
        if (btnSave.Text != "Update")
        {
            if (!ErrFlag)
            {
                TransactionNoGenerate TransNO = new TransactionNoGenerate();
                string Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup(SessionCcode, SessionLcode, "Issue Entry", SessionFinYearVal);
                if (Auto_Transaction_No == "")
                {
                    ErrFlag = true;
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Auto Generate Transaction No Error... Contact Server Admin');", true);
                }
                else
                {
                    txtIssueEntryNo.Text = Auto_Transaction_No;
                }
            }
        }

        if (!ErrFlag)
        {
            query = "Select * from Issue_Entry_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Issue_Entry_No='" + txtIssueEntryNo.Text + "'";
            DT_Check = objdata.RptEmployeeMultipleDetails(query);
            if (DT_Check.Rows.Count != 0)
            {
                SaveMode = "Update";
                query = "Delete from Issue_Entry_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Issue_Entry_No='" + txtIssueEntryNo.Text + "'";
                objdata.RptEmployeeMultipleDetails(query);
                query = "Delete from Issue_Entry_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Issue_Entry_No='" + txtIssueEntryNo.Text + "'";
                objdata.RptEmployeeMultipleDetails(query);
            }

            //Insert Main Table

            query = "Insert Into Issue_Entry_Main(Ccode,Lcode,FinYearCode,FinYearVal,Issue_Entry_No,Issue_Entry_Date,YarnLotNo,TotalBale,TotalWeight,UserID,UserName,Issue_Entry_Status,Unit,ColorName) Values('" + SessionCcode + "',";
            query = query + " '" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + txtIssueEntryNo.Text + "','" + txtIssueEntryDate.Text + "','" + txtLotNo.Text + "','0','0',";
            query = query + " '" + SessionUserID + "','" + SessionUserName + "','0','" + ddlUnit.SelectedValue + "','" + ddlColor.SelectedValue + "')";
            objdata.RptEmployeeMultipleDetails(query);


             dtd1 = (DataTable)ViewState["CountTable"];

            for (int i = 0; i < dtd1.Rows.Count; i++)
            {
                query = "Insert Into Issue_Entry_Main_Sub(Ccode,Lcode,FinYearCode,FinYearVal,Issue_Entry_No,Issue_Entry_Date,LotNo,LotDate,InvoiceNo,InvoiceDate,CountCode,CountName,VarietyCode,VarietyName,Supp_Code,Supp_Name,BaleNo,NoOfBale,BaleWeight,GoDownID,GoDownName,UserID,UserName,Percents) Values('" + SessionCcode + "',";
                query = query + " '" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + txtIssueEntryNo.Text + "','" + txtIssueEntryDate.Text + "','" + dtd1.Rows[i]["LotNo"].ToString() + "',";
                query = query + " '" + dtd1.Rows[i]["LotDate"].ToString() + "','" + dtd1.Rows[i]["InvoiceNo"].ToString() + "','" + dtd1.Rows[i]["InvoiceDate"].ToString() + "','" + dtd1.Rows[i]["CountCode"].ToString() + "','" + dtd1.Rows[i]["CountName"].ToString() + "','" + dtd1.Rows[i]["VarietyCode"].ToString() + "','" + dtd1.Rows[i]["VarietyName"].ToString() + "','" + dtd1.Rows[i]["Supp_Code"].ToString() + "','" + dtd1.Rows[i]["Supp_Name"].ToString() + "','0',";
                query = query + " '" + dtd1.Rows[i]["baleno"].ToString() + "','" + dtd1.Rows[i]["BaleWeight"].ToString() + "','" + dtd1.Rows[i]["GoDownID"].ToString() + "','" + dtd1.Rows[i]["GoDownName"].ToString() + "','" + SessionUserID + "','" + SessionUserName + "','" + dtd1.Rows[i]["Percents"].ToString() + "')";
                objdata.RptEmployeeMultipleDetails(query);


            }
            if (SaveMode == "Insert")
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Issue Entry Details Saved Successfully');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Issue Entry Details Updated Successfully');", true);
            }


            Session["Rejection_Entry_No"] = txtIssueEntryNo.Text;
            btnSave.Text = "Update";
            Load_OLD_data1();
            EventFire = true;
            //BtnSubmit_Click(sender, e);

            //Response.Redirect("IssueEntry_Main.aspx");
        }
    }

    protected void GridDeleteClick(object sender, CommandEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["CountTable"];
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            if (dt.Rows[i]["baleno"].ToString() == e.CommandName.ToString())
            {
                dt.Rows.RemoveAt(i);
                dt.AcceptChanges();
            }
        }
        ViewState["CountTable"] = dt;
        Load_OLD_data1();
        Totalsum();

        string query;
        query = "update Weight_List_Main_Sub set Status='0' where ";
        query = query + " Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        query = query + " and LotNo='" + txtLotNo.Text + "' and BaleNo='" + e.CommandName.ToString() + "'";
        query = query + " And FinYearCode='" + SessionFinYearCode + "'";
        objdata.RptEmployeeMultipleDetails(query);

    }

    private void Clear_All_Field()
    {
        txtIssueEntryNo.Text = "";
        txtIssueEntryDate.Text = System.DateTime.Now.ToString("dd/MM/yyyy");
        txtLotNo.Text = "";
        txtPercent.Text = "0";
        txtTotalWeight.Text = "0";
        //txttotalBale.Text = "";
        //txttotalkgs.Text = "";
      
        //txtCountName.Text = "";
        Initial_Data_Referesh();
        //Initial_Data_RefereshGV();

        //Count_Name_Load();
        Load_OLD_data1();
    }

   
    private void Load_OLD_data()
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        //GVModule.DataSource = dt;
        //GVModule.DataBind();

        
    }


    private void Load_OLD_data1()
    {
        DataTable dtd1 = new DataTable();
        dtd1 = (DataTable)ViewState["CountTable"];
        RptIssueEntry.DataSource = dtd1;
        RptIssueEntry.DataBind();
       
    }




    protected void btnStationName_Click(object sender, EventArgs e)
    {

    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        //Search Gate_Pass_Out
        string query = "";
        DataTable Main_DT = new DataTable();
        query = "Select * from Issue_Entry_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Issue_Entry_No='" + txtIssueEntryNo.Text + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        if (Main_DT.Rows.Count != 0)
        {
            txtIssueEntryNo.Text = Main_DT.Rows[0]["Issue_Entry_No"].ToString();
            txtIssueEntryDate.Text = Main_DT.Rows[0]["Issue_Entry_Date"].ToString();
            txtLotNo.Text = Main_DT.Rows[0]["LotNo"].ToString();
            txtLotNo.Text = Main_DT.Rows[0]["YarnLotNo"].ToString();

            ddlUnit.SelectedValue = Main_DT.Rows[0]["Unit"].ToString().Trim();
            ddlColor.SelectedValue = Main_DT.Rows[0]["ColorName"].ToString();
            DataTable dt = new DataTable();

       

        //query = "select WLS.LotNo,WLS.BaleNo,WLS.BaleWeight from Weight_List_Main_Sub WLS INNER JOIN Weight_List_Main WLM";
        //query = query + " on WLS.Weight_List_No=WLM.Weight_List_No where";
        //query = query + " WLM.Lcode='" + SessionLcode + "' And WLM.FinYearCode='" + SessionFinYearCode + "'";
        //query = query + " And WLS.LotNo='" + txtLotNo.Text + "' and WLM.Issue_Status !='1'";
        //query = query + " And WLS.Lcode='" + SessionLcode + "' And WLS.FinYearCode='" + SessionFinYearCode + "'";
        //query = query + " And WLS.Ccode='" + SessionCcode + "' and WLM.Ccode='" + SessionCcode + "'";

            //query = "Select LotNo,BaleNo,BaleWeight from Issue_Entry_Main_Sub where Ccode='" + SessionCcode + "'";
            //query = query + "And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
            //query = query + "And Issue_Entry_No='" + txtIssueEntryNo.Text + "'";
            //dt = objdata.RptEmployeeMultipleDetails(query);
            //ViewState["ItemTable"] = dt;
            //GVModule.DataSource = dt;
            //GVModule.DataBind();

             
                btnSearch_LotNo_Click(sender, e);

            DataTable dtt = new DataTable();
            query = "Select ES.LotNo,LotDate,BaleNo,ES.BaleWeight,VarietyCode,VarietyName,Supp_Code,Supp_Name,InvoiceNo,InvoiceDate,GoDownID,GoDownName,NoOfBale,ES.CountCode,ES.CountName,Percents,IM.ColorName from ";
            query = query + " Issue_Entry_Main_Sub ES inner Join Issue_Entry_Main IM on ES.Issue_Entry_No=IM.Issue_Entry_No" ;
            query = query + " where ES.Ccode='" + SessionCcode + "' And ";
            query = query + " ES.Lcode='" + SessionLcode + "' And ES.FinYearCode='" + SessionFinYearCode + "'";
            query = query + " And ES.Issue_Entry_No='" + txtIssueEntryNo.Text + "'";
            dtt = objdata.RptEmployeeMultipleDetails(query);
            ViewState["CountTable"] = dtt;
            RptIssueEntry.DataSource = dtt;
            RptIssueEntry.DataBind();

            btnSave.Text = "Update";
        }
        else
        {

        }
    }


    protected void btnApprove_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        string query = "";
        DataTable dt_Lot = new DataTable();
        DataTable dt_Find = new DataTable();
        if ((txtIssueEntryNo.Text == "") || (txtIssueEntryNo.Text == "0"))
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Issues Number Missing');", true);
            ErrFlag = true;
        }
        if (!ErrFlag)
        {
            //Inert Data to LotNo_Stage
            //query = "select * from LotNo_Stage where YarnLotNo='" + txtLotNo.Text + "' and Unit='" + ddlUnit.SelectedValue + "' and ColorName='" + ddlColor.SelectedValue + "'";
            //dt_Lot = objdata.RptEmployeeMultipleDetails(query);
            //if (dt_Lot.Rows.Count > 0)
            //{
            //    query = "delete from LotNo_Stage where YarnLotNo='" + txtLotNo.Text + "' and Unit='" + ddlUnit.SelectedValue + "' and ColorName='" + ddlColor.SelectedValue + "'";
            //    objdata.RptEmployeeMultipleDetails(query);
            //}

            //query = "insert into LotNo_Stage(Unit,ColorName,YarnLotNo,Types,StartDate) Values(";
            //query = query + "'" + ddlUnit.SelectedValue + "','" + ddlColor.SelectedValue + "','" + txtLotNo.Text + "',";
            //query = query + "'Carding','" + txtIssueEntryDate.Text + "')";
            //objdata.RptEmployeeMultipleDetails(query);


            //Check Issues Table IssuesNo Available or Not
            query = "Select * from Issue_Entry_Main where Issue_Entry_No='" + txtIssueEntryNo.Text + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
            dt_Find = objdata.RptEmployeeMultipleDetails(query);

            if (dt_Find.Rows.Count > 0)
            {
                if (dt_Find.Rows[0]["Issue_Entry_Status"].ToString().Trim() == "1")
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('This Issues Already Approval');", true);
                    ErrFlag = true;
                }
                if (!ErrFlag)
                {
                    query = "Update Issue_Entry_Main set Issue_Entry_Status='1' where Issue_Entry_No='" + txtIssueEntryNo.Text + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                    objdata.RptEmployeeMultipleDetails(query);

                    dtd1 = (DataTable)ViewState["CountTable"];

                    for (int i = 0; i < dtd1.Rows.Count; i++)
                    {


                        DataTable dtdapp = new DataTable();

                        //Insert Data Stock Transaction Ledger
                        query = "insert into Stock_Transaction_Ledger(Ccode,Lcode,FinYearCode,FinYearVal,Trans_No,Trans_Date,Trans_Date_Str,Trans_Type,LotNo,VariteyCode,VariteyName,Issue_Bale,Issue_Qty,GoDownID,GoDownName,UserID,UserName)";
                        query = query + "values('" + SessionCcode + "','" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + txtIssueEntryNo.Text + "','" + Convert.ToDateTime(txtIssueEntryDate.Text).AddDays(0).ToString("MM/dd/yyyy") + "','" + txtIssueEntryDate.Text + "','ISSUE ENTRY','" + dtd1.Rows[i]["Lotno"].ToString() + "','" + dtd1.Rows[i]["VarietyCode"].ToString() + "','" + dtd1.Rows[i]["VarietyName"].ToString() + "',";
                        query = query + "'" + dtd1.Rows[i]["NoOfBale"].ToString() + "','" + dtd1.Rows[i]["BaleWeight"].ToString() + "','" + dtd1.Rows[i]["GoDownID"].ToString() + "','" + dtd1.Rows[i]["GoDownName"].ToString() + "','" + SessionUserID + "','" + SessionUserName + "')";
                        objdata.RptEmployeeMultipleDetails(query);

                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Issue Entry Approve Successfully');", true);

                    }
                }


               
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Save This Lotno First...');", true);
            }
            else
            {
                
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Approval Not given to this LotNo');", true);
        }

        Load_Data_Empty_LotNo();
        Response.Redirect("IssueEntry_Main.aspx");

    }

    protected void GridViewClick_VarietyCode(object sender, EventArgs e)
    {

    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }
    protected void btnTransportName_Click(object sender, EventArgs e)
    {

    }
    protected void btnAddVaritey_Click(object sender, EventArgs e)
    {

    }
    private void Initial_Data_Referesh()
    {
        DataTable dtd1 = new DataTable();
        dtd1.Columns.Add(new DataColumn("Lotno", typeof(string)));
        dtd1.Columns.Add(new DataColumn("baleno", typeof(string)));
        dtd1.Columns.Add(new DataColumn("BaleWeight", typeof(string)));
        dtd1.Columns.Add(new DataColumn("VarietyCode", typeof(string)));
        dtd1.Columns.Add(new DataColumn("VarietyName", typeof(string)));
        dtd1.Columns.Add(new DataColumn("Supp_Code", typeof(string)));
        dtd1.Columns.Add(new DataColumn("TotalWeight", typeof(string)));
        dtd1.Columns.Add(new DataColumn("Supp_Name", typeof(string)));
        dtd1.Columns.Add(new DataColumn("InvoiceNo", typeof(string)));
        dtd1.Columns.Add(new DataColumn("InvoiceDate", typeof(string)));
        dtd1.Columns.Add(new DataColumn("LotDate", typeof(string)));
        dtd1.Columns.Add(new DataColumn("GoDownID", typeof(string)));
        dtd1.Columns.Add(new DataColumn("GoDownName", typeof(string)));
        dtd1.Columns.Add(new DataColumn("CountCode", typeof(string)));
        dtd1.Columns.Add(new DataColumn("CountName", typeof(string)));
        dtd1.Columns.Add(new DataColumn("Percents", typeof(string)));
        dtd1.Columns.Add(new DataColumn("ColorName", typeof(string)));
        RptIssueEntry.DataSource = dtd1;
        RptIssueEntry.DataBind();
        ViewState["CountTable"] = RptIssueEntry.DataSource;


    }


    

    

    protected void BtnCountCode_Click(object sender, EventArgs e)
    {
        //Mdlpopup_LotNo.Show();
    }


    protected void GridViewClick_LotNo(object sender, CommandEventArgs e)
    {
   
        txtLotNo.Text = Convert.ToString(e.CommandArgument);
   
        btnSearch_LotNo_Click(sender, e);
    }


    protected void BtnCountCode_Click(object sender, CommandEventArgs e)
    {
        txtLotNo.Text = Convert.ToString(e.CommandArgument);

        btnSearch_LotNo_Click(sender, e);
    }

    protected void GridDeleteCottonInwardsClick(object sender, CommandEventArgs e)
    {
    
    }

   

    public void Totalsum()
    {
        sum = "0";
        Totalbale = 0;
        DataTable dtd1 = new DataTable();

        dtd1 = (DataTable)ViewState["CountTable"];

        if (dtd1.Rows.Count > 0)
        {

            for (int i = 0; i < dtd1.Rows.Count; i++)
            {
                //txtAllBale.Text = (GVModule.Rows.Count).ToString();
                sum = (Convert.ToDecimal(sum) + Convert.ToDecimal(dtd1.Rows[i]["BaleWeight"])).ToString();
                Totalbale = Convert.ToDecimal(Totalbale) + 1;
                //txttotalBale.Text = Convert.ToString(Totalbale);
                //txttotalkgs.Text = sum;
            }
        }
        else
        {
            //txttotalBale.Text = "";
           // txttotalkgs.Text = "";
            //txtAllBale.Text = "";
        }
        //txtNetAmt.Text = ((Convert.ToDecimal(txtTotalWeight.Text) + Convert.ToDecimal(txtTaxAmt.Text) + Convert.ToDecimal(txtOtherCharge.Text)) - (Convert.ToDecimal(txtDiscount.Text))).ToString();
    }

    protected void GridViewClick_CountCode(object sender, CommandEventArgs e)
    {
        //txtCountCode.Text = Convert.ToString(e.CommandArgument);
        //txtCountName.Text = Convert.ToString(e.CommandName);
    }

    protected void btnSearch_LotNo_Click(object sender, EventArgs e)
    {
       
        //Search Gate_Pass_Out
        string query = "";
        DataTable Main_DT = new DataTable();


        DataTable dt = new DataTable();
        //ViewState["ItemTable"] = "";
        //Main_DT = (DataTable)ViewState["ItemTable"];
        string lotno;
        string baleno;
        string baleweight;
        string GoDownID;
        string GoDownName;

        query = "Select LotNo,BaleNo,InvoiceWeight as BaleWeight,GoDownID,GoDownName from Temp_Upload_Stock where InvoiceWeight !='' and  LotNo='" + txtLotNo.Text + "'";
        query = query + " and Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";

        Main_DT = objdata.RptEmployeeMultipleDetails(query);


        //query = "Select WLM.LotNo,WLM.BaleNo,WLM.BaleWeight from Weight_List_Main WL inner join Weight_List_Main_Sub WLM";
        //query = query + " on WL.Weight_List_No=WLM.Weight_List_No where";
        //query = query + " WL.LotNo='" + txtLotNo.Text + "' and WL.WL_Status='1' And (WLM.Status is null or WLM.Status='0') and";
        //query = query + " WL.Ccode='" + SessionCcode + "' And  WL.Lcode='" + SessionLcode + "' And  WL.FinYearCode='" + SessionFinYearCode + "' And";
        //query = query + " WLM.Ccode='" + SessionCcode + "' And  WLM.Lcode='" + SessionLcode + "' And  WLM.FinYearCode='" + SessionFinYearCode + "'";

        //ViewState["ItemTable"] = Main_DT;

        DataRow dr = null;
        DataTable GVdata = new DataTable();
        //GVdata = (DataTable)ViewState["ItemTable"];

        //ViewState["ItemTable"] = dt;
        //GVModule.DataSource = dt;
        //GVModule.DataBind();

        GVdata.Columns.Add("Lotno");
        GVdata.Columns.Add("baleno");
        GVdata.Columns.Add("BaleWeight");
        GVdata.Columns.Add("GoDownID");
        GVdata.Columns.Add("GoDownName");


        for (int i = 0; i < Main_DT.Rows.Count; i++)
        {

            if (Main_DT.Rows.Count > 0)
            {
                lotno = Main_DT.Rows[i]["LotNo"].ToString();
                baleno = Main_DT.Rows[i]["BaleNo"].ToString();
                baleweight = Main_DT.Rows[i]["BaleWeight"].ToString();
                GoDownID = Main_DT.Rows[i]["GoDownID"].ToString();
                GoDownName = Main_DT.Rows[i]["GoDownName"].ToString();


                query = "Select LotNo,BaleNo,BaleWeight from Issue_Entry_Main_Sub where Ccode='" + SessionCcode + "'";
                query = query + "And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
                query = query + "And LotNo = '" + lotno + "' and BaleNo='" + baleno + "'";
                dt = objdata.RptEmployeeMultipleDetails(query);


                if (dt.Rows.Count == 0)
                {
                    dr = GVdata.NewRow();
                    dr["Lotno"] = lotno;
                    dr["baleno"] = baleno;
                    dr["BaleWeight"] = baleweight;
                    dr["GoDownID"] = GoDownID;
                    dr["GoDownName"] = GoDownName;

                    GVdata.Rows.Add(dr);
                }
                else
                {
                    //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Already added for this Lot No');", true);
                }
            }
        }
   
        if (Main_DT.Rows.Count != 0)
        {
            DataTable dt_New = new DataTable();
            //GVModule.DataSource = GVdata;
            //GVModule.DataBind();
            //ViewState["ItemTable"] = GVModule.DataSource;


            query = "";
            query = query + "Select Count_Code,Count_Name from Stock_Transaction_Ledger where LotNo='" + Main_DT.Rows[0]["LotNo"].ToString() + "' and ";
            query = query + "Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
            dt_New = objdata.RptEmployeeMultipleDetails(query);
            if (dt_New.Rows.Count > 0)
            {
                //txtCountCode.Text = dt_New.Rows[0]["Count_Code"].ToString();
                //txtCountName.Text = dt_New.Rows[0]["Count_Name"].ToString();
            }
        }
        //else
        //{
        //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Data Available for this lot number');", true);
        //}
 }
    private void Load_Data_Empty_LotNo()
    {
        string query = "";
        DataTable DT = new DataTable();
        DataTable dtVal = new DataTable();
        DataTable Main_DT = new DataTable();

        query = "select LotNo,VariteyCode,VariteyName,sum(InvoiceWeight) As InvoiceWeight from Temp_Upload_Stock";
        query = query + " where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        //query = query + " And OP_Status='1'";
        query = query + " and InvoiceWeight !='0' Group by LotNo,VariteyCode,VariteyName";

        DT = objdata.RptEmployeeMultipleDetails(query);

        //for (int i = DT.Rows.Count - 1; i >= 0; i--)
        //{
        //    query = "Select LotNo,BaleNo from Temp_Upload_Stock where InvoiceWeight !='' and  LotNo='" + DT.Rows[i]["LotNo"].ToString() + "'";
        //    query = query + " and Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        //    Main_DT = objdata.RptEmployeeMultipleDetails(query);
        //    for (int j = 0; j < Main_DT.Rows.Count; i++)
        //    {

        //    }

        //    query = "select LotNo from Issue_Entry_Main where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and LotNo='" + DT.Rows[i]["LotNo"].ToString() + "' ";
        //    dtVal = objdata.RptEmployeeMultipleDetails(query);
        //    if (dtVal.Rows.Count != 0)
        //    {
        //        DT.Rows[i].Delete();
        //    }
        //}

        //DT.AcceptChanges();

        //RepeaterLotNo.DataSource = DT;
        //RepeaterLotNo.DataBind();
        //RepeaterLotNo.Visible = true;
    }


    protected void GridViewClick_Trans(object sender, CommandEventArgs e)
    {

    }

  

    protected void btnBackEnquiry_Click(object sender, EventArgs e)
    {
        Response.Redirect("IssueEntry_Main.aspx");
    }

   



    protected void ddlLotNo_SelectedIndexChanged(object sender, EventArgs e)
    {
        TempLoad();
        //DataTable dt = new DataTable();
        //string Query = "select ISNULL(Percents ,'0') as Percents from Issue_Entry_Main_Sub order by CAST(Percents as decimal(18,2)) desc";
        //dt = objdata.RptEmployeeMultipleDetails(Query);
        //if (dt.Rows.Count > 0)
        //{
        //    txtPercent.Text = dt.Rows[0]["Percents"].ToString();
        //    txtPercent_TextChanged(sender, e);
            
        //}
        //else
        //{
        //    txtPercent.Text = "0";
        //}
    }
    protected void txtPercent_TextChanged(object sender, EventArgs e)
    {
        //DataTable dt = new DataTable();
        //DataTable dt_Lot = new DataTable();
        //string TotalBale = "";
        //string Query = "select (sum(isnull(PO_Qty , '0'))  - sum(isnull(Issue_Qty , '0')) ) as TotalQty  from Stock_Transaction_Ledger  where LotNo='" + ddlLotNo.SelectedValue + "' ";
        //Query = Query + "having (sum(isnull(PO_Qty , '0'))  - sum(isnull(Issue_Qty , '0')) ) > 0";
        //dt = objdata.RptEmployeeMultipleDetails(Query);

        //if (dt.Rows.Count > 0)
        //{
        //    if (dt.Rows.Count > 0) { TotalBale = dt.Rows[0]["TotalQty"].ToString(); } else { TotalBale = "0"; }
        //    txtTotalWeight.Text = ((Convert.ToDecimal(TotalBale) * Convert.ToDecimal(txtPercent.Text)) / 100).ToString();
        //}
        //else
        //{
        //    txtTotalWeight.Text = "0";
        //}
       

       
    }
    public void TempLoad()
    {
        DataTable dt = new DataTable();
        string Query = "";
        Query = Query + "SELECT ColorName,CandyWeight FROM CottonInwards where LotNo='" + ddlLotNo.SelectedValue + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
        dt = objdata.RptEmployeeMultipleDetails(Query);
        if (dt.Rows.Count > 0)
        {
            txtTempColor.Text = dt.Rows[0]["ColorName"].ToString();
            txtTempRate.Text = dt.Rows[0]["CandyWeight"].ToString();
        }
    }
    protected void btnAddItem_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        DataTable dt = new DataTable();
        DataRow dr = null;
        DataTable dt_Cotton = new DataTable();
        //if ((txtNoofBale.Text == "0") || (txtNoofBale.Text == ""))
        //{
        //    ErrFlag = true;
        //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Give No of Bale Correctly..');", true);
        //}
        if ((txtPercent.Text == "0") || (txtPercent.Text == ""))
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Give Percent Correctly..');", true);
        }
        else if ((ddlColor.SelectedValue == "") || (ddlColor.SelectedValue == "-Select-"))
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select any one Color..');", true);
        }
        else if ((txtTotalWeight.Text == "0") || (txtTotalWeight.Text == ""))
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Give Bale Weight Correctly..');", true);
        }
        if (!ErrFlag)
        {
            string Final_Weight = "0";
            string Query = " select VarietyCode,VarietyName,Supp_Code,Supp_Name,InvoiceNo,InvoiceDate,LotDate,GoDownID,GoDownName,CountCode,CountName from CottonInwards where LotNo='" + ddlLotNo.SelectedValue + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
            dt_Cotton = objdata.RptEmployeeMultipleDetails(Query);
            if (dt_Cotton.Rows.Count > 0)
            {
                if (ViewState["CountTable"] != null)
                {
                    dt = (DataTable)ViewState["CountTable"];

                    //check Item Already add or not
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        if (dt.Rows[i]["Lotno"].ToString().ToUpper() == ddlLotNo.SelectedValue.ToString())
                        {
                            ErrFlag = true;
                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('This Lot No Already Added..');", true);
                        }
                    }

                    if (!ErrFlag)
                    {
                        dr = dt.NewRow();
                        dr["Lotno"] = ddlLotNo.SelectedValue;
                        dr["baleno"] = txtNoofBale.Text;
                        dr["BaleWeight"] = txtTotalWeight.Text;
                        dr["VarietyCode"] = dt_Cotton.Rows[0]["VarietyCode"].ToString();
                        dr["VarietyName"] = dt_Cotton.Rows[0]["VarietyName"].ToString();
                        dr["Supp_Code"] = dt_Cotton.Rows[0]["Supp_Code"].ToString();
                        dr["TotalWeight"] = txtTotalWeight.Text;
                        dr["Supp_Name"] = dt_Cotton.Rows[0]["Supp_Name"].ToString();
                        dr["InvoiceNo"] = dt_Cotton.Rows[0]["InvoiceNo"].ToString();
                        dr["InvoiceDate"] = dt_Cotton.Rows[0]["InvoiceDate"].ToString();
                        dr["LotDate"] = dt_Cotton.Rows[0]["LotDate"].ToString();
                        dr["GoDownID"] = dt_Cotton.Rows[0]["GoDownID"].ToString();
                        dr["GoDownName"] = dt_Cotton.Rows[0]["GoDownName"].ToString();
                        dr["CountCode"] = dt_Cotton.Rows[0]["CountCode"].ToString();
                        dr["CountName"] = dt_Cotton.Rows[0]["CountName"].ToString();
                        dr["Percents"] = txtPercent.Text;
                        dr["ColorName"] = ddlColor.SelectedValue;

                        dt.Rows.Add(dr);
                        ViewState["ItemTable"] = dt;
                        RptIssueEntry.DataSource = dt;
                        RptIssueEntry.DataBind();
                        if (txtNetWeight.Text == "") { txtNetWeight.Text = "0"; }
                        txtNetWeight.Text = (Convert.ToDecimal(txtNetWeight.Text) + Convert.ToDecimal(txtTotalWeight.Text)).ToString();

                        ddlLotNo.SelectedValue = "-Select-";
                        txtPercent.Text = "0";
                        txtTotalWeight.Text = "";
                        txtNoofBale.Text = "0";
                    }
                }
                else
                {
                    dr = dt.NewRow();
                    dr["Lotno"] = ddlLotNo.SelectedValue;
                    dr["baleno"] = txtNoofBale.Text;
                    dr["BaleWeight"] = txtTotalWeight.Text;
                    dr["VarietyCode"] = dt_Cotton.Rows[0]["VarietyCode"].ToString();
                    dr["VarietyName"] = dt_Cotton.Rows[0]["VarietyName"].ToString();
                    dr["Supp_Code"] = dt_Cotton.Rows[0]["Supp_Code"].ToString();
                    dr["TotalWeight"] = txtTotalWeight.Text;
                    dr["Supp_Name"] = dt_Cotton.Rows[0]["Supp_Name"].ToString();
                    dr["InvoiceNo"] = dt_Cotton.Rows[0]["InvoiceNo"].ToString();
                    dr["InvoiceDate"] = dt_Cotton.Rows[0]["InvoiceDate"].ToString();
                    dr["LotDate"] = dt_Cotton.Rows[0]["LotDate"].ToString();
                    dr["GoDownID"] = dt_Cotton.Rows[0]["GoDownID"].ToString();
                    dr["GoDownName"] = dt_Cotton.Rows[0]["GoDownName"].ToString();
                    dr["CountCode"] = dt_Cotton.Rows[0]["CountCode"].ToString();
                    dr["CountName"] = dt_Cotton.Rows[0]["CountName"].ToString();
                    dr["Percents"] = txtPercent.Text;
                    dr["ColorName"] = ddlColor.SelectedValue;
                    dt.Rows.Add(dr);
                    ViewState["ItemTable"] = dt;
                    RptIssueEntry.DataSource = dt;
                    RptIssueEntry.DataBind();

                    ddlLotNo.SelectedValue = "-Select-";
                    txtPercent.Text = "0";
                    txtTotalWeight.Text = "";
                    txtNoofBale.Text = "0";
                }
            }
            else
            {
                
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Cotton Inward Not Correctly..');", true);
            }
        
        }

    }
}
