<%@ Page Language="C#" AutoEventWireup="true"  CodeFile="Default.aspx.cs" Inherits="_Default" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
    <head>
        
        <!-- Title -->
        <title>ERP Module</title>
         <!-- Styles -->
        <link href='http://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700' rel='stylesheet' type='text/css'>
        <link href="assets/plugins/pace-master/themes/blue/pace-theme-flash.css" rel="stylesheet"/>
        <link href="assets/plugins/uniform/css/uniform.default.min.css" rel="stylesheet"/>
        <link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/plugins/fontawesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <link href="assets/plugins/line-icons/simple-line-icons.css" rel="stylesheet" type="text/css"/>	
        <link href="assets/plugins/waves/waves.min.css" rel="stylesheet" type="text/css"/>	
        <link href="assets/plugins/switchery/switchery.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/plugins/3d-bold-navigation/css/style.css" rel="stylesheet" type="text/css"/>	
        
        <!-- Theme Styles -->
        <link href="assets/css/modern.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/css/custom.css" rel="stylesheet" type="text/css"/>
        
        <script src="assets/plugins/3d-bold-navigation/js/modernizr.js"></script>
        
        
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <script type ="text/jscript" >
            function GoBack() {
                window.history.forward();
            }
        </script>
    
    </head>

    <body onload="GoBack();" class="page-login login-alt">
        <form id="form2" runat="server" >
            <cc1:ToolkitScriptManager runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true"
                ID="ScriptManager1" EnablePartialRendering="true">
            </cc1:ToolkitScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <main class="page-content">
            <div class="page-inner">
                <div id="main-wrapper">
                    <div class="row">
                        <div class="col-md-6 center">
                            <div class="login-box panel panel-white">
                                <div class="panel-body">
                                    
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                
                                            </div>
                                            <%--<img src="assets/images/common/logo.png" alt="SCOTO LOGO" width="50"/>--%>
                                            <div class="form-group"> 
                                                <asp:DropDownList ID="ddlCode" runat="server" AutoPostBack="true" 
                                                    CssClass="form-control" placeholder="Company Name" onselectedindexchanged="ddlCode_SelectedIndexChanged" required></asp:DropDownList>
                                            </div>
                                            <div class="form-group">
                                                <asp:DropDownList ID="ddlLocation" runat="server" AutoPostBack="true" 
                                                    Cssclass="form-control" placeholder="Location" onselectedindexchanged="ddlLocation_SelectedIndexChanged"></asp:DropDownList>
                                                    <%--onselectedindexchanged="ddlLocation_SelectedIndexChanged"--%>
                                            </div>
                                            
                                            <div class="form-group">
                                                <asp:DropDownList ID="ddlFinYear" runat="server" AutoPostBack="true" 
                                                    Cssclass="form-control" placeholder="Location"></asp:DropDownList>
                                            </div>
                                            
                                            <label id="ErrorDisplay" class="alert alert-danger" role="alert" runat="server">User ID and Password Incorrect...</label><br />
                                            
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group" style="text-align:right;">
                                                <%--<img src="assets/images/common/erp_icon.png" alt="Login Key" visible="false" width="70"/>--%>
                                                
                                            </div>
                                            
                                                <div class="form-group">
                                                    <asp:TextBox ID="txtusername" runat="server" CssClass="form-control" placeholder="User ID" required></asp:TextBox>
                                                    <%--<input type="email" class="form-control" placeholder="User ID" required>--%>
                                                    
                                                </div>
                                                <div class="form-group">
                                                    <asp:TextBox ID="txtpassword" runat="server" TextMode="Password" CssClass="form-control large validate[required]" placeholder="Password" required></asp:TextBox>
                                                    <%--<input type="password" class="form-control" placeholder="Password" required>--%>
                                                </div>
                                                <%--<button type="submit" class="btn btn-success btn-block">Login</button>--%>
                                                <asp:Button ID="btnLogin" runat="server" Text="Login" CssClass="btn btn-success btn-block" onclick="btnLogin_Click" />
                                            
                                        </div>
                                                                               
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!-- Row -->
                </div><!-- Main Wrapper -->
            </div><!-- Page Inner -->
        </main><!-- Page Content -->
	
</ContentTemplate>
</asp:UpdatePanel>
        <!-- Javascripts -->
        <script src="assets/plugins/jquery/jquery-2.1.3.min.js"></script>
        <script src="assets/plugins/jquery-ui/jquery-ui.min.js"></script>
        <script src="assets/plugins/pace-master/pace.min.js"></script>
        <script src="assets/plugins/jquery-blockui/jquery.blockui.js"></script>
        <script src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>
        <script src="assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js"></script>
        <script src="assets/plugins/switchery/switchery.min.js"></script>
        <script src="assets/plugins/uniform/jquery.uniform.min.js"></script>
        <script src="assets/plugins/classie/classie.js"></script>
        <script src="assets/plugins/waves/waves.min.js"></script>
        <script src="assets/js/modern.min.js"></script>    
        
        </form>
    </body>
</html>
