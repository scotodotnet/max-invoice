﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="MstOthers.aspx.cs" Inherits="MstOthers" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#example').dataTable();
            }
        });
    };
</script>

<style type="text/css">
.modalBackground
    {
        background-color: Black;
        filter: alpha(opacity=60);
        opacity: 0.6;
        z-index: 6000 !important;
    }
</style>

<script type="text/javascript">
    function SaveMsgAlert(msg) {
        swal(msg);
    }
</script>
   <style type="text/css">
        body
        {
            font-family: Arial;
            font-size: 10pt;
        }
      
        .modalPopup
        {
            background-color: #FFFFFF;
            width: 300px;
            border: 3px solid #0DA9D0;
            border-radius: 12px;
            padding: 0;
            z-index: 6001 !important;
        }
        .modalPopup .header
        {
            background-color: #2FBDF1;
            height: 30px;
            color: White;
            line-height: 30px;
            text-align: center;
            font-weight: bold;
            border-top-left-radius: 6px;
            border-top-right-radius: 6px;
        }
        .modalPopup .body
        {
            min-height: 50px;
            line-height: 30px;
            text-align: center;
            font-weight: bold;
        }
        .modalPopup .footer
        {
            padding: 6px;
        }
        .modalPopup .yes, .modalPopup .no
        {
            height: 23px;
            color: White;
            line-height: 23px;
            text-align: center;
            font-weight: bold;
            cursor: pointer;
            border-radius: 4px;
        }
        .modalPopup .yes
        {
            background-color: #2FBDF1;
            border: 1px solid #0DA9D0;
        }
        .modalPopup .no
        {
            background-color: #9F9F9F;
            border: 1px solid #5C5C5C;
        }
    </style>


<div class="page-breadcrumb">
    <ol class="breadcrumb container">
        <h4><li class="active">User Registration</li></h4> 
    </ol>
</div>
<div id="main-wrapper" class="container">
<div class="row">
    <div class="col-md-12">
        <div class="col-md-9">
			<div class="panel panel-white">
			<div class="panel panel-primary">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">User Registration</h4>
				</div>
			</div>
				<form class="form-horizontal">
				<div class="panel-body">
				
				  <div class="col-md-12">
				      <div class="row">
                         
                         <asp:Button ID="btnShow" runat="server" Text="Show Modal Popup" />
                     
			                <cc1:ModalPopupExtender ID="mpe" runat="server" PopupControlID="pnlPopup" TargetControlID="btnShow"
                                OkControlID="btnYes" CancelControlID="btnNo" BackgroundCssClass="modalBackground">
                            </cc1:ModalPopupExtender>
			            
			            <asp:Panel ID="pnlPopup" runat="server" CssClass="modalPopup" Style="display: none">
			               <div class="header">
                                Confirmation
                            </div>
                            <div class="body">
                                  <asp:DropDownList ID="DropDownList2" runat="server" class="form-control">
                                                                   <asp:ListItem>car</asp:ListItem>
                                                                   <asp:ListItem>van</asp:ListItem>
                                                                   <asp:ListItem>jeep</asp:ListItem>
                                                                </asp:DropDownList>
                            </div>
                            <div class="footer" align="right">
                                <asp:Button ID="btnYes" runat="server" Text="Yes" CssClass="yes" />
                                <asp:Button ID="btnNo" runat="server" Text="No" CssClass="no" />
                            </div>
			            </asp:Panel>
			       
				      </div>
				  </div>
				
				
					
                    <div class="form-group row"></div>
                    
                  
					
				    </div><!-- panel body end -->
				</form>
			</div><!-- panel white end -->
		</div><!-- col-9 end -->
		    
	   
		
      </div><!-- col 12 end -->
  </div><!-- row end -->
 </div><!-- main-wrapper end -->
  
</asp:Content>

