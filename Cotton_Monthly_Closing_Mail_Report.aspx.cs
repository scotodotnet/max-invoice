﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class Cotton_Monthly_Closing_Mail_Report : System.Web.UI.Page
{
    string SSQL = "";
    BALDataAccess objdata = new BALDataAccess();
    ReportDocument RD_Report = new ReportDocument();
    string SessionCcode;
    string SessionLcode;
    string SessionFinYearCode;
    string SessionFinYearVal;
    bool Errflag;
    string FromDate;
    string ToDate;
    DataTable AutoDataTable = new DataTable();    
    DateTime frmDate;
    DateTime toDate;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            //
        }
        else
        {

            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionFinYearCode = Session["FinYearCode"].ToString();
            SessionFinYearVal = Session["FinYear"].ToString();
            Stock_Details_Report();
        }
    }

    public void Stock_Details_Report()
    {
        DataTable StockDT = new DataTable();
        DataTable Department_DT = new DataTable();
        DataTable Item_DT = new DataTable();
        DataTable Open_Stck = new DataTable();
        DataTable Purchase_DT = new DataTable();
        DataTable Issue_DT = new DataTable();
        DataTable ZoneBin_DT = new DataTable();
        DataTable Item_UOM = new DataTable();
        DataTable GenReceiptDT = new DataTable();
        DataTable Scrab_DT = new DataTable();

        AutoDataTable.Columns.Add("DeptName");
        AutoDataTable.Columns.Add("Opening_Val");
        AutoDataTable.Columns.Add("PO_Val");
        AutoDataTable.Columns.Add("Issue_Val");

        FromDate = "01/" + DateTime.Now.AddMonths(-1).Month + "/" + DateTime.Now.AddMonths(-1).Year;
        var LastDayMonth = DateTime.DaysInMonth(DateTime.Now.AddMonths(-1).Year, DateTime.Now.AddMonths(-1).Month);
        ToDate = LastDayMonth + "/" + DateTime.Now.AddMonths(-1).Month + "/" + DateTime.Now.AddMonths(-1).Year;

        if (FromDate != "")
        {
            frmDate = Convert.ToDateTime(FromDate);
        }
        if (ToDate != "")
        {
            toDate = Convert.ToDateTime(ToDate);
        }


        SSQL = "Delete CMS_Stock_Details_Print";
        objdata.RptEmployeeMultipleDetails(SSQL);

        SSQL = "Select Distinct VarietyID,VarietyName from MstVariety where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
        Item_DT = objdata.RptEmployeeMultipleDetails(SSQL);
        for (int i = 0; i < Item_DT.Rows.Count; i++)
        {
            
            SSQL = "select ((sum(Open_Qty)+sum(PO_Qty))-(sum(Issue_Qty)+sum(Scrab_Qty))) as Open_Qty,((count(Open_Bale)+count(PO_Bale))-(sum(Issue_Bale)+sum(Scrab_Bale))) as Open_Bale from Stock_Transaction_Ledger where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "'";
            SSQL = SSQL + " And VariteyName='" + Item_DT.Rows[i]["VarietyName"].ToString() + "'";
            if (FromDate != "" && ToDate != "")
            {
                SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date, 103)<=CONVERT(DATETIME,'" + frmDate.AddDays(-1).ToString("dd/MM/yyyy") + "',103)";
            }
            else
            {
                SSQL = SSQL + "And Trans_Type='OPENING RECEIPT'";
            }

            Open_Stck = objdata.RptEmployeeMultipleDetails(SSQL);


            SSQL = "select sum(PO_Qty) as PO_Qty,count(PO_Bale) as PO_Bale";
            SSQL = SSQL + " from Stock_Transaction_Ledger";
            SSQL = SSQL + " where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "'";
            SSQL = SSQL + " And VariteyName='" + Item_DT.Rows[i]["VarietyName"].ToString() + "'";
            SSQL = SSQL + " And Trans_Type='WEIGHT LIST'";
            if (FromDate != "" && ToDate != "")
            {
                SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date_Str, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";
                SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date_Str, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
            }

            Purchase_DT = objdata.RptEmployeeMultipleDetails(SSQL);

            SSQL = "select sum(Issue_Qty) as Issue_Qty,count(Issue_Bale) as Issue_Bale";
            SSQL = SSQL + " from Stock_Transaction_Ledger";
            SSQL = SSQL + " where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "'";
            SSQL = SSQL + " And VariteyName='" + Item_DT.Rows[i]["VarietyName"].ToString() + "'";
            SSQL = SSQL + " And Trans_Type='ISSUE ENTRY'";
            if (FromDate != "" && ToDate != "")
            {
                SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date_Str, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";
                SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date_Str, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
            }
            Issue_DT = objdata.RptEmployeeMultipleDetails(SSQL);


            SSQL = "select sum(Scrab_Qty) as Scrab_Qty,count(Scrab_Bale) as Scrab_Bale";
            SSQL = SSQL + " from Stock_Transaction_Ledger";
            SSQL = SSQL + " where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "'";
            SSQL = SSQL + " And VariteyName='" + Item_DT.Rows[i]["VarietyName"].ToString() + "'";
            SSQL = SSQL + " And Trans_Type='SCRAB ENTRY'";
            if (FromDate != "" && ToDate != "")
            {
                SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date_Str, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";
                SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date_Str, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
            }
            Scrab_DT = objdata.RptEmployeeMultipleDetails(SSQL);



            SSQL = "Insert Into CMS_Stock_Details_Print(Ccode,Lcode,FinYearCode,FinYearVal,VarietyName,Opening_Qty,Opening_Bale,PO_Qty,PO_Bale,";
            SSQL = SSQL + "Issue_Qty,Issue_Bale,Scrab_Qty,Scrab_Bale,From_Date,To_Date,UserID,UserName) Values('" + SessionCcode + "','" + SessionLcode + "',";
            SSQL = SSQL + " '" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + Item_DT.Rows[i]["VarietyName"].ToString() + "',";
            if (Open_Stck.Rows.Count != 0)
            {
                if (Open_Stck.Rows[0]["Open_Qty"].ToString() != "")
                {
                    SSQL = SSQL + " '" + Open_Stck.Rows[0]["Open_Qty"] + "','" + Open_Stck.Rows[0]["Open_Bale"] + "',";
                }
                else
                {
                    SSQL = SSQL + " '0.00','0.00',";
                }
            }
            else
            {
                SSQL = SSQL + " '0.00','0.00',";
            }

            if (Purchase_DT.Rows.Count != 0)
            {
                if (Purchase_DT.Rows[0]["PO_Qty"].ToString() != "")
                {
                    SSQL = SSQL + "'" + Purchase_DT.Rows[0]["PO_Qty"] + "','" + Purchase_DT.Rows[0]["PO_Bale"] + "',";
                }
                else
                {
                    SSQL = SSQL + "'0.00','0.00',";
                }
            }
            else
            {
                SSQL = SSQL + "'0.00','0.00',";
            }
            if (Issue_DT.Rows.Count != 0)
            {
                if (Issue_DT.Rows[0]["Issue_Qty"].ToString() != "")
                {
                    SSQL = SSQL + "'" + Issue_DT.Rows[0]["Issue_Qty"] + "','" + Issue_DT.Rows[0]["Issue_Bale"] + "',";
                }
                else
                {
                    SSQL = SSQL + "'0.00','0.00',";
                }
            }
            else
            {
                SSQL = SSQL + "'0.00','0.00',";
            }
            if (Scrab_DT.Rows.Count != 0)
            {
                if (Scrab_DT.Rows[0]["Scrab_Qty"].ToString() != "")
                {
                    SSQL = SSQL + "'" + Scrab_DT.Rows[0]["Scrab_Qty"] + "','" + Scrab_DT.Rows[0]["Scrab_Bale"] + "',";
                }
                else
                {
                    SSQL = SSQL + "'0.00','0.00',";
                }
            }
            else
            {
                SSQL = SSQL + "'0.00','0.00',";
            }

            SSQL = SSQL + " '" + FromDate + "','" + ToDate + "','','')";
            objdata.RptEmployeeMultipleDetails(SSQL);
            //}

        }



        //Get Closing Stock
        SSQL = "Select VarietyName,Opening_Qty as OStockKg,Opening_Bale as OStockBale,PO_Qty as WListKg,PO_Bale as WListBale,Issue_Qty as IssueKg,Issue_Bale as IssueBale,";
        SSQL = SSQL + " Scrab_Qty as ScrabKg,Scrab_Bale as ScrabBale,From_Date as FromDate,To_Date as ToDate from CMS_Stock_Details_Print where";
        SSQL = SSQL + " Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
        SSQL = SSQL + " And CONVERT(DATETIME,From_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";
        SSQL = SSQL + " And CONVERT(DATETIME,To_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
        SSQL = SSQL + " order by VarietyName";

        GenReceiptDT = objdata.RptEmployeeMultipleDetails(SSQL);
        if (GenReceiptDT.Rows.Count != 0)
        {

            DataSet ds1 = new DataSet();
            ds1.Tables.Add(GenReceiptDT);
            RD_Report.Load(Server.MapPath("~/crystal/BaleStockConsuptionReport.rpt"));
            RD_Report.SetDataSource(ds1.Tables[0]);
            //Company Details Add
            DataTable dtdCompanyAddress = new DataTable();
            DataTable Close_Det = new DataTable();
            SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
            dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dtdCompanyAddress.Rows.Count != 0)
            {
                RD_Report.DataDefinition.FormulaFields["CompanyName"].Text = "'" + dtdCompanyAddress.Rows[0]["Cname"].ToString() + "'";
                RD_Report.DataDefinition.FormulaFields["Address1"].Text = "'" + dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString() + "'";
                RD_Report.DataDefinition.FormulaFields["Address2"].Text = "'" + dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString() + "'";
            }


            RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = RD_Report;
            CrystalReportViewer1.RefreshReport();
            CrystalReportViewer1.DataBind();



        }
        else
        {
            //
        }

    }
}
